<?php

namespace App\Http\Controllers;
// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\ATCB;
use App\Models\ATCBDivisi;
use App\Models\ATCBPosition;

class atcb_c extends Controller
{
    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wsalamatperusahaan")->where('id_alamat', '=', $selected)->delete();
        }

        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsalamatperusahaan')->where('id_alamat', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index(Request $request)
    {
        // $atcb = array();
        // $header = DB::table('rc_divisi_applicant_target_candidat_buffer')->get();
        // foreach ($header as $key => $val) {
        //     $r = array();
        //     $r['header'][$key] = $val;
        //     $child = DB::table('rc_applicant_target_candidat_buffer')->where('id_applicant_target', $val->id_applicant_target)->get();
        //     foreach ($child as $key2 => $val2) {
        //         $r['child'][$key2] = $val2;
        //     }
        //     $atcb[] = $r;
        // }
        // return ATCBPosition::with('divisi')->get();
        // $wsalamat = DB::table('rc_applicant_target_candidat_buffer')->get();
        
        // return view('re.atcb.index', ['atcb' => json_decode(json_encode($atcb), true), 'wsalamat' => $wsalamat]);
        $tahun_target = $request->tahun_target;
        $jumlah_posisi_filter = $request->jumlah_posisi_filter;
        
        $atcb = ATCB::all();
        $total_atcb = count($atcb);
        
        if ($tahun_target) {
            $atcb = ATCB::where('tahun_target', $tahun_target)->get();
        }

        foreach ($atcb as $a) {
            $divisions = ATCBDivisi::where('id_applicant_target', $a->id_applicant_target)
                ->select('nama_divisi')
                ->groupBy('nama_divisi')
                ->get();
            
            $jumlah_posisi_filter = 0;
            foreach ($divisions as $d) {
                $dids = ATCBDivisi::where('nama_divisi', $d->nama_divisi)
                    ->where('id_applicant_target', $a->id_applicant_target)
                    ->select('id_divisi_applicant_target')->get()->pluck('id_divisi_applicant_target');
                $positions = ATCBPosition::whereIn('id_divisi_applicant_target', $dids)
                    ->get();
                $d->positions = $positions;
                $jumlah_posisi_filter += collect($positions)->sum('target_buffer_quarter');
            } 
            $a->divisions = $divisions;
            $a->jumlah_posisi_filter = $jumlah_posisi_filter;
        }

        $atcb = collect($atcb)->sortByDesc('jumlah_posisi')->values();
        if ($jumlah_posisi_filter) {
            $atcb = collect($atcb)->sortByDesc('jumlah_posisi')->take($jumlah_posisi_filter)->values();
        }

        $year_options = ATCB::select('tahun_target')
            ->groupBy('tahun_target')
            ->get()->pluck('tahun_target');

        return view('re.atcb.index', compact('atcb', 'year_options', 'tahun_target', 'jumlah_posisi_filter', 'total_atcb'));
    }

    public function tambah()
    {
        $lk = DB::table('wsgruplokasikerja')->select('nama_grup_lokasi_kerja')->get();
        $glk['looks']=$lk;
        $tok = DB::table('wsorganisasi')->select('tingkat_organisasi')->get();
        $to['looks']=$tok;
        $wspo = DB::table('wsposisi')->select('nama_posisi')->get();
        $po['looks']=$wspo;
        $wsalamat = DB::table('rc_applicant_target_candidat_buffer')->get();
        return view('re.atcb.tambah',['glk'=>$glk, 'to'=>$to, 'po'=>$po]);
    }

    public function preview(Request $request)
    {
        $tahun_target = $request->tahun_target;

        $atcb = ATCB::select('nama_cabang_kantor')
            ->groupBy('nama_cabang_kantor')
            ->get();
        if ($tahun_target) {
            $atcb = ATCB::select('nama_cabang_kantor')
                ->where('tahun_target', $tahun_target)
                ->groupBy('nama_cabang_kantor')
                ->get();
        }

        foreach ($atcb as $a) {
            $aids = ATCB::where('nama_cabang_kantor', $a->nama_cabang_kantor)
                ->select('id_applicant_target')->get()->pluck('id_applicant_target');
            if ($tahun_target) {
                $aids = ATCB::where('nama_cabang_kantor', $a->nama_cabang_kantor)
                ->where('tahun_target', $tahun_target)
                ->select('id_applicant_target')->get()->pluck('id_applicant_target');
            }
            
            $divisions = ATCBDivisi::whereIn('id_applicant_target', $aids)
                ->select('nama_divisi')
                ->groupBy('nama_divisi')
                ->get();
            
            $total_position = 0;
            foreach ($divisions as $d) {
                $dids = ATCBDivisi::where('nama_divisi', $d->nama_divisi)
                    ->whereIn('id_applicant_target', $aids)
                    ->select('id_divisi_applicant_target')->get()->pluck('id_divisi_applicant_target');
                $d->positions = ATCBPosition::whereIn('id_divisi_applicant_target', $dids)
                    ->get();
                $total_position += count($d->positions);
            } 
            $a->divisions = $divisions;
            $a->total_position = $total_position;
        }

        return view('re.atcb.preview', ['atcb' => $atcb]);
    }

    public function detail($id_applicant_target)
    {


        $atcb = ATCB::where('id_applicant_target', $id_applicant_target)->first();
        $divisi = ATCBDivisi::with('positions')->where('id_applicant_target', $id_applicant_target)
            ->get();

        return view('re.atcb.detail', compact('atcb', 'divisi'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            // Insert TAHUN DAN CABANG di table rc_applicant_target_candidat_buffer
            $rc_applicant_target_candidat_buffer_id = DB::table('rc_applicant_target_candidat_buffer')->insertGetId([
                'tahun_target' => $request->tahun_target, 
                'nama_cabang_kantor' => $request->nama_cabang_kantor,
            ]);

            // Insert di table rc_divisi_applicant_target_candidat_buffer
            $nama_divisi = $request->nama_divisi;
            for ($i = 0; $i < count($nama_divisi); $i++) {
                $rc_divisi_applicant_target_candidat_buffer_id = DB::table('rc_divisi_applicant_target_candidat_buffer')->insertGetId([
                    'id_applicant_target' => $rc_applicant_target_candidat_buffer_id,
                    'nama_divisi' => $nama_divisi[$i],
                ]);

                // Insert di table rc_position_applicant_target_candidat_buffer
                $nama_posisi = $request->nama_posisi;
                for ($j = 0; $j < count($nama_posisi[$i]); $j++) {
                    $rc_position_applicant_target_candidat_buffer_id = DB::table('rc_position_applicant_target_candidat_buffer')->insertGetId([
                        'id_divisi_applicant_target' => $rc_divisi_applicant_target_candidat_buffer_id,
                        'nama_posisi' => $nama_posisi[$i][$j],
                        'tahun_total_vacant' => $request->tahun_total_vacant[$i][$j],
                        'rata_rata_perbulan' => $request->rata_rata_perbulan[$i][$j],
                        'tahun_buffer_candidate' => $request->tahun_buffer_candidate[$i][$j],
                        'target_buffer_candidate' => $request->target_buffer_candidate[$i][$j],
                        'tahun_buffer_quarter' => $request->tahun_target_buffer_quarter[$i][$j],
                        'target_buffer_quarter' => $request->target_buffer_quarter[$i][$j],
                    ]);

                     // Insert di table rc_quarter_applicant_target_candidat_buffer
                    for ($k = 4 * $j; $k < (4 * $j) + 4; $k++) {
                        DB::table('rc_quarter_divisi_applicant_target_candidat_buffer')->insert([
                            'id_position_applicant_target' => $rc_position_applicant_target_candidat_buffer_id,
                            'quarter' => $request->quarter[$i][$k],
                            'target_buffer' => $request->target_buffer[$i][$k],
                            'actual_buffer' => $request->actual_buffer[$i][$k],
                            'achievement_quarter' => $request->achivement_quarter[$i][$k],
                        ]);
                    }
                }
            }
        
            DB::commit();
            return redirect('/list_at');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return back();
        }
    }

    public function edit($id_applicant_target)
    {

        // Tahun dan cabang
        // $branchYear = DB::table('rc_applicant_target_candidat_buffer')
        //     ->select('tahun_target', 'nama_cabang_kantor')->where('id_applicant_target', $id_applicant_target)
        //     ->first();


        // table
        // $childTable = DB::table('rc_position_applicant_target_candidat_buffer')
        //     ->join('rc_divisi_applicant_target_candidat_buffer', 'rc_divisi_applicant_target_candidat_buffer.id_divisi_applicant_target', '=', 'rc_position_applicant_target_candidat_buffer.id_divisi_applicant_target')
        //     ->join('rc_quarter_divisi_applicant_target_candidat_buffer', 'rc_position_applicant_target_candidat_buffer.id_divisi_applicant_target', '=', 'rc_quarter_divisi_applicant_target_candidat_buffer.id_quarter_divisi_applicant_target')
        //     ->where('rc_position_applicant_target_candidat_buffer.id_position_applicant_target', $id_applicant_target)->get();

        // dd($childTable);

        $atcb = ATCB::where('id_applicant_target', $id_applicant_target)->first();
        $divisi = ATCBDivisi::with('positions')->where('id_applicant_target', $id_applicant_target)
            ->get();

        return view('re.atcb.edit', compact('atcb', 'divisi'));
    }

    public function update(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();

        try {
            // update di table rc_applicant_target_candidat_buffer
            // DB::table('rc_applicant_target_candidat_buffer')
            // ->where('id_applicant_target', $id)
            // ->update([
            //     'tahun_target' => $request->tahun_target, 
            //     'nama_cabang_kantor' => $request->nama_cabang_kantor,
            // ]);

            $divisi_ids = DB::table('rc_divisi_applicant_target_candidat_buffer')
                ->where('id_applicant_target', $id)
                ->get()->pluck('id_divisi_applicant_target');
            // return $divisi_ids;

            $position_ids = DB::table('rc_position_applicant_target_candidat_buffer')
                ->whereIn('id_divisi_applicant_target', $divisi_ids)
                ->get()->pluck('id_position_applicant_target');
            // return $position_ids;

            // Delete Quarter
            DB::table('rc_quarter_divisi_applicant_target_candidat_buffer')
                ->whereIn('id_position_applicant_target', $position_ids)
                ->delete();

            // Delete Position
            DB::table('rc_position_applicant_target_candidat_buffer')
                ->whereIn('id_position_applicant_target', $position_ids)
                ->delete();

            // Delete Divisi
            DB::table('rc_divisi_applicant_target_candidat_buffer')
                ->whereIn('id_divisi_applicant_target', $divisi_ids)
                ->delete();


            // Insert di table rc_divisi_applicant_target_candidat_buffer
            $nama_divisi = $request->nama_divisi;
            for ($i = 0; $i < count($nama_divisi); $i++) {
                $rc_divisi_applicant_target_candidat_buffer_id = DB::table('rc_divisi_applicant_target_candidat_buffer')->insertGetId([
                    'id_applicant_target' => $id,
                    'nama_divisi' => $nama_divisi[$i],
                ]);

                // Insert di table rc_position_applicant_target_candidat_buffer
                $nama_posisi = $request->nama_posisi;
                for ($j = 0; $j < count($nama_posisi[$i]); $j++) {
                    $rc_position_applicant_target_candidat_buffer_id = DB::table('rc_position_applicant_target_candidat_buffer')->insertGetId([
                        'id_divisi_applicant_target' => $rc_divisi_applicant_target_candidat_buffer_id,
                        'nama_posisi' => $nama_posisi[$i][$j],
                        'tahun_total_vacant' => $request->tahun_total_vacant[$i][$j],
                        'rata_rata_perbulan' => $request->rata_rata_perbulan[$i][$j],
                        'tahun_buffer_candidate' => $request->tahun_buffer_candidate[$i][$j],
                        'target_buffer_candidate' => $request->target_buffer_candidate[$i][$j],
                        'tahun_buffer_quarter' => $request->tahun_target_buffer_quarter[$i][$j],
                        'target_buffer_quarter' => $request->target_buffer_quarter[$i][$j],
                    ]);

                     // Insert di table rc_quarter_applicant_target_candidat_buffer
                    for ($k = 4 * $j; $k < (4 * $j) + 4; $k++) {
                        DB::table('rc_quarter_divisi_applicant_target_candidat_buffer')->insert([
                            'id_position_applicant_target' => $rc_position_applicant_target_candidat_buffer_id,
                            'quarter' => $request->quarter[$i][$k],
                            'target_buffer' => $request->target_buffer[$i][$k],
                            'actual_buffer' => $request->actual_buffer[$i][$k],
                            'achievement_quarter' => $request->achivement_quarter[$i][$k],
                        ]);
                    }
                }
            }
        
            DB::commit();
            return redirect('/list_at');
        } catch (\Exception $e) {
            DB::rollback();
            return back();
        }
    }

    public function hapus($id_applicant_target)
    {
        $del = ATCB::where('id_applicant_target', $id_applicant_target)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_at');
    }
}
