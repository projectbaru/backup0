<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DropdownController extends Controller
{

    function dropDownCity(Request $request)
    {
        $prov_id = $request->params;

        $data = DB::table('cities')->where('prov_id', $prov_id)->get();
        foreach ($data as $key => $value) {
            $data[$key]->id = $value->city_id;
            $data[$key]->text = $value->city_name;
            unset($data[$key]->city_id);
            unset($data[$key]->city_name);
            unset($data[$key]->prov_id);
        }

        return $this->succesWitdata($data);
    }

    function dropDownDistrict(Request $request)
    {
        $city_id = $request->params;
        $data = DB::table('districts')
            ->selectRaw("dis_id, dis_name")
            ->where('city_id', $city_id)
            ->get();
        foreach ($data as $key => $value) {
            $data[$key]->id = $value->dis_id;
            $data[$key]->text = $value->dis_name;

            unset($data[$key]->dis_id);
            unset($data[$key]->dis_name);
        }

        return $this->succesWitdata($data);
    }

    function dropDownSubDistrict(Request $request)
    {
        $dis_id = $request->params;
        $data = DB::table('subdistricts')
            ->selectRaw("subdis_id, subdis_name")
            ->where('dis_id', $dis_id)
            ->get();

        foreach ($data as $key => $value) {
            $data[$key]->id = $value->subdis_id;
            $data[$key]->text = $value->subdis_name;

            unset($data[$key]->subdis_id);
            unset($data[$key]->subdis_name);
        }

        return $this->succesWitdata($data);
    }
}
