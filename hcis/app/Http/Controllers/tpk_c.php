<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class tpk_c extends Controller
{
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_ptk_detail')->where('id_ptk', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function table_view()
    {
        $look_glk = DB::table('wsgruplokasikerja')->select('kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja')->get();
        $glk['looks'] = $look_glk;
        $look_posisi = DB::table('wsposisi')->select('kode_posisi','kode_perusahaan','nama_perusahaan', 'lokasi_kerja', 'tingkat_posisi', 'nama_grup_lokasi_kerja', 'deskripsi_pekerjaan_posisi')->get();
        $po['looks'] = $look_posisi;
        $look_lokasi =  DB::table('wstingkatposisi')->select('nama_tingkat_posisi', 'kode_tingkat_posisi')->get();
        $po['looks'] = $look_posisi;
        $look_peru = DB::table('wsperusahaan')->select('id_perusahaan')->get();
        $peru['looks'] = $look_posisi;

        $look_lk = DB::table('wslokasikerja')->select('kode_lokasi_kerja', 'nama_lokasi_kerja')->get();
        $lk['looks'] = $look_lk;

        $lokasiKerja = DB::table('wsposisi');
        $data['lokasiKerja'] = $lokasiKerja
            ->select('kode_lokasi_kerja', 'lokasi_kerja')
            ->groupBy('kode_lokasi_kerja', 'lokasi_kerja')
            ->distinct(['kode_lokasi_kerja', 'lokasi_kerja'])
            ->get();
        $data['grupLokasiKerja'] = $lokasiKerja
            ->select('kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja')
            ->groupBy('kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja')
            ->distinct(['kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja'])
            ->get();
        
        return view('re.ptk.index', [
            'lokasiKerja' => $data['lokasiKerja'],
            'grupLokasiKerja' => $data['grupLokasiKerja'],
        'peru' => $peru, 'glk' => $glk, 'lk' => $lk, 'po' => $po, 'data' => $data]);
    }

    public function index(Request $request)
    {

        $look_po = DB::table('wsposisi')->select('kode_posisi', 'nama_posisi', 'nama_jabatan')->get();
        $data_po['look'] = $look_po;
        $look_go = DB::table('wsgolongan')->select('kode_golongan')->get();
        $data_go['look'] = $look_go;
        $look_tgo = DB::table('wstingkatgolongan')->select('kode_tingkat_golongan')->get();
        $data_tgo['look'] = $look_go;
        $look_glk = DB::table('wsgruplokasikerja')->select('nama_grup_lokasi_kerja')->get();
        $data_glk['look'] = $look_glk;

        $tpk = DB::table('rc_ptk_detail');
        if(!empty($request->deskripsi)) {
            $tpk->where('periode_ptk', 'like', "%{$request->deskripsi}%");
        }
        if(!empty($request->nama_grup_lokasi_kerja)) {
            $tpk->where('nama_grup_lokasi_kerja', 'like', "%{$request->nama_grup_lokasi_kerja}%");
        }
        if(!empty($request->tingkat_jabatan)) {
            $tpk->where('nama_posisi', 'like', "%{$request->tingkat_jabatan}%");
        }
        if(!empty($request->nama_ptk)) {
            $tpk->where('nama_ptk', 'like', "%{$request->nama_ptk}%");
        }
        $tpk = $tpk->get();
        $ptk_detail = DB::table('rc_ptk_detail');
        if(!empty($request->deskripsi)) {
            $ptk_detail->where('periode_ptk', 'like', "%{$request->deskripsi}%");
        }
        if(!empty($request->nama_grup_lokasi_kerja)) {
            $ptk_detail->where('nama_grup_lokasi_kerja', 'like', "%{$request->nama_grup_lokasi_kerja}%");
        }
        if(!empty($request->tingkat_jabatan)) {
            $ptk_detail->where('tingkat_jabatan', 'like', "%{$request->tingkat_jabatan}%");
        }
        if(!empty($request->nama_ptk)) {
            $ptk_detail->where('nama_ptk', 'like', "%{$request->nama_ptk}%");
        }
        $ptk_detail = $ptk_detail->get();
        
        $look_pd = DB::table('rc_ptk_detail')->select('nama_ptk')->groupBy('nama_ptk')->get();
        $look_nptk['looks'] = $look_pd;
        $look_pos = DB::table('rc_ptk_detail')->select('tingkat_jabatan')->groupBy('tingkat_jabatan')->get();;
        $look_po['looks'] = $look_pos;
        $look_nglk = DB::table('rc_ptk_detail')->select('nama_grup_lokasi_kerja')->groupBy('nama_grup_lokasi_kerja')->get();;
        $look_glk['looks'] = $look_nglk;
        $look_peri = DB::table('rc_ptk_detail')->select('periode_ptk')->groupBy('periode_ptk')->get();;
        $look_per['looks'] = $look_peri;


        return view('re.ptk.view', ['look_per' => $look_per,'look_glk' => $look_glk,'look_po' => $look_po,'look_nptk' => $look_nptk, 'ptk_detail' => $ptk_detail, 'data' => $ptk_detail, 'data_po' => $data_po, 'data_go' => $data_go, 'data_glk' => $data_glk, 'tpk' => $tpk, 'request' => $request]);
    }

    public function submit_ptk(Request $request)
    {
        // dd($request->all());
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            // Fix Refresh Auto Insert: ini Gx Perlu
                // $select   = json_decode($hasPersonalTable->select);
                // if (!in_array('id_ptk', $select)) {
                //     array_push($select, 'id_ptk');
                // }
                // if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                //     $queryField         = null;
                //     $queryOperator      = null;
                //     $queryValue         = null;
                // } else {
                //     $queryField         = json_decode($hasPersonalTable->query_field);
                //     $queryOperator      = json_decode($hasPersonalTable->query_operator);
                //     $queryValue         = json_decode($hasPersonalTable->query_value);
                // }
                // $query = DB::table('rc_ptk_detail')->select($select);

                // if ($hasPersonalTable->nama_ptk) {
                //     $query->where('nama_ptk', $hasPersonalTable->nama_ptk);
                // }
                // if ($hasPersonalTable->kode_ptk) {
                //     $query->where('kode_ptk', $hasPersonalTable->kode_ptk);
                // }
                // if ($queryField) {
                //     $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                //         for ($i = 0; $i < count($queryField); $i++) {
                //             if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                //                 $date = date('Y-m-d', strtotime($queryValue[$i]));

                //                 if ($queryOperator[$i] == '%LIKE%') {
                //                     $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                //                 } else {
                //                     $sub->where($queryField[$i], $queryOperator[$i], $date);
                //                 }
                //             } else {
                //                 if ($queryOperator[$i] == '%LIKE%') {
                //                     $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                //                 } else {
                //                     $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                //                 }
                //             }
                //         }
                //     });
                // }
                // $ptk_detail = DB::table('rc_ptk_detail')->get();

                // $data = [
                //     'query' => $query->get(),
                //     'th'    => $select,
                //     'ptk_detail' => $ptk_detail
                // ];
            // End Fix

            $insert = DB::table('wsposisi')->where('nama_grup_lokasi_kerja', '=', $request->nama_grup_lokasi_kerja)->where('lokasi_kerja', '=', $request->lokasi_kerja)->get();
            foreach ($insert as $d) {
                for($a = 1; $a <= 12; $a++ ){
                    $periode = $request->tahun.str_pad($a,2,'0', STR_PAD_LEFT);
                    DB::table('rc_ptk_detail')->insert([
                        'kode_ptk' => $d->kode_posisi,
                        'nama_grup_lokasi_kerja' => $d->nama_grup_lokasi_kerja,
                        'nama_posisi' => $d->nama_posisi,
                        'nama_ptk' => $d->nama_organisasi,
                        'tingkat_jabatan' => $d->detail_tingkat_posisi,
                        'tksi_perusahaan' => $d->jumlah_karyawan_dengan_posisi_ini,
                        'kode_lokasi_kerja' => $d->kode_lokasi_kerja,
                        'nama_lokasi_kerja' => $d->lokasi_kerja,
                        'kode_grup_lokasi_kerja' => $d->kode_grup_lokasi_kerja,
                        'periode_ptk' => $periode,
                    ]);
                }
            }
            // return view('re.ptk.filterResult', $data);

        } else {

            // Fix Refresh Auto Insert: ini Gx Perlu

                // $look_po = DB::table('wsposisi')->select('kode_posisi', 'nama_posisi', 'nama_jabatan')->get();
                // $data_po['look'] = $look_po;
                // $look_go = DB::table('wsgolongan')->select('kode_golongan')->get();
                // $data_go['look'] = $look_go;
                // $look_tgo = DB::table('wstingkatgolongan')->select('kode_tingkat_golongan')->get();
                // $data_tgo['look'] = $look_go;
                // $look_glk = DB::table('wsgruplokasikerja')->select('nama_grup_lokasi_kerja')->get();
                // $data_glk['look'] = $look_glk;
                // $tpk = DB::table('rc_ptk_detail')->get();

            // ENd Fix

            $data = DB::table('wsposisi')->where('nama_grup_lokasi_kerja', '=', $request->nama_grup_lokasi_kerja)->where('lokasi_kerja', '=', $request->lokasi_kerja)->get();
            foreach ($data as $d) {
                for($a = 1; $a <= 12; $a++ ){
                    $periode = $request->tahun.str_pad($a,2,'0', STR_PAD_LEFT);
                    DB::table('rc_ptk_detail')->insert([
                        'kode_ptk' => $d->kode_posisi,
                        'nama_grup_lokasi_kerja' => $d->nama_grup_lokasi_kerja,
                        'nama_posisi' => $d->nama_posisi,
                        'nama_ptk' => $d->nama_organisasi,
                        'tingkat_jabatan' => $d->detail_tingkat_posisi,
                        'tingkat_jabatan' => $d->detail_tingkat_posisi,
                        'tksi_perusahaan' => $d->jumlah_karyawan_dengan_posisi_ini,
                        'kode_lokasi_kerja' => $d->kode_lokasi_kerja,
                        'nama_lokasi_kerja' => $d->lokasi_kerja,
                        'kode_grup_lokasi_kerja' => $d->kode_grup_lokasi_kerja,
                        'periode_ptk' => $periode,
                    ]);
                }
            }


            // return redirect("/list_table");

        }

        // $data = DB::select('select * from rc_ptk_detail where left(periode_ptk, 4) = ?', [$request->tahun]);
        $data = DB::select('select * from rc_ptk_detail where left(periode_ptk, 4) = ? AND right(periode_ptk, 2) = ?', [$request->tahun, '01']);

        foreach ($data as $d) {
            $data_transaksi_ptk_tujuan = DB::table('rc_transaksi_ptk')
                                            ->where('tipe_transaksi', '=', 'tambahan')
                                            ->where('kode_ptk_tujuan', '=', $d->kode_ptk)
                                            ->first();

            $data_transaksi_ptk_asal = DB::table('rc_transaksi_ptk')
                                            ->where('tipe_transaksi', '=', 'penggantian')
                                            ->where('kode_ptk_asal', '=', $d->kode_ptk)
                                            ->first();
            
            // dd($data_transaksi_ptk_tujuan);

            // $mp_akhir = $d->periode_ptk + $data_transaksi_ptk_tujuan->total_ptk_tujuan + $data_transaksi_ptk_asal->total_ptk_asal;
            $tksi_perusahaan = 0;
            if ($d->tksi_perusahaan != null)
                $tksi_perusahaan = $d->tksi_perusahaan;
            $vacant = $tksi_perusahaan -  $tksi_perusahaan;

            // $mp_awal_disetujui = $d->ptk_awal ? $d->ptk_awal : 0;
            // $mp_perubahan_tambah = $data_transaksi_ptk_tujuan ? $data_transaksi_ptk_tujuan->total_ptk_tujuan : 0;
            // $mp_perubahan_pengurangan = $data_transaksi_ptk_asal ? $data_transaksi_ptk_asal->total_ptk_asal : 0;
            // $mp_akhir = $mp_awal_disetujui + $mp_perubahan_tambah + $mp_perubahan_pengurangan;
            // $mp_saat_ini = $d->tksi_perusahaan;

            // DB::table('rc_detail_fulfillment_rate')->insert([
            //     'kode_departemen' => $d->kode_ptk,
            //     'nama_departemen' => $d->nama_ptk, 
            //     'nama_posisi' => $d->nama_posisi, 
            //     'cabang_kantor' => $d->nama_grup_lokasi_kerja, 
            //     'lokasi_kerja' => $d->nama_lokasi_kerja, 
            //     'level' => $d->tingkat_jabatan, 
            //     'mp_awal_disetujui' => $mp_awal_disetujui,
            //     'mp_perubahan_tambah' => $mp_perubahan_tambah,
            //     'mp_perubahan_pengurangan' => $mp_perubahan_pengurangan,
            //     'mp_akhir' => $mp_akhir,
            //     'mp_saat_ini' => $mp_saat_ini,
            //     'vacant' => $mp_akhir - $mp_saat_ini,
            // ]);            
        }

        /*
            $data_fulfullment_rate = DB::table('rc_detail_fulfillment_rate')
                                        ->sum('mp_akhir')
                                        ->sum('vacant'); */

        $data_fulfullment_rate = DB::table('rc_detail_fulfillment_rate')
                                    ->select('kode_departemen', 'nama_departemen', DB::raw('SUM(mp_akhir) AS sum_of_mp_akhir'), DB::raw('SUM(vacant) AS sum_of_vacant'))
                                    ->groupBy('kode_departemen', 'nama_departemen')
                                    ->get();
        //dd( $data_fulfullment_rate); 
        foreach ($data_fulfullment_rate as $d) {
            $fulfillment = $d->sum_of_mp_akhir / ($d->sum_of_mp_akhir + $d->sum_of_vacant + 0.000001);
            DB::table('rc_fulfillment_rate')->insert([
                'kode_departemen' => $d->kode_departemen,
                'nama_departemen' => $d->nama_departemen,
                'total_mp_akhir' => $d->sum_of_mp_akhir,
                'total_vacant' => $d->sum_of_vacant,
                'fulfillment' => $fulfillment
            ]);    
        }
        return redirect("/list_ptk");
    }

    // Fix Refresh Auto Insert
    public function list_data_ptk(Request $request)
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select   = json_decode($hasPersonalTable->select);
            if (!in_array('id_ptk', $select)) {
                array_push($select, 'id_ptk');
            }
            // print_r($select);
            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('rc_ptk_detail')->select($select);

            if(!empty($request->deskripsi)) {
                $query->where('periode_ptk', 'like', "%{$request->deskripsi}%");
            }
            if(!empty($request->nama_grup_lokasi_kerja)) {
                $query->where('nama_grup_lokasi_kerja', 'like', "%{$request->nama_grup_lokasi_kerja}%");
            }
            if(!empty($request->tingkat_jabatan)) {
                $query->where('nama_posisi', 'like', "%{$request->tingkat_jabatan}%");
            }
            if(!empty($request->nama_ptk)) {
                $query->where('nama_ptk', 'like', "%{$request->nama_ptk}%");
            }

            if ($hasPersonalTable->nama_ptk) {
                $query->where('nama_ptk', $hasPersonalTable->nama_ptk);
            }
            if ($hasPersonalTable->kode_ptk) {
                $query->where('kode_ptk', $hasPersonalTable->kode_ptk);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $ptk_detail = DB::table('rc_ptk_detail')->get();

            $data = [
                'query' => $query->get(),
                'th'    => $select,
                'ptk_detail' => $ptk_detail,
                'request' => $request
            ];

            return view('re.ptk.filterResult', $data);
        } else {

            return redirect("/list_table");
        }
    }

    public function filter_dua(Request $request)
    {
        $result = [];
        if ($request->nama_grup_lokasi_kerja && $request->nama_ptk && $request->tingkat_jabatan) {
            $result = DB::table('rc_ptk_detail')->where('nama_grup_lokasi_kerja', '=', $request->nama_grup_lokasi_kerja)->where('nama_ptk', '=', $request->nama_ptk)->where('nama_posisi', $request->tingkat_jabatan)->get();
        } else {
            $result = DB::table('rc_ptk_detail')->where('nama_grup_lokasi_kerja', '=', $request->nama_grup_lokasi_kerja)->orWhere('nama_ptk', '=', $request->nama_ptk)->orWhere('nama_posisi', $request->tingkat_jabatan)->get();
        }
        $ptk_detail = DB::table('rc_ptk_detail')->get();
        return view('re.ptk.view', ['ptk_detail' => $ptk_detail, 'data' => $result]);
    }

    public function detail($id_ptk)
    {
        // $wsposisi = DB::table('wsposisi')->where('id_posisi', $id_posisi)->get();
        // $data_ptk=DB::table('rc_ptk_detail')->get();
        $data_ptk = DB::table('rc_ptk_detail')->where('id_ptk', $id_ptk)->get();
        return view('re.ptk.detail', ['data_ptk' => $data_ptk]);
    }


    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode PTK',
                'value' => 'kode_ptk'
            ],
            [
                'text'  => 'Nama PTK',
                'value' => 'nama_ptk'
            ],
            [
                'text'  => 'Periode PTK',
                'value' => 'periode_ptk'
            ],
            [
                'text'  => 'Kode Grup Lokasi Kerja',
                'value' => 'kode_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Grup Lokasi Kerja',
                'value' => 'nama_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Kode Lokasi Kerja',
                'value' => 'kode_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Tingkat Jabatan',
                'value' => 'tingkat_jabatan'
            ],
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'PTK Awal',
                'value' => 'ptk_awal'
            ],
            [
                'text'  => 'PTK yang Digantikan',
                'value' => 'ptk_yang_digantikan'
            ],
            [
                'text'  => 'PTK Tambahan',
                'value' => 'ptk_tambahan'
            ],
            [
                'text'  => 'PTK Akhir',
                'value' => 'ptk_akhir'
            ],
            [
                'text'  => 'TKSI Perusahaan',
                'value' => 'tksi_perusahaan'
            ],
            [
                'text'  => 'TKSI Pengganti',
                'value' => 'tksi_pengganti'
            ],
            [
                'text'  => 'TKSI Final',
                'value' => 'tksi_final'
            ],
            [
                'text'  => 'Selisih',
                'value' => 'selisih'
            ],
            [
                'text'  => 'Tanggal Awal',
                'value' => 'tanggal_awal'
            ],
            [
                'text'  => 'Tanggal Akhir',
                'value' => 'tanggal_akhir'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Waktu User Input',
                'value' => 'waktu_user_input'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]           
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];
        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else {
            foreach ($test as $k => $v) {
                $test[$k]['style'] = "display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];


        return view('re.ptk.filter', $data);
    }

    function get_tampilan_baru()
    {
        return DB::table('reftable_ptk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id_ptk', 'DESC')
            ->first();
    }


    function get_tampilan($active)
    {
        return DB::table('reftable_ptk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function edit($id_ptk)
    {
        $data = DB::table('rc_ptk_detail')->where('id_ptk', $id_ptk)->first();
        if (!$data) {
            return redirect()->back();
        }

        $ptk_akhir=DB::table('rc_ptk_detail')->where('id_ptk', $id_ptk)->first();

        $data->tanggal_mulai_efektif = $data->tanggal_mulai_efektif  ? Carbon::parse($data->tanggal_mulai_efektif)->format('Y-m-d') : null;
        $data->tanggal_selesai_efektif = $data->tanggal_selesai_efektif ? Carbon::parse($data->tanggal_selesai_efektif)->format('Y-m-d') : null;
        return view('re.ptk.ubah', ['data' => $data]);
    }

    // public function edit($id_ptk){
    //     $ptk=DB::table('rc_ptk_detail')->get();
    //     return view('re.ptk.ubah',['ptk'=>$ptk]);

    // }

    public function tambah_detail()
    {
        return view('re.ptk.ubah');
    }
    public function tambah()
    {
        $ptk = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();
        return view('re.ptk.ubah', ['ptk' => $ptk]);
    }
  

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_perusahaan';
        } else {
            $select = ['id_ptk', 'nomor_permintaan', 'nama_ptk_tujuan'];
        }

        $query = DB::table('rc_ptk_detail')->select($select);

        if ($request->nama_ptk) {
            $query->where('nama_ptk', $request->nama_ptk);
        }

        if ($request->kode_ptk) {
            $query->where('kode_ptk', $request->kode_ptk);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('reftable_ptk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('reftable_ptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('reftable_ptk')->insert([
            'kode_ptk'   => $request->kode_ptk,
            'nama_ptk'   => $request->nama_ptk,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_data');
    }

    public function hapus($id_ptk)
    {
        $del =  DB::table('rc_ptk_detail')->where('id_ptk', $id_ptk)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
    }


    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("rc_ptk_detail")->where('id_ptk', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function get_number()
    {
        $max_id = DB::table('rc_ptk_detail')
            ->where('id_ptk', \DB::raw("(select max('id_ptk') from rc_ptk_detail)"))
            ->first();
        $next_ = 0;
        if (strlen($max_id->kode_ptk) < 9) {
            $next_ = 1;
        } else {
            $next_ = (int)substr($max_id->kode_ptk, -3) + 1;
        }
        $code_ = 'PTK-' . date('ymd') . str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    public function reset()
    {
        $hasPersonalTable = DB::table('rc_ptk_detail')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('reftable_ptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_ptk');
    }

    public function update(Request $request)
    {
        // dd($request->all());
        // dd($request->tksi_perusahaan);
        DB::table('rc_ptk_detail')->where('id_ptk', $request->id_ptk)->update([
            'kode_ptk'   => $request->kode_ptk,
            'nama_ptk'   => $request->nama_ptk,
            'periode_ptk'   => $request->periode_ptk,
            'kode_grup_lokasi_kerja'   => $request->kode_grup_lokasi_kerja,
            'nama_grup_lokasi_kerja'   => $request->nama_grup_lokasi_kerja,
            'kode_lokasi_kerja'  => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'  => $request->nama_lokasi_kerja,
            'kode_jabatan' => $request->kode_jabatan,
            'tingkat_jabatan' => $request->tingkat_jabatan,
            'kode_posisi' => $request->kode_posisi,
            'nama_posisi' => $request->nama_posisi,
            'ptk_awal' => $request->ptk_awal,
            'ptk_yang_digantikan' => $request->ptk_yang_digantikan,
            'ptk_tambahan' => $request->ptk_tambahan,
            'ptk_akhir' => $request->ptk_akhir,
            'tksi_perusahaan' => $request->tksi_perusahaan,
            'tksi_pengganti' => $request->tksi_pengganti,
            'tksi_final' => $request->tksi_final,
            'selisih' => $request->selisih,
            'tanggal_awal' => $request->tanggal_awal,
            'tanggal_akhir' => $request->tanggal_akhir,
            'keterangan' => $request->keterangan,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,

        ]);
        $periode_next = $request->periode_ptk + 1;
        $nama_ptk = $request->nama_ptk;
        $nama_posisi = $request->nama_posisi;
        $tingkat_jabatan = $request->tingkat_jabatan;

        
        DB::table('rc_ptk_detail')->where('periode_ptk', $periode_next)->where('nama_posisi', $nama_posisi)->where('nama_ptk', $nama_ptk)->where('tingkat_jabatan', $tingkat_jabatan)->update([
            'ptk_awal' =>$request->ptk_akhir
        ]);
        

        // START
        if (substr($request->periode_ptk, 4, 2) == "01") {
            $data_transaksi_ptk_tujuan = DB::table('rc_transaksi_ptk')
                                            ->where('tipe_transaksi', '=', 'tambahan')
                                            ->where('kode_ptk_tujuan', '=', $request->kode_ptk)
                                            ->first();

            $data_transaksi_ptk_asal = DB::table('rc_transaksi_ptk')
                                            ->where('tipe_transaksi', '=', 'penggantian')
                                            ->where('kode_ptk_asal', '=', $request->kode_ptk)
                                            ->first();
            
            $tksi_perusahaan = 0;
            if ($request->tksi_perusahaan != null)
                $tksi_perusahaan = $request->tksi_perusahaan;

            $mp_awal_disetujui = $request->ptk_awal ? $request->ptk_awal : 0;
            $mp_perubahan_tambah = $data_transaksi_ptk_tujuan ? $data_transaksi_ptk_tujuan->total_ptk_tujuan : 0;
            $mp_perubahan_pengurangan = $data_transaksi_ptk_asal ? $data_transaksi_ptk_asal->total_ptk_asal : 0;
            $mp_akhir = $mp_awal_disetujui + $mp_perubahan_tambah + $mp_perubahan_pengurangan;
            $mp_saat_ini = $tksi_perusahaan;
            $vacant = $mp_akhir -  $mp_saat_ini;

            DB::table('rc_detail_fulfillment_rate')->where('kode_departemen', $request->kode_ptk)
            ->update([
                'nama_departemen' => $request->nama_ptk, 
                'nama_posisi' => $request->nama_posisi, 
                'cabang_kantor' => $request->nama_grup_lokasi_kerja, 
                'lokasi_kerja' => $request->nama_lokasi_kerja, 
                'level' => $request->tingkat_jabatan, 
                'mp_awal_disetujui' => $mp_awal_disetujui,
                'mp_perubahan_tambah' => $mp_perubahan_tambah,
                'mp_perubahan_pengurangan' => $mp_perubahan_pengurangan,
                'mp_akhir' => $mp_akhir,
                'mp_saat_ini' => $mp_saat_ini,
                'vacant' => $mp_akhir - $mp_saat_ini,
            ]); 

            $data_fulfullment_rate = DB::table('rc_detail_fulfillment_rate')
                ->select('kode_departemen', 'nama_departemen', DB::raw('SUM(mp_akhir) AS sum_of_mp_akhir'), DB::raw('SUM(vacant) AS sum_of_vacant'))
                ->groupBy('kode_departemen', 'nama_departemen')
                ->get();

            foreach ($data_fulfullment_rate as $d) {
                $fulfillment = $d->sum_of_mp_akhir / ($d->sum_of_mp_akhir + $d->sum_of_vacant + 0.000001);
                DB::table('rc_fulfillment_rate')->where('kode_departemen', $d->kode_departemen)
                ->update([
                    'nama_departemen' => $d->nama_departemen,
                    'total_mp_akhir' => $d->sum_of_mp_akhir,
                    'total_vacant' => $d->sum_of_vacant,
                    'fulfillment' => $fulfillment
                ]);    
            }
        }
        // END

        return redirect('/list_ptk');
    }

    public function getLokasiKerjaOptions($nama_grup_lokasi_kerja)
    {
        $posisi = DB::table('wsposisi')
            ->where('nama_grup_lokasi_kerja', $nama_grup_lokasi_kerja)
            ->get();
    
        return response()->json($posisi);
    }

    public function getLokasiKerjaDetail($lokasi_kerja)
    {
        $posisi = DB::table('wsposisi')
            ->where('lokasi_kerja', $lokasi_kerja)
            ->first();
    
        return response()->json($posisi);
    }

    public function show(Request $request)
    {
        DB::statement("SET SQL_MODE=''");
        $rc_ptk_detail = DB::table('rc_ptk_detail');
        $rc_ptk_detail->select('*');
        $rc_ptk_detail->selectRaw("LEFT(periode_ptk, 4) as tahun_ptk");
        // $rc_ptk_detail->selectRaw("GROUP_CONCAT(LEFT(periode_ptk, 4) SEPARATOR ', ') as tahun_all");
        
        if(!empty($request->deskripsi)) {
            $rc_ptk_detail->where('periode_ptk', 'like', "%{$request->deskripsi}%");
        }
        if(!empty($request->nama_grup_lokasi_kerja)) {
            $rc_ptk_detail->where('nama_grup_lokasi_kerja', $request->nama_grup_lokasi_kerja);
        }
        if(!empty($request->tingkat_jabatan)) {
            $rc_ptk_detail->where('tingkat_jabatan', $request->tingkat_jabatan);
        }
        if(!empty($request->nama_ptk)) {
            $rc_ptk_detail->where('nama_ptk', $request->nama_ptk);
        }
        
        // $data['tahun_all'] = $rc_ptk_detail->first();

        $rc_ptk_detail->groupBy('kode_ptk');
        $rc_ptk_detail->groupByRaw("LEFT(periode_ptk, 4)");

        $rc_ptk_detail->orderByDesc('nama_grup_lokasi_kerja');
        $rc_ptk_detail = $rc_ptk_detail->get();
        $data['rc_ptk_detail'] = $rc_ptk_detail;

        $tahun = DB::table('rc_ptk_detail')
                            ->selectRaw("LEFT(periode_ptk, 4) as tahun")
                            ->groupByRaw("LEFT(periode_ptk, 4)")
                            ->get();
        $data['tahun'] = $tahun;
        

        // return dd($data);

        $data['request'] = $request;
        return view('re.ptk.preview', $data);

        /**
         * Print Preview Old
         */
        // $row_kode_ptk = \DB::table('rc_ptk_detail')
        //     ->select('kode_ptk', 'tingkat_jabatan',  'nama_posisi', 'nama_grup_lokasi_kerja', 'nama_lokasi_kerja')
        //     ->distinct()
        //     ->get();
        // $arr = $row_kode_ptk->toArray();

        // $kode = array_map(function ($row) {
        //     return $row->kode_ptk;
        // }, $arr);

        // $rc_ptk_detail = \DB::table('rc_ptk_detail')
        //     ->select('periode_ptk', 'kode_ptk', 'ptk_awal', 'ptk_yang_digantikan', 'ptk_tambahan', 'ptk_akhir', 'tksi_perusahaan', 'tksi_pengganti', 'selisih')
        //     ->whereIn('kode_ptk', $kode)
        //     ->get();

        // $arrPtkDetail = $rc_ptk_detail->toArray();

        // foreach ($row_kode_ptk as $key) {
        //     $key->periode_ptk = array_values(array_filter($arrPtkDetail, function ($row) use ($key) {
        //         return $row->kode_ptk == $key->kode_ptk;
        //     }));
        // }


        // return view('re.ptk.preview', compact('rc_ptk_detail', 'row_kode_ptk'));
        // End Old
    }
}

