<?php
namespace App\Http\Controllers;
// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class fr_c extends Controller
{
 
    public function index()
    {

        $ptk = DB::table('rc_ptk_detail')->get();
        $fr = DB::table('rc_fulfillment_rate')->get();
        $total_fr = DB::table('rc_fulfillment_rate')->sum('total_mp_akhir');
        $total_va=DB::table('rc_fulfillment_rate')->sum('total_vacant');
        return view('re.fr.index',['fr'=>$fr,'total_fr'=>$total_fr,'total_va'=>$total_va]);
        
    }
    
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_alamat')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function detail($kode_departemen){
        $fr_d = DB::table('rc_detail_fulfillment_rate')->where('kode_departemen', $kode_departemen)->get();


        return view('re.fr.detail',['fr_d'=>$fr_d]);
    }

    public function simpan(Request $request){
        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'  =>$request->kode_perusahaan,
            'nama_perusahaan'  =>$request->nama_perusahaan,
            'status_alamat_untuk_spt' =>$request->status_alamat_untuk_spt,
            'jenis_alamat'   =>$request->jenis_alamat,
            'alamat'  =>$request->alamat,
            'kota'  =>$request->kota,
            'kode_pos'=>$request->kode_pos,
            'telepon'=>$request->telepon,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_alamat'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        
        return redirect('/list_a');
    }

    public function duplicate(Request $request){
        
        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'   => $this->get_number(),
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'status_alamat_untuk_spt'   =>$request->status_alamat_untuk_spt,
            'jenis_alamat'   =>$request->jenis_alamat,
            'alamat'  =>$request->alamat,
            'kota'  =>$request->kota,
            'kode_pos'=>$request->kode_pos,
            'telepon'=>$request->telepon,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_alamat'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        Alert::alert('', 'Simpan Berhasil', '');
        return redirect('/list_a');
    }


    public function update(Request $request) {
        DB::table('wsalamatperusahaan')->where('id_alamat', $request->id_alamat)->update([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'status_alamat_untuk_spt'   =>$request->status_alamat_untuk_spt,
            'jenis_alamat'   =>$request->jenis_alamat,
            'alamat'  =>$request->alamat,
            'kota'  =>$request->kota,
            'kode_pos'=>$request->kode_pos,
            'telepon'=>$request->telepon,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        return redirect('/list_a');
    }
    public function tambah() {
        $look = DB::table('wsperusahaan')->select('id_perusahaan','kode_perusahaan','nama_perusahaan')->get();
        $data['looks'] = $look;
        
        $code_ = $this->get_number();

        return view('wsalamat.tambah',['data'=>$data, 'randomKode'=>$code_]);
    }

    public function get_number(){
        $max_id = DB::table('wsalamatperusahaan')
        ->where('id_alamat', \DB::raw("(select max(`id_alamat`) from wsalamatperusahaan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_alamat) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_alamat, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'AP-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
   

    public function edit($id_alamat) {
        $look = DB::table('wsperusahaan')->select('id_perusahaan','kode_perusahaan','nama_perusahaan')->get();
        $datap['looks'] = $look;
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();

        return view('wsalamat.edit',['wsalamat'=> $wsalamat],['datap'=>$datap]);
    }
    
    public function hapus($id_alamat){
        $del=DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_a');
    }
    
    

}
