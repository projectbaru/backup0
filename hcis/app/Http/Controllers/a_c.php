<?php

namespace App\Http\Controllers;
// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class a_c extends Controller
{
    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wsalamatperusahaan")->where('id_alamat', '=', $selected)->delete();
        }

        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsalamatperusahaan')->where('id_alamat', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_alamat', $select)) {
                array_push($select, 'id_alamat');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsalamatperusahaan')->select($select);

            if ($hasPersonalTable->nama_perusahaan) {
                $query->where('nama_perusahaan', $hasPersonalTable->nama_perusahaan);
            }
            if ($hasPersonalTable->kode_alamat) {
                $query->where('kode_alamat', $hasPersonalTable->kode_alamat);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'   => $select
            ];

            return view('ws.wsalamat.filterResult', $data);
        } else {
            $wsalamat = DB::table('wsalamatperusahaan')->get();
            return view('ws.wsalamat.index', ['wsalamat' => $wsalamat]);
        }
    }

    // public function allData()
    // {
    //     $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

    //     if($hasPersonalTable) {
    //         DB::table('wstampilantabledashboarduser_alamat')
    //         ->where('user_id', Auth::user()->id)
    //         ->update([
    //             'active'   => 0,
    //         ]);
    //     }

    //     $wsalamat=DB::table('wsalamatperusahaan')->get();
    //     return view('ws.wsalamat.index',['wsalamat'=>$wsalamat]);
    // }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'No Dokumen',
                'value' => 'no_dokumen'
            ],
            [
                'text'  => 'NIK Pemohon',
                'value' => 'nik_pemohon'
            ],
            [
                'text'  => 'Nama Pemohon',
                'value' => 'nama_pemohon'
            ],
            [
                'text'  => 'Jabatan Pemohon',
                'value' => 'jabatan_pemohon'
            ],
            [
                'text'  => 'Departemen Pemohon',
                'value' => 'departemen_pemohon'
            ],
            [
                'text'  => 'Penggantian Tenaga Kerja',
                'value' => 'penggantian_tenaga_kerja'
            ],
            [
                'text'  => 'Penambahan Tenaga Kerja',
                'value' => 'penambahan_tenaga_kerja'
            ],
            [
                'text'  => 'Head Hunter',
                'value' => 'head_hunter'
            ],
            [
                'text'  => 'Status Kepegawaian',
                'value' => 'status_kepegawaian'
            ],
            [
                'text'  => 'Kontrak Selama',
                'value' => 'kontrak_selama'
            ],
            [
                'text'  => 'Magang Selama',
                'value' => 'magang_selama'
            ],
            [
                'text'  => 'Jabatan Kepegawaian',
                'value' => 'jabatan_kepegawaian'
            ],
            [
                'text'  => 'Tingkat Kepegawaian',
                'value' => 'tingkat_kepegawaian'
            ],
            [
                'text'  => 'Tanggal Mulai Bekerja',
                'value' => 'tanggal_mulai_bekerja'
            ],
            [
                'text'  => 'Jumlah Kebutuhan',
                'value' => 'jumlah_kebutuhan'
            ],
            [
                'text'  => 'Jumlah Kebutuhan',
                'value' => 'jumlah_kebutuhan'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Alasan Penambahan Tenaga Kerja',
                'value' => 'alasan_penambahan_tenaga_kerja'
            ],
            [
                'text'  => 'Gaji',
                'value' => 'gaji'
            ],
            [
                'text'  => 'Tunjangan Kesehatan',
                'value' => 'tunjangan_kesehatan'
            ],
            [
                'text'  => 'Tunjangan Transportasi',
                'value' => 'tunjangan_transportasi'
            ],
            [
                'text'  => 'Tunjangan Komunikasi',
                'value' => 'tunjangan_komunikasi'
            ],
            [
                'text'  => 'Hari Kerja',
                'value' => 'hari_kerja'
            ],
            [
                'text'  => 'Jam Kerja',
                'value' => 'jam_kerja'
            ],
            [
                'text'  => 'Pengguna Ubah',
                'value' => 'pengguna_ubah'
            ]
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else {
            foreach ($test as $k => $v) {
                $test[$k]['style'] = "display:'block';";
            }
        }
        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsalamat.filter', $data);
    }

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_alamat')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function detail($id_alamat)
    {
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();
        $code_ = $this->get_number();
        return view('ws.wsalamat.detail', ['wsalamat' => $wsalamat, 'randomKode' => $code_]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_alamat';
        } else {
            $select = ['id_alamat', 'kode_alamat', 'jenis_alamat', 'nama_perusahaan'];
        }
        $query = DB::table('wsalamatperusahaan')->select($select);

        if ($request->nama_perusahaan) {
            $query->where('nama_perusahaan', $request->nama_perusahaan);
        }

        if ($request->kode_alamat) {
            $query->where('kode_alamat', $request->kode_alamat);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser_alamat')->insert([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_alamat'   => $request->kode_alamat,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_a');
    }

    public function simpan(Request $request)
    {
        $telep ='(021) '. $request->telepon;
        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   => $request->kode_alamat,
            'kode_perusahaan'   => $request->kode_perusahaan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'status_alamat_untuk_spt'   => $request->status_alamat_untuk_spt,
            'jenis_alamat'   => $request->jenis_alamat,
            'alamat'  => $request->alamat,
            'kota'  => $request->kota,
            'kode_pos' => $request->kode_pos,
            'telepon' => $telep,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'keterangan' => $request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_alamat'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        Alert::alert('', 'Simpan Berhasil', '');
        return redirect('/list_a');
    }

    public function duplicate(Request $request)
    {

        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   => $request->kode_alamat,
            'kode_perusahaan'   => $this->get_number(),
            'nama_perusahaan'   => $request->nama_perusahaan,
            'status_alamat_untuk_spt'   => $request->status_alamat_untuk_spt,
            'jenis_alamat'   => $request->jenis_alamat,
            'alamat'  => $request->alamat,
            'kota'  => $request->kota,
            'kode_pos' => $request->kode_pos,
            'telepon' => $request->telepon,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'keterangan' => $request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_alamat'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        Alert::alert('', 'Simpan Berhasil', '');
        return redirect('/list_a');
    }


    public function update(Request $request)
    {
        $telep ='(021) '. $request->telepon;
        DB::table('wsalamatperusahaan')->where('id_alamat', $request->id_alamat)->update([
            'kode_alamat'   => $request->kode_alamat,
            'kode_perusahaan'   => $request->kode_perusahaan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'status_alamat_untuk_spt'   => $request->status_alamat_untuk_spt,
            'jenis_alamat'   => $request->jenis_alamat,
            'alamat'  => $request->alamat,
            'kota'  => $request->kota,
            'kode_pos' => $request->kode_pos,
            'telepon' => $telep,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/list_a');
    }
    public function tambah()
    {
        $look = DB::table('wsperusahaan')->select('id_perusahaan', 'kode_perusahaan', 'nama_perusahaan')->get();
        $data['looks'] = $look;
        $prov = DB::table('provinces')->select('name')->get();
        $provi['looks'] = $prov;
        $code_ = $this->get_number();

        return view('ws.wsalamat.tambah', ['provi' => $provi, 'data' => $data, 'randomKode' => $code_]);
    }

    public function get_number()
    {
        $max_id = DB::table('wsalamatperusahaan')
            ->where('id_alamat', \DB::raw("(select max(`id_alamat`) from wsalamatperusahaan)"))
            ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->kode_alamat) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->kode_alamat, -3) + 1;
            }
        } else {
            $next_ = 1;
        }

        $code_ = 'AP-' . date('ym') . str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }


    public function edit($id_alamat)
    {
        $prov = DB::table('provinces')->select('name')->get();
        $provi['looks'] = $prov;
        $look = DB::table('wsperusahaan')->select('id_perusahaan', 'kode_perusahaan', 'nama_perusahaan')->get();
        $datap['looks'] = $look;
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();

        return view('ws.wsalamat.edit', ['wsalamat' => $wsalamat, 'provi'=>$provi, 'datap' => $datap]);
    }
    public function delete($id_alamat)
    {
        $data = ListPerusahaan::drop();

        return redirect('list_perusahaan');
    }
    public function hapus($id_alamat)
    {
        $del = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_a');
    }
    // public function deleteAll($id_alamat){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }
        return Redirect::to('/list_wsalamat');
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsalamatperusahaan")->whereIn('id_alamatperusahaan', explode(",", $ids))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }
    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wsalamat = DB::table('wsalamatperusahaan')
    //     ->where('nama_perusahaan','like',"%".$cari."%")
    //     ->orWhere('kode_perusahaan','like',"%".$cari."%")
    //     ->orWhere('singkatan','like',"%".$cari."%")    
    //     ->orWhere('visi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('misi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
    //     ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
    //     ->orWhere('lokasi_pajak','like',"%".$cari."%") 
    //     ->orWhere('npp','like',"%".$cari."%") 
    //     ->orWhere('npkp','like',"%".$cari."%") 
    //     ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
    //     ->orWhere('keterangan','like',"%".$cari."%") 
    //     ->orWhere('status_rekaman','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%") 
    //     ->orWhere('waktu_masuk','like',"%".$cari."%") 
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%") 
    //     ->orWhere('waktu_ubah','like',"%".$cari."%") 
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%") 
    //     ->orWhere('waktu_hapus','like',"%".$cari."%") 
    //     ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
    //     ->paginate();
    //     return view('ws.wsalamatperusahaan',['wsalamatperusahaan' =>$wsalamat]);
    // }

}
