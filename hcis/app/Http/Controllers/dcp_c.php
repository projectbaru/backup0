<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class dcp_c extends Controller
{
    # Update 12-10-2022
    public function index(Request $request)
    {
        $data = $this->get_tampilan(1);
        $ptk_detail = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        $lk = DB::table('wslokasikerja')->select('id_lokasi_kerja', 'nama_lokasi_kerja', 'kode_lokasi_kerja')->get();
        $datalk['looks']=$lk;
        $labels = [
            'Jabatan Dilamar',
            'Nama Lengkap',
            'Pengalaman Kerja',
            'Pendidikan tertinggi',
            'Status',
            'Kode CV Pelamar',
            'No. HP',
            'Lokasi Saat Ini',
            'Lokasi Kerja',
            'Tanggal Lahir',
            'Nama Sekolah',
            'Program Studi/Jurusan',
            'Tanggal Wisuda',
            'Nilai Rata-Rata',
            'Skala',
            'Informasi Lowongan',
            'Nama Karyawan/lainnya',
            'Pesan Pelamar',
            'Resume/CV',
            'Tanggal Melamar',
            'Email'
        ];

        $pelamar = DB::table('rc_data_cv_pelamar')->orderBy('nama_lengkap');
        $fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->select('jabatan_kepegawaian')->get();
        $fptk_jab['looks']=$fptk;

      

        $list_tingkat = DB::table('rc_data_lowongan_kerja')->pluck('tingkat_pekerjaan')->toArray();
        $list_lokasi = DB::table('rc_data_lowongan_kerja')->pluck('lokasi_kerja')->toArray();

        
        $fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();

       
        $loker = DB::table('rc_data_lowongan_kerja')->select('jabatan_lowongan','lokasi_kerja','tingkat_pekerjaan')->get();
        $loker_['looks']=$loker;
       
        $dlk = DB::table('rc_data_lowongan_kerja')->get();
        $list_tingkat = DB::table('rc_data_lowongan_kerja')->pluck('tingkat_pekerjaan')->toArray();
        $list_lokasi = DB::table('rc_data_lowongan_kerja')->pluck('lokasi_kerja')->toArray();

        if(!empty($request->no_dokumen_fptk)) {
            $pelamar->where('kode_cv_pelamar', 'like', "%{$request->no_dokumen_fptk}%");
        }
        // if(!empty($request->jabatan_lowongan)) {
        //     $pelamar->where('jabatan_lowongan', 'like', "%{$request->jabatan_lowongan}%");
        // }
        if(!empty($request->minimal_pendidikan)) {
            $arr = [];
            $educationLevels = array('SD', 'SMP', 'SMA', 'SMK', 'D1', 'D2', 'D3', 'S1', 'S2', 'S3');
            $inputIndex = array_search($request->minimal_pendidikan, $educationLevels);
            if ($inputIndex !== false) {
                $arr = array_slice($educationLevels, $inputIndex);
            }
            foreach ($arr as $a) {
                $pelamar->orWhere('pendidikan_tertinggi', 'like', "%{$a}%");
            }
        }
        // if(!empty($request->lokasi_kerja)) {
        //     $pelamar->where('lokasi_kerja', 'like', "%{$request->lokasi_kerja}%");
        // }


        $code_ = $this->get_number();
        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;
      
        $pelamar = $pelamar->get();
        $pelamar_jmlh = DB::table('rc_data_cv_pelamar')->count();
        

        if ($data) {
            $data->filter = json_decode($data->filter);
            return view('re.dcp.index', compact([
                'fptk',
                'dlk',
                'list_tingkat',
                'list_lokasi',
                'labels', 'data', 'datalk','ptk_detail', 'pelamar', 'request']), ['loker_'=>$loker_, 'pelamar_jmlh'=>$pelamar_jmlh, 'randomId' => $code_, 'datalk'=>$datalk]);
//            return view('re.dcp.index', $data);
        } else {
//            $wsalamat=DB::table('wsalamatperusahaan')->get();
            return view('re.dcp.index', compact([
                'fptk',
                'dlk',
                'list_tingkat',
                'list_lokasi',
                'labels', 'datalk', 'ptk_detail', 'pelamar', 'request']), ['loker_'=>$loker_,'pelamar_jmlh'=>$pelamar_jmlh,'randomId' => $code_, 'datalk'=>$datalk]);
        }
    }

    public function filterList(Request $request)
    {
        // DB::beginTransaction();

        if(empty($request->cv)) {
            return DB::table('reftable_cv')->where('user_id', Auth::user()->id)->delete();
        }

        $update = DB::table('reftable_cv')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active' => false,
            ]);

        $insert = DB::table('reftable_cv')->insert([
            'kode_cv_pelamar'   => $request->no_dokumen_fptk,
            'jabatan_lowongan'    => $request->jabatan_lowongan,
            'minimal_pendidikan'    => $request->minimal_pendidikan,
            'lokasi_kerja'      => $request->lokasi_kerja,
//            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
//            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
//            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'filter'            => json_encode($request->cv),
            'user_id'           => Auth::user()->id,
        ]);

        

        // if ($update && $insert) {
            // DB::commit();
            return response()->json(['message' => 'Filter updated successfully', 'success' => TRUE], 200);
        // }
        // DB::rollBack();
        // return response()->json(['message' => 'Filter failed to update'], 500);
    }

    public function get_tampilan($active)
    {
        return DB::table('reftable_cv')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode CV Pelamar',
                'value' => 'kode_cv_pelamar'
            ],
            [
                'text'  => 'Jabatan Lowongan',
                'value' => 'jabatan_lowongan'
            ],
            [
                'text'  => 'Tingkat Pekerjaan',
                'value' => 'tingkat_pekerjaan'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Resume CV',
                'value' => 'resume_cv'
            ],
            [
                'text'  => 'Nama Lengkap',
                'value' => 'nama_lengkap'
            ],
            [
                'text'  => 'Email',
                'value' => 'email'
            ],
            [
                'text'  => 'No HP',
                'value' => 'no_hp'
            ],
            [
                'text'  => 'Lokasi Pelamar',
                'value' => 'lokasi_pelamar'
            ],
            [
                'text'  => 'Pengalaman Kerja',
                'value' => 'pengalaman_kerja'
            ],
            [
                'text'  => 'Tahun Pengalaman Kerja',
                'value' => 'tahun_pengalaman_kerja'
            ],
            [
                'text'  => 'Bulan Pengalaman Kerja',
                'value' => 'bulan_pengalaman_kerja'
            ],
            [
                'text'  => 'Tanggal Lahir',
                'value' => 'tanggal_lahir'
            ],
            [
                'text'  => 'Jenis Kelamin',
                'value' => 'jenis_kelamin'
            ],
            [
                'text'  => 'Pendidikan Tertinggi',
                'value' => 'pendidikan_tertinggi'
            ],
            [
                'text'  => 'Nama Sekolah',
                'value' => 'nama_sekolah'
            ],
            [
                'text'  => 'Program Studi Jurusan',
                'value' => 'program_studi_jurusan'
            ],
            [
                'text'  => 'Tanggal Wisuda',
                'value' => 'tanggal_wisuda'
            ],
            [
                'text'  => 'Nilai Rata-Rata',
                'value' => 'nilai_rata_rata'
            ],
            [
                'text'  => 'Nilai Skala',
                'value' => 'nilai_skala'
            ],
            [
                'text'  => 'Informasi Lowongan',
                'value' => 'informasi_lowongan'
            ],
            [
                'text'  => 'Keterangan Informasi Pilihan',
                'value' => 'keterangan_informasi_pilihan'
            ],
            [
                'text'  => 'Pesan Pelamar',
                'value' => 'pesan_pelamar'
            ],
            [
                'text'  => 'Tanggal Melamar',
                'value' => 'tanggal_melamar'
            ],
            [
                'text'  => 'Status Lamaran',
                'value' => 'status_lamaran'
            ],
            [
                'text'  => 'Pengguna Masuk',
                'value' => 'pengguna_masuk'
            ],
            [
                'text'  => 'Waktu Masuk',
                'value' => 'waktu_masuk'
            ],
            [
                'text'  => 'Pengguna Ubah',
                'value' => 'pengguna_ubah'
            ],
            [
                'text'  => 'Waktu Ubah',
                'value' => 'waktu_ubah'
            ],
            [
                'text'  => 'Pengguna Hapus',
                'value' => 'pengguna_hapus'
            ],
            [
                'text'  => 'Waktu Hapus',
                'value' => 'waktu_hapus'
            ],
            [
                'text'  => 'Status Lamaran',
                'value' => 'status_lamaran'
            ],
            [
                'text'  => 'Jenis Kelamin',
                'value' => 'jenis_kelamin'
            ],
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else {
            foreach ($test as $k => $v) {
                $test[$k]['style']="display:'block';";
            }
        }



        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('wsalamat.filter', $data);
    }


    public function tambah()
    {
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number();
        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;

        return view('re.dcp.tambah', ['randomId' => $code_, 'datalk'=>$datalk]);
    }

    public function tambah_u()
    {
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number();
        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;

        return view('re.dcp.tambah_user_cv', ['randomId' => $code_, 'datalk'=>$datalk]);
    }
    
    public function simpan(Request $request)
    {
        $logo_perusahaan = optional($request->file('resume_cv'));
        $nama_file = $logo_perusahaan->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $logo_perusahaan->move($tujuan_upload, $nama_file);
        DB::table('rc_data_cv_pelamar')->insert([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'jabatan_lowongan'   =>$request->jabatan_lowongan,
            'tingkat_pekerjaan'   =>$request->tingkat_pekerjaan,
            'lokasi_kerja' => json_encode($request->lokasi_kerja),
            'resume_cv'   =>$nama_file,
            'nama_lengkap'  =>$request->nama_lengkap,
            'email'  =>$request->email,
            'no_hp'=>$request->no_hp,
            'lokasi_pelamar'=>$request->lokasi_pelamar,
            'pengalaman_kerja'=>$request->pengalaman_kerja,
            'tahun_pengalaman_kerja'=>$request->tahun_pengalaman_kerja,
            'bulan_pengalaman_kerja'=>$request->bulan_pengalaman_kerja,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'nama_sekolah'=>$request->nama_sekolah,
            'program_studi_jurusan'=>$request->program_studi_jurusan,
            'pendidikan_tertinggi'=>$request->pendidikan_tertinggi,
            'tanggal_wisuda'=>$request->tanggal_wisuda,
            'nilai_rata_rata'=>$request->nilai_rata_rata,
            'nilai_skala'=>$request->nilai_skala,
            'informasi_lowongan'=>$request->informasi_lowongan,
            'status_lamaran'=>$request->status_lamaran,
            'keterangan_informasi_pilihan'=>$request->keterangan_informasi_pilihan,
            'pesan_pelamar'=>$request->pesan_pelamar,
            'tanggal_melamar'=>date('d/m/Y'),
            
        ]);

        DB::table('reftable_cv')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_cv_pelamar'   => null,
                'jabatan_lowongan'   => null,
                'query_field'       => null,
                'query_operator'    => null,
                'query_value'       => null,
            ]);
        return redirect('/list_dcp');
    }

    public function edit($id_cv_pelamar)
    {
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number();
        // $datanum['randomId'] = $randomId;
        $data = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $data;
        //DB::enableQueryLog();
        $rc_cv = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->get();
        //$query = DB::getQueryLog();
        //dd($rc_cv);
        return view('re.dcp.edit', ['randomId' => $code_,'rc_cv'=>$rc_cv, 'datalk'=>$datalk]);
    }

    public function detail($id_cv_pelamar)
    {
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number();
        // $datanum['randomId'] = $randomId;
        $rc_cv = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->get();
        return view('re.dcp.detail', ['randomId' => $code_, 'rc_cv'=>$rc_cv]);
    }


    public function hapus(Request $request)
    {
        $del =  DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $request->id_cv_pelamar)->delete();
        if ($del) {
            // return $this->succesWitmessage("Berhasil hapus");
            // return redirect()->back();
            return response()->json([
                'status' => true,
                "data" => null,
                'msg' => "success"
            ], 200);
        }
        return $this->errorWithmessage("Gagal");
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("rc_ptk_detail")->where('id_ptk', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function get_number()
    {
        $max_id = DB::table('rc_data_cv_pelamar')
        ->where('id_cv_pelamar', \DB::raw("(select max(`id_cv_pelamar`) from rc_data_cv_pelamar)"))
        ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->kode_cv_pelamar) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->kode_cv_pelamar, -3) + 1;
            }
        } else {
            $next_ = 1;
        }

        $code_ = date('ym').str_pad($next_, 3, "0", STR_PAD_LEFT).'-PLM';
        return $code_;
    }
    public function reset()
    {
        $hasPersonalTable = DB::table('rc_ptk_detail')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('reftable_cv')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_ptk');
    }

    public function update(Request $request)
    {
        DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $request->id_transaksi_ptk)->update([
            'nomor_permintaan'   =>$request->nomor_permintaan,
            'tipe_transaksi'   =>$request->tipe_transaksi,
            'nomor_referensi'   =>$request->nomor_referensi,
            'tanggal_permintaan'   =>$request->tanggal_permintaan,
            'tanggal_efektif_transaksi'   =>$request->tanggal_efektif_transaksi,
            'periode_tujuan'  =>$request->periode_tujuan,
            'periode_asal'  =>$request->periode_asal,
            'kode_ptk_asal'=>$request->kode_ptk_asal,
            'nama_ptk_asal'=>$request->nama_ptk_asal,
            'total_ptk_asal'=>$request->total_ptk_asal,
            'kode_ptk_tujuan'=>$request->kode_ptk_tujuan,
            'nama_ptk_tujuan'=>$request->nama_ptk_tujuan,
            'total_ptk_tujuan'=>$request->total_ptk_tujuan,
            'nama_posisi_tujuan'=>$request->nama_posisi_tujuan,
            'nama_lokasi_kerja_tujuan'=>$request->nama_lokasi_kerja_tujuan,
            'nama_posisi_asal'=>$request->nama_posisi_asal,
            'nama_lokasi_kerja_asal'=>$request->nama_lokasi_kerja_asal,
            'deskripsi'=>$request->deskripsi,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'waktu_user_input'=>$request->waktu_user_input,
            'status_lamaran'=>$request->status_lamaran,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'waktu_user_input'=>$request->waktu_user_input,

        ]);

        return redirect('/list_data');
    }

    public function updateDCP(Request $request)
    {
        // if($request->resume_cv === 0){
            
        // }else{

        // }


        // $logo_perusahaan = optional($request->file('resume_cv'));
        // $nama_file = time() . "." . $logo_perusahaan->getClientOriginalName();
        // $nama_file = $logo_perusahaan->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $logo_perusahaan->move($tujuan_upload, $nama_file);
        
        $data_update = array(

            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'jabatan_lowongan'   =>$request->jabatan_lowongan,
            'tingkat_pekerjaan'   =>$request->tingkat_pekerjaan,
            'lokasi_kerja' => json_encode($request->lokasi_kerja),
            
            'nama_lengkap'  =>$request->nama_lengkap,
            'email'  =>$request->email,
            'no_hp'=>$request->no_hp,
            'lokasi_pelamar'=>$request->lokasi_pelamar,
            'pengalaman_kerja'=>$request->pengalaman_kerja,
            'tahun_pengalaman_kerja'=>$request->tahun_pengalaman_kerja,
            'bulan_pengalaman_kerja'=>$request->bulan_pengalaman_kerja,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'nama_sekolah'=>$request->nama_sekolah,
            'program_studi_jurusan'=>$request->program_studi_jurusan,
            'pendidikan_tertinggi'=>$request->pendidikan_tertinggi,
            'tanggal_wisuda'=>$request->tanggal_wisuda,
            'nilai_rata_rata'=>$request->nilai_rata_rata,
            'nilai_skala'=>$request->nilai_skala,
            'informasi_lowongan'=>$request->informasi_lowongan,
            'keterangan_informasi_pilihan'=>$request->keterangan_informasi_pilihan,
            'pesan_pelamar'=>$request->pesan_pelamar,
            'tanggal_melamar'=>$request->tanggal_melamar,
            'status_lamaran'=>$request->status_lamaran,
        );
        // return redirect('/list_dcp/edit/'.$request->temp_id);
        if ($request->file('resume_cv')) {
            $perusahaan_logo = optional($request->file('resume_cv'));
            $nama_file = $perusahaan_logo->getClientOriginalName();
            $tujuan_upload = 'data_file';
            $perusahaan_logo->move($tujuan_upload, $nama_file);

            $data_update['resume_cv'] = $nama_file;
        }
        DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $request->temp_id)->update($data_update);

        return redirect('/list_dcp');
        // return Redirect::route('list_dcp');
    }

    public function editData($id_cv_pelamar)
    {
        // $data = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->first();

        // return response()->json([
        //     'status' => true,
        //     "data" => $data,
        //     'msg' => "success"
        // ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);

        $code_ = $this->get_number();
        $dlk = DB::table('rc_data_lowongan_kerja')->get();
        $datalk['looks'] = DB::table('rc_data_lowongan_kerja')->select('lokasi_kerja')->get();
        $rc_cv = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->get();
        $list_tingkat = DB::table('rc_data_lowongan_kerja')->pluck('tingkat_pekerjaan')->toArray();
        $list_lokasi = DB::table('rc_data_lowongan_kerja')->pluck('lokasi_kerja')->toArray();
        $returnHTML = view('re.dcp.edit_html')
            ->with('randomId', $code_)
            ->with('list_tingkat', $list_tingkat)
            ->with('list_lokasi', $list_lokasi)
            ->with('rc_cv', $rc_cv)
            ->with('datalk', $datalk)
            ->with('dlk', $dlk)
            ->render();

        return response()->json([
            'status' => true,
            "data" => $returnHTML,
            'msg' => "success"
        ], 200);
    }

    public function detailData($id_cv_pelamar)
    {
        // $data = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->first();

        // return response()->json([
        //     'status' => true,
        //     "data" => $data,
        //     'msg' => "success"
        // ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);

         // $data = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->first();

        // return response()->json([
        //     'status' => true,
        //     "data" => $data,
        //     'msg' => "success"
        // ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);

        $code_ = $this->get_number();
        $datalk['looks'] = DB::table('rc_data_lowongan_kerja')->select('lokasi_kerja')->get();
        $rc_cv = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->get();
        $list_tingkat = DB::table('rc_data_lowongan_kerja')->pluck('tingkat_pekerjaan')->toArray();
        $list_lokasi = DB::table('rc_data_lowongan_kerja')->pluck('lokasi_kerja')->toArray();
        $returnHTML = view('re.dcp.detail_html')
            ->with('randomId', $code_)
            ->with('list_tingkat', $list_tingkat)
            ->with('list_lokasi', $list_lokasi)
            ->with('rc_cv', $rc_cv)
            ->with('datalk', $datalk)
            ->render();

        return response()->json([
            'status' => true,
            "data" => $returnHTML,
            'msg' => "success"
        ], 200);
    }
}
