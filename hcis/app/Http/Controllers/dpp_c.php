<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class dpp_c extends Controller
{

    public function index(Request $request)
    {
        // y
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_data_pelamar', $select)) {
                array_push($select, 'id_data_pelamar');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('rc_form_data_pelamar')->select($select);

            if ($hasPersonalTable->nama_lengkap) {
                $query->where('nama_lengkap', $hasPersonalTable->nama_lengkap);
            }
            if ($hasPersonalTable->kode_data_pelamar) {
                $query->where('kode_data_pelamar', $hasPersonalTable->kode_data_pelamar);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            // print_r($data);
            return view('re.dpp.filterResult', $data);
        } else {
            
            // $data = DB::table('rc_formulir_permintaan_tenaga_kerja')->where('no_dokumen ', '=', $request->no_dokumen)->where('pendidikan', '=', $request->pendidikan)->where('jabatan_kepegawaian', '=', $request->jabatan_kepegawaian)->where('lokasi_kerja', '=', $request->lokasi_kerja)->get();
            // foreach ($data as $d) {
            //     DB::table('rc_data_cv_pelamar')->insert([
            //         'kode_cv_pelamar' => $d->kode_cv_pelamar,
            //         'jabatan_lowongan' => $d->jabatan_lowongan,
            //         'tingkat_pekerjaan' => $d->tingkat_pekerjaan,
            //         'lokasi_kerja' => $d->lokasi_kerja ,
            //     ]);
            // }
            $lk = DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen','pendidikan','jabatan_kepegawaian','lokasi_kerja')->get();
            $ptk_detail['looks']=$lk;
            $rc_cv = DB::table('rc_form_data_pelamar')->get();
            return view('re.dpp.index', ['rc_cv' => $rc_cv,'ptk_detail'=>$ptk_detail]);
        }
    }

    public function reset()
    {
        $hasPersonalTable = DB::table('rc_form_data_pelamar')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('reftable_dpp')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_dpp');
    }

    //data return
    public function return()
    {
        $lk = DB::table('wslokasikerja')->select('id_lokasi_kerja','nama_lokasi_kerja','kode_lokasi_kerja')->get();
        $datalk['looks']=$lk;
        return view('re.dpp.tambah',['datalk'=>$datalk]);

    }

    public function detail($id_data_pelamar){
        $dpp = DB::table('rc_form_data_pelamar')->get();
        $dpp_k = DB::table('rc_form_data_pelamar')
        ->join('rc_susunan_keluarga_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_susunan_keluarga_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_susunan_keluarga_pelamar.*')
        ->get();
        $dpp_k = DB::table('rc_form_data_pelamar')
        ->join('rc_susunan_keluarga_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_susunan_keluarga_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_susunan_keluarga_pelamar.*')
        ->get();
        $dpp_p = DB::table('rc_form_data_pelamar')
        ->join('rc_form_latar_belakang_pendidikan','rc_form_data_pelamar.id_data_pelamar', '=','rc_form_latar_belakang_pendidikan.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_form_latar_belakang_pendidikan.*')
        ->get();
        $dpp_ku = DB::table('rc_form_data_pelamar')
        ->join('rc_pelatihan_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_pelatihan_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_pelatihan_pelamar.*')
        ->get();
        $dpp_bah = DB::table('rc_form_data_pelamar')
        ->join('rc_pelatihan_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_pelatihan_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_pelatihan_pelamar.*')
        ->get();
        $dpp_o = DB::table('rc_form_data_pelamar')
        ->join('rc_pelatihan_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_pelatihan_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_pelatihan_pelamar.*')
        ->get();

        $dpp_lb = DB::table('rc_form_data_pelamar')
        ->join('rc_form_latar_belakang_pendidikan','rc_form_data_pelamar.id_data_pelamar', '=','rc_form_latar_belakang_pendidikan.id_data_pelamar')
        ->select('rc_form_latar_belakang_pendidikan.*','rc_form_data_pelamar.*')
        ->get();

        $dpp_kr = DB::table('rc_form_data_pelamar')
        ->join('rc_kontak_refrensi_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_kontak_refrensi_pelamar.id_data_pelamar')
        ->select('rc_kontak_refrensi_pelamar.*','rc_form_data_pelamar.*')
        ->get();

        $dpp_kd = DB::table('rc_form_data_pelamar')
        ->join('rc_kontak_darurat','rc_form_data_pelamar.id_data_pelamar', '=','rc_kontak_darurat.id_data_pelamar')
        ->select('rc_kontak_darurat.*','rc_form_data_pelamar.*')
        ->get();
        $dpp = DB::table('rc_form_data_pelamar')->where('id_data_pelamar', $id_data_pelamar)->get();

        $code_=$this->get_number();
        return view('re.dpp.detail',['dpp_kr'=>$dpp_kr,'dpp_kd'=>$dpp_kd, 'dpp'=>$dpp, 'dpp_o'=>$dpp_o, 'dpp_lb'=>$dpp_lb, 'dpp'=> $dpp,'dpp_bah'=> $dpp_bah,'dpp_ku'=> $dpp_ku,'dpp_k'=> $dpp_k,'dpp_p'=> $dpp_p,'randomKode'=>$code_]);
    }

    public function get_number(){
        $max_id = DB::table('rc_form_data_pelamar')
        ->where('id_data_pelamar', \DB::raw("(select max(`id_data_pelamar`) from rc_form_data_pelamar)"))
        ->first();
        $next_ = 0;
        if(strlen($max_id->kode_data_pelamar) < 9){
            $next_ = 1;
        }else{
            $next_ = (int)substr($max_id->kode_data_pelamar, -3) + 1;
        }
        $code_ = 'DPP'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    
    public function tambah()
    {
        // $randomId = random_int(2, 50);
        // $datanum['randomId'] = $randomId;
        $dcp = DB::table('rc_data_cv_pelamar')->get();
        $lk = DB::table('wslokasikerja')->select('id_lokasi_kerja','nama_lokasi_kerja','kode_lokasi_kerja')->get();
        $datalk['looks']=$lk;
        $dpp = DB::table('rc_form_data_pelamar')->select('id_data_pelamar')->get();
        $datadpp['looks']=$dpp;
        $lbp = DB::table('rc_latar_belakang_pendidikan')->select('id_pendidikan_pelamar', 'tingkat_pendidikan', 'nama_sekolah', 'jurusan', 'tahun_masuk', 'tahun_keluar', 'jabatan', 'status _kelulusan')->get();
        $lbp['looks']=$lbp;
        $krp = DB::table('rc_kontak_refrensi_pelamar')->select('id_kontak_refrensi_pelamar', 'nama_lengkap', 'alamat_telpon', 'pekerjaan', 'hubungan' )->get();
        $krp['looks']=$krp;
        $kp = DB::table('rc_keluarga_pelamar')->select('hubungan_keluarga', 'nama_keluarga', 'jenis_kelamin', 'usia_tahun_lahir', 'pendidikan_terakhir' )->get();
        $kp['looks']=$kp;
        $jp = DB::table('rc_jawaban_pilihan')->select('jawaban', 'penjelasan')->get();
        $jp['looks']=$jp;
        $je = DB::table('rc_jawaban_esai')->select('jawaban')->get();
        $je['looks']=$je;
        $jmm = DB::table('rc_jawaban_marston_model')->select('nama_lengkap','jawaban')->get();
        $jmm['looks']=$jmm;
        $kd = DB::table('rc_kontak_darurat')->select('nama_lengkap','alamat_telpon','pekerjaan','hubungan')->get();
        $kd['looks']=$kd;
        
        return view('re.dpp.tambah',['datalk'=>$datalk, 'dcp'=>$dcp,'datadpp'=>$datadpp,'lbp'=>$lbp, 'krp'=>$krp, 'kp'=>$kp,'jp'=>$jp ]);
    }
    
    public function tambah_user()
    {
        // $randomId = random_int(2, 50);
        // $datanum['randomId'] = $randomId;
        $dcp = DB::table('rc_data_cv_pelamar')->get();
        $lk = DB::table('wslokasikerja')->select('id_lokasi_kerja','nama_lokasi_kerja','kode_lokasi_kerja')->get();
        $datalk['looks']=$lk;
        $dpp = DB::table('rc_form_data_pelamar')->select('id_data_pelamar')->get();
        $datadpp['looks']=$dpp;
        $lbp = DB::table('rc_latar_belakang_pendidikan')->select('id_pendidikan_pelamar', 'tingkat_pendidikan', 'nama_sekolah', 'jurusan', 'tahun_masuk', 'tahun_keluar', 'jabatan', 'status _kelulusan')->get();
        $lbp['looks']=$lbp;
        $krp = DB::table('rc_kontak_refrensi_pelamar')->select('id_kontak_refrensi_pelamar', 'nama_lengkap', 'alamat_telpon', 'pekerjaan', 'hubungan' )->get();
        $krp['looks']=$krp;
        $kp = DB::table('rc_keluarga_pelamar')->select('hubungan_keluarga', 'nama_keluarga', 'jenis_kelamin', 'usia_tahun_lahir', 'pendidikan_terakhir' )->get();
        $kp['looks']=$kp;
        $jp = DB::table('rc_jawaban_pilihan')->select('jawaban', 'penjelasan')->get();
        $jp['looks']=$jp;
        $je = DB::table('rc_jawaban_esai')->select('jawaban')->get();
        $je['looks']=$je;
        $jmm = DB::table('rc_jawaban_marston_model')->select('nama_lengkap','jawaban')->get();
        $jmm['looks']=$jmm;
        $kd = DB::table('rc_kontak_darurat')->select('nama_lengkap','alamat_telpon','pekerjaan','hubungan')->get();
        $kd['looks']=$kd;
        
        return view('re.dpp.tambah_user_cv',['datalk'=>$datalk, 'dcp'=>$dcp, 'datadpp'=>$datadpp, 'lbp'=>$lbp, 'kd'=>$lbp]);
    }
    
    function get_tampilan($active)
    {
        return DB::table('reftable_dpp')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode CV Pelamar',
                'value' => 'kode_cv_pelamar'
            ],
            [
                'text'  => 'Kode Data Pelamar',
                'value' => 'kode_data_pelamar'
            ],
            [
                'text'  => 'Posisi Lamaran',
                'value' => 'posisi_lamaran'
            ],
            [
                'text'  => 'Nama Lengkap',
                'value' => 'nama_lengkap'
            ],
            [
                'text'  => 'Nama Panggilan',
                'value' => 'nama_panggilan'
            ],
            [
                'text'  => 'Tempat Lahir',
                'value' => 'tempat_lahir'
            ],
            [
                'text'  => 'Tanggal Lahir',
                'value' => 'tanggal_lahir'
            ],
            [
                'text'  => 'Agama',
                'value' => 'agama'
            ],
            [
                'text'  => 'Kebangsaan',
                'value' => 'kebangsaan'
            ],
            [
                'text'  => 'Jenis Kelamin',
                'value' => 'jenis_kelamin'
            ],
            [
                'text'  => 'Berat Badan',
                'value' => 'berat_badan'
            ],
            [
                'text'  => 'Tinggi badan',
                'value' => 'tinggi_badan'
            ],
            [
                'text'  => 'No KTP',
                'value' => 'no_ktp'
            ],
            [
                'text'  => 'Email Pribadi',
                'value' => 'email_pribadi'
            ],
            [
                'text'  => 'Alamat Sosmed',
                'value' => 'alamat_sosmed'
            ],
            [
                'text'  => 'Kategori SIM',
                'value' => 'kategori_sim'
            ],
            [
                'text'  => 'No.SIM',
                'value' => 'no_sim'
            ],
            [
                'text'  => 'No.HP',
                'value' => 'no_hp'
            ],
            [
                'text'  => 'No.Telepon',
                'value' => 'no_telepon'
            ],
            [
                'text'  => 'Status Kendaraan',
                'value' => 'status_kendaraan'
            ],
            [
                'text'  => 'Jenis Merk Kendaraan',
                'value' => 'jenis_merk_kendaraan'
            ],
            [
                'text'  => 'Tempat Tinggal',
                'value' => 'tempat_tinggal'
            ],
            [
                'text'  => 'Status Perkawinan',
                'value' => 'status_perkawinan'
            ],
            [
                'text'  => 'Alamat Di dalam Kota',
                'value' => 'alamat_didalam_kota'
            ],
            [
                'text'  => 'Kode Pos 1',
                'value' => 'kode_pos_1'
            ],
            [
                'text'  => 'Alamat Diluar Kota',
                'value' => 'alamat_diluar_kota'
            ],
            [
                'text'  => 'Kode Pos 1',
                'value' => 'kode_pos_1'
            ],
            [
                'text'  => 'Alamat Diluar Kota',
                'value' => 'alamat_diluar_kota'
            ],
            [
                'text'  => 'Kode Pos 2',
                'value' => 'kode_pos_2'
            ],
            [
                'text'  => 'Pas Foto',
                'value' => 'pas_foto'
            ],
            [
                'text'  => 'Hoby dan Kegiatan',
                'value' => 'hoby_dan_kegiatan'
            ],
            [
                'text'  => 'Kegiatan Membaca',
                'value' => 'kegiatan_membaca'
            ],
            [
                'text'  => 'Surat Kabar',
                'value' => 'surat_kabar'
            ],
            [
                'text'  => 'Majalah',
                'value' => 'majalah'
            ],
            [
                'text'  => 'Pokok Yang Dibaca',
                'value' => 'pokok_yang_dibaca'
            ],
            [
                'text'  => 'Tugas Jabatan Terakhir',
                'value' => 'tugas_jabatan_terakhir'
            ],
            [
                'text'  => 'Tanggal Lamar',
                'value' => 'tanggal_lamar'
            ],
            [
                'text'  => 'Gambar Denah',
                'value' => 'gambar_denah'
            ],
            [
                'text'  => 'Tempat TTD',
                'value' => 'tempat_ttd'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Waktu User Input',
                'value' => 'waktu_user_input'
            ]
            
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('re.dpp.filter', $data);
    }


    public function simpan(Request $request){
        if(($request->status_form) ==''){
            $status_form='new';
        }
        DB::table('rc_form_data_pelamar')->insert([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'kode_data_pelamar'   =>$request->kode_data_pelamar,
            'posisi_lamaran'   =>$request->posisi_lamaran,
            'pendidikan_terakhir' =>$request->pendidikan_terakhir,
            'nama_lengkap'   =>$request->nama_lengkap,
            'nama_panggilan'   =>$request->nama_panggilan,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tanggal_lahir'  =>$request->tanggal_lahir,
            'agama'=>$request->agama,
            'kebangsaan'=>$request->kebangsaan,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'berat_badan'=>$request->berat_badan,
            'tinggi_badan'=>$request->tinggi_badan,
            'no_ktp'=>$request->no_ktp,
            'email_pribadi'=>$request->email_pribadi,
            'alamat_sosmed'=>$request->alamat_sosmed,
            'kategori_sim'=>$request->kategori_sim,
            'no_sim'=>$request->no_sim,
            'no_hp'=>$request->no_hp,
            'no_telepon'=>$request->no_telepon,
            'status_kendaraan'=>$request->status_kendaraan,
            'jenis_merk_kendaraan'=>$request->jenis_merk_kendaraan,
            'tempat_tinggal'=>$request->tempat_tinggal,
            'status_perkawinan'=>$request->status_perkawinan,
            'alamat_didalam_kota'=>$request->alamat_didalam_kota,
            'kode_pos_1'=>$request->kode_pos_1,
            'alamat_diluar_kota'=>$request->alamat_diluar_kota,
            'kode_pos_2'=>$request->kode_pos_2,
            'pas_foto'=>$request->pas_foto,
            'hoby_dan_kegiatan'=>$request->hoby_dan_kegiatan,
            'kegiatan_membaca'=>$request->kegiatan_membaca,
            'surat_kabar'=>$request->surat_kabar,
            'pokok_yang_dibaca'=>$request->pokok_yang_dibaca,
            'tugas_jabatan_terakhir'=>$request->tugas_jabatan_terakhir,
            'tanggal_lamar'=>$request->tanggal_lamar,
            'gambar_denah'=>$request->gambar_denah,
            'tempat_ttd'=>$request->tempat_ttd,
            'tanda_tangan'=>$request->tanda_tangan,
            'status_rekaman'=>$request->status_rekaman,
            'waktu_user_input'=>$request->waktu_user_input,
        ]);

        return redirect('/list_an');
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_deskripsi_pekerjaan';
        } else {
            $select = ['id_data_pelamar','kode_data_pelamar', 'posisi_lamaran', 'nama_lengkap', 'no_ktp', 'no_hp', 'email_pribadi'];
        }

        $query = DB::table('rc_form_data_pelamar')->select($select);

        if ($request->nama_lengkap) {
            $query->where('nama_lengkap', $request->nama_lengkap);
        }
        if ($request->kode_data_pelamar) {
            $query->where('kode_data_pelamar', $request->kode_data_pelamar);
        }
        

        $hasPersonalTable = DB::table('reftable_dpp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('reftable_dpp')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('reftable_dpp')->insert([
            'kode_data_pelamar' => $request->kode_data_pelamar,
            'nama_lengkap'      => $request->nama_lengkap,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_dpp');
    }

    
    public function edit($id_data_pelamar) {
        $skp = DB::table('rc_form_data_pelamar')->join('rc_susunan_keluarga_pelamar','rc_form_data_pelamar.id_data_pelamar', '=','rc_susunan_keluarga_pelamar.id_data_pelamar')
        ->select('rc_form_data_pelamar.*','rc_susunan_keluarga_pelamar.*')
        ->get();

        $dcp_l = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar')->get();
        $data_dcp['looks'] = $dcp_l;
        $dpp = DB::table('rc_form_data_pelamar')->select('id_data_pelamar')->get();
        $datadpp['looks']=$dpp;
        $lbp = DB::table('rc_latar_belakang_pendidikan')->select('id_pendidikan_pelamar', 'tingkat_pendidikan', 'nama_sekolah', 'jurusan', 'tahun_masuk', 'tahun_keluar', 'jabatan', 'status _kelulusan')->get();
        $lbp['looks']=$lbp;
        $krp = DB::table('rc_kontak_refrensi_pelamar')->select('id_kontak_refrensi_pelamar', 'nama_lengkap', 'alamat_telpon', 'pekerjaan', 'hubungan' )->get();
        $krp['looks']=$krp;
        $kp = DB::table('rc_keluarga_pelamar')->select('hubungan_keluarga', 'nama_keluarga', 'jenis_kelamin', 'usia_tahun_lahir', 'pendidikan_terakhir' )->get();
        $kp['looks']=$kp;
        $jp = DB::table('rc_jawaban_pilihan')->select('jawaban', 'penjelasan')->get();
        $jp['looks']=$jp;
        $je = DB::table('rc_jawaban_esai')->select('jawaban')->get();
        $je['looks']=$je;
        $jmm = DB::table('rc_jawaban_marston_model')->select('nama_lengkap','jawaban')->get();
        $jmm['looks']=$jmm;
        $kd = DB::table('rc_kontak_darurat')->select('nama_lengkap','alamat_telpon','pekerjaan','hubungan')->get();
        $kd['looks']=$kd;

        $dpp = DB::table('rc_form_data_pelamar')->where('id_data_pelamar', $id_data_pelamar)->get();
        return view('re.dpp.edit', ['datalk'=>$datalk, 'dcp'=>$dcp, 'datadpp'=>$datadpp, 'lbp'=>$lbp, 'kd'=>$kd]);
    }

    public function update(Request $request) {
        DB::table('rc_form_data_pelamar')->where('id_data_pelamar', $request->id_data_pelamar)->update([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'kode_data_pelamar'   =>$request->kode_data_pelamar,
            'posisi_lamaran'   =>$request->posisi_lamaran,
            'pendidikan_terakhir' =>$request->pendidikan_terakhir,
            'nama_lengkap'   =>$request->nama_lengkap,
            'nama_panggilan'   =>$request->nama_panggilan,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tanggal_lahir'  =>$request->tanggal_lahir,
            'agama'=>$request->agama,
            'kebangsaan'=>$request->kebangsaan,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'berat_badan'=>$request->berat_badan,
            'tinggi_badan'=>$request->tinggi_badan,
            'no_ktp'=>$request->no_ktp,
            'email_pribadi'=>$request->email_pribadi,
            'alamat_sosmed'=>$request->alamat_sosmed,
            'kategori_sim'=>$request->kategori_sim,
            'no_sim'=>$request->no_sim,
            'no_hp'=>$request->no_hp,
            'no_telepon'=>$request->no_telepon,
            'status_kendaraan'=>$request->status_kendaraan,
            'jenis_merk_kendaraan'=>$request->jenis_merk_kendaraan,
            'tempat_tinggal'=>$request->tempat_tinggal,
            'status_perkawinan'=>$request->status_perkawinan,
            'alamat_didalam_kota'=>$request->alamat_didalam_kota,
            'kode_pos_1'=>$request->kode_pos_1,
            'alamat_diluar_kota'=>$request->alamat_diluar_kota,
            'kode_pos_2'=>$request->kode_pos_2,
            'pas_foto'=>$request->pas_foto,
            'hoby_dan_kegiatan'=>$request->hoby_dan_kegiatan,
            'kegiatan_membaca'=>$request->kegiatan_membaca,
            'surat_kabar'=>$request->surat_kabar,
            'pokok_yang_dibaca'=>$request->pokok_yang_dibaca,
            'tugas_jabatan_terakhir'=>$request->tugas_jabatan_terakhir,
            'tanggal_lamar'=>$request->tanggal_lamar,
            'gambar_denah'=>$request->gambar_denah,
            'tempat_ttd'=>$request->tempat_ttd,
            'tanda_tangan'=>$request->tanda_tangan,
            'status_rekaman'=>$request->status_rekaman,
            'waktu_user_input'=>$request->waktu_user_input,
            
        ]);

        return redirect('/list_dpp');
    }

    public function update_api(Request $request) {
        DB::table('rc_form_data_pelamar')->where('id_data_pelamar', $request->id_data_pelamar)->update([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'kode_data_pelamar'   =>$request->kode_data_pelamar,
            'posisi_lamaran'   =>$request->posisi_lamaran,
            'pendidikan_terakhir' =>$request->pendidikan_terakhir,
            'nama_lengkap'   =>$request->nama_lengkap,
            'nama_panggilan'   =>$request->nama_panggilan,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tanggal_lahir'  =>$request->tanggal_lahir,
            'agama'=>$request->agama,
            'kebangsaan'=>$request->kebangsaan,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'berat_badan'=>$request->berat_badan,
            'tinggi_badan'=>$request->tinggi_badan,
            'no_ktp'=>$request->no_ktp,
            'email_pribadi'=>$request->email_pribadi,
            'alamat_sosmed'=>$request->alamat_sosmed,
            'kategori_sim'=>$request->kategori_sim,
            'no_sim'=>$request->no_sim,
            'no_hp'=>$request->no_hp,
            'no_telepon'=>$request->no_telepon,
            'status_kendaraan'=>$request->status_kendaraan,
            'jenis_merk_kendaraan'=>$request->jenis_merk_kendaraan,
            'tempat_tinggal'=>$request->tempat_tinggal,
            'status_perkawinan'=>$request->status_perkawinan,
            'alamat_didalam_kota'=>$request->alamat_didalam_kota,
            'kode_pos_1'=>$request->kode_pos_1,
            'alamat_diluar_kota'=>$request->alamat_diluar_kota,
            'kode_pos_2'=>$request->kode_pos_2,
            'pas_foto'=>$request->pas_foto,
            'hoby_dan_kegiatan'=>$request->hoby_dan_kegiatan,
            'kegiatan_membaca'=>$request->kegiatan_membaca,
            'surat_kabar'=>$request->surat_kabar,
            'pokok_yang_dibaca'=>$request->pokok_yang_dibaca,
            'tugas_jabatan_terakhir'=>$request->tugas_jabatan_terakhir,
            'tanggal_lamar'=>$request->tanggal_lamar,
            'gambar_denah'=>$request->gambar_denah,
            'tempat_ttd'=>$request->tempat_ttd,
            'tanda_tangan'=>$request->tanda_tangan,
            'status_rekaman'=>$request->status_rekaman,
            'waktu_user_input'=>$request->waktu_user_input,
            
        ]);

        return redirect('/list_dpp');
    }

    public function simpan_api(Request $request){
        $form_data= DB::table('rc_form_data_pelamar')->insert([
            'status'   =>$request->status,
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'kode_data_pelamar'   =>$request->kode_data_pelamar,
            'posisi_lamaran'   =>$request->posisi_lamaran,
            'pendidikan_terakhir' =>$request->pendidikan_terakhir,
            'nama_lengkap'   =>$request->nama_lengkap,
            'nama_panggilan'   =>$request->nama_panggilan,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tanggal_lahir'  =>$request->tanggal_lahir,
            'agama'=>$request->agama,
            'kebangsaan'=>$request->kebangsaan,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'berat_badan'=>$request->berat_badan,
            'tinggi_badan'=>$request->tinggi_badan,
            'no_ktp'=>$request->no_ktp,
            'email_pribadi'=>$request->email_pribadi,
            'alamat_sosmed'=>$request->alamat_sosmed,
            'kategori_sim'=>$request->kategori_sim,
            'no_sim'=>$request->no_sim,
            'no_hp'=>$request->no_hp,
            'no_telepon'=>$request->no_telepon,
            'status_kendaraan'=>$request->status_kendaraan,
            'jenis_merk_kendaraan'=>$request->jenis_merk_kendaraan,
            'tempat_tinggal'=>$request->tempat_tinggal,
            'status_perkawinan'=>$request->status_perkawinan,
            'alamat_didalam_kota'=>$request->alamat_didalam_kota,
            'kode_pos_1'=>$request->kode_pos_1,
            'alamat_diluar_kota'=>$request->alamat_diluar_kota,
            'kode_pos_2'=>$request->kode_pos_2,
            'pas_foto'=>$request->pas_foto,
            'hoby_dan_kegiatan'=>$request->hoby_dan_kegiatan,
            'kegiatan_membaca'=>$request->kegiatan_membaca,
            'surat_kabar'=>$request->surat_kabar,
            'majalah'=>$request->majalah,
            'pokok_yang_dibaca'=>$request->pokok_yang_dibaca,
            'tugas_jabatan_terakhir'=>$request->tugas_jabatan_terakhir,
            'tanggal_lamar'=>$request->tanggal_lamar,
            'gambar_denah'=>$request->gambar_denah,
            'tempat_ttd'=>$request->tempat_ttd,
            'tanda_tangan'=>$request->tanda_tangan,
            'status_rekaman'=>$request->status_rekaman,
            
        ]);

        $form_data = \DB::table('rc_form_data_pelamar')
        ->where('kode_data_pelamar', $request->kode_data_pelamar)
        ->orderBy('id_data_pelamar', 'DESC')
        ->first();
        
        DB::table('rc_jawaban_esai')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'jawaban'       => $request->jawaban
        ]);
        
        DB::table('rc_jawaban_pilihan')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'jawaban'       => $request->jawaban,
            'penjelasan'       => $request->penjelasan
        ]);
        DB::table('rc_kontak_darurat')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_kontak_darurat'       => $request->kode_kontak_darurat,
            'nama_lengkap'       => $request->nama_lengkap,
            'alamat_telpon'       => $request->alamat_telpon,
            'pekerjaan'       => $request->pekerjaan,
            'hubungan'       => $request->hubungan,
        ]);
        DB::table('rc_jawaban_marston_model')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'nama_lengkap'       => $request->nama_lengkap,
            'jawaban'       => $request->jawaban
        ]);
        DB::table('rc_negatif_positif_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kategori'       => $request->kategori,
            'keterangan'       => $request->keterangan            
        ]);
        DB::table('rc_organisasi_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_organisasi_pelamar'       => $request->kode_organisasi_pelamar,
            'nama_organisasi'       => $request->nama_organisasi,
            'jenis_kegiatan'       => $request->jenis_kegiatan,            
            'jabatan'       => $request->jabatan,
            'tahun'       => $request->tahun            
        ]);
        DB::table('rc_pelatihan_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_pelatihan_pelamar'       => $request->kode_pelatihan_pelamar,
            'bidang_pelatihan'       => $request->bidang_pelatihan,
            'penyelenggara'       => $request->penyelenggara,            
            'tempat_kota'       => $request->tempat_kota,
            'lama_pelatihan'       => $request->lama_pelatihan,
            'tahun_pelatihan'       => $request->tahun_pelatihan, 
            'dibiayai_oleh' =>$request->dibiayai_oleh

        ]); 

        DB::table('rc_pertanyaan_marston_model')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_pelatihan_pelamar'       => $request->kode_pelatihan_pelamar,
            'marston_model'       => $request->marston_model,
            'pertanyaan'       => $request->pertanyaan

        ]);

        DB::table('rc_pertanyaan_pilihan_esai')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_pelatihan_pelamar'       => $request->kode_pelatihan_pelamar,
            'pertanyaan'       => $request->pertanyaan,
            'kategori_pertanyaan'       => $request->kategori_pertanyaan
        ]);

        DB::table('rc_susunan_keluarga_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'hubungan_keluarga'       => $request->hubungan_keluarga,
            'nama_keluarga'       => $request->nama_keluarga,
            'jenis_kelamin'       => $request->jenis_kelamin,
            'usia_tahun_lahir'       => $request->usia_tahun_lahir,
            'pendidikan_terakhir'       => $request->pendidikan_terakhir,
            'jabatan'       => $request->jabatan,
            'perusahaan'          => $request->perusahaan,
            'keterangan_susunan_keluarga' =>$request->keterangan_susunan_keluarga
        ]);
         
        
        DB::table('rc_latar_belakang_pendidikan')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'tingkat_pendidikan' => $request->tingkat_pendidikan,
            'nama_sekolah'       => $request->nama_sekolah,
            'jurusan'       => $request->jurusan,
            'tahun_masuk'       => $request->tahun_masuk,
            'tahun_keluar'       => $request->tahun_keluar,
            'jabatan'       => $request->jabatan,
            'status_kelulusan'       => $request->status_kelulusan,
            
        ]);
        
        DB::table('rc_jawaban_marston_model')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'tingkat_pendidikan' => $request->tingkat_pendidikan,
            'nama_sekolah'       => $request->nama_sekolah,
            'jurusan'       => $request->jurusan,
            'tahun_masuk'       => $request->tahun_masuk,
            'tahun_keluar'       => $request->tahun_keluar,
            'jabatan'       => $request->jabatan,
            'status_kelulusan'  => $request->status_kelulusan,
            
        ]);

        DB::table('rc_jawaban_pilihan')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'jawaban' => $request->jawaban,
            'penjelasan'       => $request->penjelasan,
        ]);
        DB::table('rc_jawaban_esai')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'id_pertanyaan_pilihan_esai' => $request->id_pertanyaan_pilihan_esai,
            'jawaban'       => $request->jawaban,
        ]);
        
        DB::table('rc_bahasa_asing')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'id_bahasa_asing' => $request->id_bahasa_asing,
            'bahasa'       => $request->bahasa,
            'mendengar'       => $request->mendengar,
            'berbicara'       => $request->berbicara,
            'membaca'       => $request->membaca,
            'menulis'       => $request->menulis,
        ]);

        DB::table('rc_negatif_positif_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kategori' => $request->kategori,
            'keterangan'       => $request->keterangan,
        ]);

        DB::table('rc_pelatihan_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'bidang_pelatihan' => $request->bidang_pelatihan,
            'penyelenggara'       => $request->penyelenggara,
            'tempat_kota'       => $request->tempat_kota,
            'lama_pelatihan'       => $request->lama_pelatihan,
            'tahun_pelatihan'       => $request->tahun_pelatihan,
            'dibiayai_oleh'       => $request->dibiayai_oleh,
        ]);
        DB::table('rc_organisasi_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_organisasi_pelamar' => $request->kode_organisasi_pelamar,
            'nama_organisasi'       => $request->nama_organisasi,
            'jenis_kegiatan'       => $request->jenis_kegiatan,
            'jabatan'       => $request->jabatan,
            'tahun'       => $request->tahun,
        ]);
        DB::table('rc_pertanyaan_marston_model')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_organisasi_pelamar' => $request->kode_organisasi_pelamar,
            'nama_organisasi'       => $request->nama_organisasi,
            'jenis_kegiatan'       => $request->jenis_kegiatan,
            'jabatan'       => $request->jabatan,
            'tahun'       => $request->tahun,
        ]);
        
        DB::table('rc_pertanyaan_pilihan_esai')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'pertanyaan' => $request->pertanyaan,
            'kategori_pertanyaan'       => $request->kategori_pertanyaan,            
        ]);

        DB::table('rc_riwayat_kerja_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_riwayat_kerja_pelamar'=>$request->kode_riwayat_kerja_pelamar,
            'nama_perusahaan' => $request->nama_perusahaan,
            'alamat'       => $request->alamat,
            'nama_atasan'       => $request->nama_atasan, 
            'lama_bekerja'       => $request->lama_bekerja,
            'gaji_terakhir'       => $request->gaji_terakhir,
            'jenis_usaha'       => $request->jenis_usaha,    
            'alasan_berhenti'       => $request->alasan_berhenti,
            'jabatan_awal'       => $request->jabatan_awal,            
            'jabatan_akhir'       => $request->jabatan_akhir,
            'telp_perusahaan'       => $request->telp_perusahaan,
            'nama_direktur'       => $request->nama_direktur,            
        ]);
        DB::table('rc_susunan_keluarga_pelamar')->insert([
            'id_data_pelamar'   =>$form_data->id_data_pelamar,
            'kode_susunan_keluarga_pelamar'=>$request->kode_susunan_keluarga_pelamar,
            'hubungan_keluarga' => $request->hubungan_keluarga,
            'nama_keluarga'       => $request->nama_keluarga,
            'jenis_kelamin'       => $request->jenis_kelamin, 
            'usia_tahun_lahir'       => $request->usia_tahun_lahir,
            'pendidikan_terakhir'       => $request->pendidikan_terakhir,
            'jabatan'       => $request->jabatan,    
            'perusahaan'       => $request->perusahaan,
            'keterangan_susunan_keluarga'       => $request->keterangan_susunan_keluarga,            
            'jabatan_akhir'       => $request->jabatan_akhir,
            'telp_perusahaan'       => $request->telp_perusahaan,
            'nama_direktur'       => $request->nama_direktur,            
        ]);

         if ($form_data) {
//            $insertedStepProcess = DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $insertDataId)->first();
            if (count($request->tingkat_pendidikan) > 0) {
                for ($i = 0; $i < count($request->tingkat_pendidikan); $i++) {
                    // $checkNoUrut = DB::table('rc_detail_step_process_recruitment')
                    //     ->where('no_urut', $request->no_urut[$i])
                    //     ->where('kode_step_process_recruitment', $form_data)
                    //     ->first();

                    // if ($checkNoUrut) {
                    //     DB::rollBack();
                    //     return redirect()->route('tambah_jpr', ['error' => 'No Urut sudah ada!']);
                    // }

                    $insertDetail= DB::table('rc_latar_belakang_pendidikan')->insert([
                        'id_data_pelamar'   =>$form_data->id_data_pelamar,
                        'tingkat_pendidikan'       => $request->tingkat_pendidikan[$i],
                        'nama_sekolah'       => $request->nama_sekolah[$i],
                        'tahun_masuk'       => $request->tahun_keluar[$i],
                        'tahun_keluar'       => $request->tahun_keluar[$i]
                        
                    ]);
                }
            }

            // kursus/pelatihan
            if (count($request->bidang_pelatihan) > 0) {
                for ($i = 0; $i < count($request->bidang_pelatihan); $i++) {
                    $insertDetail_pelatihan= DB::table('rc_pelatihan_pelamar')->insert([
                        'id_data_pelamar'   =>$form_data->id_data_pelamar,
                        
                        'bidang_pelatihan'       => $request->bidang_pelatihan[$i],
                        'penyelenggara'       => $request->penyelenggara[$i],
                        'tempat_kota'       => $request->tempat_kota[$i],
                        'tahun_pelatihan'       => $request->tahun_pelatihan[$i],
                        'dibiayai_oleh'       => $request->dibiayai_oleh[$i],
                        'lama_pelatihan'       => $request->lama_pelatihan[$i]
                        
                    ]);
                }
            }
            // bahasa
            if (count($request->bahasa) > 0) {
                for ($i = 0; $i < count($request->bahasa); $i++) {
                    $insertDetail_bahasa= DB::table('rc_bahasa_asing')->insert([
                        'id_data_pelamar'   =>$form_data->id_data_pelamar,
                        'bahasa'       => $request->bahasa[$i],
                        'mendengar'       => $request->mendengar[$i],
                        'berbicara'       => $request->berbicara[$i],
                        'membaca'       => $request->membaca[$i],
                        'menulis'       => $request->menulis[$i]
                        
                    ]);
                }
            }
            if (count($request->nama_organisasi) > 0) {
                for ($i = 0; $i < count($request->nama_organisasi); $i++) {
                    $insertDetail_organisasi= DB::table('rc_organisasi_pelamar')->insert([
                        'id_data_pelamar'   =>$form_data->id_data_pelamar,
                        'nama_organisasi'   => $request->nama_organisasi[$i],
                        'jenis_kegiatan'   => $request->jenis_kegiatan[$i],
                        'jabatan'       => $request->jabatan[$i],
                        'tahun'       => $request->tahun[$i]
                        
                    ]);
                }
            }
        }
        if ($form_data && $insertDetail && $insertDetail_pelatihan && $insertDetail_bahasa && $insertDetail_organisasi){
            DB::commit();
            return redirect('/list_dpp');
        }else {
            DB::rollBack();
            return redirect()->route('tambah_jpr');
        }


    }

}