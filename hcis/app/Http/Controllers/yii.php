<?php




public function actionSendapproval($id)
    {
        $model = $this->findModel($id);
        $atonum = rand(0,99999999);
        
        $con = \Yii::$app->db; 
        $auth_id    = Yii::$app->user->identity->id;
        $query = "SELECT ph.no_approval FROM ba_header ph
                    WHERE ph.no_approval = '$atonum'
                    LIMIT 1;";			
        $cari = $con->createCommand($query)->queryOne();
        
        if($cari){
            $atonum = rand(0,99999999);
        }
        
        $model->no_approval = $atonum;
        $model->save(false);
        var_dump($atonum);
        
            //send approval
            $today=date('Y-m-d H:i:s');
            //id modul
            $module_record_id = $atonum; 
            $doctype_id = 1000066; 
            $title = "BERITA ACARA";
            $documentno = $model->id_ba;
            $documentdate = $today;
            $description =  strip_tags($model->judul_ba,"") ;
            
            // dibuat oleh
            $loginer=Yii::$app->user->identity->id;
            $nik = Masterkaryawan::find()->where(['Kode_Karyawan'=>$loginer])->one()->nik;
            $ad_user_id_z = Yii::$app->fungsi2->find_ad_user_id($nik);
            
            $created_external_by = $ad_user_id_z;
            $createdby = $ad_user_id_z; 
            $ad_user_id = $ad_user_id_z; 
            $header = 1000040;
            $jabatan = 'USER INPUT';
            
            //sign
            $list_user_approval = [];
            $sign = BaSign::find()
            ->where(['ba_id'=>$id])
            ->all();
            //var_dump($sign);
            if($sign){
                foreach($sign as $rr){
                    $nik = Masterkaryawan::find()->where(['Kode_Karyawan'=>$rr['user_id']])->one()->nik;
                    $kode_ad_user_id = Yii::$app->fungsi2->find_ad_user_id($nik);
                    $header = "1000040";
                    if($rr['header']=="Dibuat Oleh"){
                        $header = "1000040";
                    }else if($rr['header']=="Diperiksa Oleh"){
                        $header = "1000042";
                    }else if($rr['header']=="Disetujui Oleh"){
                        $header = "1000045";
                    }else if($rr['header']=="Diketahui Oleh"){
                        $header = "1000044";
                    } 
                    
                    $sign1 = array(
                            "ad_user_id" => $kode_ad_user_id,
                            "header" => $header,
                            "jabatan" => $rr['jabatan'],
                    );
                    array_push($list_user_approval, $sign1);
                }
            }
            //var_dump($list_user_approval);
            //sign
            
            $penutup = "Demikian berita acara ini kami sampaikan, atas kerjasa dan perhatian kami ucapkan banyak terima kasih";
            $isi = $model->isi_ba . '<br>' . $penutup;
            $scontent = array(
                //judul
                "data" => 
                [
                    "document_type" => strip_tags($this->getDepartemen($model->department),""),
                    "documentno" => strip_tags($model->judul_ba,""),
                    "record_id" => $model->id_ba
                ],
                //Tab
                "segment" =>
                [
                //Tab 1
                [
                    "title" => "detail [:] INFORMASI [:] list",
                    "content" =>
                    //isi
                    [
                        "Approval ID [:] $atonum [:] full_html",
                        "Judul [:] $model->judul_ba [:] full_html",
                        "Isi [:] $isi [:] full_html",
                    ]
                ],
                
                
                ]
            );	
            $content = json_encode($scontent);
            $body =  array("content" => $content,
                        "doctype_id" => $doctype_id,
                        "module_record_id" => $module_record_id,
                        "createdby" => $createdby,
                        "created_external_by" => $created_external_by,
                        "title" => $title,
                        "documentno" => $documentno,
                        "documentdate" => $documentdate,
                        "description" => $description,
                        "ad_user_id" => $ad_user_id,
                        "header" => $header,
                        "jabatan" => $jabatan,
                        "list_user_approval" => $list_user_approval
                        );	
                        
            $post_data = Yii::$app->fungsi2->callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/create', json_encode($body));
            $response = json_decode($post_data, true);
            var_dump($response); 
            
            //ubah status
            $model = $this->findModel($id);
            $model->status = "WAITING APPROVAL";
            $model->save(false);
            
            Yii::$app->session->setFlash('tsuccess', "Send Approval Berhasil");
            return $this->redirect(['index']);
            
    }




public function find_ad_user_id($id){
    $body =  array("search" => $nik);	
    $post_data = Yii::$app->fungsi2->callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/ad_user_search', json_encode($body));
    $resp = json_decode($post_data, true); 
    $content = $resp['content'];
    if($content=="exist"){
        $ad_user_id = $resp['response'][0]['ad_user_id'];
    }else{
        $ad_user_id = "";
    }
    
    return($ad_user_id);
}


function callAPI2($method, $url, $data){
    $curl = curl_init();
    switch ($method){
    case "POST":
        curl_setopt($curl, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "PUT":
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
        break;
    default:
        if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
        }	
    // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoIjoiZGV2X21vYmlsZV90YW1fc2UiLCJpYXQiOjE1NzYxMzIxNzJ9.d-s_0FfLfr62LQoMLg6HQ8hgEP_VZJrLLWBAra34Mfg',
        'Content-Type: application/json',
    
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;   
} 



    protected function findModel($id)
    {
        if (($model = BaHeader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    function getDepartemen($kode){
		//convert departemen
		$departemen="";$s=1;
		if(isset($kode)){
			$str_arr = explode (",", $kode);  
			foreach($str_arr as $r){
				$adepartemen="";
				$con = \Yii::$app->db;
				$query_departemen = "SELECT kode,keterangan FROM struktur_content where kode='$r' limit 1";			
				$model_departemen = $con->createCommand($query_departemen)->queryOne();
				if($model_departemen){
					$adepartemen = $model_departemen['keterangan'];
				}else{
					$adepartemen = "";
				}
				
				if($s==1){
					$departemen = $adepartemen;
				}else{
					$departemen = $departemen . "<br>" . $adepartemen ;	
				}
				$s++;
			}
		}
		//end convert departemen	
		return $departemen;
	}
