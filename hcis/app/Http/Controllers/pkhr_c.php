<?php

namespace App\Http\Controllers;

// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Rc_pencapaian;
use App\Models\Rc_Kpi_Pencapaian_Bulanan;
use App\Models\Rc_Kpi;

class pkhr_c extends Controller
{
    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("rc_pencapaian_kpi_hr_recruitment")->where('id_pencapaian_kpi_hr_recruitment', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_pencapaian_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Nama Parameter',
                'value' => 'nama_parameter'
            ],
            [
                'text'  => 'Tahun Pencapaian',
                'value' => 'tahun_pencapaian'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Nama User Input',
                'value' => 'nama_user_input'
            ],
            [
                'text'  => 'Tanggal Input',
                'value' => 'tanggal_input'
            ],
            [
                'text'  => 'User Ubah',
                'value' => 'user_ubah'
            ],
            [
                'text'  => 'Waktu Ubah',
                'value' => 'waktu_ubah'
            ],
            [
                'text'  => 'User Hapus',
                'value' => 'user_hapus'
            ],
            [
                'text'  => 'Waktu Hapus',
                'value' => 'waktu_hapus'
            ]
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else {
            foreach ($test as $k => $v) {
                $test[$k]['style']="display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('re.pkhr.filter', $data);
    }

    public function get_number()
    {
        $max_id = DB::table('rc_pencapaian_kpi_hr_recruitment')
        ->where('id_pencapaian_kpi_hr_recruitment', \DB::raw("(select max(`id_pencapaian_kpi_hr_recruitment`) from rc_pencapaian_kpi_hr_recruitment)"))
        ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->kode_perusahaan) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->kode_perusahaan, -3) + 1;
            }
        } else {
            $next_ = 1;
        }


        $code_ = 'PKHR-'.date('ym').str_pad($next_, 3, "0", STR_PAD_LEFT);
        return $code_;
    }

    public function edit($id_pencapaian_kpi_hr_recruitment)
    {
        //GET HEADER
        $pkhr = array();
        $header                   = DB::table('rc_pencapaian_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->first();

        $pkhr['header_id']        = $id_pencapaian_kpi_hr_recruitment;
        $pkhr['nama_parameter']   = $header->nama_parameter;
        $pkhr['tahun_pencapaian'] = $header->tahun_pencapaian;
        //GET CHILD 1
        $child1 = DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->get();
        //loop CHILD
        foreach ($child1 as $key => $val) {
            $r       = array();
            $r['c1'][$key] = $val;
            $child2  = DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->where('id_kpi_hr_recruitment', $val->id_kpi_hr_recruitment)->get();
            foreach ($child2 as $key2 => $val2) {
                $c2 = array();
                $r['c2'][$key2] = $val2;
            }

            $pkhr['child'][] = $r;
        }

        dd($pkhr);

        return view('re.pkhr.edit', ['pkhr'=>json_decode(json_encode($pkhr), true)]);
    }

    public function edit_pkhr($id_pencapaian_kpi_hr_recruitment)
    {
        //GET HEADER
        $pkhr = array();
        $header                   = DB::table('rc_pencapaian_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->first();

        $pkhr['header_id']        = $id_pencapaian_kpi_hr_recruitment;
        $pkhr['nama_parameter']   = $header->nama_parameter;
        $pkhr['tahun_pencapaian'] = $header->tahun_pencapaian;
        //GET CHILD 1
        $child1 = DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->get();
        //loop CHILD
        foreach ($child1 as $key => $val) {
            $r       = array();
            $r['c1'] = $val;
            $child2  = DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->where('id_kpi_hr_recruitment', $val->id_kpi_hr_recruitment)->get();
            foreach ($child2 as $key2 => $val2) {
                $c2 = array();
                $r['c2'][$key2] = $val2;
            }

            $pkhr['child'][] = $r;
        }

        // dd($pkhr);

        return view('re.pkhr.edit_pkhr', ['pkhr'=>json_decode(json_encode($pkhr), true)]);
    }

    public function detail($id_pencapaian_kpi_hr_recruitment)
    {
        $pkhr = array();
        $header                   = DB::table('rc_pencapaian_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->first();
        $pkhr['header_id']        = $id_pencapaian_kpi_hr_recruitment;
        $pkhr['nama_parameter']   = $header->nama_parameter;
        $pkhr['tahun_pencapaian'] = $header->tahun_pencapaian;
        //GET CHILD 1
        $child1 = DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->get();
        //loop CHILD
        foreach ($child1 as $key => $val) {
            $r       = array();
            $r['c1'] = $val;
            $child2  = DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->where('id_kpi_hr_recruitment', $val->id_kpi_hr_recruitment)->get();
            foreach ($child2 as $key2 => $val2) {
                $c2 = array();
                $r['c2'][$key2] = $val2;
            }

            $pkhr['child'][] = $r;
        }

        // return $pkhr;

        return view('re.pkhr.detail', ['pkhr'=>json_decode(json_encode($pkhr), true)]);
    }

    public function index(Request $request)
    {
        $users = DB::table('rc_kpi_hr_recruitment')->get();
        $t_w=$users->sum->weight;

       
        // $ach = DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->select('ach')->where('bulan', '=', 'januari')->get();
        // $users = DB::table('rc_kpi_hr_recruitment')->get();
        // $sum=DB::table('rc_kpi_hr_recruitment')->select('weight')->sum();
        
        $tahun = $request->tahun_pencapaian;
        if($tahun){
            $tahun = $tahun;
        }else{
            $tahun = 1;
        }

        $jan = [];
        $februari = [];
        $maret = [];
        $april = [];
        $mei = [];
        $juni = [];
        $juli = [];
        $agustus = [];
        $september = [];
        $oktober = [];
        $november = [];
        $desember = [];

        $pencapaian = Rc_pencapaian::with('more_details')->where('tahun_pencapaian', $tahun)->get();
        $more_details = [];
        foreach($pencapaian as $p) {    
            $more_details = array_merge($more_details, $p->more_details->toArray());
        }

        if ($pencapaian) {
            $jan = collect($more_details)->where('bulan', '=', 'Januari')->values()->all();
            $februari = collect($more_details)->where('bulan', '=', 'Februari')->values()->all();
            $maret = collect($more_details)->where('bulan', '=', 'Maret')->values()->all();
            $april = collect($more_details)->where('bulan', '=', 'April')->values()->all();
            $mei = collect($more_details)->where('bulan', '=', 'Mei')->values()->all();
            $juni = collect($more_details)->where('bulan', '=', 'Juni')->values()->all();
            $juli = collect($more_details)->where('bulan', '=', 'Juli')->values()->all();
            $agustus = collect($more_details)->where('bulan', '=', 'Agustus')->values()->all();
            $september = collect($more_details)->where('bulan', '=', 'September')->values()->all();
            $oktober = collect($more_details)->where('bulan', '=', 'Oktober')->values()->all();
            $november = collect($more_details)->where('bulan', '=', 'November')->values()->all();
            $desember = collect($more_details)->where('bulan', '=', 'Desember')->values()->all();    
        }

        $pkhr = array();
        $header = DB::table('rc_pencapaian_kpi_hr_recruitment')->get();
        foreach ($header as $key => $val) {
            $r = array();
            $r['header'][$key] = $val;
            $child = DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment',$val->id_pencapaian_kpi_hr_recruitment)->get();
            foreach ($child as $key2 => $val2) {
                $r['child'][$key2] = $val2;
            }
            $pkhr[] = $r;
        }
        // return json_decode(json_encode($pkhr), true);die;
        // $pkhr_p=DB::table('rc_pencapaian_kpi_hr_recruitment')->get();
        $year_options = DB::table('rc_pencapaian_kpi_hr_recruitment')
            ->select('tahun_pencapaian')
            ->groupBy('tahun_pencapaian')
            ->get()->pluck('tahun_pencapaian');

        return view('re.pkhr.index', [
            'pkhr'=>json_decode(json_encode($pkhr), true), 
            'year_options'=>$year_options, 
            't_w'=>$t_w, 
            'jan'=>$jan, 
            'februari'=>$februari, 
            'maret'=>$maret, 
            'april'=>$april, 
            'mei'=>$mei, 
            'juni'=>$juni, 
            'juli'=>$juli, 
            'agustus'=>$agustus, 
            'september'=>$september, 
            'oktober'=>$oktober, 
            'november'=>$november, 
            'desember'=>$desember, 
            'tahun' => $tahun 
        ]);
    }

    public function get_tampilan($active)
    {
        return DB::table('reftable_pkhr')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function simpan(Request $request)
    {
        DB::beginTransaction();
        try {
            $idheader = DB::table('rc_pencapaian_kpi_hr_recruitment')->insertGetId([
                        'nama_parameter'            => $request->nama_parameter,
                        'tahun_pencapaian'          => $request->tahun_pencapaian

            ]);
            //insert detail
            for ($i=1;$i<=$request->jmldetail;$i++) {
                $a = 'nama_kpi'.$i;
                $b = 'polarization'.$i;
                $c = 'uom'.$i;
                $d = 'weight'.$i;
                $e = 'definition'.$i.'_1';
                $f = 'definition'.$i.'_2';
                $g = 'output'.$i;
                $z = 'target'.$i;

                // checkbox
                $ca = 'manager'.$i;
                $cb = 'pic'.$i;
                $cc = 'staff'.$i;

                $manager=(!empty($request->$ca) && !is_null($request->$ca)) ? 'active' : '';
                $pic=(!empty($request->$cb) && !is_null($request->$cb)) ? 'active' : '';
                $staff=(!empty($request->$cc) && !is_null($request->$cc)) ? 'active' : '';

                if (!empty($request->$a) && !is_null($request->$a)) {
                    $iddetail = DB::table('rc_kpi_hr_recruitment')->insertGetId([
                                            'id_pencapaian_kpi_hr_recruitment' => $idheader,
                                            'nama_kpi'     => $request->$a,
                                            'polarization' => $request->$b,
                                            'uom'          => $request->$c,
                                            'weight'       => $request->$d,
                                            'target'       => $request->$z,
                                            'definition_1' => $request->$e,
                                            'definition_2' => $request->$f,
                                            'output'       => $request->$g,
                                            'manager'      => $manager,
                                            'pic'          => $pic,
                                            'staff'        => $staff,
                    ]);
                    //nested detail
                    $h = 'bulan'.$i;
                    $j = 'target_perbulan'.$i;
                    $k = 'weightd'.$i;
                    $l = 'act'.$i;
                    $m = 'ach'.$i;

                    for ($x=0;$x<count($request->$h);$x++) {
                        $bulan = $request->$h[$x];
                        $target_perbulan = $request->$j[$x];
                        $weightd = $request->$k[$x];
                        $act = $request->$l[$x];
                        $ach = $request->$m[$x];
                        DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->insert([
                          'id_kpi_hr_recruitment' => $iddetail,
                          'bulan' => $bulan,
                          'target_perbulan' => $target_perbulan,
                          'weight' => $weightd,
                          'act' => $act,
                          'ach'=> $ach,
                        ]);
                    }
                }
            }

            DB::commit();
            return redirect('/list_pkhr');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('tambah_pkhr');
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $idheader = $request->header_id;
            DB::table('rc_pencapaian_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment',$idheader)->update([
                        'nama_parameter'            => $request->nama_parameter,
                        'tahun_pencapaian'          => $request->tahun_pencapaian

            ]);
            //delete child1 and child2
            $getIdChild1 = DB::table('rc_kpi_hr_recruitment')
                                ->where('id_pencapaian_kpi_hr_recruitment',$idheader)
                                ->select('id_kpi_hr_recruitment')->get();
            if(count($getIdChild1) > 0) {
                foreach($getIdChild1 as $r) {
                    DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->where('id_kpi_hr_recruitment',$r->id_kpi_hr_recruitment)->delete();
                }
            }
            DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment',$idheader)->delete();

            //end delete

            //insert detail
            DB::enableQueryLog();
            for ($i=1;$i<=$request->jmldetail;$i++) {
                $a = 'nama_kpi'.$i;
                $b = 'polarization'.$i;
                $c = 'uom'.$i;
                $d = 'weight'.$i;
                $e = 'definition'.$i.'_1';
                $f = 'definition'.$i.'_2';
                $g = 'output'.$i;
                $z = 'target'.$i;

                // checkbox
                $ca = 'manager'.$i;
                $cb = 'pic'.$i;
                $cc = 'staff'.$i;

                $manager=(!empty($request->$ca) && !is_null($request->$ca)) ? 'active' : '';
                $pic=(!empty($request->$cb) && !is_null($request->$cb)) ? 'active' : '';
                $staff=(!empty($request->$cc) && !is_null($request->$cc)) ? 'active' : '';

                if (!empty($request->$a) && !is_null($request->$a) && isset($_POST[$a])) {
                    $iddetail = DB::table('rc_kpi_hr_recruitment')->insertGetId([
                                            'id_pencapaian_kpi_hr_recruitment' => $idheader,
                                            'nama_kpi'     => $request->$a,
                                            'polarization' => $request->$b,
                                            'uom'          => $request->$c,
                                            'weight'       => $request->$d,
                                            'target'       => $request->$z,
                                            'definition_1' => $request->$e,
                                            'definition_2' => $request->$f,
                                            'output'       => $request->$g,
                                            'manager'      => $manager,
                                            'pic'          => $pic,
                                            'staff'        => $staff,
                    ]);
                    //nested detail
                    $h = 'bulan'.$i;
                    $j = 'target_perbulan'.$i;
                    $k = 'weightd'.$i;
                    $l = 'act'.$i;
                    $m = 'ach'.$i;
                    for ($x=0;$x<count($request->$h);$x++) {
                        $bulan = $request->$h[$x];
                        $target_perbulan = $request->$j[$x];
                        $weightd = $request->$k[$x];
                        $act = $request->$l[$x];
                        $ach = $request->$m[$x];
                        DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->insert([
                          'id_kpi_hr_recruitment' => $iddetail,
                          'bulan' => $bulan,
                          'target_perbulan' => $target_perbulan,
                          'weight' => $weightd,
                          'act' => $act,
                          'ach'=> $ach,
                        ]);
                    }
                }
            }
            DB::commit();
            return redirect('/list_pkhr');
        } catch (\Throwable $th) {
            DB::rollback();
            echo "<pre>"; var_dump($th->getMessage()); echo "</pre>";die;
            return redirect()->route('tambah_pkhr');
        }
    }

    public function update_pkhr(Request $request)
    {
        DB::beginTransaction();
        try {
            $rc_kpi_hr_recruitment = DB::table('rc_kpi_hr_recruitment')
                ->where('id_pencapaian_kpi_hr_recruitment', $request->header_id)
                ->select('id_kpi_hr_recruitment');

            foreach($rc_kpi_hr_recruitment->get() as $rc_kpi_hr_recruitments){
                $rc_kpi_hr_recruitment_pencapaian_bulanan = DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')
                ->where('id_kpi_hr_recruitment', $rc_kpi_hr_recruitments->id_kpi_hr_recruitment)
                ->delete();
            }

            $rc_kpi_hr_recruitment->delete();

            $rc_pencapaian_kpi_hr_recruitment = DB::table('rc_pencapaian_kpi_hr_recruitment')
                ->where('id_pencapaian_kpi_hr_recruitment', $request->header_id)
                ->delete();

            $idheader = DB::table('rc_pencapaian_kpi_hr_recruitment')->insertGetId([
                'nama_parameter'            => $request->nama_parameter,
                'tahun_pencapaian'          => $request->tahun_pencapaian
            ]);
            //insert detail
            for ($i=1;$i<=$request->jmldetail;$i++) {
                $a = 'nama_kpi'.$i;
                $b = 'polarization'.$i;
                $c = 'uom'.$i;
                $d = 'weight'.$i;
                $e = 'definition'.$i.'_1';
                $f = 'definition'.$i.'_2';
                $g = 'output'.$i;
                $z = 'target'.$i;

                // checkbox
                $ca = 'manager'.$i;
                $cb = 'pic'.$i;
                $cc = 'staff'.$i;

                $manager=(!empty($request->$ca) && !is_null($request->$ca)) ? 'active' : '';
                $pic=(!empty($request->$cb) && !is_null($request->$cb)) ? 'active' : '';
                $staff=(!empty($request->$cc) && !is_null($request->$cc)) ? 'active' : '';

                if (!empty($request->$a) && !is_null($request->$a)) {
                    $iddetail = DB::table('rc_kpi_hr_recruitment')->insertGetId([
                                            'id_pencapaian_kpi_hr_recruitment' => $idheader,
                                            'nama_kpi'     => $request->$a,
                                            'polarization' => $request->$b,
                                            'uom'          => $request->$c,
                                            'weight'       => $request->$d,
                                            'target'       => $request->$z,
                                            'definition_1' => $request->$e,
                                            'definition_2' => $request->$f,
                                            'output'       => $request->$g,
                                            'manager'      => $manager,
                                            'pic'          => $pic,
                                            'staff'        => $staff,
                    ]);
                    //nested detail
                    $h = 'bulan'.$i;
                    $j = 'target_perbulan'.$i;
                    $k = 'weightd'.$i;
                    $l = 'act'.$i;
                    $m = 'ach'.$i;

                    for ($x=0;$x<count($request->$h);$x++) {
                        $bulan = $request->$h[$x];
                        $target_perbulan = $request->$j[$x];
                        $weightd = $request->$k[$x];
                        $act = $request->$l[$x];
                        $ach = $request->$m[$x];
                        DB::table('rc_kpi_hr_recruitment_pencapaian_bulanan')->insert([
                          'id_kpi_hr_recruitment' => $iddetail,
                          'bulan' => $bulan,
                          'target_perbulan' => $target_perbulan,
                          'weight' => $weightd,
                          'act' => $act,
                          'ach'=> $ach,
                        ]);
                    }
                }
            }

            DB::commit();
            return redirect('/list_pkhr');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('tambah_pkhr');
        }
    }

    public function tambah()
    {
        $data_pkhr=DB::table('rc_kpi_hr_recruitment')->get();
        $pkhr=DB::table('rc_pencapaian_kpi_hr_recruitment')->select('tahun_pencapaian', 'status_rekaman')->get();
        $pkhr=DB::table('rc_pencapaian_kpi_hr_recruitment')->select('tahun_pencapaian', 'status_rekaman')->get();
        $datakpi=DB::table('rc_pencapaian_kpi_hr_recruitment')->select('tahun_pencapaian', 'tanggal_input', 'nama_parameter')->get();
        return view('re.pkhr.tambah', ['datakpi'=>$datakpi, 'pkhr'=>$pkhr]);
    }

    public function get_w_pkhr($id)
    {
        $look = DB::table('rc_kpi_hr_recruitment')
            ->where('id_pencapaian_kpi_hr_recruitment', $id)
            ->select('id_kpi_hr_recruitment', 'nama_kpi', 'polarization', 'oum', 'weight', 'definition_1', 'definition_2', 'output', 'target', 'manager', 'pic', 'staff')
            ->get();

        foreach ($look as $keyDet => $val) {
            $look[$keyDet]->no = ($keyDet + 1);
        }
        return $look;
        // print_r($look);
    }

    public function hapus($id_pencapaian_kpi_hr_recruitment)
    {
        $del =  DB::table('rc_pencapaian_kpi_hr_recruitment')
        ->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }

    public function preview($id_pencapaian_kpi_hr_recruitment)
    {
        
        $pkhr_p = DB::table('rc_kpi_hr_recruitment')->where('id_pencapaian_kpi_hr_recruitment', $id_pencapaian_kpi_hr_recruitment)->get();
        return view('re.pkhr.preview');
    }

}