<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class kac_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wskantorcabang")->where('id_kantor', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_kac')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wskantorcabang')->where('id_kantor', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_kantor', $select)) {
                array_push($select, 'id_kantor');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wskantorcabang')->select($select);

            if($hasPersonalTable->kode_kantor) {
                $query->where('kode_kantor', $hasPersonalTable->kode_kantor);
            }
            if($hasPersonalTable->nama_kantor) {
                $query->where('nama_kantor', $hasPersonalTable->nama_kantor);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('ws.wskantorcabang.filterResult', $data);
        } else {
            $wskantorcabang=DB::table('wskantorcabang')->get();
            return view('ws.wskantorcabang.index',['wskantorcabang'=>$wskantorcabang]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kac')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kac')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wskantorcabang=DB::table('wskantorcabang')->get();
        return view('wskantorcabang',['wskantorcabang'=>$wskantorcabang]);
    }

    public function filter()
    {
        $fields = [
            // [
            //     'text'  => 'Id Kantor',
            //     'value' => 'id_kantor'
            // ],
            [
                'text'  => 'Kode Kantor',
                'value' => 'kode_kantor'
            ],
            [
                'text'  => 'Nama kantor',
                'value' => 'nama_kantor'
            ],
            [
                'text'  => 'Kode Kelas',
                'value' => 'kode_kelas'
            ],
            [
                'text'  => 'Tipe Kantor',
                'value' => 'tipe_kantor'
            ],
            [
                'text'  => 'Kode Lokasi Kerja',
                'value' => 'kode_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Nomor NPWP',
                'value' => 'nomor_npwp'
            ],
            [
                'text'  => 'UMK UMP',
                'value' => 'umk_ump'
            ],
            [
                'text'  => 'TTD SPT',
                'value' => 'ttd_spt'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
        ];

            $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wskantorcabang.filter', $data);
    }

    public function detail($id_kantor){
        $wskantorcabang = DB::table('wskantorcabang')->where('id_kantor', $id_kantor)->get();
        return view('ws.wskantorcabang.detail',['wskantorcabang'=> $wskantorcabang]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_kantor';
        } else {
            $select = ['id_kantor', 'kode_kantor', 'nama_kantor', 'tipe_kantor'];
        }

        $query = DB::table('wskantorcabang')->select($select);

        if($request->kode_kantor) {
            $query->where('kode_kantor', $request->kode_kantor);
        }

        if($request->nama_kantor) {
            $query->where('nama_kantor', $request->nama_kantor);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kac')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kac')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_kac')->insert([
            'kode_kantor'   => $request->kode_kantor,
            'nama_kantor'   => $request->nama_kantor,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_kac');
    }
    
    public function simpan(Request $request){
        DB::table('wskantorcabang')->insert([
            'kode_kantor'   =>$request->kode_kantor,
            'nama_kantor'   =>$request->nama_kantor,
            'kode_kelas'         =>$request->kode_kelas,
            'tipe_kantor'            =>$request->tipe_kantor,
            'kode_lokasi_kerja'   =>$request->kode_lokasi_kerja,
            'nama_lokasi_kerja'  =>$request->nama_lokasi_kerja,
            'nomor_npwp'  =>$request->nomor_npwp,
            'umk_ump'  =>$request->umk_ump,
            'ttd_spt'  =>$request->ttd_spt,
            'keterangan'  =>$request->keterangan,
            'status_rekaman'  =>$request->status_rekaman,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'  =>$request->tanggal_selesai_efektif,
        ]);

        DB::table('wstampilantabledashboarduser_kac')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_kantor'   => NULL,
                'nama_kantor'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_kac');
    }

    public function update(Request $request) {
        DB::table('wskantorcabang')->where('id_kantor', $request->id_kantor)->update([
            'id_kantor'   =>$request->id_kantor,
            'kode_kantor' =>$request->kode_kantor,
            'nama_kantor' =>$request->nama_kantor,
            'kode_kelas'       =>$request->kode_kelas,
            'tipe_kantor'       =>$request->tipe_kantor,
            'kode_lokasi_kerja'         =>$request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   =>$request->nama_lokasi_kerja,
            'nomor_npwp'   =>$request->nomor_npwp,
            'umk_ump'   =>$request->umk_ump,
            'ttd_spt'   =>$request->ttd_spt,
            'keterangan'   =>$request->keterangan,
            'status_rekaman'   =>$request->status_rekaman,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_kac');
    }
    public function tambah() {
        $look = DB::table('wslokasikerja')->select('id_lokasi_kerja','kode_lokasi_kerja','nama_lokasi_kerja')->get();
        $dataklk['looks'] = $look;
        $code_ = $this->get_number();

        return view('ws.wskantorcabang.tambah',['dataklk'=>$dataklk,'random'=>$code_]);
    }

    public function get_number(){
        $max_id = DB::table('wskantorcabang')
        ->where('id_kantor', \DB::raw("(select max(`id_kantor`) from wskantorcabang)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_kantor) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_kantor, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'KC-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    
    public function edit($id_kantor) {
        $look = DB::table('wslokasikerja')->select('id_lokasi_kerja','kode_lokasi_kerja','nama_lokasi_kerja')->get();
        $dataklk['looks'] = $look;
        $wskantorcabang = DB::table('wskantorcabang')->where('id_kantor', $id_kantor)->get();
        return view('ws.wskantorcabang.edit',['wskantorcabang'=> $wskantorcabang, 'dataklk'=>$dataklk]);
    }
    public function delete($id_perusahaan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_kantor){
       $del= DB::table('wskantorcabang')->where('id_kantor', $id_kantor)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_kac');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kac')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kac')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_kac');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wskantorcabang")->whereIn('id_kantor',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wskantorcabang = DB::table('wskantorcabang')
    //     ->where('kode_kantor','like',"%".$cari."%")
    //     ->orWhere('nama_kantor','like',"%".$cari."%")
    //     ->orWhere('kode_kelas','like',"%".$cari."%")    
    //     ->orWhere('tipe_kantor','like',"%".$cari."%")    
    //     ->orWhere('kode_lokasi_kerja','like',"%".$cari."%")    
    //     ->orWhere('nama_lokasi_kerja','like',"%".$cari."%")    
    //     ->orWhere('nomor_npwp','like',"%".$cari."%")
    //     ->orWhere('umk_ump','like',"%".$cari."%")  
    //     ->orWhere('ttd_spt','like',"%".$cari."%") 
    //     ->orWhere('keterangan','like',"%".$cari."%")  
    //     ->orWhere('status_rekaman','like',"%".$cari."%")   
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")    
    //     ->paginate();

    //     return view('wskantorcabang',['wskantorcabang' =>$wskantorcabang]);
    // }

}
