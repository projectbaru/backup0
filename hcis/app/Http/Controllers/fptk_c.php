<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\FPTK;
use App\Models\Master_Kar;
use App\Helpers\HelperFunctions;

class fptk_c extends Controller {

    protected function findModel($id){
        if (($model = FPTK::where('id_fptk', $id)->first()) !== null) {
            return $model;
        } else {
            // throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    function getDepartemen($kode){
		//convert departemen
		$departemen="";$s=1;
		if(isset($kode)){
			$str_arr = explode (",", $kode);  
			foreach($str_arr as $r){
				$adepartemen="";
				$con = \Yii::$app->db;
				$query_departemen = "SELECT kode,keterangan FROM struktur_content where kode='$r' limit 1";			
				$model_departemen = $con->createCommand($query_departemen)->queryOne();
				if($model_departemen){
					$adepartemen = $model_departemen['keterangan'];
				}else{
					$adepartemen = "";
				}
				
				if($s==1){
					$departemen = $adepartemen;
				}else{
					$departemen = $departemen . "<br>" . $adepartemen ;	
				}
				$s++;
			}
		}
		//end convert departemen	
		return $departemen;
	}

    public function multiDelete(Request $request){
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index(){
        $rc_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->orderBy('id_fptk', 'asc')->get();
        
        return view('re.fptk.index',['rc_fptk'=>$rc_fptk]);
    }

    public function index_a()
    {
        $rc_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk.list_approval',['rc_fptk'=>$rc_fptk]);
    }

    public function index_send_a()
    {
        $rc_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk.send_approv_list',['rc_fptk'=>$rc_fptk]);
    }

    public function tambah()
    {
        $data_kar=Master_Kar::select('Nama_Karyawan','Kode_Karyawan', 'nik','Tipe_User')->where('active','=','Y')->orWhere('id_dept','=','14')->orderBy("Nama_Karyawan", "asc")->get();
        $data_k['looks']=$data_kar;
                

        $data_jab=DB::table('wsjabatan')->select('nama_jabatan')->get();
        $data_jb['looks']=$data_jab;
        $data_lok=DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $data_lk['looks']=$data_lok;
        $data_lh['looks']=DB::table('wsgruplokasikerja')->select('nama_grup_lokasi_kerja')->get();
        $data_glok['looks']=$data_lh;
        $data_rc=DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen','nik_pemohon','nama_pemohon','jabatan_pemohon','departemen_pemohon','penggantian_tenaga_kerja','penambahan_tenaga_kerja','penambahan_tenaga_kerja','head_hunter','status_kepegawaian','kontrak_selama')->get();
        return view('re.fptk.tambah',['data_k' => $data_k,'$data_glok'=>$data_glok, 'data_rc'=>$data_rc,'data_jb'=>$data_jb, 'data_lk'=>$data_lk, 'data_lh'=>$data_lh]);
    }

    public function approval()
    {
        $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk.list_status',['data_fptk'=>$data_fptk]);
    } 

    public function list_approval(){
        $rc_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk.list_approval',['rc_fptk'=>$rc_fptk]);
    }
    
    public function edit($id_fptk)
    {
        $data_kar=Master_Kar::select('Nama_Karyawan','Kode_Karyawan', 'nik','Tipe_User')->orderBy("Nama_Karyawan", "asc")->where('active','=','Y')->orWhere('id_dept','=','14')->get();
        $data_k['looks']=$data_kar;
        
        $data_jbk=DB::table('wsjabatan')->select('nama_jabatan')->get();
        $data_lk=DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $data_rf=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();

        // $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();
        // $data_fp['looks']=$data_fptk;

        return view('re.fptk.edit', ['data_rf'=>$data_rf,'data_k'=>$data_k, 'data_lk'=>$data_lk, 'data_jbk'=>$data_jbk]);
    }

    public function preview($id_fptk)
    {    
        $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        $data=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();
        return view('re.fptk.preview', compact('data','data_fptk'));
    }
    
    public function detail($id_fptk)
    {
        $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();
        $data_jbk=DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk.detail', ['data_fptk'=>$data_fptk,'data_jbk'=>$data_jbk]);
    }
    
    public function upload_doc($id_fptk){
        $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();
        return view('re.fptk.upload_doc', ['data_fptk'=>$data_fptk]);
    }

    public function detail_ap($id_fptk){
        $data_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->first();
        $data_jbk = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();

        $doctype_id = "1000066";
        $module_record_id = $data_fptk->no_approval;
        
        $body =  array("doctype_id" => $doctype_id,
                        "module_record_id" => $module_record_id);	
        $post_data = HelperFunctions::callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/list_approval_status', json_encode($body));
        $response = json_decode($post_data, true); 

        $urutan = 0;
        $approval_name = "";
        $approval_jabatan = "";
        $status_approval = "";
        $approval_note = "";
        $reject_reason = "";
        $reject_by = "";
        $approval_id = "";
        $sru_wfl_progress_id = "";
        $jmlos = 1;

        foreach($response['response'] as $row){
            if($row['wfl_state'] == "OS"){
                
                if($jmlos == 1){
                    $urutan = $row['urutan'];
                    $approval_name = $row['user'];
                    $approval_jabatan = $row['jabatan'];
                    $status_approval = "WAITING APPROVAL";
                    $approval_note = $row['note'];
                    $approval_id = $row['approval_id'];
                    $sru_wfl_progress_id = $row['sru_wfl_progress_id'];
                }
                $jmlos++;
                
            }else if($row['wfl_state'] == "CC"){
                if($row['approved'] == "Rejected"){
                    $status_approval = "REJECTED";
                    $reject_by = $row['user'];
                }else{
                    $status_approval = "RELEASE";
                    $urutan = $row['urutan'];
                    $approval_name = $row['user'];
                    $approval_jabatan=$row['jabatan'];
                    $approval_note = $row['note'];
                    $approval_id = $row['approval_id'];
                    $sru_wfl_progress_id = $row['sru_wfl_progress_id'];	 
                }
            }
            
            if($row['approved'] == "Rejected"){
                $reject_reason = $row['reject_reason_global'];
            }
        
        }

        $approval_nik = HelperFunctions::find_nik_idempiere($approval_id);

        if($approval_nik == ""){
            $approval_kode = "";
        }else{
            $approval_kode = Master_Kar::where('nik', $approval_nik )->first()->Kode_Karyawan;
        }
        
        $model = FPTK::where('id_fptk', $id_fptk)->update([
            "status" => $status_approval,
            "approval_kode" => $approval_kode,
            "approval_id" => $approval_id,
            "sru_wfl_progress_id" => $sru_wfl_progress_id,
        ]);
    
        return view('re.fptk.list_status', ['data_fptk'=>$data_fptk, 'data_jbk'=>$data_jbk, 'approver' => $response['response']]);
    }

    public function fptk_r()
    {
        $rc_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        return view('re.fptk_report.index', ['rc_fptk'=>$rc_fptk]);
    }

    public function detail_fptk_r($id_fptk)
    {
        $rc_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk',$id_fptk)->get();
        return view('re.fptk_report.detail', ['rc_fptk'=>$rc_fptk]);
    }

    public function actionSendapproval_2($id_fptk) {
        $fptk = FPTK::where('id_fptk', $id_fptk)->first();
        $pemohon = Master_Kar::where('Kode_Karyawan', $fptk['kode_pemohon'] )->first();
        $atonum = rand(0,99999999);
        $no_approval_exist = DB::table('rc_formulir_permintaan_tenaga_kerja')->where('no_approval', $atonum)->first();
        if($no_approval_exist) $atonum = rand(0,99999999);
        $model = FPTK::where('id_fptk', $id_fptk)->update(['no_approval' => $atonum]);

        //kasi link approval
        $link_approve2 = "<a href=http://10.10.20.231:2020/sirup/reqmis/viewapproval/>Approve</a>";

        $today = date('Y-m-d H:i:s');
        $auth_id = Auth::user()->id;

        //id modul
        $module_record_id = $atonum; 
        $doctype_id = 1000066; 
        $title = "rc_formulir_permintaan_tenaga_kerja";
        $documentno = $fptk->id_fptk;
        $documentdate = $today;
        $description =  "permintaan_tenaga_kerja";

        // dibuat oleh
		$jabatan = 'Pembuat';

		$ad_user_id_z = HelperFunctions::find_ad_user_id($pemohon->nik);

		$created_external_by = $ad_user_id_z;
		$createdby = $ad_user_id_z;
		$ad_user_id = $ad_user_id_z;
		$header = 1000040;

        //sign
        $list_user_approval = [];
        $approvels = array(['jabatan' => 'Dibuat Oleh', 'id' => $fptk->kode_pemohon, 'header' => 1000040],
                                ['jabatan' => 'Diperiksa Oleh', 'id' => $fptk->kode_atasan_pemohon, 'header' => 1000042],
                                ['jabatan' => 'Disetujui Oleh', 'id' => $fptk->kode_penyetuju, 'header' => 1000045],
                                ['jabatan' => 'Disetujui/Dianalisa Oleh', 'id' => $fptk->kode_penganalisa, 'header' => 1000044]
        );
        foreach($approvels as $ap) {
            $approval_nik = Master_Kar::where(['Kode_Karyawan' => $ap['id']])->first();
            $kode_ad_user_id = HelperFunctions::find_ad_user_id($approval_nik->nik);
            $sign = array(
                    "ad_user_id" => $kode_ad_user_id,
                    "header" => $ap['header'],
                    "jabatan" => $ap['jabatan'],
            );
            array_push($list_user_approval, $sign);
        }

        $scontent = array(
			//setting tombol
			"misc" =>
			[
				"approve" => true,
				"reject" => true,
				"note_only" => true
			],
			//judul
			"data" =>
			[
				"document_type" => $fptk->id_fptk,
				"documentno" => $fptk->id_fptk . "/RC_FPTK\/" . $atonum,
				"record_id" => $atonum,
			],
			//Tab
			"segment" =>
			[
			  //Tab 1
			  [
				 "title" => "detail [:] INFORMASI [:] list",
				 "content" =>
				 //isi
				 [
					"Approval ID [:] $atonum [:] full_html",
					"Nomor FPTK [:] $fptk->id_fptk [:] full_html",
					"Nama Pengaju [:] $pemohon->Nama_Karyawan [:] full_html",
					"Jabatan [:] $fptk->jabatan_pemohon [:] full_html",
					"Departemen [:] $fptk->departemen [:] full_html",
					"Departemen [:] $fptk->departemen_pemohon [:] full_html",
					// "Tanggal Pengajuan [:] $fptk->tgl_pengajuan [:] full_html",
				 ]
			  ],


			]
		);
		$content = json_encode($scontent);

		$body =  array("content" => $content,
					   "doctype_id" => $doctype_id,
					   "module_record_id" => $module_record_id,
					   "createdby" => $createdby,
					   "created_external_by" => $created_external_by,
					   "title" => $title,
					   "documentno" => $documentno,
					   "documentdate" => $documentdate,
					   "description" => $description,
					   "ad_user_id" => $ad_user_id,
					   "header" => $header,
					   "jabatan" => $jabatan,
					   "html_title" => "Approval & Atachment",
					   "html" => $link_approve2,
					   "list_user_approval" => $list_user_approval
					   );


		$post_data = HelperFunctions::callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/create', json_encode($body));
		$response = json_decode($post_data, true);
		
        $model = FPTK::where('id_fptk', $id_fptk)->update(['status' => "Waiting Approval"]);

        return redirect()->route('list_fptk')->with('success', 'Approval Sent');
    }

    public function simpan(Request $request)
    {

        if(($request->status) ==''){
            $status='new';
        }
        
        $lampiran_file = optional($request->file('lampiran_file'));
        $nama_file = time() . "." . $lampiran_file->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $lampiran_file->move($tujuan_upload, $nama_file);
        DB::table('rc_formulir_permintaan_tenaga_kerja')->insert([
            'nik_pemohon'   => $request->nik_pemohon,
            'nama_pemohon'         => $request->nama_pemohon,
            'jabatan_pemohon'   => $request->jabatan_pemohon,
            'nik_pemohon'   => $request->nik_pemohon,
            'kode_pemohon'   => $request->kode_pemohon,
            'departemen_pemohon'   => $request->departemen_pemohon,
            'penggantian_tenaga_kerja'  => $request->penggantian_tenaga_kerja,
            'penambahan_tenaga_kerja' => $request->penambahan_tenaga_kerja,
            'head_hunter' => $request->head_hunter,
            'status'=>$status,
            'status_kepegawaian' => $request->status_kepegawaian,
            'tunjangan_makan' => $request->tunjangan_makan,
            'status_kandidat' => $request->status_kandidat,
            'kontrak_selama' => $request->kontrak_selama,
            'magang_selama' => $request->magang_selama,
            'jabatan_kepegawaian' => $request->jabatan_kepegawaian,
            'tingkat_kepegawaian' => $request->tingkat_kepegawaian,
            'tanggal_mulai_bekerja' => $request->tanggal_mulai_bekerja,
            'jumlah_kebutuhan' => $request->jumlah_kebutuhan,
            'lokasi_kerja' => $request->lokasi_kerja,
            'alasan_penambahan_tenaga_kerja' => $request->alasan_penambahan_tenaga_kerja,
            'gaji' => $request->gaji,
            'tunjangan_kesehatan' => $request->tunjangan_kesehatan,
            'tunjangan_transportasi' => $request->tunjangan_transportasi,
            'tunjangan_komunikasi' => $request->tunjangan_komunikasi,
            'hari_kerja' => $request->hari_kerja,
            'jam_kerja' => $request->jam_kerja,
            'jenis_kelamin' => $request->jenis_kelamin,
            'usia_minimal' => $request->usia_minimal,
            'usia_maksimal' => $request->usia_maksimal,
            'pendidikan' => $request->pendidikan,
            'pengalaman' => $request->pengalaman,
            'kemampuan_lainnya' => $request->kemampuan_lainnya,
            'lampiran_file' => $nama_file,
            'jabatan_penyetuju' => $request->jabatan_penyetuju,
            'nik_penyetuju'   => $request->nik_penyetuju,
            'kode_penyetuju'   => $request->kode_penyetuju,
            'nama_penyetuju' => $request->nama_penyetuju,
            'ttd_penyetuju' => $request->ttd_penyetuju,
            'nama_penganalisa' => $request->nama_penganalisa,
            'nik_penganalisa'   => $request->nik_penganalisa,
            'kode_penganalisa'   => $request->kode_penganalisa,
            'ttd_hr' => $request->ttd_hr,
            'analisa_hrd' => $request->analisa_hrd,
            'ttd_pemohon' => $request->ttd_pemohon, 
            'nama_atasan_pemohon' => $request->nama_atasan_pemohon, 
            'nik_atasan'   => $request->nik_atasan,
            'kode_atasan_pemohon'   => $request->kode_atasan_pemohon,
            'ttd_atasan_pemohon' => $request->ttd_atasan_pemohon, 
            'jabatan_atasan_pemohon' => $request->jabatan_atasan_pemohon, 
            'status_rekaman' => $request->status_rekaman, 
            'nik_input_user' => $request->nik_input_user, 
            'nama_user_input' => $request->nama_user_input, 
            'pengguna_masuk' => $request->pengguna_masuk,
            'dokumen_terakhir' => $request->dokumen_terakhir,
            'tanggal_upload_dokumen_terakhir' => $request->tanggal_upload_dokumen_terakhir,
        ]);
        return redirect('/list_fptk');
    }

    public function dupli(Request $request){
        $data_fptk=DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->get();
        return view('re.fptk.list_status',['data_fptk'=>$data_fptk]);
    }

    public function duplicate(Request $request){
        
        DB::table('rc_formulir_permintaan_tenaga_kerja')->insert([
            'nik_pemohon'   => $request->nik_pemohon,
            'nama_pemohon'         => $request->nama_pemohon,
            'kode_pemohon'         => $request->kode_pemohon,
            'jabatan_pemohon'   => $request->jabatan_pemohon,
            'departemen_pemohon'   => $request->departemen_pemohon,
            'penggantian_tenaga_kerja'  => $request->penggantian_tenaga_kerja,
            'status_kandidat' => $request->status_kandidat,
            'penambahan_tenaga_kerja' => $request->penambahan_tenaga_kerja,
            'head_hunter' => $request->head_hunter,
            'status_kepegawaian' => $request->status_kepegawaian,
            'tunjangan_makan' => $request->tunjangan_makan,
            'status_kandidat' => $request->status_kandidat,
            'kontrak_selama' => $request->kontrak_selama,
            'magang_selama' => $request->magang_selama,
            'jabatan_kepegawaian' => $request->jabatan_kepegawaian,
            'tingkat_kepegawaian' => $request->tingkat_kepegawaian,
            'tanggal_mulai_bekerja' => $request->tanggal_mulai_bekerja,
            'jumlah_kebutuhan' => $request->jumlah_kebutuhan,
            'lokasi_kerja' => $request->lokasi_kerja,
            'alasan_penambahan_tenaga_kerja' => $request->alasan_penambahan_tenaga_kerja,
            'gaji' => $request->gaji,
            'tunjangan_kesehatan' => $request->tunjangan_kesehatan,
            'tunjangan_transportasi' => $request->tunjangan_transportasi,
            'tunjangan_komunikasi' => $request->tunjangan_komunikasi,
            'hari_kerja' => $request->hari_kerja,
            'jam_kerja' => $request->jam_kerja,
            'jenis_kelamin' => $request->jenis_kelamin,
            'usia_minimal' => $request->usia_minimal,
            'usia_maksimal' => $request->usia_maksimal,
            'pendidikan' => $request->pendidikan,
            'pengalaman' => $request->pengalaman,
            'kemampuan_lainnya' => $request->kemampuan_lainnya,
            'lampiran_file' => $request->lampiran_file,
            'jabatan_penyetuju' => $request->jabatan_penyetuju,
            'nama_penyetuju' => $request->nama_penyetuju,
            'ttd_penyetuju' => $request->ttd_penyetuju,
            'nama_penganalisa' => $request->nama_penganalisa,
            'hasil_penyetujuan_hr' => $request->hasil_penyetujuan_hr,
            'ttd_hr' => $request->ttd_hr,
            'analisa_hrd' => $request->analisa_hrd,
            'ttd_pemohon' => $request->ttd_pemohon, 
            'nama_atasan_pemohon' => $request->nama_atasan_pemohon, 
            'ttd_atasan_pemohon' => $request->ttd_atasan_pemohon, 
            'kode_atasan_pemohon' => $request->kode_atasan_pemohon, 
            'jabatan_atasan_pemohon' => $request->jabatan_atasan_pemohon, 
            'status_rekaman' => $request->status_rekaman, 
            'nik_input_user' => $request->nik_input_user, 
            'nama_user_input' => $request->nama_user_input, 
            'pengguna_masuk' => $request->pengguna_masuk,
            'dokumen_terakhir' => $request->dokumen_terakhir,
            'tanggal_upload_dokumen_terakhir' => $request->tanggal_upload_dokumen_terakhir,
        ]);

        return redirect('/list_fptk');
    }

    public function hapus($id_fptk)
    {
        $del =  DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $id_fptk)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_p');
    }
    
    public function filter(){
        $fields = [
            [
                'text'  => 'No Dokumen',
                'value' => 'no_dokumen'
            ],
            [
                'text'  => 'NIK Pemohon',
                'value' => 'nik_pemohon'
            ],
            [
                'text'  => 'Nama Pemohon',
                'value' => 'nama_pemohon'
            ],
            [
                'text'  => 'Jabatan Pemohon',
                'value' => 'jabatan_pemohon'
            ],
            [
                'text'  => 'Departemen Pemohon',
                'value' => 'departemen_pemohon'
            ],
            [
                'text'  => 'Penggantian Tenaga Kerja',
                'value' => 'penggantian_tenaga_kerja'
            ],
            [
                'text'  => 'Penambahan Tenaga Kerja',
                'value' => 'penambahan_tenaga_kerja'
            ],
            [
                'text'  => 'Head Hunter',
                'value' => 'head_hunter'
            ],
            [
                'text'  => 'Status Kepegawaian',
                'value' => 'status_kepegawaian'
            ],
            [
                'text'  => 'Kontrak Selama',
                'value' => 'kontrak_selama'
            ],
            [
                'text'  => 'Magang Selama',
                'value' => 'magang_selama'
            ],
            [
                'text'  => 'Jabatan Kepegawaian',
                'value' => 'jabatan_kepegawaian'
            ],
            [
                'text'  => 'Tingkat Kepegawaian',
                'value' => 'tingkat_kepegawaian'
            ],
            [
                'text'  => 'Tanggal Mulai Bekerja',
                'value' => 'tanggal_mulai_bekerja'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Alasan Penambahan Tenaga Kerja',
                'value' => 'alasan_penambahan_tenaga_kerja'
            ],
            [
                'text'  => 'Gaji',
                'value' => 'gaji'
            ],
            [
                'text'  => 'Tunjangan Kesehatan',
                'value' => 'tunjangan_kesehatan'
            ],
            [
                'text'  => 'Tunjangan Transportasi',
                'value' => 'tunjangan_transportasi'
            ],
            [
                'text'  => 'Tunjangan Komunikasi',
                'value' => 'tunjangan_komunikasi'
            ],
            [
                'text'  => 'Hari Kerja',
                'value' => 'hari_kerja'
            ],
            [
                'text'  => 'Jam Kerja',
                'value' => 'jam_kerja'
            ],
            [
                'text'  => 'Jenis Kelamin',
                'value' => 'jenis_kelamin'
            ],
            [
                'text'  => 'Usia Minimal',
                'value' => 'usia_minimal'
            ],
            [
                'text'  => 'Usia Maksimal',
                'value' => 'usia_maksimal'
            ],
            [
                'text'  => 'Pendidikan',
                'value' => 'pendidikan'
            ],
            [
                'text'  => 'Pengalaman',
                'value' => 'pengalaman'
            ],
            [
                'text'  => 'Kemampuan Layanan',
                'value' => 'kemampuan_layanan'
            ],
            [
                'text'  => 'Lampiran File',
                'value' => 'lampiran_file'
            ],
            [
                'text'  => 'Jabatan Penyetuju',
                'value' => 'jabatan_penyetuju'
            ],
            [
                'text'  => 'Nama Penyetuju',
                'value' => 'nama_penyetuju'
            ],
            [
                'text'  => 'TTD Penyetuju',
                'value' => 'ttd_penyetuju'
            ],
            [
                'text'  => 'Nama Penganalisa',
                'value' => 'nama_penganalisa'
            ],
            [
                'text'  => 'Hasil Penyetujuan HR',
                'value' => 'hasil_penyetujuan_hr'
            ],
            [
                'text'  => 'TTD HR',
                'value' => 'ttd_hr'
            ],
            [
                'text'  => 'Analisa HRD',
                'value' => 'analisa_hrd'
            ],
            [
                'text'  => 'TTD Pemohon',
                'value' => 'ttd_pemohon'
            ],
            [
                'text'  => 'Nama Atasan Pemohon',
                'value' => 'nama_atasan_pemohon'
            ],
            [
                'text'  => 'TTD Atasan Pemohon',
                'value' => 'ttd_atasan_pemohon'
            ],
            [
                'text'  => 'Jabatan Atasan Pemohon',
                'value' => 'jabatan_atasan_pemohon'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'NIK Input User',
                'value' => 'nik_input_user'
            ],
            [
                'text'  => 'Nama User Input',
                'value' => 'nama_user_input'
            ],
            [
                'text'  => 'Dokumen Terakhir',
                'value' => 'dokumen_terakhir'
            ],
            [
                'text'  => 'Tanggal Upload Dokumen Terakhir',
                'value' => 'tanggal_upload_dokumen_terakhir'
            ]
           
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }
       
        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('re.fptk.filter', $data);
    }

    public function update(Request $request)
    {
        
        $data_update = array(
            'no_dokumen'   => $request->no_dokumen,
            'status'         => $request->status,
            'nik_pemohon'         => $request->nik_pemohon,
            'nama_pemohon' => $request->nama_pemohon,
            'kode_pemohon'   => $request->kode_pemohon,
            'jabatan_pemohon' => $request->jabatan_pemohon,
            'departemen_pemohon'   => $request->departemen_pemohon,
            'penggantian_tenaga_kerja'  => $request->penggantian_tenaga_kerja,
            'status_kandidat' => $request->status_kandidat,
            'penambahan_tenaga_kerja' => $request->penambahan_tenaga_kerja,
            'head_hunter' => $request->head_hunter,
            'status_kepegawaian' => $request->status_kepegawaian,
            'tunjangan_makan' => $request->tunjangan_makan,
            'kontrak_selama' => $request->kontrak_selama,
            'magang_selama' => $request->magang_selama,
            'jabatan_kepegawaian' => $request->jabatan_kepegawaian,
            'tingkat_kepegawaian' => $request->tingkat_kepegawaian,
            'tanggal_mulai_bekerja' => $request->tanggal_mulai_bekerja,
            'jumlah_kebutuhan' => $request->jumlah_kebutuhan,
            'lokasi_kerja' => $request->lokasi_kerja,
            'alasan_penambahan_tenaga_kerja' => $request->alasan_penambahan_tenaga_kerja,
            'gaji' => $request->gaji,
            'tunjangan_kesehatan' => $request->tunjangan_kesehatan,
            'tunjangan_transportasi' => $request->tunjangan_transportasi,
            'tunjangan_komunikasi' => $request->tunjangan_komunikasi,
            'hari_kerja' => $request->hari_kerja,
            'jam_kerja' => $request->jam_kerja,
            'jenis_kelamin' => $request->jenis_kelamin,
            'usia_minimal' => $request->usia_minimal,
            'usia_maksimal' => $request->usia_maksimal,
            'pendidikan' => $request->pendidikan,
            'pengalaman' => $request->pengalaman,
            'kemampuan_lainnya' => $request->kemampuan_lainnya,
            'lampiran_file' => $request->lampiran_file,
            'jabatan_penyetuju' => $request->jabatan_penyetuju,
            'kode_penyetuju' => $request->kode_penyetuju,
            'nik_penyetuju' => $request->nik_penyetuju,
            'nama_penyetuju' => $request->nama_penyetuju,
            'ttd_penyetuju' => $request->ttd_penyetuju,
            'nama_penganalisa' => $request->nama_penganalisa,
            'nama_penganalisa' => $request->nik_penganalisa,
            'ttd_hr' => $request->ttd_hr,
            'analisa_hrd' => $request->analisa_hrd,
            'ttd_pemohon' => $request->ttd_pemohon, 
            'nama_atasan_pemohon' => $request->nama_atasan_pemohon,
            'kode_atasan_pemohon' => $request->kode_atasan_pemohon, 
            'nik_atasan' => $request->nik_atasan,
            'ttd_atasan_pemohon' => $request->ttd_atasan_pemohon, 
            'jabatan_atasan_pemohon' => $request->jabatan_atasan_pemohon, 
            'status_rekaman' => $request->status_rekaman, 
            'nik_input_user' => $request->nik_input_user, 
            'nama_user_input' => $request->nama_user_input, 
            'pengguna_masuk' => $request->pengguna_masuk,
            'dokumen_terakhir' => $request->dokumen_terakhir,
            'tanggal_upload_dokumen_terakhir' => $request->tanggal_upload_dokumen_terakhir,
        );
        if ($request->file('lampiran_file')) {
            $lampiran_file = optional($request->file('lampiran_file'));
            $nama_file = time() . "." . $lampiran_file->getClientOriginalName();
            $tujuan_upload = 'data_file';
            $lampiran_file->move($tujuan_upload, $nama_file);
            $data_update['lampiran_file'] = $nama_file;
        }

        DB::table('rc_formulir_permintaan_tenaga_kerja')->where('id_fptk', $request->id_fptk)->update($data_update); 

        return redirect('/list_fptk');
    }

}
