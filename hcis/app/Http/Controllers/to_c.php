<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class to_c extends Controller
{
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wstingkatorganisasi')->where('id_tingkat_organisasi', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_tingkat_organisasi', $select)) {
                array_push($select, 'id_tingkat_organisasi');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wstingkatorganisasi')->select($select);

            if($hasPersonalTable->kode_tingkat_organisasi) {
                $query->where('kode_tingkat_organisasi', $hasPersonalTable->kode_tingkat_organisasi);
            }
            if($hasPersonalTable->nama_tingkat_organisasi) {
                $query->where('nama_tingkat_organisasi', $hasPersonalTable->nama_tingkat_organisasi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('ws.wstingkat_org.filterResult', $data);
        } else {
            $wstingkatorganisasi=DB::table('wstingkatorganisasi')->get();
            return view('ws.wstingkat_org.index',['wstingkatorganisasi'=>$wstingkatorganisasi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_to')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_organisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wsorganisasi=DB::table('wstingkatorganisasi')->get();
        return view('ws.wstingkat_org.index',['wstingkatorganisasi'=>$wsorganisasi]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_tingkat_organisasi';
        } else {
            $select = ['id_tingkat_organisasi', 'kode_tingkat_organisasi', 'nama_tingkat_organisasi', 'urutan_tingkat', 'urutan_tampilan'];
        }

        $query = DB::table('wstingkatorganisasi')->select($select);

        if($request->kode_tingkat_organisasi) {
            $query->where('kode_tingkat_organisasi', $request->kode_tingkat_organisasi);
        }

        if($request->nama_tingkat_organisasi) {
            $query->where('nama_tingkat_organisasi', $request->nama_tingkat_organisasi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_to')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_to')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_to')->insert([
            'kode_tingkat_organisasi'   => $request->kode_tingkat_organisasi,
            'nama_tingkat_organisasi'   => $request->nama_tingkat_organisasi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_to');
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Tingkat Organisasi',
                'value' => 'kode_tingkat_organisasi'
            ],
            [
                'text'  => 'Nama Tingkat Organisasi',
                'value' => 'nama_tingkat_organisasi'
            ],
            [
                'text'  => 'Urutan Tingkat',
                'value' => 'urutan_tingkat'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }


        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wstingkat_org.filter', $data);
    }

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_to')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wstingkatorganisasi = DB::table('wstingkatorganisasi')
    //     ->where('kode_tingkat_organisasi','like',"%".$cari."%")
    //     ->orWhere('nama_tingkat_organisasi','like',"%".$cari."%")
    //     ->orWhere('urutan_tingkat','like',"%".$cari."%")
    //     ->orWhere('urutan_tampilan','like',"%".$cari."%")
    //     ->orWhere('keterangan','like',"%".$cari."%")
    //     ->orWhere('status_rekaman','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%")
    //     ->orWhere('waktu_masuk','like',"%".$cari."%")
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%")
    //     ->orWhere('waktu_ubah','like',"%".$cari."%")
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%")
    //     ->orWhere('waktu_hapus','like',"%".$cari."%")
    //     ->paginate();
    //     return view('ws.wstingkat_org',['wstingkatorganisasi' =>$wstingkatorganisasi]);
    // }
    
    public function tambah() {
        $code_=$this->get_number();
        return view('ws.wstingkat_org.tambah', ['random'=>$code_]);

    }

    public function get_number(){
        $max_id = DB::table('wstingkatorganisasi')
        ->where('id_tingkat_organisasi', \DB::raw("(select max(`id_tingkat_organisasi`) from wstingkatorganisasi)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_tingkat_organisasi) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_tingkat_organisasi, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'TORG-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

   
    
    public function simpan(Request $request){
        
        DB::table('wstingkatorganisasi')->insert([
            'kode_tingkat_organisasi'   =>$request->kode_tingkat_organisasi,
            'nama_tingkat_organisasi'   =>$request->nama_tingkat_organisasi,
            'urutan_tingkat'            =>$request->urutan_tingkat,
            'urutan_tampilan'         =>$request->urutan_tampilan,
            'keterangan'            =>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,

        ]);

        DB::table('wstampilantabledashboarduser_to')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_tingkat_organisasi'   => NULL,
                'nama_tingkat_organisasi'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        return redirect('/list_to');
    }

    public function duplicate(Request $request){
        DB::table('wstingkatorganisasi')->insert([
            'kode_tingkat_organisasi'   =>$this->get_number(),
            'nama_tingkat_organisasi'   =>$request->nama_tingkat_organisasi,
            'urutan_tingkat'            =>$request->urutan_tingkat,
            'urutan_tampilan'         =>$request->urutan_tampilan,
            'keterangan'            =>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,

        ]);

        DB::table('wstampilantabledashboarduser_to')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_tingkat_organisasi'   => NULL,
                'nama_tingkat_organisasi'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        return redirect('/list_to');
    }

    public function detail($id_tingkat_organisasi){
        $wstingkatorganisasi = DB::table('wstingkatorganisasi')->where('id_tingkat_organisasi', $id_tingkat_organisasi)->get();
        $code_=$this->get_number();
        return view('ws.wstingkat_org.detail',['wstingkatorganisasi'=> $wstingkatorganisasi, 'random'=>$code_]);
    }
    public function edit($id_tingkat_organisasi) {
        $wstingkatorganisasi = DB::table('wstingkatorganisasi')->where('id_tingkat_organisasi', $id_tingkat_organisasi)->get();
        return view('ws.wstingkat_org.edit',['wstingkatorganisasi'=> $wstingkatorganisasi]);
    }
    public function hapus($id_tingkat_organisasi){
       $del = DB::table('wstingkatorganisasi')->where('id_tingkat_organisasi', $id_tingkat_organisasi)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_o');
    }
    public function update(Request $request) {
        DB::table('wstingkatorganisasi')->where('id_tingkat_organisasi', $request->id_tingkat_organisasi)->update([
            'id_tingkat_organisasi'             =>$request->id_tingkat_organisasi,
            'kode_tingkat_organisasi'   =>$request->kode_tingkat_organisasi,
            'nama_tingkat_organisasi'   =>$request->nama_tingkat_organisasi,
            'urutan_tingkat'   =>$request->urutan_tingkat,
            'urutan_tampilan'         =>$request->urutan_tampilan,
            'keterangan'   =>$request->keterangan,
            'status_rekaman'        =>$request->status_rekaman,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk'=>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);
        return redirect('/list_to');
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wstingkatorganisasi")->where('id_tingkat_organisasi', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function deleteAll(){
        DB::table('wstingkatperusahaan')->insert();
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tingkatorganisasi')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tingkatorganisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_wstingkatorganisasi');
    }
    public function reset_all(){
        $hasPersonalTable = DB::table('wstampilantabledashaboarduser_tingkatorganisasi')->where('user_id', Auth::user()->id)->orderBy('id', '');
    }
}
