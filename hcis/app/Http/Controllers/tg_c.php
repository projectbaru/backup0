<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class tg_c extends Controller
{
    public function index(Request $request)
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_tingkat_golongan', $select)) {
                array_push($select, 'id_tingkat_golongan');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wstingkatgolongan')->select($select);

            if ($hasPersonalTable->nama_tingkat_golongan) {
                $query->where('nama_tingkat_golongan', $hasPersonalTable->nama_tingkat_golongan);
            }
            if ($hasPersonalTable->kode_tingkat_golongan) {
                $query->where('kode_tingkat_golongan', $hasPersonalTable->kode_tingkat_golongan);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('ws.wstingkatgolongan.filterResult', $data);
        } else {
            $wstingkatgolongan = DB::table('wstingkatgolongan')->get();

            if ($request->has('keyword')) {
                $wstingkatgolongan = DB::table('wstingkatgolongan')
                    ->where(function ($query) use ($request) {
                        $query->where('nama_perusahaan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('kode_tingkat_golongan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_tingkat_golongan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('urutan_tampilan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('keterangan', 'LIKE', '%' . $request->keyword . '%');
                    })
                    ->get();
            }
            foreach ($wstingkatgolongan as $key => $value) {
                $wstingkatgolongan[$key]->detail_item = $this->get_detail_item($value->items, true);
            }

            $data = [
                'wstingkatgolongan' => $wstingkatgolongan
            ];

            return view('ws.wstingkatgolongan.index', $data);
        }
    }

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_tg')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function detail($id_tingkat_golongan)
    {
        $wstingkatgolongan = DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->first();

        $items = '';

        if ($wstingkatgolongan->items) {

            $selectedItems = str_replace('[', '', $wstingkatgolongan->items);
            $selectedItems = str_replace(']', '', $selectedItems);
            $selectedItems = str_replace('"', '', $selectedItems);
            $selectedItems = explode(',', $selectedItems);

            $query = DB::table('wstingkatgolongan_item')->whereIn('id', $selectedItems)->get(); // ??

            foreach ($query as $row) {
                if ($items == '') {
                    $items .= $row->name;
                } else {
                    $items .= ', ' . $row->name;
                }
            }

            $items .= '.';
        }

        $items_new = $this->get_detail_item($wstingkatgolongan->items, false);
        $code_ = $this->get_number();

        $data = [
            'items' => $items,
            'items_new' => $items_new,
            'random' => $code_ ,
            'wstingkatgolongan' => $wstingkatgolongan
        ];


        return view('ws.wstingkatgolongan.detail', $data);
    }

    function get_detail_item($id, $param)
    {
        $selectedItems = str_replace('[', '', $id);
        $selectedItems = str_replace(']', '', $selectedItems);
        $selectedItems = str_replace('"', '', $selectedItems);
        $selectedItems = explode(',', $selectedItems);

        $ret = DB::table('wsgolongan')
            ->whereIn('id_golongan', $selectedItems)
            ->select('id_golongan', 'nama_golongan')
            ->get();
        $data = "";
        if ($param) {
            foreach ($ret as $key) {
                if ($data == "") {
                    $data .= $key->nama_golongan;
                } else {
                    $data .= ", " . $key->nama_golongan;
                }
            }
            return $data;
        }
        return $ret;
    }

    function duplicate(Request $request)
    {
        $id = $request->tg_id;

        if ($id) {
            $wstingkatgolongan = DB::table('wstingkatgolongan')
                ->where('id_tingkat_golongan', $id)
                ->first();
            DB::table('wstingkatgolongan')->insert([
                'nama_perusahaan'   => $wstingkatgolongan->nama_perusahaan,
                'kode_tingkat_golongan'   => $wstingkatgolongan->kode_tingkat_golongan,
                'nama_tingkat_golongan'    => $wstingkatgolongan->nama_tingkat_golongan,
                'urutan_tampilan'   => $wstingkatgolongan->urutan_tampilan,
                'tanggal_mulai_efektif'   => $wstingkatgolongan->tanggal_mulai_efektif ? date('Y-m-d', strtotime($wstingkatgolongan->tanggal_mulai_efektif)) : null,
                'tanggal_selesai_efektif'   => $wstingkatgolongan->tanggal_selesai_efektif ? date('Y-m-d', strtotime($wstingkatgolongan->tanggal_selesai_efektif)) : null,
                'keterangan'   => $wstingkatgolongan->keterangan,
                'items'   => $wstingkatgolongan->items,
                'pengguna_masuk' => Auth::user()->id,
                'waktu_masuk' => date('Y-m-d')
            ]);
            $this->showAlert('success', "Berhasil salin data");
            return redirect('/list_tg');
        }

        $this->showAlert('error', "Data tidak ditemukan");
        return redirect('/list_tg');
    }



    public function simpan(Request $request)
    {
        $items = null;

        if ($request->items) {
            $items = implode(';', (array) $request->items);
            // $items = json_encode($items);
        }
        // return $request->all();
        DB::table('wstingkatgolongan')->insert([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_tingkat_golongan'   => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'    => $request->nama_tingkat_golongan,
            'urutan_tampilan'   => $request->urutan_tampilan,
            // 'tanggal_mulai'   => $request->tanggal_mulai ? date('Y-m-d', strtotime($request->tanggal_mulai)) : null,
            'tanggal_mulai_efektif'   => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'keterangan'   => $request->keterangan,
            'items'   => $items,
            'pengguna_masuk' => Auth::user()->id,
            'waktu_masuk' => date('Y-m-d')
        ]);

        return redirect('/list_tg');
    }
    public function tambah()
    {
        $look = DB::table('wsperusahaan')->select('id_perusahaan', 'nama_perusahaan')->get();
        $np['looks'] = $look;
        // $items = DB::table('wsgolongan')->select('nama_golongan', 'id_golongan')->groupBy('nama_golongan')->get();
        $items = DB::table('wsgolongan')->select('nama_golongan', 'id_golongan')->get();
        $data = [
            'items' => $items
        ];

        $code_=$this->get_number();

        return view('ws.wstingkatgolongan.tambah', $data, ['np' => $np,'random'=>$code_]);
    }

    

    public function get_number(){
        $max_id = DB::table('wstingkatgolongan')
        ->where('id_tingkat_golongan', \DB::raw("(select max(`id_tingkat_golongan`) from wstingkatgolongan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_tingkat_golongan) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_tingkat_golongan, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'TGOL-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }


    public function edit($id_tingkat_golongan)
    {
        $wstingkatgolongan = DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->first();

        $selectedItems = '';
        $selectedOptionItems = [];

        if ($wstingkatgolongan->items) {
            $selectedItems = str_replace('[', '', $wstingkatgolongan->items);
            $selectedItems = str_replace(']', '', $selectedItems);
            $selectedItems = str_replace('"', '', $selectedItems);

            $selectedOptionItems = explode(',', $selectedItems);
        }

        $items = DB::table('wsgolongan')->get();

        $data = [
            'id' => $id_tingkat_golongan,
            'items' => $items,
            'selectedItems' => $selectedItems,
            'selectedOptionItems' => $selectedOptionItems,
            'wstingkatgolongan' => $wstingkatgolongan
        ];
        

        $look = DB::table('wsperusahaan')->select('id_perusahaan', 'nama_perusahaan')->get();
        $nama['looks'] = $look;
        return view('ws.wstingkatgolongan.edit', $data, ['nama'=>$nama]);
    }

    public function update(Request $request)
    {
        $items = null;
        $id_tingkat_golongan = $request->id_tingkat_golongan;
        if ($request->items) {
            $items = implode(';', (array) $request->items);
            // $items = json_encode($items);
        }

        DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)
        ->update([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_tingkat_golongan'   => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'    => $request->nama_tingkat_golongan,
            'urutan_tampilan'   => $request->urutan_tampilan,
            'tanggal_mulai'   => $request->tanggal_mulai ? date('Y-m-d', strtotime($request->tanggal_mulai)) : null,
            'tanggal_mulai_efektif'   => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'keterangan'   => $request->keterangan,
            'items'   => $items,
            'pengguna_ubah' => Auth::user()->id,
            'waktu_ubah' => date('Y-m-d')
        ]);

        return redirect('/list_tg');
    }

    public function delete($id_tingkat_golongan)
    {

        DB::table("wstingkatgolongan")->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function hapus($id_tingkat_golongan)
    {

        $del = DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_tg');
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        foreach ($request->multiDelete as $id_tingkat_golongan) {
            DB::table("wstingkatgolongan")->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Tingkat Golongan',
                'value' => 'kode_tingkat_golongan'
            ],
            [
                'text'  => 'Nama Tingkat Golongan',
                'value' => 'nama_tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wstingkatgolongan.filter', $data);
    }


    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_tingkat_golongan';
        } else {
            $select = ['id_tingkat_golongan', 'nama_tingkat_golongan', 'kode_tingkat_golongan', 'nama_perusahaan'];
        }
        $query = DB::table('wstingkatgolongan')->select($select);
        if ($request->kode_tingkat_golongan) {
            $query->where('kode_tingkat_golongan', $request->kode_tingkat_golongan);
        }
        if ($request->nama_tingkat_golongan) {
            $query->where('nama_tingkat_golongan', $request->nama_tingkat_golongan);
        }
        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }
        DB::table('wstampilantabledashboarduser_tg')->insert([
            'kode_tingkat_golongan'  => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'  => $request->nama_tingkat_golongan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_tg');
    }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_tg');
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }
        $wstingkatgolongan = DB::table('wstingkatgolongan')->get();
        return view('ws.wstingkatgolongan.index', ['wstingkatgolongan' => $wstingkatgolongan]);
    }
}
