<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RcOrganisasiPelamar;
use App\Http\Resources\APIResource;

class RcOrganisasiPelamarController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = RcOrganisasiPelamar::all();
        return new APIResource($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = RcOrganisasiPelamar::create($request->all());
        return new APIResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query = RcOrganisasiPelamar::where('id_organisasi_pelamar', $id);
        $query->update($request->all());
        $data = $query->first();
        return new APIResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = RcOrganisasiPelamar::where('id_organisasi_pelamar', $id)
            ->delete();
        return response(null, 204);
    }
}
