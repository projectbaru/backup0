<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\APIResource;
use Illuminate\Support\Facades\DB;
use App\Models\RcFormDataPelamar;
use App\Models\RcSusunanKeluargaPelamar;
use App\Models\RcFormLatarBelakangPendidikan;
use App\Models\RcPelatihanPelamar;
use App\Models\RcBahasaAsing;
use App\Models\RcOrganisasiPelamar;
use App\Models\RcPertanyaanPilihanEssai;

class DPPController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = RcFormDataPelamar::with([
            'susunan_keluarga',
            'latar_belakang_pendidikan',
            'pelatihan',
            'bahasa_asing',
            'organisasi',
            'riwayat_kerja',
            'referensi_pelamar',
            'kontak_darurat',
            'negatif_positif',
            'jawaban_pilihan.pertanyaan_pilihan_esai',
            'jawaban_esai.pertanyaan_pilihan_esai',
            'jawaban_marston.pertanyaan_marston_model',
        ])->get();
        return new APIResource($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
        $rcFormDataPelamar = RcFormDataPelamar::create([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'no_ktp' => $request->no_ktp,
            'posisi_lamaran' => $request->posisi_lamaran,
            'email_pribadi' => $request->email_pribadi,
            'nama_lengkap' => $request->nama_lengkap,
            'alamat_sosmed' => $request->alamat_sosmed,
            'nama_panggilan' => $request->nama_panggilan,
            'kategori_sim' => $request->kategori_sim,
            'no_sim' => $request->no_sim,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,

            'no_hp' => $request->no_hp,
            'agama' => $request->agama,
            'no_telepon' => $request->no_telepon,
            'kebangsaan' => $request->kebangsaan,
            'tempat_tinggal' => $request->tempat_tinggal,
            'jenis_kelamin' => $request->jenis_kelamin,
            'status_perkawinan' => $request->status_perkawinan,
            'berat_badan' => $request->berat_badan,
            'tinggi_badan' => $request->tinggi_badan,
            'alamat_diluar_kota' => $request->alamat_diluar_kota,
            'status_kendaraan' => $request->status_kendaraan,

            'jenis_merk_kendaraan' => $request->jenis_merk_kendaraan,
            'kode_pos_1' => $request->kode_pos_1,
            'alamat_didalam_kota' => $request->alamat_didalam_kota,
            'kode_pos_2' => $request->kode_pos_2,
            'pas_foto' => $request->pas_foto,
        ]);
        $idDataPelamar = $rcFormDataPelamar->id;
        
        foreach ($request->susunan_keluarga as $sk) {
            RcSusunanKeluargaPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'hubungan_keluarga' => $sk["hubungan_keluarga"],
                'nama_keluarga' => $sk["nama_keluarga"],
                'jenis_kelamin' => $sk["jenis_kelamin"],
                'usia_tahun_lahir' => $sk["usia_tahun_lahir"],
                'pendidikan_terakhir' => $sk["pendidikan_terakhir"],
                'jabatan' => $sk["jabatan"],
                'perusahaan' => $sk["perusahaan"],
                'keterangan_susunan_keluarga' => $sk["keterangan_susunan_keluarga"],
            ]);
        }

        foreach ($request->latar_belakang_pendidikan as $lbp) {
            RcFormLatarBelakangPendidikan::create([
                'id_data_pelamar' =>  $idDataPelamar,
                'tingkat_pendidikan' => $lbp["tingkat_pendidikan"],
                'nama_sekolah' => $lbp["nama_sekolah"],
                'tempat_kota' => $lbp["tempat_kota"],
                'jurusan' => $lbp["jurusan"],
                'tahun_masuk' => $lbp["tahun_masuk"],
                'tahun_keluar' => $lbp["tahun_keluar"],
                'status_kelulusan' => $lbp["status_kelulusan"],
            ]);
        }

        foreach ($request->pelatihan as $p) {
            RcPelatihanPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'bidang_pelatihan' => $p["bidang_pelatihan"],
                'penyelenggara' => $p["penyelenggara"],
                'tempat_kota' => $p["tempat_kota"],
                'lama_pelatihan' => $p["lama_pelatihan"],
                'tahun_pelatihan' => $p["tahun_pelatihan"],
                'dibiayai_oleh' => $p["dibiayai_oleh"],
            ]);
        }

        foreach ($request->bahasa_asing as $ba) {
            RcBahasaAsing::create([
                'id_data_pelamar' => $idDataPelamar,
                'bahasa' => $ba["bahasa"],
                'mendengar' => $ba["mendengar"],
                'berbicara' => $ba["berbicara"],
                'membaca' => $ba["membaca"],
                'menulis' => $ba["menulis"],
            ]);
        }

        foreach ($request->organisasi as $o) {
            RcOrganisasiPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'nama_organisasi' => $o["nama_organisasi"],
                'jenis_kegiatan' => $o["jenis_kegiatan"],
                'jabatan' => $o["jabatan"],
                'tahun' => $o["tahun"],
            ]);
        }
        foreach ($request->susunan_keluarga as $sk) {
            RcOrganisasiPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'nama_organisasi' => $o["nama_organisasi"],
                'jenis_kegiatan' => $o["jenis_kegiatan"],
                'jabatan' => $o["jabatan"],
                'tahun' => $o["tahun"],
            ]);
        }
        foreach ($request->riwayat_kerja as $rk) {
            RcRiwayatKerjaPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'nama_perusahaan' => $rk["nama_perusahaan"],
                'alamat' => $rk["alamat"],
                'nama_atasan' => $rk["nama_atasan"],
                'lama_bekerja' => $rk["lama_bekerja"],
                'gaji_terakhir' => $rk["gaji_terakhir"],
                'jenis_usaha' => $rk["jenis_usaha"],
                'alasan_berhenti' => $rk["alasan_berhenti"],
                'jabatan_awal' => $rk["jabatan_awal"],
                'jabatan_akhir' => $rk["jabatan_akhir"],
                'telp_perusahaan' => $rk["telp_perusahaan"],
                'nama_direktur' => $rk["nama_direktur"],
            ]);
        }

        foreach ($request->referensi_pelamar as $rp) {
            RcRiwayatKerjaPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'kode_kontak_refrensi_pelamar' => $rp["kode_kontak_refrensi_pelamar"],
                'nama_lengkap' => $rp["nama_lengkap"],
                'alamat_telpon' => $rp["alamat_telpon"],
                'pekerjaan' => $rp["pekerjaan"],
                'hubungan' => $rp["hubungan"],
            ]);
        }
        foreach ($request->kontak_referensi as $kr) {
            RcKontakReferensiPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'kode_kontak_refrensi_pelamar' => $kr["kode_kontak_refrensi_pelamar"],
                'nama_lengkap' => $kr["nama_lengkap"],
                'alamat_telpon' => $kr["alamat_telpon"],
                'pekerjaan' => $kr["pekerjaan"],
                'hubungan' => $kr["hubungan"],
            ]);
        }
        foreach ($request->kontak_darurat as $kd) {
            RcKontakDarurat::create([
                'id_data_pelamar' => $idDataPelamar,
                'kode_kontak_darurat' => $kd["kode_kontak_darurat"],
                'nama_lengkap' => $kd["nama_lengkap"],
                'alamat_telpon' => $kd["alamat_telpon"],
                'pekerjaan' => $kd["pekerjaan"],
                'hubungan' => $kd["hubungan"],
            ]);
        }
        foreach ($request->negatif_positif as $npp) {
            RcNegatifPositifPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'kategori' => $npp["kategori"],
                'keterangan' => $kr["keterangan"],
            ]);
        }
        
        foreach ($request->pertanyaan_pilihan as $pp) {
            $rcPertanyaanPilihanEsai = RcPertanyaanPilihanEsai::create([
                'kode_pertanyaan_pilihan_esai' => $pp["kode_pertanyaan_pilihan_esai"],
                'pertanyaan' => $pp["pertanyaan"],
                'kategori_pertanyaan' => $pp["kategori_pertanyaan"],
            ]);

            RcJawabanPilihan::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_pilihan_esai' => $rcPertanyaanPilihanEsai->id_pertanyaan_pilihan_sesai,
                'jawaban' => $pp["jawaban_pilihan"]["jawaban"],
                'penjelasan' => $pp["jawaban_pilihan"]["penjelasan"],
            ]);
        }

        foreach ($request->pertanyaan_esai as $pe) {
            $rcPertanyaanPilihanEsai = RcPertanyaanPilihanEsai::create([
                'kode_pertanyaan_pilihan_esai' => $pe["kode_pertanyaan_pilihan_esai"],
                'pertanyaan' => $pe["pertanyaan"],
                'kategori_pertanyaan' => $pe["kategori_pertanyaan"],
            ]);

            RcJawabanEsai::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_pilihan_esai' => $rcPertanyaanPilihanEsai->id_pertanyaan_pilihan_sesai,
                'jawaban' => $pe["jawaban_esai"]["jawaban"],
            ]);
        }

        foreach ($request->pertanyaan_marston as $pm) {
            $rcPertanyaanMarstonModel = RcPertanyaanMarstonModel::create([
                'kode_pertanyaan_marston_model' => $pm["kode_pertanyaan_marston_model"],
                'rston_model' => $pm["rston_model"],
                'pertanyaan' => $pm["pertanyaan"],
            ]);

            RcJawabanMarstonModel::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_marston_model' => $rcPertanyaanMarstonModel->id_pertanyaan_marston_model,
                'nama_lengkap' => $pm["jawaban_marston"]["nama_lengkap"],
                'jawaban' => $pm["jawaban_marston"]["jawaban"],
            ]);
        }

        
        DB::commit();
        return new APIResource($rcFormDataPelamar);

        } catch(\Exception $e) {
            DB::rollback();
            return response($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $query = RcBahasaAsing::where('id_bahasa_asing ', $id);
        // $query->update($request->all());
        // $data = $query->first();
        // return new APIResource($data);
        DB::beginTransaction();
        try {
        $query = RcFormDataPelamar::where('id_data_pelamar', $id);
        $query->update([
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'no_ktp' => $request->no_ktp,
            'posisi_lamaran' => $request->posisi_lamaran,
            'email_pribadi' => $request->email_pribadi,
            'nama_lengkap' => $request->nama_lengkap,
            'alamat_sosmed' => $request->alamat_sosmed,
            'nama_panggilan' => $request->nama_panggilan,
            'kategori_sim' => $request->kategori_sim,
            'no_sim' => $request->no_sim,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,

            'no_hp' => $request->no_hp,
            'agama' => $request->agama,
            'no_telepon' => $request->no_telepon,
            'kebangsaan' => $request->kebangsaan,
            'tempat_tinggal' => $request->tempat_tinggal,
            'jenis_kelamin' => $request->jenis_kelamin,
            'status_perkawinan' => $request->status_perkawinan,
            'berat_badan' => $request->berat_badan,
            'tinggi_badan' => $request->tinggi_badan,
            'alamat_diluar_kota' => $request->alamat_diluar_kota,
            'status_kendaraan' => $request->status_kendaraan,

            'jenis_merk_kendaraan' => $request->jenis_merk_kendaraan,
            'kode_pos_1' => $request->kode_pos_1,
            'alamat_didalam_kota' => $request->alamat_didalam_kota,
            'kode_pos_2' => $request->kode_pos_2,
            'pas_foto' => $request->pas_foto,
        ]);
        $idDataPelamar = $id;
        
        RcSusunanKeluargaPelamar::where('id_data_pelamar', $id)->delete();
        RcFormLatarBelakangPendidikan::where('id_data_pelamar', $id)->delete();
        RcPelatihanPelamar::where('id_data_pelamar', $id)->delete();
        RcBahasaAsing::where('id_data_pelamar', $id)->delete();
        RcOrganisasiPelamar::where('id_data_pelamar', $id)->delete();
       
        $idsPertanyaanPilihan = RcJawabanPilihan::where('id_data_pelamar', $id)->pluck('id_pertanyaan_pilihan_esai')->toArray();
        $idsPertanyaanEsai = RcJawabanEsai::where('id_data_pelamar', $id)->pluck('id_pertanyaan_pilihan_esai')->toArray();
        $idsPertanyaanMarston = RcJawabanMarstonModel::where('id_data_pelamar', $id)->pluck('id_pertanyaan_marston_model')->toArray();
        RcPertanyaanPilihanEsai::whereIn('id_pertanyaan_pilihan_esai', array_merge($idsPertanyaanPilihan, $idsPertanyaanEsai))->delete();
        RcPertanyaanMarstonModel::whereIn('id_pertanyaan_marston_model', $idsPertanyaanMarston)->delete();
        RcJawabanPilihan::where('id_data_pelamar', $id)->delete();
        RcJawabanEsai::where('id_data_pelamar', $id)->delete();
        RcJawabanMarstonModel::where('id_data_pelamar', $id)->delete();

        foreach ($request->susunan_keluarga as $sk) {
            RcSusunanKeluargaPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'hubungan_keluarga' => $sk["hubungan_keluarga"],
                'nama_keluarga' => $sk["nama_keluarga"],
                'jenis_kelamin' => $sk["jenis_kelamin"],
                'usia_tahun_lahir' => $sk["usia_tahun_lahir"],
                'pendidikan_terakhir' => $sk["pendidikan_terakhir"],
                'jabatan' => $sk["jabatan"],
                'perusahaan' => $sk["perusahaan"],
                'keterangan_susunan_keluarga' => $sk["keterangan_susunan_keluarga"],
            ]);
        }

        foreach ($request->latar_belakang_pendidikan as $lbp) {
            RcFormLatarBelakangPendidikan::create([
                'id_data_pelamar' =>  $idDataPelamar,
                'tingkat_pendidikan' => $lbp["tingkat_pendidikan"],
                'nama_sekolah' => $lbp["nama_sekolah"],
                'tempat_kota' => $lbp["tempat_kota"],
                'jurusan' => $lbp["jurusan"],
                'tahun_masuk' => $lbp["tahun_masuk"],
                'tahun_keluar' => $lbp["tahun_keluar"],
                'status_kelulusan' => $lbp["status_kelulusan"],
            ]);
        }

        foreach ($request->pelatihan as $p) {
            RcPelatihanPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'bidang_pelatihan' => $p["bidang_pelatihan"],
                'penyelenggara' => $p["penyelenggara"],
                'tempat_kota' => $p["tempat_kota"],
                'lama_pelatihan' => $p["lama_pelatihan"],
                'tahun_pelatihan' => $p["tahun_pelatihan"],
                'dibiayai_oleh' => $p["dibiayai_oleh"],
            ]);
        }

        foreach ($request->bahasa_asing as $ba) {
            RcBahasaAsing::create([
                'id_data_pelamar' => $idDataPelamar,
                'bahasa' => $ba["bahasa"],
                'mendengar' => $ba["mendengar"],
                'berbicara' => $ba["berbicara"],
                'membaca' => $ba["membaca"],
                'menulis' => $ba["menulis"],
            ]);
        }

        foreach ($request->organisasi as $o) {
            RcOrganisasiPelamar::create([
                'id_data_pelamar' => $idDataPelamar,
                'nama_organisasi' => $o["nama_organisasi"],
                'jenis_kegiatan' => $o["jenis_kegiatan"],
                'jabatan' => $o["jabatan"],
                'tahun' => $o["tahun"],
            ]);
        }
        
        foreach ($request->pertanyaan_pilihan as $pp) {
            $rcPertanyaanPilihanEsai = RcPertanyaanPilihanEsai::create([
                'kode_pertanyaan_pilihan_esai' => $pp["kode_pertanyaan_pilihan_esai"],
                'pertanyaan' => $pp["pertanyaan"],
                'kategori_pertanyaan' => $pp["kategori_pertanyaan"],
            ]);

            RcJawabanPilihan::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_pilihan_esai' => $rcPertanyaanPilihanEsai->id_pertanyaan_pilihan_sesai,
                'jawaban' => $pp["jawaban_pilihan"]["jawaban"],
                'penjelasan' => $pp["jawaban_pilihan"]["penjelasan"],
            ]);
        }

        foreach ($request->pertanyaan_esai as $pe) {
            $rcPertanyaanPilihanEsai = RcPertanyaanPilihanEsai::create([
                'kode_pertanyaan_pilihan_esai' => $pe["kode_pertanyaan_pilihan_esai"],
                'pertanyaan' => $pe["pertanyaan"],
                'kategori_pertanyaan' => $pe["kategori_pertanyaan"],
            ]);

            RcJawabanEsai::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_pilihan_esai' => $rcPertanyaanPilihanEsai->id_pertanyaan_pilihan_sesai,
                'jawaban' => $pe["jawaban_esai"]["jawaban"],
            ]);
        }

        foreach ($request->pertanyaan_marston as $pm) {
            $rcPertanyaanMarstonModel = RcPertanyaanMarstonModel::create([
                'kode_pertanyaan_marston_model' => $pm["kode_pertanyaan_marston_model"],
                'rston_model' => $pm["rston_model"],
                'pertanyaan' => $pm["pertanyaan"],
            ]);

            RcJawabanMarstonModel::create([
                'id_data_pelamar' => $idDataPelamar,
                'id_pertanyaan_marston_model' => $rcPertanyaanMarstonModel->id_pertanyaan_marston_model,
                'nama_lengkap' => $pm["jawaban_marston"]["nama_lengkap"],
                'jawaban' => $pm["jawaban_marston"]["jawaban"],
            ]);
        }


        DB::commit();
        $data = $query->first();
        return new APIResource($data);

        } catch(\Exception $e) {
            DB::rollback();
            return response($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RcFormDataPelamar::where('id_data_pelamar', $id) ->delete();
        RcSusunanKeluargaPelamar::where('id_data_pelamar', $id)->delete();
        RcFormLatarBelakangPendidikan::where('id_data_pelamar', $id)->delete();
        RcPelatihanPelamar::where('id_data_pelamar', $id)->delete();
        RcBahasaAsing::where('id_data_pelamar', $id)->delete();
        RcOrganisasiPelamar::where('id_data_pelamar', $id)->delete();
        
        $idsPertanyaanPilihan = RcJawabanPilihan::where('id_data_pelamar', $id)->pluck('id_pertanyaan_pilihan_esai')->toArray();
        $idsPertanyaanEsai = RcJawabanEsai::where('id_data_pelamar', $id)->pluck('id_pertanyaan_pilihan_esai')->toArray();
        $idsPertanyaanMarston = RcJawabanMarstonModel::where('id_data_pelamar', $id)->pluck('id_pertanyaan_marston_model')->toArray();
        RcPertanyaanPilihanEsai::whereIn('id_pertanyaan_pilihan_sesai', array_merge($idsPertanyaanPilihan, $idsPertanyaanEsai))->delete();
        RcPertanyaanMarstonModel::whereIn('id_pertanyaan_marston_model', $idsPertanyaanMarston)->delete();
        RcJawabanPilihan::where('id_data_pelamar', $id)->delete();
        RcJawabanEsai::where('id_data_pelamar', $id)->delete();
        RcJawabanMarstonModel::where('id_data_pelamar', $id)->delete();
    
        return response(null, 204);
    }
}