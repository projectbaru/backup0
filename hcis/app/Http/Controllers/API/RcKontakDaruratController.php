<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RcKontakDarurat;
use App\Http\Resources\APIResource;

class RcKontakDaruratController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = RcKontakDarurat::all();
        return new APIResource($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = RcKontakDarurat::create($request->all());
        return new APIResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query = RcKontakDarurat::where('id_kontak_darurat', $id);
        $query->update($request->all());
        $data = $query->first();
        return new APIResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = RcKontakDarurat::where('id_kontak_darurat', $id)
            ->delete();
        return response(null, 204);
    }
}
