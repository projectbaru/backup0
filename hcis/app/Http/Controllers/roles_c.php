<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class roles_c extends Controller
{
    //yepi
    public function __construct()
    {
        $this->middleware('auth');
    }
    //yepi
    public function index()
    {
        $login = false;
        $roles = DB::table('roles')->get();
        $np = DB::table('wsperusahaan')->get();
        if (Auth::user()->id) {
            $this->showAlert('success', 'Berhasil Login');
            $login = true;
            return view('roles.index', ['roles' => $roles, 'np' => $np, 'login' => $login]);
        } else {
            return redirect('login');
        }
    }
}
