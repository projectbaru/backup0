<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class g_c extends Controller
{
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsgolongan')->where('id_golongan', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_golongan', $select)) {
                array_push($select, 'id_golongan');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wsgolongan')->select($select);

            if($hasPersonalTable->kode_golongan) {
                $query->where('kode_golongan', $hasPersonalTable->kode_golongan);
            }
            if($hasPersonalTable->nama_golongan) {
                $query->where('nama_golongan', $hasPersonalTable->nama_golongan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('ws.wsgolongan.filterResult', $data);
        } else {
            $wsgolongan=DB::table('wsgolongan')->get();
            return view('ws.wsgolongan.index',['wsgolongan'=>$wsgolongan]);
        }
    }

    public function simpan(Request $request){
        DB::table('wsgolongan')->insert([
            'kode_golongan'   =>$request->kode_golongan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'nama_golongan'   =>$request->nama_golongan,
            'tingkat_golongan'    =>$request->tingkat_golongan,
            'urutan_golongan' =>$request->urutan_golongan,
            'urutan_tampilan'  =>$request->urutan_tampilan,
            'tingkat_posisi'        =>$request->tingkat_posisi,
            'jabatan'          =>$request->jabatan,
            'deskripsi'      =>$request->deskripsi,
            'tanggal_mulai_efektif'       =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
            
        ]);

        DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_golongan'   => NULL,
                'nama_golongan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_g');
    }

    

    public function detail($id_golongan) {
        $wsgolongan = DB::table('wsgolongan')->where('id_golongan', $id_golongan)->get();
        $code_=$this->get_number();
        return view('ws.wsgolongan.detail',['wsgolongan'=> $wsgolongan, 'random'=>$code_]);
    }

    public function hapus($id_golongan){
        $del = DB::table('wsgolongan')->where('id_golongan', $id_golongan)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_g');
    } 

    public function filter_tambah()
    {
        $fields_tambah = [
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Nama Golongan',
                'value' => 'nama_golongan'
            ],
            [
                'text'  => 'Tingkat Golongan',
                'value' => 'tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Golongan',
                'value' => 'urutan_golongan'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Jabatan',
                'value' => 'jabatan'
            ],
            [
                'text'  => 'Deskripsi',
                'value' => 'deskripsi'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsgolongan.filter', $data);
    }


    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Nama Golongan',
                'value' => 'nama_golongan'
            ],
            [
                'text'  => 'Tingkat Golongan',
                'value' => 'tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Golongan',
                'value' => 'urutan_golongan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Jabatan',
                'value' => 'jabatan'
            ],
            [
                'text'  => 'Deskripsi',
                'value' => 'deskripsi'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsgolongan.filter', $data);
    }

    public function get_number(){
        $max_id = DB::table('wsgolongan')
        ->where('id_golongan', \DB::raw("(select max(`id_golongan`) from wsgolongan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_golongan) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_golongan, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'GOL-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

   

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_golongan')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_golongan';

        } else {
            $select = ['id_golongan','kode_golongan', 'nama_golongan', 'nama_perusahaan'];
        }
        $query = DB::table('wsgolongan')->select($select);
        if($request->kode_golongan) {
            $query->where('kode_golongan', $request->kode_golongan);
        }
        if($request->nama_golongan) {
            $query->where('nama_golongan', $request->nama_golongan);
        }
        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        DB::table('wstampilantabledashboarduser_golongan')->insert([
            'kode_golongan'   => $request->kode_golongan,
            'nama_golongan'   => $request->nama_golongan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_g');
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_g');
    }

    public function update(Request $request) {
        // $perusahaan_logo = optional($request->file('perusahaan_logo'));
        // $nama_file=time().".".$perusahaan_logo->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $perusahaan_logo->move($tujuan_upload,$nama_file);
        DB::table('wsgolongan')->where('id_golongan', $request->id_golongan)->update([
            'kode_golongan'   =>$request->kode_golongan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'nama_golongan'   =>$request->nama_golongan,
            'tingkat_golongan'    =>$request->tingkat_golongan,
            'urutan_golongan' =>$request->urutan_golongan,
            'urutan_tampilan'  =>$request->urutan_tampilan,
            'tingkat_posisi'        =>$request->tingkat_posisi,
            'jabatan'          =>$request->jabatan,
            'deskripsi'      =>$request->deskripsi,
            'tanggal_mulai_efektif'       =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        return redirect('/list_g');
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wsgolongan=DB::table('wsgolongan')->get();
        return view('ws.wsgolongan.index',['wsgolongan'=>$wsgolongan]);
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsgolongan")->where('id_golongan', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function tambah() {
        $look = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dj['looks'] = $look;
        $look = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dj['looks'] = $look;
        $look = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $np['looks'] = $look;
        $looktp = DB::table('wsposisi')->select('id_posisi','tingkat_posisi')->get();
        $tp['looks'] = $looktp;
        $look = DB::table('wstingkatgolongan')->select('id_tingkat_golongan','nama_tingkat_golongan')->get();
        $tg['looks'] = $look;
        $code_=$this->get_number();
        return view('ws.wsgolongan.tambah', ['dj'=>$dj,'tg'=>$tg,'np'=>$np,'tp'=>$tp, 'random'=>$code_]);
    }
    public function edit($id_golongan) {
        $look = DB::table('wstingkatgolongan')->select('id_tingkat_golongan','nama_tingkat_golongan')->get();
        $tg['looks'] = $look;
        $look = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $np['looks'] = $look;
        $looktp = DB::table('wsposisi')->select('id_posisi','tingkat_posisi')->get();
        $tp['looks'] = $looktp;
        $look = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dj['looks'] = $look;
        $wsgolongan = DB::table('wsgolongan')->where('id_golongan', $id_golongan)->get();
        return view('ws.wsgolongan.edit',['wsgolongan'=> $wsgolongan,'tg'=> $tg, 'np'=>$np, 'tp'=>$tp, 'dj'=>$dj]);
    }
    
}
