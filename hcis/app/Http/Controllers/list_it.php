<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class list_it extends Controller
{
    public function index(Request $request)
    {
        $job = DB::table('rc_data_lowongan_kerja');
        
        if(!empty($request->kategori)) {
            $job->where('kategori_pekerjaan', 'like', "%{$request->kategori}%");
        }
        if(!empty($request->keyword)) {
            $job->where('jabatan_lowongan', 'like', "%{$request->keyword}%")
                ->orWhere('deskripsi_pekerjaan', 'like', "%{$request->keyword}%");
        }
        $job = $job->paginate(10);

        $cat = DB::table('rc_data_lowongan_kerja')->distinct()->pluck('kategori_pekerjaan')->toArray();
        return view('re.list_it.index', compact('job', 'cat', 'request'));
   }

   public function detail($id_lowongan_kerja)
    {
        $job =DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;
        return view('re.list_it.detail', ['job' => $job,'datalk' => $datalk]);
    }

    public function simpan(Request $request)
    {
        $logo_perusahaan = optional($request->file('resume_cv'));
        $nama_file = time() . "." . $logo_perusahaan->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $logo_perusahaan->move($tujuan_upload, $nama_file);
        DB::table('rc_data_cv_pelamar')->insert([
            'jabatan_lowongan' =>$request->jabatan_lowongan,
            'resume_cv'   =>$nama_file,
            'nama_lengkap'  =>$request->nama_lengkap,
            'email'  =>$request->email,
            'no_hp'=>$request->no_hp,
            'lokasi_pelamar'=>$request->lokasi_pelamar,
            'pengalaman_kerja'=>$request->pengalaman_kerja,
            'tahun_pengalaman_kerja'=>$request->tahun_pengalaman_kerja,
            'bulan_pengalaman_kerja'=>$request->bulan_pengalaman_kerja,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'nama_sekolah'=>$request->nama_sekolah,
            'program_studi_jurusan'=>$request->program_studi_jurusan,
            'pendidikan_tertinggi'=>$request->pendidikan_tertinggi,
            'tanggal_wisuda'=>$request->tanggal_wisuda,
            'nilai_rata_rata'=>$request->nilai_rata_rata,
            'nilai_skala'=>$request->nilai_skala,
            'informasi_lowongan'=>$request->informasi_lowongan,
            'keterangan_informasi_pilihan'=>$request->keterangan_informasi_pilihan,
            'pesan_pelamar'=>$request->pesan_pelamar,
            'tanggal_melamar'=>$request->tanggal_melamar,
            'status_lamaran'=>$request->status_lamaran
        ]);
        return redirect('/lamar_pekerjaan');
    }

    public function createHtml($id_lowongan_kerja)
    {
        $data =DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->first();

        $returnHTML = view('re.list_it.create_html')
            ->with('data', $data)
            ->render();

        return response()->json([
            'status' => true,
            "data" => $returnHTML,
            'msg' => "success"
        ], 200);
    }
}
