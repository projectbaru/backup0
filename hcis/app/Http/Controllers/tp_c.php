<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class tp_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wstingkatposisi")->where('id_perusahaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wstingkatposisi')->where('id_tingkat_posisi', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_tingkat_posisi', $select)) {
                array_push($select, 'id_tingkat_posisi');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wstingkatposisi')->select($select);

            if($hasPersonalTable->kode_tingkat_posisi) {
                $query->where('kode_tingkat_posisi', $hasPersonalTable->kode_tingkat_posisi);
            }
            if($hasPersonalTable->nama_tingkat_posisi) {
                $query->where('nama_tingkat_posisi', $hasPersonalTable->nama_tingkat_posisi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('ws.wstingkatposisi.filterResult', $data);
        } else {
            $wstingkatposisi=DB::table('wstingkatposisi')->get();
            return view('ws.wstingkatposisi.index',['wstingkatposisi'=>$wstingkatposisi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wstingkatposisi=DB::table('wstingkatposisi')->get();
        return view('ws.wstingkatposisi',['wstingkatposisi'=>$wstingkatposisi]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Tingkat Posisi',
                'value' => 'kode_tingkat_posisi'
            ],
            [
                'text'  => 'Nama Tingkat Posisi',
                'value' => 'nama_tingkat_posisi'
            ],
            [
                'text'  => 'Urutan Tingkat',
                'value' => 'urutan_tingkat'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wstingkatposisi.filter', $data);
    }


    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_tp')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }
    public function detail($id_tingkat_posisi){
        $wstingkatposisi = DB::table('wstingkatposisi')->where('id_tingkat_posisi', $id_tingkat_posisi)->get();
        return view('ws.wstingkatposisi.detail',['wstingkatposisi'=> $wstingkatposisi]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_tingkat_posisi';
        } else {
            $select = ['id_tingkat_posisi', 'kode_tingkat_posisi', 'nama_tingkat_posisi', 'urutan_tingkat', 'urutan_tampilan'];
        }

        $query = DB::table('wstingkatposisi')->select($select);

        if($request->kode_tingkat_posisi) {
            $query->where('kode_tingkat_posisi', $request->kode_tingkat_posisi);
        }

        if($request->nama_tingkat_posisi) {
            $query->where('nama_tingkat_posisi', $request->nama_tingkat_posisi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tp')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_tp')->insert([
            'kode_tingkat_posisi'   => $request->kode_tingkat_posisi,
            'nama_tingkat_posisi'   => $request->nama_tingkat_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_tp');
    }
    public function simpan(Request $request){
      
        DB::table('wstingkatposisi')->insert([
            'kode_tingkat_posisi'   =>$request->kode_tingkat_posisi,
            'nama_tingkat_posisi'   =>$request->nama_tingkat_posisi,
            'urutan_tingkat'         =>$request->urutan_tingkat,
            'urutan_tampilan'   =>$request->urutan_tampilan,
            'keterangan'   =>$request->keterangan,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            
        ]);

        DB::table('wstampilantabledashboarduser_tp')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_tingkat_posisi'   => NULL,
                'nama_tingkat_posisi'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_tp');
    }

    public function update(Request $request) {
        DB::table('wstingkatposisi')->where('id_tingkat_posisi', $request->id_tingkat_posisi)->update([
            'id_tingkat_posisi'         =>$request->id_tingkat_posisi,
            'kode_tingkat_posisi'       =>$request->kode_tingkat_posisi,
            'nama_tingkat_posisi'       =>$request->nama_tingkat_posisi,
            'urutan_tingkat'       =>$request->urutan_tingkat,
            'urutan_tampilan'         =>$request->urutan_tampilan,
            'keterangan'   =>$request->keterangan,
            'status_rekaman'   =>$request->status_rekaman,
            'tanggal_mulai_efektif'  =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            
        ]);

        return redirect('/list_tp');
    }
    public function tambah() {

        $tp = DB::table('wstingkatposisi')->get();

        $code_=$this->get_number();
        return view('ws.wstingkatposisi.tambah',['random'=>$code_]);
    }


    public function get_number(){
        $max_id = DB::table('wstingkatposisi')
        ->where('id_tingkat_posisi', \DB::raw("(select max(`id_tingkat_posisi`) from wstingkatposisi)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_tingkat_posisi) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_tingkat_posisi, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'TPO-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    
    

  

    public function edit($id_tingkat_posisi) {
        $wstingkatposisi = DB::table('wstingkatposisi')->where('id_tingkat_posisi', $id_tingkat_posisi)->get();
        return view('ws.wstingkatposisi.edit',['wstingkatposisi'=> $wstingkatposisi]);
    }
    public function delete($id_perusahaan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_tingkat_posisi){
        $del = DB::table('wstingkatposisi')->where('id_tingkat_posisi', $id_tingkat_posisi)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_tp');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wstingkatposisi');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wstingkatposisi")->whereIn('id_perusahaan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wstingkatposisi = DB::table('wstingkatposisi')
    //     ->where('nama_perusahaan','like',"%".$cari."%")
    //     ->orWhere('kode_perusahaan','like',"%".$cari."%")
    //     ->orWhere('singkatan','like',"%".$cari."%")    
    //     ->orWhere('visi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('misi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
    //     ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
    //     ->orWhere('lokasi_pajak','like',"%".$cari."%") 
    //     ->orWhere('npp','like',"%".$cari."%") 
    //     ->orWhere('npkp','like',"%".$cari."%") 
    //     ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
    //     ->orWhere('keterangan','like',"%".$cari."%") 
    //     ->orWhere('status_rekaman','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%") 
    //     ->orWhere('waktu_masuk','like',"%".$cari."%") 
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%") 
    //     ->orWhere('waktu_ubah','like',"%".$cari."%") 
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%") 
    //     ->orWhere('waktu_hapus','like',"%".$cari."%") 
    //     ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
    //     ->paginate();

    //     return view('ws.wstingkatposisi',['wstingkatposisi' =>$wstingkatposisi]);
    // }

}
