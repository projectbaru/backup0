<?php
namespace App\Http\Controllers;
// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\BR;
use App\Models\BRData;

class br_c extends Controller
{
    public function __construct()
    {
        $this->semesters = [
            [
                "name"  =>  "Semester 1",
                "quarters"  =>  [
                    [
                        "name" => "Q1",
                        "months" => ["Jan", "Feb", "Mar", "Q1"],
                    ],
                    [
                        "name" =>"Q2",
                        "months" => ["Apr", "Mei", "Jun", "Q2"],
                    ],
                ],
                "months" => ["Jan", "Feb", "Mar", "Q1", "Apr", "Mei", "Jun", "Q2"],
            ],
            [
                "name"  =>  "Semester 2",
                "quarters"  =>  [
                    [
                        "name" => "Q3",
                        "months" => ["Jul", "Aug", "Sept", "Q3"],
                    ],
                    [
                        "name" =>"Q4",
                        "months" => ["Oct", "Nov", "Des", "Q4"],
                    ],
                ],
                "months" => ["Jul", "Aug", "Sept", "Q3", "Oct", "Nov", "Des", "Q4"],
            ]
        ];

        $this->cost_centers = [
            ["label" => "1. Job Posting", "value" => "job_posting"],
            ["label" => "2. Training Karyawan Baru", "value" => "training_karyawan_baru"],
            ["label" => "3. Assessment", "value" => "assessment"],
            ["label" => "3.1 Promosi", "value" => "promosi"],
            ["label" => "3.2 New Join", "value" => "new_join"],
            ["label" => "4. Medical Check Up", "value" => "medical_check_up"],
            ["label" => "5. Job Fair", "value" => "job_fair"],
            ["label" => "6. Assesment Tools", "value" => "assesment_tools"],
        ];
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_budgeting_recruitment')->where('id_budgeting_recruitment', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $br = BR::all();
        foreach ($br as $b) {
            $b->job_posting = BRData::where('cost_center', 'job_posting')
                ->where('kode_budgeting_recruitment', $b->id_budgeting_recruitment)
                ->first();
            $b->training_karyawan_baru = BRData::where('cost_center', 'training_karyawan_baru')
                ->where('kode_budgeting_recruitment', $b->id_budgeting_recruitment)
                ->first();
        }

        return view('re.br.index', compact('br'));
    }

    public function tambah(Request $req) {
        
        $tahun_budget = null;
        $data['semesters'] =  $this->semesters;
        $data['cost_centers'] =  $this->cost_centers;
        if ($req->tahun_budget) {
            $tahun_budget = $req->tahun_budget ;
            $pdwk->where('tahun_budget ', $req->tahun_budget );
        }

        return view('re.br.tambah', $data, ['tahun_budget' => $tahun_budget]);
    }


    
    public function simpan(Request $req) {
        // return $req->all();
        DB::beginTransaction();

        try {
            // Insert BR
            $br_id = DB::table('rc_budgeting_recruitment')->insertGetId([
                'tahun_budgeting_recruitment' => $req->tahun_budgeting_recruitment,
                'nama_user_input' => Auth::user()->name,
                'tanggal_input' => date('Y-m-d H:i:s'),
            ]);

            DB::table('rc_budgeting_recruitment')
            ->where('id_budgeting_recruitment', $br_id)
            ->update([
                'kode_budgeting_recruitment' => $br_id,
            ]);

            // Insert BR Data
            foreach ($req->cost_centers as $key1 => $value1) {
                foreach ($value1["months"] as $key2 => $value2) {
                    $input = [
                        "kode_budgeting_recruitment" => $br_id,
                        "cost_center" => $key1,
                        "budgeting_year" => $value1["budgeting_year"],
                    ];
                    if (in_array($key2, ["Jan", "Feb", "Mar", "Q1", "Apr", "Mei", "Jun", "Q2"])) {
                        $input["semester_budgeting"] = "1";
                    } else if (in_array($key2, ["Jul", "Aug", "Sept", "Q3", "Oct", "Nov", "Des", "Q4"])) {
                        $input["semester_budgeting"] = "2";
                    }

                    if (substr($key2, 0, 1) != "Q") {
                        $input["bulan_budgeting"] = $key2;
                        $input["actual"] = $value2["actual"];
                        $input["actual_persen_month"] = $value2["actual_persen_month"];
                        $input["actual_persen_kum"] = $value2["actual_persen_kum"];
                    } else {
                        $input["quarter"] = $key2;
                        $input["actual_quarter"] = $value2["actual_quarter"];
                        $input["remaining_budget"] = $value2["remaining_budget"];
                        $input["actual_persen_kum_quarter"] = $value2["actual_persen_kum_quarter"];
                    }

                    DB::table('rc_data_budgeting_recruitment')->insert($input);
                }
            }

            DB::commit();
            return redirect('/list_br');
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
            return back();
        }
    }

    public function edit($id)
    {
        $data['semesters'] =  $this->semesters;
        $data['cost_centers'] =  $this->cost_centers;
        $br = BR::where('id_budgeting_recruitment', $id)->first();
        $brd = BRData::where('kode_budgeting_recruitment', $id)->get();
        $data['byear'] = BRData::select('cost_center', 'budgeting_year')->where('kode_budgeting_recruitment', $id)
            ->groupBy('cost_center', 'budgeting_year')->pluck('budgeting_year', 'cost_center');

        $br->cost_centers = collect($brd)
            ->groupBy('cost_center')
            ->map(function ($c) {  //cost center
                return collect($c)->groupBy('bulan_budgeting') //bulan
                ->map(function ($d) {
                    if (count($d) == 1) return $d[0];
                    else if (count($d) == 4) return collect($d)->groupBy('quarter') //quarter
                    ->map(function ($e) {
                        if (count($e) == 1) return $e[0];
                    });
                });
        });

        $data['br'] = $br;

        return view('re.br.edit', $data);
    }

    public function detail($id)
    {
        $data['semesters'] =  $this->semesters;
        $data['cost_centers'] =  $this->cost_centers;
        $br = BR::where('id_budgeting_recruitment', $id)->first();
        $brd = BRData::where('kode_budgeting_recruitment', $id)->get();
        $data['byear'] = BRData::select('cost_center', 'budgeting_year')->where('kode_budgeting_recruitment', $id)
            ->groupBy('cost_center', 'budgeting_year')->pluck('budgeting_year', 'cost_center');

        $br->cost_centers = collect($brd)
            ->groupBy('cost_center')
            ->map(function ($c) {  //cost center
                return collect($c)->groupBy('bulan_budgeting') //bulan
                ->map(function ($d) {
                    if (count($d) == 1) return $d[0];
                    else if (count($d) == 4) return collect($d)->groupBy('quarter') //quarter
                    ->map(function ($e) {
                        if (count($e) == 1) return $e[0];
                    });
                });
        });

        $data['br'] = $br;

        return view('re.br.detail', $data);
    }

    public function update(Request $req, $id) {
        // return $req->all();
        DB::beginTransaction();

        try {
            // Update BR
            DB::table('rc_budgeting_recruitment')
            ->where('id_budgeting_recruitment', $id)
            ->update([
                'tahun_budgeting_recruitment' => $req->tahun_budgeting_recruitment,
                'user_ubah' => Auth::user()->name,
                'waktu_ubah' => date('Y-m-d H:i:s'),
            ]);

            // Rollback BR Data
            DB::table('rc_data_budgeting_recruitment')
            ->where('kode_budgeting_recruitment', $id)
            ->delete();

            // Insert New BR Data
            foreach ($req->cost_centers as $key1 => $value1) {
                foreach ($value1["months"] as $key2 => $value2) {
                    $input = [
                        "kode_budgeting_recruitment" => $id,
                        "cost_center" => $key1,
                        "budgeting_year" => $value1["budgeting_year"],
                    ];
                    if (in_array($key2, ["Jan", "Feb", "Mar", "Q1", "Apr", "Mei", "Jun", "Q2"])) {
                        $input["semester_budgeting"] = "1";
                    } else if (in_array($key2, ["Jul", "Aug", "Sept", "Q3", "Oct", "Nov", "Des", "Q4"])) {
                        $input["semester_budgeting"] = "2";
                    }

                    if (substr($key2, 0, 1) != "Q") {
                        $input["bulan_budgeting"] = $key2;
                        $input["actual"] = $value2["actual"];
                        $input["actual_persen_month"] = $value2["actual_persen_month"];
                        $input["actual_persen_kum"] = $value2["actual_persen_kum"];
                    } else {
                        $input["quarter"] = $key2;
                        $input["actual_quarter"] = $value2["actual_quarter"];
                        $input["remaining_budget"] = $value2["remaining_budget"];
                        $input["actual_persen_kum_quarter"] = $value2["actual_persen_kum_quarter"];
                    }

                    DB::table('rc_data_budgeting_recruitment')->insert($input);
                }
            }

            DB::commit();
            return redirect('/list_br');
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
            return back();
        }
    }

    public function hapus($id)
    {
        DB::beginTransaction();

        try {
            // Delete BR Data
            DB::table('rc_data_budgeting_recruitment')
            ->where('kode_budgeting_recruitment', $id)
            ->delete();

            // Delete BR
            DB::table('rc_budgeting_recruitment')
            ->where('id_budgeting_recruitment', $id)
            ->delete();

            DB::commit();
            
            return $this->succesWitmessage("Berhasil hapus");
            
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorWithmessage("Gagal");

        }
    }

    public function preview(Request $request, $id)
    {
        $br = BR::where('id_budgeting_recruitment', $id)->first();
        $br_data = BRData::where('kode_budgeting_recruitment', $id)->get();
        $byear = BRData::select('cost_center', 'budgeting_year')->where('kode_budgeting_recruitment', $id)
        ->groupBy('cost_center', 'budgeting_year')->pluck('budgeting_year', 'cost_center');

        $data = collect($br_data)
            ->groupBy('cost_center')
            ->map(function ($c) {  //cost center
                return collect($c)->groupBy('bulan_budgeting') //bulan
                ->map(function ($d) {
                    if (count($d) == 1) return $d[0];
                    else if (count($d) == 4) return collect($d)->groupBy('quarter') //quarter
                    ->map(function ($e) {
                        if (count($e) == 1) return $e[0];
                    });
                });
        }); 

        $semesters =  $this->semesters;

        return view('re.br.preview', compact('br', 'byear', 'data', 'semesters'));
    }
}