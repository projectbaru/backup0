<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;



class p_c extends Controller
{
    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wsperusahaan")->where('id_perusahaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsperusahaan')->where('id_perusahaan', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

   
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function index()
    {
        // y
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_perusahaan', $select)) {
                array_push($select, 'id_perusahaan');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsperusahaan')->select($select);

            if ($hasPersonalTable->nama_perusahaan) {
                $query->where('nama_perusahaan', $hasPersonalTable->nama_perusahaan);
            }
            if ($hasPersonalTable->kode_perusahaan) {
                $query->where('kode_perusahaan', $hasPersonalTable->kode_perusahaan);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            // print_r($data);

            return view('ws.wsperusahaan.filterResult', $data);
        } else {
            $wsperusahaan = DB::table('wsperusahaan')->get();
            return view('ws.wsperusahaan.index', ['wsperusahaan' => $wsperusahaan]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        $wsperusahaan = DB::table('wsperusahaan')->get();
        return view('ws.wsperusahaan.index', ['wsperusahaan' => $wsperusahaan]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Perusahaan',
                'value' => 'id_perusahaan'
            ],
            [
                'text'  => 'Logo Perusahaan',
                'value' => 'perusahaan_logo'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Singkatan',
                'value' => 'singkatan'
            ],
            [
                'text'  => 'Visi Perusahaan',
                'value' => 'visi_perusahaan'
            ],
            [
                'text'  => 'Misi Perusahaan',
                'value' => 'misi_perusahaan'
            ],
            [
                'text'  => 'Nilai Perusahaan',
                'value' => 'nilai_perusahaan'
            ],
            [
                'text'  => 'Keterangan Perusahaan',
                'value' => 'keterangan_perusahaan'
            ],
            [
                'text'  => 'Tanggal Mulai Perusahaan',
                'value' => 'tanggal_mulai_perusahaan'
            ],
            [
                'text'  => 'Tanggal Selesai Perusahaan',
                'value' => 'tanggal_selesai_perusahaan'
            ],
            [
                'text'  => 'Jenis Perusahaan',
                'value' => 'jenis_perusahaan'
            ],
            [
                'text'  => 'Jenis Bisnis Perusahaan',
                'value' => 'jenis_bisnis_perusahaan'
            ],
            [
                'text'  => 'Jumlah Perusahaan',
                'value' => 'jumlah_perusahaan'
            ],
            [
                'text'  => 'Nomor NPWP Perusahaan',
                'value' => 'nomor_npwp_perusahaan'
            ],
            [
                'text'  => 'Lokasi Pajak',
                'value' => 'lokasi_pajak'
            ],
            [
                'text'  => 'NPP',
                'value' => 'npp'
            ],
            [
                'text'  => 'NPKP',
                'value' => 'npkp'
            ],
            [
                'text'  => 'ID Logo Perusahaan',
                'value' => 'id_logo_perusahaan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Jumlah Karyawan',
                'value' => 'jumlah_karyawan'
            ]
        ];


        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsperusahaan.filter', $data);
    }

    public function detail($id_perusahaan)
    {
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        $code_ = $this->get_number();
        return view('ws.wsperusahaan.detail', ['wsperusahaan' => $wsperusahaan,'random'=>$code_]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_perusahaan';
        } else {
            $select = ['id_perusahaan', 'nama_perusahaan', 'perusahaan_logo', 'visi_perusahaan', 'misi_perusahaan'];
        }

        $query = DB::table('wsperusahaan')->select($select);

        if ($request->nama_perusahaan) {
            $query->where('nama_perusahaan', $request->nama_perusahaan);
        }

        if ($request->kode_perusahaan) {
            $query->where('kode_perusahaan', $request->kode_perusahaan);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser')->insert([
            'kode_perusahaan'   => $request->kode_perusahaan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_p');
    }

    public function simpan(Request $request)
    {
        $logo_perusahaan = optional($request->file('logo_perusahaan'));
        $nama_file = time() . "." . $logo_perusahaan->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $logo_perusahaan->move($tujuan_upload, $nama_file);
        DB::table('wsperusahaan')->insert([
            'perusahaan_logo'   => $nama_file,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_perusahaan'   => $request->kode_perusahaan,
            'singkatan'         => $request->singkatan,
            'visi_perusahaan'   => $request->visi_perusahaan,
            'misi_perusahaan'   => $request->misi_perusahaan,
            'nilai_perusahaan'  => $request->nilai_perusahaan,
            'tanggal_mulai_perusahaan' => $request->tanggal_mulai_perusahaan,
            'tanggal_selesai_perusahaan' => $request->tanggal_selesai_perusahaan,
            'jenis_perusahaan' => $request->jenis_perusahaan,
            'jenis_bisnis_perusahaan' => $request->jenis_bisnis_perusahaan,
            'jumlah_karyawan' => $request->jumlah_karyawan,
            'nomor_npwp_perusahaan' => $request->nomor_npwp_perusahaan,
            'lokasi_pajak' => $request->lokasi_pajak,
            'npp' => $request->npp,
            'npkp' => $request->npkp,
            'id_logo_perusahaan' => $request->id_logo_perusahaan,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'keterangan' => $request->keterangan,
        ]);
        // $this->showAlert('success', "Berhasil salin data");


        DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_p');
    }

    function duplicate(Request $request)
    {
       

            DB::table('wsperusahaan')->insert([
                'perusahaan_logo'   => $request->perusahaan_logo,
                'nama_perusahaan'   => $request->nama_perusahaan,
                'kode_perusahaan'   => $request->kode_perusahaan,
                'singkatan'         => $request->singkatan,
                'visi_perusahaan'   => $request->visi_perusahaan,
                'misi_perusahaan'   => $request->misi_perusahaan,
                'nilai_perusahaan'  => $request->visi_perusahaan,
                'tanggal_mulai_perusahaan' => $request->tanggal_mulai_perusahaan,
                'tanggal_selesai_perusahaan' => $request->tanggal_selesai_perusahaan,
                'jenis_perusahaan' => $request->jenis_perusahaan,
                'jenis_bisnis_perusahaan' => $request->jenis_bisnis_perusahaan,
                'jumlah_karyawan' => $request->jumlah_karyawan,
                'nomor_npwp_perusahaan' => $request->nomor_npwp_perusahaan,
                'lokasi_pajak' => $request->lokasi_pajak,
                'npp' => $request->npp,
                'npkp' => $request->npkp,
                'id_logo_perusahaan' => $request->id_logo_perusahaan,
                'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
                'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
                'keterangan' => $request->keterangan,
                'pengguna_masuk' => Auth::user()->id

            ]);

            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nama_perusahaan'   => NULL,
                    'kode_perusahaan'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);

        return redirect('/list_p');
    }

    // public function simpan_duplikat(Request $request)
    // {
    // $logo_perusahaan = optional($request->file('logo_perusahaan'));
    // $nama_file=time().".".$logo_perusahaan->getClientOriginalName();
    // $tujuan_upload = 'data_file';
    // $logo_perusahaan->move($tujuan_upload,$nama_file);
    //     DB::table('wsperusahaan')->insert([
    //         'perusahaan_logo'   => $request->perusahaan_logo,
    //         'nama_perusahaan'   => $request->nama_perusahaan,
    //         'kode_perusahaan'   => $request->kode_perusahaan,
    //         'singkatan'         => $request->singkatan,
    //         'visi_perusahaan'   => $request->visi_perusahaan,
    //         'misi_perusahaan'   => $request->misi_perusahaan,
    //         'nilai_perusahaan'  => $request->visi_perusahaan,
    //         'tanggal_mulai_perusahaan' => $request->tanggal_mulai_perusahaan,
    //         'tanggal_selesai_perusahaan' => $request->tanggal_selesai_perusahaan,
    //         'jenis_perusahaan' => $request->jenis_perusahaan,
    //         'jenis_bisnis_perusahaan' => $request->jenis_bisnis_perusahaan,
    //         'jumlah_karyawan' => $request->jumlah_karyawan,
    //         'nomor_npwp_perusahaan' => $request->nomor_npwp_perusahaan,
    //         'lokasi_pajak' => $request->lokasi_pajak,
    //         'npp' => $request->npp,
    //         'npkp' => $request->npkp,
    //         'id_logo_perusahaan' => $request->id_logo_perusahaan,
    //         'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
    //         'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
    //         'keterangan' => $request->keterangan,
    //     ]);

    //     DB::table('wstampilantabledashboarduser')
    //         ->where('user_id', Auth::user()->id)
    //         ->update([
    //             'nama_perusahaan'   => NULL,
    //             'kode_perusahaan'   => NULL,
    //             'query_field'       => NULL,
    //             'query_operator'    => NULL,
    //             'query_value'       => NULL,
    //         ]);

    //     return redirect('/list_p');
    // }

    public function update(Request $request)
    {
        $data_update = array( 
            'id_perusahaan'         => $request->id_perusahaan,
            'nama_perusahaan'       => $request->nama_perusahaan,
            'kode_perusahaan'       => $request->kode_perusahaan,
            'singkatan'         => $request->singkatan,
            'visi_perusahaan'   => $request->visi_perusahaan,
            'misi_perusahaan'   => $request->misi_perusahaan,
            'nilai_perusahaan'  => $request->nilai_perusahaan,
            'keterangan_perusahaan' => $request->keterangan_perusahaan,
            'tanggal_mulai_perusahaan' => $request->tanggal_mulai_perusahaan,
            'tanggal_selesai_perusahaan' => $request->tanggal_selesai_perusahaan,
            'jenis_perusahaan' => $request->jenis_perusahaan,
            'jenis_bisnis_perusahaan' => $request->jenis_bisnis_perusahaan,
            'jumlah_karyawan' => $request->jumlah_karyawan,
            'nomor_npwp_perusahaan' => $request->nomor_npwp_perusahaan,
            'lokasi_pajak' => $request->lokasi_pajak,
            'npp' => $request->npp,
            'npkp' => $request->npkp,
            'id_logo_perusahaan' => $request->id_logo_perusahaan,
            'keterangan' => $request->keterangan,
            'status_rekaman' => $request->status_rekaman,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'pengguna_masuk' => $request->pengguna_masuk,
            'waktu_masuk' => $request->waktu_masuk,
            'pengguna_ubah' => $request->pengguna_ubah,
            'waktu_ubah' => $request->waktu_ubah,
            'pengguna_hapus' => $request->pengguna_hapus,
            'waktu_hapus' => $request->waktu_hapus,
        );
        if ($request->file('perusahaan_logo')) {
            $perusahaan_logo = optional($request->file('perusahaan_logo'));
            $nama_file = time() . "." . $perusahaan_logo->getClientOriginalName();
            $tujuan_upload = 'data_file';
            $perusahaan_logo->move($tujuan_upload, $nama_file);

            $data_update['perusahaan_logo'] = $nama_file;
        }
        DB::table('wsperusahaan')->where('id_perusahaan', $request->id_perusahaan)->update($data_update);

        return redirect('/list_p');
    }
    public function tambah()
    {
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number();
        // $datanum['randomId'] = $randomId;

        return view('ws.wsperusahaan.tambah', ['randomId' => $code_]);
    }

    public function get_number(){
        $max_id = DB::table('wsperusahaan')
        ->where('id_perusahaan', \DB::raw("(select max(`id_perusahaan`) from wsperusahaan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_perusahaan) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_perusahaan, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'P-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    

    public function edit($id_perusahaan)
    {
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        return view('ws.wsperusahaan.edit', ['wsperusahaan' => $wsperusahaan]);
    }
    public function edit_duplikat($id_perusahaan)
    {
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        return view('ws.wsperusahaan.edit', ['wsperusahaan' => $wsperusahaan]);
    }
    public function delete($id_perusahaan)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function delete_duplikat($id_perusahaan)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_perusahaan)
    {
        $del =  DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_wsperusahaan');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsperusahaan")->whereIn('id_perusahaan', explode(",", $ids))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }

}
