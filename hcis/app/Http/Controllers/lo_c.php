<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class lo_c extends Controller
{

    public function index(Request $request)
    {
        $hasPersonalTable = $this->get_tampilan(1);
        $data = $this->get_tampilan(1);
        $ptk_detail = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        $lk = DB::table('wslokasikerja')->select('id_lokasi_kerja', 'nama_lokasi_kerja', 'kode_lokasi_kerja')->get();
        $datalk['looks']=$lk;
        $labels = [
            'Jabatan',
            'Tanggal Upload',
            'Kategori Pekerjaan',
            'Minimal Pendidikan',
            'Status Lowongan',
            'Lokasi',
            'Peran Kerja',
            'Kode Kategori Lowongan Kerja',
            'Pengalaman Kerja',
            'Deskripsi Pekerjaan',
            'Tingkat',
            'Syarat Pengalaman',
            'Syarat Kemampuan',
            'Syarat Lainnya',
        ];

        $pelamar = DB::table('rc_data_lowongan_kerja');
        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;

        $list_no_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->pluck('no_dokumen')->toArray();
        $list_lokasi = DB::table('rc_formulir_permintaan_tenaga_kerja')->pluck('lokasi_kerja')->toArray();

        // if(!empty($request->no_dokumen_fptk)) {
        //     $pelamar->where('kode_cv_pelamar', 'like', "%{$request->no_dokumen_fptk}%");
        // }
        $data_pelamar = DB::table('rc_data_lowongan_kerja')->select("*")->get();
        if(!empty($request->cari)) {
            foreach($data_pelamar as $data_pelamars){
                foreach($data_pelamars as $col => $val){
                    $pelamar->orWhere($col, "like", "%{$request->cari}%");
                }
            }
        }
       
        $pelamar = $pelamar->get();

        // dd($pelamar);

        if ($data) {
            $data->filter = json_decode($data->filter);
            return view('re.lo.index', compact(['labels', 'data', 'datalk','ptk_detail', 'request', 'pelamar','list_no_fptk','list_lokasi']));
//            return view('re.dcp.index', $data);
        } else {
//            $wsalamat=DB::table('wsalamatperusahaan')->get();
            return view('re.lo.index', compact(['labels', 'datalk', 'ptk_detail', 'request' , 'pelamar','list_no_fptk','list_lokasi']));
        }
    }

    
    public function filterList(Request $request)
    {
        // DB::beginTransaction();

        if(empty($request->cv)) {
            return DB::table('reftable_dlo')->where('user_id', Auth::user()->id)->delete();
        }

        $update = DB::table('reftable_dlo')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active' => false,
            ]);

        $insert = DB::table('reftable_dlo')->insert([
            'jabatan_lowongan'   => $request->jabatan_lowongan,
            'kategori_pekerjaan'    => $request->kategori_pekerjaan,
            'lokasi_kerja'    => $request->lokasi_kerja,
            'pengalaman_kerja'      => $request->pengalaman_kerja,
            'tingkat_kerjaan'      => $request->tingkat_kerjaan,
//            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
//            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
//            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'filter'            => json_encode($request->cv),
            'user_id'           => Auth::user()->id,
        ]);

        

        // if ($update && $insert) {
            // DB::commit();
            return response()->json(['message' => 'Filter updated successfully', 'success' => TRUE], 200);
        // }
        // DB::rollBack();
        // return response()->json(['message' => 'Filter failed to update'], 500);
    }

    function get_tampilan($active)
    {
        return DB::table('reftable_dlo')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function hapus($id_lowongan_kerja)
    {
        $del =  DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

    }

    
    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function dashboard()
    {
        $list_dm = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','marketing')->get();
        $list_it = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','it')->get();
        $list_hrd = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','hrd')->get();
        $list_t = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','teknisi')->get();
        $list_a = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','akutansi')->get();
        $list_s = DB::table('rc_data_lowongan_kerja')->get();
        return view('re.lo.dashboard', ['list_dm'=>$list_dm,'list_it'=>$list_it,'list_hrd'=>$list_hrd,'list_t'=>$list_t,'list_a'=>$list_a,'list_s'=>$list_s]);
    }

    public function tambah()
    {
        $list_lo = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        return view('re.lo.tambah',['list_lo'=>$list_lo]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Lowongan Kerja',
                'value' => 'id_lowongan_kerja'
            ],
            [
                'text'  => 'Kode Kategori Lowongan Kerja',
                'value' => 'kode_kategori_lowongan_kerja'
            ],
            [
                'text'  => 'Nomor Dokumen FPTK',
                'value' => 'nomer_dokumen_fptk'
            ],
            [
                'text'  => 'Kategori Pekerjaan',
                'value' => 'kategori_pekerjaan'
            ],
            [
                'text'  => 'Jabatan Lowongan',
                'value' => 'jabatan_lowongan'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Tingkat Kerjaan',
                'value' => 'tingkat_kerjaan'
            ],
            [
                'text'  => 'Pengalaman Kerja',
                'value' => 'pengalaman_kerja'
            ],
            [
                'text'  => 'Status Kepegawaian',
                'value' => 'status_kepegawaian'
            ],
            [
                'text'  => 'Minimal Pendidikan',
                'value' => 'minimal_pendidikan'
            ],
            [
                'text'  => 'Peran Kerja',
                'value' => 'peran_kerja'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan',
                'value' => 'deskripsi_pekerjaan'
            ],
            [
                'text'  => 'Syarat Pengalaman',
                'value' => 'syarat_pengalaman'
            ],
            [
                'text'  => 'Syarat Kemampuan',
                'value' => 'syarat_kemampuan'
            ],
            [
                'text'  => 'Syarat Lainnya',
                'value' => 'syarat_lainnya'
            ],
            [
                'text'  => 'Jumlah Karyawan',
                'value' => 'jumlah_karyawan'
            ],
            [
                'text'  => 'Tanggal Upload',
                'value' => 'tanggal_upload'
            ],
            [
                'text'  => 'Status Lowongan',
                'value' => 'status_lowongan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('re.lo.filter', $data);
    }
   
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_alamat';
        } else {
            $select = ['id_lowongan_kerja', 'kode_kategori_lowongan_kerja', 'nomer_dokumen_fptk', 'kategori_pekerjaan'];
        }
        $query = DB::table('rc_data_lowongan_kerja')->select($select);

        if($request->kode_kategori_lowongan_kerja) {
            $query->where('kode_kategori_lowongan_kerja', $request->kode_kategori_lowongan_kerja);
        }

        if($request->nomer_dokumen_fptk) {
            $query->where('nomer_dokumen_fptk', $request->nomer_dokumen_fptk);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('reftable_dlo')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('reftable_dlo')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('reftable_dlo')->insert([
            'jabatan_lowongan'   => $request->kode_kategori_lowongan_kerja,
            'nomer_dokumen_fptk'   => $request->kode_alamat,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_loker');
    }

   
    public function simpan(Request $request)
    {
        DB::table('rc_data_lowongan_kerja')->insert([
            'kode_kategori_lowongan_kerja'   => $request->kode_kategori_lowongan_kerja,
            'nomer_dokumen_fptk'   => json_encode($request->nomer_dokumen_fptk),
            'kategori_pekerjaan' => $request->kategori_pekerjaan,
            'jabatan_lowongan' => $request->jabatan_lowongan,
            'lokasi_kerja'   => json_encode($request->lokasi_kerja),
            'tingkat_pekerjaan'   => $request->tingkat_kerjaan,
            'pengalaman_kerja'  => $request->pengalaman_kerja,
            'status_kepegawaian'  => $request->status_kepegawaian,
            'minimal_pendidikan' => $request->minimal_pendidikan,
            'peran_kerja' => $request->peran_kerja,
            'deskripsi_pekerjaan' => $request->deskripsi_pekerjaan,
            'syarat_pengalaman' => $request->syarat_pengalaman,
            'syarat_kemampuan' => $request->syarat_kemampuan,
            'syarat_lainnya' => $request->syarat_lainnya,
            'jumlah_karyawan' => $request->jumlah_karyawan,
            'tanggal_upload' => $request->tanggal_upload,
            'status_lowongan' => $request->status_lowongan,
            'status_rekaman' => $request->status_rekaman,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
        ]);

        DB::table('reftable_dlo')
            ->where('user_id', Auth::user()->id)
            ->update([
                'jabatan_lowongan'   => NULL,
                'kategori_pekerjaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_loker');
    }
    public function edit($id_lowongan_kerja)
    {
        $list_lokasi = DB::table('rc_formulir_permintaan_tenaga_kerja')->pluck('lokasi_kerja')->toArray();
        $list_no_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->pluck('no_dokumen')->toArray();
        $list_loker = DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        $ptk_detail = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();
        // dd($list_loker);
        return view('re.lo.edit_html', [
            'list_loker' => $list_loker,
            'list_no_fptk' => $list_no_fptk,
            'list_lokasi' => $list_lokasi,
            'ptk_detail' => $ptk_detail,
        ]);
    }

    public function editData($id_lowongan_kerja)
    {
        // $data = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->first();

        // return response()->json([
        //     'status' => true,
        //     "data" => $data,
        //     'msg' => "success"
        // ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);

        // $code_ = $this->get_number();
     

        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;
        $list_loker = DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        $ptk_detail = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();

        $returnHTML = view('re.lo.edit_html')
            // ->with('randomId', $code_)
            ->with('datalk', $datalk)
            ->with('list_loker', $list_loker)
            ->with('ptk_detail', $ptk_detail)
            ->render();

        return response()->json([
            'status' => true,
            "data" => $returnHTML,
            'msg' => "success"
        ], 200);
    }

    public function DetailData($id_lowongan_kerja)
    {
        // $data = DB::table('rc_data_cv_pelamar')->where('id_cv_pelamar', $id_cv_pelamar)->first();

        // return response()->json([
        //     'status' => true,
        //     "data" => $data,
        //     'msg' => "success"
        // ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);

        // $code_ = $this->get_number();
     

        $look = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $look;
        $list_loker = DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        $ptk_detail = DB::table('rc_formulir_permintaan_tenaga_kerja')->get();

        $returnHTML = view('re.lo.detail_html')
            // ->with('randomId', $code_)
            ->with('datalk', $datalk)
            ->with('list_loker', $list_loker)
            ->with('ptk_detail', $ptk_detail)
            ->render();

        return response()->json([
            'status' => true,
            "data" => $returnHTML,
            'msg' => "success"
        ], 200);
    }


    public function update(Request $request) {
        DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $request->id_lowongan_kerja)->update([
            'nomer_dokumen_fptk'   =>$request->nomer_dokumen_fptk,
            'kategori_pekerjaan'   =>$request->kategori_pekerjaan,
            'jabatan_lowongan'   =>$request->jabatan_lowongan,
            'lokasi_kerja'   =>$request->lokasi_kerja,
            'tingkat_pekerjaan'  =>$request->tingkat_kerjaan,
            'pengalaman_kerja'  =>$request->pengalaman_kerja,
            'status_kepegawaian'=>$request->status_kepegawaian,
            'minimal_pendidikan'=>$request->minimal_pendidikan,
            'peran_kerja'=>$request->peran_kerja,
            'deskripsi_pekerjaan'=>$request->deskripsi_pekerjaan,
            'syarat_pengalaman'=>$request->syarat_pengalaman,
            'syarat_kemampuan'=>$request->syarat_kemampuan,
            'syarat_lainnya'=>$request->syarat_lainnya,
            'jumlah_karyawan'=>$request->jumlah_karyawan,
            'tanggal_upload'=>$request->tanggal_upload,
            'status_lowongan'=>$request->status_lowongan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_loker');
    }


    public function list_data()
    {
        $list_dm = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','marketing')->get();
        $list_it = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','it')->get();
        $list_hrd = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','hrd')->get();
        $list_t = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','teknisi')->get();
        $list_a = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','akutansi')->get();
        $list_s = DB::table('rc_data_lowongan_kerja')->get();
        return view('re.lo.list_data_lowongan', ['list_dm'=>$list_dm,'list_it'=>$list_it,'list_hrd'=>$list_hrd,'list_t'=>$list_t,'list_a'=>$list_a,'list_s'=>$list_s]);
    }

    public function detail_lowongan($id_lowongan_kerja)
    {
        $list_dm = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','marketing')->get();
        $list_it = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','it')->get();
        $list_hrd = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','hrd')->get();
        $list_t = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','teknisi')->get();
        $list_a = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','akutansi')->get();
        $list_s = DB::table('rc_data_lowongan_kerja')->get();
        
        $list_loker = DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        return view('re.lo.detail_lowongan', ['list_loker' => $list_loker, 'list_dm'=>$list_dm,'list_it'=>$list_it,'list_hrd'=>$list_hrd,'list_t'=>$list_t,'list_a'=>$list_a,'list_s'=>$list_s ]);
    }

    public function detail($id_lowongan_kerja)
    {
        $list_loker = DB::table('rc_data_lowongan_kerja')->where('id_lowongan_kerja', $id_lowongan_kerja)->get();
        return view('re.lo.detail', ['list_loker' => $list_loker]);
    }

    public function list_detail(){
        return view('re.lo.detail_lowongan');
    }

    public function list_data_loker(){
        $list_dm = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','marketing')->get();
        $list_it = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','it')->get();
        $list_hrd = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','hrd')->get();
        $list_t = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','teknisi')->get();
        $list_a = DB::table('rc_data_lowongan_kerja')->where('kategori_pekerjaan','=','akutansi')->get();
        $list_s = DB::table('rc_data_lowongan_kerja')->get();
        return view('re.lo.list_data_lowongan', ['list_dm'=>$list_dm,'list_it'=>$list_it,'list_hrd'=>$list_hrd,'list_t'=>$list_t,'list_a'=>$list_a,'list_s'=>$list_s ]);
    }

}