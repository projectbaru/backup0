<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class kj_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wskelompokjabatan")->where('id_kelompok_jabatan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wskelompokjabatan')->where('id_kelompok_jabatan', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_kelompok_jabatan', $select)) {
                array_push($select, 'id_kelompok_jabatan');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wskelompokjabatan')->select($select);

            if($hasPersonalTable->kode_kelompok_jabatan) {
                $query->where('kode_kelompok_jabatan', $hasPersonalTable->kode_kelompok_jabatan);
            }
            if($hasPersonalTable->nama_kelompok_jabatan) {
                $query->where('nama_kelompok_jabatan', $hasPersonalTable->nama_kelompok_jabatan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('ws.wskelompokjabatan.filterResult', $data);
        } else {
            $wskelompokjabatan=DB::table('wskelompokjabatan')->get();
            return view('ws.wskelompokjabatan.index',['wskelompokjabatan'=>$wskelompokjabatan]);
        }
    }
    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_jabatan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_jabatan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wskelompokjabatan=DB::table('wskelompokjabatan')->get();
        return view('wskelompokjabatan',['wskelompokjabatan'=>$wskelompokjabatan]);
    }

    public function filter()
    {
        $fields = [
            // [
            //     'text'  => 'Id Kelompok Jabatan',
            //     'value' => 'id_kelompok_jabatan'
            // ],
            [
                'text'  => 'Kode Kelompok Jabatan',
                'value' => 'kode_kelompok_jabatan'
            ],
            [
                'text'  => 'Nama Kelompok Jabatan',
                'value' => 'nama_kelompok_jabatan'
            ],
            [
                'text'  => 'Deskripsi Kelompok Jabatan',
                'value' => 'deskripsi_kelompok_jabatan'
            ],
            [
                'text'  => 'Dari Golongan',
                'value' => 'dari_golongan'
            ],
            [
                'text'  => 'Sampai Golongan',
                'value' => 'sampai_golongan'
            ],
            [
                'text'  => 'Grup Jabatan',
                'value' => 'grup_jabatan'
            ],
            [
                'text'  => 'Tipe Jabatan',
                'value' => 'tipe_jabatan'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan',
                'value' => 'deskripsi_pekerjaan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wskelompokjabatan.filter', $data);
    }


    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_kj')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function detail($id_kelompok_jabatan){
        $wskelompokjabatan = DB::table('wskelompokjabatan')->where('id_kelompok_jabatan', $id_kelompok_jabatan)->get();
        return view('ws.wskelompokjabatan.detail',['wskelompokjabatan'=> $wskelompokjabatan]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_kelompok_jabatan';
        } else {
            $select = ['id_kelompok_jabatan', 'kode_kelompok_jabatan', 'nama_kelompok_jabatan', 'deskripsi_kelompok_jabatan'];
        }

        $query = DB::table('wskelompokjabatan')->select($select);

        if($request->kode_kelompok_jabatan) {
            $query->where('kode_kelompok_jabatan', $request->kode_kelompok_jabatan);
        }

        if($request->nama_kelompok_jabatan) {
            $query->where('nama_kelompok_jabatan', $request->nama_kelompok_jabatan);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kj')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_kj')->insert([
            'kode_kelompok_jabatan'   => $request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan'   => $request->nama_kelompok_jabatan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_kj');
    }
    public function simpan(Request $request){
        DB::table('wskelompokjabatan')->insert([
            'kode_kelompok_jabatan'   =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan'   =>$request->nama_kelompok_jabatan,
            'deskripsi_kelompok_jabatan'         =>$request->deskripsi_kelompok_jabatan,
            'dari_golongan'   =>$request->dari_golongan,
            'sampai_golongan'   =>$request->sampai_golongan,
            'grup_jabatan'  =>$request->grup_jabatan,
            'tipe_jabatan'=>$request->tipe_jabatan,
            'deskripsi_pekerjaan'=>$request->deskripsi_pekerjaan,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk'=>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        DB::table('wstampilantabledashboarduser_kj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_kelompok_jabatan'   => NULL,
                'nama_kelompok_jabatan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_kj');
    }

    public function update(Request $request) {
        DB::table('wskelompokjabatan')->where('id_kelompok_jabatan', $request->id_kelompok_jabatan)->update([
            'id_kelompok_jabatan'     =>$request->id_kelompok_jabatan,
            'kode_kelompok_jabatan'       =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan'       =>$request->nama_kelompok_jabatan,
            'deskripsi_kelompok_jabatan'       =>$request->deskripsi_kelompok_jabatan,
            'dari_golongan'         =>$request->dari_golongan,
            'sampai_golongan'   =>$request->sampai_golongan,
            'grup_jabatan'   =>$request->grup_jabatan,
            'tipe_jabatan'  =>$request->tipe_jabatan,
            'deskripsi_pekerjaan'=>$request->deskripsi_pekerjaan,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk'=>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah' =>$request->waktu_ubah,
            'pengguna_hapus' =>$request->pengguna_hapus,
            'waktu_hapus' =>$request->waktu_hapus,
        ]);
        return redirect('/list_kj');
    }
    public function tambah() {
        $look = DB::table('wsdeskripsipekerjaan')->select('id_deskripsi_pekerjaan','deskripsi_pekerjaan')->get();
        $datas['looks'] = $look;
        $lookng = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datang['looks'] = $lookng;
        $code_ = $this->get_number();

        return view('ws.wskelompokjabatan.tambah',['random'=>$code_,'datas'=>$datas,'datang'=>$datang]);
    }

    public function get_number(){
        $max_id = DB::table('wskelompokjabatan')
        ->where('id_kelompok_jabatan', \DB::raw("(select max(`id_kelompok_jabatan`) from wskelompokjabatan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_kelompok_jabatan) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_kelompok_jabatan, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'KJ-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
  

    public function edit($id_kelompok_jabatan) {
        $look = DB::table('wsdeskripsipekerjaan')->select('id_deskripsi_pekerjaan','deskripsi_pekerjaan')->get();
        $datas['looks'] = $look;
        $lookdg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datadg['looks'] = $lookdg;
        $looksg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datasg['looks'] = $looksg;
        $wskelompokjabatan = DB::table('wskelompokjabatan')->where('id_kelompok_jabatan', $id_kelompok_jabatan)->get();
        return view('ws.wskelompokjabatan.edit',['wskelompokjabatan'=> $wskelompokjabatan,'datas'=>$datas,'datadg'=>$datadg,'datasg'=>$datasg]);
    }
    public function delete($id_kelompok_jabatan) {
        $data=ListPerusahaan::drop();
        return redirect('list_kj');
    }
    public function hapus($id_kelompok_jabatan){
        $del = DB::table('wskelompokjabatan')->where('id_kelompok_jabatan', $id_kelompok_jabatan)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
    }
    // public function deleteAll($id_jabatan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kj')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_kj');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wskelompokjabatan")->whereIn('id_kelompok_jabatan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wskelompokjabatan = DB::table('wskelompokjabatan')
    //     ->where('kode_kelompok_jabatan','like',"%".$cari."%")
    //     ->orWhere('nama_kelompok_jabatan','like',"%".$cari."%")
    //     ->orWhere('deskripsi_kelompok_jabatan','like',"%".$cari."%")    
    //     ->orWhere('dari_golongan','like',"%".$cari."%")    
    //     ->orWhere('sampai_golongan','like',"%".$cari."%")    
    //     ->orWhere('grup_jabatan','like',"%".$cari."%")    
    //     ->orWhere('tipe_jabatan','like',"%".$cari."%")    
    //     ->orWhere('deskripsi_pekerjaan','like',"%".$cari."%")    
    //     ->orWhere('keterangan','like',"%".$cari."%")    
    //     ->orWhere('status_rekaman','like',"%".$cari."%")    
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")    
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%")     
    //     ->orWhere('waktu_masuk','like',"%".$cari."%") 
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%") 
    //     ->orWhere('waktu_ubah','like',"%".$cari."%") 
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%") 
    //     ->orWhere('waktu_hapus','like',"%".$cari."%") 
    //     ->paginate();

    //     return view('wskelompokjabatan',['wskelompokjabatan' =>$wskelompokjabatan]);
    // }

}
