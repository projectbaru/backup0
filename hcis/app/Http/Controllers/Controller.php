<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use RealRashid\SweetAlert\Facades\Alert;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function showAlert($type, $msg)
    {
        if ($type == 'success') {
            $ret =   Alert::success('Success!', $msg);
        }
        if ($type == 'error') {
            $ret =   Alert::error('Error!', $msg);
        }
        if ($type == 'warning') {
            $ret =   Alert::warning('Warning! ', $msg);
        }
        if ($type == 'info') {
            $ret =   Alert::info('Info!', $msg);
        }
        return  $ret;
    }

    public function succesWitdata($data)
    {
        return response()->json(
            [
                "code" => 1,
                "status" => true,
                "error" => null,
                "message" => "Success",
                'data' => $data,
            ],
            200
        );
    }

    public function succesWitmessage($msg)
    {
        return response()->json(
            [
                "code" => 1,
                "status" => true,
                "error" => null,
                "message" => $msg,
                'data' => null,
            ],
            200
        );
    }

    public function errorWithmessage($msg)
    {

        return response()->json(
            [
                "code" => 0,
                "status" => false,
                "error" => null,
                "message" => $msg,
                'data' => null,
            ],
            200
        );
    }
}
