<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Models\PDWK;
use App\Models\DWK;
use App\Models\Master_Kar;

class dwk_c extends Controller
{
    public $verifiedBy = [];
    public function __construct()
    {
        $this->verifiedBy = ['Karyawan A', 'Karyawan B', 'Karyawan C'];
    }


    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_data_wawancara_kandidat')->where('id_perusahaan', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }


    function get_tampilan($active)
    {
        return DB::table('reftable_dwk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }


    public function index_dwk()
    {
        $dwk = DB::table('rc_data_wawancara_kandidat')->get();
        $pdwk = DB::table('rc_penyiapan_data_wawancara_kandidat')->get();

        return view('re.dwk.index_dwk', ['dwk' => $dwk, 'pdwk' => $pdwk]);
    }

    public function index_pdwk(Request $req)
    {
        // $pdwk = DB::table('rc_penyiapan_data_wawancara_kandidat')

        // ->join('rc_data_wawancara_kandidat','rc_penyiapan_data_wawancara_kandidat.id_penyiapan_dwk', '=','rc_data_wawancara_kandidat.id_penyiapan_dwk')
        // ->select('rc_data_wawancara_kandidat.*','rc_penyiapan_data_wawancara_kandidat.*')
        // ->get();
        $tahun = null;
        $pdwk = PDWK::with('list_dwk');
        if ($req->tahun) {
            $tahun = $req->tahun;
            $pdwk->where('tahun', $req->tahun);
        }
        $pdwk = $pdwk->get();

        return view('re.dwk.index_pdwk', ['pdwk' => $pdwk, 'tahun' => $tahun]);
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('reftable_dwk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('reftable_dwk')
                ->where('user_id', Auth::user()->id);
        }

        $wsperusahaan = DB::table('wsperusahaan')->get();
        return view('wsperusahaan.index', ['wsperusahaan' => $wsperusahaan]);
    }

    public function detail($id_perusahaan)
    {
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        $code_ = $this->get_number();
        return view('wsperusahaan.detail', ['wsperusahaan' => $wsperusahaan, 'random' => $code_]);
    }

    public function detail_dwk($id_data_wawancara_kandidat)
    {

        $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        $data = DB::table('rc_data_wawancara_kandidat')->where('id_data_wawancara_kandidat', $id_data_wawancara_kandidat)->first();
        $jabatan = DB::table('wsjabatan')->select('id_jabatan', 'nama_jabatan')->get();
        $loker = DB::table('wslokasikerja')->select('id_lokasi_kerja', 'nama_lokasi_kerja')->get();
        $jadwal1 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 1)->first();
        $jadwal2 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 2)->first();
        $jadwal3 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 3)->first();
        $look_kp = DB::table('rc_process_recruitment')->select('kode_process_recruitment', 'kode_cv_pelamar', 'nama_kandidat')->get();
        $kprk['looks'] = $look_kp;
        
        return view('re.dwk.detail_dwk', ['kprk' => $kprk, 'data_kar'=>$data_kar], compact('data', 'jabatan', 'loker', 'jadwal1', 'jadwal2', 'jadwal3'));
        // $dwk = DB::table('rc_data_wawancara_kandidat')->where('id_data_wawancara_kandidat', $id_data_wawancara_kandidat)->first();
        // return view('re.dwk.detail_dwk', ['data' => $dwk]);
    }

    public function detail_pdwk($id_penyiapan_dwk)
    {
        $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        $pdwk = DB::table('rc_penyiapan_data_wawancara_kandidat')->where('id_penyiapan_dwk', $id_penyiapan_dwk)->first();
        $pdwk->preparedBy = json_decode($pdwk->disiapkan_oleh) ?? [];
        $pdwk->diperiksa_oleh = json_decode($pdwk->diperiksa_oleh) ?? [];      

        return view('re.dwk.detail_pdwk', ['data' => $pdwk, 'data_kar' => $data_kar]);
    }

    public function simpan_pdwk(Request $request)
    {
        DB::table('rc_penyiapan_data_wawancara_kandidat')->insert([
            'disiapkan_oleh'   => json_encode($request->disiapkan_oleh),
            'kode_penyiapan'   => $request->kode_penyiapan,
            'tahun'   => $request->tahun,
            'diperiksa_oleh'   => json_encode($request->diperiksa_oleh),
            'revisi'   => $request->revisi,
            'status_rekaman'   => $request->status_rekaman,
            'keterangan'   => $request->keterangan,
            'waktu_input'   => now(),
        ]);
        return redirect('/list_pdwk');
    }


    function duplicate(Request $request)
    {
        $id = $request->tg_id;
        if ($id) {
            $wsperusahaan = DB::table('wsperusahaan')
                ->where('id_perusahaan', $id)
                ->first();

            DB::table('wsperusahaan')->insert([
                'perusahaan_logo'   => $wsperusahaan->perusahaan_logo,
                'nama_perusahaan'   => $wsperusahaan->nama_perusahaan,
                'kode_perusahaan'   => $wsperusahaan->kode_perusahaan,
                'singkatan'         => $wsperusahaan->singkatan,
                'visi_perusahaan'   => $wsperusahaan->visi_perusahaan,
                'misi_perusahaan'   => $wsperusahaan->misi_perusahaan,
                'nilai_perusahaan'  => $wsperusahaan->visi_perusahaan,
                'tanggal_mulai_perusahaan' => $wsperusahaan->tanggal_mulai_perusahaan,
                'tanggal_selesai_perusahaan' => $wsperusahaan->tanggal_selesai_perusahaan,
                'jenis_perusahaan' => $wsperusahaan->jenis_perusahaan,
                'jenis_bisnis_perusahaan' => $wsperusahaan->jenis_bisnis_perusahaan,
                'jumlah_karyawan' => $wsperusahaan->jumlah_karyawan,
                'nomor_npwp_perusahaan' => $wsperusahaan->nomor_npwp_perusahaan,
                'lokasi_pajak' => $wsperusahaan->lokasi_pajak,
                'npp' => $wsperusahaan->npp,
                'npkp' => $wsperusahaan->npkp,
                'id_logo_perusahaan' => $wsperusahaan->id_logo_perusahaan,
                'tanggal_mulai_efektif' => $wsperusahaan->tanggal_mulai_efektif,
                'tanggal_selesai_efektif' => $wsperusahaan->tanggal_selesai_efektif,
                'keterangan' => $wsperusahaan->keterangan,
                'pengguna_masuk' => Auth::user()->id

            ]);

            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nama_perusahaan'   => NULL,
                    'kode_perusahaan'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);

            $this->showAlert('success', "Berhasil salin data");
            return redirect('/list_p');
        }

        $this->showAlert('error', "Data tidak ditemukan");
        return redirect('/list_p');
    }

    public function update_pdwk(Request $request)
    {
        DB::table('rc_penyiapan_data_wawancara_kandidat')->where('id_penyiapan_dwk', $request->id_penyiapan_dwk)->update([

            'kode_penyiapan' => $request->kode_penyiapan,
            'disiapkan_oleh' => json_encode($request->disiapkan_oleh),
            'tahun' => $request->tahun,
            'diperiksa_oleh' => json_encode($request->diperiksa_oleh),
            'keterangan' => $request->keterangan,
            'revisi' => $request->revisi
        ]);
        return redirect('/list_pdwk');
    }

    public function update_dwk(Request $req)
    {
        // $hk = $req->hasil_konfirmasi_2;
        // if ($req->hasil_konfirmasi_1) {
        //     $hk = $req->hasil_konfirmasi_1;
        // }

        $data_update = array(
            'id_penyiapan_dwk' => $req->pdwk_id,
            'kode_process_recruitment_kandidat' => $req->kode_process_recruitment_kandidat,
            'kode_cv_pelamar' => $req->kode_cv_pelamar,
            'nama_kandidat' => $req->nama_kandidat,
            'hasil_konfirmasi' => $req->hasil_konfirmasi,
            'jam_wawancara' => $req->jam_wawancara,
            'via' => $req->via,
            'tempat' => $req->tempat,
            'nama_atasan' => $req->nama_atasan,
            'date_on_join' => $req->date_on_join,
            'status_hasil_interview' => $req->status_hasil_interview,
            'jabatan' => $req->jabatan,
            'lokasi_kerja' =>  $req->lokasi_kerja,
            'keterangan' => $req->keterangan,


        );

        DB::table('rc_data_wawancara_kandidat')
            ->where('id_data_wawancara_kandidat', $req->id_data_wawancara_kandidat)
            ->update($data_update);

        DB::table('rc_data_jadwal_wawancara_kandidat')
            ->where('id_data_wawancara_kandidat', $req->id_data_wawancara_kandidat)
            ->where('urutan_interview', 1)
            ->update([
                'tanggal_interview' => $req->tanggal_interview_1,
                'nama_pewawancara' => $req->nama_pewawancara_1,
            ]);

        DB::table('rc_data_jadwal_wawancara_kandidat')
            ->where('id_data_wawancara_kandidat', $req->id_data_wawancara_kandidat)
            ->where('urutan_interview', 2)
            ->update([
                'tanggal_interview' => $req->tanggal_interview_2,
                'nama_pewawancara' => $req->nama_pewawancara_2,
            ]);

        DB::table('rc_data_jadwal_wawancara_kandidat')
            ->where('id_data_wawancara_kandidat', $req->id_data_wawancara_kandidat)
            ->where('urutan_interview', 3)
            ->update([
                'tanggal_interview' => $req->tanggal_interview_3,
                'nama_pewawancara' => $req->nama_pewawancara_3,
            ]);

        return redirect('/list_dwk/' . $req->pdwk_id);
    }

    public function tambah_pdwk()
    {
        // $randomId = random_int(2, 50);jab
        $code_ = $this->get_number();
        // $datanum['randomId'] = $randomId;
        $look = DB::table('wsjabatan')->select('id_jabatan', 'nama_jabatan')->get();
        $jab['looks'] = $look;
        $data_kar = Master_Kar::select('Nama_Karyawan', 'Kode_Karyawan', 'nik', 'Tipe_User')->orderBy("Nama_Karyawan", "asc")->where('active', '=', 'Y')->get();
        $data_k['looks'] = $data_kar;
        $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        return view('re.dwk.tambah_pdwk', ['random' => $code_, 'jab' => $jab, 'data_k' => $data_k, 'data_kar'=>$data_kar]);
    }

    public function getDwkPr(Request $request) {
        $selectedPosition = $request->input('kode_process_recruitment_kandidat');
        
        // Fetch steps based on the selected position
        $dwk =  DB::table('rc_detail_process_recruitment')->where('kode_process_rekruitment', $selectedPosition)->get();
        
        return response()->json(['dwk' => $dwk]);
    }

    public function tambah_dwk()
    {

        // $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        // $randomId = random_int(2, 50);
        $code_ = $this->get_number_dwk();
        // $datanum['randomId'] = $randomId;
        // $data = DB::table('rc_data_wawancara_kandidat')->where('id_data_wawancara_kandidat', $id_data_wawancara_kandidat)->first();
        $kprk = DB::table('rc_process_recruitment')->get();
        $kp_dwk = DB::table('rc_data_wawancara_kandidat')->get();
        $users = DB::table('users')->get();


        $look = DB::table('wsjabatan')->select('id_jabatan', 'nama_jabatan')->get();
        $jab['looks'] = $look;
        $look = DB::table('wslokasikerja')->select('id_lokasi_kerja', 'nama_lokasi_kerja')->get();
        $lk['looks'] = $look;

        return view(
            're.dwk.tambah_dwk', ['randomId' => $code_,'kprk' => $kprk, 'jab' => $jab, 'lk' => $lk, 'kp_dwk' => $kp_dwk, 'users' => $users]);
    }

    public function post_tambah_dwk(Request $req)
    {
        // dd($req->all());
        // $kp_dwk = DB::table('rc_penyiapan_data_wawancara_kandidat')->whereIdPenyiapanDataWawancaraKandidat($req->kode_penyiapan_data_wawancara_kandidat)->first();

        // $id = DB::table('rc_penyiapan_data_wawancara_kandidat')->insertGetId([
        //     // 'kode_penyiapan' => self::get_number(),
        //     'created_at' => date('Y-m-d H:i:s'),
        // ]);

        // DB::table('rc_data_jadwal_wawancara_kandidat')->insert([
        //     'id_data_wawancara_kandidat' => $req->pdwk_id,
        //     'urutan_interview' => 1,
        //     'tanggal_interview' => $req->tanggal_interview_1,
        //     'nama_pewawancara' => $req->nama_pewawancara_1,
        // ]);

        // DB::table('rc_data_jadwal_wawancara_kandidat')->insertGetId([
        //     'id_data_wawancara_kandidat' => $req->pdwk_id,
        //     'urutan_interview' => 2,
        //     'tanggal_interview' => $req->tanggal_interview_2,
        //     'nama_pewawancara' => $req->nama_pewawancara_2,
        // ]);

        // DB::table('rc_data_jadwal_wawancara_kandidat')->insertGetId([
        //     'id_data_wawancara_kandidat' => $req->pdwk_id,
        //     'urutan_interview' => 3,
        //     'tanggal_interview' => $req->tanggal_interview_3,
        //     'nama_pewawancara' => $req->nama_pewawancara_3,
        // ]);

        $hk = $req->hasil_konfirmasi_2;
        if ($req->hasil_konfirmasi_1) {
            $hk = $req->hasil_konfirmasi_1;
        }

        $maxid = DWK::max('id_data_wawancara_kandidat');
        $id_data_wawancara_kandidat = $maxid + 1;

        $id = DB::table('rc_data_wawancara_kandidat')->insert([
            'id_data_wawancara_kandidat' => $id_data_wawancara_kandidat,
            'id_penyiapan_dwk' => $req->pdwk_id,
            'kode_process_recruitment_kandidat' => $req->kode_process_recruitment_kandidat,
            'kode_data_wawancara_kandidat' => $req->kode_data_wawancara_kandidat,
            'kode_cv_pelamar' => $req->kode_cv_pelamar,
            'nama_kandidat' => $req->nama_kandidat,
            'hasil_konfirmasi' => $req->hasil_konfirmasi,
            'jam_wawancara' => $req->jam_wawancara,
            'via' => $req->via,
            'tempat' => $req->tempat,
            'nama_atasan' => $req->nama_atasan,
            'date_on_join' => $req->date_on_join,
            'jabatan' => $req->jabatan,
            'status_hasil_interview' => $req->status_hasil_interview,
            'lokasi_kerja' =>  $req->lokasi_kerja,
            'keterangan' => $req->keterangan,
        ]);

        $i = 1;
        foreach($req->interviews as $int) {
            DB::table('rc_data_jadwal_wawancara_kandidat')->insert([
                'id_data_wawancara_kandidat' => $id_data_wawancara_kandidat,
                'urutan_interview' => $i,
                'tanggal_interview' => $int['tanggal_interview'],
                'nama_pewawancara' => $int['nama_pewawancara'],
            ]);
            $i++;
        }

        return redirect('/list_dwk/' . $req->pdwk_id);
    }

    public function get_number()
    {
        $max_id = DB::table('rc_penyiapan_data_wawancara_kandidat')
            ->where('id_penyiapan_dwk', \DB::raw("(select max(`id_penyiapan_dwk`) from rc_penyiapan_data_wawancara_kandidat)"))
            ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->kode_penyiapan) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->kode_penyiapan, -3) + 1;
            }
        } else {
            $next_ = 1;
        }
        $code_ = 'KP-' . date('ym') . str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    public function get_number_dwk()
    {
        $max_id = DB::table('rc_data_wawancara_kandidat')
            ->where('id_data_wawancara_kandidat', \DB::raw("(select max('id_data_wawancara_kandidat') from rc_data_wawancara_kandidat)"))
            ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->kode_data_wawancara_kandidat) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->kode_data_wawancara_kandidat, -3) + 1;
            }
        } else {
            $next_ = 1;
        }
        $code_ = 'DWK-' . str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    public function edit_dwk($id_data_wawancara_kandidat)
    {
        // $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        $data = DB::table('rc_data_wawancara_kandidat')->where('id_data_wawancara_kandidat', $id_data_wawancara_kandidat)->first();
        $jadwal = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->get();
        $po = DB::table('rc_process_recruitment')
            ->where('kode_process_recruitment', $data->kode_process_recruitment_kandidat)
            ->first();
        $details = DB::table('rc_detail_process_recruitment')
            ->where('id_rc_process_recruitment', $po->id_process_recruitment)
            ->get('nama_step')->toArray();
        $users = DB::table('users')->get();

        $i = 0;
        foreach ($jadwal as $j) {
            if (count($details) > $i) {
                $j->nama_step = $details[$i]->nama_step;
            } else {
                $j->nama_step = "";
            }
            $i++;
        }

        $data->atasan = json_decode($data->nama_atasan) ?? [];
         
        $jabatan = DB::table('wsjabatan')->select('id_jabatan', 'nama_jabatan')->get();
        $loker = DB::table('wslokasikerja')->select('id_lokasi_kerja', 'nama_lokasi_kerja')->get();
        $jadwal1 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 1)->first();
        $jadwal2 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 2)->first();
        $jadwal3 = DB::table('rc_data_jadwal_wawancara_kandidat')->where('id_data_wawancara_kandidat', $data->id_data_wawancara_kandidat)->where('urutan_interview', 3)->first();
        $look_kp = DB::table('rc_process_recruitment')->select('kode_process_recruitment', 'kode_cv_pelamar', 'nama_kandidat', 'id_process_recruitment')->get();
        $kprk['looks'] = $look_kp;

        return view('re.dwk.edit_dwk', [
            'kprk' => $kprk,
            'data_kar' => [],
            'jadwal' => $jadwal,
        ], compact('data', 'jabatan', 'loker', 'jadwal1', 'jadwal2', 'jadwal3', 'users'));
    }


    public function edit_pdwk($id_penyiapan_dwk)
    {
        $data_kar = Master_Kar::select('Nama_Karyawan')->where('active', '=', 'Y')->get();
        $pdwk = DB::table('rc_penyiapan_data_wawancara_kandidat')->where('id_penyiapan_dwk', $id_penyiapan_dwk)->first();
        $pdwk->preparedBy = json_decode($pdwk->disiapkan_oleh) ?? [];
        $pdwk->diperiksa_oleh = json_decode($pdwk->diperiksa_oleh) ?? [];      

        return view('re.dwk.edit_pdwk', ['data' => $pdwk, 'data_kar' => $data_kar]);

        // $returnHTML = view('re.dwk.edit_pdwk')

        //     ->with('data_kar', $data_kar)
        //     ->with('pdwk', $pdwk)
        //     ->render();

        // return response()->json([
        //     'status' => true,
        //     "data" => $returnHTML,
        //     'msg' => "success"
        // ], 200);
    }

    public function filter_dua(Request $request)
    {
        $result = [];
        if ($request->tahun) {
            $result = DB::table('rc_penyiapan_data_wawancara_kandidat')->where('tahun', '=', $request->tahun)->get();
        } else {
            $result = DB::table('rc_penyiapan_data_wawancara_kandidat')->where('tahun', '=', $request->tahun)->get();
        }
        $ptk_detail = DB::table('rc_penyiapan_data_wawancara_kandidat')->get();
        return view('re.ptk.view', ['ptk_detail' => $ptk_detail, 'data' => $result]);
    }

    public function tambah_kan($id_penyiapan_dwk)
    {
        // $pdwk_kan = DB::table('rc_penyiapan_data_wawancara_kandidat')

        // ->join('rc_data_wawancara_kandidat','rc_penyiapan_data_wawancara_kandidat.id_penyiapan_dwk', '=','rc_data_wawancara_kandidat.id_penyiapan_dwk')
        // ->select('rc_data_wawancara_kandidat.*','rc_penyiapan_data_wawancara_kandidat.*')
        // ->get();

        $pdwk_kan = PDWK::where('id_penyiapan_dwk', $id_penyiapan_dwk)
            ->with('list_dwk')->first();

        return view('re.dwk.index_dwk', ['pdwk' => $pdwk_kan]);
    }

    public function delete($id_data_wawancara_kandidat)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function delete_duplikat($id_perusahaan)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }

    public function hapus_dwk($id_data_wawancara_kandidat)
    {
        $del =  DB::table('rc_data_wawancara_kandidat')->where('id_data_wawancara_kandidat', $id_data_wawancara_kandidat)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }
    public function hapus_pdwk($id_penyiapan_dwk)
    {
        $del =  DB::table('rc_penyiapan_data_wawancara_kandidat')->where('id_penyiapan_dwk', $id_penyiapan_dwk)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_wsperusahaan');
    }

    public function preview_dwk($id_penyiapan_dwk)
    {
        $pdwk = PDWK::where('id_penyiapan_dwk', $id_penyiapan_dwk)->first();
        $dwk = DWK::with(['list_jadwal_wawancara' => function ($q) {
            return $q->orderBy('urutan_interview', 'ASC')->get();
        }])
            ->where('id_penyiapan_dwk', $id_penyiapan_dwk)->get();

        return view('re.dwk.print_dwk', ['pdwk' => $pdwk, 'dwk' => $dwk]);
    }
}
