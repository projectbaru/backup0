<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class kc_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wskelascabang")->where('id_kelas_cabang', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wskelascabang')->where('id_kelas_cabang', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_kc')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_kelas_cabang', $select)) {
                array_push($select, 'id_kelas_cabang');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wskelascabang')->select($select);

            if($hasPersonalTable->kode_kelas_cabang) {
                $query->where('kode_kelas_cabang', $hasPersonalTable->kode_kelas_cabang);
            }
            if($hasPersonalTable->nama_kelas_cabang) {
                $query->where('nama_kelas_cabang', $hasPersonalTable->nama_kelas_cabang);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('ws.wskelascabang.filterResult', $data);
        } else {
            $wskelascabang=DB::table('wskelascabang')->get();
            return view('ws.wskelascabang.index',['wskelascabang'=>$wskelascabang]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kc')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kc')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wskelascabang=DB::table('wskelascabang')->get();
        return view('ws.wskelascabang',['wskelascabang'=>$wskelascabang]);
    }

    public function filter()
    {
        $fields = [
            // [
            //     'text'  => 'Id Kantor',
            //     'value' => 'id_kantor'
            // ],
            [
                'text'  => 'Kode Kelas Cabang',
                'value' => 'kode_kelas_cabang'
            ],
            [
                'text'  => 'Nama Kelas Cabang',
                'value' => 'nama_kelas_cabang'
            ],
            [
                'text'  => 'Area Kelas Cabang',
                'value' => 'area_kelas_cabang'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
                   ];

                   $operators = [
                    '=', '<>', '%LIKE%'
                ];
        
                $filters = false;
                //uy
                $temp = $this->get_tampilan(1);
                // $blok = count($fields);
                $blok = count($fields);
                $test = $fields;
        
                if ($temp) {
                    $filters = true;
                    $temp->query_field = json_decode($temp->query_field, true);
                    $temp->query_operator = json_decode($temp->query_operator, true);
                    $temp->query_value = json_decode($temp->query_value, true);
                    $temp->select = json_decode($temp->select, true);
        
                    for ($f = 0; $f < $blok; $f++) {
                        $style = "display:block;";
                        foreach ($temp->select as $key) {
                            if ($key == $test[$f]['value']) {
                                $style = "display:none;";
                            }
                        }
                        $test[$f]['style'] = $style;
                    }
                } else { 
                    foreach($test as $k => $v){
                        $test[$k]['style']="display:'block';";
        
                    }
        
                }
        
               
        
                $data = [
                    'fields'    => $fields,
                    'operators' => $operators,
                    'temp' => $temp,
                    'filters' => $filters,
                    'damn' => $test
                ];
        
                // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
                // $dataitems = [
                //     'items' => $items
                // ];
        
                return view('ws.wskelascabang.filter', $data);
            }

    public function detail($id_kelas_cabang){
        $wskelascabang = DB::table('wskelascabang')->where('id_kelas_cabang', $id_kelas_cabang)->get();
        return view('ws.wskelascabang.detail',['wskelascabang'=> $wskelascabang]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_kelas_cabang';
        } else {
            $select = ['id_kelas_cabang', 'kode_kelas_cabang', 'nama_kelas_cabang', 'area_kelas_cabang'];
        }

        $query = DB::table('wskelascabang')->select($select);

        if($request->kode_kelas_cabang) {
            $query->where('kode_kelas_cabang', $request->kode_kelas_cabang);
        }

        if($request->nama_kelas_cabang) {
            $query->where('nama_kelas_cabang', $request->nama_kelas_cabang);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kc')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kc')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_kc')->insert([
            'kode_kelas_cabang'   => $request->kode_kelas_cabang,
            'nama_kelas_cabang'   => $request->nama_kelas_cabang,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_kc');
    }
    public function simpan(Request $request){
        DB::table('wskelascabang')->insert([
            'kode_kelas_cabang'   =>$request->kode_kelas_cabang,
            'nama_kelas_cabang'   =>$request->nama_kelas_cabang,
            'area_kelas_cabang'         =>$request->area_kelas_cabang,
            'keterangan'            =>$request->keterangan,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'  =>$request->tanggal_selesai_efektif,
            
        ]);

        DB::table('wstampilantabledashboarduser_kc')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_kelas_cabang'   => NULL,
                'nama_kelas_cabang'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_kc');
    }

    public function update(Request $request) {
        DB::table('wskelascabang')->where('id_kelas_cabang', $request->id_kelas_cabang)->update([
            'id_kelas_cabang'   =>$request->id_kelas_cabang,
            'kode_kelas_cabang' =>$request->kode_kelas_cabang,
            'nama_kelas_cabang' =>$request->nama_kelas_cabang,
            'area_kelas_cabang'       =>$request->area_kelas_cabang,
            'keterangan'       =>$request->keterangan,
            'status_rekaman'         =>$request->status_rekaman,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
            'pengguna_masuk'  =>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        return redirect('/list_kc');
    }
    public function tambah() {
        $look = DB::table('wskelascabang')->select('id_kelas_cabang','area_kelas_cabang')->get();
        $dataakc['looks'] = $look;
        $code_ = $this->get_number();

        return view('ws.wskelascabang.tambah',['random'=>$code_,'dataakc'=>$dataakc]);
    }

    public function get_number(){
        $max_id = DB::table('wskelascabang')
        ->where('id_kelas_cabang', \DB::raw("(select max(`id_kelas_cabang`) from wskelascabang)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_kelas_cabang) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_kelas_cabang, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'KC-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    
    public function edit($id_kelas_cabang) {
        $look = DB::table('wskelascabang')->select('id_kelas_cabang','area_kelas_cabang')->get();
        $dataakc['looks'] = $look;
        $wskelascabang = DB::table('wskelascabang')->where('id_kelas_cabang', $id_kelas_cabang)->get();
        return view('ws.wskelascabang.edit',['wskelascabang'=> $wskelascabang, 'dataakc' => $dataakc]);
    }
    public function delete($id_perusahaan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_kelas_cabang){
        $del = DB::table('wskelascabang')->where('id_kelas_cabang', $id_kelas_cabang)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_kc');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_kc')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_kc')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_kc');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wskelascabang")->whereIn('id_kelas_cabang',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wskelascabang = DB::table('wskelascabang')
    //     ->where('kode_kelas_cabang','like',"%".$cari."%")
    //     ->orWhere('nama_kelas_cabang','like',"%".$cari."%")
    //     ->orWhere('area_kelas_cabang','like',"%".$cari."%")    
    //     ->orWhere('keterangan','like',"%".$cari."%")    
    //     ->orWhere('status_rekaman','like',"%".$cari."%")    
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")    
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")    
    //     ->paginate();

    //     return view('ws.wskelascabang',['wskelascabang' =>$wskelascabang]);
    // }

}
