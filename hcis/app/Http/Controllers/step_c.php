<?php

namespace App\Http\Controllers;

use App\Models\DetailStepProcessRecruitment;
use App\Models\StepProcessRecruitment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class step_c extends Controller
{
    public function multiDelete(Request $request)
    {
        DB::beginTransaction();
        $deleteParent = DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $request->id)->delete();
        if ($deleteParent) {
            $deleteChild = DB::table('rc_detail_step_process_recruitment')->where('kode_step_process_recruitment', $request->id)->delete();
        }

        if ($deleteParent && $deleteChild) {
            DB::commit();
            return response()->json([
                'status' => true,
                "data" => null,
                'msg' => "success delete"
            ], 200);
        }

        DB::rollBack();
        return response()->json([
            'status' => false,
            "data" => null,
            'msg' => "delete failed"
        ], 500);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $step = StepProcessRecruitment::get();
        foreach ($step as $key => $value) {
            $step[$key]['details'] = DetailStepProcessRecruitment::where('kode_step_process_recruitment', $value->id_step_process_recruitment)->get();
        }
        return view('re.step.index', ['step' => $step]);
    }

    public function edit($id_step_process_recruitment)
    {
        // $detail_des = DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $id_step_process_recruitment)->get()[0];

        $data = StepProcessRecruitment::with('details')->where('id_step_process_recruitment', $id_step_process_recruitment)->first();
        $detail_step    = DB::table('rc_detail_step_process_recruitment')
            ->where('kode_step_process_recruitment', $id_step_process_recruitment)
            ->get();
        return view('re.step.edit', ['data' => $data, 'detail_step' => $detail_step]);
    }

    public function detail($id_step_process_recruitment)
    {
        // $detail_des = DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $id_step_process_recruitment)->get()[0];

        $data = StepProcessRecruitment::with('details')->where('id_step_process_recruitment', $id_step_process_recruitment)->first();
        $detail_step    = DB::table('rc_detail_step_process_recruitment')
            ->where('kode_step_process_recruitment', $id_step_process_recruitment)
            ->get();
        return view('re.step.detail', ['data' => $data, 'detail_step' => $detail_step]);
    }

    public function update(Request $request)
{
    // dd($request->all());
    $id = $request->kode_step_process_recruitment;

    DB::beginTransaction();
    $updateDataId = DB::table('rc_step_process_recruitment')
        ->where('id_step_process_recruitment', $request->id_step_process_recruitment)
        ->update([
            'nama_jabatan' => $request->nama_jabatan
        ]);

    if (is_array($request->id_detail_step_process_recruitment) && count($request->id_detail_step_process_recruitment) > 0) {
        $id_detail_step_process_recruitment = $request->id_detail_step_process_recruitment;
        $massiveDelete = DB::table('rc_detail_step_process_recruitment')
            ->where('kode_step_process_recruitment', $request->id_step_process_recruitment)
            ->delete();

        for ($i = 0; $i < count($request->nama_step); $i++) {
            $checkNoUrut = DB::table('rc_detail_step_process_recruitment')
                ->where('no_urut', $request->no_urut[$i])
                ->where('kode_step_process_recruitment', $request->id_step_process_recruitment)
                ->first();

            if ($checkNoUrut && $checkNoUrut->id_detail_step_process_recruitment != $request->id_detail_step_process_recruitment[$i]) {
                DB::rollBack();
                return redirect()->route('ubah_jpr', $request->id_step_process_recruitment)->with('error', 'No Urut sudah ada!');
            }

            $insertDetail = DB::table('rc_detail_step_process_recruitment')
                ->insert([
                    'kode_step_process_recruitment' => $request->id_step_process_recruitment,
                    'nama_step'       => $request->nama_step[$i],
                    'nama_posisi'                     => $request->nama_jabatan,
                    'keterangan'      => $request->keterangan[$i],
                    'no_urut'         => $request->no_urut[$i]
                ]);
        }
    }

    DB::commit();

    if (is_array($request->kualifikasi_jabatan) && count($request->kualifikasi_jabatan) > 0) {
        for ($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
            DB::table('rc_detail_step_process_recruitment')
                ->insert([
                    'wsdeskripsipekerjaan_id'   => $id,
                    'nama_step'       => $request->nama_step[$i],
                    'keterangan'     => $request->keterangan[$i],
                    'no_urut'          => $request->no_urut[$i]
                ]);
        }
    }
    return redirect('/list_step');
}


    public function tambah()
    {
        return view('re.step.tambah');
    }

    public function simpan(Request $request)
    {
        //validation Request
        $validator = Validator::make(
            $request->all(),
            [
                'nama_jabatan' => 'required|unique:rc_step_process_recruitment|max:255',
            ],
            [
                'required' => ' :attribute harus di isi.',
                'unique' => ' :attribute sudah ada'
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
            exit();
        }
        DB::beginTransaction();
        try {
            $insertDataId = DB::table('rc_step_process_recruitment')->insertGetId([
                'nama_jabatan' => $request->nama_jabatan
            ]);
            if ($insertDataId) {
                //$insertedStepProcess = DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $insertDataId)->first();
                if (count($request->keterangan) > 0) {
                    for ($i = 0; $i < count($request->keterangan); $i++) {
                        $checkNoUrut = DB::table('rc_detail_step_process_recruitment')
                            ->where('no_urut', $request->no_urut[$i])
                            ->where('kode_step_process_recruitment', $insertDataId)
                            ->first();

                        if ($checkNoUrut) {
                            DB::rollBack();
                            return redirect()->route('tambah_step', ['error' => 'No Urut sudah ada!']);
                        }

                        $insertDetail = DB::table('rc_detail_step_process_recruitment')->insert([
                            'kode_step_process_recruitment' => $insertDataId,
                            'nama_posisi'                     => $request->nama_jabatan,
                            'nama_step'                     => $request->nama_step[$i],
                            'keterangan'                    => $request->keterangan[$i],
                            'no_urut'                       => $request->no_urut[$i]
                        ]);
                    }
                }
            }
            DB::commit();
            return redirect('/list_step');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('tambah_step');
        }
    }




    public function hapus($id_step_process_recruitment)
    {
        $del =  DB::table('rc_step_process_recruitment')->where('id_step_process_recruitment', $id_step_process_recruitment)->delete();
        if ($del) {
            DetailStepProcessRecruitment::where('kode_step_process_recruitment', $id_step_process_recruitment)->delete();
            return $this->succesWitmessage("Berhasil hapus");
        }
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }

    public function hapusDetail($id_detail_step_process_recruitment)
{
    try {
        // Hapus data berdasarkan id_detail_step_process_recruitment
        DetailStepProcessRecruitment::where('id_detail_step_process_recruitment', $id_detail_step_process_recruitment)->delete();

        return response()->json(['message' => 'Data berhasil dihapus']);
    } catch (\Exception $e) {
        return response()->json(['message' => 'Terjadi kesalahan saat menghapus data'], 500);
    }
}

}
