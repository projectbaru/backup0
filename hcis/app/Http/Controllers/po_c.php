<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class po_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsposisi")->where('id_posisi', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsposisi')->where('id_posisi', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_posisi', $select)) {
                array_push($select, 'id_posisi');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsposisi')->select($select);

            if($hasPersonalTable->kode_posisi) {
                $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
            }
            if($hasPersonalTable->nama_posisi) {
                $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('ws.wsposisi.filterResult', $data);
        } else {
            $wsposisi=DB::table('wsposisi')->get();
            return view('ws.wsposisi.index',['wsposisi'=>$wsposisi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_po')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsposisi=DB::table('wsposisi')->get();
        return view('ws.wsposisi',['wsposisi'=>$wsposisi]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Posisi',
                'value' => 'id_posisi'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'Kode MPP',
                'value' => 'kode_mpp'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Detail Tingkat Posisi',
                'value' => 'detail_tingkat_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Detail Tingkat Posisi',
                'value' => 'detail_tingkat_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan Posisi',
                'value' => 'deskripsi_pekerjaan_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan Posisi',
                'value' => 'deskripsi_pekerjaan_posisi'
            ],
            [
                'text'  => 'Kode Posisi Atasan',
                'value' => 'kode_posisi_atasan'
            ],
            [
                'text'  => 'Nama Posisi Atasan',
                'value' => 'nama_posisi_atasan'
            ],
            [
                'text'  => 'Tipe Area',
                'value' => 'tipe_area'
            ],
            [
                'text'  => 'Dari Golongan',
                'value' => 'dari_golongan'
            ],
            [
                'text'  => 'Sampai Golongan',
                'value' => 'sampai_golongan'
            ],
            [
                'text'  => 'Posisi Aktif',
                'value' => 'posisi_aktif'
            ],
            [
                'text'  => 'Komisi',
                'value' => 'komisi'
            ],
            [
                'text'  => 'Kepala Fungsional',
                'value' => 'kepala_fungsional'
            ],
            [
                'text'  => 'Flag Operasional',
                'value' => 'flag_operasional'
            ],
            [
                'text'  => 'Status Posisi',
                'value' => 'status_posisi'
            ],
            [
                'text'  => 'Nomor Surat',
                'value' => 'nomor_surat'
            ],
            [
                'text'  => 'Jumlah Karyawan Posisi Ini',
                'value' => 'jumlah_karyawan_dengan_posisi_ini'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsposisi.filter', $data);
    }

    public function get_tampilan_posisi($id_posisi){
        $wsposisi = DB::table('wstingkatposisi')->where('id_tingkat_posisi', $id_tingkat_posisi)->get();
        return view('ws.wsposisi.detail',['wsposisi'=> $wsposisi]);
    }

    public function detail($id_posisi){
        $wsposisi = DB::table('wsposisi')->where('id_posisi', $id_posisi)->get();
        return view('ws.wsposisi.detail',['wsposisi'=> $wsposisi]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_posisi';
        } else {
            $select = ['id_posisi', 'kode_posisi', 'nama_posisi', 'nama_jabatan'];
        }

        $query = DB::table('wsposisi')->select($select);

        if($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }

        if($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_po')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_po')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_po')->insert([
            'kode_posisi'   => $request->kode_posisi,
            'nama_posisi'   => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_po');
    }
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_po')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }
    public function simpan(Request $request){
 
        DB::table('wsposisi')->insert([
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_posisi'       =>$request->kode_posisi,
            'nama_posisi'       =>$request->nama_posisi,
            'kode_mpp'         =>$request->kode_mpp,
            'tingkat_posisi'   =>$request->tingkat_posisi,
            'detail_tingkat_posisi'   =>$request->detail_tingkat_posisi,
            'nama_jabatan'  =>$request->nama_jabatan,
            'nama_organisasi'=>$request->nama_organisasi,
            'tingkat_organisasi'=>$request->tingkat_organisasi,
            'lokasi_kerja' => $request->lokasi_kerja,
            'nama_grup_lokasi_kerja' => $request->nama_grup_lokasi_kerja,
            'tipe_posisi'=>$request->tipe_posisi,
            'deskripsi_pekerjaan_posisi'=>$request->deskripsi_pekerjaan_posisi,
            'nama_posisi_atasan'=>$request->nama_posisi_atasan,
            'dari_golongan'=>$request->dari_golongan,
            'sampai_golongan'=>$request->sampai_golongan,
            'tipe_area'=>$request->tipe_area,
            'posisi_aktif' =>$request->posisi_aktif,
            'komisi' =>$request->komisi,
            'kepala_fungsional' =>$request->kepala_fungsional,
            'flag_operasional'=>$request->flag_operasional,
            'status_posisi'=>$request->status_posisi,
            'nomor_surat'=>$request->nomor_surat,
            'jumlah_karyawan_dengan_posisi_ini'=>$request->jumlah_karyawan_dengan_posisi_ini,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_posisi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_posisi'   => NULL,
                'nama_posisi'   => NULL,
                'query_field'   => NULL,
                'query_operator'=> NULL,
                'query_value'   => NULL,
            ]);

        return redirect('/list_po');
    }

    public function update(Request $request) {
        DB::table('wsposisi')->where('id_posisi', $request->id_posisi)->update([
            'id_posisi'         =>$request->id_posisi,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_posisi'       =>$request->kode_posisi,
            'nama_posisi'           =>$request->nama_posisi,
            'kode_mpp'                =>$request->kode_mpp,
            'tingkat_posisi'            =>$request->tingkat_posisi,
            'detail_tingkat_posisi'     =>$request->detail_tingkat_posisi,
            'kode_jabatan'          =>$request->kode_jabatan,
            'nama_jabatan'          =>$request->nama_jabatan,
            'nama_organisasi'   =>$request->nama_organisasi,
            'tingkat_organisasi'   =>$request->tingkat_organisasi,
            'lokasi_kerja' => $request->lokasi_kerja,
            'nama_grup_lokasi_kerja' => $request->nama_grup_lokasi_kerja,
            'tipe_posisi'   =>$request->tipe_posisi,
            'deskripsi_pekerjaan_posisi'   =>$request->deskripsi_pekerjaan_posisi,
            'kode_posisi_atasan'   =>$request->kode_posisi_atasan,
            'nama_posisi_atasan'   =>$request->nama_posisi_atasan,
            'tipe_area'   =>$request->tipe_area,
            'dari_golongan'   =>$request->dari_golongan,
            'sampai_golongan'   =>$request->sampai_golongan,
            'posisi_aktif'   =>$request->posisi_aktif,
            'komisi'   =>$request->komisi,
            'kepala_fungsional'   =>$request->kepala_fungsional,
            'flag_operasional'   =>$request->flag_operasional,
            'status_posisi'   =>$request->status_posisi,
            'nomor_surat'   =>$request->nomor_surat,
            'jumlah_karyawan_dengan_posisi_ini'   =>$request->jumlah_karyawan_dengan_posisi_ini,
            'keterangan'   =>$request->keterangan,
            'status_rekaman'   =>$request->status_rekaman,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_po');
    }
    public function tambah() {
        $lookp = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $datap['looks'] = $lookp;
        $lookpo = DB::table('wsposisi')->select('id_posisi','kode_posisi','tingkat_posisi','nama_posisi','nama_posisi_atasan','nama_jabatan')->get();
        $datapo['looks'] = $lookpo;
        $looklk = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $looklk;
        
        $lookglk = DB::table('wsgruplokasikerja')->select('kode_grup_lokasi_kerja','nama_grup_lokasi_kerja')->get();
        $dataglk['looks'] = $lookglk;
        $looktp = DB::table('wstingkatposisi')->select('id_tingkat_posisi','urutan_tingkat','nama_tingkat_posisi')->get();
        $datatp['looks'] = $looktp;
        $lookj = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dataj['looks'] = $lookj;
        $looko = DB::table('wsorganisasi')->select('id_organisasi','nama_organisasi','tingkat_organisasi')->get();
        $datao['looks'] = $looko;
        $lookg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datag['looks'] = $lookg;
        $lookto = DB::table('wstingkatorganisasi')->select('id_tingkat_organisasi','urutan_tingkat')->get();
        $datato['looks'] = $lookto;
        $code_=$this->get_number();
        return view('ws.wsposisi.tambah',['random'=>$code_,'dataglk'=>$dataglk,'datap'=>$datap,'datapo'=>$datapo,'dataj'=>$dataj,'datao'=>$datao,'datato'=>$datato,'datag'=>$datag,'datatp'=>$datatp,'datalk'=>$datalk]);
    }
    


    public function get_number(){
        $max_id = DB::table('wsposisi')
        ->where('id_posisi', \DB::raw("(select max(`id_posisi`) from wsposisi)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_posisi) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_posisi, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'PO-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    


    public function edit($id_posisi) {
        $lookp = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $datap['looks'] = $lookp;
        $looktp = DB::table('wstingkatposisi')->select('id_tingkat_posisi','urutan_tingkat','nama_tingkat_posisi')->get();
        $datatp['looks'] = $looktp;
        $looktg = DB::table('wstingkatgolongan')->select('id_tingkat_golongan','nama_perusahaan','kode_tingkat_golongan','urutan_tampilan')->get();
        $datatg['looks'] = $looktg;
        $lookj = DB::table('wsgruplokasikerja')->select('id_grup_lokasi_kerja','nama_grup_lokasi_kerja','lokasi_kerja')->get();
        $dataglk['looks'] = $lookj;
        $lookj = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dataj['looks'] = $lookj;
        $lookdg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datadg['looks'] = $lookdg;
        $looko = DB::table('wsorganisasi')->select('id_organisasi','nama_organisasi','tingkat_organisasi')->get();
        $datao['looks'] = $looko;
        $lookto = DB::table('wstingkatorganisasi')->select('id_tingkat_organisasi','urutan_tingkat')->get();
        $datato['looks'] = $lookto;
        $looklk = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $looklk;
        $wsposisi = DB::table('wsposisi')->where('id_posisi', $id_posisi)->get();
        return view('ws.wsposisi.edit',['datalk'=>$datalk,'datatg'=>$datatg,'wsposisi'=> $wsposisi,'datadg'=>$datadg,'dataglk'=>$dataglk,'datatp'=>$datatp, 'datap'=>$datap, 'dataj'=>$dataj, 'datato'=>$datato, 'datao'=>$datao]);
    }
 
    public function hapus($id_posisi){
        $del =DB::table('wsposisi')->where('id_posisi', $id_posisi)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_po');
    }
    // public function deleteAll($id_posisi){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_po')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_po')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_po');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsposisi")->whereIn('id_posisi',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
   
}
