<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class tpk_c extends Controller
{

    public function index()
    {
        // y
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_cv_pelamar', $select)) {
                array_push($select, 'id_cv_pelamar');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('rc_cv_data_pelamar')->select($select);

            if ($hasPersonalTable->nama_sekolah) {
                $query->where('nama_sekolah', $hasPersonalTable->nama_sekolah);
            }
            if ($hasPersonalTable->kode_cv_pelamar) {
                $query->where('kode_cv_pelamar', $hasPersonalTable->kode_cv_pelamar);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            // print_r($data);

            return view('rc_data_cv_pelamar.filterResult', $data);
        } else {
            $rc_data = DB::table('rc_data_cv_pelamar')->get();
            return view('rc_data_cv_pelamar.index', ['rc_data' => $rc_data]);
        }
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id CV Pelamar',
                'value' => 'id_cv_pelamar'
            ],
            [
                'text'  => 'Kode CV Pelamar',
                'value' => 'kode_cv_pelamar'
            ],
            [
                'text'  => 'Jabatan Lowongan',
                'value' => 'jabatan_lowongan'
            ],
            [
                'text'  => 'Tingkat Pekerjaan',
                'value' => 'tingkat_pekerjaan'
            ],
            [
                'text'  => 'lokasi_kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Resume CV',
                'value' => 'resume_cv'
            ],
            [
                'text'  => 'Nama Lengkap',
                'value' => 'nama_lengkap'
            ],
            [
                'text'  => 'Email',
                'value' => 'email'
            ],
            [
                'text'  => 'No HP',
                'value' => 'no_hp'
            ],
            [
                'text'  => 'Lokasi Pelamar',
                'value' => 'lokasi_pelamar'
            ],
            [
                'text'  => 'Pengalaman Kerja',
                'value' => 'pengalaman_kerja'
            ],
            [
                'text'  => 'Tahun Pengalaman Kerja',
                'value' => 'tahun_pengalaman_kerja'
            ],
            [
                'text'  => 'Bulan Pengalaman Kerja',
                'value' => 'bulan_pengalaman_kerja'
            ],
            [
                'text'  => 'Tanggal Lahir',
                'value' => 'tanggal_lahir'
            ],
            [
                'text'  => 'Jenis Kelamin',
                'value' => 'jenis_kelamin'
            ],
            [
                'text'  => 'Pendidikan Tertinggi',
                'value' => 'pendidikan_tertinggi'
            ],
            [
                'text'  => 'Nama Sekolah',
                'value' => 'nama_sekolah'
            ],
            [
                'text'  => 'Program Studi Jurusan',
                'value' => 'program_studi_jurusan'
            ],
            [
                'text'  => 'Tanggal Wisuda',
                'value' => 'tanggal_wisuda'
            ],
            [
                'text'  => 'Nilai Rata-Rata',
                'value' => 'nilai_rata_rata'
            ],
            [
                'text'  => 'Nilai Skala',
                'value' => 'nilai_skala'
            ],
            [
                'text'  => 'Informasi Lowongan',
                'value' => 'informasi_lowongan'
            ],
            [
                'text'  => 'Keterangan Informasi Pilihan',
                'value' => 'keterangan_informasi_pilihan'
            ],
            [
                'text'  => 'Pesan Pelamar',
                'value' => 'pesan_pelamar'
            ],
            [
                'text'  => 'Tanggal Melamar',
                'value' => 'tanggal_melamar'
            ],
            [
                'text'  => 'Status Lamaran',
                'value' => 'status_lamaran'
            ]
        ];


        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('rc_data_cv_pelamar.filter', $data);
    }

    function duplicate(Request $request)
    {
        $id = $request->tg_id;

        if ($id) {
            $wsperusahaan = DB::table('rc_data_cv_pelamar')
                ->where('id_cv_pelamar', $id)
                ->first();

            DB::table('data_cv_pelamar')->insert([
                'kode_cv_pelamar'   => $wsperusahaan->kode_cv_pelamar,
                'jabatan_lowongan'   => $wsperusahaan->jabatan_lowongan,
                'tingkat_pekerjaan'   => $wsperusahaan->tingkat_pekerjaan,
                'lokasi_kerja'         => $wsperusahaan->lokasi_kerja,
                'resume_cv'   => $wsperusahaan->resume_cv,
                'nama_lengkap'   => $wsperusahaan->nama_lengkap,
                'email'  => $wsperusahaan->email,
                'no_hp' => $wsperusahaan->no_hp,
                'lokasi_pelamar' => $wsperusahaan->lokasi_pelamar,
                'pengalaman_kerja' => $wsperusahaan->pengalaman_kerja,
                'tahun_pengalaman_kerja' => $wsperusahaan->tahun_pengalaman_kerja,
                'bulan_pengalaman_kerja' => $wsperusahaan->bulan_pengalaman_kerja,
                'tanggal_lahir' => $wsperusahaan->tanggal_lahir,
                'jenis_kelamin' => $wsperusahaan->jenis_kelamin,
                'pendidikan_tertinggi' => $wsperusahaan->pendidikan_tertinggi,
                'nama_sekolah' => $wsperusahaan->nama_sekolah,
                'program_studi_jurusan' => $wsperusahaan->program_studi_jurusan,
                'nama_sekolah' => $wsperusahaan->nama_sekolah,
                'program_studi_jurusan' => $wsperusahaan->program_studi_jurusan,
                'tanggal_wisuda' => $wsperusahaan->tanggal_wisuda,
                'nilai_rata_rata' => $wsperusahaan->nilai_rata_rata,
                'nilai_skala' => $wsperusahaan->nilai_skala,
                
                'pengguna_masuk' => Auth::user()->id

            ]);

            DB::table('rc_data_cv_pelamar')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nama_lengkap'   => NULL,
                    'kode_cv_pelamar'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);

            $this->showAlert('success', "Berhasil salin data");
            return redirect('/list_p');
        }

        $this->showAlert('error', "Data tidak ditemukan");
        return redirect('/list_dcp');
    }

}