<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class dp_c extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->debug = env('DEV');
    }

    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wsdeskripsipekerjaan")->where('id_deskripsi_pekerjaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function hapus($id_deskripsi_pekerjaan)
    {
        //   y
        $del = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->delete();
        // return redirect('/list_dp');
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
    }

    // public function index(Request $request)
    // {
    //     $look = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->select('id', 'deskripsi_pekerjaan')->get();
    //     $deskripsi['looks'] = $look;
    //     $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
    //     if ($hasPersonalTable) {
    //         $select             = json_decode($hasPersonalTable->select);
    //         if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
    //             $queryField         = null;
    //             $queryOperator      = null;
    //             $queryValue         = null;
    //         } else {
    //             $queryField         = json_decode($hasPersonalTable->query_field);
    //             $queryOperator      = json_decode($hasPersonalTable->query_operator);
    //             $queryValue         = json_decode($hasPersonalTable->query_value);
    //         }
    //         $query = DB::table('wsdeskripsipekerjaan')->select($select);

    //         if ($hasPersonalTable->nama_posisi) {
    //             $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
    //         }
    //         if ($hasPersonalTable->kode_posisi) {
    //             $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
    //         }
    //         if ($queryField) {
    //             $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
    //                 for ($i = 0; $i < count($queryField); $i++) {
    //                     if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
    //                         $date = date('Y-m-d', strtotime($queryValue[$i]));

    //                         if ($queryOperator[$i] == '%LIKE%') {
    //                             $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
    //                         } else {
    //                             $sub->where($queryField[$i], $queryOperator[$i], $date);
    //                         }
    //                     } else {
    //                         if ($queryOperator[$i] == '%LIKE%') {
    //                             $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
    //                         } else {
    //                             $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
    //                         }
    //                     }
    //                 }
    //             });
    //         }

    //         $ret = $query->get();
    //         foreach ($ret as $key => $value) {
    //             $detailFilter = $this->get_w_deskripsi_pekerjaan($value->id_deskripsi_pekerjaan);
    //             $ret[$key]->detail_deskripsi = $this->set_list($detailFilter);
    //         }

    //         $data = [
    //             'query' => $ret,
    //             'th'    => $select
    //         ];

    //         return view('ws.wsdeskripsipekerjaan.filterResult', ['deskripsi' => $deskripsi], $data);
    //     } else {
    //         $wsdeskripsi = DB::table('wsdeskripsipekerjaan')->get();
    //         $look = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->select('id', 'deskripsi_pekerjaan')->get();
    //         $deskripsi['looks'] = $look;
    //         if ($request->has('keyword')) {
    //             $wsdeskripsi = DB::table('wsdeskripsipekerjaan')
    //                 ->where(function ($query) use ($request) {
    //                     $query->where('kode_posisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nama_posisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('keterangan', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('tanggal_mulai_efektif', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('tanggal_selesai_efektif', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nomor_dokumen', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('edisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('tanggal_edisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nomor_revisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('tanggal_revisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nama_jabatan', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nama_karyawan', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('divisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('lokasi_kerja', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('fungsi_jabatan', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nama_lokasi_kerja', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('nama_pengawas', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('kode_posisi', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('lingkup_aktivitas', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('id_logo_perusahaan', 'LIKE', '%' . $request->keyword . '%')
    //                         ->orWhere('departemen', 'LIKE', '%' . $request->keyword . '%');
    //                 })
    //                 ->get();
    //         }

    //         foreach ($wsdeskripsi as $k => $v) {
    //             $look = $this->get_w_deskripsi_pekerjaan($v->id_deskripsi_pekerjaan);
    //             $wsdeskripsi[$k]->detail_deskripsi = $this->set_list($look);
    //         }

    //         return view('ws.wsdeskripsipekerjaan.index', ['wsdeskripsi' => $wsdeskripsi, 'wsdeskripsi' => $wsdeskripsi]);
    //     }
    // }

    public function index(Request $request)
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_deskripsi_pekerjaan', $select)) {
                array_push($select, 'id_deskripsi_pekerjaan');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsdeskripsipekerjaan')->select($select);

            if ($hasPersonalTable->kode_posisi) {
                $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
            }
            if ($hasPersonalTable->nama_posisi) {
                $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $ret = $query->get();
            foreach ($ret as $key => $value) {
                $look = $this->get_w_deskripsi_pekerjaan($value->id_deskripsi_pekerjaan);
                $ret[$key]->detail_deskripsi = $this->set_list($look);
            }

            $data = [
                'query' => $ret,
                'th'    => $select
            ];

            return view('ws.wsdeskripsipekerjaan.filterResult', $data);
        } else {
            $wsdeskripsi_hdp = DB::table('wsdeskripsipekerjaan')->get();

            if ($request->has('keyword')) {
                $wsdeskripsi_hdp = DB::table('wsdeskripsipekerjaan')
                    ->where(function ($query) use ($request) {
                        $query->where('kode_posisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_posisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('keterangan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('tanggal_mulai_efektif', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('tanggal_selesai_efektif', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nomor_dokumen', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('edisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('tanggal_edisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nomor_revisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('tanggal_revisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_jabatan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_karyawan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('divisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('lokasi_kerja', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('fungsi_jabatan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_lokasi_kerja', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('nama_pengawas', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('kode_posisi', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('lingkup_aktivitas', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('id_logo_perusahaan', 'LIKE', '%' . $request->keyword . '%')
                            ->orWhere('departemen', 'LIKE', '%' . $request->keyword . '%');
                    })
                    ->get();
            }

            foreach ($wsdeskripsi_hdp as $k => $v) {
                $look = $this->get_w_deskripsi_pekerjaan($v->id_deskripsi_pekerjaan);
                $wsdeskripsi_hdp[$k]->detail_deskripsi = $this->set_list($look);
            }
            return view('ws.wsdeskripsipekerjaan.index_hdp', ['wsdeskripsi_hdp' => $wsdeskripsi_hdp]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_dp')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        $wsdeskripsi = DB::table('wsdeskripsipekerjaan')->get();
        return view('wsdeskripsi', ['wsdeskripsi' => $wsdeskripsi]);
    }

    // public function filter()
    // {
    //     $fields = [
    //         [
    //             'text'  => 'Id Deskripsi Pekerjaan',
    //             'value' => 'id_deskripsi_pekerjaan'
    //         ],
    //         [
    //             'text'  => 'Kode Deskripsi',
    //             'value' => 'kode_deskripsi'
    //         ],
    //         [
    //             'text'  => 'Nama Posisi',
    //             'value' => 'nama_posisi'
    //         ],

    //         [
    //             'text'  => 'Deskripsi Pekerjaan',
    //             'value' => 'deskripsi_pekerjaan'
    //         ]
    //     ];

    //     $operators = [
    //         '=', '<>', '%LIKE%'
    //     ];

    //     $data = [
    //         'fields'    => $fields,
    //         'operators' => $operators,
    //     ];

    //     return view('ws.wsdeskripsipekerjaan.filter', $data);
    // }

    public function filter()
    {
        $fields = [
            // [
            //     'text'  => 'Id Deskripsi Pekerjaan',
            //     'value' => 'id_deskripsi_pekerjaan'
            // ],
            // [
            //     'text'  => 'Kode Deskripsi',
            //     'value' => 'kode_deskripsi'
            // ],
            [
                'text'  => 'Kode Deskripsi',
                'value' => 'kode_deskripsi'
            ],
            [
                'text'  => 'Nama Atasan Langsung',
                'value' => 'nama_atasan_langsung'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan',
                'value' => 'deskripsi_pekerjaan'
            ],
            [
                'text'  => 'Tanggal Mulai efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Nomor Dokumen',
                'value' => 'nomor_dokumen'
            ],
            [
                'text'  => 'Edisi',
                'value' => 'edisi'
            ],
            [
                'text'  => 'Tanggal Edisi',
                'value' => 'tanggal_edisi'
            ],
            [
                'text'  => 'Nomor Revisi',
                'value' => 'nomor_revisi'
            ],
            [
                'text'  => 'Tanggal Revisi',
                'value' => 'tanggal_revisi'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Karyawan',
                'value' => 'nama_karyawan'
            ],
            [
                'text'  => 'Divisi',
                'value' => 'divisi'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Dibuat Oleh',
                'value' => 'dibuat_oleh'
            ],
            [
                'text'  => 'Diperiksa Oleh',
                'value' => 'diperiksa_oleh'
            ],
            [
                'text'  => 'Fungsi Jabatan',
                'value' => 'fungsi_jabatan'
            ],
            [
                'text'  => 'Tanggung Jawab',
                'value' => 'tanggung_jawab'
            ],
            [
                'text'  => 'Departemen',
                'value' => 'departemen'
            ],
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Pengawas',
                'value' => 'nama_pengawas'
            ],
            [
                'text'  => 'Lingkup Aktivitas',
                'value' => 'lingkup_aktivitas'
            ],
            [
                'text'  => 'ID Logo Perusahaan',
                'value' => 'id_logo_perusahaan'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

       

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wsdeskripsipekerjaan.filter', $data);
    }

    public function detail($id_deskripsi_pekerjaan)
    {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $detail = $this->get_w_deskripsi_pekerjaan($id_deskripsi_pekerjaan);


        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $deskripsiPekerjaan = [];
        $kualifikasiJabatan = [];
        $tanggungJawab      = [];
        $wewenang           = [];

        foreach ($wsdeskripsipekerjaan_tanggung_jawab as $row) {
            array_push($tanggungJawab, $row->tanggung_jawab);
            // if(!$tanggungJawab) {
            //     $tanggungJawab = $row->tanggung_jawab;
            // } else {
            //     $tanggungJawab .= ', '.$row->tanggung_jawab;
            // }
        }

        foreach ($wsdeskripsipekerjaan_wewenang as $row) {
            array_push($wewenang, $row->wewenang);
            // if(!$wewenang) {
            //     $wewenang = $row->wewenang;
            // } else {
            //     $wewenang .= ', '.$row->wewenang;
            // }
        }

        foreach ($wsdeskripsipekerjaan_kualifikasi_jabatan as $row) {
            array_push($kualifikasiJabatan, array("jabatan" => $row->kualifikasi_jabatan, "tipe" => $row->tipe_kualifikasi));
            // if(!$kualifikasiJabatan) {
            //     $kualifikasiJabatan = $row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // } else {
            //     $kualifikasiJabatan .= ', '.$row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // }
        }

        foreach ($wsdeskripsipekerjaan_deskripsi_pekerjaan as $row) {
            array_push($deskripsiPekerjaan, array(
                "deskripsi" => $row->deskripsi_pekerjaan,
                "pdca" => $row->pdca,
                "bsc" => $row->bsc,
            ));

            // if(!$deskripsiPekerjaan) {
            //     $deskripsiPekerjaan = $row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            // } else {
            //     $deskripsiPekerjaan .= '; '.$row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            //     //pdca,bsc
            // }
        }

        $data = [
            'deskripsiPekerjaan'    => $deskripsiPekerjaan,
            'kualifikasiJabatan'    => $kualifikasiJabatan,
            'tanggungJawab'         => $tanggungJawab,
            'wewenang'              => $wewenang,
            'wsdeskripsipekerjaan'  => $wsdeskripsipekerjaan,
            'detail' => $detail
        ];

        // dd($data);
        // return;
        return view('ws.wsdeskripsipekerjaan.detail', $data);
    }

    public function detail_des($id_deskripsi_pekerjaan)
    {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $deskripsiPekerjaan = [];
        $kualifikasiJabatan = [];
        $tanggungJawab      = [];
        $wewenang           = [];

        foreach ($wsdeskripsipekerjaan_tanggung_jawab as $row) {
            array_push($tanggungJawab, $row->tanggung_jawab);
            // if(!$tanggungJawab) {
            //     $tanggungJawab = $row->tanggung_jawab;
            // } else {
            //     $tanggungJawab .= ', '.$row->tanggung_jawab;
            // }
        }

        foreach ($wsdeskripsipekerjaan_wewenang as $row) {
            array_push($wewenang, $row->wewenang);
            // if(!$wewenang) {
            //     $wewenang = $row->wewenang;
            // } else {
            //     $wewenang .= ', '.$row->wewenang;
            // }
        }

        foreach ($wsdeskripsipekerjaan_kualifikasi_jabatan as $row) {
            array_push($kualifikasiJabatan, array("jabatan" => $row->kualifikasi_jabatan, "tipe" => $row->tipe_kualifikasi));
            // if(!$kualifikasiJabatan) {
            //     $kualifikasiJabatan = $row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // } else {
            //     $kualifikasiJabatan .= ', '.$row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // }
        }

        foreach ($wsdeskripsipekerjaan_deskripsi_pekerjaan as $row) {
            array_push($deskripsiPekerjaan, array(
                "deskripsi" => $row->deskripsi_pekerjaan,
                "pdca" => $row->pdca,
                "bsc" => $row->bsc,
            ));

            // if(!$deskripsiPekerjaan) {
            //     $deskripsiPekerjaan = $row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            // } else {
            //     $deskripsiPekerjaan .= '; '.$row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            //     //pdca,bsc
            // }
        }

        $data = [
            'deskripsiPekerjaan'    => $deskripsiPekerjaan,
            'kualifikasiJabatan'    => $kualifikasiJabatan,
            'tanggungJawab'         => $tanggungJawab,
            'wewenang'              => $wewenang,
            'wsdeskripsipekerjaan'  => $wsdeskripsipekerjaan
        ];

        return view('ws.wsdeskripsipekerjaan.detail', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_deskripsi_pekerjaan';
        } else {
            $select = ['id_deskripsi_pekerjaan', 'kode_posisi', 'nama_posisi', 'deskripsi_pekerjaan', 'nama_kelompok_jabatan'];
        }

        $query = DB::table('wsdeskripsipekerjaan')->select($select);

        if ($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }
        if ($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }
        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_dp')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser_dp')->insert([
            'kode_posisi'       => $request->kode_posisi,
            'nama_posisi'       => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_dp');
    }

    public function filterSubmit_hdp(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_deskripsi_pekerjaan';
        } else {
            $select = ['id_deskripsi_pekerjaan', 'kode_posisi', 'nama_posisi', 'keterangan', 'deskripsi_pekerjaan'];
        }

        $query = DB::table('wsdeskripsipekerjaan')->select($select);

        if ($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }

        if ($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_dp')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser_dp')->insert([
            'kode_posisi'   => $request->kode_posisi,
            'nama_posisi'   => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/');
    }

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_dp')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }
    function get_w_deskripsi_pekerjaan($id)
    {
        $look = DB::table('detail_desc_pekerjaan')
            ->where('desc_id', $id)
            ->select('id', 'deskripsi_pekerjaan', 'pdca', 'bsc')
            ->get();

        foreach ($look as $keyDet => $val) {
            $look[$keyDet]->no = ($keyDet + 1);
        }
        return $look;
        // print_r($look);
    }

    function set_list($data)
    {
        $ret = "-";
        if (count($data)) {
            $ret = '<ul class="mb-0">';
            foreach ($data as $key) {
                $ret .= '<li>' . $key->deskripsi_pekerjaan . '</li>';
            }
            $ret .= '</ul>';
        }
        return $ret;
    }

    // yepi
    public function simpan(Request $request)
    {
        // return response()->json($request->all(), 200);

        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->insert([
            'kode_deskripsi'            => $request->kode_deskripsi,
            'nama_posisi'               => $request->nama_posisi,
            'keterangan'                => $request->keterangan,
            'tanggal_mulai_efektif'     => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'nomor_dokumen'             => $request->nomor_dokumen,
            'edisi'                     => $request->edisi,
            'tanggal_edisi'             => $request->tanggal_edisi ? date('Y-m-d', strtotime($request->tanggal_edisi)) : null,
            'nomor_revisi'              => $request->nomor_revisi,
            'tanggal_revisi'            => $request->tanggal_revisi ? date('Y-m-d', strtotime($request->tanggal_revisi)) : null,
            'nama_jabatan'              => $request->nama_jabatan,
            'nama_karyawan'             => $request->nama_karyawan,
            'divisi'                    => $request->divisi,
            'lokasi_kerja'              => $request->lokasi_kerja,
            'fungsi_jabatan'            => $request->fungsi_jabatan,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_pengawas'             => $request->nama_pengawas,
            'kode_posisi'               => $request->kode_posisi,
            'lingkup_aktivitas'         => $request->lingkup_aktivitas,
            'id_logo_perusahaan'        => $request->id_logo_perusahaan,
            'departemen'                => $request->departemen,
            'pengguna_masuk'            => Auth::user()->id,
            'waktu_masuk'               => date('Y-m-d')
        ]);

        $wsdeskripsipekerjaan = \DB::table('wsdeskripsipekerjaan')
            ->where('kode_deskripsi', $request->kode_deskripsi)
            ->orderBy('id_deskripsi_pekerjaan', 'DESC')
            ->first();

        if ($request->tanggung_jawab && count($request->tanggung_jawab) > 0) {
            for ($i = 0; $i < count($request->tanggung_jawab); $i++) {
                DB::table('wsdeskripsipekerjaan_tanggung_jawab')->insert([
                    'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                    'tanggung_jawab'            => $request->tanggung_jawab[$i]
                ]);
            }
        }

        if ($request->wewenang && count($request->wewenang) > 0) {
            for ($i = 0; $i < count($request->wewenang); $i++) {
                DB::table('wsdeskripsipekerjaan_wewenang')->insert([
                    'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                    'wewenang'                  => $request->wewenang[$i]
                ]);
            }
        }

        if ($request->kualifikasi_jabatan && count($request->kualifikasi_jabatan) > 0) {
            for ($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
                DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->insert([
                    'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                    'kualifikasi_jabatan'       => $request->kualifikasi_jabatan[$i],
                    'tipe_kualifikasi'          => $request->tipe_kualifikasi[$i]
                ]);
            }
        }

        if ($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
            for ($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
                DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->insert([
                    'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                    'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
                    'pdca'                      => $request->pdca[$i],
                    'bsc'                       => $request->bsc[$i]
                ]);
            }
        }

        return redirect('/list_dp');
    }

    // y
    public function simpan_hdp(Request $request)
    {

        $data = [
            'kode_deskripsi'            => $request->kode_deskripsi,
            'keterangan'                => $request->keterangan,
            'tanggal_mulai_efektif'     => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'nomor_dokumen'             => $request->nomor_dokumen,
            'edisi'                     => $request->edisi,
            'tanggal_edisi'             => $request->tanggal_edisi ? date('Y-m-d', strtotime($request->tanggal_edisi)) : null,
            'nomor_revisi'              => $request->nomor_revisi,
            'tanggal_revisi'            => $request->tanggal_revisi ? date('Y-m-d', strtotime($request->tanggal_revisi)) : null,
            'nama_jabatan'              => $request->nama_jabatan,
            'nama_karyawan'             => $request->nama_karyawan,
            'divisi'                    => $request->divisi,
            'lokasi_kerja'              => $request->lokasi_kerja,
            'fungsi_jabatan'            => $request->fungsi_jabatan,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_atasan_langsung'      => $request->nama_atasan_langsung,
            'nama_pengawas'             => $request->nama_pengawas,
            'dibuat_oleh'               => $request->dibuat_oleh,
            'diperiksa_oleh'            => $request->diperiksa_oleh,
            'disahkan_oleh'             => $request->disahkan_oleh,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_posisi'               => $request->nama_posisi,
            'kode_posisi'               => $request->kode_posisi,
            'lingkup_aktivitas'         => $request->lingkup_aktivitas,
            'id_logo_perusahaan'        => $request->id_logo_perusahaan,
            'departemen'                => $request->departemen,
            'pengguna_masuk'            => Auth::user()->id,
            'waktu_masuk'               => date('Y-m-d')
        ];
        // dd($data);

        try {
            $insert_data = DB::table('wsdeskripsipekerjaan')->insert($data);
            if ($insert_data) {
                $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')
                    ->where('kode_deskripsi', $request->kode_deskripsi)
                    ->orderBy('id_deskripsi_pekerjaan', 'DESC')
                    ->first();

                if ($request->tanggung_jawab && count($request->tanggung_jawab) > 0) {
                    for ($t = 0; $t < count($request->tanggung_jawab); $t++) {
                        DB::table('wsdeskripsipekerjaan_tanggung_jawab')->insert([
                            'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                            'tanggung_jawab'            => $request->tanggung_jawab[$t] ? $request->tanggung_jawab[$t] : "-"
                        ]);
                    }
                }

                if ($request->wewenang && count($request->wewenang) > 0) {
                    for ($i = 0; $i < count($request->wewenang); $i++) {
                        DB::table('wsdeskripsipekerjaan_wewenang')->insert([
                            'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                            'wewenang'                  => $request->wewenang[$i]
                        ]);
                    }
                }

                if ($request->kualifikasi_jabatan && count($request->kualifikasi_jabatan) > 0) {
                    for ($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
                        DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->insert([
                            'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                            'kualifikasi_jabatan'       => $request->kualifikasi_jabatan[$i],
                            'tipe_kualifikasi'          => $request->tipe_kualifikasi[$i]
                        ]);
                    }
                }

                // if ($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
                //     for ($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
                //         DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->insert([
                //             'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                //             'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
                //             'pdca'                      => $request->pdca[$i],
                //             'bsc'                       => $request->bsc[$i]
                //         ]);
                //     }
                // }

                if ($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
                    for ($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
                        DB::table('detail_desc_pekerjaan')->insert([
                            'desc_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                            'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
                            'pdca'                      => $request->pdca[$i] ? $request->pdca[$i] : null,
                            'bsc'                       => $request->bsc[$i] ? $request->bsc[$i] : null,
                            // 'kode_posisi'               => $request->det_kode_posisi[$i] ? $request->det_kode_posisi[$i] : null,
                            // 'nama_posisi'               => $request->det_nama_posisi[$i] ? $request->det_nama_posisi[$i] : null
                        ]);
                    }
                }
            }
            return redirect('/list_dp');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    // y
    public function update(Request $request)
    {
        $id = $request->id_deskripsi_pekerjaan;

        DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('detail_desc_pekerjaan')->where('desc_id', $id)->delete();

        DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id)->update([
            'kode_deskripsi'            => $request->kode_deskripsi,
            'keterangan'                => $request->keterangan,
            'tanggal_mulai_efektif'     => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'nomor_dokumen'             => $request->nomor_dokumen,
            'edisi'                     => $request->edisi,
            'tanggal_edisi'             => $request->tanggal_edisi ? date('Y-m-d', strtotime($request->tanggal_edisi)) : null,
            'nomor_revisi'              => $request->nomor_revisi,
            'tanggal_revisi'            => $request->tanggal_revisi ? date('Y-m-d', strtotime($request->tanggal_revisi)) : null,
            'nama_jabatan'              => $request->nama_jabatan,
            'nama_karyawan'             => $request->nama_karyawan,
            'divisi'                    => $request->divisi,
            'lokasi_kerja'              => $request->lokasi_kerja,
            'fungsi_jabatan'            => $request->fungsi_jabatan,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_pengawas'             => $request->nama_pengawas,
            'nama_atasan_langsung'      => $request->nama_atasan_langsung,
            'nama_pengawas'             => $request->nama_pengawas,
            'dibuat_oleh'               => $request->dibuat_oleh,
            'diperiksa_oleh'            => $request->diperiksa_oleh,
            'disahkan_oleh'             => $request->disahkan_oleh,
            'kode_posisi'               => $request->kode_posisi,
            'nama_posisi'               => $request->nama_posisi,
            'lingkup_aktivitas'         => $request->lingkup_aktivitas,
            'id_logo_perusahaan'        => $request->id_logo_perusahaan,
            'departemen'                => $request->departemen,
            'pengguna_ubah'             => Auth::user()->id,
            'waktu_ubah'                => date('Y-m-d')
        ]);

        if ($request->tanggung_jawab && count($request->tanggung_jawab) > 0) {
            for ($i = 0; $i < count($request->tanggung_jawab); $i++) {
                DB::table('wsdeskripsipekerjaan_tanggung_jawab')->insert([
                    'wsdeskripsipekerjaan_id'   => $id,
                    'tanggung_jawab'            => $request->tanggung_jawab[$i]
                ]);
            }
        }

        if ($request->wewenang && count($request->wewenang) > 0) {
            for ($i = 0; $i < count($request->wewenang); $i++) {
                DB::table('wsdeskripsipekerjaan_wewenang')->insert([
                    'wsdeskripsipekerjaan_id'   => $id,
                    'wewenang'                  => $request->wewenang[$i]
                ]);
            }
        }

        if ($request->kualifikasi_jabatan && count($request->kualifikasi_jabatan) > 0) {
            for ($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
                DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->insert([
                    'wsdeskripsipekerjaan_id'   => $id,
                    'kualifikasi_jabatan'       => $request->kualifikasi_jabatan[$i],
                    'tipe_kualifikasi'          => $request->tipe_kualifikasi[$i]
                ]);
            }
        }

        // if ($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
        //     for ($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
        //         DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->insert([
        //             'wsdeskripsipekerjaan_id'   => $id,
        //             'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
        //             'pdca'                      => $request->pdca[$i],
        //             'bsc'                       => $request->bsc[$i]
        //         ]);
        //     }
        // }


        if ($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
            for ($det = 0; $det < count($request->deskripsi_pekerjaan); $det++) {
                DB::table('detail_desc_pekerjaan')->insert([
                    'desc_id'                   => $id,
                    'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$det],
                    'pdca'                      => $request->pdca[$det] ? $request->pdca[$det] : null,
                    'bsc'                       => $request->bsc[$det] ? $request->bsc[$det] : null
                    // 'kode_posisi'               => $request->det_kode_posisi[$det] ? $request->det_kode_posisi[$det] : null,
                    // 'nama_posisi'               => $request->det_nama_posisi[$det] ? $request->det_nama_posisi[$det] : null
                ]);
            }
        }
        return redirect('/list_dp');
    }

    // public function tambah_hdp()
    // {
    //     $debug = $this->debug;
    //     $lookpo = DB::table('wsposisi')->select('id_posisi', 'kode_posisi', 'tingkat_posisi', 'nama_posisi', 'nama_posisi_atasan')->get();
    //     $datapo['looks'] = $lookpo;
    //     $looklk = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
    //     $datalk['looks'] = $looklk;
    //     $result = DB::table("users")->selectRaw("id,name,email")->get();

    //     return view('ws.wsdeskripsipekerjaan.tambah_hdp', ['datapo' => $datapo, 'datalk' => $datalk, 'debug' => $debug])->with("data", $result);
    // }

    public function tambah()
    {
        $debug = $this->debug;

        $lookpo = DB::table('wsposisi')->select('id_posisi', 'kode_posisi', 'tingkat_posisi', 'nama_posisi', 'nama_posisi_atasan')->get();
        $datapo['looks'] = $lookpo;
        $looklk = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $looklk;
        $result = DB::table("users")->selectRaw("id,name,email")->get();
        $code_ = $this->get_number();

        return view('ws.wsdeskripsipekerjaan.tambah', ['random'=>$code_,'datapo' => $datapo, 'datalk' => $datalk, 'debug' => $debug])->with("data", $result);
    }

    public function get_number(){
        $max_id = DB::table('wsdeskripsipekerjaan')
        ->where('id_deskripsi_pekerjaan', \DB::raw("(select max(`id_deskripsi_pekerjaan`) from wsdeskripsipekerjaan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_deskripsi) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_deskripsi, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'DP-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    public function edit_hdp($id_deskripsi_pekerjaan)
    {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();
        $wsposisi = DB::table('wsposisi')->select('nama_posisi','kode_posisi')->get();
        $datapo['looks'] = $wsposisi;
        $data = [
            'datapo'                                  => $datapo,
            'id'                                        => $id_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan'                      => $wsdeskripsipekerjaan,
            'wsdeskripsipekerjaan_deskripsi_pekerjaan'  => $wsdeskripsipekerjaan_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan_kualifikasi_jabatan'  => $wsdeskripsipekerjaan_kualifikasi_jabatan,
            'wsdeskripsipekerjaan_tanggung_jawab'       => $wsdeskripsipekerjaan_tanggung_jawab,
            'wsdeskripsipekerjaan_wewenang'             => $wsdeskripsipekerjaan_wewenang
        ];
        $result = DB::table("users")->get();
        return view('ws.wsdeskripsipekerjaan.edit_hdp', $data)->with("data", $result);
    }

    public function edit($id_deskripsi_pekerjaan)
    {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];
        $detail = $this->get_w_deskripsi_pekerjaan($id_deskripsi_pekerjaan);

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $data = [
            'id'                                        => $id_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan'                      => $wsdeskripsipekerjaan,
            'wsdeskripsipekerjaan_deskripsi_pekerjaan'  => $wsdeskripsipekerjaan_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan_kualifikasi_jabatan'  => $wsdeskripsipekerjaan_kualifikasi_jabatan,
            'wsdeskripsipekerjaan_tanggung_jawab'       => $wsdeskripsipekerjaan_tanggung_jawab,
            'wsdeskripsipekerjaan_wewenang'             => $wsdeskripsipekerjaan_wewenang,
            'detail' => $detail
        ];
        $result = DB::table("users")->get();
        $lookpo = DB::table('wsposisi')->select('id_posisi', 'kode_posisi', 'tingkat_posisi', 'nama_posisi', 'nama_posisi_atasan')->get();
        $datapo['looks'] = $lookpo;
        $looklk = DB::table('wslokasikerja')->select('nama_lokasi_kerja')->get();
        $datalk['looks'] = $looklk;
        return view('ws.wsdeskripsipekerjaan.edit_hdp', $data, ['datapo' => $datapo, 'datalk' => $datalk])->with("data", $result);
    }

    public function delete($id_deskripsi_pekerjaan)
    {
        DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->delete();
        return redirect('/')->with('success', 'data berhasil dihapus');
    }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_wsdeskripsi');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsdeskripsi")->whereIn('id_perusahaan', explode(",", $ids))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }

    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wsdeskripsi = DB::table('wsdeskripsipekerjaan')
    //     ->where('nama_perusahaan','like',"%".$cari."%")
    //     ->orWhere('kode_perusahaan','like',"%".$cari."%")
    //     ->orWhere('singkatan','like',"%".$cari."%")
    //     ->orWhere('visi_perusahaan','like',"%".$cari."%")
    //     ->orWhere('misi_perusahaan','like',"%".$cari."%")
    //     ->orWhere('nilai_perusahaan','like',"%".$cari."%")
    //     ->orWhere('keterangan_perusahaan','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")
    //     ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")
    //     ->orWhere('jenis_perusahaan','like',"%".$cari."%")
    //     ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")
    //     ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
    //     ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")
    //     ->orWhere('lokasi_pajak','like',"%".$cari."%")
    //     ->orWhere('npp','like',"%".$cari."%")
    //     ->orWhere('npkp','like',"%".$cari."%")
    //     ->orWhere('id_logo_perusahaan','like',"%".$cari."%")
    //     ->orWhere('keterangan','like',"%".$cari."%")
    //     ->orWhere('status_rekaman','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%")
    //     ->orWhere('waktu_masuk','like',"%".$cari."%")
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%")
    //     ->orWhere('waktu_ubah','like',"%".$cari."%")
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%")
    //     ->orWhere('waktu_hapus','like',"%".$cari."%")
    //     ->orWhere('jumlah_karyawan','like',"%".$cari."%")
    //     ->paginate();
    //     return view('wsdeskripsi',['wsdeskripsi' =>$wsdeskripsi]);
    // }
}
