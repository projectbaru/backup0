<?php
// app/Http/Controllers/pro_c.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class pro_c extends Controller
{
    public function index()
    {
        // y
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_process_recruitment', $select)) {
                array_push($select, 'id_process_recruitment');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('rc_process_recruitment')->select($select);

            if ($hasPersonalTable->nama_kandidat) {
                $query->where('nama_kandidat', $hasPersonalTable->nama_kandidat);
            }
            if ($hasPersonalTable->kode_process_recruitment) {
                $query->where('kode_process_recruitment', $hasPersonalTable->kode_process_recruitment);
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            // print_r($data);

            return view('re.pr.filterResult', $data);
        } else {
            $rc_prosess = DB::table('rc_process_recruitment')->get();
            return view('re.pr.index', ['rc_prosess' => $rc_prosess]);
        }
    }

    public function get_tampilan($active)
    {
        return DB::table('reftable_pro')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function detail($id_process_recruitment)
    {
        $data = DB::table('rc_process_recruitment AS pr')
                ->join('rc_detail_process_recruitment AS dpr', 'pr.id_process_recruitment', '=', 'dpr.id_rc_process_recruitment')
                ->where('pr.id_process_recruitment', $id_process_recruitment)
                ->select('pr.*', 'pr.status AS status_header', 'dpr.*', 'dpr.status AS status_detail')
                ->get();

        $pr= DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen')->get();
        $no_doc['looks']=$pr;
        $dcp = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar', 'nama_lengkap', 'jabatan_lowongan', 'email', 'no_hp')->get();
        $no_cv['looks']=$dcp;
        $dpp = DB::table('rc_form_data_pelamar')->select('kode_data_pelamar')->get();
        $kode['looks']=$dpp;
        $spr = DB::table('rc_step_process_recruitment')->select('nama_jabatan')->get();
        $nama_posisi['looks']=$spr;
        
        $dpr = DB::table('rc_detail_process_recruitment')->where('id_rc_process_recruitment', $id_process_recruitment)->get();

        return view('re.pr.detail', ['dpr'=>$dpr,'no_doc'=>$no_doc,'nama_posisi'=>$nama_posisi, 'no_cv'=>$no_cv, 'kode'=>$kode,'data'=>$data,'id_process_recruitment'=>$id_process_recruitment]);
    }

    public function hapus($id_process_recruitment){
        $del =DB::table('rc_process_recruitment')->where('id_process_recruitment', $id_process_recruitment)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_po');
    }

    //public function detail($id_process_recruitment)
    //{
    //    $detail_fptk = DB::table('rc_process_recruitment')
    //    ->join('rc_formulir_permintaan_tenaga_kerja', 'rc_formulir_permintaan_tenaga_kerja.no_dokumen', '=', 'rc_formulir_permintaan_tenaga_kerja.no_dokumen')
    //    ->select('rc_process_recruitment.*', 'rc_formulir_permintaan_tenaga_kerja.*')
    //    ->where('rc_process_recruitment.id_process_recruitment', $id_process_recruitment)
    //    ->get();
    //    $detail_data_p = DB::table('rc_process_recruitment')
    //    ->join('rc_form_data_pelamar', 'rc_form_data_pelamar.kode_data_pelamar', '=', 'rc_process_recruitment.kode_data_pelamar')
    //    ->select('rc_process_recruitment.*', 'rc_form_data_pelamar.*')
    //    ->where('rc_process_recruitment.id_process_recruitment', $id_process_recruitment)
    //    ->get();
    //    $dpp_cv = DB::table('rc_process_recruitment')
    //    ->join('rc_data_cv_pelamar', 'rc_data_cv_pelamar.kode_cv_pelamar', '=', 'rc_process_recruitment.kode_cv_pelamar')
    //    ->select('rc_process_recruitment.*', 'rc_data_cv_pelamar.*')
    //    ->where('rc_process_recruitment.id_process_recruitment', $id_process_recruitment)
    //    ->get();

    //    $wspro = DB::table('')->where('id_process_recruitment', $id_process_recruitment)->get();
    //    $wspro = DB::table('rc_process_recruitment')->where('id_process_recruitment', $id_process_recruitment)->get();
    //    return view('re.pr.detail', ['wspro'=> $wspro, 'detail_fptk'=> $detail_fptk, 'detail_data_p'=> $detail_data_p, 'dpp_cv'=> $dpp_cv]);
    //}

    public function detail_cv($kode_cv_pelamar)
    {
        $rc_cv = DB::table('rc_data_cv_pelamar')->where('kode_cv_pelamar', $kode_cv_pelamar)->get();
        return view('re.dcp.detail', ['rc_cv'=> $rc_cv]);
    }

    public function detail_fptk($no_dokumen)
    {
        $data_fptk = DB::table('rc_formulir_permintaan_tenaga_kerja')->where('no_dokumen', $no_dokumen)->get();
        return view('re.pr.detail_fptk', ['data_fptk'=> $data_fptk]);
    }

   

    public function detail_dpp($kode_data_pelamar)
    {
        $dpp = DB::table('rc_form_data_pelamar')->where('kode_data_pelamar', $kode_data_pelamar)->get();
        return view('re.pr.detail_dpp', ['dpp'=> $dpp]);
    }

    public function edit($id_process_recruitment)
    {
        $data = DB::table('rc_process_recruitment AS pr')
                ->join('rc_detail_process_recruitment AS dpr', 'pr.id_process_recruitment', '=', 'dpr.id_rc_process_recruitment')
                ->where('pr.id_process_recruitment', $id_process_recruitment)
                ->select('pr.*', 'pr.status AS status_header', 'dpr.*', 'dpr.status AS status_detail')
                ->get();

        $pr= DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen')->get();
        $no_doc['looks']=$pr;
        $dcp = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar', 'nama_lengkap', 'jabatan_lowongan', 'email', 'no_hp')->get();
        $no_cv['looks']=$dcp;
        $dpp = DB::table('rc_form_data_pelamar')->select('kode_data_pelamar')->get();
        $kode['looks']=$dpp;
        $spr = DB::table('rc_step_process_recruitment')->select('nama_jabatan')->get();
        $nama_posisi['looks']=$spr;
        
        $dpr = DB::table('rc_detail_process_recruitment')->where('id_rc_process_recruitment', $id_process_recruitment)->get();

        return view('re.pr.edit', ['dpr'=>$dpr,'no_doc'=>$no_doc,'nama_posisi'=>$nama_posisi, 'no_cv'=>$no_cv, 'kode'=>$kode,'data'=>$data,'id_process_recruitment'=>$id_process_recruitment]);
    }

    //public function edit($id_process_recruitment)
    //{
    //    $pr= DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen')->get();
    //    $no_doc['looks']=$pr;
    //    $dcp = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar', 'nama_lengkap', 'jabatan_lowongan')->get();
    //    $no_cv['looks']=$dcp;
    //    $dpp = DB::table('rc_form_data_pelamar')->select('kode_data_pelamar')->get();
    //    $kode['looks']=$dpp;
    //    $spr = DB::table('rc_step_process_recruitment')->select('nama_jabatan')->get();
    //    $nama_posisi['looks']=$spr;
    //    return view('re.pr.edit', ['no_doc'=>$no_doc,'nama_posisi'=>$nama_posisi, 'no_cv'=>$no_cv, 'kode'=>$kode]);
    //}

    public function getStepsByPosition(Request $request) {
        $selectedPosition = $request->input('nama_posisi');
        
        // Fetch steps based on the selected position
        $steps =  DB::table('rc_detail_step_process_recruitment')->where('nama_posisi', $selectedPosition)->get();
        
        return response()->json(['steps' => $steps]);
    }

    public function tambah()
    {
        $pr= DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen')->get();
        $no_doc['looks']=$pr;
        $dcp = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar', 'nama_lengkap', 'jabatan_lowongan', 'no_hp','email')->get();
        $no_cv['looks']=$dcp;
        $dpp = DB::table('rc_form_data_pelamar')->select('kode_data_pelamar')->get();
        $kode['looks']=$dpp;
        $spr = DB::table('rc_step_process_recruitment')->select('nama_jabatan')->get();
        $nama_posisi['looks']=$spr;
        $dpr = DB::table('rc_detail_step_process_recruitment')->get();
        $code_ = $this->get_number();
        
        return view('re.pr.tambah', ['code_'=>$code_,'dpr'=>$dpr,'no_doc'=>$no_doc,'nama_posisi'=>$nama_posisi, 'no_cv'=>$no_cv, 'kode'=>$kode]);
    }


    public function get_number(){
        $max_id = DB::table('rc_process_recruitment')
        ->where('id_process_recruitment', \DB::raw("(select max(`id_process_recruitment`) from rc_process_recruitment)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_process_recruitment) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_process_recruitment, -2) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = str_pad($next_,  2, "0", STR_PAD_LEFT).'/PR/'.str_pad($next_,  5, "0", STR_PAD_LEFT).'/'.date('Y');
        return $code_;
    }

    public function simpan(Request $request)
    {
        DB::beginTransaction();
        $jumlahLangkah = $request->jumlah_langkah;


        try {
            $idProcessRecruitment = DB::table('rc_process_recruitment')->insertGetId([
                                'kode_process_recruitment'   =>$request->kode_process_recruitment,
                                'no_dokumen'                 =>$request->no_dokumen,
                                'kode_cv_pelamar'            =>$request->kode_cv_pelamar,
                                'kode_data_pelamar'          =>$request->kode_data_pelamar,
                                'nama_kandidat'              =>$request->nama_kandidat,
                                'nama_posisi'                =>$request->nama_posisi,
                                'jabatan_dilamar'            =>$request->jabatan_dilamar,
                                'jumlah_langkah' => $request->jumlah_langkah,
                                'no_hp'                      =>$request->no_hp,
                                'email'                      =>$request->email,
                                'id_step_process_recruitment'=>$request->id_step_process_recruitment,
                                'status'                     =>$request->status,
                                'nama_user_input'            =>$request->nama_user_input,
                                'waktu_user_input'           =>$request->waktu_user_input
            ]);

            //insert step 1
            $step = array();
            for ($i=1;$i<=$jumlahLangkah;$i++) {
                $a = 'nama_step'.$i;
                $b = 'no_urut'.$i;
                $c = 'target_tanggal'.$i;
                $d = 'tanggal_aktual'.$i;
                $e = 'status_step'.$i;
                $f = 'keterangan'.$i;
                $dataStep = array(
                  'id_rc_process_recruitment' => $idProcessRecruitment,
                  'id_detail_step_process_recruitment' => $request->id_step_process_recruitment,
                  'nama_step' => $request->$a,
                  'no_urut' => $request->$b,
                  'target_tanggal' => $request->$c,
                  'tanggal_aktual' => $request->$d,
                  'status' => $request->$e,
                  'keterangan' => $request->$f,
                );
                array_push($step, $dataStep);
            }
            DB::table('rc_detail_process_recruitment')->insert($step);
            DB::commit();
            return redirect('/list_process');
        } catch(\Exception $e) {
            dd($e->getMessage());
            die;
            DB::rollback();
            return redirect()->route('tambah_pro');
        }

        return redirect('/list_pro');
    }

    public function update(Request $request)
    {
        $id_process_recruitment = $request->id_process_recruitment;
        DB::beginTransaction();
        


        try {
            $jumlahLangkah = $request->jumlah_langkah;
            DB::table('rc_process_recruitment')->where('id_process_recruitment', $id_process_recruitment)->update([
                                //'kode_process_recruitment'   =>'PEL/'.date('y').'/'.date('m').strtotime(date('h:i:s')),
                                'no_dokumen'                 =>$request->no_dokumen,
                                'kode_cv_pelamar'            =>$request->kode_cv_pelamar,
                                'kode_data_pelamar'          =>$request->kode_data_pelamar,
                                'nama_kandidat'              =>$request->nama_kandidat,
                                'nama_posisi'                =>$request->nama_posisi,
                                'jabatan_dilamar'            =>$request->jabatan_dilamar,
                                'id_step_process_recruitment'=>$request->id_step_process_recruitment,
                                'status'                     =>$request->status,
                                'no_hp'                     =>$request->no_hp,
                                'email'                     =>$request->email,
                                'nama_user_input'            =>$request->nama_user_input,
                                'jumlah_langkah'            => $request->jumlah_langkah,
                                'waktu_user_input'           =>$request->waktu_user_input
            ]);

            //delete detail
            DB::table('rc_detail_process_recruitment')->where('id_rc_process_recruitment', $id_process_recruitment)->delete();
            

            //insert step 1
            $step = [];
            for ($i=1;$i<=$jumlahLangkah;$i++) {
                $a = 'nama_step'.$i;
                $b = 'no_urut'.$i;
                $c = 'target_tanggal'.$i;
                $d = 'tanggal_aktual'.$i;
                $e = 'status_step'.$i;
                $f = 'keterangan'.$i;
                $dataStep = array(
                  'id_rc_process_recruitment' => $id_process_recruitment,
                  'id_detail_step_process_recruitment' => $request->id_step_process_recruitment,
                  'nama_step' => $request->$a,
                  'no_urut' => $request->$b,
                  'target_tanggal' => $request->$c,
                  'tanggal_aktual' => $request->$d,
                  'status' => $request->$e,
                  'keterangan' => $request->$f,
                );
                array_push($step, $dataStep);
            }
            DB::table('rc_detail_process_recruitment')->insert($step);
            DB::commit();
            return redirect('/list_process');
        } catch(\Exception $e) {
            dd($e->getMessage());
            die;
            DB::rollback();
            return redirect()->route('tambah_pro');
        }

        return redirect('/list_process');
    }

    public function getDetailProcess(Request $request, $id) {

        $data = DB::table('rc_detail_process_recruitment')
            ->where('id_rc_process_recruitment', $id)
            ->get();
        
        return response()->json(['data' => $data]);
    }
}
