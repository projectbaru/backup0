<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('roles.index');
    }
    public function tologin(){
        return view('tologin');
    }
    public function dashboard(){
        return view('dashboard.index');
    }

}
