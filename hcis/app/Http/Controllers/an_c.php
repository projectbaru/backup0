<?php
namespace App\Http\Controllers;
// Use Alert;
// use  RealRashid\SweetAlert\SweetAlertServiceProvider ;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class an_c extends Controller
{
    public function index()
    {
        $rc_an=DB::table('rc_analisa_hasil_interview')->get();
        return view('re.ahi.index', ['rc_an'=>$rc_an]);
    }
    public function duplikat()
    {
        
        return view('re.ahi.index');
    
    }

    public function beri_analisa()
    {
        return view('re.ahi.berianalisa');
    }
    public function beri_analisa_simpan()
    {
        if(($request->status_form) ==''){
            $status_form='new';
        }
        DB::table('rc_analisa_hasil_interview')->insert([
            'kode_analisa_hasil_interview'   =>$request->kode_analisa_hasil_interview,
            'kode_wawancara_kandidat'   =>$request->kode_wawancara_kandidat,
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'nama_pelamar'   =>$request->nama_pelamar,
            'posisi_yang_dilamar'   =>$request->posisi_yang_dilamar,
            'pendidikan'  =>$request->pendidikan,
            'tanggal_interview'  =>$request->tanggal_interview,
            'status_marketing'=>$request->status_marketing,
            'status_form'=>$status_form,
            'waktu_kirim_form'=>$request->waktu_kirim_form,
            'nama_user_input'=>$request->nama_user_input,
            'tanggal_input'=>$request->tanggal_input,
            'user_ubah'=>$request->user_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'user_hapus'=>$request->user_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        return redirect('/list_an');
        return view('re.ahi.index');
    
    }

    public function index_status()
    {
        $rc_an=DB::table('rc_analisa_hasil_interview')->get();
        $rc_an=DB::table('rc_penganalisa_hasil_interview')->get();
        return view('re.ahi.status_pen', ['rc_an'=>$rc_an]);
    }
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
    }

    public function detail($id_analisa_hasil_interview){
        $rc_an=DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->get();
        $pre = DB::table('rc_process_recruitment')->select('kode_process_recruitment')->get();
        $pr['looks']=$pre;
        $rc_d  = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
        $rc_data['looks']=$rc_d;
        return view('re.ahi.detail', ['rc_an'=>$rc_an, 'pr'=>$pr, 'rc_data'=>$rc_data]);
    }

    public function filter_dua(Request $request)
    {
        $result = [];
        if ($request->tahun_dibuat) {
            $result = DB::table('rc_analisa_hasil_interview')->where('tahun_dibuat', '=', $request->tahun_dibuat)->get();
        } else {
            $result = DB::table('rc_analisa_hasil_interview')->where('tahun_dibuat', '=', $request->tahun_dibuat)->get();
        }
        $ptk_detail = DB::table('rc_ptk_detail')->get();
        return view('re.ptk.view', ['ptk_detail' => $ptk_detail, 'data' => $result]);
    }

    public function simpan(Request $request){
        if(($request->status_form) ==''){
            $status_form='new';
        }
        DB::table('rc_analisa_hasil_interview')->insert([
            'kode_analisa_hasil_interview'   =>$request->kode_analisa_hasil_interview,
            'kode_wawancara_kandidat'   =>$request->kode_wawancara_kandidat,
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'nama_pelamar'   =>$request->nama_pelamar,
            'posisi_yang_dilamar'   =>$request->posisi_yang_dilamar,
            'pendidikan'  =>$request->pendidikan,
            'tanggal_interview'  =>$request->tanggal_interview,
            'status_marketing'=>$request->status_marketing,
            'status_form'=>$status_form,
            'waktu_kirim_form'=>$request->waktu_kirim_form,
            'nama_user_input'=>$request->nama_user_input,
            'tanggal_input'=>$request->tanggal_input,
            'user_ubah'=>$request->user_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'user_hapus'=>$request->user_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        return redirect('/list_an');
    }
    
    public function edit($id_analisa_hasil_interview) {
        $rc_an = DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->get();
        $analisa_hasil_interview    = DB::table('rc_penganalisa_hasil_interview')
            
            ->get();
        $rc_d  = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
        $rc_data['looks']=$rc_d;
        $pre = DB::table('rc_process_recruitment')->select('kode_process_recruitment')->get();
        $pr['looks']=$pre;

        $data = [
            'analisa_hasil_interview'=> $analisa_hasil_interview,
        ];

        $dcp=DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
  
        return view('re.ahi.edit', $data, ['dcp'=>$dcp,'rc_an'=>$rc_an, 'rc_data'=>$rc_data, 'pr'=>$pr]);
    }

    public function print($id_analisa_hasil_interview) {
        $rc_an = DB::table('rc_analisa_hasil_interview')->get();
        $rc_d  = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
        $rc_data['looks']=$rc_d;
        $pre = DB::table('rc_process_recruitment')->select('kode_process_recruitment')->get();
        $pr['looks']=$pre;

        $data=DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
  
        return view('re.ahi.preview', compact('data'),['rc_an'=>$rc_an, 'rc_data'=>$rc_data, 'pr'=>$pr]);
    }

    public function preview($id_analisa_hasil_interview) {
        
        $rc_an=DB::table('rc_analisa_hasil_interview')
        // ->join('rc_penganalisa_hasil_interview', 'rc_penganalisa_hasil_interview.id_analisa_hasil_interview', '=', 'rc_penganalisa_hasil_interview.id_analisa_hasil_interview')
        // ->select('rc_analisa_hasil_interview.*','rc_penganalisa_hasil_interview.*')
        ->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->get();
        $rc_an_d=DB::table('rc_penganalisa_hasil_interview')
        ->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->get();
        return view('re.ahi.preview',['rc_an'=>$rc_an, 'rc_an_d'=>$rc_an_d]);
    }

    public function update(Request $request) {
        DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $request->id_analisa_hasil_interview)->update([
            'kode_analisa_hasil_interview'   =>$request->kode_analisa_hasil_interview,
            'kode_wawancara_kandidat'   =>$request->kode_wawancara_kandidat,
            'kode_cv_pelamar'   =>$request->kode_cv_pelamar,
            'nama_pelamar'   =>$request->nama_pelamar,
            'posisi_yang_dilamar'   =>$request->posisi_yang_dilamar,
            'pendidikan'  =>$request->pendidikan,
            'tanggal_interview'  =>$request->tanggal_interview,
            'status_marketing'=>$request->status_marketing,
            'status_form'=>$request->status_form,
            'waktu_kirim_form'=>$request->waktu_kirim_form,
        ]);
        return redirect('/list_an');
    }

    public function get_number(){
        $max_id = DB::table('rc_analisa_hasil_interview')
        ->where('id_analisa_hasil_interview', \DB::raw("(select max(`id_analisa_hasil_interview`) from wsalamatperusahaan)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_analisa_hasil_interview) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_analisa_hasil_interview, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'AHI-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }

    public function tambah() {
        $rc_an = DB::table('rc_analisa_hasil_interview')->get();
        $rc_data = DB::table('rc_data_cv_pelamar')->select('kode_cv_pelamar','pendidikan_tertinggi')->get();
        $pr = DB::table('rc_process_recruitment')->select('kode_process_recruitment', 'nama_kandidat', 'kode_cv_pelamar')->get();
        $dwk = DB::table('rc_data_wawancara_kandidat')->select('kode_process_recruitment_kandidat', 'nama_kandidat', 'kode_cv_pelamar', 'jabatan')->get();
        $rdwk['looks']=$dwk;
        $code_ = $this->get_number();        
        return view('re.ahi.tambah', ['rc_data'=>$rc_data, 'rdwk'=>$rdwk, 'rc_an'=>$rc_an, 'pr'=>$pr, 'random'=>$code_]);
    }
    
    public function hapus($id_analisa_hasil_interview){
        $del=DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
    }

    public function reset() {
        $hasPersonalTable = DB::table('rc_analisa_hasil_interview')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('rc_analisa_hasil_interview')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_an');
    }
    public function multiple_delete($id_analisa_hasil_interview){
        $del=DB::table('rc_analisa_hasil_interview')->where('id_analisa_hasil_interview', $id_analisa_hasil_interview)->delete();
        if($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
    }

}
