<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class lk_c extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wslokasikerja")->where('id_lokasi_kerja', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function lk_baru($id_lokasi_kerja)
    {
        $lk = DB::table('wslokasikerja')->where('id_lokasi_kerja', Auth::user()->id)->orderBy('id_lokasi_kerja', 'DESC')->first();
        $data_lk['looks']=$lk;
        $glk = DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', Auth::user()->id)->orderBy('id_grup_lokasi_kerja', 'DESC')->first();
        $data_glk['looks']=$glk;
        return view('ws.wslokasikerja.detail', ['wslokasikerja' => $wslokasikerja,'data_lk'=>$data_lk, 'data_glk'=>$data_glk]);
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wslokasikerja')->where('id_lokasi_kerja', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
    }
    
    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select             = json_decode($hasPersonalTable->select);
            if (!in_array('id_lokasi_kerja', $select)) {
                array_push($select, 'id_lokasi_kerja');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wslokasikerja')->select($select)
                ->leftJoin('provinces as dp', 'wslokasikerja.provinsi', '=', 'dp.prov_id')
                ->leftJoin('cities as c', 'wslokasikerja.kabupaten_kota', '=', 'c.city_id')
                ->leftJoin('districts as d', 'wslokasikerja.kecamatan', '=', 'd.dis_id')
                ->leftJoin('subdistricts as sd', 'wslokasikerja.kelurahan', '=', 'sd.subdis_id')
                ->selectRaw('wslokasikerja.*, dp.prov_name, dp.prov_id, c.city_name, c.city_id, d.dis_name, d.dis_id, sd.subdis_name, sd.subdis_id');
                
            
            if ($hasPersonalTable->kode_lokasi_kerja) {
                $query->where('kode_lokasi_kerja', $hasPersonalTable->kode_lokasi_kerja);
            }
            if ($hasPersonalTable->nama_lokasi_kerja) {
                $query->where('nama_lokasi_kerja', $hasPersonalTable->nama_lokasi_kerja);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $wslokasikerja = DB::table('wslokasikerja')
            ->leftJoin('provinces as dp', 'wslokasikerja.provinsi', '=', 'dp.prov_id')
            ->leftJoin('cities as c', 'wslokasikerja.kabupaten_kota', '=', 'c.city_id')
            ->leftJoin('districts as d', 'wslokasikerja.kecamatan', '=', 'd.dis_id')
            ->leftJoin('subdistricts as sd', 'wslokasikerja.kelurahan', '=', 'sd.subdis_id')
            ->selectRaw("wslokasikerja.*, dp.prov_name, dp.prov_id, c.city_name, c.city_id, d.dis_name, d.dis_id, sd.subdis_name, sd.subdis_id")
            ->get();

            $data = [
                'query' => $query->get(),
                'th'    => $select,
                'wslokasikerja' => $wslokasikerja,
            ];

            return view('ws.wslokasikerja.filterResult', $data);
        } else {
            $wslokasikerja = DB::table('wslokasikerja')
            
            ->get();
            
            return view('ws.wslokasikerja.index', ['wslokasikerja' => $wslokasikerja]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                    //baru
                    'disabled' =>1,
                ]);
        }

        $wslokasikerja = DB::table('wslokasikerja')->get();
        return view('ws.wslokasikerja', ['wslokasikerja' => $wslokasikerja]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Lokasi Kerja',
                'value' => 'id_lokasi_kerja'
            ],
            [
                'text'  => 'Kode Lokasi Kerja',
                'value' => 'kode_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Grup Lokasi Kerja',
                'value' => 'nama_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Id Grup Lokasi Kerja',
                'value' => 'id_grup_lokasi_kerja'
            ],
            [
                'text'  => 'ID Kantor',
                'value' => 'id_kantor'
            ],
            [
                'text'  => 'Zona Waktu',
                'value' => 'zona_waktu'
            ],
            [
                'text'  => 'Alamat',
                'value' => 'alamat'
            ],
            [
                'text'  => 'Provinsi',
                'value' => 'provinsi'
            ],
            [
                'text'  => 'Kecamatan',
                'value' => 'kecamatan'
            ],
            [
                'text'  => 'Kelurahan',
                'value' => 'kelurahan'
            ],
            [
                'text'  => 'Kabupaten Kota',
                'value' => 'kabupaten_kota'
            ],
            [
                'text'  => 'Kode Pos',
                'value' => 'kode_pos'
            ],
            [
                'text'  => 'Negara',
                'value' => 'negara'
            ],
            [
                'text'  => 'Nomor Telepon',
                'value' => 'nomor_telepon'
            ],
            [
                'text'  => 'Email',
                'value' => 'email'
            ],
            [
                'text'  => 'Fax',
                'value' => 'fax'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else { 
            foreach($test as $k => $v){
                $test[$k]['style']="display:'block';";

            }

        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];

        // $items = DB::table('wstampilantabledashboarduser')->orderBy('id', 'DESC')->first();
        // $dataitems = [
        //     'items' => $items
        // ];

        return view('ws.wslokasikerja.filter', $data);
    }

    public function detail($id_lokasi_kerja)
    {
           $wslokasikerja = DB::table('wslokasikerja')
            ->where('id_lokasi_kerja', $id_lokasi_kerja)
            ->get();
        // $wslokasikerja = DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->get();
        return view('ws.wslokasikerja.detail', ['wslokasikerja' => $wslokasikerja]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }

            // $select[] = 'id_lokasi_kerja';
        } else {
            $select = ['id_lokasi_kerja', 'kode_lokasi_kerja', 'nama_lokasi_kerja', 'nama_grup_lokasi_kerja'];
        }

        $query = DB::table('wslokasikerja')->select($select);

        if ($request->kode_lokasi_kerja) {
            $query->where('kode_lokasi_kerja', $request->kode_lokasi_kerja);
        }

        if ($request->nama_lokasi_kerja) {
            $query->where('nama_lokasi_kerja', $request->nama_lokasi_kerja);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_lk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_lk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser_lk')->insert([
            'kode_lokasi_kerja'   => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   => $request->nama_lokasi_kerja,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_lk');
    }
    public function simpan(Request $request)
    {
        DB::table('rc_lowongan_kerja')->insert([
            'kode_lokasi_kerja'   => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   => $request->nama_lokasi_kerja,
            'nama_grup_lokasi_kerja' => $request->nama_grup_lokasi_kerja,
            'id_grup_lokasi_kerja' => $request->id_grup_lokasi_kerja,
            'id_kantor'   => $request->id_kantor,
            'zona_waktu'   => $request->zona_waktu,
            'alamat'  => $request->alamat,
            'provinsi'  => $request->province_id,
            'kabupaten_kota' => $request->city_id,
            'kecamatan' => $request->dis_id,
            'kelurahan' => $request->subdis_id,
            'kode_pos' => $request->kode_pos,
            'negara' => $request->negara,
            'nomor_telepon' => $request->nomor_telepon,
            'email' => $request->email,
            'fax' => $request->fax,
            'keterangan' => $request->keterangan,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
       
        ]);

        DB::table('wstampilantabledashboarduser_lk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_lokasi_kerja'   => NULL,
                'nama_lokasi_kerja'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_lk');
    }

    public function simpan_duplicate(Request $request)
    {
        DB::table('wslokasikerja')->insert([
            'kode_lokasi_kerja'   => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   => $request->nama_lokasi_kerja,
            'id_grup_lokasi_kerja'         => $request->id_grup_lokasi_kerja,
            'id_kantor'   => $request->id_kantor,
            'zona_waktu'   => $request->zona_waktu,
            'alamat'  => $request->alamat,
            'provinsi'  => $request->provinsi,
            'kabupaten_kota' => $request->kabupaten_kota,
            'kecamatan' => $request->kecamatan,
            'kelurahan' => $request->kelurahan,
            'kode_pos' => $request->kode_pos,
            'negara' => $request->negara,
            'nomor_telepon' => $request->nomor_telepon,
            'email' => $request->email,
            'fax' => $request->fax,
            'keterangan' => $request->keterangan,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
        ]);

        DB::table('wstampilantabledashboarduser_lk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_lokasi_kerja'   => NULL,
                'nama_lokasi_kerja'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_lk');
    }

    

    public function update(Request $request)
    {
        DB::table('wslokasikerja')->where('id_lokasi_kerja', $request->id_lokasi_kerja)->update([
            'id_lokasi_kerja'         => $request->id_lokasi_kerja,
            'kode_lokasi_kerja'       => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'       => $request->nama_lokasi_kerja,
            'nama_grup_lokasi_kerja'       => $request->nama_grup_lokasi_kerja,
            'id_grup_lokasi_kerja'       => $request->id_grup_lokasi_kerja,
            'id_kantor'         => $request->id_kantor,
            'zona_waktu'   => $request->zona_waktu,
            'alamat'   => $request->alamat,
            'provinsi'  => $request->province_id,
            'kabupaten_kota' => $request->city_id,
            'kecamatan' => $request->dis_id,
            'kelurahan' => $request->subdis_id,
            'kode_pos' => $request->kode_pos,
            'negara' => $request->negara,
            'nomor_telepon' => $request->nomor_telepon,
            'email' => $request->email,
            'fax' => $request->fax,
            'keterangan' => $request->keterangan,
            'status_rekaman' => $request->status_rekaman,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
            'status_rekaman' => $request->status_rekaman,
            'tanggal_mulai_efektif' => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' => $request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_lk');
    }

    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser_lk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function tambah()
    {
        $look = DB::table('wsgruplokasikerja')->select('id_grup_lokasi_kerja', 'kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja')->get();
        $glk['looks'] = $look;
        $prov = DB::table('provinces')->selectRaw('prov_id,prov_name')->get();

        $look = DB::table('wskantorcabang')->select('id_kantor', 'kode_kantor', 'nama_kantor')->get();
        $ik['looks'] = $look;
        $code_ = $this->get_number();
        
        return view('ws.wslokasikerja.tambah', ['random'=>$code_,'glk' => $glk, 'ik' => $ik, 'prov' => $prov]);
    }

    public function get_number(){
        $max_id = DB::table('wslokasikerja')
        ->where('id_lokasi_kerja', \DB::raw("(select max(`id_lokasi_kerja`) from wslokasikerja)"))
        ->first();
        $next_ = 0;
        
        if($max_id){
            if(strlen($max_id->kode_lokasi_kerja) < 9){
                $next_ = 1;
            }else{
                $next_ = (int)substr($max_id->kode_lokasi_kerja, -3) + 1;
            }
        }else{
            $next_ = 1;
        }
        
        $code_ = 'LK-'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
        return $code_;
    }
    
   

    public function edit($id_lokasi_kerja)
    {
 
        $prov = DB::table('provinces')->selectRaw('prov_id,prov_name')->get();
        $pc = DB::table('postalcode')->select('postal_id', 'postal_code')->get();
        $data_pc['looks'] = $pc;
        $look = DB::table('wsgruplokasikerja')->select('id_grup_lokasi_kerja', 'kode_grup_lokasi_kerja','nama_grup_lokasi_kerja')->get();
        $glk['looks'] = $look;
        $wslokasikerja = DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->get();
        $look = DB::table('wskantorcabang')->select('id_kantor', 'kode_kantor')->get();
        $ik['looks'] = $look;

        return view('ws.wslokasikerja.edit', ['data_pc' => $data_pc, 'data_prov' => $prov, 'wslokasikerja' => $wslokasikerja, 'glk' => $glk, 'ik' => $ik]);
    }
    public function delete($id_lokasi_kerja)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_lk');
    }
    public function hapus($id_lokasi_kerja)
    {
        $del = DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_lk');
    }
    // public function deleteAll($id_lokasi_kerja){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_wslokasikerja');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wslokasikerja")->whereIn('id_lokasi_kerja', explode(",", $ids))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }

    
    


    // public function cari(Request $request){
    //     $cari = $request->cari;
    //     $wslokasikerja = DB::table('wslokasikerja')
    //     ->where('nama_perusahaan','like',"%".$cari."%")
    //     ->orWhere('kode_perusahaan','like',"%".$cari."%")
    //     ->orWhere('singkatan','like',"%".$cari."%")    
    //     ->orWhere('visi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('misi_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
    //     ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
    //     ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
    //     ->orWhere('lokasi_pajak','like',"%".$cari."%") 
    //     ->orWhere('npp','like',"%".$cari."%") 
    //     ->orWhere('npkp','like',"%".$cari."%") 
    //     ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
    //     ->orWhere('keterangan','like',"%".$cari."%") 
    //     ->orWhere('status_rekaman','like',"%".$cari."%")
    //     ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
    //     ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
    //     ->orWhere('pengguna_masuk','like',"%".$cari."%") 
    //     ->orWhere('waktu_masuk','like',"%".$cari."%") 
    //     ->orWhere('pengguna_ubah','like',"%".$cari."%") 
    //     ->orWhere('waktu_ubah','like',"%".$cari."%") 
    //     ->orWhere('pengguna_hapus','like',"%".$cari."%") 
    //     ->orWhere('waktu_hapus','like',"%".$cari."%") 
    //     ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
    //     ->paginate();

    //     return view('ws.wslokasikerja',['wslokasikerja' =>$wslokasikerja]);
    // }

}
