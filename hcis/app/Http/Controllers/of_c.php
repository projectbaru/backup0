<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;



class of_c extends Controller
{
    public function delete_multi(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("wsperusahaan")->where('id_perusahaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsperusahaan')->where('id_perusahaan', $request->multiDelete[$i])->delete();
        }

        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);

        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
   
    function get_tampilan($active)
    {
        return DB::table('wstampilantabledashboarduser')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function index()
    {    
        $rc_of=DB::table('rc_ontime_fulfillment')->get();
        $jumlah_permintaan = DB::table('rc_ontime_fulfillment')->sum('jumlah_permintaan');
        $karyawan_masuk = DB::table('rc_ontime_fulfillment')->sum('karyawan_masuk');
        $kekurangan = DB::table('rc_ontime_fulfillment')->sum('kekurangan');
        return view('re.of.index',['rc_of'=>$rc_of , 'jumlah_permintaan' => $jumlah_permintaan ,  'karyawan_masuk' => $karyawan_masuk ,  'kekurangan' => $kekurangan]);
    
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        $wsperusahaan = DB::table('wsperusahaan')->get();
        return view('wsperusahaan.index', ['wsperusahaan' => $wsperusahaan]);
    }

    

    public function detail($id_ontime_fulfillment)
    {
        $of = DB::table('rc_ontime_fulfillment')->where('id_ontime_fulfillment', $id_ontime_fulfillment)->get();
        
        return view('re.of.detail', ['of' => $of]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_perusahaan';
        } else {
            $select = ['id_perusahaan', 'nama_perusahaan', 'perusahaan_logo', 'visi_perusahaan', 'misi_perusahaan'];
        }

        $query = DB::table('wsperusahaan')->select($select);

        if ($request->nama_perusahaan) {
            $query->where('nama_perusahaan', $request->nama_perusahaan);
        }

        if ($request->kode_perusahaan) {
            $query->where('kode_perusahaan', $request->kode_perusahaan);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('wstampilantabledashboarduser')->insert([
            'kode_perusahaan'   => $request->kode_perusahaan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_p');
    }

    public function simpan(Request $request)
    {
        DB::table('rc_ontime_fulfillment')->insert([
            'nomor_dokumen_fptk'   => $request->nomor_dokumen_fptk,
            'nama_pemohon'   => $request->nama_pemohon,
            'jabatan_pemohon'         => $request->jabatan_pemohon,
            'departemen_pemohon'   => $request->departemen_pemohon,
            'lokasi'   => $request->lokasi,
            'jumlah_permintaan'  => $request->jumlah_permintaan,
            'karyawan_masuk' => $request->karyawan_masuk,
            'jumlah_pemenuhan' => $request->jumlah_pemenuhan,
            'kekurangan' => $request->kekurangan,
            'penggantian_tenaga_kerja' => $request->penggantian_tenaga_kerja,
            'penambahan_tenaga_kerja' => $request->penambahan_tenaga_kerja,
            'tgl_user_input_fptk' => $request->tgl_user_input_fptk,
            'tgl_approval_pic_fptk' => $request->tgl_approval_pic_fptk,
            'tgl_atasan_pic_fptk' => $request->tgl_atasan_pic_fptk,
            'tgl_approval_gm_fptk' => $request->tgl_approval_gm_fptk,
            'tanggal_masuk_fptk_hrd' => $request->tanggal_masuk_fptk_hrd,
            'tanggal_masuk_fptk_pimpinan' => $request->tanggal_masuk_fptk_pimpinan,
            'waktu_pengerjaan' => $request->waktu_pengerjaan,
            'lead_time_un' => $request->lead_time_un,
            'tanggal_target_pemenuhan' => $request->tanggal_target_pemenuhan,
            'tanggal_masuk_karyawan' => $request->tanggal_masuk_karyawan,
            'kode_process_recruitment' => $request->kode_process_recruitment,
            'kode_cv_pelamar' => $request->kode_cv_pelamar,
            'nama_karyawan' => $request->nama_karyawan,
            'referensi' => $request->referensi,
            'no_rq' => $request->no_rq,
            'keterangan' => $request->keterangan,
            'status_ach' => $request->status_ach,
            'status_rekaman' => $request->status_rekaman,
            'nama_user_input' => $request->nama_user_input,
            'waktu_user_input' => $request->waktu_user_input,
        ]);
        // $this->showAlert('success', "Berhasil salin data");


        // DB::table('wstampilantabledashboarduser')
        //     ->where('user_id', Auth::user()->id)
        //     ->update([
        //         'kode_cv_pelamar'   => NULL,
        //         'nama_lengkap'   => NULL,
        //         'query_field'       => NULL,
        //         'query_operator'    => NULL,
        //         'query_value'       => NULL,
        //     ]);

        return redirect('/list_of');
    }

    function duplicate(Request $request)
    {
        $id = $request->tg_id;

        if ($id) {
            $wsperusahaan = DB::table('wsperusahaan')
                ->where('id_perusahaan', $id)
                ->first();

            DB::table('wsperusahaan')->insert([
                'perusahaan_logo'   => $wsperusahaan->perusahaan_logo,
                'nama_perusahaan'   => $wsperusahaan->nama_perusahaan,
                'kode_perusahaan'   => $wsperusahaan->kode_perusahaan,
                'singkatan'         => $wsperusahaan->singkatan,
                'visi_perusahaan'   => $wsperusahaan->visi_perusahaan,
                'misi_perusahaan'   => $wsperusahaan->misi_perusahaan,
                'nilai_perusahaan'  => $wsperusahaan->visi_perusahaan,
                'tanggal_mulai_perusahaan' => $wsperusahaan->tanggal_mulai_perusahaan,
                'tanggal_selesai_perusahaan' => $wsperusahaan->tanggal_selesai_perusahaan,
                'jenis_perusahaan' => $wsperusahaan->jenis_perusahaan,
                'jenis_bisnis_perusahaan' => $wsperusahaan->jenis_bisnis_perusahaan,
                'jumlah_karyawan' => $wsperusahaan->jumlah_karyawan,
                'nomor_npwp_perusahaan' => $wsperusahaan->nomor_npwp_perusahaan,
                'lokasi_pajak' => $wsperusahaan->lokasi_pajak,
                'npp' => $wsperusahaan->npp,
                'npkp' => $wsperusahaan->npkp,
                'id_logo_perusahaan' => $wsperusahaan->id_logo_perusahaan,
                'tanggal_mulai_efektif' => $wsperusahaan->tanggal_mulai_efektif,
                'tanggal_selesai_efektif' => $wsperusahaan->tanggal_selesai_efektif,
                'keterangan' => $wsperusahaan->keterangan,
                'pengguna_masuk' => Auth::user()->id

            ]);

            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nama_perusahaan'   => NULL,
                    'kode_perusahaan'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);

            $this->showAlert('success', "Berhasil salin data");
            return redirect('/list_p');
        }

        $this->showAlert('error', "Data tidak ditemukan");
        return redirect('/list_p');
    }


    public function update(Request $request)
    {
        DB::table('rc_ontime_fulfillment')->where('id_ontime_fulfillment', $request->id_ontime_fulfillment)->update([
            'nomor_dokumen_fptk'   => $request->nomor_dokumen_fptk,
            'nama_pemohon'   => $request->nama_pemohon,
            'jabatan_pemohon' => $request->jabatan_pemohon,
            'departemen_pemohon'   => $request->departemen_pemohon,
            'lokasi'   => $request->lokasi,
            'jumlah_permintaan'  => $request->jumlah_permintaan,
            'karyawan_masuk' => $request->karyawan_masuk,
            'jumlah_pemenuhan' => $request->jumlah_pemenuhan,
            'kekurangan' => $request->kekurangan,
            'penggantian_tenaga_kerja' => $request->penggantian_tenaga_kerja,
            'penambahan_tenaga_kerja' => $request->penambahan_tenaga_kerja,
            'tgl_user_input_fptk' => $request->tgl_user_input_fptk,
            'tgl_approval_pic_fptk' => $request->tgl_approval_pic_fptk,
            'tgl_atasan_pic_fptk' => $request->tgl_atasan_pic_fptk,
            'tgl_approval_gm_fptk' => $request->tgl_approval_gm_fptk,
            'tanggal_masuk_fptk_hrd' => $request->tanggal_masuk_fptk_hrd,
            'tanggal_masuk_fptk_pimpinan' => $request->tanggal_masuk_fptk_pimpinan,
            'waktu_pengerjaan' => $request->waktu_pengerjaan,
            'lead_time_un' => $request->lead_time_un,
            'tanggal_target_pemenuhan' => $request->tanggal_target_pemenuhan,
            'tanggal_masuk_karyawan' => $request->tanggal_masuk_karyawan,
            'kode_process_recruitment' => $request->kode_process_recruitment,
            'kode_cv_pelamar' => $request->kode_cv_pelamar,
            'nama_karyawan' => $request->nama_karyawan,
            'referensi' => $request->referensi,
            'no_rq' => $request->no_rq,
            'keterangan' => $request->keterangan,
            'status_ach' => $request->status_ach,
        ]);
        return redirect('/list_of');
    }
    public function tambah()
    {
        // $randomId = random_int(2, 50);
        // $code_ = $this->get_number();
        // $datanum['randomId'] = $randomId;
        $rc_of=DB::table('rc_formulir_permintaan_tenaga_kerja')->select('no_dokumen')->get();
        $data_of['looks']=$rc_of;
        return view('re.of.tambah',['data_of'=>$data_of]);
    }

    // public function get_number(){
    //     $max_id = DB::table('wsperusahaan')
    //     ->where('id_perusahaan', \DB::raw("(select max(`id_perusahaan`) from wsperusahaan)"))
    //     ->first();
    //     $next_ = 0;
    //     if(strlen($max_id->kode_perusahaan) < 9){
    //         $next_ = 1;
    //     }else{
    //         $next_ = (int)substr($max_id->kode_perusahaan, -3) + 1;
    //     }
    //     $code_ = 'P'.date('ym').str_pad($next_,  3, "0", STR_PAD_LEFT);
    //     return $code_;
    // }

    public function edit($id_ontime_fulfillment)
    {
        $of = DB::table('rc_ontime_fulfillment')->where('id_ontime_fulfillment', $id_ontime_fulfillment)->get();
        return view('re.of.edit', ['of' => $of]);
    }

    public function preview()
    {
        $of=DB::table('rc_ontime_fulfillment')->get();
        $jumlah_permintaan = DB::table('rc_ontime_fulfillment')->sum('jumlah_permintaan');
        $karyawan_masuk = DB::table('rc_ontime_fulfillment')->sum('karyawan_masuk');
        $kekurangan = DB::table('rc_ontime_fulfillment')->sum('kekurangan');
        return view('re.of.preview',['of'=>$of , 'jumlah_permintaan' => $jumlah_permintaan ,  'karyawan_masuk' => $karyawan_masuk ,  'kekurangan' => $kekurangan]);
    
    }
    
    public function delete($id_perusahaan)
    {
        $data = ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    
    public function hapus($id_ontime_fulfillment)
    {
        $del =  DB::table('rc_ontime_fulfillment')->where('id_ontime_fulfillment', $id_ontime_fulfillment)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");

        // return redirect('/list_p');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 1,
                ]);
        }

        return Redirect::to('/list_wsperusahaan');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsperusahaan")->whereIn('id_perusahaan', explode(",", $ids))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }

}
