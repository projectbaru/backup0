<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class tptk_c extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_number_penggantian()
    {
        $max_id = DB::table('rc_transaksi_ptk')
            ->where('id_transaksi_ptk', \DB::raw("(select max(`id_transaksi_ptk`) from rc_transaksi_ptk)"))
            ->first();
        $next_ = 0;

        if ($max_id) {
            if (strlen($max_id->nomor_permintaan) < 9) {
                $next_ = 1;
            } else {
                $next_ = (int)substr($max_id->nomor_permintaan, -3) + 1;
            }
        } else {
            $next_ = 1;
        }

        $code_ = str_pad($next_,  4, "0", STR_PAD_LEFT) . '/' . 'TAMBAH' . '/' . 'PTK' . '/' . 'HRD' . '/' . date('Y');
        return $code_;
    }

    public function index()
    {
        $hasPersonalTable = $this->get_tampilan(1);
        if ($hasPersonalTable) {

            $select = json_decode($hasPersonalTable->select);
            if (!in_array('id_transaksi_ptk', $select)) {
                array_push($select, 'id_transaksi_ptk');
            }
            // print_r($select);

            if ($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('rc_transaksi_ptk')->select($select);

            if ($hasPersonalTable->nomor_permintaan) {
                $query->where('nomor_permintaan', $hasPersonalTable->nomor_permintaan);
            }
            if ($hasPersonalTable->tipe_transaksi) {
                $query->where('tipe_transaksi', $hasPersonalTable->tipe_transaksi);
            }
            if ($queryField) {
                $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                    for ($i = 0; $i < count($queryField); $i++) {
                        if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if ($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('re.tptk.filterResult', $data);
        } else {

            $tptk = DB::table('rc_transaksi_ptk')->get();
            return view('re.tptk.index', ['tptk' => $tptk]);
        }
    }

    public function hapus($id_transaksi_ptk)
    {
        $del = DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->delete();
        if ($del) return $this->succesWitmessage("Berhasil hapus");
        return $this->errorWithmessage("Gagal");
        // return redirect('/list_a');
    }
    public function multiDelete(Request $request)
    {
        if (!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for ($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $request->multiDelete[$i])->delete();
        }
        return response()->json([
            'status' => true,
            "data" => null,
            'msg' => "success"
        ], 200);
        // return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Nomor Permintaan',
                'value' => 'nomor_permintaan'
            ],
            [
                'text'  => 'Tipe Transaksi',
                'value' => 'tipe_transaksi'
            ],
            [
                'text'  => 'Nomor Referensi',
                'value' => 'nomor_referensi'
            ],
            [
                'text'  => 'Tanggal Permintaan',
                'value' => 'tanggal_permintaan'
            ],
            [
                'text'  => 'Tanggal Efektif Transaksi',
                'value' => 'tanggal_efektif_transaksi'
            ],
            [
                'text'  => 'Periode Tujuan',
                'value' => 'periode_tujuan'
            ],
            [
                'text'  => 'Periode Asal',
                'value' => 'periode_asal'
            ],
            [
                'text'  => 'Kode PTK Asal',
                'value' => 'kode_ptk_asal'
            ],
            [
                'text'  => 'Nama PTK Asal',
                'value' => 'nama_ptk_asal'
            ],
            [
                'text'  => 'Total PTK Asal',
                'value' => 'total_ptk_asal'
            ],
            [
                'text'  => 'Kode PTK Tujuan',
                'value' => 'kode_ptk_tujuan'
            ],
            [
                'text'  => 'Nama PTK Tujuan',
                'value' => 'nama_ptk_tujuan'
            ],
            [
                'text'  => 'Total PTK Tujuan',
                'value' => 'total_ptk_tujuan'
            ],
            [
                'text'  => 'Nama Posisi Tujuan',
                'value' => 'nama_posisi_tujuan'
            ],
            [
                'text'  => 'Nama Lokasi Kerja Tujuan',
                'value' => 'nama_lokasi_kerja_tujuan'
            ],
            [
                'text'  => 'Nama Posisi Asal',
                'value' => 'nama_posisi_asal'
            ],
            [
                'text'  => 'Nama Lokasi Kerja Asal',
                'value' => 'nama_lokasi_kerja_asal'
            ],
            [
                'text'  => 'Deskripsi',
                'value' => 'deskripsi'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Waktu User Input',
                'value' => 'waktu_user_input'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]

        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];
        $filters = false;
        //uy
        $temp = $this->get_tampilan(1);
        // $blok = count($fields);
        $blok = count($fields);
        $test = $fields;

        if ($temp) {
            $filters = true;
            $temp->query_field = json_decode($temp->query_field, true);
            $temp->query_operator = json_decode($temp->query_operator, true);
            $temp->query_value = json_decode($temp->query_value, true);
            $temp->select = json_decode($temp->select, true);

            for ($f = 0; $f < $blok; $f++) {
                $style = "display:block;";
                foreach ($temp->select as $key) {
                    if ($key == $test[$f]['value']) {
                        $style = "display:none;";
                    }
                }
                $test[$f]['style'] = $style;
            }
        } else {
            foreach ($test as $k => $v) {
                $test[$k]['style'] = "display:'block';";
            }
        }

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
            'temp' => $temp,
            'filters' => $filters,
            'damn' => $test
        ];


        return view('re.tptk.filter', $data);
    }

    public function filter_dua(Request $request)
    {
        $result = [];
        if ($request->no_dokumen && $request->pendidikan && $request->jabatan_kepegawaian && $request->lokasi_kerja) {
            $result = DB::table('rc_data_cv_pelamar')->where('no_dokumen', '=', $request->no_dokumen)->where('pendidikan', '=', $request->pendidikan)->where('jabatan_kepegawaian', $request->jabatan_kepegawaian)->where('lokasi_kerja', $request->lokasi_kerja)->get();
        } else {
            $result = DB::table('rc_data_cv_pelamar')->where('no_dokumen', '=', $request->no_dokumen)->orWhere('pendidikan', '=', $request->pendidikan)->orWhere('jabatan_kepegawaian', $request->jabatan_kepegawaian)->orWhere('lokasi_kerja', $request->lokasi_kerja)->get();
        }
        $ptk_detail = DB::table('rc_ptk_detail')->get();
        return view('re.ptk.view', ['ptk_detail' => $ptk_detail, 'data' => $result]);
    }

    public function delete($id_transaksi_ptk)
    {
        $look = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();
        Alert::warning('Sukses Hapus', '!');
        return Redirect::to('/list_tptk');
    }

    public function edit_tambahan()
    {
        $tptk_tamb = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();
        return view('re.tptk.edit_tamb', ['tptk_tamb' => $tptk_tamb]);
    }

    public function edit_peng()
    {
        $tptk_peng = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();

        return view('re.tptk.edit_peng', ['tptk_peng' => $tptk_peng]);
    }


    public function detail_tamb($id_transaksi_ptk)
    {
        $tptk_tamb = DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->get();

        return view('re.tptk.detail_t', ['tptk_tamb' => $tptk_tamb]);
    }
    public function detail_peng($id_transaksi_ptk)
    {
        $tptk_peng = DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->get();

        return view('re.tptk.detail_p', ['tptk_peng' => $tptk_peng]);
    }
    // public function detail_peng(){
    //     $tptk_peng = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();

    //     return view('re.tptk.detail_peng',['tptk_peng'=> $tptk_peng]);
    // }


    public function update(Request $request)
    {
        DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $request->id_transaksi_ptk)->update([
            'id_transaksi_ptk' => $request->id_transaksi_ptk,
            'nomor_permintaan' => $request->nomor_permintaan,
            'tipe_transaksi' => $request->tipe_transaksi,
            'nomor_referensi' => $request->nomor_referensi,
            'tanggal_permintaan' => $request->tanggal_permintaan,
            'tanggal_efektif_transaksi' => $request->tanggal_efektif_transaksi,
            'periode_tujuan' => $request->periode_tujuan,
            'periode_asal' => $request->periode_asal,
            'kode_ptk_asal' => $request->kode_ptk_asal,
            'nama_ptk_asal' => $request->nama_ptk_asal,
            'total_ptk_asal' => $request->total_ptk_asal,
            'kode_ptk_tujuan' => $request->kode_ptk_tujuan,
            'nama_ptk_tujuan' => $request->nama_ptk_tujuan,
            'total_ptk_tujuan' => $request->total_ptk_tujuan,
            'deskripsi' => $request->deskripsi,
            'keterangan' => $request->keterangan,
            'status_rekaman' => $request->status_rekaman,
            'waktu_user_input' => $request->waktu_user_input,
            'pengguna_masuk' => $request->pengguna_masuk,
            'waktu_masuk' => $request->waktu_masuk,
            'pengguna_ubah' => $request->pengguna_ubah,
            'waktu_ubah' => $request->waktu_ubah,
            'pengguna_hapus' => $request->pengguna_hapus,
            'waktu_hapus' => $request->waktu_hapus,
        ]);
        
        if (!empty($id_transaksi_ptk)) {
            DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->update($data);
            Session::flash('success', "Berhasil mengubah data.");

            if ($flag == 0) {
                $ptk_detail = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->posisi)
                    ->where('periode_ptk', $request->tahun_periode)
                    ->where('nama_lokasi_kerja', $request->lokasi_kerja)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                    ->first();
                
                if ($ptk_detail) {
                    DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->posisi)
                    ->where('periode_ptk', $request->tahun_periode)
                    ->where('nama_lokasi_kerja', $request->lokasi_kerja)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                    ->update([
                        'ptk_akhir' => $ptk_detail->ptk_akhir + $request->total_ptk,
                    ]);
                }
            }
        } else {
            $insert = DB::table('rc_transaksi_ptk')
                ->insert($data);
            if ($insert) {
                Session::flash('success', "Berhasil menyimpan data.");

                if ($flag == 0) {
                    $ptk_detail = DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_tujuan)
                        ->where('periode_ptk', $request->periode_tujuan)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                        ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                        ->first();
                    
                    if ($ptk_detail) {
                        DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_tujuan)
                        ->where('periode_ptk', $request->periode_tujuan)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                        ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                        ->update([
                            'ptk_akhir' => $ptk_detail->ptk_akhir + $request->total_ptk,
                        ]);
                    }
                }
            }

            DB::table('reftable_tptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nomor_permintaan'   => NULL,
                    'tipe_transaksi'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);  // ??            
        }
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        // return $request->all();

        if ($displayedColumn) {
            for ($i = 0; $i < count($displayedColumn); $i++) {
                array_push($select, $displayedColumn[$i]);
            }
            // $select[] = 'id_transaksi_ptk';
        } else {
            $select = ['id_transaksi_ptk', 'nomor_permintaan', 'tipe_transaksi'];
        }
        $query = DB::table('rc_transaksi_ptk')->select($select);

        if ($request->nomor_permintaan) {
            $query->where('nomor_permintaan', $request->nomor_permintaan);
        }

        if ($request->tipe_transaksi) {
            $query->where('tipe_transaksi', $request->tipe_transaksi);
        }

        if (count($queryField) > 0) {
            $query->where(function ($sub) use ($queryField, $queryOperator, $queryValue) {
                for ($i = 0; $i < count($queryField); $i++) {
                    if ($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $date . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if ($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%' . $queryValue[$i] . '%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('reftable_tptk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('reftable_tptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }

        DB::table('reftable_tptk')->insert([
            'nomor_permintaan'   => $request->nomor_permintaan,
            'tipe_transaksi'   => $request->tipe_transaksi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_tptk');
    }

    public static function getMaxCode($tambah = false)
    {
        $get = DB::table('rc_transaksi_ptk')
            ->select(DB::raw('LEFT(nomor_permintaan, 4) as max_id'));
        if ($tambah) {
            $get = $get->where('flag', 0);
            $prefix = 'TAMBAH';
        } else {
            $get = $get->where('flag', 1);
            $prefix = 'PENGGANTIAN'; 
        }
        $max_id = $get->orderBy('id_transaksi_ptk', 'DESC')->first();
        $next_ = 0;

        if ($max_id && $max_id->max_id) {
            $next_ = (int)$max_id->max_id + 1;
        } else {
            $next_ = 1;
        }
        $code_ = str_pad($next_,  4, "0", STR_PAD_LEFT) . '/' . $prefix . '/' . 'PTK' . '/' . 'HRD' . '/' . date('Y');
        return $code_;
    }


    public function tambah_tamb()
    {
        $ptk_peri= DB::table('rc_ptk_detail')->select('periode_ptk')->groupBy('periode_ptk')->get();
        $ptk_pp['looks'] = $ptk_peri;
        $ptk_pos = DB::table('rc_ptk_detail')->select('nama_posisi')->groupBy('nama_posisi')->get();
        $ptk_np['looks'] = $ptk_pos;
        $ptk_nam = DB::table('rc_ptk_detail')->select('nama_ptk')->groupBy('nama_ptk')->get();
        $ptk_nptk['looks'] = $ptk_nam;
        $ptk_naml = DB::table('rc_ptk_detail')->select('nama_lokasi_kerja')->groupBy('nama_lokasi_kerja')->get();
        $ptk_nlk['looks'] = $ptk_naml;
        $code_t = $this->getMaxCode(true);
        $code_p = $this->getMaxCode(false);        
        // $tptk_tamb = DB::table('rc_transaksi_ptk')->get();
        return view('re.tptk.tambah_tamb', ['ptk_nlk' => $ptk_nlk,'ptk_nptk' => $ptk_nptk,'ptk_np' => $ptk_np, 'ptk_pp'=>$ptk_pp, 'random' => $code_t, 'random_p' => $code_p]);
    }
    public function get_number_t()
    {
        $max_id = DB::table('rc_transaksi_ptk')
            ->select(DB::raw('MAX(id_transaksi_ptk) AS max_id'))
            ->where('tipe_transaksi', 'tambahan')
            ->first();
    
        $next_ = 0;
        if ($max_id && $max_id->max_id) {
            $next_ = (int)$max_id->max_id + 1;
        } else {
            $next_ = 1;
        }
    
        $code_ = str_pad($next_,  4, "0", STR_PAD_LEFT) . '/' . 'TAMBAH' . '/' . 'PTK' . '/' . 'HRD' . '/' . date('Y');
    
        return $code_;
    }

    public function get_number_p()
    {
        $max_id = DB::table('rc_transaksi_ptk')
            ->select(DB::raw('MAX(id_transaksi_ptk) AS max_id'))
            ->where('tipe_transaksi', 'yang penggantian')
            ->first();
    
        $next_ = 0;
        if ($max_id && $max_id->max_id) {
            $next_ = (int)$max_id->max_id + 1;
        } else {
            $next_ = 1;
        }
        $code_p = str_pad($next_,  4, "0", STR_PAD_LEFT) . '/' . 'PENGGANTIAN' . '/' . 'PTK' . '/' . 'HRD' . '/' . date('Y');
        return $code_p;
    }
    public function tambah_peng()
    {
        $ptk = DB::table('rc_ptk_detail')->select('periode_ptk', 'nama_posisi', 'nama_lokasi_kerja')->get();
        // $rc_transaksi = DB::table('rc_transaksi_ptk')->select('id_transaksi_ptk')->get();
        $tptk_peng = DB::table('rc_transaksi_ptk')->get();
        return view('re.tptk.tambah_peng', ['tptk_peng ' => $tptk_peng, 'ptk' => $ptk]);
    }

    public function update_id($id_transaksi_ptk, Request $request)
    {
        DB::table('rc_transaksi_tpk')
            ->where('id_transaksi_ptk', $id_transaksi_ptk)
            ->update([
                'nomor_permintaan' => $request->nomor_permintaan,
                'tipe_transaksi'   => $request->tipe_transaksi,
                'nomor_referensi'   => $request->nomor_referensi,
                'tanggal_permintaan'   => $request->tanggal_permintaan,
                'tanggal_efektif_transaksi'   => $request->tanggal_efektif_transaksi,
                'periode_tujuan'   => $request->periode_tujuan,
                'periode_asal'   => $request->periode_asal,
                'kode_ptk_asal'   => $request->kode_ptk_asal,
                'nama_ptk_asal'   => $request->nama_ptk_asal,
                'total_ptk_asal'   => $request->total_ptk_asal,
                'kode_ptk_tujuan'   => $request->kode_ptk_tujuan,
                'nama_ptk_tujuan'   => $request->nama_ptk_tujuan,
                'total_ptk_tujuan'   => $request->total_ptk_tujuan,
                'nama_posisi_tujuan' => $request->nama_posisi_tujuan,
                'nama_lokasi_kerja_tujuan' => $request->nama_lokasi_kerja_tujuan
            ]);
        return Redirect::to('/list_tptk');
    }


    public function simpan(Request $request)
    {
        // return response()->json(['data' => $request->all()], 200);

        $validator = Validator::make($request->all(), [
            'ischeckedTipe' => 'required|integer',
            'nomor_permintaan_tambahan' => 'required_if:ischeckedTipe,==,0',
            'nomor_permintaan_pergantian' => 'required_if:ischeckedTipe,==,1',
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors()->first();
            return $msg;
            return Redirect::back()->withErrors(['msg' => $msg]);
        }

        $nomor_permintaan = null;
        if ($request->ischeckedTipe === '0') {
            $tipe_transaksi = 'tambahan';
            $nomor_permintaan = $request->nomor_permintaan_tambahan;
        }

        if ($request->ischeckedTipe === '1') {
            $tipe_transaksi = 'yang penggantian';
            $nomor_permintaan = $request->nomor_permintaan_pergantian;
        }

        $flag = $request->ischeckedTipe;
        $id_transaksi_ptk = $request->id_transaksi_ptk;
        $data = [
            'nomor_permintaan' => $nomor_permintaan,
            'tipe_transaksi' => $tipe_transaksi,
            'nomor_referensi'   => $request->nomor_referensi,
            'tanggal_permintaan'   => $request->tanggal_permintaan,
            'tanggal_efektif_transaksi'   => $request->tanggal_efektif_transaksi,
            'tanggal_mulai_efektif'   => $request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif,
            'keterangan'   => $request->keterangan,
            'flag' => $flag
        ];

        if ($flag == 0) {
            
            $data['nama_ptk_tujuan'] = $request->nama_ptk_tujuan_ptkt;
            $data['total_ptk_tujuan'] = $request->total_ptk;
            $data['deskripsi'] = $request->deskripsi;
            $data['periode_tujuan'] = $request->periode_tujuan;
            $data['nama_posisi_tujuan'] = $request->nama_posisi_tujuan;
            $data['nama_lokasi_kerja_tujuan'] = $request->nama_lokasi_kerja_tujuan;
        }
        if ($flag == 1) {
            $data['nama_ptk_tujuan'] = $request->nama_ptk_tujuan_ptkp;
            $data['total_ptk_tujuan'] = $request->total_ptk_tujuan;
            $data['periode_tujuan'] = $request->periode_tujuan_peng;
            $data['nama_posisi_tujuan'] = $request->nama_posisi_tujuan_peng;
            $data['nama_lokasi_kerja_tujuan'] = $request->nama_lokasi_kerja_tujuan_peng;

            $data['nama_ptk_asal'] = $request->nama_ptk_asal;
            $data['periode_asal'] = $request->periode_asal;
            $data['nama_posisi_asal'] = $request->nama_posisi_asal;
            $data['nama_lokasi_kerja_asal'] = $request->nama_lokasi_kerja_asal;
            $data['deskripsi'] = $request->deskripsi_ptkp;
        }

       

        DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->update($data);
        if (!empty($id_transaksi_ptk)) {
            DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->update($data);
            Session::flash('success', "Berhasil mengubah data.");

            if ($flag == 0) {
                $ptk_detail = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_tujuan)
                    ->where('periode_ptk', $request->periode_tujuan)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                    ->first();
                
                if ($ptk_detail) {
                    DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_tujuan)
                    ->where('periode_ptk', $request->periode_tujuan)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                    ->update([
                        'ptk_tambahan' => $ptk_detail->ptk_tambahan + $request->total_ptk,
                    ]);
                }
            }

            if ($flag == 1) {
                $ptk_detailtujuan = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_tujuan_peng)
                    ->where('periode_ptk', $request->periode_tujuan_peng)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan_peng)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkp)
                    ->first();
                
                if ($ptk_detailtujuan) {
                    DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_tujuan_peng)
                    ->where('periode_ptk', $request->periode_tujuan_peng)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan_peng)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkp)
                    ->update([
                        'ptk_tambahan' => $ptk_detailtujuan->ptk_tambahan + $request->total_ptk_tujuan,
                    ]);
                }

                // PTK ASAL
                $ptk_detailasal = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_asal)
                    ->where('periode_ptk', $request->periode_asal)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_asal)
                    ->where('nama_ptk', $request->nama_ptk_asal)
                    ->first();
                
                if ($ptk_detailasal) {
                    DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_asal)
                    ->where('periode_ptk', $request->periode_asal)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_asal)
                    ->where('nama_ptk', $request->nama_ptk_asal)
                    ->update([
                        'ptk_yang_digantikan' => $ptk_detailasal->ptk_yang_digantikan + $request->total_ptk_tujuan,
                    ]);
                }
            }
        } else {
            $insert = DB::table('rc_transaksi_ptk')
                ->insert($data);
            if ($insert) {
                Session::flash('success', "Berhasil menyimpan data.");

                if ($flag == 0) {
                    $ptk_detail = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_tujuan)
                    ->where('periode_ptk', $request->periode_tujuan)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                    ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                        ->first();
                    
                    if ($ptk_detail) {
                        DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_tujuan)
                        ->where('periode_ptk', $request->periode_tujuan)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan)
                        ->where('nama_ptk', $request->nama_ptk_tujuan_ptkt)
                        ->update([
                            'ptk_tambahan' => $ptk_detail->ptk_tambahan + $request->total_ptk,
                        ]);
                    }
                }
                if ($flag == 1) {
                    $ptk_detailtujuan = DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_tujuan_peng)
                        ->where('periode_ptk', $request->periode_tujuan_peng)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan_peng)
                        ->where('nama_ptk', $request->nama_ptk_tujuan_ptkp)
                        ->first();
                    
                    if ($ptk_detailtujuan) {
                        DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_tujuan_peng)
                        ->where('periode_ptk', $request->periode_tujuan_peng)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_tujuan_peng)
                        ->where('nama_ptk', $request->nama_ptk_tujuan_ptkp)
                        ->update([
                            'ptk_tambahan' => $ptk_detailtujuan->ptk_tambahan + $request->total_ptk_tujuan,
                        ]);
                    }
                    $ptk_detailasal = DB::table('rc_ptk_detail')
                    ->where('nama_posisi', $request->nama_posisi_asal)
                    ->where('periode_ptk', $request->periode_asal)
                    ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_asal)
                    ->where('nama_ptk', $request->nama_ptk_asal)
                    ->first();
                
                    if ($ptk_detailasal) {
                        DB::table('rc_ptk_detail')
                        ->where('nama_posisi', $request->nama_posisi_asal)
                        ->where('periode_ptk', $request->periode_asal)
                        ->where('nama_lokasi_kerja', $request->nama_lokasi_kerja_asal)
                        ->where('nama_ptk', $request->nama_ptk_asal)
                        ->update([
                            'ptk_yang_digantikan' => $ptk_detailasal->ptk_yang_digantikan + $request->total_ptk_tujuan,
                        ]);
                    }
                }
            }

            DB::table('reftable_tptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'nomor_permintaan'   => NULL,
                    'tipe_transaksi'   => NULL,
                    'query_field'       => NULL,
                    'query_operator'    => NULL,
                    'query_value'       => NULL,
                ]);  // ??            
        }
        // if (!empty($id_transaksi_ptk)) {
        //     DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id_transaksi_ptk)->update($data);
        //     Session::flash('success', "Berhasil mengubah data.");
        // } else {
        //     $insert = DB::table('rc_transaksi_ptk')
        //         ->insert($data);
        //     if ($insert) {
        //         Session::flash('success', "Berhasil menyimpan data.");
        //     }

        //     DB::table('reftable_tptk')
        //         ->where('user_id', Auth::user()->id)
        //         ->update([
        //             'nomor_permintaan'   => NULL,
        //             'tipe_transaksi'   => NULL,
        //             'query_field'       => NULL,
        //             'query_operator'    => NULL,
        //             'query_value'       => NULL,
        //         ]);     
        // }

        // $select_ptk= DB::table('rc_ptk_detail')
        // ->select('ptk_akhir')
        // ->get();
        
        // DB::table('rc_ptk_detail')
        // ->where('periode_ptk', '=', $request->periode_tujuan)->where('nama_posisi', '=', $request->nama_posisi_tujuan)->where('nama_ptk', '=', $request->nama_ptk_tujuan_ptkt)
        // ->update([
        //     'ptk_akhir' =>$select_ptk+$request->total_ptk,
        // ]);
        
        // foreach ($insert as $d) {
        //     for($a = 1; $a <= 12; $a++ ){
        //         $periode = $request->tahun.str_pad($a,2,'0', STR_PAD_LEFT);
        //         DB::table('rc_ptk_detail')->insert([
        //             'ptk_akhir' => $d->ptk_akhir,
        //         ]);
        //     }
        // }
        
        return redirect('/list_tptk');
    }

    function show($id)
    {
       
        $ptk_peri= DB::table('rc_ptk_detail')->select('periode_ptk')->groupBy('periode_ptk')->get();
        $ptk_pp['looks'] = $ptk_peri;
        $ptk_pos = DB::table('rc_ptk_detail')->select('nama_posisi')->groupBy('nama_posisi')->get();
        $ptk_np['looks'] = $ptk_pos;
        $ptk_nam = DB::table('rc_ptk_detail')->select('nama_ptk')->groupBy('nama_ptk')->get();
        $ptk_nptk['looks'] = $ptk_nam;
        $ptk_naml = DB::table('rc_ptk_detail')->select('nama_lokasi_kerja')->groupBy('nama_lokasi_kerja')->get();
        $ptk_nlk['looks'] = $ptk_naml;
        
        $get = DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id)->first();
        if (!$get) {
            return redirect('list_tptk')->withErrors(['msg' => "Data tidak ditemukan."]);
        }
        $get->sub = $get->flag == 0 ? 'Tambahan' : 'Pergantian';
        $code_t = $this->getMaxCode(true);
        $code_p = $this->getMaxCode(false);

        return view('re.tptk.detail', ['ptk_nlk' => $ptk_nlk,'ptk_nptk' => $ptk_nptk,'ptk_np' => $ptk_np, 'ptk_pp'=>$ptk_pp,'data' => $get, 'random_t' => $code_t, 'random_p' => $code_p]);
    }

    function edit($id)
    {

        $ptk_peri= DB::table('rc_ptk_detail')->select('periode_ptk')->groupBy('periode_ptk')->get();
        $ptk_pp['looks'] = $ptk_peri;
        $ptk_pos = DB::table('rc_ptk_detail')->select('nama_posisi')->groupBy('nama_posisi')->get();
        $ptk_np['looks'] = $ptk_pos;
        $ptk_nam = DB::table('rc_ptk_detail')->select('nama_ptk')->groupBy('nama_ptk')->get();
        $ptk_nptk['looks'] = $ptk_nam;
        $ptk_naml = DB::table('rc_ptk_detail')->select('nama_lokasi_kerja')->groupBy('nama_lokasi_kerja')->get();
        $ptk_nlk['looks'] = $ptk_naml;
        
        

        $get = DB::table('rc_transaksi_ptk')->where('id_transaksi_ptk', $id)->first();
        if (!$get) {
            return redirect('list_tptk')->withErrors(['msg' => "Data tidak ditemukan."]);
        }
        $get->sub = $get->flag == 0 ? 'Tambahan' : 'Pergantian';
        $code_t = $this->getMaxCode(true);
        $code_p = $this->getMaxCode(false);

        return view('re.tptk.edit', [ 'ptk_nlk' => $ptk_nlk,'ptk_nptk' => $ptk_nptk,'ptk_np' => $ptk_np, 'ptk_pp'=>$ptk_pp,'data' => $get, 'random_t' => $code_t, 'random_p' => $code_p]);
    }

    public function duplicate(Request $request)
    {
        DB::table('rc_transaksi_ptk')->insert([
            'nomor_permintaan' => $request->nomor_permintaan,
            'tipe_transaksi'   => $request->tipe_transaksi,
            'nomor_referensi'   => $request->nomor_referensi,
            'tanggal_permintaan'   => $request->tanggal_permintaan,
            'tanggal_efektif_transaksi'   => $request->tanggal_efektif_transaksi,
            'periode_tujuan'   => $request->periode_tujuan,
            'periode_asal'   => $request->periode_asal,
            'kode_ptk_asal'   => $request->kode_ptk_asal,
            'nama_ptk_asal'   => $request->nama_ptk_asal,
            'total_ptk_asal'   => $request->total_ptk_asal,
            'kode_ptk_tujuan'   => $request->kode_ptk_tujuan,
            'nama_ptk_tujuan'   => $request->nama_ptk_tujuan,
            'total_ptk_tujuan'   => $request->total_ptk_tujuan,
            'nama_posisi_tujuan' => $request->nama_posisi_tujuan,
            'nama_lokasi_kerja_tujuan' => $request->nama_lokasi_kerja_tujuan,
            'nama_posisi_asal' => $request->nama_posisi_asal,
            'nama_lokasi_kerja_asal' => $request->nama_lokasi_kerja_asal,
            'deskripsi'   => $request->deskripsi,
            'keterangan'   => $request->keterangan,
            'status_rekaman'   => $request->status_rekaman,
            'waktu_user_input'   => $request->waktu_user_input,
            // 'pengguna_masuk'   =>$request->pengguna_masuk,
            // 'waktu_masuk'   =>$request->waktu_masuk,
            // 'pengguna_ubah'   =>$request->pengguna_ubah,
            // 'waktu_ubah'   =>$request->waktu_ubah,
            // 'pengguna_hapus'   =>$request->pengguna_hapus,
            // 'waktu_hapus'   =>$request->waktu_hapus,  
        ]);

        DB::table('rc_transaksi_ptk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nomor_permintaan'   => NULL,
                'tipe_transaksi'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_tptk');
    }

    public function reset()
    {
        $personalTable = DB::table('wstampilantabledashboarduser_tptk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        return Redirect::to('/list_tptk');
    }

    public function deleteMulti(Request $request)
    {
        foreach ($request->selectedws as $selected) {
            DB::table("rc_transaksi_ptk")->where('id_transaksi_ptk', '=', $selected)->delete();
        }

        return redirect()->back();
    }


    function get_tampilan($active)
    {
        return DB::table('reftable_tptk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tptk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if ($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tptk')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'active'   => 0,
                ]);
        }
        $tptk = DB::table('rc_transaksi_ptk')->get();
        return view('re.tptk.detail', ['tptk' => $tptk]);
    }

    public function multiDelete_id(Request $request)
    {
        $tptk = DB::table('rc_transaksi_ptk')->get();
        return view('re.tptk.detail', ['tptk' => $tptk]);
    }

    function get_tampilan_id($active)
    {
        return DB::table('wstampilantabledashboarduser_tptk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id_transaksi_ptk', 'DESC')
            ->first();
    }
    function get_tampilan_end($active)
    {
        return DB::table('wstampilantabledashboarduser_tptk')
            ->where(['user_id' => Auth::user()->id, 'active' => $active])
            ->orderBy('id_transaksi_ptk', 'DESC')
            ->first();
    }
}
