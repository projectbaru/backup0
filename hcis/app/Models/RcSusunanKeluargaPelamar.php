<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcSusunanKeluargaPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_susunan_keluarga_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kode_susunan_keluarga_pelamar',
        'hubungan_keluarga',
        'nama_keluarga',
        'jenis_kelamin',
        'usia_tahun_lahir',
        'pendidikan_terakhir',
        'jabatan',
        'perusahaan',
        'keterangan_susunan_keluarga',
    ];
}
