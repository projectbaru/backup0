<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcJawabanPilihan extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_jawaban_pilihan';
    protected $fillable = [
        'id_data_pelamar',
        'jawaban',
        'penjelasan',
    ];
}
