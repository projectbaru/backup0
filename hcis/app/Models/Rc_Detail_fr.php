<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rc_Detail_fr extends Model
{
    use HasFactory;
    protected $table = 'rc_detail_fulfillment_rate';
    protected  $primaryKey = 'id_fulfillment_rate';

    

    
    public function fulfillmentRate()
    {
        return $this->belongsTo(Rc_fr::class, 'id_fulfillment_rate');
    }

    
}
