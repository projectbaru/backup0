<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StepProcessRecruitment extends Model
{
    use HasFactory;

    protected $table = 'rc_step_process_recruitment';

    protected $guarded = [
        'id_step_process_recruitment',
    ];

    public function details()
    {
        return $this->hasMany(DetailStepProcessRecruitment::class, 'kode_step_process_recruitment', 'kode_step_process_recruitment');
    }

}
