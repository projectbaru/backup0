<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fdp extends Model
{
    use HasFactory;

    protected $table = 'rc_form_data_pelamar';

    protected $guarded = [
        'id_data_pelamar',
    ];

    public function details()
    {
        return $this->hasMany(SKP::class, 'id_data_pelamar', 'id_data_pelamar');
        
    }

}
