<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BRData extends Model
{
    use HasFactory;

    protected $table = 'rc_data_budgeting_recruitment';
    protected  $primaryKey = 'id_data_budgeting_recruitment';
}
