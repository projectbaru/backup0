<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailStepProcessRecruitment extends Model
{
    use HasFactory;

    protected $table = 'rc_detail_step_process_recruitment';

    protected $guarded = [
        'id_detail_step_process_recruitment',
    ];

    public function parent()
    {
        return $this->belongsTo(StepProcessRecruitment::class, 'kode_step_process_recruitment', 'kode_step_process_recruitment');
    }
}
