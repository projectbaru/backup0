<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SKP extends Model
{
    use HasFactory;

    protected $table = 'rc_susunan_keluarga_pelamar';

    protected $guarded = [
        'id_susunan_keluarga_pelamar',
    ];

    public function parent()
    {
        return $this->belongsTo(StepProcessRecruitment::class, 'id_data_pelamar', 'id_data_pelamar');
    }
}
