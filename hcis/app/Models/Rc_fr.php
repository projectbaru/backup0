<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rc_fr extends Model
{
    use HasFactory;

    protected $table = 'rc_fulfillment_rate';
    protected  $primaryKey = 'id_fulfillment_rate';


    public function detailFulfillmentRate()
    {
        return $this->hasMany(Rc_Detail_fr::class, 'id_fulfillment_rate');
    }
}
