<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcPelatihanPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_pelatihan_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kode_pelatihan_pelamar',
        'bidang_pelatihan',
        'penyelenggara',
        'tempat_kota',
        'lama_pelatihan',
        'tahun_pelatihan',
        'dibiayai_oleh',
    ];
}
