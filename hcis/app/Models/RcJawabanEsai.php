<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcJawabanEsai extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_jawaban_esai';
    protected $fillable = [
        'id_data_pelamar',
        'id_pertanyaan_pilihan_esai',
        'jawaban',
    ];

    public function pertanyaan_pilihan_esai()
    {
        return $this->belongsTo(RcPertanyaanPilihanEsai::class, 'id_pertanyaan_pilihan_esai', 'id_pertanyaan_pilihan_sesai');
    }
}
