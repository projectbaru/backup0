<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ATCBQuarter extends Model
{
    use HasFactory;

    protected $table = 'rc_quarter_divisi_applicant_target_candidat_buffer';
    protected  $primaryKey = 'id_quarter_divisi_applicant_target';
}
