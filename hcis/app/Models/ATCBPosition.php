<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ATCBPosition extends Model
{
    use HasFactory;

    protected $table = 'rc_position_applicant_target_candidat_buffer';
    protected  $primaryKey = 'id_position_applicant_target';
    protected $with = ['divisi', 'quarters'];

    public function divisi()
    {
        return $this->belongsTo(ATCBDivisi::class, 'id_divisi_applicant_target', 'id_divisi_applicant_target');
    }

    public function quarters()
    {
        return $this->hasMany(ATCBQuarter::class, 'id_position_applicant_target', 'id_position_applicant_target');
    }
}
