<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ATCB extends Model
{
    use HasFactory;

    protected $table = 'rc_applicant_target_candidat_buffer';
    protected  $primaryKey = 'id_applicant_target';

    public function more_details()
    {
        return $this->hasManyThrough(
          ATCBPosition::class, 
          ATCBDivisi::class, 
          'id_applicant_target', // id foreignkey intermediet model 
          'id_divisi_applicant_target', // id foreignkey final model
          'id_applicant_target', // id local 
          'id_divisi_applicant_target', // id local intermediete model
        );
    }

    public function divisions()
    {
        return $this->hasMany(ATCBDivisi::class, 'id_applicant_target', 'id_applicant_target');
    }

}
