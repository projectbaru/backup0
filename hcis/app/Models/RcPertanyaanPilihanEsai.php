<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcPertanyaanPilihanEsai extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_pertanyaan_pilihan_esai';
    protected $fillable = [
        'kode_pertanyaan_marston_model',
        'pertanyaan',
        'kategori_pertanyaan',
    ];
}
