<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcRiwayatKerjaPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_riwayat_kerja_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kode_riwayat_kerja_pelamar',
        'nama_perusahaan',
        'alamat',
        'nama_atasan',
        'lama_bekerja',
        'gaji_terakhir',
        'jenis_usaha',
        'alasan_berhenti',
        'jabatan_awal',
        'jabatan_akhir',
        'telp_perusahaan',
        'nama_direktur',
    ];
}
