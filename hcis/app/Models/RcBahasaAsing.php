<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcBahasaAsing extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_bahasa_asing';
    protected $fillable = [
        'id_data_pelamar',
        'kode_bahasa_asing_pelamar',
        'bahasa',
        'mendengar',
        'berbicara',
        'membaca',
        'menulis',
    ];
}
