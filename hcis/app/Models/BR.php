<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BR extends Model
{
    use HasFactory;

    protected $table = 'rc_budgeting_recruitment';
    protected  $primaryKey = 'id_budgeting_recruitment';

    public function more_details()
    {
        return $this->hasMany(BRData::class, 'kode_budgeting_recruitment', 'kode_budgeting_recruitment');
    }
}
