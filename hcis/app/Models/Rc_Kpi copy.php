<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rc_Kpi extends Model
{
    use HasFactory;

    protected $table = 'rc_kpi_hr_recruitment';

    protected $guarded = [
        'id_kpi_hr_recruitment',
        'id_pencapaian_kpi_hr_recruitment'
    ];

}
