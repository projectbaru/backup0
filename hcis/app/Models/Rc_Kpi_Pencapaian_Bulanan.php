<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rc_Kpi_Pencapaian_Bulanan extends Model
{
    use HasFactory;

    protected $table = 'rc_kpi_hr_recruitment_pencapaian_bulanan';

    protected $guarded = [
        'id',
        'id_kpi_hr_recruitment'
    ];   
}
