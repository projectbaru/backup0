<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcNegatifPositifPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_negatif_positif_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kategori',
        'keterangan',
    ];
}
