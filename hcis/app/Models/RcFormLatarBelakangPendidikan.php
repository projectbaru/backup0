<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcFormLatarBelakangPendidikan extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_form_latar_belakang_pendidikan';
    protected $fillable = [
        'id_data_pelamar',
        'kode_pendidikan_pelamar',
        'tingkat_pendidikan',
        'nama_sekolah',
        'jurusan',
        'tempat_kota',
        'tahun_masuk',
        'tahun_keluar',
        'jabatan',
        'status_kelulusan',
    ];
}
