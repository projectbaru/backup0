<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JWK extends Model
{
    use HasFactory;
    protected $table='rc_data_jadwal_wawancara_kandidat';
}
