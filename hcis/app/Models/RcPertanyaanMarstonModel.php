<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcPertanyaanMarstonModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_pertanyaan_marston_model';
    protected $fillable = [
        'kode_pertanyaan_marston_model',
        'rston_model',
        'pertanyaan',
    ];
}
