<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcJawabanMarstonModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_jawaban_marston_model';
    protected $fillable = [
        'id_data_pelamar',
        'nama_lengkap',
        'jawaban',
    ];
}
