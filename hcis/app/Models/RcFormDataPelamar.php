<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcFormDataPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_form_data_pelamar';
    protected $fillable = [
        'status',
        'kode_cv_pelamar',
        'kode_data_pelamar',
        'posisi_lamaran',
        'pendidikan_terakhir',
        'nama_lengkap',
        'nama_panggilan',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'kebangsaan',
        'jenis_kelamin',
        'berat_badan',
        'tinggi_badan',
        'no_ktp',
        'email_pribadi',
        'alamat_sosmed',
        'kategori_sim',
        'no_sim',
        'no_hp',
        'no_telepon',
        'status_kendaraan',
        'jenis_merk_kendaraan',
        'tempat_tinggal',
        'status_perkawinan',
        'alamat_didalam_kota',
        'kode_pos_1',
        'alamat_diluar_kota',
        'kode_pos_2',
        'pas_foto',
        'hoby_dan_kegiatan',
        'kegiatan_membaca',
        'surat_kabar',
        'majalah',
        'pokok_yang_dibaca',
        'tugas_jabatan_terakhir	',
        'tanggal_lamar',
        'gambar_denah',
        'tempat_ttd',
        'tanda_tangan',
        'status_rekaman',
    ];

    public function susunan_keluarga()
    {
        return $this->hasMany(RcSusunanKeluargaPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }

    public function latar_belakang_pendidikan()
    {
        return $this->hasMany(RcFormLatarBelakangPendidikan::class, 'id_data_pelamar', 'id_data_pelamar');
    }

    public function pelatihan()
    {
        return $this->hasMany(RcPelatihanPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }

    public function bahasa_asing()
    {
        return $this->hasMany(RcBahasaAsing::class, 'id_data_pelamar', 'id_data_pelamar');
    }

    public function organisasi()
    {
        return $this->hasMany(RcOrganisasiPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function riwayat_kerja()
    {
        return $this->hasMany(RcRiwayatKerjaPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function referensi_pelamar()
    {
        return $this->hasMany(RcKontakReferensiPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function kontak_darurat()
    {
        return $this->hasMany(RcKontakDarurat::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function negatif_positif()
    {
        return $this->hasMany(RcNegatifPositifPelamar::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function jawaban_pilihan()
    {
        return $this->hasMany(RcJawabanPilihan::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    public function jawaban_esai()
    {
        return $this->hasMany(RcJawabanEsai::class, 'id_data_pelamar', 'id_data_pelamar');
    }

    public function jawaban_marston()
    {
        return $this->hasMany(RcJawabanMarstonModel::class, 'id_data_pelamar', 'id_data_pelamar');
    }
    
}
