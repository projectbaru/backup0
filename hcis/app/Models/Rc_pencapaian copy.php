<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rc_pencapaian extends Model
{
    use HasFactory;

    protected $table = 'rc_pencapaian_kpi_hr_recruitment';
    protected  $primaryKey = 'id_pencapaian_kpi_hr_recruitment';

    protected $guarded = [
        'id_pencapaian_kpi_hr_recruitment',
    ];

    public function more_details()
    {
        return $this->hasManyThrough(
          Rc_Kpi_Pencapaian_Bulanan::class, 
          Rc_Kpi::class, 
          'id_pencapaian_kpi_hr_recruitment', // id foreignkey intermediet model 
          'id_kpi_hr_recruitment', // id foreignkey final model
          'id_pencapaian_kpi_hr_recruitment', // id local 
          'id_kpi_hr_recruitment', // id local intermediete model
        );
    }

}
