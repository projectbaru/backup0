<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ATCBDivisi extends Model
{
    use HasFactory;

    protected $table = 'rc_divisi_applicant_target_candidat_buffer';
    protected  $primaryKey = 'id_divisi_applicant_target';

    public function positions()
    {
        return $this->hasMany(ATCBPosition::class, 'id_divisi_applicant_target', 'id_divisi_applicant_target');
    }
}
