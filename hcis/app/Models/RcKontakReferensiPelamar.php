<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcKontakReferensiPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_kontak_refrensi_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kode_kontak_refrensi_pelamar',
        'nama_lengkap',
        'alamat_telpon',
        'pekerjaan',
        'hubungan',
    ];
}
