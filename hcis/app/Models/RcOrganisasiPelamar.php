<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcOrganisasiPelamar extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_organisasi_pelamar';
    protected $fillable = [
        'id_data_pelamar',
        'kode_organisasi_pelamar',
        'nama_organisasi',
        'jenis_kegiatan',
        'jabatan',
        'tahun',
    ];
}
