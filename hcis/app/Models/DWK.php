<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DWK extends Model
{
    use HasFactory;
    protected $table='rc_data_wawancara_kandidat';

    public function list_jadwal_wawancara(){
        return $this->hasMany(JWK::class, 'id_data_wawancara_kandidat', 'id_data_wawancara_kandidat');
    }
}
