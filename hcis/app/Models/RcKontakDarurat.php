<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcKontakDarurat extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'rc_kontak_darurat';
    protected $fillable = [
        'id_data_pelamar',
        'kode_kontak_darurat',
        'nama_lengkap',
        'alamat_telpon',
        'pekerjaan',
        'hubungan',
    ];
}
