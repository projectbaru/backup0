<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PDWK extends Model
{
    use HasFactory;
    protected $table='rc_penyiapan_data_wawancara_kandidat';

    public function list_dwk(){
        return $this->hasMany(DWK::class, 'id_penyiapan_dwk', 'id_penyiapan_dwk');
    }

    
    public function list_pdwk(){
        return $this->hasMany(DWK::class, 'id_penyiapan_dwk', 'id_penyiapan_dwk');
    }
}
