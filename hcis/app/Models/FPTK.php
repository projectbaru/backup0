<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FPTK extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $table = 'rc_formulir_permintaan_tenaga_kerja';

    // public function list_dwk(){
    //     return $this->hasMany(DWK::class, 'id_penyiapan_dwk', 'id_penyiapan_dwk');
    // }

    
    // public function list_pdwk(){
    //     return $this->hasMany(DWK::class, 'id_penyiapan_dwk', 'id_penyiapan_dwk');
    // }
}
