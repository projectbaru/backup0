<?php

namespace App\Helpers;

class HelperFunctions {

    public static function callAPI2($method, $url, $data){
        $curl = curl_init();
        switch ($method){
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }	
        // OPTIONS:
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoIjoiZGV2X21vYmlsZV90YW1fc2UiLCJpYXQiOjE1NzYxMzIxNzJ9.d-s_0FfLfr62LQoMLg6HQ8hgEP_VZJrLLWBAra34Mfg',
            'Content-Type: application/json',
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            // EXECUTE:
            $result = curl_exec($curl);
            if(!$result){die("Connection Failure");}
            curl_close($curl);
            return $result;   
    }

    public static function find_ad_user_id($nik) {
        $body =  array("search" => $nik);	
        $post_data = HelperFunctions::callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/ad_user_search', json_encode($body));
        $resp = json_decode($post_data, true); 
        $content = $resp['content'];
        $ad_user_id = $content == "exist" ? $resp['response'][0]['ad_user_id'] : "";
        return($ad_user_id);
    }

    public static function find_nik_idempiere($kode) {
        $body =  array("search" => $kode);	
        $post_data = HelperFunctions::callAPI2('POST', 'http://10.10.1.237:3000/api/idempiere/wfl/ad_user_search', json_encode($body));
        $resp = json_decode($post_data, true); 
        $content = $resp['content'];
        $value = $content == "exist" ? $resp['response'][0]['value'] : '';
        return($value);
    }
    

}
