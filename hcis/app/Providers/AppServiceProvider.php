<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\HelperFunctions;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (file_exists($file = app_path('Helpers/HelperFunctions.php'))) {
            require_once($file);
        }
    }
}
