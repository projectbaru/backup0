@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Posisi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<div class="container" style="width:95%;">
<form action="{{route('simpan_usm')}}" method="post">
<hr>    
{{ csrf_field() }}
    @foreach($wsusm as $data)
    <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_standar_upah_minimum }}">

    <table>
        <tr>
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_usm', $data->id_standar_upah_minimum)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;"  id="btn_delete" href="{{route('hapus_usm', $data->id_standar_upah_minimum)}}">Hapus</a></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_usm')}}">Batal</a></td>
            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <!-- <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
            </td> -->
        </tr>
    </table><br>
        <div class="row">
            <div class="col-md-3" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Kode Posisi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_posisi" value="KP-{{now()->isoFormat('dmYYs')}}">{{$data->kode_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Nama Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Pendidikan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tingkat_pendidikan" value="{{$data->tingkat_pendidikan}}">{{$data->tingkat_pendidikan}}</td>
                    </tr>
                </table>
            </div>
            
            <div class="col-md-9">
                <table style="font-size:12px;">
                    <tr>
                        <td>Kode Lokasi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_lokasi" value="{{$data->kode_lokasi}}">{{$data->kode_lokasi}}</td>
                    </tr>
                    <tr>
                        <td>Nama Lokasi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_lokasi" value="{{$data->nama_lokasi}}">{{$data->nama_lokasi}}</td>
                    </tr>
                    <tr>
                        <td>Upah Minimum</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="upah_minimum" value="{{$data->upah_minimum}}">{{$data->upah_minimum}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        <b style="color:black;">Informasi Lainnya</b>
        <div class="row">
            <div class="col">
                <table style="font-size:12px;">
                    <tr>
                        <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
    @endforeach
</form>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
				$('#nama_lokasi').change(function(){
					$('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
				})
			</script>


@endpush
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_usm') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .usm").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection