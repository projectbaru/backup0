@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Standar Upah Minimum @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<form action="{{route('simpan_usm')}}" method="post" enctype="multipart/form-data">
	<hr>
	{{ csrf_field() }}
	<div class="form-group">
		<div class="">
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Posisi</label>
						<div class="col-sm-10">
							<input type="text" readonly value="{{$random}}" name="kode_posisi" style="font-size:11px;" id="colFormLabelSm" class="form-control form-control-sm" placeholder="">
						</div>
					</div>
					<div class="form-group row" >
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
						<div class="col-sm-10">
							<select style="font-size:11px;" required name="nama_posisi" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
								<option value="" selected disabled>--- Pilih ---</option>
								@foreach ($posisi['looks'] as $p )
								<option value="{{$p->nama_posisi}}">{{$p->nama_posisi}}</option>
								@endforeach
							</select><p style="color:red;font-size:10px;">* wajib isi</p>															
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
						<div class="col-sm-10">
							<select name="tingkat_pendidikan" required id="pendidikan" class="form-control form-control-sm" style="font-size:11px;">
								<option value="SD">SD</option>
								<option value="SLTP">SLTP</option>
								<option value="SLTA">SLTA</option>
								<option value="D1">D1</option>
								<option value="D2">D2</option>
								<option value="D3">D3</option>
								<option value="S1">S1</option>
								<option value="S2">S2</option>
								<option value="S3">S3</option>
							</select><p style="color:red;font-size:10px;">* wajib isi</p>															
							<!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
						</div>													
					</div>
					
				</div>
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Lokasi</label>
						<div class="col-sm-10">
							<input type="text" readonly name="kode_lokasi" style="font-size:11px;" value="" class="form-control form-control-sm" id="kode_lokasi" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Lokasi</label>
						<div class="col-sm-10">
							<select name="nama_lokasi" id="nama_lokasi" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
								<option value="" selected disabled>--- Pilih ---</option>
								@foreach ($lokasi['looks'] as $p ) 
								<option value="{{$p->lokasi_kerja}}" data-kode_lokasi="{{$p->kode_grup_lokasi_kerja}}">{{$p->lokasi_kerja}}</option>
								@endforeach
							</select><p style="color:red;font-size:10px;">* wajib isi</p>																														
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Upah Minimum</label>
						<div class="col-sm-10">
							<input type="number" required style="font-size:11px;" name="upah_minimum" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi (dengan nomor)</p>															
							
						</div>
					</div>
				</div>
			</div>
			<hr>
			<b style="color:black;">Informasi Lainnya</b>
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>															
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
						<div class="col-sm-10">
						<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
				</div>												
			</div><hr>
		</div>
	</div>
	<div>
		<table>
		<tr>
			<td>
				<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
				<a href="{{ route('list_usm') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
		</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
$('#nama_lokasi').change(function(){
	$('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
})
</script>


@endpush

		
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
		$("#mm-active").addClass("active");
        $(" .usm").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection