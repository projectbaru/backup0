@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
table th{
     text-align:center;   
    }
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Standar Upah Minimum @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Standar Upah Minimum</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamsum')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>
<form action="{{ URL::to('/list_usm/hapus_banyak') }}" method="POST" id="form_delete">
    @csrf
    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead style="color:black;">
            <tr>
                <th style="">No</th>
                <th scope="col" style="">Kode Posisi</th>
                {{-- <th scope="col">Logo Perusahaan</th> --}}
                <th scope="col" style="">Nama Posisi</th>
                <th scope="col" style="">Pendidikan</th>
                <th scope="col" style="">Kode Lokasi</th>
                <th scope="col" style="">Nama Lokasi</th>
                <th scope="col" style="">Upah Minimum</th>
                <th scope="col" style="">Aksi</th>
                <th scope="col" style=""><i class="fa-solid fa-check"></i></th>
            </tr>
        </thead>
        <tbody style="font-size:11px;">
            @php $b=1; @endphp
            @foreach ($wsusm as $data)
            {{-- {{dd($data)}} --}}
                <tr>
                    <td>{{ $b++;}}</td>
                    <td>{{ $data->kode_posisi }}</td>
                    {{-- <td>{{$data -> logo_perusahaan}}</td> --}}
                    <td>{{ $data->nama_posisi }}</td>
                    <td>{{ $data->tingkat_pendidikan }}</td>
                    <td>{{ $data->kode_lokasi }}</td>
                    <td>{{ $data->nama_lokasi }}</td>
                    <td>{{ $data->upah_minimum }}</td>
                    <td>
                        <a href="{{ URL::to('/list_usm/detail/'.$data->id_standar_upah_minimum) }}" class="btn btn-secondary btn-sm">Detail</a>
                        <a href="{{ URL::to('/list_usm/edit/'.$data->id_standar_upah_minimum) }}" class="btn btn-success btn-sm">Edit</a>
                    </td>
                    <td>
                        <input type="checkbox" name="multiDelete[]" value="{{ $data->id_standar_upah_minimum }}" id="multiDelete">
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <table>
        <tr>
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_usm')}}">Tambah</a></td>
            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <td>
            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>

        </td>
        </tr>
    </table>

</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection