@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Standar Upah Minimum @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<form action="{{route('update_usm')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    @foreach($wsusm as $data)
    <hr>
    <div class="form-group " style="width:95%;">
        <div class="">
            <div class="row" >
                <div class="col" style="font-size: 10px;">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-10">
                        <input type="hidden" name="id_standar_upah_minimum" value="{{ $data->id_standar_upah_minimum }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                        <div class="col-sm-10">
                        <input type="text" readonly name="kode_posisi" value="{{ $data->kode_posisi }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                        <div class="col-sm-10">
                        <!-- <input type="text" name="nama_posisi" value="{{ $data->nama_posisi }}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                        <select name="nama_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                            @foreach ($posisi['looks'] as $v)
                                <option value="{{$v->nama_posisi}}" {{ $v->nama_posisi == $data->nama_posisi ? 'selected' : NULL }}>{{$v->nama_posisi}}</option>
                            @endforeach
                        </select><p style="color:red;font-size:10px;">* wajib isi</p>
                    </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
                            <div class="col-sm-10">
                                <select name="tingkat_pendidikan" id="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                    <!-- <option value="-">None</option> -->
                                    <option value="SD" {{ $data->tingkat_pendidikan == 'SD' ? 'selected' : NULL }}>SD</option>
                                    <option value="SLTP" {{ $data->tingkat_pendidikan == 'SLTP' ? 'selected' : NULL }}>SLTP</option>
                                    <option value="SLTA" {{ $data->tingkat_pendidikan == 'SLTA' ? 'selected' : NULL }}>SLTA</option>
                                    <option value="D1" {{ $data->tingkat_pendidikan == 'D1' ? 'selected' : NULL }}>D1</option>
                                    <option value="D2" {{ $data->tingkat_pendidikan == 'D2' ? 'selected' : NULL }}>D2</option>
                                    <option value="D3" {{ $data->tingkat_pendidikan == 'D3' ? 'selected' : NULL }}>D3</option>
                                    <option value="S1" {{ $data->tingkat_pendidikan == 'S1' ? 'selected' : NULL }}>S1</option>
                                    <option value="S2" {{ $data->tingkat_pendidikan == 'S2' ? 'selected' : NULL }}>S2</option>
                                    <option value="S3" {{ $data->tingkat_pendidikan == 'S3' ? 'selected' : NULL }}>S3</option>
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                            </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi</label>
                        <div class="col-sm-10">
                            
                            {{-- <select name="kode_lokasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($lokasi['looks'] as $v)
                                    <option value="{{$v->kode_grup_lokasi_kerja}}" {{ $v->kode_grup_lokasi_kerja == $data->kode_lokasi ? 'selected' : NULL }}>{{$v->kode_grup_lokasi_kerja}}</option>
                                @endforeach
                            </select> --}}
                        <!-- <input type="kode_lokasi" name="kode_lokasi" value="{{ $data->kode_lokasi }}"  style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                        <input type="kode_lokasi" name="kode_lokasi" value="{{ $data->kode_lokasi }}"  style="font-size:11px;" class="form-control form-control-sm" id="kode_lokasi" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi</label>
                        <div class="col-sm-10">
                        <!-- <input type="text" name="nama_lokasi" value="{{ $data->nama_lokasi }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            <select name="nama_lokasi" class="form-control form-control-sm vendorId" style="font-size:11px;" id="nama_lokasi">
                                <option value="" selected disabled>--- Pilih ---</option>
                                <option value="-">-</option>
                                @foreach ($lokasi['looks'] as $v)
                                    {{-- <option value="{{$v->id_grup_lokasi_kerja}}" {{ $v->nama_grup_lokasi_kerja == $data->nama_lokasi ? 'selected' : NULL }}>{{$v->nama_grup_lokasi_kerja}}</option> --}}
                                    <option value="{{$v->lokasi_kerja}}" {{ $v->lokasi_kerja == $data->nama_lokasi ? 'selected' : NULL }}  data-kode_lokasi="{{$v->kode_grup_lokasi_kerja}}">{{$v->lokasi_kerja}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Upah Minimum</label>
                        <div class="col-sm-10">
                            <input type="text" name="upah_minimum" value="{{ $data->upah_minimum }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div><p style="color:red;font-size:10px;">* wajib isi</p>
                    </div>
                </div>
            </div>
            <hr>
            <b style="color:black;">Informasi Lainnya</b>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" value="{{ $data->tanggal_mulai_efektif }}" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                            <!-- <input type="date" name="tanggal_mulai_efektif" value="{{ $data->tanggal_mulai_efektif }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" > -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" value="{{ $data->tanggal_selesai_efektif }}" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                            <!-- <input type="date" name="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" > -->
                        </div>
                    </div>                
                </div>
                <hr>
            </div>
        </div><hr>
    </div> @endforeach
    <div>
        <table>
            <tr>
            <!-- <td>
                <a href="" class="btn">Hapus</a>
            </td> -->
                <td>
                    <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                </td>
                <td>
                    <a class="btn btn-danger btn-sm" href="{{route('hapus_usm',$data->id_standar_upah_minimum)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                </td>
                <td>
                    <a href="{{ route('list_usm') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                </td>
            </tr>
        </table>
    </div>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
    $('#nama_lokasi').change(function(){
        $('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
    })
</script>

@endpush
