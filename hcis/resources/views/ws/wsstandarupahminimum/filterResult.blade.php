@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
table th{
     text-align:center;   
    }
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Posisi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Standar Upah Minimum</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamsum')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
    <!-- <h5>Per Table</h5> -->    
</div><br>
<form action="{{ URL::to('/list_usm/hapus_banyak') }}" method="POST" id="form_delete">
    @csrf
    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead>
                <th style="">No</th>
                @for($i = 0; $i < count($th); $i++)
                    <!-- @if($th[$i] == 'id_standar_upah_perusahaan')
                        <th></th>
                    @endif -->
                    
                    @if($th[$i] == 'kode_posisi')
                        <th style="">Kode Posisi</th>
                    @endif
                    @if($th[$i] == 'nama_posisi')
                        <th style="">Nama Posisi</th>
                    @endif
                    @if($th[$i] == 'tingkat_pendidikan')
                        <th style="">Tingkat Pendidikan</th>
                    @endif
                    @if($th[$i] == 'kode_lokasi')
                        <th style="">Kode Lokasi</th>
                    @endif
                    @if($th[$i] == 'nama_lokasi')
                        <th style="">Nama Lokasi</th>
                    @endif
                    @if($th[$i] == 'upah_minimum')
                        <th style="">Upah Minimum</th>
                    @endif
                    @if($th[$i] == 'status_rekaman')
                        <th style="">Status Rekaman</th>
                    @endif
                    @if($th[$i] == 'tanggal_mulai_efektif')
                        <th style="">Tanggal Mulai Efektif</th>
                    @endif
                    @if($th[$i] == 'tanggal_selesai_efektif')
                        <th style="">Tanggal Selesai Efektif</th>
                    @endif
                    @if($th[$i] == 'pengguna_masuk')
                        <th style="">Pengguna Masuk</th>
                    @endif
                    @if($th[$i] == 'waktu_masuk')
                        <th style="">Waktu Masuk</th>
                    @endif
                    @if($th[$i] == 'pengguna_ubah')
                        <th style="">Pengguna Ubah</th>
                    @endif
                    @if($th[$i] == 'waktu_ubah')
                        <th style="">Waktu Ubah</th>
                    @endif
                    @if($th[$i] == 'pengguna_hapus')
                        <th style="">Pengguna Hapus</th>
                    @endif
                    @if($th[$i] == 'waktu_hapus')
                        <th style="">Waktu Hapus</th>
                    @endif
                    
                    @if($i == count($th) - 1)
                        <th style="">Aksi</th>
                        <th style=""><i class="fa-solid fa-check"></i></th>
                    @endif
                @endfor
            </thead>
            <tbody style="font-size:11px;">
                @php $b=1; @endphp
                @foreach($query as $row)
                    <tr>
                        <td>{{$b++;}}</td>
                        @for($i = 0; $i < count($th); $i++)
                            <!-- @if($th[$i] == 'id_standar_upah_minimum')
                                <td>{{ $row->id_standar_upah_minimum ?? 'NO DATA' }}</td>
                            @endif -->
                            @if($th[$i] == 'kode_posisi')
                                <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'nama_posisi')
                                <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tingkat_pendidikan')
                                <td>{{ $row->tingkat_pendidikan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kode_lokasi')
                                <td>{{ $row->kode_lokasi ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'nama_lokasi')
                                <td>{{ $row->nama_lokasi ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'upah_minimum')
                                <td>{{ $row->upah_minimum ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'status_rekaman')
                                <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_mulai_efektif')
                                <td>{{ $row->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_selesai_efektif')
                                <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'pengguna_masuk')
                                <td>{{ $row->pengguna_masuk ? date('d-m-Y', strtotime($row->tanggal_selesai_perusahaan)) : 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'waktu_masuk')
                                <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'pengguna_ubah')
                                <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'waktu_ubah')
                                <td>{{ $row->waktu_ubah ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'pengguna_hapus')
                                <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'waktu_hapus')
                                <td>{{ $row->waktu_hapus ?? 'NO DATA' }}</td>
                            @endif
                            @if($i == count($th) - 1)
                                <td>  
                                    <a href="{{ URL::to('/list_usm/detail/'.$row->id_standar_upah_minimum) }}" class="btn btn-secondary btn-sm">Detail</a>
                                    <a href="{{ URL::to('/list_usm/edit/'.$row->id_standar_upah_minimum) }}" class="btn btn-success btn-sm">Edit</a>
                                    <!-- <a href="{{ URL::to('/list_wsperusahaan/hapus/'.$row->id_standar_upah_minimum) }}" class="btn btn-danger btn-xs mr-2">Hapus</a> -->
                                </td>
                                <td>
                                    <input id="multiDelete" type="checkbox" name="multiDelete[]" value="{{ $row->id_standar_upah_minimum }}">

                                </td>
                            @endif
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
        <table>
            <tr>
                <td><a class="btn btn-success btn-sm" href="{{route('tambah_usm')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                <td></td>
                <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                <td></td>
                <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                </td>
            </tr>
        </table>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .usm").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection