@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Standar Upah Minimum @endslot
@slot('title') Workstrukture @endslot
@endcomponent                         
                        <form action="{{route('update_tamsum')}}" class="" method="POST">
                        @csrf
                            <input type="hidden" name="displayedColumn" id="displayedColumnInput" value="kode_posisi,nama_posisi,tingkat_pendidikan,kode_lokasi,nama_lokasi,upah_minimum">
                            <div class="my-3">
                                <div style="">
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 1. Masukkan Nama</h6>
                                    <br>
                                    <div class="row mb-3">
                                        <label for="nama_posisi" class="col-sm-2 col-form-label" style="font-size:12px;">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="text" style="font-size:10px;" name="nama_posisi" class="form-control form-control-sm" id="nama_posisi" value="{{$filters ? $temp->nama_posisi : ''}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="kode_posisi" class="col-sm-2 col-form-label" style="font-size:12px;">Kode</label>
                                        <div class="col-sm-10">
                                            <input type="text" style="font-size:10px;" name="kode_posisi" class="form-control form-control-sm" id="kode_posisi" value="{{$filters ? $temp->kode_posisi : ''}}">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 2. Spesifikasi Kriteria Pencarian</h6>
                                    <table class="table table-borderless">
                                        <thead style="font-size:12px;">
                                            <th>Halaman</th>
                                            <th>Operator</th>
                                            <th>Nilai</th>
                                            <th>Aksi</th>
                                        </thead>
                                        <tbody id="queryInputContainer" style="font-size:10px;">
                                            @if($filters && !empty($temp->query_field))
                                            @for($k= 0; $k < count($temp->query_field); $k++)
                                                <tr>
                                                    <td>
                                                        <select name="queryField[]" id="queryField1" class="queryField form-control" data-number="1" style="font-size:10px;height: calc(2.28rem + 2px);">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++) <option class="form-control" style="font-size:10px;" value="{{ $fields[$i]['value'] }}" <?= $fields[$i]['value'] == $temp->query_field[$k] ? 'selected' : '' ?>>{{ $fields[$i]['text'] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="queryOperator[]" id="queryOperator1" class="queryOperator form-control " style="font-size:10px;height: calc(2.28rem + 2px);" data-number="{{($k+1)}}">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < count($operators); $i++) <option class="form-control" style="font-size:10px;" value="{{ $operators[$i] }}" <?= $operators[$i] == $temp->query_operator[$k] ? 'selected' : '' ?>>{{ $operators[$i] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td id="queryValueContainer{{($k+1)}}">
                                                        <input type="text" name="queryValue[]" class="form-control form-control-sm" style="font-size:10px;height: calc(2.28rem + 2px);" id="queryValue{{($k+1)}}" value="{{ $temp->query_value[$k] }}">
                                                    </td>
                                                    <td>
                                                        @if($k == 0)
                                                        <button type="button" class="btn btn-sm btn-primary addQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-plus"></i>
                                                        </button>
                                                        @else
                                                        <button type="button" class="btn btn-sm btn-danger removeQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-minus"></i>
                                                        </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endfor
                                                @else
                                                <tr>
                                                    <td>
                                                        <select name="queryField[]" id="queryField1" class="queryField form-control" data-number="1" style="font-size:10px;height: calc(2.28rem + 2px);">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++) <option class="form-control" style="font-size:10px;" value="{{ $fields[$i]['value'] }}">{{ $fields[$i]['text'] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="queryOperator[]" id="queryOperator1" class="queryOperator form-control " style="font-size:10px;height: calc(2.28rem + 2px);" data-number="1">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < count($operators); $i++) <option class="form-control" style="font-size:10px;" value="{{ $operators[$i] }}">{{ $operators[$i] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td id="queryValueContainer1">
                                                        <input type="text" name="queryValue[]" class="form-control form-control-sm" style="font-size:10px;height: calc(2.28rem + 2px);" id="queryValue1">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-primary addQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-plus"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="">
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 3. Pilih halaman yang ditampilkan</h6>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h5 style="font-size:12px;text-align:center;color:black;">Halaman Tersedia</h5>
                                            <select id="availableColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                @for($p = 0; $p < count($damn); $p++) 
                                                <option value="{{ $damn[$p]['value'] }}" style="<?= $damn[$p]['style'] ?>">{{ $damn[$p]['text'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col">
                                            <div style="margin-top: 70px;border-radius:5px;">
                                                <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-right fa-sm" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-left fa-sm" style="color:black;"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h5 style="font-size:12px;text-align:center;color:black;">Halaman yang dipilih</h5>
                                            <select id="displayedColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                @if($filters)
                                                @foreach($temp->select as $s)
                                                <option value="{{ $s}}"><?= ucwords(str_replace("_", " ", $s)) ?></option>
                                                @endforeach
                                                @else
                                                <option value="kode_posisi">Kode Posisi</option>
                                                    <option value="nama_posisi">Nama Posisi</option>
                                                    <option value="tingkat_pendidikan">Pendidikan</option>
                                                    <option value="kode_lokasi">Kode Lokasi</option>
                                                    <option value="nama_lokasi">Nama Lokasi</option>
                                                    <option value="upah_minimum">Upah Minimum</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col">
                                            <div style="margin-top: 50px;border-radius:5px;">
                                                <button class=" btn btn-sm" type="button" id="movetoFirst" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrows-up-to-line fa-sm" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="moveup" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-up fa-1x" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="movedown" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-down fa-1x" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="movetoLast" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrows-down-to-line fa-sm" style="color:black;"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="container-fluid form-group form-group-sm">
                                            <br>
                                            <div style="font-size: 11px;">
                                                <input type="submit" value="Submit" class="btn btn-primary btn-sm" style="border-radius:5px;font-size:11px;">
                                                <a href="{{ route('list_usm') }}" value="batal" class="btn btn-danger btn-sm" style="border-radius:5px;font-size:11px;">Batal</a>
                                            </div>
                                        </div>
                                    </div><br>
                                </div><br>
                                </div><br>
                            </div>
                        </form>
                        @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
				$('#nama_lokasi').change(function(){
					$('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
				})
			</script>

@include('js.wsperusahaan.filter')
@endpush
