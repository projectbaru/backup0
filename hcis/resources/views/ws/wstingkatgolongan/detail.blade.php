@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Golongan @endslot
@slot('title') Workstrukture @endslot
@endcomponent 
<div class="container" style="width:95%;">
    <form action="{{ route('duplicate_tg') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="tg_id" id="tg_id" value="{{ $wstingkatgolongan->id_tingkat_golongan }}">
        <hr>
        <div>
            <table>
                <tr>
                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_tg', $wstingkatgolongan->id_tingkat_golongan)}}">Ubah</a></td>
                    <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_tg', $wstingkatgolongan->id_tingkat_golongan)}}">Hapus</a></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_tg')}}">Batal</a></td>
                </tr>
            </table>
        </div>
        <br>
        <b>Informasi Tingkat Golongan</b>
        <div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <table style="font-size:12px;">
                            <tr>
                                <td>Nama Perusahaan</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <input type="hidden" name="nama_perusahaan" value="{{ $wstingkatgolongan->nama_perusahaan}}">{{ $wstingkatgolongan->nama_perusahaan ?? 'NO DATA' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Kode Tingkat Golongan</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="kode_tingkat_golongan" value="KTG-{{now()->isoFormat('dmYYs')}}">{{ $wstingkatgolongan->kode_tingkat_golongan ?? 'NO DATA' }}</td>
                            </tr>
                            <tr>
                                <td>Nama Tingkat Golongan</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="nama_tingkat_golongan" value="{{ $wstingkatgolongan->nama_tingkat_golongan }}">{{ $wstingkatgolongan->nama_tingkat_golongan ?? 'NO DATA' }}</td>
                            </tr>
                            <tr>
                                <td>Urutan Tampilan</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="urutan_tampilan" value="{{ $wstingkatgolongan->urutan_tampilan }}">{{ $wstingkatgolongan->urutan_tampilan ?? 'NO DATA' }}</td>
                            </tr>
                            {{-- <tr>
                                            <td>Tingkat Golongan</td>
                                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                            <td><input type="hidden" name="tingkat_golongan" value="{{ $$data->items }}">{{ $items ?? 'NO DATA' }}</td>
                            </tr> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <b>List Tingkat Golongan</b>
        <div class="" style="font-size:12px;">
            <div style="background-color: #ffffff;width: 200px;padding-top: 20px; padding-bottom: 30px;">
                <!-- @foreach (explode(',', $items) as $item)
                <span>{{ $item }}</span><br>
                @endforeach -->
                @foreach ($items_new as $k)
                <span>{{ $k->nama_golongan }}</span><br>
                @endforeach
            </div>
        </div>
        <hr>
        <b>Rekaman Informasi</b>

        <div class="">
            <div class="form-group">
                <div class="row">
                    <div class="col" style="font-size:12px;">
                        <table>

                            <!-- <tr>
                                        <td>Tanggal Mulai</td>
                                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="hidden" name="tanggal_mulai" value="{{ $wstingkatgolongan->tanggal_mulai ?? 'NO DATA' }}">{{ $wstingkatgolongan->tanggal_mulai ?? 'NO DATA' }}</td>
                                    </tr> -->
                            <tr>
                                <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="tanggal_mulai_efektif" value="{{ date('d-m-Y', strtotime($wstingkatgolongan->tanggal_mulai_efektif ?? 'NO DATA')) }}">{{ date('d-m-Y', strtotime($wstingkatgolongan->tanggal_mulai_efektif ?? 'NO DATA')) }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="tanggal_selesai_efektif" value="{{ date('d-m-Y', strtotime($wstingkatgolongan->tanggal_selesai_efektif ?? 'NO DATA')) }}">{{ date('d-m-Y', strtotime($wstingkatgolongan->tanggal_selesai_efektif ?? 'NO DATA' ))}}</td>
                            </tr>
                            <tr>
                                <td>Keterangan&nbsp;&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                <td><input type="hidden" name="keterangan" value="{{ $wstingkatgolongan->keterangan ?? 'NO DATA' }}">{{ $wstingkatgolongan->keterangan ?? 'NO DATA' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </form>
</div>   
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

@include('js.wstingkatgolongan.tambah')
@endpush

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_tg') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .tg").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection