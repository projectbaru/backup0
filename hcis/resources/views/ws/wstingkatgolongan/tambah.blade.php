@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Golongan @endslot
@slot('title') Workstrukture @endslot
@endcomponent     
                    <form action="{{route('simpan_tg')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="items" id="items">
                        <hr>
                        <div class="form-group " style="width:95%;">
                            <div class="">
                                <b style="color:black;">Informasi Tingkat Golongan</b><br><br>
                                <div class="row" style="color:black;">
                                    <div class="col">
                                        <div class="form-group row" style="">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Nama Perusahaan</label>
                                            <div class="col-sm-10">
                                                <select name="nama_perusahaan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($np['looks'] as $np )
                                                    <option value="{{$np->nama_perusahaan}}">{{$np->nama_perusahaan}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input type="text" name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Golongan</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="kode_tingkat_golongan" class="form-control form-control-sm" value="{{$random}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Golongan</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nama_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                            <div class="col-sm-10">
                                                <input type="number" name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>


                                                        </div> -->
                                </div>
                                <hr>
                                <b style="color:black;">List Tingkat Golongan</b>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-5">
                                        <select id="defaultList" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                            <?php $nama_golongan_list = []; ?>
                                            @foreach($items as $item)
                                                <?php
                                                if (!in_array($item->nama_golongan, $nama_golongan_list)) {
                                                    $nama_golongan_list[] = $item->nama_golongan;
                                                ?>
                                                    <option value="{{ $item->id_golongan }}">{{ $item->nama_golongan }}</option>
                                                <?php
                                                }
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <div style="margin-top: 70px;border-radius:5px;">
                                            <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                                <i class="fa-solid fa-arrow-right fa-sm" style="color:black;"></i>
                                            </button>
                                            <br>
                                            <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                                <i class="fa-solid fa-arrow-left fa-sm" style="color:black;"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;"></select>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->

                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_tg') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </form>
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

@include('js.wstingkatgolongan.tambah')
@endpush

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .tg").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection