@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Lokasi Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<form action="{{route('update_lk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group">
        <div class="">
            @foreach($wslokasikerja as $data)
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-10">
                            <input type="hidden" name="id_lokasi_kerja" value="{{ $data->id_lokasi_kerja }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
                        <div class="col-sm-10">
                            <input required readonly type="text" name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_lokasi_kerja}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                        <div class="col-sm-10">
                            <input required type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_lokasi_kerja}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
                        <div class="col-sm-10">
                            <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($glk['looks'] as $glk )
                                <option value="{{$glk->nama_grup_lokasi_kerja}}" data-kode_lokasi="{{$glk->kode_grup_lokasi_kerja}}" {{ $glk->nama_grup_lokasi_kerja == $data->nama_grup_lokasi_kerja ? 'selected' : NULL }}>{{$glk->nama_grup_lokasi_kerja }}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input required type="text" name="id_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-grup row">
                        <input type="hidden"  name="id_grup_lokasi_kerja" id="kode_lokasi" style="font-size:11px;" value="{{ $data->id_grup_lokasi_kerja }}" class="form-control form-control-sm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>

                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Kantor Cabang</label>
                        <div class="col-sm-10">
                            <select name="id_kantor" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                <option value=""></option>
                                @foreach ($ik['looks'] as $ik)
                                <option value="{{$ik->kode_kantor}}" {{ $ik->kode_kantor == $data->id_kantor ? 'selected' : NULL }}>{{$ik->kode_kantor}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input required type="text" name="id_kantor" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->id_kantor}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Zona Waktu</label>
                        <div class="col-sm-10">
                            <input required type="number" name="zona_waktu" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->zona_waktu}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat Perusahaan</label>
                        <div class="col-sm-10">
                            <input required type="text" name="alamat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->alamat}}" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                                            <label for="province_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                            <div class="col-sm-10">
                                                <select name="province_id" style="font-size:11px;" class="form-control form-control-sm vendorId select2" id="province_id" onchange="getDropdown(this, 'dropDownCity', 'city_id', );" style="font-size:11px;">
                                                    @foreach ($data_prov as $p)
                                                    <option style="font-size:11px;" value="{{ $p->prov_id }}" {{ $p->prov_name == $data->provinsi ? 'selected' : NULL }}>{{$p->prov_name}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input required type="text" name="provinsi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->provinsi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kabupaten/Kota</label>
                                            <div class="col-sm-10" style="font-size:11px;">
                                                <select name="city_id" id="city_id" style="font-size:11px;" class="form-control form-control-sm vendorId select2" onchange="getDropdown(this, 'dropDownDistrict', 'dis_id');" style="font-size:11px;">
                                                    <option value="" selected disabled>--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="dis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kecamatan</label>
                                            <div class="col-sm-10" style="font-size:11px;">
                                                <select name="dis_id" id="dis_id" style="font-size:11px;" class="form-control form-control-sm vendorId select2" onchange="getDropdown(this, 'dropDownSubDistrict', 'subdis_id');" disabled style="font-size:11px;">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="subdis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kelurahan</label>
                                            <div class="col-sm-10" style="font-size:11px;">
                                                <select name="subdis_id" id="subdis_id" style="font-size:11px;" class="form-control form-control-sm vendorId select2" style="font-size:11px;">
                                                </select>
                                            </div>
                                        </div>
                </div>
                <div class="col">
                    
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
                        <div class="col-sm-10">
                            <!-- <select name="kabupaten_kota" style="font-size:11px;" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                @foreach ($data_pc['looks'] as $data_pc)
                                <option value="{{$data_pc->postal_code}}" style="font-size:11px;">{{$data_pc->postal_code}}</option>
                                @endforeach
                            </select> -->
                            <input required type="number"  style="font-size:11px;" name="kode_pos" value="{{$data->kode_pos}}" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>

                            <!-- <input required type="text" name="kode_pos" class="form-control form-control-sm" value="{{$data->kode_pos}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Negara</label>
                        <div class="col-sm-10">
                            <select required name="negara" id="negara" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" {{ $data->negara == '' ? 'selected' : NULL }}>--Belum Dipilih--</option>
                                <option value="indonesia" {{ $data->negara == 'indonesia' ? 'selected' : NULL }}>Indonesia</option>
                                <option value="-">-</option>
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Telepon</label>
                        <div class="col-sm-10">
                            <input required type="text" name="nomor_telepon" class="form-control form-control-sm" id="colFormLabelSm" value="{{$data->nomor_telepon}}" style="font-size:11px;" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control form-control-sm" value="{{$data->email}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Fax</label>
                        <div class="col-sm-10">
                            <input type="text" name="fax" class="form-control form-control-sm" value="{{$data->fax}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                        <div class="col-sm-10">
                            <input type="text" name="keterangan" class="form-control form-control-sm" value="{{$data->keterangan}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" value="{{$data->tanggal_mulai_efektif}}" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai_efektif}}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                            <p style="color:red;font-size:10px;">* wajib isi</p>

                            <!-- <input  type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            @endforeach
            <div>
                <table>
                    <tr>

                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="{{route('hapus_lk',$data->id_lokasi_kerja)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                        </td>
                        <td>
                            <a href="{{ route('list_lk') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')

<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_lokasi').val($('#nama_grup_lokasi_kerja option:selected').data('kode_lokasi'));
    })
</script>


<script type="text/javascript">
    var provinsi;
    var city;
    var district;
    var subdistrict;

    $(document).ready(function() {
        $('.select2').select2();

        var datas = <?= json_encode($data); ?>;
        provinsi = datas.provinsi;
        if (provinsi !== null) {
            $("#province_id").val(provinsi).trigger('change');
        }
        city = datas.kabupaten_kota;
        if (city !== null) {
            setTimeout(() => {
                $("#city_id").val(city).trigger('change');
            }, 500);
        }

        district = datas.kecamatan;
        if (district !== null) {
            setTimeout(() => {
                $("#dis_id").val(district).trigger('change');
            }, 1000);
        }

        subdistrict = datas.kelurahan;
        if (subdistrict !== null) {
            setTimeout(() => {
                $("#subdis_id").val(subdistrict);
            }, 500);
        }
    });

    async function getDropdown(sel, urlParams, elm) {
        // console.log(sel.value);
        // var prov = $("#province_id").val();
        if (sel.value !== "") {
            var param = {
                params: sel.value
            };
            await ajaxData(param, urlParams, elm);
        }
    }

    function ajaxData(param, urlAjax, elm) {
        let url = `{{ url('${urlAjax}') }}`;

        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            dataType: 'JSON',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(res) {
                if (res.status) {
                    $(`#${elm}`).removeAttr('disabled');
                    resovlData(res.data, elm);
                }
            },

            error: function(err) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal!",
                    text: "Terjadi kesalahan saat menghapus data.",
                });
            },
        });
    }

    function resovlData(data, elm) {
        if (data) {
            var html = '';
            html += '<option value="" disabled selected>--Belum dipilih--</option>';
            data.forEach(el => {
                switch (elm) {
                    case 'city_id':
                        var slectedCity = '';
                        if (city == el.id) {
                            slectedCity = 'selected';
                        }
                        html += '<option value="' + el.id + '" ' + slectedCity + '>' + el.text + '</option>';
                        break;
                    case 'dis_id':
                        var selectedDis = '';

                        if (district == el.id) {
                            selectedDis = 'selected';
                        }
                        html += '<option value="' + el.id + '" ' + selectedDis + '>' + el.text + '</option>';
                        break;
                    case 'subdis_id':
                        var selectedSub = '';
                        if (subdistrict == el.id) {
                            selectedSub = 'selected';
                        }
                        html += '<option value="' + el.id + '" ' + selectedSub + '>' + el.text + '</option>';
                        break;
                    default:
                        html += '<option value="' + el.id + '" ' + slectedVal + '>' + el.text + '</option>';
                        break;
                }
            });

            $(`#${elm}`).html(html);
        }
    }
</script>

@endpush


