@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Lokasi Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
                    <form action="{{route('simpan_lk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
                        <div class="form-group">
                            <div class="">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value="{{$random}}" readonly name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($glk['looks'] as $glk )
                                                    <option value="{{$glk->nama_grup_lokasi_kerja}}" data-kode_lokasi="{{$glk->kode_grup_lokasi_kerja}}">{{$glk->nama_grup_lokasi_kerja }}</option>
                                                    @endforeach
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                                <!-- <input required type="text" name="id_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-grup row">
                                            <input type="hidden"  name="id_grup_lokasi_kerja" id="kode_lokasi" style="font-size:11px;" value="" class="form-control form-control-sm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>

                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Kantor Cabang</label>
                                            <div class="col-sm-10">
                                                <select name="id_kantor" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($ik['looks'] as $ik )
                                                    <option value="{{$ik->kode_kantor}}">{{$ik->kode_kantor}}&nbsp;[{{$ik->nama_kantor}}]</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input required type="text" name="id_kantor" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Zona Waktu</label>
                                            <div class="col-sm-6">
                                                <input required type="number" name="zona_waktu" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                       
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat Perusahaan</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="alamat" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="province_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="province_id" id="province_id" style="font-size:11px;" class="form-control-select select2" onchange="getDropdown(this, 'dropDownCity', 'city_id');">
                                                    <option value=""  style="font-size:11px;" selected disabled>--Pilih disini--</option>
                                                    @foreach($prov as $k)
                                                    <option value="{{$k->prov_id}}"  style="font-size:11px;">{{ $k->prov_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kabupaten/Kota</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="city_id" id="city_id"  style="font-size:11px;" class="form-control-select select2" disabled onchange="getDropdown(this, 'dropDownDistrict', 'dis_id');">
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="dis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kecamatan</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="dis_id" id="dis_id"  style="font-size:11px;" class="form-control-select select2" disabled onchange="getDropdown(this, 'dropDownSubDistrict', 'subdis_id');">
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="subdis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kelurahan</label>
                                            <div class="col-sm-10">
                                                <select name="subdis_id" id="subdis_id"  style="font-size:11px;" class="form-control-select select2" disabled>
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <input required type="number"  style="font-size:11px;" name="kode_pos" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div><p style="color:red;font-size:10px;">* wajib isi</p>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Negara</label>
                                            <div class="col-sm-10">
                                                <select required name="negara" id="negara" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" disabled selected>--Belum Dipilih--</option>
                                                    <option value="indonesia">Indonesia</option>
                                                    <option value="lainnya">Lainnya</option>
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Telepon</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="nomor_telepon" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Fax</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="fax" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                            <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_lk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush



@section('add-scripts')

<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_lokasi').val($('#nama_grup_lokasi_kerja option:selected').data('kode_lokasi'));
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });

    async function getDropdown(sel, urlParams, elm) {
        // console.log(sel.value);
        // var prov = $("#province_id").val();
        if (sel.value !== "") {
            var param = {
                params: sel.value
            };
            await ajaxData(param, urlParams, elm);
        }
    }

    function ajaxData(param, urlAjax, elm) {
        let url = `{{ url('${urlAjax}') }}`;

        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            dataType: 'JSON',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(res) {
                if (res.status) {
                    $(`#${elm}`).removeAttr('disabled');
                    resovlData(res.data, elm);
                }
            },

            error: function(err) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal!",
                    text: "Terjadi kesalahan saat menghapus data.",
                });
            },
        });
    }

    function resovlData(data, elm) {
        if (data) {
            var html = '';
            html += '<option value="" disabled style="font-size:11px;" selected>--Belum dipilih--</option>';
            data.forEach(el => {
                html += '<option style="font-size:11px;" value="' + el.id + '">' + el.text + '</option>';
            });

            $(`#${elm}`).html(html);
        }
    }

    $("#mm-active").addClass("active");
        $(" .lk").addClass("active");
        $("#a-homes").addClass("active");
</script>
@endsection