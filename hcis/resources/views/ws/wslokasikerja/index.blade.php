@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
table th{
     text-align:center;   
    }
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Lokasi Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Lokasi Kerja</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamlk')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>
                                <form action="{{ URL::to('/list_lk/hapus_banyak') }}" method="POST">
                                    @csrf
                                    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead style="color:black;">
                                            <tr>
                                                <th style="">No</th>
                                                <th style="" scope="col">Kode Lokasi Kerja</th>
                                                <!-- {{-- <th scope="col">Logo Perusahaan</th> --}} -->
                                                <th style="" scope="col">Nama Lokasi Kerja</th>
                                                <th style="" scope="col">Aksi</th>
                                                <th style=""><i class="fa-solid fa-check"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach ($wslokasikerja as $data)
                                            {{-- {{dd($data)}} --}}
                                                <tr>
                                                    <td>{{ $b++; }}</td>
                                                    <td>{{ $data->kode_lokasi_kerja }}</td>
                                                    <!-- {{-- <td>{{$data -> logo_perusahaan}}</td> --}} -->
                                                    <td>{{ $data->nama_lokasi_kerja }}</td>
                                                    <td>
                                                        <a href="{{ URL::to('/list_lk/detail/'.$data->id_lokasi_kerja) }}" class="btn btn-secondary btn-sm">Detail</a>
                                                        <a href="{{ URL::to('/list_lk/edit/'.$data->id_lokasi_kerja) }}" class="btn btn-success btn-sm">Edit</a>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="multiDelete[]" value="{{ $data->id_lokasi_kerja }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_lk')}}">Tambah</a></td>
                                            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                            <td>
                                            <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" style="border-radius:5px; font-size:11px;" data-bs-target="#deleteModal">
                                                Hapus
                                            </button>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                        <div class="modal-body text-center">
                                                            <i class="fa-regular fa-circle-exclamation text-warning" style="font-size: 58px;"></i>
                                                            <br><br>
                                                            <p>Hapus list yang dipilih?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
                                                            <button type="submit" class="btn btn-primary">Ya</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush


