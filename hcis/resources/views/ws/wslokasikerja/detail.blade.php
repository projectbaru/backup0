@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Lokasi Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<div class="container" style="width:95%;">
    <form action="{{route('simpan_duplicate')}}" method="post">{{ csrf_field() }}
        <hr>
        @foreach($wslokasikerja as $data)
        <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_lokasi_kerja }}" />

        <table>
            <tr>
                <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_lk', $data->id_lokasi_kerja)}}">Ubah</a></td>
                <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_lk', $data->id_lokasi_kerja)}}" id="btn_delete">Hapus</a></td>
                <td><a class="btn btn-secondary btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_lk')}}">Batal</a></td>                
            </tr>
        </table><br>
        <!-- <b style="color:black;">Informasi Perusahaan</b> -->
            <div class="row">
                <div class="col-md-3" >
                    <table style="font-size:12px;">
                        <tr>
                            <td>Kode Lokasi Kerja</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kode_lokasi_kerja" value="LK-{{now()->isoFormat('dmYYs')}}">{{$data->kode_lokasi_kerja}}</td>
                        </tr>
                        <tr>
                            <td>Nama Lokasi Kerja</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="nama_lokasi_kerja" value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</td>
                        </tr>
                        <tr>
                            <td>Nama Grup Lokasi Kerja</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="nama_grup_lokasi_kerja" value="{{$data->nama_grup_lokasi_kerja}}">{{$data->nama_grup_lokasi_kerja}}</td>
                        </tr>
                        <tr>
                            <td>ID Kantor Cabang</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="id_kantor" value="{{$data->id_kantor}}">{{$data->id_kantor}}</td>
                        </tr>
                        <tr>
                            <td>Zona Waktu</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="zona_waktu" value="{{$data->zona_waktu}}">{{$data->zona_waktu}}</td>
                        </tr>
                        <tr>
                            <td>Alamat Perusahaan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="alamat" value="{{$data->alamat}}">{{$data->alamat}}</td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="provinsi" value="{{$data->provinsi}}">{{$data->provinsi}}</td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kecamatan" value="{{$data->kecamatan}}">{{$data->kecamatan}}</td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kelurahan" value="{{$data->kelurahan}}">{{$data->kelurahan}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-9" style="font-size:12px;">
                    <table>
                        <tr>
                            <td>Kabupaten / Kota</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kabupaten_kota" value="{{$data->kabupaten_kota}}">{{$data->kabupaten_kota}}</td>
                        </tr>
                        <tr>
                            <td>Kode Pos</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kode_pos" value="{{$data->kode_pos}}">{{$data->kode_pos}}</td>
                        </tr>
                        <tr>
                            <td>Negara</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="negara" value="{{$data->negara}}">{{$data->negara}}</td>
                        </tr>
                        <tr>
                            <td>Nomor Telepon</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="nomor_telepon" value="{{$data->nomor_telepon}}">{{$data->nomor_telepon}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="email" value="{{$data->email}}">{{$data->email}}</td>
                        </tr>
                        <tr>
                            <td>Fax</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="fax" value="{{$data->fax}}">{{$data->fax}}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai Efektif</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{ $data->tanggal_mulai_efektif ? date('d-m-Y', strtotime($data->tanggal_mulai_efektif)) : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai Efektif</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                        </tr>
                    </table>
                </div>
            </div><br>
        @endforeach
    </form>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_lk') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .lk").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection