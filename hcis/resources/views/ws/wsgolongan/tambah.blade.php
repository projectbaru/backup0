@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Golongan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
    <form action="{{route('simpan_g')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
        <hr>
        <div class="form-group" style="width:95%;">
            <div class="row">
                <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Golongan</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_golongan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""  value="{{$random}}" readonly><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                            <div class="col-sm-10">
                            <select name="nama_perusahaan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($np['looks'] as $np )
                                <option value="{{$np->nama_perusahaan}}">{{$np->nama_perusahaan}}</option>
                                @endforeach
                            </select>	<p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Golongan</label>
                            <div class="col-sm-10">
                            <select name="nama_golongan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                
                                <option value="Gol 1">Gol 1</option>
                                <option value="Gol 2">Gol 2</option>
                                <option value="Gol 3">Gol 3</option>
                                
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>	
                            <!-- <input type="text" name="nama_golongan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" ><p style="color:red;font-size:10px;">* wajib isi</p> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Golongan</label>
                            <div class="col-sm-10">
                            <select name="tingkat_golongan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($tg['looks'] as $tg )
                                <option value="{{$tg->nama_tingkat_golongan}}">{{$tg->nama_tingkat_golongan}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>	
                            <!-- <input type="text" name="tingkat_golongan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Golongan</label>
                            <div class="col-sm-10">
                            <input type="text" name="urutan_golongan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                            <div class="col-sm-10">
                            <input type="number" name="urutan_tampilan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                            <div class="col-sm-10">
                            <select name="tingkat_posisi" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($tp['looks'] as $tp )
                                <option value="{{$tp->tingkat_posisi}}">{{$tp->tingkat_posisi}}</option>
                                @endforeach
                            </select>	
                            <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                            <div class="col-sm-10">
                            <select name="jabatan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($dj['looks'] as $j )
                                <option value="{{$j->nama_jabatan}}">{{$j->nama_jabatan}}</option>
                                @endforeach
                            </select>	
                            <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                            <div class="col-sm-10">
                            <textarea type="text" name="deskripsi" class="form-control form-control-sm" style="font-size:11px;" rows="6" cols="50" id="colFormLabelSm" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b style="color:black;">Rekaman Informasi</b><br><br>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                            <div class="col-sm-10">
                            <textarea type="text" name="keterangan" rows="6" cols="50" class="form-control form-control-sm"  style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                            </div>
                        </div>
                        <hr>                            
                    </div>
                </div>
            </div>
            <div >
                <table>
                    <tr>

                    <!-- <td>
                        <a href="" class="btn">Hapus</a>
                    </td> -->
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                        </td>
                        <td>
                            <a href="{{ route('list_g') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div><br>
        </div>
    
    </form>
    @endsection

    @push('scripts')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    @endpush
