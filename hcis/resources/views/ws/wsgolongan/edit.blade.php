@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Golongan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('update_g')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group" style="width:95%;">
        @foreach($wsgolongan as $data)
        <div class="row">
            <div class="col">
            <b>Informasi Golongan</b>

                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-10">
                        <input type="hidden" name="id_golongan"  id="tg_id"  value="{{ $data->id_golongan }}" class="form-control form-control-sm"  name="tg_id" id="tg_id" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                        <div class="col-sm-10">
                        <select name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                            <option value=""></option>
                            @foreach ($np['looks'] as $np)
                                <option value="{{$np->nama_perusahaan}}" {{ $np->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$np->nama_perusahaan}}</option>
                            @endforeach
                        </select><p style="color:red;font-size:10px;">* wajib isi</p>
                        <!-- <input type="text" required name="tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tingkat_golongan}}"> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Golongan</label>
                        <div class="col-sm-10">
                        <input type="text" readonly required name="kode_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_golongan}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Golongan</label>
                        <div class="col-sm-10">
                        <input type="text" required name="nama_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_golongan}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Golongan</label>
                        <div class="col-sm-10">
                        <select name="tingkat_golongan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                            <option value=""></option>
                            @foreach ($tg['looks'] as $tg)
                                <option value="{{$tg->nama_tingkat_golongan}}" {{ $tg->nama_tingkat_golongan == $data->tingkat_golongan ? 'selected' : NULL }}>{{$tg->nama_tingkat_golongan}}</option>
                            @endforeach
                        </select><p style="color:red;font-size:10px;">* wajib isi</p>
                        <!-- <input type="text" required name="tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tingkat_golongan}}"> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Golongan</label>
                        <div class="col-sm-10">
                        <input type="text" required name="urutan_golongan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_golongan}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                        <div class="col-sm-10">
                        <input type="number" required name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tampilan}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                        <div class="col-sm-10">
                        <select name="tingkat_posisi" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                            <option value=""></option>
                            @foreach ($tp['looks'] as $tp)
                                <option value="{{$tp->tingkat_posisi}}" {{ $tp->tingkat_posisi == $data->tingkat_posisi ? 'selected' : NULL }}>{{$tp->tingkat_posisi}}</option>
                            @endforeach
                        </select>
                        <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_posisi}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                        <div class="col-sm-10">
                        <select name="jabatan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                            <option value=""></option>
                            @foreach ($dj['looks'] as $j)
                                <option value="{{$j->nama_jabatan}}" {{ $j->nama_jabatan == $data->jabatan ? 'selected' : NULL }}>{{$j->nama_jabatan}}</option>
                            @endforeach
                        </select>
                        <!-- <select name="jabatan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                            <option value=""></option>
                            
                        </select> -->

                        <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->jabatan}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                        <div class="col-sm-10">
                        <textarea type="text" name="deskripsi" rows="6" cols="50" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi}}" id="colFormLabelSm" placeholder="">{{$data->deskripsi}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <b>Rekaman Informasi</b>
            <div class="row">
            <div class="col">
                <div class="form-group row">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                    <div class="col-sm-10">
                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}"  placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                    <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai_efektif}}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                    <div class="col-sm-10">
                    <textarea type="text" name="keterangan" rows="6" cols="50" class="form-control form-control-sm" value="{{$data->keterangan}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                    </div>
                </div>
                <hr>
            </div>
            </div>
        </div>
        @endforeach
        <div class="" style="">
            <table>
                <tr>

                <!-- <td>
                    <a href="" class="btn">Hapus</a>
                </td> -->
                    <td>
                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                    </td>
                    <td>
                        <a class="btn btn-danger btn-sm" href="{{route('hapus_g',$data->id_golongan)}}" id="btn_delete" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                    </td>
                    <td>
                        <a href="{{ route('list_g') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                    </td>
                </tr>
            </table>
        </div><br>
    </div>
</form>
@endsection
@push('scripts')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    @endpush

@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_g') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
            $("#mm-active").addClass("active");
        $(" .g").addClass("active");
        $("#a-homes").addClass("active");
        });
    </script>
@endsection