@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Kelas Cabang @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<div class="container" style="width:95%;">
    <form action="{{route('simpan_kc')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
        <hr>
        <div>
            <div class="" style="width:95%" >
            
            @foreach($wskelascabang as $data)
            <table>
                <tr>
                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_kc', $data->id_kelas_cabang)}}">Ubah</a></td>
                    <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_kc', $data->id_kelas_cabang)}}">Hapus</a></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_kc')}}">Batal</a></td>
                    <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                    <!-- <td>
                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                    </td> -->
                </tr>
            </table><br>
            <!-- <b style="color:black;">Informasi Perusahaan</b> -->
            <div class="row">
                <div class="col-md-4">
                    <table style="font-size:12px;">
                        <tr>
                            <td>Kode Kelas Cabang</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kode_kelas_cabang" value="KKC-{{now()->isoFormat('dmYYs')}}">
                                {{$data->kode_kelas_cabang}}</td>
                        </tr>
                        <tr>
                            <td>Nama Kelas Cabang</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="nama_kelas_cabang" value="{{$data->nama_kelas_cabang}}">{{$data->nama_kelas_cabang}}</td>
                        </tr>
                        <tr>
                            <td>Area Kelas Cabang</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="area_kelas_cabang" value="{{$data->area_kelas_cabang}}">{{$data->area_kelas_cabang}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-8">
                    <table style="font-size:12px;">
                        <tr>
                            <td>Keterangan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">
                                {{$data->keterangan}}
                            </td>
                        </tr>
                        <tr>
                            <td>Efektif Tanggal Mulai</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_mulai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}</td>
                        </tr>
                        <tr>
                            <td>Efektif Tanggal Akhir</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_selesai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                        </tr>
                    </table><br><br>
                </div>                                         
            </div>
            @endforeach
        </div>
    </form>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
				$('#exampleRadios1').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", false);
					$("#input_area_kelas_cabang").attr("disabled", true);
					$("#select_area_kelas_cabang").attr("required", true);
					$("#input_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").val("");
				})
				$('#exampleRadios2').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", true);
					$("#input_area_kelas_cabang").attr("disabled", false);
					$("#select_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").attr("required", true);
					$("#select_area_kelas_cabang").val("");
				})
			</script>
@endpush
					@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
		$("#mm-active").addClass("active");
        $(" .kc").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection