@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Kelas Cabang @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('simpan_kc')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
	<hr>
	<div class="form-group" style="width:95%;">
		<div class="">
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelas Cabang</label>
						<div class="col-sm-10">
						<input type="text" name="kode_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$random}}" readonly required><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelas Cabang</label>
						<div class="col-sm-10">
						<input type="text" name="nama_kelas_cabang" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Area Kelas Cabang</label>
						<div class="col-sm-10">
							<div class="form-check">
								<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1">
								<label class="form-check-label" for="exampleRadios1" checked>
									<select disabled name="area_kelas_cabang" style="font-size:11px;" class="form-control form-control-sm" id="select_area_kelas_cabang">
										<option value="" selected>--- Pilih ---</option>
										@foreach ($dataakc['looks'] as $v )
										<option value="{{$v->area_kelas_cabang}}">{{$v->area_kelas_cabang}}</option>
										@endforeach
									</select>
								</label>
							</div><br>
							<div class="form-check">
								<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" checked>
								<label class="form-check-label" for="exampleRadios2">
									<input type="text" name="area_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;"  id="input_area_kelas_cabang" placeholder="">
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
						<div class="col-sm-10">
						<textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal Mulai</label>
						<div class="col-sm-10">
						<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal akhir</label>
						<div class="col-sm-10">
						<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>

						<!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->
						</div>
					</div>

				</div>                                                                                                 
			</div><hr>
		</div>
	</div>
	<div>
		<table>
			<tr>
				<!-- <td>
				<a href="" class="btn">Hapus</a>
			</td> -->
				<!-- <td>
				<a href="" class="btn">Hapus</a>
			</td> -->
				<td>
					<button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
				</td>
				<td>
					<a href="{{ route('list_kc') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
				</td>
				
			</tr>
		</table>
		<br>
	</div>

</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
				$('#exampleRadios1').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", false);
					$("#input_area_kelas_cabang").attr("disabled", true);
					$("#select_area_kelas_cabang").attr("required", true);
					$("#input_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").val("");
				})
				$('#exampleRadios2').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", true);
					$("#input_area_kelas_cabang").attr("disabled", false);
					$("#select_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").attr("required", true);
					$("#select_area_kelas_cabang").val("");
				})
			</script>
@endpush

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
		$("#mm-active").addClass("active");
        $(" .kc").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection