@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Organisasi</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamo')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>          
        <form action="{{ URL::to('/list_o/hapus_banyak') }}" method="POST" id="form_delete">
            @csrf
            <table id="example" class="table table-bordered dt-responsive nowrap" style="text-align:center;">

                    <thead style="color:black;font-size:12px;">
                        <tr>
                            <th >No</th>
                            <th scope="col" >Nama Organisasi</th>
                            <th scope="col" >Nama Perusahaan</th>
                            <th scope="col" >Tingkat Organisasi</th>
                            <th scope="col" >Nama Struktur Organisasi</th>
                            <th scope="col" >Versi Struktur Organisasi</th>
                            <th scope="col" >Tanggal Mulai</th>
                            <th scope="col" >Tanggal Selesai</th>
                            <th scope="col" >Keterangan</th>
                            <th scope="col" >Aksi</th>
                            <th scope="col" >V</th>

                        </tr>
                    </thead>
                    <tbody style="font-size:11px;">
                        @php $b=1; @endphp
                        @foreach ($wsorganisasi as $data)
                        {{-- {{dd($data)}} --}}
                            <tr>
                                <td>{{ $b++; }}</td>
                                <td>{{ $data->nama_organisasi }}</td>
                                <td>{{ $data->nama_perusahaan }}</td>
                                <td>{{ $data->tingkat_organisasi }}</td>
                                <td>{{ $data->nama_struktur_organisasi }}</td>
                                <td>{{ $data->versi_struktur_organisasi  }}</td>
                                <td>{{ date('d-m-Y', strtotime($data->tanggal_mulai ))}}</td>
                                <td>{{ date('d-m-Y', strtotime($data->tanggal_selesai )) }}</td>
                                <td>{{ $data->keterangan }}</td>
                                <td>
                                    <a href="{{ URL::to('/list_o/detail/'.$data->id_organisasi) }}" class="btn btn-warning btn-sm">Detail</a>
                                    <a href="{{ URL::to('/list_o/edit/'.$data->id_organisasi) }}" class="btn btn-success btn-sm">Edit</a>
                                </td>
                                <td>         
                                    <input type="checkbox" name="multiDelete[]" id="multiDelete" value="{{ $data->id_organisasi }}">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_o')}}">Tambah</a></td>
                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                        <td>
                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                        </td>
                    </tr>
                </table>
        </form>
    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush


@section('add-scripts')
<script>
    $(function() {
        $(document).ready(function() {
            $('#example').DataTable({
                responsive: true,
            });
        });    
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Terhapus!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection
