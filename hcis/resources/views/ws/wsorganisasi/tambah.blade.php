@extends('layouts.master')
@section('title') Dashboard @endsection

@section('css')
    <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') Workstrukture @endslot
        @slot('li_3') Organisasi @endslot
        @slot('title') Workstrukture @endslot
    @endcomponent
    
                                        <form action="{{route('simpan_o')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            <hr>
                                            <div class="form-group" style="width:95%;"><br>
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                                <div class="col-sm-10">
                                                                <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                    <select name="nama_perusahaan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="" selected>--- Pilih ---</option>
                                                                        @foreach ($perusahaan['lookp'] as $p )
                                                                        <option value="{{$p->nama_perusahaan}}">{{$p->nama_perusahaan}}</option>
                                                                        @endforeach
                                                                    </select><p style="color:red;font-size:10px;">* wajib isi</p>	
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""  value="{{$randomKode}}" readonly><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text"  required name="nama_organisasi" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tipe_area" required id="tipe_organisasi" required class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="" selected disabled>--Pilih--</option>
                                                                        <option value="BO">BO - Branch Office</option>
                                                                        <option value="HO">HO - Head Office</option>
                                                                    </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" name="grup_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pusat Biaya</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" name="kode_pusat_biaya" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_mulai" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai" placeholder="Tanggal mulai"><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai" placeholder="Tanggal selesai" />

                                                                <!-- <input type="date" name="tanggal_selesai" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">ID Induk Organisasi</label>
                                                                <div class="col-sm-10">
                                                                   
                                                                    
                                                                    {{-- <input type="text" readonly name="id_induk_organisasi" style="font-size:11px;" value="{{$data->induk_organisasi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                                                                    <input type="text" readonly name="id_induk_organisasi" style="font-size:11px;" value="" class="form-control form-control-sm" id="id_induk_organisasi" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Induk Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <select name="induk_organisasi" id="induk_organisasi" style='font-size:11px;'  class="form-control form-control-sm" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($organisasi['looko'] as $o ) 
                                                                        <option value="{{$o->induk_organisasi}}" data-id_induk_organisasi="{{$o->id_induk_organisasi}}">{{$o->induk_organisasi}}</option>
                                                                        @endforeach
                                                                    </select>															
                                                                </div>
                                                            </div>

                                                            <!-- <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Induk Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" readonly name="id_induk_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Induk Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <select name="induk_organisasi" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($organisasi['looko'] as $o )
                                                                    <option value="{{$o->nama_organisasi}}">{{$o->nama_organisasi}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="text" name="induk_organisasi" class="form-control form-control-sm" id="colFormLabelSm" style="font-size:11px;" placeholder="">
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Unit Kerja</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" required name="unit_kerja" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">Informasi Lainnya</b>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Struktur Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="nama_struktur_organisasi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Versi Struktur Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="versi_struktur_organisasi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tingkat Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tingkat_organisasi" id="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($tg['lookp'] as $p )
                                                                        <option value="{{$p->urutan_tingkat}}" data-name ="{{$p->kode_tingkat_organisasi}}">{{$p->urutan_tingkat}} - {{$p->nama_tingkat_organisasi}}</option>
                                                                        @endforeach
                                                                    </select>		<p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Tingkat Organisasi</label>
                                                                <div class="col-sm-10">
                                                                {{-- <input type="text" required name="kode_tingkat_organisasi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                                                                <select name="kode_tingkat_organisasi" id="kode_tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($tg['lookp'] as $kp )
                                                                    <option value="{{$kp->kode_tingkat_organisasi}}" data-kode ="{{$kp->urutan_tingkat}}">{{$kp->kode_tingkat_organisasi}}</option>
                                                                    @endforeach
                                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>	
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <select name="kode_tingkat_organisasi" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($tg['lookp'] as $tg )
                                                                    <option value="{{$tg->kode_tingkat_organisasi}}">{{$tg->kode_tingkat_organisasi}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="text"  required name="kode_tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Indeks Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="number" name="nomor_indeks_organisasi" required style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <input type="number" name="urutan_organisasi" required style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan Organisasi</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan_organisasi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Leher Struktur</label>
                                                                <div class="col-sm-5">
                                                                <input type="checkbox" name="leher_struktur" style="font-size:11px;" value="Yes" class="" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Aktif</label>
                                                                <div class="col-sm-5">
                                                                <input type="checkbox" name="aktif" class="" id="colFormLabelSm"  value="Yes" style="font-size:11px;" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <hr>
                                                    <b style="color:black;">Informasi Lainnya</b>
                                                    <div class="row" style="">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" required id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan" id="colFormLabelSm" class="form-control form-control-sm" rows="4" cols="50" style="font-size:11px;" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="" style="">
                                                <table>
                                                    <tr>
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                     
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_o') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </form>
                                     



    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <!-- <script>
        $('#induk_organisasi').change(function(){
            $('#id_induk_organisasi').val($('#induk_organisasi option:selected').data('id_induk_organisasi'));
        })
    </script> -->
    <script>
        $('#induk_organisasi').change(function(){
            $('#id_induk_organisasi').val($('#induk_organisasi option:selected').data('id_induk_organisasi'));
        })
    </script>
    <script>			
        $('#tingkat_organisasi').change(function(){
            $('#kode_tingkat_organisasi').val($('#tingkat_organisasi option:selected').data('name'));
        })
        $('#kode_tingkat_organisasi').change(function(){
            $('#tingkat_organisasi').val($('#kode_tingkat_organisasi option:selected').data('kode'));
        })
    </script>

@endsection