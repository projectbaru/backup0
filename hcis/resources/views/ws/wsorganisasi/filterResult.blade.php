@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Organisasi</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamo')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>

                            <form action="{{ URL::to('/list_o/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                <table id="example" class="table table-bordered dt-responsive nowrap" style="text-align:center;">
                                        <thead style="font-size:12px;">
                                        <th style="">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_organisasi')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'nama_perusahaan')
                                                    <th style="">Nama Perusahaan</th>
                                                @endif
                                                @if($th[$i] == 'kode_organisasi')
                                                    <th style="">Kode Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nama_organisasi')
                                                    <th style="">Nama Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'tipe_area')
                                                    <th style="">Tipe Area</th>
                                                @endif
                                                @if($th[$i] == 'grup_organisasi')
                                                    <th style="">Grup Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'unit_kerja')
                                                    <th style="">Unit Kerja</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai')
                                                    <th style="">Tanggal Mulai</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai')
                                                    <th style="">Tanggal Selesai</th>
                                                @endif
                                                @if($th[$i] == 'id_induk_organisasi')
                                                    <th style="">ID Induk Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'induk_organisasi')
                                                    <th style="">Induk Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nama_struktur_organisasi')
                                                    <th style="">Nama Struktur Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'versi_struktur_organisasi')
                                                    <th style="">Versi Struktur Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'kode_tingkat_organisasi')
                                                    <th style="">Kode Tingkat Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nomor_indeks_organisasi')
                                                    <th style="">Nomor Indeks Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'tingkat_organisasi')
                                                    <th style="">Tingkat Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'urutan_organisasi')
                                                    <th style="">Urutan Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'keterangan_organisasi')
                                                    <th style="">Keterangan Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'leher_struktur')
                                                    <th style="">Leher Struktur</th>
                                                @endif
                                                @if($th[$i] == 'aktif')
                                                    <th style="">Aktif</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="">Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                    <th style="">Tanggal Selesai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_masuk')
                                                    <th style="">Pengguna Masuk</th>
                                                @endif
                                                @if($th[$i] == 'waktu_masuk')
                                                    <th style="">Waktu Masuk</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_ubah')
                                                    <th style="">Waktu Masuk</th>
                                                @endif
                                                @if($i == count($th) - 1)
                                                    <th style="">Aksi</th>
                                                    <th style="">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_organisasi')
                                                            <td>{{ $row->id_organisasi ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'nama_perusahaan')
                                                            <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_organisasi')
                                                            <td>{{ $row->kode_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_organisasi')
                                                            <td>{{ $row->nama_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tipe_area')
                                                            <td>{{ $row->tipe_area ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'grup_organisasi')
                                                            <td>{{ $row->grup_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'unit_kerja')
                                                            <td>{{ $row->unit_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai')
                                                            <td>{{ date('d-m-Y', strtotime( $row->tanggal_mulai)) }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai')
                                                            <td>{{ date('d-m-Y', strtotime($row->tanggal_selesai)) }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_induk_organisasi')
                                                            <td>{{ $row->id_induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'induk_organisasi')
                                                            <td>{{ $row->induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_struktur_organisasi')
                                                            <td>{{ $row->nama_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'versi_struktur_organisasi')
                                                            <td>{{ $row->versi_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_tingkat_organisasi')
                                                            <td>{{ $row->kode_tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_indeks_organisasi')
                                                            <td>{{ $row->nomor_indeks_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tingkat_organisasi')
                                                            <td>{{ $row->tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'urutan_organisasi')
                                                            <td>{{ $row->urutan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan_organisasi')
                                                            <td>{{ $row->keterangan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'leher_struktur')
                                                            <td>{{ $row->leher_struktur ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'aktif')
                                                            <td>{{ $row->aktif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_masuk')
                                                            <td>{{ $row->pengguna_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_masuk')
                                                            <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                            <td>                                                                                                        
                                                                <a href="{{ URL::to('/list_o/detail/'.$row->id_organisasi) }}" class="btn btn-warning btn-sm">Detail</a>
                                                                <a href="{{ URL::to('/list_o/edit/'.$row->id_organisasi) }}" class="btn btn-success btn-sm">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_organisasi }}" id="multiDelete">
                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_o')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <td></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td></td>
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
        });
    });
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .o").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection