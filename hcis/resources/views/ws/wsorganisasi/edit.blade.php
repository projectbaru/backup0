@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  

<form action="{{route('update_o')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group " style="width:95%;">
        <div class="">
            @foreach($wsorganisasi as $data)
            <div class="row">
                <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                            <div class="col-sm-10">
                            <input type="hidden"  id="temp_id"  name="id_organisasi" value="{{ $data->id_organisasi }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                            </div>
                        </div>
            
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                            <div class="col-sm-10">
                                <select name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                    <option value=""></option>
                                    @foreach ($np['lookp'] as $np)
                                        <option value="{{$np->nama_perusahaan}}" {{ $np->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$np->nama_perusahaan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_perusahaan}}"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Organisasi</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_organisasi" readonly class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_organisasi}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                            <div class="col-sm-10">
                            <input type="text" required name="nama_organisai" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_organisasi}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Organisasi</label>
                            <div class="col-sm-10">
                            <select name="tipe_area" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                @foreach ($datas['looks'] as $v)
                                    <option value="{{$v->tipe_area}}" {{ $v->tipe_area == $data->tipe_area ? 'selected' : NULL }}>{{$v->tipe_area}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" required name="tipe_area" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tipe_area}}" id="colFormLabelSm" placeholder=""></textarea> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Organisasi</label>
                            <div class="col-sm-10">
                            <input type="text" name="grup_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->grup_organisasi}}" id="colFormLabelSm" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pusat Biaya</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_pusat_biaya" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_pusat_biaya}}" id="colFormLabelSm" placeholder="">
                            </div>
                        </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai" style="font-size:11px;" required value="{{$data->tanggal_mulai}}" class="form-control form-control-sm startDates" id="tanggal_mulai" placeholder="Tanggal mulai"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai}}" id="tanggal_selesai" placeholder="Tanggal selesai" readonly/>

                        <!-- <input type="date" name="tanggal_selesai" class="form-control form-control-sm" value="{{$data->tanggal_selesai}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">ID Induk Organisasi</label>
                        <div class="col-sm-10">
                            
                            
                            {{-- <input type="text" readonly name="id_induk_organisasi" style="font-size:11px;" value="{{$data->induk_organisasi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                            <input type="text" readonly name="id_induk_organisasi" style="font-size:11px;" value="{{$data->id_induk_organisasi}}" class="form-control form-control-sm" id="id_induk_organisasi" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Induk Organisasi</label>
                        <div class="col-sm-10">
                            <select name="induk_organisasi" id="induk_organisasi" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($organisasi['looko'] as $o ) 
                                <option value="{{$o->induk_organisasi}}" data-id_induk_organisasi="{{$o->id_induk_organisasi}}" {{ $o->induk_organisasi == $data->induk_organisasi ? 'selected' : NULL }}>{{$o->induk_organisasi}}</option>
                                @endforeach
                            </select>															
                        </div>
                    </div>

                    <!-- <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Induk Organisasi</label>
                        <div class="col-sm-10">
                        <input type="text" name="id_induk_organisasi" class="form-control form-control-sm" value="{{$data->id_induk_organisasi}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Induk Organisasi</label>
                        <div class="col-sm-10">
                            <select name="induk_organisasi" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                <option value=""></option>
                                @foreach ($datas['looks'] as $v)
                                    <option value="{{$v->nama_organisasi}}" {{ $v->nama_organisasi == $data->induk_organisasi ? 'selected' : NULL }}>{{$v->nama_organisasi}}</option>
                                @endforeach
                            </select>
                        <input type="text" name="induk_organisasi" class="form-control form-control-sm" id="colFormLabelSm" value="{{$data->induk_organisasi}}" style="font-size:11px;" placeholder="">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Unit Kerja</label>
                        <div class="col-sm-10">
                            <input type="text" required name="unit_kerja" class="form-control form-control-sm" value="{{$data->unit_kerja}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                </div> 
            </div>
            <hr>
            <b>Informasi Lainnya</b>
            <br>
            <br>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Struktur Organisasi</label>
                        <div class="col-sm-10">
                        <input type="text" required name="nama_struktur_organisasi" style="font-size:11px;" value="{{$data->nama_struktur_organisasi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Versi Struktur Organisasi</label>
                        <div class="col-sm-10">
                        <input type="text" required name="versi_struktur_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->versi_struktur_organisasi}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                        <div class="col-sm-10">
                            <input type="text" name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{$data->tingkat_organisasi}}" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Organisasi</label>
                        <div class="col-sm-10">
                        <select name="kode_tingkat_organisasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                            <option value=""></option>
                            @foreach ($to['looks'] as $v)
                                <option value="{{$v->kode_tingkat_organisasi}}" {{ $v->kode_tingkat_organisasi == $data->kode_tingkat_organisasi ? 'selected' : NULL }}>{{$v->kode_tingkat_organisasi}}</option><p style="color:red;font-size:10px;">* wajib isi</p>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Indeks Organisasi</label>
                        <div class="col-sm-10">
                        <input type="number" required name="nomor_indeks_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->nomor_indeks_organisasi}}" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Organisasi</label>
                        <div class="col-sm-10">
                        <input type="number" required name="urutan_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->urutan_organisasi}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan Organisasi</label>
                        <div class="col-sm-10">
                        <textarea type="text" name="keterangan_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->keterangan_organisasi}}" rows="4" cols="50" id="colFormLabelSm" placeholder="">{{$data->keterangan_organisasi}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Leher Struktur</label>
                        <div class="col-sm-5">
                        <input type="checkbox" name="leher_struktur" style="font-size:11px;" class="" id="colFormLabelSm" value="{{$data->leher_struktur}}" {{ $data->leher_struktur == 'Yes' ? 'checked' : NULL }}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Aktif</label>
                        <div class="col-sm-5">
                        <input type="checkbox" name="aktif" class="" id="colFormLabelSm" style="font-size:11px;" value="{{$data->aktif}}" {{ $data->aktif == 'Yes' ? 'checked' : NULL }}>
                        </div>
                    </div>
                </div>
            </div>  
            <hr>
            <b style="color:black;">Informasi Lainnya</b>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" required name="tanggal_mulai_efektif" id="colFormLabelSm" class="form-control form-control-sm" value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" value="{{ $data->tanggal_selesai_efektif }}" />
                        <!-- <input type="date" name="tanggal_selesai_efektif" id="colFormLabelSm" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                        <div class="col-sm-10">
                        <textarea type="text" name="keterangan" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" rows="4" cols="50" value="{{$data->keterangan}}" placeholder="">{{$data->keterangan}}</textarea>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
            @endforeach
            <div >
                <table>
                    <tr>

                    <!-- <td>
                        <a href="" class="btn">Hapus</a>
                    </td> -->
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="{{route('hapus_o',$data->id_organisasi)}}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;" id="btn_delete" >Hapus</a>
                        </td>
                        <td>
                            <a href="{{ route('list_o') }}" class="btn btn-secondary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>


@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')
<script>
    $('#induk_organisasi').change(function(){
        $('#id_induk_organisasi').val($('#induk_organisasi option:selected').data('id_induk_organisasi'));
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .o").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection