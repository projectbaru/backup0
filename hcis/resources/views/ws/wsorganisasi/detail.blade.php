@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<div class="container" style="width:95%;">
    <form action="{{route('simpan_o')}}" method="post"> {{ csrf_field() }}
        @foreach($wsorganisasi as $data)
        <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_organisasi }}" />

        <hr>
            <table>
                <tr>
                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_o', $data->id_organisasi)}}">Ubah</a></td>
                    <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_o', $data->id_organisasi)}}">Hapus</a></td>
                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_o')}}">Batal</a></td>
                </tr>
            </table><br>
            <div style="">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4" >
                            <table style="font-size:12px;">
                                <tr>
                                    <td>Nama Perusahaan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                </tr>
                                <tr>
                                    <td>Kode Organisasi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="kode_organisasi" value="{{$randomKode}}">{{$data->kode_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="nama_organisasi" value="{{$data->nama_organisasi}}">{{$data->nama_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Tipe Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="tipe_area" value="{{$data->tipe_area}}">{{$data->tipe_area}}</td>
                                </tr>
                                <tr>
                                    <td>Grup Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="grup_organisasi" value="{{$data->grup_organisasi}}">{{$data->grup_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Kode Pusat Biaya</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="kode_pusat_biaya" value="{{$data->kode_pusat_biaya}}">{{$data->kode_pusat_biaya}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-8" style="font-size:12px;">
                            <table style="font-size:12px;">
                                <tr>
                                    <td>Tanggal Mulai</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="tanggal_mulai" value="{{$data->tanggal_mulai}}">{{ $data->tanggal_mulai ? date('d-m-Y', strtotime($data->tanggal_mulai)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Selesai</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="tanggal_selesai" value="{{$data->tanggal_selesai}}">{{ $data->tanggal_selesai ? date('d-m-Y', strtotime($data->tanggal_selesai)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <td>ID Induk Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="id_induk_organisasi" value="{{$data->id_induk_organisasi}}">{{$data->id_induk_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Induk Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="induk_organisasi" value="{{$data->induk_organisasi}}">{{$data->induk_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Unit Kerja</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="unit_kerja" value="{{$data->unit_kerja}}">{{$data->unit_kerja}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <b style="color:black;">Informasi Lainnya</b>
                    <div class="row">
                        <div class="col-md-4">
                            <table style="font-size:12px;">
                                <tr>
                                    <td>Nama Struktur Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="nama_struktur_organisasi" value="{{$data->nama_struktur_organisasi}}">{{$data->nama_struktur_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Versi Struktur Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="versi_struktur_organisasi" value="{{$data->versi_struktur_organisasi}}">{{$data->versi_struktur_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Kode Tingkat Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="kode_tingkat_organisasi" value="{{$data->kode_tingkat_organisasi}}">{{$data->kode_tingkat_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Indeks Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="nomor_indeks_organisasi" value="{{$data->nomor_indeks_organisasi}}">{{$data->nomor_indeks_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Urutan Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="urutan_organisasi" value="{{$data->urutan_organisasi}}">{{$data->urutan_organisasi}}</td>
                                </tr>
                                <tr>
                                    <td>Keterangan Organisasi</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="keterangan_organisasi" value="{{$data->keterangan_organisasi}}">{{$data->keterangan_organisasi}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-8">
                            <table style="font-size:12px;">
                                <tr>
                                    <td>Leher Struktur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" onclick="return false" name="leher_struktur" value="{{$data->leher_struktur}}" {{ $data->leher_struktur == 'Yes' ? 'checked' : NULL }}></td>
                                </tr>
                                <tr>
                                    <td>Aktif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="aktif" onclick="return false"  value="{{$data->aktif}}" {{ $data->aktif == 'Yes' ? 'checked' : NULL }}></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <b style="color:black;">Rekaman Informasi</b>
                    <div class="row">
                        <div class="col">
                            <table style="font-size:12px;">
                                <tr>
                                    <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{ $data->tanggal_mulai_efektif ? date('d-m-Y', strtotime($data->tanggal_mulai_efektif)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{ $data->tanggal_selesai_efektif ? date('d-m-Y', strtotime($data->tanggal_selesai_efektif)) : '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Keterangan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
            <br>
            @endforeach
    </form>
</div>
@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .o").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection