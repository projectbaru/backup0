@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Kantor Cabang @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('simpan_kac')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
	<hr>
	<div class="form-group" style="width:95%;">
		<div class="">
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kantor</label>
						<div class="col-sm-10">
						<input type="text" name="kode_kantor" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""  readonly  required value="{{$random}}" ><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kantor</label>
						<div class="col-sm-10">
						<input type="text" name="nama_kantor" required class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Lokasi Kerja</label>
						<div class="col-sm-10">
							<select name="kode_lokasi_kerja" id="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
								<option value="" selected disabled>--- Pilih ---</option>
								@foreach ($dataklk['looks'] as $k )
								<option value="{{$k->kode_lokasi_kerja}}" data-name ="{{$k->nama_lokasi_kerja}}">{{$k->kode_lokasi_kerja}}</option>
								@endforeach
							</select><p style="color:red;font-size:10px;">* wajib isi</p>		
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Lokasi Kerja</label>
						<div class="col-sm-10">
						{{-- <input type="text" required name="nama_lokasi_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
						<select name="nama_lokasi_kerja" id="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach ($dataklk['looks'] as $n )
							<option value="{{$n->nama_lokasi_kerja}}" data-kode ="{{$n->kode_lokasi_kerja}}">{{$n->nama_lokasi_kerja}}</option>
							@endforeach
						</select><p style="color:red;font-size:10px;">* wajib isi</p>	
						</div>
					</div>
					<!-- <div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
						<div class="col-sm-10">

						
							<input readonly type="text" name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
						<div class="col-sm-10">
							<select name="nama_lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" required>
								<option value="" selected>--- Pilih ---</option>
								@foreach ($dataklk['looks'] as $p )
								<option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
								@endforeach
							</select>
							<input type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
						</div>
					</div> -->
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Kantor</label>
						<div class="col-sm-10">
							<select name="tipe_kantor" required id="tipe_kantor" class="form-control form-control-sm" style="font-size:11px;">
								<option value="pusat">Pusat</option>
								<option value="cabang">Cabang</option>
								<option value="ro">RO</option>
							</select><p style="color:red;font-size:10px;">* wajib isi</p>
							<!-- <input type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">UMK / UMP</label>
						<div class="col-sm-10">
							<input type="number" name="umk_ump" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
						</div>
					</div>
				</div>
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor NPWP</label>
						<div class="col-sm-10">
						<!-- <input type="text" id="npwp" name="nomor_npwp" placeholder="8 600 00 000" required class="form-control form-control-sm" pattern="(8 6)\d{2} \d{2} \d{3}" /> -->
							<input id="npwp" name="nomor_npwp" data-inputmask="'mask': '99.999.999.9-999.999'" style="font-size:11px;" placeholder="" required  class="form-control form-control-sm" />
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Ttd SPT (ID PIC)</label>
						<div class="col-sm-10">
						<input type="text" name="ttd_spt"  class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
						<div class="col-sm-10">
						<textarea type="text" name="keterangan" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" required placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
						<div class="col-sm-10">
						<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

						<!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>
							<!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->
						</div>
					</div>

				</div>                                                                                                 
			</div><hr>
		</div>
	</div>
	<div>
		<table>
			<tr>
				<!-- <td>
				<a href="" class="btn">Hapus</a>
			</td> -->
				<!-- <td>
				<a href="" class="btn">Hapus</a>
			</td> -->
				
				<td>
					<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
				</td>
				<td>
					<a href="{{ route('list_kac') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
				</td>
			</tr>
		</table>
		<br>
	</div>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
			<script> 
				$(document).ready(function() {
					$('#npwp').inputmask();
				});
			</script>
			<script>
				$('#kode_lokasi_kerja').change(function(){
					$('#nama_lokasi_kerja').val($('#kode_lokasi_kerja option:selected').data('name'));
				})
				$('#nama_lokasi_kerja').change(function(){
					$('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode'));
				})
			</script>
@endpush
			
		
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .kac").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection
