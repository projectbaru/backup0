@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Alamat Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('simpan_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group" style="width:95%;">
        <div class="">
            <div class="row">
                <div class="col" style="font-size: 10px;">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Alamat</label>
                        <div class="col-sm-10">
                            <input type="text" required name="kode_alamat" style="font-size:11px;" value="{{$randomKode}}" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Perusahaan</label>
                        <div class="col-sm-10">
                            <select name="kode_perusahaan" id="kode_perusahaan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($data['looks'] as $p )
                                <option value="{{$p->kode_perusahaan}}" data-name="{{$p->nama_perusahaan}}">{{$p->kode_perusahaan}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Perusahaan</label>
                        <div class="col-sm-10">
                            {{-- <input type="text" required name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                            <select name="nama_perusahaan" id="name_p" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($data['looks'] as $np )
                                <option value="{{$np->nama_perusahaan}}" data-kode="{{$np->kode_perusahaan}}">{{$np->nama_perusahaan}}</option>
                                @endforeach
                            </select>
                            <p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Status Alamat Untuk SPT</label>
                        <div class="col-sm-10">
                            <select name="status_alamat_untuk_spt" id="status_alamat_untuk_spt" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                            <p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Alamat</label>
                        <div class="col-sm-10">
                            <select name="jenis_alamat" id="jenis_alamat" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="Alamat Kantor Pusat">Alamat Kantor Pusat</option>
                                <option value="Alamat Cabang">Alamat Cabang</option>
                            </select>
                            <p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Alamat</label>
                        <div class="col-sm-10">
                            <textarea type="text" rows="8" cols="50" required name="alamat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Provinsi</label>
                        <div class="col-sm-10">
                            <select name="kota" id="name_p" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($provi['looks'] as $prov )
                                <option value="{{$prov->name}}" >{{$prov->name}}</option>
                                @endforeach
                            </select>
                            <!-- <input type="text" name="kota" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Pos</label>
                        <div class="col-sm-10">
                            <input type="number" required name="kode_pos" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                            <p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Telepon</label>
                        <!-- <div class="col-sm-10">
                            <input type="number" name="telepon" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div> -->
                        <div class="col-sm-10">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend input-group-prepend-sm">
                                    <span class="input-group-text input-group-text-sm">(021)</span>
                                </div>
                                <input type="number" name="telepon" class="form-control form-control-sm" aria-label="Amount (to the nearest dollar)">
                                
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <hr>
            <b style="color:black;">Rekaman Informasi</b><br>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" rows="8" cols="50" style="font-size:11px;" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
    <div class="">
        <table>
            <tr>
                <td>
                    <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                </td>
                <td>
                    <a href="{{ route('list_a') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                </td>
            </tr>
        </table>
    </div>
    <br>
</form>


<!-- <script src="{{ asset('js/sb-admin-2.min.js') }}"></script> -->
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
@endsection

@push('scripts')
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
    $(document).ready(function() {
        $("#mm-active").addClass("active");
        $(" .ap").addClass("active");
        $("#a-homes").addClass("active");
    });

    $('#kode_perusahaan').change(function() {
        $('#name_p').val($('#kode_perusahaan option:selected').data('name'));
    })
    $('#name_p').change(function() {
        $('#kode_perusahaan').val($('#name_p option:selected').data('kode'));
    })
</script>
@endsection