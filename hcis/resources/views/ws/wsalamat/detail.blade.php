@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Alamat Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<div class="container" style="width:90%;">
<form action="{{route('duplicate_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <div>
        <hr>
        @foreach($wsalamat as $data)
        <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_alamat }}" />

        <table>
            <tr>
                <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_a', $data->id_alamat)}}">Ubah</a></td>
                <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_a', $data->id_alamat)}}" id="btn_delete">Hapus</a></td>
                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_a')}}">Batal</a></td>
            </tr>
        </table><br>
        <!-- <b style="color:black;">Informasi Perusahaan</b> -->
        <div class="row container" style="width:80%;">
            <div class="col">
                <table style="font-size:12px;">
                    <tr>
                        <td></td>
                        <td></td>
                        <td><input type="hidden" name="kode_alamat" value="{{$randomKode}}"></td>
                    </tr>
                    <tr>
                        <td>Kode Perusahaan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_perusahaan" value="{{$data->kode_perusahaan}}">{{$data->kode_perusahaan}}</td>
                    </tr>
                    <tr>
                        <td>Nama Perusahaan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                    </tr>
                    <tr>
                        <td>Status Alamat Untuk SPT</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="status_alamat_untuk_spt" value="{{$data->status_alamat_untuk_spt}}">{{$data->status_alamat_untuk_spt}}</td>
                    </tr>
                    <tr>
                        <td>Jenis Alamat</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="jenis_alamat" value="{{$data->jenis_alamat}}">{{$data->jenis_alamat}}</td>
                    </tr>
                </table>
            </div>
            <div class="col" style="font-size:12px;">
                <table>
                    <tr>
                        <td valign="top">Alamat Perusahaan</td>
                        <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="alamat" value="{{$data->alamat}}">{{$data->alamat}}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kota" value="{{$data->kota}}">{{$data->kota}}</td>
                    </tr>
                    <tr>
                        <td>Kode Pos</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_pos" value="{{$data->kode_pos}}">{{$data->kode_pos}}</td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="telepon" value="{{$data->telepon}}">{{$data->telepon}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        
        <div class="row container" style="width:80%;">
        <b style="color:black;">Rekaman Informasi</b>
            <div class="col">
                <table style="font-size:12px;">
                    <tr>
                        <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_mulai_efektif" value="$data->tanggal_mulai_efektif">{{ $data->tanggal_mulai_efektif ? date('d-m-Y', strtotime($data->tanggal_mulai_efektif)) : '-' }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_selesai_efektif" value="$data->tanggal_selesai_efektif">{{ $data->tanggal_selesai_efektif ? date('d-m-Y', strtotime($data->tanggal_selesai_efektif)) : '-' }}</td>
                    </tr>
                    <tr>
                        <td>Keterangan&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                    </tr>
                </table>
            </div>
        </div> <br>
        @endforeach
        <br><br>

    </div>
</form>
</div>

@endsection

@push('scripts')
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_a') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });

        $("#mm-active").addClass("active");
        $(" .ap").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection