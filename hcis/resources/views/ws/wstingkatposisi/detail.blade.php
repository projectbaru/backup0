@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Posisi @endslot
@slot('title') Workstrukture @endslot
@endcomponent      
<div class="container" style="width:95%;">
<form action="{{route('simpan_tp')}}" method="post">{{ csrf_field() }}
    <hr>
    @foreach($wstingkatposisi as $data)
    <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_tingkat_posisi }}">

    <table>
        <tr>
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_tp', $data->id_tingkat_posisi)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_tp', $data->id_tingkat_posisi)}}"  id="btn_delete">Hapus</a></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_tp')}}">Batal</a></td>
        </tr>
    </table><br>
    <b style="color:black;">Detail Tingkat Posisi</b>
        <div class="row">
            <div class="col-md-4" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Kode Tingkat Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_tingkat_posisi" value="KTP-{{now()->isoFormat('dmYYs')}}">{{$data->kode_tingkat_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Nama Tingkat Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_tingkat_posisi" value="{{$data->nama_tingkat_posisi}}">{{$data->nama_tingkat_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Tingkat Urutan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="urutan_tingkat" value="{{$data->urutan_tingkat}}">{{$data->urutan_tingkat}}</td>
                    </tr>
                    <tr>
                        <td>Urutan Tampilan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="urutan_tampilan" value="{{$data->urutan_tampilan}}">{{$data->urutan_tampilan}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8" style="font-size:12px;">
                <table>
                    <tr>
                        <td>Keterangan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai Efektif</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_mulai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai Efektif</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_selesai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                    </tr>
                </table>
            </div>
        </div>
    @endforeach
    <br>
</form>
</div>                          	
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>



@endpush

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_tp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .tpo").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection
