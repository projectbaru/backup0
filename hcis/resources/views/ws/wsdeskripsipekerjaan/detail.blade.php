@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Kantor Cabang @endslot
@slot('title') Workstrukture @endslot
@endcomponent
                    <form action="{{route('simpan_dp')}}" method="post" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <input type="hidden" name="temp_id" id="temp_id" value="{{ $wsdeskripsipekerjaan->id_deskripsi_pekerjaan }}" />

                        <hr>
                        <table>
                            <tr>
                                <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_hdp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Ubah</a></td>
                                <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_dp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Hapus</a></td>
                                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_dp')}}">Batal</a></td>
                            </tr>
                        </table>
                        <div>
                            <b>Informasi Deskripsi Pekerjaan</b>
                            <div class="row">
                                <div class="col">
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Kode Deskripsi Pekerjaan</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->kode_deskripsi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Dokumen</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nomor_dokumen ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Edisi</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->edisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Edisi</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->tanggal_edisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Revisi</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nomor_revisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <hr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Revisi</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->tanggal_revisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pengawas</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_pengawas ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Fungsi Jabatan</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->fungsi_jabatan ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lingkup Aktivitas</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->lingkup_aktivitas ?? 'NO DATA' }}</td>
                                        </tr>
                                        {{-- {{dd($tanggungJawab)}} --}}
                                        @foreach ($tanggungJawab as $tanggung)
                                        <tr>
                                            <td>Tanggung Jawab {{ $loop->iteration }}</td>
                                            <td>:</td>
                                            <td>{{ $tanggung ?? 'NO DATA' }}</td>
                                        </tr>
                                        @endforeach

                                        @foreach ($wewenang as $w)
                                        <tr>
                                            <td>Wewenang {{ $loop->iteration }}</td>
                                            <td>:</td>
                                            <td>{{ $w ?? 'NO DATA' }}</td>
                                        </tr>
                                        @endforeach

                                    </table>
                                </div>
                                <div class="col">
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Nama Jabatan</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_jabatan ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Karyawan</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_karyawan ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Divisi</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->divisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Departemen</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->departemen ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Lokasi Kerja</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col">
                                    <table style="font-size:12px;">
                                        {{-- <tr>
                                            <td>Kualifikasi Jabatan</td>
                                            <td>&nbsp;&nbsp;:</td>
                                            <td>{{ $kualifikasiJabatan ?? 'NO DATA' }}</td>
                                        </tr> --}}
                                        @foreach ($kualifikasiJabatan as $jabatan)
                                        <tr>
                                            <td>Kualifikasi Jabatan {{ $loop->iteration }}</td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                                            <td>{{ $jabatan['jabatan'] ?? 'NO DATA' }}</td>
                                        </tr>
                                        @endforeach


                                    </table>
                                </div>
                                <div class="col">
                                    <table style="font-size:12px;">
                                        @foreach ($kualifikasiJabatan as $jabatan)
                                        <tr>
                                            <td>Tipe {{ $loop->iteration }}</td>
                                            <td>:</td>
                                            <td>{{ $jabatan['tipe'] ?? 'NO DATA' }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Kolom Tanda Tangan</b>
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Nama Atasan Langsung</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_atasan_langsung ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pengawas</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->nama_pengawas ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Dibuat Oleh</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->dibuat_oleh ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Diperiksa Oleh</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->diperiksa_oleh ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Disahkan Oleh</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->disahkan_oleh ?? 'NO DATA' }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <b>Rekaman Informasi</b>
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Tanggal Mulai Efektif</td>
                                            <td>:</td>
                                            <td>{{date('d-m-Y', strtotime($wsdeskripsipekerjaan->tanggal_mulai_efektif ?? 'NO DATA' ))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Selesai Efektif</td>
                                            <td>:</td>
                                            <td>{{date('d-m-Y', strtotime($wsdeskripsipekerjaan->tanggal_selesai_efektif ?? 'NO DATA' ))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Keterangan</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->keterangan ?? 'NO DATA' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <b>Detail</b>
                            <div class="row">
                                <div class="col-md-6">
                                    <table style="font-size:12px; width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>No</td>
                                                <td>PDCA</td>
                                                <td>BSC</td>
                                                <td>Deskripsi Kerja</td>
                                            </tr>
                                            @foreach($detail as $d)
                                            <tr>
                                                <td>{{ $d->no }}</td>
                                                <td>{{ $d->pdca }}</td>
                                                <td>{{ $d->bsc }}</td>
                                                <td>{{ $d->deskripsi_pekerjaan }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tr>
                                            <td>PDCA</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>BSC</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Deskripsi Kerja</td>
                                            <td>:</td>
                                            <td>{{ $wsdeskripsipekerjaan->keterangan ?? 'NO DATA' }}</td>
                                        </tr> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

@endpush
