@extends('wsdeskripsipekerjaan.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Edit Detail Deskripsi Pekerjaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">deskripsi pekerjaan</a></li>
                                        <li class="breadcrumb-item"><a href="#!">detail deskripsi pekerjaan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form autocomplete="off" action="{{ URL::to('/update_dp') }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="id_deskripsi_pekerjaan" id="id_deskripsi_pekerjaan" value="{{ $wsdeskripsipekerjaan->id_deskripsi_pekerjaan }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="">
                                <b style="color:black;">Informasi</b>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode
                                                Posisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="kode_posisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->kode_posisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Posisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_posisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nama_posisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="descriptionInputContainer">

                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" value="{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif }}" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                                            <!-- <input type="date" style="font-size:11px;" name="tanggal_mulai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif }}" class="form-control form-control-sm" require id="colFormLabelSm" placeholder=""> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                        <div class="col-sm-10">
                                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif }}" placeholder="Tanggal selesai efektif"/>

                                            <!-- <input type="date" style="font-size:11px;" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif }}" require id="colFormLabelSm" placeholder=""> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" style="font-size:11px;" rows="4" cols="50" name="keterangan" value="{{ $wsdeskripsipekerjaan->keterangan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $wsdeskripsipekerjaan->keterangan }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <!-- <td>
                            <a href="" class="btn">Hapus</a>
                        </td> -->
                                    <!-- <td>
                            <a href="" class="btn">Hapus</a>
                        </td> -->
                                    <td>
                                        <a href="{{ route('list_dp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

@include('js.wsdeskripsipekerjaan.edit')
@endsection
@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_a') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection
                 