@extends('wsdeskripsipekerjaan.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Edit Deskripsi Pekerjaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">header deskripsi pekerjaan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form autocomplete="off" action="{{ route('update_dp')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <hr>
                        <div class="form-group" style="width:95%;">
                            <div class="">
                                <div class="row">
                                    <div class="col" style="font-size: 10px;">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;"></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" style="font-size:11px;" name="id_deskripsi_pekerjaan" class="form-control form-control-sm" require id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->id_deskripsi_pekerjaan }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Deskripsi
                                                Pekerjaan</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly style="font-size:11px;" name="kode_deskripsi" class="form-control form-control-sm" require id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->kode_deskripsi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Dokumen</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nomor_dokumen" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nomor_dokumen }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Edisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="edisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->edisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                Edisi</label>
                                            <div class="col-sm-10">
                                                <input type="date" style="font-size:11px;" name="tanggal_edisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->tanggal_edisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nomor
                                                Revisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nomor_revisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nomor_revisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                Revisi</label>
                                            <div class="col-sm-10">
                                                <input type="date" style="font-size:11px;" name="tanggal_revisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->tanggal_revisi }}"></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="nama_posisi" id="nama_posisi" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datapo['looks'] as $po )
                                                    <option value="{{$po->nama_posisi}}" data-name="{{$po->nama_posisi}}" {{ $po->nama_posisi == $wsdeskripsipekerjaan->nama_posisi ? 'selected' : NULL }}>{{$po->nama_posisi}}</option><p style="color:red;font-size:10px;">* wajib isi</p>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Jabatan</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_jabatan" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nama_jabatan }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Karyawan</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_karyawan" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nama_karyawan }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Divisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="divisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->divisi }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Departemen</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="departemen" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->departemen }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_lokasi_kerja" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nama_lokasi_kerja }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <!-- <b style="color:black;">Detail Pajak Perusahaan</b> -->
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                Pengawas</label>
                                            <div class="col-sm-10 autocomplete">
                                                <input type="text" style="font-size:11px;" name="nama_pengawas" class="form-control form-control-sm" {{-- id="colFormLabelSm" --}} id="myInput" value="{{ $wsdeskripsipekerjaan->nama_pengawas }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Fungsi
                                                Jabatan</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="fungsi_jabatan" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->fungsi_jabatan }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lingkup
                                                Aktivitas</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="lingkup_aktivitas" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->lingkup_aktivitas }}"><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <br>
                                        <hr style="width:50%;">
                                        <div class="form-group row" id="responsibilityInputContainer">
                                        </div>
                                        <hr>
                                        <div class="form-group row" id="authorityInputContainer">
                                        </div>
                                        <hr>
                                        <div class="form-group row" id="qualificationInputContainer">
                                        </div>
                                        <br>
                                        <hr style="width:50%;">
                                    </div>
                                </div>
                                <b style="color:black;">Kolom Tanda Tangan</b>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Atasan Langsung</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_atasan_langsung" value="{{$wsdeskripsipekerjaan->nama_atasan_langsung}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Pengawas</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="nama_pengawas" value="{{$wsdeskripsipekerjaan->nama_pengawas}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Dibuat Oleh</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="dibuat_oleh" value="{{$wsdeskripsipekerjaan->dibuat_oleh}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Diperiksa Oleh</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="diperiksa_oleh" value="{{$wsdeskripsipekerjaan->diperiksa_oleh}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Disahkan Oleh</label>
                                            <div class="col-sm-10">
                                                <input type="text" style="font-size:11px;" name="disahkan_oleh" value="{{$wsdeskripsipekerjaan->disahkan_oleh}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                Mulai Efektif</label>
                                            <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" value="{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif }}" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

                                                <!-- <input type="date" style="font-size:11px;" name="tanggal_mulai_efektif" class="form-control form-control-sm" id="tanggal_mulai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif }}"> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                Selesai Efektif</label>
                                            <div class="col-sm-10">
                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif }}" placeholder="Tanggal selesai efektif"/>

                                                <!-- <input type="date" style="font-size:11px;" name="tanggal_selesai_efektif" class="form-control form-control-sm" id="tanggal_selesai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif }}"> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" style="font-size:11px;" name="keterangan" rows="4" cols="50" class="form-control form-control-sm" id="colFormLabelSm">{{ $wsdeskripsipekerjaan->keterangan }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Detail</b>
                                <div class="row" id="detailWrapper">
                                    <div class="col-sm-11">
                                        <div class="form-group row">
                                            <label for="kode_posisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="kode_posisi" id="kode_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datapo['looks'] as $p )
                                                    <option value="{{$p->kode_posisi}}" <?= $wsdeskripsipekerjaan->kode_posisi == $p->kode_posisi ? 'selected' : '' ?>>{{$p->kode_posisi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="nama_posisi[]" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datapo['looks'] as $v )
                                                    <option value="{{$v->nama_posisi}}">{{$v->nama_posisi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div> -->
                                        <div class="form-group row" id="descriptionInputContainer">
                                            <label for="deskripsi_pekerjaan1" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan 1</label>
                                            <div class="col-sm-4">
                                                <textarea name="deskripsi_pekerjaan[]" id="deskripsi_pekerjaan1" value="{{ $detail[0]->deskripsi_pekerjaan }}" style="font-size:11px;" rows="5" cols="60" class="form-control form-control-sm" required><?= $detail[0]->deskripsi_pekerjaan ?></textarea>
                                            </div>
                                            <label for="pdca1" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                                            <div class="col-sm-2">
                                                <select style="font-size:11px;" name="pdca[]" id="pdca1" class="form-control form-control-sm" required>
                                                    <option value="" selected disabled>--- Belum dipilih ---</option>
                                                    <option value="plan" <?= $detail[0]->pdca == 'plan' ? 'selected' : '' ?>>Plan</option>
                                                    <option value="do" <?= $detail[0]->pdca == 'do' ? 'selected' : '' ?>>Do</option>
                                                    <option value="check" <?= $detail[0]->pdca == 'check' ? 'selected' : '' ?>>Check</option>
                                                    <option value="action" <?= $detail[0]->pdca == 'action' ? 'selected' : '' ?>>Action</option>
                                                </select>
                                            </div>
                                            <label for="bsc1" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                                            <div class="col-sm-2">
                                                <select style="font-size:11px;" name="bsc[]" id="bsc1" class="form-control form-control-sm" required>
                                                    <option value="" selected disabled>--- Belum dipilih ---</option>
                                                    <option value="Perspektif BSC" <?= $detail[0]->bsc == 'Perspektif BSC' ? 'selected' : '' ?>>Perspektif BSC</option>
                                                    <option value="C" <?= $detail[0]->bsc == 'C' ? 'selected' : '' ?>>C</option>
                                                    <option value="F" <?= $detail[0]->bsc == 'F' ? 'selected' : '' ?>>F</option>
                                                    <option value="IP" <?= $detail[0]->bsc == 'IP' ? 'selected' : '' ?>>IP</option>
                                                    <option value="LG" <?= $detail[0]->bsc == 'LG' ? 'selected' : '' ?>>LG</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-sm btn-primary" style="font-size:10px;" type="button" id="addDescriptionDet">
                                            <i class="fa-solid fa-plus"></i>
                                        </button>
                                    </div>
                                    @if(count($detail) > 1)
                                    @foreach($detail->slice(1, count($detail)) as $d)
                                    <div id="descriptionCloneDet{{ $d->no }}" class="mt-3 col-sm-11 descriptionCloneDet{{ $d->no }}">
                                        <div class="form-group row">
                                            <label for="deskripsi_pekerjaan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan</label>
                                            <div class="col-sm-4">
                                                <textarea rows="5" cols="60" style="font-size:11px;" id="deskripsi_pekerjaan{{ $d->no }}" name="deskripsi_pekerjaan[]" value="{{ $d->deskripsi_pekerjaan }}" class="form-control form-control-sm" required>{{ $d->deskripsi_pekerjaan }}</textarea>
                                            </div>
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                                            <div class="col-sm-2">
                                                <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                                    <option value="" selected disabled>--- Belum dipilih ---</option>
                                                    <option value="plan" <?= $d->pdca == 'plan' ? 'selected' : '' ?>>Plan</option>
                                                    <option value="do" <?= $d->pdca == 'do' ? 'selected' : '' ?>>Do</option>
                                                    <option value="check" <?= $d->pdca == 'check' ? 'selected' : '' ?>>Check</option>
                                                    <option value="action" <?= $d->pdca == 'action' ? 'selected' : '' ?>>Action</option>
                                                </select>
                                            </div>
                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                                            <div class="col-sm-2">
                                                <select style="font-size:11px;" name="bsc[]" class="form-control form-control-sm" required>
                                                    <option value="" selected disabled>--- Belum dipilih ---</option>
                                                    <option value="Perspektif BSC" <?= $d->bsc == 'Perspektif BSC' ? 'selected' : '' ?>>Perspektif BSC</option>
                                                    <option value="C" <?= $d->bsc == 'C' ? 'selected' : '' ?>>C</option>
                                                    <option value="F" <?= $d->bsc == 'F' ? 'selected' : '' ?>>F</option>
                                                    <option value="IP" <?= $d->bsc == 'IP' ? 'selected' : '' ?>>IP</option>
                                                    <option value="LG" <?= $d->bsc == 'LG' ? 'selected' : '' ?>>LG</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 descriptionCloneDet{{ $d->no }}">
                                        <button style="font-size:10px; margin-top:15px;" type="button" class="btn btn-danger btn-sm removeDescriptionDet" data-number="{{ $d->no }}">
                                            <i class="fa-solid fa-minus"></i>
                                        </button>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                        <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                        <td>
                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                        </td>
                                        <td>
                                            <a href="{{ route('list_dp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script>
    function autocomplete(inp, arr) {
        var currentFocus;
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            this.parentNode.appendChild(a);
            for (i = 0; i < arr.length; i++) {
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    b.addEventListener("click", function(e) {
                        inp.value = this.getElementsByTagName("input")[0].value;
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                currentFocus++;
                addActive(x);
            } else if (e.keyCode == 38) { //up
                currentFocus--;
                addActive(x);
            } else if (e.keyCode == 13) {
                e.preventDefault();
                if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            if (!x) return false;
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        document.addEventListener("click", function(e) {
            closeAllLists(e.target);
        });
    }
    var nama = [];
    // yepi 
    var nama = [];
    var datas = <?= json_encode($data); ?>;
    datas.forEach(el => {
        nama.push(el.name);
    });

    autocomplete(document.getElementById("myInput"), nama);
</script>
@include('js.wsdeskripsipekerjaan.edit')
@endsection
@section('add-scripts')
<script>
    $(function() {
        // yepi
        var looks = <?= json_encode($detail) ?>;
        var jumlah = looks.length;

        var wrapperDet = $('#detailWrapper');
        var cc = jumlah > 0 ? jumlah : 1;
        console.log(cc)


        $('#addDescriptionDet').click(function(e) {
            e.preventDefault();

            if (cc < 1) {
                cc++;
            }
            cc++;
            $(wrapperDet).append(`<div id="descriptionCloneDet${cc}" class="mt-3 col-sm-11 descriptionCloneDet${cc}">                                                          
                                <div class="form-group row">
                                  <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan</label>
                                    <div class="col-sm-4">
                                      <textarea rows="5" cols="60" style="font-size:11px;" id="deskripsi_pekerjaan${cc}"  name="deskripsi_pekerjaan[]" class="form-control form-control-sm" required></textarea>
                                    </div>
                                  <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                                  <div class="col-sm-2">
                                    <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                        <option value="" selected disabled>--- Belum dipilih ---</option>
                                        <option value="plan">Plan</option>
                                        <option value="do">Do</option>
                                        <option value="check">Check</option>
                                        <option value="action">Action</option>
                                    </select>
                                </div>
                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                                <div class="col-sm-2">
                                    <select style="font-size:11px;" name="bsc[]" class="form-control form-control-sm" required>
                                        <option value="" selected disabled>--- Belum dipilih ---</option>
                                        <option value="BSC">BSC</option>
                                        <option value="C">C</option>
                                        <option value="F">F</option>
                                        <option value="IP">IP</option>
                                        <option value="LG">LG</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                        <div class="col-sm-1 descriptionCloneDet${cc}">
                            <button style="font-size:10px; margin-top:15px;" type="button" class="btn btn-danger btn-sm removeDescriptionDet" data-number="${cc}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    `);
            $('.removeDescriptionDet').click(function(e) {
                e.preventDefault();
                var numbers = $(this).data('number');
                $(`.descriptionCloneDet${numbers}`).remove();
                cc--;
            });
        });

        if (jumlah > 1) {
            $('.removeDescriptionDet').click(function(e) {
                e.preventDefault();
                var numbers = $(this).data('number');
                $(`.descriptionCloneDet${numbers}`).remove();
                cc--;
            });
        }

    });
</script>
@endsection