@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Deskripsi Pekerjaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
    <form autocomplete="off" action="{{ URL::to('/simpan_dp') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <hr>

        <div class="form-group " style="width:95%;">
            <div class="">
                <div class="row">
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Deskripsi
                                Pekerjaan</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="kode_deskripsi" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="" value="{{$random}}" readonly><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Dokumen</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nomor_dokumen" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Edisi</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="edisi" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                Edisi</label>
                            <div class="col-sm-10">
                                <input type="date" style="font-size:11px;" name="tanggal_edisi" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nomor
                                Revisi</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nomor_revisi" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                Revisi</label>
                            <div class="col-sm-10">
                                <input type="date" style="font-size:11px;" name="tanggal_revisi" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                Posisi</label>
                            <div class="col-sm-10">
                                    <select name="nama_posisi" id="nama_posisi" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
                                        <option value="" selected disabled>--- Pilih ---</option>
                                        @foreach ($datapo['looks'] as $p ) 
                                        <option value="{{$p->nama_posisi}}" data-kode_posisi="{{$p->kode_posisi}}">{{$p->nama_posisi}}</option>
                                        @endforeach
                                    </select><p style="color:red;font-size:10px;">* wajib isi</p>

                                <!-- <input type="text" style="font-size:11px;" name="nama_jabatan" class="form-control form-control-sm" id="nama_jabatan" placeholder="">

                                <select name="nama_posisi" id="nama_posisi" readonly class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datapo['looks'] as $po )
                                    <option value="{{$po->nama_posisi}}" data-kode="{{$po->kode_posisi}}">{{$po->nama_posisi}}</option>
                                    @endforeach
                                </select> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_jabatan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Jabatan</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nama_jabatan" class="form-control form-control-sm" id="nama_jabatan" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_karyawan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Karyawan</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nama_karyawan" class="form-control form-control-sm" id="nama_karyawan" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="divisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Divisi</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="divisi" class="form-control form-control-sm" id="divisi" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="departemen" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Departemen</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="departemen" class="form-control form-control-sm" id="departemen" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_lokasi_kerja" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Lokasi Kerja</label>
                            <div class="col-sm-10">
                                <select name="nama_lokasi_kerja" id="nama_lokasi_kerja" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datalk['looks'] as $lk )
                                    <option value="{{$lk->nama_lokasi_kerja}}" data-name="{{$lk->nama_lokasi_kerja}}">{{$lk->nama_lokasi_kerja}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <!-- <b style="color:black;">Detail Pajak Perusahaan</b> -->
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="nama_pengawas" style="font-size:11px;" class=" col-sm-2 col-form-label col-form-label-sm">Nama Pengawas</label>
                            <div class="col-sm-10 autocomplete">
                                <input type="text" style="font-size:11px;" name="nama_pengawas" class="form-control form-control-sm" id="nama_pengawas" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fungsi_jabatan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Fungsi Jabatan</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="fungsi_jabatan" class="form-control form-control-sm" id="fungsi_jabatan" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lingkup_aktivitas" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lingkup Aktivitas</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="lingkup_aktivitas" class="form-control form-control-sm" id="lingkup_aktivitas" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <br>
                        <hr style="width:50%;">
                        <div class="form-group row" id="responsibilityInputContainer">
                            <label for="tanggung_jawab" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm" required>Tanggung Jawab</label>
                            <div class="col-sm-9">
                                <input type="text" style="font-size:11px;" id="tanggung_jawab" name="tanggung_jawab[]" class="form-control form-control-sm"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addResponsibilityInput">
                                    <i class="fa-solid fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row" id="authorityInputContainer">
                            <label for="wewenang" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang</label>
                            <div class="col-sm-9">
                                <input type="text" style="font-size:11px;" id="wewenang" name="wewenang[]" class="form-control form-control-sm" required><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addAuthorityInput">
                                    <i class="fa-solid fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row" id="qualificationInputContainer">
                            <label for="kualifikasi_jabatan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan</label>
                            <div class="col-sm-3">
                                <input type="text" style="font-size:11px;" id="kualifikasi_jabatan" name="kualifikasi_jabatan[]" class="form-control form-control-sm" required><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <label for="tipe_kualifikasi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi</label>
                            <div class="col-sm-4">
                                <select name="tipe_kualifikasi[]" id="tipe_kualifikasi" style="font-size:11px;" class="form-control form-control-sm" required>
                                    <option value="" selected disabled>--- Belum dipilih ---</option>
                                    <option value="pendidikan">Pendidikan</option>
                                    <option value="pengalaman">Pengalaman</option>
                                    <option value="pelatihan/pengetahuan tambahan">Pelatihan/pengetahuan Tambahan</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
                                    <i class="fa-solid fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <br>
                        <hr style="width:50%;">
                    </div>
                </div>
                <b style="color:black;">Kolom Tanda Tangan</b>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Atasan Langsung</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nama_atasan_langsung" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Pengawas</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="nama_pengawas" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Dibuat Oleh</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="dibuat_oleh" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Diperiksa Oleh</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="diperiksa_oleh" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Disahkan Oleh</label>
                            <div class="col-sm-10">
                                <input type="text" style="font-size:11px;" name="disahkan_oleh" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b style="color:black;">Rekaman Informasi</b>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                Mulai Efektif</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                Selesai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                            <div class="col-sm-10">
                                <textarea type="text" rows="4" cols="50" style="font-size:11px;" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b style="color:black;">Detail</b>
                <div class="row" id="detailWrapper">
                    <div class="col-sm-11">
                        <div class="form-group row">
                            <label for="kode_posisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Posisi</label>
                            <div class="col-sm-10">
                                <!-- <select name="det_kode_posisi[]" id="det_kode_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datapo['looks'] as $p )
                                    <option data-name ="{{$p->nama_posisi}}" value="{{$p->kode_posisi}}">{{$p->kode_posisi}}</option>
                                    @endforeach
                                </select> -->
                                <input type="text" readonly name="kode_posisi" style="font-size:11px;" value="" class="form-control form-control-sm" id="kode_posisi" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>

                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label for="det_nama_posisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Posisi</label>
                            <div class="col-sm-10">
                                <select name="det_nama_posisi[]" class="form-control form-control-sm" id="det_nama_posisi" style="font-size:11px;">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datapo['looks'] as $v )
                                    <option value="{{$v->nama_posisi}}">{{$v->nama_posisi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group row" id="descriptionInputContainer">
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan</label>
                            <div class="col-sm-4">
                                <textarea name="deskripsi_pekerjaan[]" style="font-size:11px;" rows="5" cols="60" class="form-control form-control-sm" required></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                            <div class="col-sm-2">
                                <select style="font-size:11px;" name="pdca[]" class="form-control form-control-sm" required>
                                    <option value="" selected disabled>--- Belum
                                        dipilih ---</option>
                                    <option value="plan">Plan</option>
                                    <option value="do">Do</option>
                                    <option value="check">Check</option>
                                    <option value="action">Action</option>
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <label for="bsc" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                            <div class="col-sm-2">
                                <select style="font-size:11px;" name="bsc[]" id="bsc" class="form-control form-control-sm" required>
                                    <option value="" selected disabled>--- Belum
                                        dipilih ---</option>
                                    <option value="Perspektif BSC">Perspektif BSC</option>
                                    <option value="C">C</option>
                                    <option value="F">F</option>
                                    <option value="IP">IP</option>
                                    <option value="LG">LG</option>
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                            <!-- <div class="col-sm-1">
                                <button class="btn btn-sm btn-primary" style="font-size:10px;" type="button" id="addDescriptionInput">
                                    <i class="fa-solid fa-plus"></i>
                                </button>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-sm btn-primary" style="font-size:10px;" type="button" id="addDescriptionDet">
                            <i class="fa-solid fa-plus"></i>
                        </button>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:10px;">Simpan</button>
                    </td>
                    <td>
                        <a href="{{ route('list_dp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:10px;">Batal</a>
                    </td>
                </tr>
            </table>
        </div>
        <br>
    </form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
        $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $('#nama_posisi').change(function(){
            $('#kode_posisi').val($('#nama_posisi option:selected').data('kode_posisi'));
        })
    </script>
    <!-- <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script> -->
    <script>
        function autocomplete(inp, arr) {
            var currentFocus;
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) {
                    return false;
                }
                currentFocus = -1;
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);
                for (i = 0; i < arr.length; i++) {
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        b = document.createElement("DIV");
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        b.addEventListener("click", function(e) {
                            inp.value = this.getElementsByTagName("input")[0].value;
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                if (!x) return false;
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            document.addEventListener("click", function(e) {
                closeAllLists(e.target);
            });
        }
        // yepi 
        var nama = [];
        var datas = <?= json_encode($data); ?>;
        datas.forEach(el => {
            nama.push(el.name);
        });

        autocomplete(document.getElementById("nama_pengawas"), nama);
    </script>
        @include('js.wsdeskripsipekerjaan.tambah')
@endsection

    
