@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Posisi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<div class="container" style="width:95%;">
<form action="{{route('simpan_po')}}" method="post">{{ csrf_field() }}
    <hr>
    @foreach($wsposisi as $data)
    <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_posisi }}">

    <table>
        <tr>
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_po', $data->id_posisi)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_po', $data->id_posisi)}}">Hapus</a></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_po')}}">Batal</a></td>
            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <!-- <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
            </td> -->
        </tr>
    </table><br>
        <div class="row">
            <div class="col-md-4" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Nama Perusahaan</td>
                        <td vlign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                    </tr>
                    <tr>
                        <td>Kode Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_posisi" value="POS-{{now()->isoFormat('dmYYs')}}">{{$data->kode_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Nama Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                    </tr>                          
                    <tr>
                        <td>Kode MPP</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="kode_mpp" value="{{$data->kode_mpp}}">{{$data->kode_mpp}}</td>
                    </tr>
                    <tr>
                        <td>Tingkat Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tingkat_posisi" value="{{$data->tingkat_posisi}}">{{$data->tingkat_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Detail Tingkat Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="detail_tingkat_posisi" value="{{$data->detail_tingkat_posisi}}">{{$data->detail_tingkat_posisi}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Nama Jabatan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_jabatan" value="{{$data->nama_jabatan}}">{{$data->nama_jabatan}}</td>
                    </tr>
                    <tr>
                        <td>Nama Organisasi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_organisasi" value="{{$data->nama_organisasi}}">{{$data->nama_organisasi}}</td>
                    </tr>
                    <tr>
                        <td>Tingkat Organisasi </td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tingkat_organisasi" value="{{$data->tingkat_organisasi}}">{{$data->tingkat_organisasi}}</td>
                    </tr>                          
                    <tr>
                        <td>Tipe Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tipe_posisi" value="{{$data->tipe_posisi}}">{{$data->tipe_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Deskripsi Pekerjaan Posisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="deskripsi_pekerjaan_posisi" value="{{$data->deskripsi_pekerjaan_posisi}}">{{$data->deskripsi_pekerjaan_posisi}}</td>
                    </tr>
                    <tr>
                        <td>Nama Posisi Atasan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nama_posisi_atasan" value="{{$data->nama_posisi_atasan}}">{{$data->nama_posisi_atasan}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
        <b style="color:black;">Informasi Lainnya</b>
        <hr>
        <div class="row">
            <div class="col-md-3" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Dari Golongan&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data->dari_golongan}}</td>
                    </tr>
                    <tr>
                        <td>Sampai Golongan&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data->sampai_golongan}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-9" >
                <table style="font-size:12px;">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tipe Area</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tipe_area" value="{{$data->tipe_area}}">{{$data->tipe_area}}</td>
                    </tr>
                </table>
            </div>
        </div><br>
        <b style="color:black;">Flag Posisi</b>
        <hr>
        <div class="row">
            <div class="col-md-3" >
                <table style="font-size:12px;">
                    <tr>
                        <td>Posisi Aktif</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="checkbox" onclick="return false" name="posisi_aktif" value="{{$data->posisi_aktif}}" {{ $data->posisi_aktif == 'yes' ? 'checked' : NULL }}></td>
                    </tr>
                    <tr>
                        <td>Komisi</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="checkbox" onclick="return false" name="komisi" value="{{$data->komisi}}" {{ $data->komisi == 'yes' ? 'checked' : NULL }}></td>
                    </tr>
                    <tr>
                        <td>Kepala Fungsional</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="checkbox" onclick="return false" name="kepala_fungsional" value="{{$data->kepala_fungsional}}" {{ $data->kepala_fungsional == 'yes' ? 'checked' : NULL }}></td>
                    </tr>
                    <tr>
                        <td>Flag Operasional</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td>
                        <input type="checkbox" onclick="return false" name="flag_operasional" value="{{$data->flag_operasional}}" {{ $data->flag_operasional == 'yes' ? 'checked' : NULL }}>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">Status Posisi</td>
                        <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <div class="form-check col">
                                        <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios1" value="kosong" {{ $data->status_posisi == 'kosong' ? 'checked' : NULL }}>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Kosong
                                        </label><br>
                                        <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios2" value="terisi" {{ $data->status_posisi == 'terisi' ? 'checked' : NULL }}>
                                        <label class="form-check-label" for="exampleRadios2">
                                            Terisi
                                        </label><br>
                                        <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)" {{ $data->status_posisi == 'terisi(pjt)' ? 'checked' : NULL }}>
                                        <label class="form-check-label" for="exampleRadios3">
                                            Terisi (PJT)
                                        </label><br>
                                    </div>     
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-9">
                <table style="font-size:12px;">
                    <tr>
                        <td>Nomor Surat</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="nomor_surat" value="{{$data->nomor_surat}}">{{$data->nomor_surat}}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Karyawan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="jumlah_karyawan_dengan_posisi_ini" value="{{$data->jumlah_karyawan_dengan_posisi_ini}}">{{$data->jumlah_karyawan_dengan_posisi_ini}}</td>
                    </tr>
                </table>
            </div>
        </div><br>
        <b style="color:black;">Rekaman Informasi</b>
        <hr>
        <div class="row">
            <div class="col">
                <table style="font-size:12px;">
                    <tr>
                        <td>Tanggal Mulai Efektif</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_mulai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai Efektif</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="tanggal_selesai_efektif" value="{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                    </tr>
                </table>
                
            </div>
        </div>
    @endforeach
</form>
</div>
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_po') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .po").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection