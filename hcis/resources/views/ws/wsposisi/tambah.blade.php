@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Posisi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
                    <form action="{{route('simpan_po')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
                        <div class="form-group" style="width:95%; color:black;">
                            <div class="">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                            <div class="col-sm-10">
                                                <select required name="nama_perusahaan" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected >--- Pilih ---</option>
                                                    @foreach ($datap['looks'] as $p )
                                                    <option value="{{$p->nama_perusahaan}}">{{$p->nama_perusahaan}}</option>
                                                    @endforeach
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                            <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                                            <div class="col-sm-10">
                                                <!-- <select required name="kode_posisi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected >--- Pilih ---</option>
                                                    @foreach ($datapo['looks'] as $po )
                                                    <option value="{{$po->kode_posisi}}">{{$po->kode_posisi}}</option>
                                                    @endforeach
                                                </select> -->
                                                <input type="text"  readonly name="kode_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$random}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                            <div class="col-sm-10">
                                                <input type="text" required name="nama_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                <p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode MPP</label>
                                            <div class="col-sm-10">
                                                <input type="text" required name="kode_mpp" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                            <div class="col-sm-10">
                                                <select required name="tingkat_posisi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datatp['looks'] as $po )
                                                    <option value="{{$po->urutan_tingkat}}">{{$po->urutan_tingkat}}</option>
                                                    @endforeach
                                                </select>
                                                
                                                <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-lael col-form-label-sm" style="font-size:11px;">Detail Tingkat Posisi</label>
                                            <div class="col-sm-10">
                                                <select required name="detail_tingkat_posisi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected >--- Pilih ---</option>
                                                    @foreach ($datatp['looks'] as $dtp )
                                                    <option value="{{$dtp->nama_tingkat_posisi}}">{{$dtp->nama_tingkat_posisi}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input type="text" name="detail_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                                            <div class="col-sm-10">
                                                <select required name="nama_jabatan" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected >--- Pilih ---</option>
                                                    @foreach ($dataj['looks'] as $j )
                                                    <option value="{{$j->nama_jabatan}}">{{$j->nama_jabatan}}</option>
                                                    @endforeach
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                                <!-- <input type="text" required name="nama_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                            <div class="col-sm-10">
                                                <select required name="nama_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datao['looks'] as $o )
                                                    <option value="{{$o->nama_organisasi}}">{{$o->nama_organisasi}}</option>
                                                    @endforeach
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                            <!-- <input type="text" required name="nama_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                                            <div class="col-sm-10">
                                                <select required name="tingkat_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datato['looks'] as $ut)
                                                    <option value="{{$ut->urutan_tingkat}}">{{$ut->urutan_tingkat}}</option>
                                                    @endforeach
                                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                            <!-- <input type="text" required name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="tipe_posisi" id="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="none">None</option>
                                                    <option value="tunggal">Tunggal</option>
                                                    <option value="banyak">Banyak</option>
                                                </select>
                                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan Posisi</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" name="deskripsi_pekerjaan_posisi" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi Atasan</label>
                                            <div class="col-sm-10">
                                                <!-- <select required name="tingkat_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                    <option value="" selected >--- Pilih ---</option>
                                                    @foreach ($datapo['looks'] as $po )
                                                    <option value="{{$po->nama_posisi_atasan}}">{{$po->nama_posisi_atasan}}</option>
                                                    @endforeach
                                                </select> -->
                                                <input type="text" name="nama_posisi_atasan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                
                                <b style="color:black;">Informasi Lainnya</b><hr>
                                
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                            <div class="col-sm-10">
                                                <select required style="font-size:11px;" name="dari_golongan" class="form-control form-control-sm">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datag['looks'] as $p )
                                                    <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input type="text" name="dari_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                            <div class="col-sm-10">
                                                <select required style="font-size:11px;" name="sampai_golongan" class="form-control form-control-sm">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($datag['looks'] as $p )
                                                    <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input type="text" name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Area</label>
                                            <div class="col-sm-10">
                                                <select name="tipe_area" id="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="BO">BO</option>
                                                    <option value="HO">HO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    @foreach($datalk['looks'] as $lk)
                                                        <option value="{{$lk->nama_lokasi_kerja}}">{{$lk->nama_lokasi_kerja}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    @foreach($dataglk['looks'] as $glk)
                                                        <option value="{{$glk->nama_grup_lokasi_kerja}}">{{$glk->nama_grup_lokasi_kerja}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>  
                                            
                                    </div>
                                  
                                </div>  
                                <b style="color:black;">Flag Posisi</b>
                                <hr>
                                <div class="row">  
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Posisi Aktif</label>
                                            <div class="col-sm-5">
                                            <input type="checkbox" name="posisi_aktif" value="yes" style="font-size:11px;" id="colFormLabelSm">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Komisi</label>
                                            <div class="col-sm-5">
                                                <input type="checkbox" name="komisi" value="yes" style="font-size:11px;" id="colFormLabelSm">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kepala Fungsional</label>
                                            <div class="col-sm-5">
                                                <input type="checkbox" value="yes" name="kepala_fungsional" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Flag Operasional</label>
                                            <div class="col-sm-5">
                                            <input type="checkbox" value="yes" name="flag_operasional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Posisi</label>
                                            <div class="col-sm-5">

                                                <div class="form-check col" style="font-size:11px;">
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios1" value="kosong">
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        Kosong
                                                    </label><br>
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios2" value="terisi">
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Terisi
                                                    </label><br>
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)">
                                                    <label class="form-check-label" for="exampleRadios3">
                                                        Terisi (PJT)
                                                    </label><br>
                                                </div>     
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Surat</label>
                                            <div class="col-sm-10">
                                            <input type="text" name="nomor_surat" style="font-size:11px;" class="form-control form-control-sm" class="" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                                            <div class="col-sm-10">
                                            <input type="number" name="jumlah_karyawan_dengan_posisi_ini" class="form-control form-control-sm" id="colFormLabelSm" style="font-size:11px;" placeholder="">
                                            </div>
                                        </div>                                           
                                    </div> 
                                    </div><br>
                                    <b style="color:black;">Rekaman Informasi</b>
                                    <hr>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                <div class="col-sm-10">
                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                <div class="col-sm-10">
                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">keterangan</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                         
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <!-- <td>
                                    <a href="" class="btn">Hapus</a>
                                </td> -->
                                    <!-- <td>
                                    <a href="" class="btn">Hapus</a>
                                </td> -->
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_po') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>

                    </form>
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_po') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .po").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection