@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Kelas Cabang @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('simpan_kj')}}" method="post">{{ csrf_field() }}
    <hr>
    @foreach($wskelompokjabatan as $data)
    <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_kelompok_jabatan }}" />

    <table>
        <tr>
            
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_kj', $data->id_kelompok_jabatan)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_kj', $data->id_kelompok_jabatan)}}" id="btn_delete">Hapus</a></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_kj')}}">Batal</a></td>

            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <!-- <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
            </td> -->
        </tr>
    </table><br>
    <b style="color:black;">Informasi Kelompok Jabatan</b>
            <div class="row">
                <div class="col" >
                    <table style="font-size:12px;">
                        <tr>
                            <td>Kode Kelompok Jabatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="kode_kelompok_jabatan" value="KKJ-{{now()->isoFormat('dmYYs')}}">{{$data->kode_kelompok_jabatan}}</td>
                        </tr>
                        <tr>
                            <td>Nama Kelompok Jabatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="nama_kelompok_jabatan" value="{{$data->nama_kelompok_jabatan}}">{{$data->nama_kelompok_jabatan}}</td>
                        </tr>
                        <tr>
                            <td>Deskripsi Kelompok Jabatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="deskripsi_kelompok_jabatan" value="{{$data->deskripsi_kelompok_jabatan}}">{{$data->deskripsi_kelompok_jabatan}}</td>
                        </tr>
                        <tr>
                            <td>Dari Golongan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data->dari_golongan}}</td>
                        </tr>
                        <tr>
                            <td>Sampai Golongan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data->sampai_golongan}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col" style="font-size:12px;">
                    <table>
                        <tr>
                            <td>Grup Jabatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="grup_jabatan" value="{{$data->grup_jabatan}}">{{$data->grup_jabatan}}</td>
                        </tr>
                        <tr>
                            <td>Tipe Jabatan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tipe_jabatan" value="{{$data->tipe_jabatan}}">{{$data->tipe_jabatan}}</td>
                        </tr>
                        <tr>
                            <td>Deskripsi Pekerjaan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$data->deskripsi_pekerjaan}}">{{$data->deskripsi_pekerjaan}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr>
            <b style="color:black;">Rekaman Informasi</b>
            <div class="row">
                <div class="col" style="font-size:12px;">
                    <table>
                        <tr>
                            <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{date('d-m-Y', strtotime($data->tanggal_mulai_efektif))}}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai Efektif</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{date('d-m-Y', strtotime($data->tanggal_selesai_efektif))}}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                            <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                        </tr>
                    </table>
                </div>
            </div>
    <hr>       
    @endforeach
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

@endpush
        @section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_kj') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .kj").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection