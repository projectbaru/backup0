@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<form action="{{route('update_j')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group">
        <div class="">
            @foreach($wsjabatan as $data)
            <div class="row">
                <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                            <div class="col-sm-10">
                            <input type="hidden" name="id_jabatan" value="{{ $data->id_jabatan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                            </div>
                        </div>
            
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_jabatan" readonly required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_jabatan}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="nama_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_jabatan}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_kelompok_jabatan}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="nama_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_kelompok_jabatan}}" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="grup_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->grup_jabatan}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="deskripsi_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi_jabatan}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_mulai" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai" value="{{$data->tanggal_mulai}}" placeholder="Tanggal mulai"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" name="tanggal_mulai" required class="form-control form-control-sm " style="font-size:11px;" value="{{$data->tanggal_mulai}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai}}" id="tanggal_selesai" placeholder="Tanggal selesai" />

                        <!-- <input type="date" name="tanggal_selesai" class="form-control form-control-sm" value="{{$data->tanggal_selesai}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Magang</label>
                        <div class="col-sm-1">

                        <input type="checkbox" name="magang" class="form-control form-control-sm" style="font-size:5px;" id="colFormLabelSm" placeholder="" value="{{$data->magang}}" {{ $data->magang == 'ya' ? 'checked' : NULL }} >
                        </div>
                    </div>
                
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                        <div class="col-sm-10">
                            <select name="tipe_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="-" {{ $data->tipe_jabatan == '' ? 'selected' : NULL }}>None</option>
                                <option value="manajerial" {{ $data->tipe_jabatan == 'manajerial' ? 'selected' : NULL }}>Manajerial</option>
                                <option value="administratif" {{ $data->tipe_jabatan == 'administratif' ? 'selected' : NULL }}>Administratif</option>
                                <option value="analis" {{ $data->tipe_jabatan == 'analis' ? 'selected' : NULL }}>Analis</option>

                            </select>                                                            
                        </div>
                    </div>
                    
                </div> 
            </div>
            <hr>
            <b>Informasi Lainnya</b>
            <br>
            <br>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                        <div class="col-sm-10">
                            <select required style="font-size:11px;" name="dari_golongan" id="produk00" class="form-control form-control-sm" >
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($datadg['looks'] as $p )
                                    <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                        <div class="col-sm-10">
                            <select required style="font-size:11px;" name="sampai_golongan" id="produk00" class="form-control form-control-sm" >
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($datadg['looks'] as $v )
                                    <option value="{{$v->nama_golongan}}" {{ $v->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$v->nama_golongan}}</option>
                                @endforeach
                            </select>                                                            
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <b>Rekaman Informasi</b>
            <br><br>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" value="{{$data->tanggal_mulai_efektif}}" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm"  style="font-size:11px;" value="{{$data->tanggal_mulai_efektif}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}" placeholder="Tanggal selesai efektif"/>

                        <!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                        <div class="col-sm-10">
                        <textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->keterangan}}" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                        </div>
                    </div>
                    <hr>
                </div>                                          
            </div>  
            
            @endforeach
            <div>
                <table>
                    <tr>

                    <!-- <td>
                        <a href="" class="btn">Hapus</a>
                    </td> -->
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="{{route('hapus_j',$data->id_jabatan)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;"  id="btn_delete">Hapus</a>
                        </td>
                        <td>
                            <a href="{{ route('list_j') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
        @section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_j') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
            $("#mm-active").addClass("active");
        $(" .j").addClass("active");
        $("#a-homes").addClass("active");
        });
    </script>
@endsection
