@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
    <form action="{{route('simpan_j')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
        <hr>
        <div class="form-group">
            <div class="">
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="kode_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$random}}" id="colFormLabelSm" placeholder="" readonly><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="nama_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" name="tipe_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                            <div class="col-sm-10">
                                <select name="kode_kelompok_jabatan" id="produk00" required style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected>--- Belum Pilih ---</option>
                                    @foreach ($datakkj['looks'] as $p )
                                    <option value="{{$p->kode_kelompok_jabatan}}">{{$p->kode_kelompok_jabatan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                            <div class="col-sm-10">
                                <select name="nama_kelompok_jabatan" id="produk00" required style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected>--- Belum Pilih ---</option>
                                    @foreach ($datakkj['looks'] as $v )
                                    <option value="{{$v->nama_kelompok_jabatan}}">{{$v->nama_kelompok_jabatan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                <!-- <input type="text" name="nama_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Jabatan</label>
                            <div class="col-sm-10">
                                <select required name="grup_jabatan" id="produk00"  style="font-size:11px;" class="form-control form-control-sm">
                                    <option value="" selected>--- Belum Pilih ---</option>
                                    @foreach ($datakkj['looks'] as $a )
                                    <option value="{{$a->grup_jabatan}}">{{$a->grup_jabatan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                                <!-- <input type="text" name="grup_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Jabatan</label>
                            <div class="col-sm-10">
                                <input type="text" required name="deskripsi_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai" placeholder="Tanggal mulai"><p style="color:red;font-size:10px;">* wajib isi</p>

                                <!-- <input type="date" name="tanggal_mulai" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai" placeholder="Tanggal selesai" />

                            <!-- <input type="date" name="tanggal_selesai" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Magang</label>
                            <div class="col-sm-1">
                            <input type="checkbox" name="magang" class="form-control form-control-sm" style="font-size:5px;" id="colFormLabelSm" value="ya" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                            <div class="col-sm-10">
                                <select name="tipe_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                    <option value="-">--Belum Dipilih--</option>
                                    <option value="manajerial">Manajerial</option>
                                    <option value="administratif">Administratif</option>
                                    <option value="analis">Analis</option>
                                </select>
                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                            </div>
                        </div>
            
                    </div> 
                </div>
                <hr>
                <b style="color:black;">Informasi Lainnya</b>
                <br>
                <br>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                            <div class="col-sm-10">
                                <select required name="dari_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected>--- Pilih ---</option>
                                    @foreach ($datadg['looks'] as $p )
                                    <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                    @endforeach
                                </select>
                            <!-- <input type="text" name="dari_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                            <div class="col-sm-10">
                                <select required name="sampai_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected>--- Pilih ---</option>
                                    @foreach ($datadg['looks'] as $v )
                                    <option value="{{$v->nama_golongan}}">{{$v->nama_golongan}}</option>
                                    @endforeach
                                </select>
                            <!-- <input type="text" name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        
                    </div>
                
                </div>  
                <hr>
                <b style="color:black;">Rekaman Informasi</b>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                            <div class="col-sm-10">
                            <textarea type="text" name="keterangan" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                </div><hr>
            </div>
        </div>
        <div >
            <table>
                <tr>
                

                    <td>
                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                    </td>
                    <td>
                        <a href="{{ route('list_j') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                    </td>
                </tr>
            </table>
        </div>

    </form>
    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush