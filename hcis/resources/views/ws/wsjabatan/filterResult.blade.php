@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
table th{
     text-align:center;   
}

</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Jabatan</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamj')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>
                            <form action="{{ URL::to('/list_j/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                    <div style="overflow-x:auto">
                                    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <th style="">No</th>
                                                @for($i = 0; $i < count($th); $i++)
                                                    <!-- @if($th[$i] == 'id_jabatan')
                                                        <th></th>
                                                    @endif -->
                                                    @if($th[$i] == 'kode_jabatan')
                                                        <th style="">Kode Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'nama_jabatan')
                                                        <th style="">Nama Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'tipe_jabatan')
                                                        <th style="">Tipe Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'kode_kelompok_jabatan')
                                                        <th style="">Kode Kelompok Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'nama_kelompok_jabatan')
                                                        <th style="">Nama Kelompok Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'grup_jabatan')
                                                        <th style="">Grup Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'deskripsi_jabatan')
                                                        <th style="">Deskripsi Jabatan</th>
                                                    @endif
                                                    @if($th[$i] == 'tanggal_mulai')
                                                        <th style="">Tanggal Mulai</th>
                                                    @endif
                                                    @if($th[$i] == 'tanggal_selesai')
                                                        <th style="">Tanggal Selesai</th>
                                                    @endif
                                                    @if($th[$i] == 'dari_golongan')
                                                        <th style="">Dari Golongan</th>
                                                    @endif
                                                    @if($th[$i] == 'sampai_golongan')
                                                        <th style="">Sampai Golongan</th>
                                                    @endif
                                                    @if($th[$i] == 'magang')
                                                        <th style="">Magang</th>
                                                    @endif
                                                    @if($th[$i] == 'keterangan')
                                                        <th style="">Keterangan</th>
                                                    @endif
                                                    @if($th[$i] == 'status_rekaman')
                                                        <th style="">Status Rekaman</th>
                                                    @endif
                                                    @if($th[$i] == 'tanggal_mulai_efektif')
                                                        <th style="">Tanggal Mulai Efektif</th>
                                                    @endif
                                                    @if($th[$i] == 'tanggal_selesai_efektif')
                                                        <th style="">Tanggal Selesai Efektif</th>
                                                    @endif
                                                    @if($i == count($th) - 1)
                                                        <th style="">Aksi</th>
                                                        <th style=""><i class="fa-solid fa-check"></i></th>
                                                    @endif
                                                @endfor
                                            </thead>
                                            <tbody style="font-size:11px;">
                                                @php $b=1; @endphp
                                                @foreach($query as $row)
                                                    <tr>
                                                        <td>{{$b++;}}</td>
                                                        @for($i = 0; $i < count($th); $i++)
                                                            <!-- @if($th[$i] == 'id_jabatan')
                                                                <td>{{ $row->id_jabatan ?? 'NO DATA' }}</td>
                                                            @endif -->
                                                            @if($th[$i] == 'kode_jabatan')
                                                                <td>{{ $row->kode_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_jabatan')
                                                                <td>{{ $row->nama_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tipe_jabatan')
                                                                <td>{{ $row->tipe_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'kode_kelompok_jabatan')
                                                                <td>{{ $row->kode_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_kelompok_jabatan')
                                                                <td>{{ $row->nama_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'grup_jabatan')
                                                                <td>{{ $row->grup_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'deskripsi_jabatan')
                                                                <td>{{ $row->deskripsi_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai')
                                                                <td>{{ $row->tanggal_mulai ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai')
                                                                <td>{{ $row->tanggal_selesai ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'dari_golongan')
                                                                <td>{{ $row->dari_golongan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'sampai_golongan')
                                                                <td>{{ $row->sampai_golongan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'magang')
                                                                <td>{{ $row->magang ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                                <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'status_rekaman')
                                                                <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                                <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                                <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                            @endif
                                                            @if($i == count($th) - 1)
                                                                <td>                                                                                                        
                                                                    <a href="{{ URL::to('/list_j/detail/'.$row->id_jabatan) }}" class="btn btn-secondary btn-sm">Detail</a>
                                                                    <a href="{{ URL::to('/list_j/edit/'.$row->id_jabatan) }}" class="btn btn-success btn-sm">Edit</a>
                                                                </td>
                                                                <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_jabatan }}" id="multiDelete">

                                                                </td>
                                                            @endif
                                                        @endfor
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_j')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .j").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection


