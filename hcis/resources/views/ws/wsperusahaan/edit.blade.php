@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('update_p')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    @foreach($wsperusahaan as $data)
    
    <div class="form-group" style="width:95%;">
        <div class="">
            <div class="row" >
                <div class="col" style="font-size: 10px;">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-10">
                            <input type="hidden" name="id_perusahaan" name="temp_id" id="temp_id"  value="{{ $data->id_perusahaan }}" class="form-control form-control-sm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Logo Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="file" name="perusahaan_logo" value="{{ $data->perusahaan_logo }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><br>
                            <img src="{{url('/data_file/'.$data -> perusahaan_logo)}}"  width="100%" alt="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                        <div class="col-sm-10">
                            <input type="text" required name="nama_perusahaan" value="{{ $data->nama_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
                        <div class="col-sm-10">
                            <input type="text" readonly required name="kode_perusahaan" value="{{ $data->kode_perusahaan }}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Singkatan</label>
                        <div class="col-sm-10">
                            <input type="text" required name="singkatan" value="{{ $data->singkatan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Perusahaan</label>
                        <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai_perusahaan" style="font-size:11px;" value="{{ $data->tanggal_mulai_perusahaan }}"  class="form-control form-control-sm startDates" id="tanggal_mulai_perusahaan" placeholder="Tanggal mulai Perusahaan"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" required name="tanggal_mulai_perusahaan" value="{{ $data->tanggal_mulai_perusahaan }}"  style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_perusahaan" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_perusahaan" placeholder="Tanggal selesai perusahaan" value="{{ $data->tanggal_selesai_perusahaan }}" />

                        <!-- <input type="date" name="tanggal_selesai_perusahaan" value="{{ $data->tanggal_selesai_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="text" name="jenis_perusahaan" value="{{ $data->jenis_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Bisnis Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="text" name="jenis_bisnis_perusahaan" value="{{ $data->jenis_bisnis_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" style="font-size:11px;" name="jumlah_karyawan" value="{{ $data->jumlah_karyawan }}" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Visi Perusahaan</label>
                        <div class="col-sm-11">
                        <textarea type="text"  required name="visi_perusahaan" rows="8" cols="50" value="{{ $data->visi_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->visi_perusahaan }}</textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Misi Perusahaan</label>
                        <div class="col-sm-11">
                        <textarea type="text" required name="misi_perusahaan" rows="8" cols="50" value="{{ $data->misi_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->misi_perusahaan }}</textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Perusahaan</label>
                        <div class="col-sm-11">
                            <textarea type="text" required name="nilai_perusahaan" rows="8" cols="50" value="{{ $data->nilai_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->nilai_perusahaan }}</textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <b style="color:black;">Detail Pajak Perusahaan</b>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor NPWP Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="text" id="npwp" data-inputmask="'mask': '99.999.999.9-999.999'" required name="nomor_npwp_perusahaan" value="{{ $data->nomor_npwp_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Pajak</label>
                        <div class="col-sm-10">
                            <input type="text" required name="lokasi_pajak" value="{{ $data->lokasi_pajak }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>                
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">NPP</label>
                        <div class="col-sm-10">
                        <input type="text" name="npp" value="{{ $data->npp }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">NPKP</label>
                        <div class="col-sm-10">
                        <input type="text" name="npkp" value="{{ $data->npkp }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <b style="color:black;">ID</b>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Logo Perusahaan</label>
                        <div class="col-sm-10">
                        <input type="text" name="id_logo_perusahaan" value="{{ $data->id_logo_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>          
                </div>
            </div>
            <hr>
            <b style="color:black;">Rekaman Informasi</b>
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" value="{{ $data->tanggal_mulai_efektif }}" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" required name="tanggal_mulai_efektif"  style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" value="{{ $data->tanggal_selesai_efektif }}" />

                        <!-- <input type="date" name="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                        <div class="col-sm-10">
                        <textarea type="text" name="keterangan" rows="8" cols="50" value=""  class="form-control form-control-sm" id="colFormLabelSm" style="font-size:11px;" placeholder="">{{ $data->keterangan }}</textarea>
                        </div>
                    </div>        
                </div>
            </div><hr>

        </div>
    </div> @endforeach
    <div class="" style="">
        <table>
            <tr>

            <!-- <td>
                <a href="" class="btn">Hapus</a>
            </td> -->
                <td>
                    <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                </td>
                <td>
                    <a class="btn btn-danger btn-sm" href="{{route('hapus_p',$data->id_perusahaan)}}" class="btn" id="btn_delete"  style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                </td>
                <td>
                    <a href="{{ route('list_p') }}" class="btn btn-secondary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                </td>
                
            </tr>
        </table>
    </div>
</form>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
<script>
    CKEDITOR.replace('visi_perusahaan');
    CKEDITOR.replace('misi_perusahaan');
    CKEDITOR.replace('nilai_perusahaan');
  </script>
@endpush


@section('add-scripts')


<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(".p").addClass("active");
        $("#a-homes").addClass("active");
        
    });
</script>

@endsection