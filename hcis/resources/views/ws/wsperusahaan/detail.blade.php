@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush
@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<hr>
<div class="container" style="width:95%;">

<form action="{{route('duplicate_pd')}}" method="POST" enctype="multipart/form-data">{{ csrf_field() }}
    
    @php $b=1; @endphp
    @foreach($wsperusahaan as $data)
    <input type="hidden" name="temp_id" id="tg_id" value="{{ $data->id_perusahaan }}" />
    <table>
        <tr>
            <!-- <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="">Print</a></td> -->
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_p', $data->id_perusahaan)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_p', $data->id_perusahaan)}}" id="btn_delete">Hapus</a></td>
            <td><a class="btn btn-secondary btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_p')}}">Batal</a></td>

            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <!-- <td>
                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                    </td> -->
        </tr>
    </table><br>
    <b style="color:black;">Informasi Perusahaan</b><br><br>
    <div class="row">
        <div class="col-md-8">
            <table style="font-size:12px;">
                <tr>
                    <td valign="top">Logo Perusahaan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;<input type="hidden" name="perusahaan_logo" value="{{$data->perusahaan_logo}}"><img src="{{url('/data_file/'.$data -> perusahaan_logo)}}" name="perusahaan_logo" value="{{$data->perusahaan_logo}}" width="80%" alt=""></td>
                </tr>
                <tr>
                    <td>Nama Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                </tr>
                <tr>
                    <td>Kode Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="kode_perusahaan" value="{{$data->kode_perusahaan}}">{{$data->kode_perusahaan}}</td>
                </tr>
                <tr>
                    <td>Singkatan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="singkatan" value="{{$data->singkatan}}">{{$data->singkatan}}</td>
                </tr>
                <tr><br>
                    <td valign="top">Visi Perusahaan</td>
                    <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="visi_perusahaan" value="{{$data->visi_perusahaan}}">{!!$data->visi_perusahaan!!}</td>
                </tr>
                <tr>
                    <td valign="top">Misi Perusahaan</td>
                    <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="misi_perusahaan" value="{{$data->misi_perusahaan}}">{!!$data->misi_perusahaan!!}</td>
                </tr>
                <tr>
                    <td valign="top">Nilai Perusahaan</td>
                    <td valign="top">&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="nilai_perusahaan" value="{{$data->nilai_perusahaan}}">{!!$data->nilai_perusahaan!!}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4" style="font-size:12px;">
            <table style="font-size:12px;">
                
                <tr>
                    <td>Tanggal Mulai Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_mulai_perusahaan" value="{{$data->tanggal_mulai_perusahaan}}">{{ $data->tanggal_mulai_perusahaan ? date('d-m-Y', strtotime($data->tanggal_mulai_perusahaan)) : '-' }}</td>
                </tr>
                <tr>
                    <td>Tanggal Selesai Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_selesai_perusahaan" value="{{ $data->tanggal_selesai_perusahaan }}">{{ $data->tanggal_selesai_perusahaan ? date('d-m-Y', strtotime($data->tanggal_selesai_perusahaan)) : '-' }}</td>
                </tr>
                <tr>
                    <td>Jenis Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="jenis_perusahaan" value="{{$data->jenis_perusahaan}}">{{$data->jenis_perusahaan}}</td>
                </tr>
                <tr>
                    <td>Jenis Bisnis Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="jenis_bisnis_perusahaan" value="{{$data->jenis_bisnis_perusahaan}}">{{$data->jenis_bisnis_perusahaan}}</td>
                </tr>
                <tr>
                    <td>Jumlah Karyawan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="jumlah_karyawan" value="{{$data->jumlah_karyawan}}">{{$data->jumlah_karyawan}}</td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <b style="color:black;">Detail Pajak Perusahaan</b>
    <div class="row">
        <div class="col">
            <table style="font-size:12px;">
                <tr>
                    <td>Nomor NPWP Perusahaan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="nomor_npwp_perusahaan" value="{{$data->nomor_npwp_perusahaan}}">{{$data -> nomor_npwp_perusahaan}}</td>
                </tr>
                <tr>
                    <td>Lokasi Pajak</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="lokasi_pajak" value="{{$data->lokasi_pajak}}">{{$data -> lokasi_pajak}}</td>
                </tr>
            </table>
        </div>
        <div class="col" style="font-size:12px;">
            <table>
                <tr>
                    <td>NPP</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="npp" value="{{$data->npp}}">{{$data -> npp}}</td>
                </tr>
                <tr>
                    <td>NPKP</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="npkp" value="{{$data->npkp}}">{{$data -> npkp}}</td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <b style="color:black;">ID Logo</b>
    <div class="row">
        <div class="col">
            <table style="font-size:12px;">
                <tr>
                    <td>ID Logo Perusahaan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="id_logo_perusahaan" value="{{$data->id_logo_perusahaan}}">{{$data -> id_logo_perusahaan}}</td>
                </tr>
            </table>
        </div>

    </div>
    <hr>
    <b style="color:black;">Rekaman Informasi</b>
    <div class="row">
        <div class="col">
            <table style="font-size:12px;">
                <tr>
                    <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{ $data->tanggal_mulai_efektif }}">{{ $data->tanggal_mulai_efektif ? date('d-m-Y', strtotime($data->tanggal_mulai_efektif)) : '-' }}</td>
                </tr>
                <tr>
                    <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}">{{ $data->tanggal_selesai_efektif ? date('d-m-Y', strtotime($data->tanggal_selesai_efektif)) : '-' }}</td>
                </tr>
                <tr>
                    <td>Keterangan&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                </tr>
            </table><br>
        </div>
    </div>
    @endforeach
</form>
</div>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
@endpush

@section('add-scripts')


<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .p").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>

@endsection