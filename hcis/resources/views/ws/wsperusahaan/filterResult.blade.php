@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
table th{
     text-align:center;   
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent   

<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Perusahaan</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamp')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>

<form action="{{ URL::to('/list_p/hapus_banyak') }}" method="POST"  id="form_delete">
    @csrf
    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead style="color:black;">
            <th style="">No</th>
                @for($i = 0; $i < count($th); $i++)
                    <!-- @if($th[$i] == 'id_perusahaan')
                        <th></th>
                    @endif -->
                    @if($th[$i] == 'perusahaan_logo')
                        <th style="">Logo Perusahaan</th>
                    @endif
                    @if($th[$i] == 'nama_perusahaan')
                        <th style="width:150px;">Nama Perusahaan</th>
                    @endif
                    @if($th[$i] == 'kode_perusahaan')
                        <th style="">Kode Perusahaan</th>
                    @endif
                    @if($th[$i] == 'singkatan')
                        <th style="">Singkatan</th>
                    @endif
                    @if($th[$i] == 'visi_perusahaan')
                        <th style="">Visi Perusahaan</th>
                    @endif
                    @if($th[$i] == 'misi_perusahaan')
                        <th style="">Misi Perusahaan</th>
                    @endif
                    @if($th[$i] == 'nilai_perusahaan')
                        <th style="">Nilai Perusahaan</th>
                    @endif
                    <!-- @if($th[$i] == 'keterangan_perusahaan')
                        <th style="">Keterangan Perusahaan</th>
                    @endif -->
                    @if($th[$i] == 'tanggal_mulai_perusahaan')
                        <th style="">Tanggal Mulai Perusahaan</th>
                    @endif
                    @if($th[$i] == 'tanggal_selesai_perusahaan')
                        <th style="">Tanggal Selesai Perusahaan</th>
                    @endif
                    @if($th[$i] == 'jenis_perusahaan')
                        <th style="">Jenis Perusahaan</th>
                    @endif
                    @if($th[$i] == 'jenis_bisnis_perusahaan')
                        <th style="">Jenis Bisnis Perusahaan</th>
                    @endif
                    @if($th[$i] == 'jumlah_perusahaan')
                        <th style="">Jumlah Perusahaan</th>
                    @endif
                    @if($th[$i] == 'nomor_npwp_perusahaan')
                        <th style="">Nomor NPWP Perusahaan</th>
                    @endif
                    @if($th[$i] == 'keterangan_perusahaan')
                        <th style="">Keterangan Perusahaan</th>
                    @endif
                    @if($th[$i] == 'lokasi_pajak')
                        <th style="">Lokasi Pajak</th>
                    @endif
                    @if($th[$i] == 'npp')
                        <th style="">NPP</th>
                    @endif
                    @if($th[$i] == 'npkp')
                        <th style="">NPKP</th>
                    @endif
                    @if($th[$i] == 'id_logo_perusahaan')
                        <th style="">ID Logo Perusahaan</th>
                    @endif
                    @if($th[$i] == 'keterangan')
                        <th style="">Keterangan</th>
                    @endif
                    @if($th[$i] == 'status_rekaman')
                        <th style="">Status Rekaman</th>
                    @endif
                    @if($th[$i] == 'tanggal_mulai_efektif')
                        <th style="">Tanggal Mulai Efektif</th>
                    @endif
                    @if($th[$i] == 'tanggal_selesai_efektif')
                        <th style="">Tanggal Selesai Efektif</th>
                    @endif
                    @if($th[$i] == 'jumlah_karyawan')
                        <th style="">Jumlah Karyawan</th>
                    @endif
                    @if($i == count($th) - 1)
                        <th style="">Aksi</th>
                        <th style=""><i class="fa-solid fa-check"></i></th>

                    @endif
                @endfor
            </thead>
            <tbody style="font-size:11px;">
                @php $b=1 @endphp @foreach($query as $row)
                    <tr>
                        <td>{{$b++}}</td>
                        @for($i = 0; $i < count($th); $i++)
                            <!-- @if($th[$i] == 'id_perusahaan')
                                <td>{{ $row->id_perusahaan ?? 'NO DATA' }}</td>
                            @endif -->
                            @if($th[$i] == 'perusahaan_logo')
                                <td><img src="{{url('/data_file/'.$row -> perusahaan_logo)}}" width="100%" alt=""></td>
                            @endif
                            @if($th[$i] == 'nama_perusahaan')
                                <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kode_perusahaan')
                                <td>{{ $row->kode_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'singkatan')
                                <td>{{ $row->singkatan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'visi_perusahaan')
                                <td>{!! $row->visi_perusahaan ?? 'NO DATA' !!}</td>
                            @endif
                            @if($th[$i] == 'misi_perusahaan')
                                <td>{!! $row->misi_perusahaan ?? 'NO DATA' !!}</td>
                            @endif
                            @if($th[$i] == 'nilai_perusahaan')
                                <td>{!! $row->nilai_perusahaan ?? 'NO DATA' !!}</td>
                            @endif
                            @if($th[$i] == 'keterangan_perusahaan')
                                <td>{{ $row->keterangan_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_mulai_perusahaan')
                                <td>{{ $row->tanggal_mulai_perusahaan ? date('d-m-Y', strtotime($row->tanggal_mulai_perusahaan)) : 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_selesai_perusahaan')
                                <td>{{ $row->tanggal_selesai_perusahaan ? date('d-m-Y', strtotime($row->tanggal_selesai_perusahaan)) : 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'jenis_perusahaan')
                                <td>{{ $row->jenis_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'jenis_bisnis_perusahaan')
                                <td>{{ $row->jenis_bisnis_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'jumlah_perusahaan')
                                <td>{{ $row->jumlah_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'nomor_npwp_perusahaan')
                                <td>{{ $row->nomor_npwp_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'lokasi_pajak')
                                <td>{{ $row->lokasi_pajak ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'npp')
                                <td>{{ $row->npp ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'npkp')
                                <td>{{ $row->npkp ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'id_logo_perusahaan')
                                <td>{{ $row->id_logo_perusahaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'keterangan')
                                <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'status_rekaman')
                                <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_mulai_efektif')
                                <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_selesai_efektif')
                                <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                            @endif
                            @if($th[$i] == 'jumlah_karyawan')
                                <td>{{ $row->jumlah_karyawan.' karyawan' ?? 'NO DATA' }}</td>
                            @endif
                            @if($i == count($th) - 1)
                                <td> 
                                    <a href="{{ URL::to('/list_p/detail/'.$row->id_perusahaan) }}" class="btn btn-warning btn-sm">Detail</a>
                                    <a href="{{ URL::to('/list_p/edit/'.$row->id_perusahaan) }}" class="btn btn-success btn-sm">Edit</a>
                                </td>
                                <td>
                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_perusahaan }}"  id="multiDelete">                                  

                                </td>
                            @endif
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
        <table>
            <tr>
                <td><a class="btn btn-success btn-sm" href="{{route('tambah_p')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                </td>
                <!-- <td>
                    <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Print</button>
                </td> -->
            </tr>
    </table>
</form>
                               
                                        @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
        });
    });
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection