@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css'>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
					
<form action="{{route('simpan_p')}}" method="post" enctype="multipart/form-data">
	<hr>
	{{ csrf_field() }}
	<div class="form-group " style="width:95%;">
		<div class="">
			<div class="row">
				<div class="col" style="font-size: 10px;">
					
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Logo Perusahaan</label>
						<div class="col-sm-10">
							<input type="file" name="logo_perusahaan" style="font-size:11px;" id="foto" placeholder="">
						</div>
					</div>
					<div class="form-group row" >
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>												
						<div class="col-sm-10">
						<input type="text" style="font-size:11px;" name="nama_perusahaan" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="" required><p style="color:red;font-size:10px;">* wajib isi</p>
							
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="kode_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required  value="{{$randomId}}" readonly><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Singkatan</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="singkatan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jumlah Karyawan</label>
						<div class="col-sm-10">
							<input type="number" style="font-size:11px;" name="jumlah_karyawan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
				</div>
				
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Perusahaan</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_mulai_perusahaan" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_perusahaan" placeholder="Tanggal mulai perusahaan"><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Perusahaan</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_selesai_perusahaan" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_perusahaan" placeholder="Tanggal selesai perusahaan"/></div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Perusahaan</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="jenis_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Bisnis Perusahaan</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="jenis_bisnis_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Visi Perusahaan</label>
						<div class="col-sm-11">
						<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="visi_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Misi Perusahaan</label>
						<div class="col-sm-11">
							<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="misi_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nilai Perusahaan</label>
						<div class="col-sm-11">
							<textarea type="textarea" style="font-size:11px;"  rows="8" cols="50" name="nilai_perusahaan" required class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<b style="color:black;">Detail Pajak Perusahaan</b>
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nomor NPWP Perusahaan</label>
						<div class="col-sm-10">
							<input id="npwp" name="nomor_npwp_perusahaan" data-inputmask="'mask': '99.999.999.9-999.999'" style="font-size:11px;" placeholder="" required class="form-control form-control-sm" /><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Pajak</label>
						<div class="col-sm-10">
							<input type="text" required style="font-size:11px;" name="lokasi_pajak" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">NPP</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="npp" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">NPKP</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="npkp" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
				</div>
			</div>
			<hr>
			<b style="color:black;">ID</b>
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">ID Logo Perusahaan</label>
						<div class="col-sm-10">
							<input type="text" style="font-size:11px;" name="id_logo_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
					</div>
				</div>
			</div>
			<hr>
			<b style="color:black;">Rekaman Informasi</b>
			<div class="row">
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
						<div class="col-sm-10">
							<input type="date" data-date-format="mm-dd-yyyy" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
						<div class="col-sm-10">
							<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
						</div>
					</div>
				</div>
			</div>
			<hr>
		</div>
	</div>
	<div style="width: 90%;">
		<table>
		<tr>
			<!-- <td>
			<a href="" class="btn">Hapus</a>
		</td> -->
			<!-- <td>
			<a href="" class="btn">Hapus</a>
		</td> -->
			
			<td>
				<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
				<a href="{{ route('list_p') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
		</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
<script>
    CKEDITOR.replace('visi_perusahaan');
	CKEDITOR.replace('misi_perusahaan');
    CKEDITOR.replace('nilai_perusahaan');
  </script>

<script type="">
$(document).ready(function() {
		$('#tanggal_mulai_perusahaan').datepicker({
			format:"dd-mm-yyyy",
			autoclose: true
		}

		);
	});
</script>
@endpush


@section('add-scripts')
<script>
	$(document).ready(function(){
   $("#foto").change(function(){
     fileobj = document.getElementById('foto').files[0];
     var fname = fileobj.name;
     var ext = fname.split(".").pop().toLowerCase();
     if(ext == "jpeg" || ext == "png" || ext == "jpg"){
        $("#info_img_file").html(fname);
     }else{
        alert("Hanya untuk file jpg, jpeg dan png saja..");
        $("#foto").val("");
        $("#info_img_file").html("Tidak ada file yag dimasukkan");
        return false;
     }
   });
//    $("#audio_file").change(function(){
//      fileobj = document.getElementById('audio_file').files[0];
//      var fname = fileobj.name;
//      var ext = fname.split(".").pop().toLowerCase();
//      if(ext == "mp3" || ext == "mp4" || ext == "wav"){
//         $("#info_audio_file").html(fname);
//      }else{
//         alert("Accepted file mp3, mp4 and wav only..");
//         $("#aud_file").val("");
//         $("#info_audio_file").html("No file selected");
//         return false;
//      }
//    });
  
});
</script>
<script>
    $(document).ready(function() {
        $("#mm-active").addClass("active");
        $(" .p").addClass("active");
        $("#a-homes").addClass("active");
    });


</script>
@endsection

