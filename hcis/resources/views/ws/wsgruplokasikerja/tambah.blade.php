@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Grup Lokasi Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent
                                        <form action="{{route('simpan_glk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            <hr>
                                            <div class="form-group">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required value="{{$random}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Grup Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tipe_grup_lokasi_kerja" id="tipe_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" >
                                                                        <option value="-">--Belum Dipilih--</option>
                                                                        <option value="lokasi kerja">Lokasi Kerja</option>
                                                                        <option value="regional">Regional</option>
                                                                    </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                    <select name="lokasi_kerja" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($data['looks'] as $p )
                                                                        <option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" name="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" rows="4" cols="50" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"> -->

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

                                                                <!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->

                                                                <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                                </div>
                                                            </div>
                                                        </div>                                      
                                                    </div> 
                                                    <hr>                                                                       
                                                </div>
                                            </div>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_glk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
            
                                        </form>
                                        @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .glk").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection
