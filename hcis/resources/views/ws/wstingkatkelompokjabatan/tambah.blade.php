@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Kelompok Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent     
    <form action="{{route('simpan_tkj')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
        <hr>
        <div class="form-group">
            <div class="">
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Kelompok Jabatan</label>
                            <div class="col-sm-10">
                            
                                <input type="text"  name="kode_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$random}}" readonly required><p style="color:red;font-size:10px;">* wajib isi</p>             
                            
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Kelompok Jabatan</label>
                            <div class="col-sm-10">
                            <input type="text" required name="nama_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b style="color:black;">Kelompok jabatan</b>
                <br>
                <br>
                <div class="row">
                    <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Kelompok Jabatan</label>
                        <div class="col-sm-10">
                            <select name="kode_kelompok_jabatan" id="kode_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                <option value="" selected disabled>--- Pilih ---</option>
                                @foreach ($datas['looks'] as $k )
                                <option value="{{$k->kode_kelompok_jabatan}}" data-name ="{{$k->nama_kelompok_jabatan}}">{{$k->kode_kelompok_jabatan}}</option>
                                @endforeach
                            </select><p style="color:red;font-size:10px;">* wajib isi</p>		
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Kelompok Jabatan</label>
                        <div class="col-sm-10">
                        {{-- <input type="text" required name="nama_kelompok_jabatan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                        <select name="nama_kelompok_jabatan" id="nama_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                            <option value="" selected disabled>--- Pilih ---</option>
                            @foreach ($datas['looks'] as $p )
                            <option value="{{$p->nama_kelompok_jabatan}}" data-kode ="{{$p->kode_kelompok_jabatan}}">{{$p->nama_kelompok_jabatan}}</option>
                            @endforeach
                        </select><p style="color:red;font-size:10px;">* wajib isi</p>	
                        </div>
                    </div>


                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                            <div class="col-sm-10">
                                <select required style="font-size:11px;" name="kode_kelompok_jabatan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datas['looks'] as $p )
                                    <option value="{{$p->kode_kelompok_jabatan}}">{{$p->kode_kelompok_jabatan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                            <div class="col-sm-10">
                                <select required name="nama_kelompok_jabatan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datakj['looks'] as $p )
                                    <option value="{{$p->nama_kelompok_jabatan}}">{{$p->nama_kelompok_jabatan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" required name="nama_kelompok_jabatan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                            <div class="col-sm-10">
                            <!-- <input type="text" required name="dari_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                <select required name="dari_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datang['looks'] as $p )
                                    <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                            <div class="col-sm-10">
                                <select required name="sampai_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($datang['looks'] as $p )
                                    <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                    @endforeach
                                </select><p style="color:red;font-size:10px;">* wajib isi</p>
                            <!-- <input type="text" required name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                    </div>
                        
                    </div>
                    
                </div>  
                <hr>
                <b style="color:black;">Informasi Lainnya</b>
                <div class="row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_mulai" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai" placeholder="Tanggal mulai"><p style="color:red;font-size:10px;">* wajib isi</p>

                            <!-- <input type="date" required name="tanggal_mulai" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                            <div class="col-sm-10">
                            <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai" placeholder="Tanggal selesai"/>

                            <!-- <input type="date" name="tanggal_selesai" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                            <div class="col-sm-10">
                            <textarea type="text" name="keterangan"  id="colFormLabelSm" class="form-control form-control-sm" rows="4" cols="50"  style="font-size:11px;" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                    </td>
                    <td>
                        <a href="{{ route('list_tkj') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                    </td>
                    
                </tr>
            </table>
            <br>
        </div>

    </form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

<script>
        $('#kode_kelompok_jabatan').change(function(){
            $('#nama_kelompok_jabatan').val($('#kode_kelompok_jabatan option:selected').data('name'));
        })
        $('#nama_kelompok_jabatan').change(function(){
            $('#kode_kelompok_jabatan').val($('#nama_kelompok_jabatan option:selected').data('kode'));
        })
    </script>


@endpush

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .tkj").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection
    

