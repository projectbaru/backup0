@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Kelompok Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent     
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Tingkat Kelompok Jabatan</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamtkj')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>
                            <form action="{{ URL::to('/list_tkj/hapus_banyak') }}" method="POST"  id="form_delete">
                                @csrf
                                    <table id="example" class="table-responsive-xl table-bordered table-striped table-hover table-xl" style="width:100%;font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead>
                                            <th style="">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_tingkat_kelompok_jabatan')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'kode_tingkat_kelompok_jabatan')
                                                    <th style="">Kode Tingkat Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'nama_tingkat_kelompok_jabatan')
                                                    <th style="">Nama Tingkat kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'kode_kelompok_jabatan')
                                                    <th style="">Kode Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'nama_kelompok_jabatan')
                                                    <th style="">Nama Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'dari_golongan')
                                                    <th style="">Dari Golongan</th>
                                                @endif
                                                @if($th[$i] == 'sampai_golongan')
                                                    <th style="">Sampai Golongan</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai')
                                                    <th style="">Tanggal Mulai</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai')
                                                    <th style="">Tanggal Selesai</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="">Status Rekaman</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th style="">Aksi</th>
                                                    <th style="">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->id_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->kode_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->nama_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_kelompok_jabatan')
                                                            <td>{{ $row->kode_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_kelompok_jabatan')
                                                            <td>{{ $row->nama_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'dari_golongan')
                                                            <td>{{ $row->dari_golongan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'sampai_golongan')
                                                            <td>{{ $row->sampai_golongan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai')
                                                            <td>{{ $row->tanggal_mulai ? date('d-m-Y', strtotime($row->tanggal_mulai)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai')
                                                            <td>{{ $row->tanggal_selesai ? date('d-m-Y', strtotime($row->tanggal_selesai)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                        <td> 
                                                            <a href="{{ URL::to('/list_tkj/detail/'.$row->id_tingkat_kelompok_jabatan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;                                        
                                                            <a href="{{ URL::to('/list_tkj/edit/'.$row->id_tingkat_kelompok_jabatan) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                            <input type="checkbox" name="multiDelete[]" value="{{ $row->id_tingkat_kelompok_jabatan }}" id="multiDelete">
                                                        </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_tkj')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>



@endpush
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .tkj").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection


