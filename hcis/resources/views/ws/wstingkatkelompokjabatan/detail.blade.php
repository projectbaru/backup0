@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Kelompok Jabatan @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<div class="container" style="width:95%;">
<form action="{{route('simpan_tkj')}}" method="post">
    <hr>
    {{ csrf_field() }}
    @foreach($wstingkatkelompokjabatan as $data)
    <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_tingkat_kelompok_jabatan }}">

    <table>
        <tr>
            
            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_tkj', $data->id_tingkat_kelompok_jabatan)}}">Ubah</a></td>
            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_tkj', $data->id_tingkat_kelompok_jabatan)}}">Hapus</a></td>
            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_tkj')}}">Batal</a></td>

            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
            <!-- <td>
                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
            </td> -->
        </tr>
    </table><br>
    
    <div class="row">
        <div class="col" >
            <table style="font-size:12px;">
                <tr>
                    <td>Kode Tingkat Kelompok Jabatan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="kode_tingkat_kelompok_jabatan" value="TKJ-{{now()->isoFormat('dmYYs')}}">{{$data->kode_tingkat_kelompok_jabatan}}</td>
                </tr>
                <tr>
                    <td>Nama Tingkat Kelompok Jabatan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="nama_tingkat_kelompok_jabatan" value="{{$data->nama_tingkat_kelompok_jabatan}}">{{$data->nama_tingkat_kelompok_jabatan}}</td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <b style="color:black;">Kelompok Jabatan</b>
    <div class="row">
        <div class="col" >
            <table style="font-size:12px;">
                <tr>
                    <td>Kode Tingkat Kelompok Jabatan&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="kode_kelompok_jabatan" value="{{$data->kode_kelompok_jabatan}}">{{$data->kode_kelompok_jabatan}}</td>
                </tr>
                <tr>
                    <td>Nama Kelompok Jabatan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="nama_kelompok_jabatan" value="{{$data->nama_kelompok_jabatan}}">{{$data->nama_kelompok_jabatan}}</td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table style="font-size:12px;">
                <tr>
                    <td>Dari Golongan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data->dari_golongan}}</td>
                </tr>
                <tr>
                    <td>Sampai Golongan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data->sampai_golongan}}</td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <b style="color:black;">Rekaman Informasi</b>
    <div class="row">
        <div class="col" >
            <table style="font-size:12px;">
                <tr>
                    <td>Tanggal Mulai Efektif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_mulai" value="{{$data->tanggal_mulai}}">{{date('d-m-Y', strtotime($data->tanggal_mulai))}}</td>
                </tr>
                <tr>
                    <td>Tanggal Selesai Efektif&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="tanggal_selesai" value="{{$data->tanggal_selesai}}">{{date('d-m-Y', strtotime($data->tanggal_selesai))}}</td>
                </tr>
                <tr>
                    <td>Keterangan&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                </tr>
            </table>
        </div>                     
    </div>
    @endforeach
</form>
</div>     
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

<script>
        $('#kode_kelompok_jabatan').change(function(){
            $('#nama_kelompok_jabatan').val($('#kode_kelompok_jabatan option:selected').data('name'));
        })
        $('#nama_kelompok_jabatan').change(function(){
            $('#kode_kelompok_jabatan').val($('#nama_kelompok_jabatan option:selected').data('kode'));
        })
    </script>


@endpush
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_tkj') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection