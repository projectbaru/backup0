@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent  
<form action="{{route('simpan_to')}}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<hr>
	<div class="form-group" style="width:95%;">
		<div class="">
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<!-- <div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Tingkat Organisasi</label>
						<div class="col-sm-10">
							<input type="text" required name="tingkat_organisasi"  value="KTO-{{now()->isoFormat('dmYYs')}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="" class="form-control form-control-sm"></div>
					</div> -->
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Tingkat Organisasi</label>
						<div class="col-sm-10">
							<input type="text" required name="kode_tingkat_organisasi"  value="{{$random}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="" class="form-control form-control-sm"><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row" >
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Organisasi</label>
						<div class="col-sm-10">
							<input type="text" required style="font-size:11px;" name="nama_tingkat_organisasi" class="form-control form-control-sm" require id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Urutan</label>
						<div class="col-sm-10">
							<input type="number" required style="font-size:11px;" name="urutan_tingkat" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
						<div class="col-sm-10">
							<input type="number" required style="font-size:11px;" name="urutan_tampilan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					
				</div>
				<div class="col">
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
						<div class="col-sm-10">
							<textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/><p style="color:red;font-size:10px;">* wajib isi</p>
						</div>
					</div>
				</div>
			</div>
			<hr>
		
		</div>
	</div>
	<div class="" style="">
		<table>
		<tr>

			
			<td>
				<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
				<a href="{{ route('list_to') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
		</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .to").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection