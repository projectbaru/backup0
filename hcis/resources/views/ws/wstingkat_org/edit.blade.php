@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Tingkat Organisasi @endslot
@slot('title') Workstrukture @endslot
@endcomponent   
<form action="{{route('update_to')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
    <hr>
    <div class="form-group" style="width:95%;">
        <div class="">
            @foreach($wstingkatorganisasi as $data)
            <div class="row">
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-10">
                        <input type="hidden" name="id_tingkat_organisasi" value="{{ $data->id_tingkat_organisasi }}" name="temp_id" id="temp_id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Organisasi</label>
                        <div class="col-sm-10">
                        <input type="text" name="kode_tingkat_organisasi" readonly class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_tingkat_organisasi}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Organisasi</label>
                        <div class="col-sm-10">
                        <input type="text" name="nama_tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_tingkat_organisasi}}"><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Urutan</label>
                        <div class="col-sm-10">
                        <input type="number" name="urutan_tingkat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tingkat}}" id="colFormLabelSm" placeholder=""><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                        <div class="col-sm-10">
                        <input type="number" name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tampilan}}" id="colFormLabelSm" placeholder=""></textarea><p style="color:red;font-size:10px;">* wajib isi</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                        <div class="col-sm-10">
                        <textarea type="text" rows="4" cols="50" name="keterangan" value="{{ $data->keterangan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" style="font-size:11px;">{{ $data->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif"><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tanggal_mulai_efektif}}"> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai  Efektif</label>
                        <div class="col-sm-10">
                        <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" value="{{ $data->tanggal_selesai_efektif }}" /><p style="color:red;font-size:10px;">* wajib isi</p>

                        <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tanggal_selesai_efektif}}"> -->
                        </div>
                    </div>
                    
                </div>
            </div><hr>
            @endforeach
            <div class="" style="">
                <table>
                    <tr>

                    <!-- <td>
                        <a href="" class="btn">Hapus</a>
                    </td> -->
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="{{route('hapus_to',$data->id_tingkat_organisasi)}}" class="btn"  id="btn_delete" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                        </td>
                        <td>
                            <a href="{{ route('list_to') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


@endpush

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_to') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
        $("#mm-active").addClass("active");
        $(" .to").addClass("active");
        $("#a-homes").addClass("active");
    });
</script>
@endsection