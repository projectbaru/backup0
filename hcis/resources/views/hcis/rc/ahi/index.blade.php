
@extends('re.cv.side')
@section('content')

<div class="pcoded-wrapper">
    <!-- Link to open the modal -->
    <div style="">
        <div id="ex1" class="modal" style="margin-top:100px;overflow:auto;">
        <div>
        <div class="pcoded-wrapper" >
        <div class="pcoded-content" >
            <div class="pcoded-inner-content">
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-12" style="margin-top:10px;">
                                        <div class="page-header-title" >
                                            <h5 class="m-b-10">Tambah Data CV Pelamar</h5>
                                        </div>
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                            <li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
                                            <li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>  
                            <form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
                                <hr>
                                {{ csrf_field() }}
                                <div class="form-group" style="width:95%;">
                                    <div class="">
                                        <!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                                            <p>Tambah Data CV Pelamar</p>
                                        </div> -->
                                        <b>Melamar Lowongan</b>
                                        <div class="row">
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                                                    <!-- <div class="col-sm-9">
                                                    <input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div> -->
                                                    <div class="col-sm-9">
                                                        <input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
                                                    <div class="col-sm-9">
                                                        <!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
                                                        <select name="tingkat_pekerjaan" id="tingkat_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="" selected disabled>--Tingkat--</option>
                                                            <option value="staff" >Staff</option>
                                                            <option value="middle" >Middle</option>
                                                            <option value="senior">Senior</option>
                                                        </select>	
                                                    </div>
                                            
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                    <div class="col-sm-9">
                                                        <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                            @foreach ($datalk['looks'] as $np )
                                                            <option value="{{$np->nama_lokasi_kerja}}">{{$np->nama_lokasi_kerja}}</option>
                                                            @endforeach
                                                        </select>	
                                                    
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                            <hr>
                                            <b>Personal Information</b>
                                            <div class="row">
                                                <div class="col" style="font-size: 10px;">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
                                                        <div class="col-sm-9">
                                                        <input type="file" download name="resume_cv" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
                                                        <div class="col-sm-9">
                                                        <input type="nama_lengkap" required name="nama_lengkap" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                                                        <div class="col-sm-9">
                                                        <input type="email" name="email" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
                                                        <div class="col-sm-9">
                                                        <input type="number" required name="no_hp" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
                                                        <div class="col-sm-9">
                                                        <input type="text" required name="lokasi_pelamar" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
                                                        <div class="col-sm-9">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="pengalaman_kerja"  value="Berpengalaman" >
                                                                <label class="form-check-label" for="flexRadioDefault1">
                                                                    Memiliki Pengalaman
                                                                </label>
                                                            </div>&nbsp;&nbsp;&nbsp;
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="pengalaman_kerja" value="Lulusan Baru">
                                                                <label class="form-check-label" for="flexRadioDefault1">
                                                                    Lulusan Baru
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
                                                        <div class="col-sm-4">
                                                        <input type="text" required name="tahun_pengalaman_kerja" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >Tahun
                                                        </div>
                                                        <div class="col-sm-5">
                                                        <input type="text" required name="bulan_pengalaman_kerja" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >Bulan
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
                                                        <div class="col-sm-9">
                                                            <input type="date" required name="tanggal_lahir" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
                                                        <div class="col-sm-9">
                                                            <input type="date" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
                                                        <!-- <div class="col-sm-4">
                                                            <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
                                                        </div> -->
                                                        <div class="col-sm-9">
                                                            <div class="form-check">
                                                                <input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1">
                                                                <label class="form-check-label" for="flexRadioDefault1">
                                                                    Laki-laki
                                                                </label>
                                                            </div>&nbsp;&nbsp;&nbsp;
                                                            <div class="form-check">
                                                                <input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1">
                                                                <label class="form-check-label" for="flexRadioDefault1">
                                                                    Perempuan
                                                                </label>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        <hr>
                                        <b>Pendidikan</b>
                                        <br>
                                        
                                        <div class="row">
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-labelgit -sm" style="font-size:11px;">Pendidikan Tertinggi</label>
                                                    <div class="col-sm-9">
                                                        <select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="git form-control form-control-sm" style="font-size:11px;">
                                                            <!-- <option value="" selected disabled>--Tingkat--</option> -->
                                                            <option value="SMK/Sederajat">SMK/Sederajat</option>
                                                            <option value="D1">D1</option>
                                                            <option value="D2">D2</option>
                                                            <option value="D3">D3</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>

                                                        </select>	
                                                        <!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="nama_sekolah" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" required name="program_studi_jurusan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
                                                    <div class="col-sm-9">
                                                        <input type="date" required name="tanggal_wisuda" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
                                                    <div class="col-sm-4">
                                                        <input type="input" required name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                                    </div>Skala*
                                                    <div class="col-sm-4">
                                                    <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
                                                        <input type="text" required name="nilai_skala" style="font-size:11px;" id="nilai_skala" class="form-control form-control-sm" placeholder="">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <hr>
                                        <b style="font-size:11px;">Informasi Tambahan</b>
                                        <hr>
                                        <div class="row">
                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
                                            <div class="col-sm-4">
                                                <select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="Web SRU">Web SRU</option>
                                                    <option value="Job Street" >Job Street</option>
                                                    <option value="Karyawan SRU" >Karyawan SRU</option>
                                                    <option value="Lainnya" >Lainnya</option>
                                                </select>	
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" required name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-12 form-floating">
                                                <textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div style="width: 100%;">
                                    <table>
                                    <tr>
                                        <!-- <td>
                                            <input type="text">
                                        </td> -->
                                        <td>
                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                        </td>
                                        <td>
                                            <a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                        </td>
                                    </tr>
                                    </table>
                                </div>
                                <br>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <a href="#" rel="modal:close">Close</a>
    </div>
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <br>
                    <br>
                    <div class="row ">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-body  table-border-style">
                                    <div class="table-responsive-xl " style="">
                                        <form action="{{ route('filter_dua_dcp')}}" method="POST">
                                            @csrf
                                            <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                                                    <p>DATA CV PELAMAR</p>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="header p-3 mb-1" style="width:70%; border-radius:5px; border:2px solid #dfdfdf;">
                                                        <div class="container" style="background-color:#3949ab;color:white;text-align:center;">
                                                            <p>Filtering Data</p>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col" style="font-size: 10px;">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">No Dokumen FPTK</label>
                                                                    <div class="col-sm-5" style="font-size:11px;">
                                                                        <select name="no_dokumen" id="no_dokumen" class="form-control form-control-sm" style="font-size:11px;">
                                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                                            @foreach ($ptk_detail['looks'] as $d )
                                                                            <option value="{{$d->no_dokumen}}" >{{$d->no_dokumen}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan yang dibutuhkan</label>
                                                                    <div class="col-sm-5" style="font-size:11px;">
                                                                        <select name="jabatan_kepegawaian" id="jabatan_kepegawaian" class="form-control form-control-sm" style="font-size:11px;">
                                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                                            @foreach ($ptk_detail['looks'] as $d)
                                                                                <option value="{{$d->jabatan_kepegawaian}}">{{$d->jabatan_kepegawaian}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col" style="font-size: 10px;">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Minimal Pendidikan</label>
                                                                    <div class="col-sm-5" style="font-size:11px;">
                                                                        <select name="pendidikan" id="pendidikan" class="form-control form-control-sm" style="font-size:11px;">
                                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                                            @foreach ($ptk_detail['looks'] as $d )
                                                                            <option value="{{$d->pendidikan}}" >{{$d->pendidikan}}</option>
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" name="lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="container" style="text-align:center;width:20%;">
                                                            <button type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">Filter</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <form action="update_tamdcp" method="POST" >
                                                            <div class="row m-2" style="border:2px solid #f3f3f3;width:95%;">
                                                                <div class="col-md-2 m-2" style="font-size: 10px;">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Jabatan Dilamar</label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Nama Lengkap</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Pengalaman Kerja</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Pendidikan tertinggi</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Status</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 m-2">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Kode CV Pelamar</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">No. HP</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Lokasi Saat Ini</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Tanggal Lahir</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Nama Sekolah</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 m-2">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Program Studi/Jurusan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Tanggal Wisuda</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Nilai Rata-Rata</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Skala</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Informasi Lowongan</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 m-2" >
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Nama Karyawan/lainnya</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Pesan Pelamar</label>
                                                                    </div>
                                                                
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Resume/CV</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Tanggal Melamar</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:11px;">Email</label>
                                                                    </div>
                                                                   
                                                                </div>
                                                                &nbsp;
                                                                <div class="col row mt-2">
                                                                    <div class=""> 
                                                                        <button type="submit" style="" name="kode_lokasi_kerja" class="btn btn-primary btn-sm" id="kode_lokasi_kerja" placeholder="" value="filter">Tampil Data</button>
                                                                    </div>&nbsp;
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                    <div class="mb-6">
                                        List Analisa Hasil Interview
                                        <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
                                            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                                                <thead style="color:black;font-size: 14px; ">
                                                    <tr>
                                                        <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">Kode Analisa Hasil Interview</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Nama Pelamar</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Posisi yang dilamar</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Pendidikan</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Tanggal Interview</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Created by</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Created at</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">V</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="font-size: 12px;color:black;">
                                                @php $b=1; @endphp
                                                    @foreach($rc_cv as $data)
                                                    {{-- {{dd($data)}} --}}
                                                        <tr>
                                                            <td>{{$b++;}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->Kode Analisa Hasil Interview}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->nama_pelamar}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->posisi_yang_dilamar}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->pendidikan}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->tanggal_interview}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->created_by}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->created_at}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->status}}</td>
                                                            <td>
                                                                <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                                <a href="{{ URL::to('/list_dcp/detail/'.$data->id_cv_pelamar) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                <a href="{{ URL::to('/list_dcp/edit/'.$data->id_cv_pelamar) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $data->id_cv_pelamar }}" id="multiDelete" >
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_an')}}">Tambah</a></td>
                                                    <td>
                                                        <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                                        <button class="btn btn-danger btn-sm"  style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                        
                                    </div>
                                </div>
                                
                            </div>
                           
                            <!-- <p><a href="#ex1" rel="modal:open">Open Modal</a></p> -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- <p><a href="#ex1" rel="modal:open">Open--</a></p> -->

<!-- <div id="ex1" class="">
  <p>Thanks for clicking. That felt good.</p>
  <a href="#" rel="modal:close">Close</a>
</div> -->
<!-- <p><a href="#ex1" rel="modal:open">Open Modal</a></p> -->
    <script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

   <!-- modals -->

    <!-- Remember to include jQuery :) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> -->


    
            
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
@endsection

@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection