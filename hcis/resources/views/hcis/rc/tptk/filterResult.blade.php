@extends('re.transaksi_ptk.side')
@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Transaksi PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">transaksi ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamtptk')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                        <form action="{{ URL::to('/list_tptk/hapus_banyak') }}" method="POST"  id="form_delete">
                                            @csrf
                                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead style="color:black;">
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                        @for($i = 0; $i < count($th); $i++)
                                                            <!-- @if($th[$i] == 'id_perusahaan')
                                                                <th></th>
                                                            @endif -->
                                                            @if($th[$i] == 'nomor_permintaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Permintaan</th>
                                                            @endif
                                                            @if($th[$i] == 'tipe_transaksi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Transaksi</th>
                                                            @endif
                                                            @if($th[$i] == 'nomor_referensi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Referensi</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_permintaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Permintaan</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_efektif_transaksi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Efektif Transaksi</th>
                                                            @endif
                                                            @if($th[$i] == 'periode_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Periode Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'periode_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Periode Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_ptk_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode PTK Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_ptk_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama PTK Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'total_ptk_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Total PTK Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_ptk_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode PTK Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_ptk_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama PTK Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'total_ptk_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Total PTK Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_lokasi_kerja_tujuan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja Tujuan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_lokasi_kerja_asal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja Asal</th>
                                                            @endif
                                                            @if($th[$i] == 'deskripsi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Deskripsi</th>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                            @endif
                                                            @if($th[$i] == 'status_rekaman')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                            @endif
                                                            @if($th[$i] == 'waktu_user_input')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu User Input</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                            @endif
                                                            @if($i == count($th) - 1)
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                            @endif
                                                        @endfor
                                                    </thead>
                                                    <tbody style="font-size:11px;">
                                                        @php $b=1 @endphp 
                                                        @foreach($query as $row)
                                                            <tr>
                                                                <td style="">{{$b++}}</td>
                                                                @for($i = 0; $i < count($th); $i++)
                                                                    <!-- @if($th[$i] == 'id_perusahaan')
                                                                        <td>{{ $row->id_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif -->
            
                                                                    @if($th[$i] == 'nomor_permintaan')
                                                                        <td>{{ $row->nomor_permintaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tipe_transaksi')
                                                                        <td>{{ $row->tipe_transaksi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nomor_referensi')
                                                                        <td>{{ $row->nomor_referensi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    
                                                                    @if($th[$i] == 'tanggal_permintaan')
                                                                    <td>{{ $row->tanggal_permintaan ? date('d-m-Y', strtotime($row->tanggal_permintaan)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_efektif_transaksi')
                                                                    <td>{{ $row->tanggal_efektif_transaksi ? date('d-m-Y', strtotime($row->tanggal_efektif_transaksi)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                 
                                                                    @if($th[$i] == 'periode_tujuan')
                                                                        <td>{{ $row->periode_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'periode_asal')
                                                                        <td>{{ $row->periode_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_ptk_asal')
                                                                        <td>{{ $row->kode_ptk_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_ptk_asal')
                                                                        <td>{{ $row->nama_ptk_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'total_ptk_asal')
                                                                        <td>{{ $row->total_ptk_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_ptk_tujuan')
                                                                        <td>{{ $row->kode_ptk_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_ptk_tujuan')
                                                                        <td>{{ $row->nama_ptk_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'total_ptk_tujuan')
                                                                        <td>{{ $row->total_ptk_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_posisi_tujuan')
                                                                        <td>{{ $row->nama_posisi_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_lokasi_kerja_tujuan')
                                                                        <td>{{ $row->nama_lokasi_kerja_tujuan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_posisi_asal')
                                                                        <td>{{ $row->nama_posisi_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_lokasi_kerja_asal')
                                                                        <td>{{ $row->nama_lokasi_kerja_asal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'deskripsi')
                                                                        <td>{{ $row->deskripsi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'keterangan')
                                                                        <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'status_rekaman')
                                                                        <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'waktu_user_input')
                                                                        <td>{{ $row->waktu_user_input ? date('d-m-Y', strtotime($row->waktu_user_input)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_mulai_efektif')
                                                                        <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_selesai_efektif')
                                                                        <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($i == count($th) - 1)
                                                                        <td> 
                                                                            <a href="{{ URL::to('/list_tptk/detail/'.$row->id_transaksi_ptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                            <a href="{{ URL::to('/list_tptk/edit/'.$row->id_transaksi_ptk) }}" class="">Edit</a>
                                                                        </td>
                                                                        <td>
                                                                        <input type="checkbox" name="multiDelete[]" value="{{ $row->id_transaksi_ptk }}"  id="multiDelete">                                  
                                                                        </td>
                                                                    @endif
                                                                @endfor
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_tptk_peng')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                        <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                        <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                        </td>
                                                        <!-- <td>
                                                            <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Print</button>
                                                        </td> -->
                                                    </tr>
                                                </table>
                                        </form>
            
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        var pathname = window.location.pathname;
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_a') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
    });
</script>
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection