@extends('re.transaksi_ptk.side')

@section('content')
<div class="pcoded-wrapper">
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">

									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Transaksi PTK Pengganti</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">transaksi ptk pengganti</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<form action="{{route('simpan_tptk')}}" method="post" enctype="multipart/form-data">
						<hr>
						{{ csrf_field() }}
						<div class="form-group" style="width:95%;">
							<div class="">
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Transaksi PTK</label>
											<div class="form-check col-sm-3">
												<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
												<label class="form-check-label" for="defaultCheck1">
													Tambahan
												</label>
											</div>
											<div class="form-check col-sm-3">
												<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
												<label class="form-check-label" for="defaultCheck1">
													Penggantian
												</label>
											</div>
										</div>
									</div>

								</div>
								<hr>
								<b>Informasi</b>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
											<div class="col-sm-9">
												<input type="text" required name="nomor_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
											<div class="col-sm-9">
												<input type="text" name="nomor_referensi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
											<div class="col-sm-9">
												<input type="date" name="tanggal_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
											<div class="col-sm-9">
												<input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
									</div>
								</div>
								<hr>
								<b>Informasi PTK Tambahan</b>
								<br>
								<br>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
											<div class="col-sm-10">
												<input type="text" required name="nama_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Asal</label>
											<div class="col-sm-10">
												<input type="text" name="nama_ptk_asal" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
											<div class="col-sm-9">
												<input type="text" name="total_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
											<div class="col-sm-9">
												<textarea type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
											</div>
										</div>
									</div>

									<!-- tidak ada  -->
									<div class="col" style="font-size: 10px;">
										<b style="font-size:11px;">PTK Asal</b>
										<hr>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
											<div class="col-sm-10">
												<select name="periode_asal" required id="periode_asal" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
											<div class="col-sm-10">
												<select name="nama_posisi_asal" required id="nama_posisi_asal" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
											<div class="col-sm-10">
												<select name="nama_posisi_asal" id="nama_posisi_asal" required class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<b style="font-size:11px;">PTK Tujuan</b>
										<hr>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
											<div class="col-sm-10">
												<select name="periode_tujuan" id="periode_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
											<div class="col-sm-10">
												<select name="nama_posisi_tujuan" id="nama_posisi_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
											<div class="col-sm-10">
												<select name="nama_lokasi_kerja_tujuan" id="nama_lokasi_kerja_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
													<option value="">--- Pilih ---</option>
												</select>
											</div>
										</div>
									</div>
									<!-- tidak ada -->
								</div>
								<hr>
								<b>Rekaman Informasi</b>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
											<div class="col-sm-10">
												<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

												<!-- <input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
											<div class="col-sm-10">
												<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />

												<!-- <input type="date" name="tanggal_selesai_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
											<div class="col-sm-10">
												<textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div style="width: 100%;">
							<table>
								<tr>
									<!-- <td>
										<input type="text">
									</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
							</table>
						</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection