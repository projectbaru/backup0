@extends('re.transaksi_ptk.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Ubah Transaksi PTK {{$data->sub}}</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">transaksi ptk {{strtolower($data->sub)}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error! </strong>{{$errors->first()}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    @endif
                    <form action="{{route('simpan_tptk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
                        {{ csrf_field() }}
                        <div class="form-group" style="width:95%;">
                            <input type="hidden" name="id_transaksi_ptk" id="id_transaksi_ptk" value="{{ $data->id_transaksi_ptk }}">
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Transaksi PTK</label>
                                        <div class="form-check col-sm-3">
                                            <input class="form-check-input" type="checkbox" name="ischecked" value="0" id="isCheckTambhan" required>
                                            <label class="form-check-label" for="isCheckTambhan">Tambahan</label>
                                        </div>
                                        <div class="form-check col-sm-3">
                                            <input class="form-check-input" name="ischecked" type="checkbox" value="1" id="isCheckPergantian" required>
                                            <label class="form-check-label" for="isCheckPergantian">Penggantian</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <b>Informasi</b>
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="nomor_permintaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
                                        <div class="col-sm-9">
                                            <input type="text" required name="nomor_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan" value="{{$data->nomor_permintaan}}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nomor_referensi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="nomor_referensi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->nomor_referensi}}" id="nomor_referensi" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="tanggal_permintaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
                                        <div class="col-sm-9">
                                            <input type="date" name="tanggal_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_permintaan" value="{{ $data->tanggal_permintaan }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="tanggal_efektif_transaksi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
                                        <div class="col-sm-9">
                                            <input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_efektif_transaksi" value="{{ $data->tanggal_efektif_transaksi }}" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <b>Informasi PTK {{$data->sub}}</b>
                            <br>
                            <br>

                            <div class="row" id="ptkIfTambahan">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="nama_ptk_tujuan_ptkt" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
                                        <div class="col-sm-9">
                                            <input type="text" required name="nama_ptk_tujuan_ptkt" style="font-size:11px;" class="form-control form-control-sm ptkt" value="{{ $data->nama_ptk_tujuan }}" id="nama_ptk_tujuan_ptkt" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="total_ptk" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
                                        <div class="col-sm-9">
                                            <input type="number" name="total_ptk" style="font-size:11px;" class="form-control form-control-sm ptkt" id="total_ptk" value="{{ $data->total_ptk_tujuan }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="deskripsi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;" class="form-control form-control-sm ptkt" id="deskripsi" placeholder="">{{ $data->deskripsi }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- tidak ada  -->
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="tahun_periode" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                                        <div class="col-sm-10">
                                            <select name="tahun_periode" id="tahun_periode" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="posisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                                        <div class="col-sm-10">
                                            <select name="posisi" id="posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lokasi_kerja" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                                        <div class="col-sm-10">
                                            <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- tidak ada -->
                            </div>

                            <div class="row" id="ptkIfPergantian">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="nama_ptk_tujuan_ptkp" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="nama_ptk_tujuan_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_tujuan_ptkp" value="{{ $data->nama_ptk_tujuan }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nama_ptk_asal" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Asal</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nama_ptk_asal" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_asal" value="{{ $data->nama_ptk_asal }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="total_ptk_tujuan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
                                        <div class="col-sm-9">
                                            <input type="number" name="total_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm ptkp" id="total_ptk_tujuan" value="{{ $data->total_ptk_tujuan }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="deskripsi_ptkp" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" rows="4" cols="50" name="deskripsi_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="deskripsi_ptkp" placeholder="">{{ $data->deskripsi }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- tidak ada  -->
                                <div class="col" style="font-size: 10px;">
                                    <b style="font-size:11px;">PTK Asal</b>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="periode_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                                        <div class="col-sm-10">
                                            <select name="periode_asal" id="periode_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                                <option value="">gg</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nama_posisi_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                                        <div class="col-sm-10">
                                            <select name="nama_posisi_asal" id="nama_posisi_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nama_lokasi_kerja_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                                        <div class="col-sm-10">
                                            <select name="nama_lokasi_kerja_asal" id="nama_lokasi_kerja_asal" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col" style="font-size: 10px;">
                                    <b style="font-size:11px;">PTK Tujuan</b>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="periode_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                                        <div class="col-sm-10">
                                            <select name="periode_tujuan" id="periode_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nama_posisi_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                                        <div class="col-sm-10">
                                            <select name="nama_posisi_tujuan" id="nama_posisi_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nama_lokasi_kerja_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                                        <div class="col-sm-10">
                                            <select name="nama_lokasi_kerja_tujuan" id="nama_lokasi_kerja_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                                <option value="">--- Pilih ---</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- tidak ada -->
                            </div>
                            <hr>
                            <b>Rekaman Informasi</b>
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="lokasi_kerja" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" required style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif" value="{{ $data->tanggal_mulai_efektif }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="tanggal_selesai_efektif" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{ $data->tanggal_selesai_efektif }}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="keterangan" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="keterangan" placeholder="">{{ $data->keterangan }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 90%;">
                            <table>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('hapus_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });

        var flag = <?= json_encode($data->flag) ?>;
        if (flag == 0) {
            $('#isCheckTambhan').click();
            isTambahan();
        }
        if (flag == 1) {
            $("#isCheckPergantian").click();
            isPergantian();
        }

        $("#isCheckTambhan").change(function() {
            if (this.checked) {
                isTambahan();
            } else {
                $("#isCheckPergantian").attr('disabled', false);
                $("#isCheckPergantian").attr('required', false);
                $('.ptkp').attr('required', false);
            }
        });

        $("#isCheckPergantian").change(function() {
            if (this.checked) {
                isPergantian();
            } else {
                $("#isCheckTambhan").attr('disabled', false);
                $('.ptkt').attr('required', false);
                $("#ptkIfTambahan").show();
                $("#ptkIfPergantian").hide();
            }
        });

    });

    function isTambahan() {
        $("#isCheckPergantian").attr('disabled', true);
        $("#isCheckPergantian").removeAttr('required', true);
        $('.ptkp').attr('required', false);
        $("#ptkIfTambahan").show();
        $("#ptkIfPergantian").hide();
    }

    function isPergantian() {
        $("#isCheckTambhan").attr('disabled', true);
        $('.ptkt').attr('required', false);
        $("#ptkIfTambahan").hide();
        $("#ptkIfPergantian").show();
    }
</script>
@endsection