@extends('re.transaksi_ptk.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Ubah Transaksi PTK Penggantian</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">transaksi ptk penggantian</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <form action="{{route('update_tptkp')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
							{{ csrf_field() }}
							@foreach($tptk_peng as $data)
							<div class="form-group" style="width:95%;">
								<div class="">
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-3 col-form-label col-form-label-sm"><b>Tipe Transaksi PTK</b>:</label>
												<div class="col-sm-4">
													<input type="checkbox" name="tipe_transaksi" style="font-size:11px;width:50%;" value="{{$data->tipe_transaksi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Tambahan
												</div>
												<div class="col-sm-4">
													<input type="checkbox" name="tipe_transaksi" style="font-size:11px;width:50%;" value="{{$data->tipe_transaksi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Penggantian
												</div>
											</div>
										</div>
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<!-- <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Logo Perusahaan</label> -->
				
											</div>
										</div>
										
									</div>
									<hr>
									<b>Informasi</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
												<div class="col-sm-9">
												<input type="text" required value="{{$data->nomor_permintaan}}" name="nomor_permintaan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->nomor_referensi}}" name="nomor_referensi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
												<div class="col-sm-9">
												<input type="date"  name="tanggal_permintaan" value="{{$data->tanggal_permintaan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
										</div>
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
												<div class="col-sm-9">
												<input type="date" required value="{{$data->tanggal_efektif_transaksi}}" name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
										</div>
									</div>
									<hr>
									<b>Informasi PTK Tambahan</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
												<div class="col-sm-9">
													<input type="text" required name="nama_ptk_tujuan" value="{{$data->nama_ptk_tujuan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
												<div class="col-sm-9">
													<input type="text" name="total_ptk" style="font-size:11px;" value="{{$data->total_ptk}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
												<div class="col-sm-9">
													<textarea type="text" rows="4" cols="50" name="deskripsi" value="{{$data->deskripsi}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
												</div>
											</div>
										</div>
										<!-- tidak ada  -->
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
												<div class="col-sm-10">
													<select name="tahun_periode" id="tahun_periode"  class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
												<div class="col-sm-10">
													<select name="posisi" id="posisi" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
												<div class="col-sm-10">
													<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
										</div>
										<!-- tidak ada -->
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
												<div class="col-sm-10">
													<select name="tahun_periode" id="tahun_periode"  class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
												<div class="col-sm-10">
													<select name="posisi" id="posisi" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
												<div class="col-sm-10">
													<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
														<option value="">--- Pilih ---</option>
													</select>		
												</div>
											</div>
										</div>
										
									</div>
									<hr>
									<b>Rekaman Informasi</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
												<div class="col-sm-10">
													
												<input type="date" required value="{{$data->tanggal_efektif_transaksi}}" name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>	
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
												<div class="col-sm-10">
												<input type="date" name="tanggal_selesai_transaksi" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
												<div class="col-sm-10">
													<textarea type="text" rows="6" cols="50" value="{{$data->keterangan}}" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							<div style="width: 90%;">
								<table>
								<tr>
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
                    </form>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection