<!DOCTYPE html>
<html lang="en">

<head>
    <title>Data Pelamar</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Perusahaan" />
    <meta name="author" content="Codedthemes" />
    <link rel="icon" href="{{asset('assets')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/fontawesome-all.min.css')}}">
    <!-- <link rel="stylesheet" href="{{asset('assets/plugins/animation/css/animate.min.css')}}"> -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- <link href="{{ asset('css/all.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet"> -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css"> -->
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
</head>

<body class="">
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <nav class="pcoded-navbar menupos-fixed menu-light brand-blue ">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="index.html" class="b-brand">
                    <!-- <img src="" alt="" class="logo images"> -->
                    <!-- <img src="../assets/images/logo-icon.svg" alt="" class="logo-thumb images"> -->
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
            </div>
            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Menus</label>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('dashboard')}}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
  
                    <li class="nav-item pcoded-hasmenu" id="hasOpen1">
                        <a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Work Structure</span></a>
                        <ul class="pcoded-submenu" style="font-size:11px;">
                            <li class=""><a href="{{route('list_p')}}" class="">Perusahaan</a></li>
                            <li class=""><a href="{{route('list_a')}}" class="">Alamat Perusahaan</a></li>
                            <li class=""><a href="{{route('list_o')}}" class="">Organisasi</a></li>
                            <li class=""><a href="{{route('list_to')}}" class="">Tingkat Organisasi</a></li>
                            <li class=""><a href="{{route('list_g')}}" class="">Golongan</a></li>
                            <li class=""><a href="{{route('list_tg')}}" class="">Tingkat Golongan</a></li>
                            <li class=""><a href="{{route('list_po')}}" class="">Posisi</a></li>
                            <li class=""><a href="{{route('list_tp')}}" class="">Tingkat Posisi</a></li>
                            <li class=""><a href="{{route('list_lk')}}" class="">Lokasi Kerja</a></li>
                            <li class=""><a href="{{route('list_glk')}}" class="">Grup Lokasi Kerja</a></li>
                            <li class=""><a href="{{route('list_kc')}}" class="">Kelas Cabang</a></li>
                            <li class=""><a href="{{route('list_kac')}}" class="">Kantor Cabang</a></li>
                            <!-- <li class=""><a href="{{route('list_dp')}}" class="">Deskripsi Pekerjaan</a></li> -->
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link"><span class="pcoded-mtext">Deskripsi Pekerjaan</span></a>
                                <ul class="pcoded-submenu" style="font-size:11px;">
                                    <li class=""><a href="{{route('list_dp')}}" class="">Header Deskripsi Pekerjaan</a></li>
                                    <li class=""><a href="" class="">Detail Deskripsi</a></li>
                                </ul>
                            </li>
                            <li class=""><a href="{{route('list_j')}}" class="">Jabatan</a></li>
                            <li class=""><a href="{{route('list_kj')}}" class="">Kelompok Jabatan</a></li>
                            <li class=""><a href="{{route('list_tkj')}}" class="">Tingkat Kelompok Jabatan</a></li>
                            <li class=""><a href="{{route('list_usm')}}" class="">Standar Upah Minimum</a></li>
                        </ul>
                    </li>
                    <li class="nav-item pcoded-hasmenu" id="hasOpen">
                        <a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Recruitment</span></a>
                        <ul class="pcoded-submenu" style="font-size:11px;">
                            <!-- <li class=""><a href="{{route('list_ptk')}}" class="">PTK</a></li> -->
                            <li class=""><a href="{{route('list_tptk')}}" class="">Transaksi PTK</a></li>
                            <li class=""><a href="{{route('list_dcp')}}" class="">Data CV Pelamar</a></li>
                            <li class=""><a href="{{route('list_dpp')}}" class="">Data Personal Pelamar</a></li>
                            <li class=""><a href="{{route('list_step')}}" class="">Step-Step Process Recruitment</a></li>
                            <li class=""><a href="{{route('list_fptk')}}" class="">Formulir Permintaaan Tenaga Kerja</a></li>
                            <li class=""><a href="" class="">Process Recruitment</a></li>
                            <li class=""><a href="" class="">Lowongan Kerja</a></li>
                            <li class=""><a href="" class="">FPTK Report</a></li>
                            <li class=""><a href="" class="">Analisa Hasil Interview</a></li>
                            <li class=""><a href="" class="">Pencapaian KPI HR Recruitment</a></li>
                            <li class=""><a href="" class="">Budgeting Recruitment</a></li>
                            <li class=""><a href="" class="">Fullfillment Rate</a></li>
                            <li class=""><a href="" class="">Ontime Fulltiment</a></li>
                            <li class=""><a href="" class="">Applicant Target Candidate Buffer</a></li>
                            <li class=""><a href="" class="">Daftar Wawancara Kandidat</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
            <a href="index.html" class="b-brand">
                <!-- <img src="../assets/images/logo.svg" alt="" class="logo images">
                <img src="../assets/images/logo-icon.svg" alt="" class="logo-thumb images"> -->
            </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="#!">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">
            <a href="#!" class="mob-toggler"></a>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <div class="main-search open">
                        <!-- <div class="input-group"> -->
                        <!-- <input type="text" id="m-search" class="form-control" placeholder="Search . . ."> -->
                        <!-- <a href="#!" class="input-group-append search-close">
                                <i class="feather icon-x input-group-text"></i>
                            </a>
                            <span class="input-group-append search-btn btn btn-primary">
                                <i class="feather icon-search input-group-text"></i>
                            </span> -->
                        <!-- </div> -->
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li>
                    <div class="dropdown drp-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-user"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="" class="img-radius" alt="">
                                <span>User</span>
                               
                            </div>
                            <ul class="pro-body">
                                <li><a href="#!" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a></li>
                                <li><a href="#!" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                <li>
                                <a  title="Logout" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                                    <i class="feather icon-log-out"></i>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                </li>
                                <!-- <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
                                <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li> -->
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <section class="pcoded-main-container">
        @yield('content')
    </section>

    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/pcoded.min.js')}}"></script>
    <!-- <script src="{{ asset('js/custom.js') }}"></script> -->

    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

 
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
            var pathname = window.location.pathname;
            if (pathname != '/list_tptk') {
                $("#hasOpen").trigger('click');
            }
        });
    </script>
    @yield('add-scripts')
</body>

</html>