@extends('re.ptk.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- <div class="page-header mb-1">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <br>
                    <br>
                    <div class="row ">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-body  table-border-style">
                                    <div class="table-responsive-xl " style="">
                                        <form action="" method="POST" id="form_filter">
                                        @csrf
                                            <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container" style="color:white;text-align:center;">
                                                    <p>Data</p>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-4" style="font-size: 10px;">
                                                        <div class="m-2 p-2" style="border:2px solid #f3f3f3;width:95%;">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No Dokumen FPTK</label>
                                                                <div class="col-sm-9">

                                                                    <input type="text" id="no_dokumen_fptk"  name="no_dokumen_fptk" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan yang dibutuhkan</label>
                                                                <div class="col-sm-9">
                                            
                                                                    <input type="text" id="jabatan_lowongan"   name="jabatan_lowongan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Minimal Pendidikan</label>
                                                                <div class="col-sm-9">
                                                                    
                                                                    <input type="text" id="minimal_pendidikan"   name="minimal_pendidikan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="lokasi_kerja"   name="lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="container" style="text-align:center;width:50%;">
                                                                <a type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">Filter</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <form action="{{ route('filter_ptk') }}" method="POST" id="form_filter" >
                                                            <div class="row m-2" style="border:2px solid #f3f3f3;width:95%;">
                                                                @if(isset($labels) && count($labels) > 0)
                                                                    <div class="col-md-2 m-2" style="font-size: 10px;">
                                                                    @foreach($labels as $idx => $label)
                                                                        <div class=" row">
                                                                            <div class="">
                                                                                <input @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter)) {{ 'checked' }} @endif type="checkbox" style="font-size:3px;" name="cv[]" value="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="{{strtolower(str_replace(" ", "_", $label))}}" placeholder="">
                                                                            </div>&nbsp;
                                                                            <label for="{{strtolower(str_replace(" ", "_", $label))}}" class=" " style="font-size:11px;">{{$label}}</label>
                                                                        </div>
                                                                        @if($idx%5 == 0 && $idx != 0)
                                                                            </div>
                                                                            <div class="col-md-2 m-2">
                                                                        @else
                                                                        @endif
                                                                    @endforeach
                                                                    </div>
                                                                @endif

                                                                &nbsp;
                                                                <div class="col row mt-2">
                                                                    <div class="">
{{--                                                                        <input type="submit" value="Submit" class="btn btn-primary btn-sm" style="border-radius:5px;font-size:11px;">--}}
                                                                        <button type="submit" style="" name="kode_lokasi_kerja" class="btn btn-primary btn-sm" id="kode_lokasi_kerja" placeholder="" value="filter">Tampil Data</button>
                                                                    </div>&nbsp;
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                    <div class="mb-6">
                                        <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                            <thead style="color:black;font-size: 12px; ">
                                                <tr >
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">No</th>
                                                    @if(isset($data) && isset($labels) && count($labels) > 0)
                                                        @foreach($labels as $idx => $label)
                                                            @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">{{$label}}</th>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <th style="color:black;font-size: 12px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Jabatan dilamaran</th>
                                                        <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Nama Lengkap</th>
                                                        <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Pengalaman Kerja</th>
                                                        <th style="color:black;font-size: 12px; background-color:#a5a5a5;" scope="col">Pendidikan Tertinggi</th>
                                                        <th style="color:black;font-size: 12px; background-color:#a5a5a5;" scope="col">Status</th>
                                                        <th style="color:black;font-size: 12px; background-color:#a5a5a5;" scope="col">Kode CV Pelamar</th>
                                                    @endif
                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">V</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 11px;">
                                            @php $b=1; @endphp
                                                {{-- {{dd($data)}} --}}
                                                    <tr>
                                                        <td style=""></td>
                                                        @if(isset($data) && isset($labels) && count($labels) > 0)
                                                            @foreach($labels as $idx => $label)
                                                                @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                                                    <td style=""></td>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <td style=""></td>
                                                            <td style=""></td>
                                                            <td style=""></td>
                                                            <td style=""></td>
                                                            <td style=""></td>
                                                            <td style=""></td>
                                                        @endif
                                                
                                                        <td>
                                                            <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                            <a href="{{ URL::to('/list_dcp/detail/') }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                            <a href="{{ URL::to('/list_dcp/edit/') }}" class="">Edit</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="multiDelete[]" value="" id="multiDelete" >
                                                        </td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tr>
                                                <td>
                                                    <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_dcp')}}">Tambah</a></td>
                                            
                                                <td>
                                                    <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                                    <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_filter").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_filter").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection