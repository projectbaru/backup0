@extends('re.cv.side')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Edit Data CV Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($rc_cv as $data)
							<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_cv_pelamar }}" />

							<div class="form-group" style="width:95%;">
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
										<b>Melamar Lowongan</b>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan yang dilamar</label>
													<!-- <div class="col-sm-9">
													<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div> -->
													<div class="col-sm-9">
														<input type="text" required name="jabatan_lowongan" value="{{$data->jabatan_lowongan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Melamar</label>
													<div class="col-sm-9">
														<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
														<select name="tingkat_pekerjaan" id="tingkat_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
															<option value="" selected disabled>--Tingkat--</option>
															<option value="staff" {{ $data->tingkat_pekerjaan == 'staff' ? 'selected' : NULL }}>Staff</option>
															<option value="middle" {{ $data->tingkat_pekerjaan == 'middle' ? 'selected' : NULL }}>Middle</option>
															<option value="senior" {{ $data->tingkat_pekerjaan == 'senior' ? 'selected' : NULL }}>Senior</option>
														</select>	
													</div>
											
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
													<div class="col-sm-9">
													<select name="lokasi_kerja[]"  id="select2-multiple" multiple="multiple" class="select2-multiple form-control form-control-sm" style="font-size:11px; ">
														@foreach ($datalk['looks'] as $np )
														<option value="{{$np->nama_lokasi_kerja}}" @if(old('np') == $np->nama_lokasi_kerja || $np->nama_lokasi_kerja == $data->lokasi_kerja) selected @endif>{{$np->nama_lokasi_kerja}}</option>
														@endforeach
													</select>
													
													</div>
												
												</div>
											</div>
										</div>
										<hr>
										<b>Personal Information</b>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
													<div class="col-sm-9">
														<a style="font-size:11px;" class="btn btn-outline-success btn-sm" href="{{asset('data_file/'.$data->resume_cv)}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="download resume" download>Download</a>
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
													<div class="col-sm-9">
													<input type="nama_lengkap" required name="nama_lengkap" value="{{$data->nama_lengkap}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
													<div class="col-sm-9">
													<input type="email" name="email" style="font-size:11px;" value="{{$data->email}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
													<div class="col-sm-9">
													<input type="number" required name="no_hp" style="font-size:11px;" value="{{$data->no_hp}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
													<div class="col-sm-9">
													<input type="text" required name="lokasi_pelamar" style="font-size:11px;" value="{{$data->lokasi_pelamar}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input" type="radio" name="pengalaman_kerja"  value="Berpengalaman" {{ $data->pengalaman_kerja == 'Berpengalaman' ? 'checked' : NULL }} >
															<label class="form-check-label" for="flexRadioDefault1">
																Memiliki Pengalaman
															</label>
														</div>&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" type="radio" name="pengalaman_kerja" value="Lulusan Baru" {{ $data->pengalaman_kerja == 'Lulusan Baru' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Lulusan baru
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
													<div class="col-sm-4">
													<input type="text" required name="tahun_pengalaman_kerja" style="font-size:11px;" value="{{$data->tahun_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">Tahun
													</div>
													<div class="col-sm-5">
													<input type="text" required name="bulan_pengalaman_kerja" style="font-size:11px;" value="{{$data->bulan_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="" >Bulan
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
													<div class="col-sm-9">
													<input type="date" required name="tanggal_lahir" style="font-size:11px;" value="{{$data->tanggal_lahir}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
													
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
													<!-- <div class="col-sm-4">
														<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
													</div>
													<div class="col-sm-5">
														<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
													</div> -->
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1" value="{{$data->jenis_kelamin}}" {{ $data->jenis_kelamin == 'Laki-laki' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Laki-laki
															</label>
														</div>&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1" value="{{$data->jenis_kelamin}}" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Perempuan
															</label>
														</div>
													</div>
													
												</div>
											</div>
										</div>
									<hr>
									<b>Pendidikan</b>
									<br>
									
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
												<div class="col-sm-9">
													<select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="form-control form-control-sm" style="font-size:11px;">
														<!-- <option value="" selected disabled>--Tingkat--</option> -->
														<option value="SMK/Sederajat" {{ $data->pendidikan_tertinggi == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
														<option value="D1" {{ $data->pendidikan_tertinggi == 'D1' ? 'selected' : NULL }}>D1</option>
														<option value="D2" {{ $data->pendidikan_tertinggi == 'D2' ? 'selected' : NULL }}>D2</option>
														<option value="D3" {{ $data->pendidikan_tertinggi == 'D3' ? 'selected' : NULL }}>D3</option>
														<option value="S1" {{ $data->pendidikan_tertinggi == 'S1' ? 'selected' : NULL }}>S1</option>
														<option value="S2" {{ $data->pendidikan_tertinggi == 'S2' ? 'selected' : NULL }}>S2</option>
														<option value="S3" {{ $data->pendidikan_tertinggi == 'S3' ? 'selected' : NULL }}>S3</option>

													</select>	
													<!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
												<div class="col-sm-9">
													<input type="text" name="nama_sekolah" style="font-size:11px;" value="{{$data->nama_sekolah}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
												<div class="col-sm-9">
													<input type="text" name="program_studi_jurusan" value="{{$data->program_studi_jurusan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
												<div class="col-sm-9">
													<input type="date" name="tanggal_wisuda" style="font-size:11px;" value="{{$data->tanggal_wisuda}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
												<div class="col-sm-4">
													<input type="input" name="nilai_rata_rata" style="font-size:11px;" value="{{$data->nilai_rata_rata}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>Skala*
												<div class="col-sm-4">
												<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
													<input type="text" name="nilai_skala" style="font-size:11px;" value="{{$data->nilai_skala}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											
										</div>

									</div>
									<hr>
									<b style="font-size:11px;">Informasi Tambahan</b>
									<hr>
									<div class="row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
										<div class="col-sm-4">
											<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
												<option value="Web SRU" {{ $data->keterangan_informasi_pilihan == 'Web SRU' ? 'selected' : NULL }}>Web SRU</option>
												<option value="Job Street" {{ $data->keterangan_informasi_pilihan == 'Job Street' ? 'selected' : NULL }}>Job Street</option>
												<option value="Karyawan SRU" {{ $data->keterangan_informasi_pilihan == 'Karyawan SRU' ? 'selected' : NULL }}>Karyawan SRU</option>
												<option value="Lainnya" {{ $data->keterangan_informasi_pilihan == 'Lainnya' ? 'selected' : NULL }}>Lainnya</option>
											</select>	
										</div>
										<div class="col-sm-4">
											<input type="text" name="keterangan_informasi_pilihan" value="{{$data->keterangan_informasi_pilihan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
										</div>
									</div><br>
									<div class="row">
										<div class="col-sm-12 form-floating">
											<textarea class="form-control" name="pesan_pelamar" value="{{$data->pesan_pelamar}}" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px">{{$data->pesan_pelamar}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
									<tr>
										<!-- <td>
											<input type="text">
										</td> -->
										<td>
											<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
										</td>
										<td>
											<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
										</td>
										<td>
											<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_dcp', $data->id_cv_pelamar)}}" id="btn_delete">Hapus</a>
										</td>

									</tr>
								</table>
							</div>
							<br>
							@endforeach
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
       
	   <script>
		 $(document).ready(function() {
			 // Select2 Multiple
			 $('.select2-multiple').select2({
				 placeholder: "Lokasi Kerja",
				 allowClear: true
			 });
 
		 });
 
	 </script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection