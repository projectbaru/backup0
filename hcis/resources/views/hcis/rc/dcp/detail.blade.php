@extends('re.transaksi_ptk.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Detail Data CV Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($rc_cv as $data)
							<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_cv_pelamar }}" />

							<div class="form-group" style="width:95%;">
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
									<b>Melamar Lowongan</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
                                            <table>
                                                <div class="form-group row">
                                                    <tr>
                                                        <td>{{$data->jabatan_lowongan}}</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Melamar</td>
                                                        <td>:</td>
                                                        <td>{{$data->tanggal_melamar}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tingkat</td>
                                                        <td>:</td>
                                                        <td>{{$data->tingkat_pekerjaan}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lokasi Kerja</td>
                                                        <td>:</td>
                                                        <td>{{$data->lokasi_kerja}}</td>
                                                    </tr>                                               
                                                </div>
                                            </table>
										</div>
									</div>
										<hr>
										<b>Personal Information</b>
										<br><br>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
													<div class="col-sm-9">
                                                    	<a href="{{asset('data_file/'.$data->resume_cv)}}" class="btn btn-outline-success btn-sm" download> Download</a>
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
													<div class="col-sm-9">
													<input type="nama_lengkap" readonly required name="nama_lengkap" value="{{$data->nama_lengkap}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
													<div class="col-sm-9">
													<input type="tanggal_lahir" readonly required name="tanggal_lahir" value="{{$data->tanggal_lahir}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
													<div class="col-sm-9">
													<input type="lokasi_pelamar" readonly required name="lokasi_pelamar" value="{{$data->lokasi_pelamar}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input" type="radio" name="pengalaman_kerja" disabled value="Berpengalaman" readonly {{ $data->pengalaman_kerja == 'Berpengalaman' ? 'checked' : NULL }} >
															<label class="form-check-label" for="flexRadioDefault1">
																Memiliki Pengalaman
															</label>
														</div>&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" type="radio" name="pengalaman_kerja" disabled value="Lulusan Baru" readonly {{ $data->pengalaman_kerja == 'Lulusan Baru' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Lulusan baru
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
													<div class="col-sm-4">
													<input type="text" readonly required name="tahun_pengalaman_kerja" style="font-size:11px;" value="{{$data->tahun_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">Tahun
													</div>
													<div class="col-sm-5">
													<input type="text" readonly required name="bulan_pengalaman_kerja" style="font-size:11px;" value="{{$data->bulan_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="" >Bulan
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
													<div class="col-sm-9">
													<input type="email" readonly required name="email" value="{{$data->email}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
											</div>
										</div>
										<b>Pendidikan</b>
										<div class="row">
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
													<div class="col-sm-9">
														<input type="text" readonly required name="text" value="{{$data->pendidikan_tertinggi}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
													<div class="col-sm-9">
														<input type="text" readonly required name="text" value="{{$data->nama_sekolah}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
													<div class="col-sm-9">
														<input type="text" readonly required name="program_studi_jurusan" value="{{$data->program_studi_jurusan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/Tanggal Perkiraan Wisuda</label>
													<div class="col-sm-9">
														<input type="text" readonly required name="tanggal_wisuda" value="{{$data->tanggal_wisuda}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
													<div class="col-sm-4">
														<input type="text" value="{{$data->nilai_rata_rata}}" readonly name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" data-inputmask="'mask': '9999 9999 9999 9999'" class="form-control form-control-sm" placeholder="">
													</div>Skala*
													<div class="col-sm-4">
													<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
														<input type="text" value="{{$data->nilai_skala}}" readonly name="nilai_skala" style="font-size:11px;" id="nilai_skala" data-inputmask="'mask': '9999 9999 9999 9999'" class="form-control form-control-sm" placeholder="">
													</div>
												</div>
											</div>
										</div>
										<b>Informasi Tambahan</b>
										<div class="row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
											<div class="col-sm-4">
												<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" disabled style="font-size:11px;">
													<option value="Web SRU" {{$data->keterangan_informasi_pilihan='Web SRU' ? 'selected' : NULL }} >Web SRU</option>
													<option value="Job Street" {{$data->keterangan_informasi_pilihan='Job Street' ? 'selected' : NULL }}>Job Street</option>
													<option value="Karyawan SRU" {{$data->keterangan_informasi_pilihan='Karyawan SRU' ? 'selected' : NULL }}>Karyawan SRU</option>
													<option value="Lainnya" {{$data->keterangan_informasi_pilihan='Lainnya' ? 'selected' : NULL }}>Lainnya</option>
												</select>	
											</div>
											<div class="col-sm-4">
												<input type="text" readonly required value="{{$data->keterangan_informasi_pilihan}}" name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
											</div>
										</div><br>
										<div class="row">
											<div class="col-sm-12 form-floating">
												<textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" value="{{$data->pesan_pelamar}}" style="height: 100px">{{$data->pesan_pelamar}}</textarea>
											</div>
										</div>
								</div>
								<br>
								<b>Penilaian HRD</b>
								<div class="row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Lamaran</label>
									<div class="col-sm-4">
										<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" disabled style="font-size:11px;">
											<option value="Belum Diperiksa" {{$data->status_lamaran='Web SRU' ? 'selected' : NULL }} >Web SRU</option>
											<option value="Sedang Dipertimbangkan" {{$data->status_lamaran='Job Street' ? 'selected' : NULL }}>Sedang Dipertimbangkan</option>
											<option value="Lulus" {{$data->status_lamaran='Lulus' ? 'selected' : NULL }}>Lulus</option>
										</select>	
									</div>
									
								</div><br>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<td>
										<a href="{{ URL::to('/list_dcp/edit/'.$data->id_cv_pelamar) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Edit</a>
									</td>
									<td>
										<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
									<td>
										<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_dcp', $data->id_cv_pelamar)}}"  id="btn_delete">Hapus</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
							@endforeach
						</form>
					</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection