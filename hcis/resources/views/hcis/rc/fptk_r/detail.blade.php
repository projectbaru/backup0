
@extends('re.fptk_report.side')
@section('content')

<div class="pcoded-wrapper">
    <!-- Link to open the modal -->
    <div style="">
        <div style="margin-top:100px;overflow:auto;">
        <div>
        <div class="pcoded-wrapper" >
        <div class="pcoded-content" >
            <div class="pcoded-inner-content">
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-12" style="margin-top:10px;">
                                        <div class="page-header-title" >
                                            <h5 class="m-b-10">Detail Report Formulir Permintaan Tenaga Kerja</h5>
                                        </div>
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                            <li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
                                            <li class="breadcrumb-item"><a href="#!">Detail Report Formulir Permintaan Tenaga Kerja</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        @foreach($rc_fptk as $data)
                        
                            <div class="">
                                <table id="example" class="">
                                    <tr>
                                        <td>No ISO</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>No Dokumen</td>
                                        <td>:</td>
                                        <td>{{$data->no_dokumen}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>:</td>
                                        <td>{{$data->tanggal_upload_dokumen_terakhir}}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                        <div class="row">
                            <hr>
                            <div class="col">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Nama Pemohon</td>
                                        <td>:</td>
                                        <td>{{$data->nama_pemohon}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:</td>
                                        <td>{{$data->jabatan_pemohon}}</td>
                                    </tr>
                                    <tr>
                                        <td>Departemen</td>
                                        <td>:</td>
                                        <td>{{$data->departemen_pemohon}}</td>
                                    </tr>
                                    <tr>
                                        <td>Dokumen FPTK Terakhir</td>
                                        <td>:</td>
                                        <td><a href="" download>Download</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Jumlah Kebutuhan</td>
                                        <td>:</td>
                                        <td>{{$data->jumlah_kebutuhan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan yang Dibutuhkan</td>
                                        <td>:</td>
                                        <td>{{$data->jabatan_kepegawaian}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tingkat</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="staff" id="flexCheckDefault" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }} >
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Staff
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="middle" id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Middle
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="senior" id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Senior
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> -->


    
            
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
@endsection

@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection