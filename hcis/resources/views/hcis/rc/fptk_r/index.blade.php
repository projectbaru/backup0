@extends('re.cv.side') 
@section('content') 
<div class="pcoded-wrapper">
    <div id="ex1" class="" style="margin-top:100px;overflow:auto;">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              <div class="main-body">
                <div class="page-wrapper">
                  <div class="page-header">
                    <div class="page-block">
                      <div class="row align-items-center">
                        <div class="col-md-12" style="margin-top:10px;">
                          <div class="page-header-title">
                            <h5 class="m-b-10">FPTK Report</h5>
                          </div>
                          <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                              <a href="index.html">
                                <i class="feather icon-home"></i>
                              </a>
                            </li>
                            <li class="breadcrumb-item">
                              <a href="#!">Recruitment</a>
                            </li>
                            <li class="breadcrumb-item">
                              <a href="#!">FPTK Report</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                    <thead style="color:black;font-size: 14px; ">
                      <tr>
                        <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">No</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Nama Pemohon</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">No Dokumen</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Jabatan Yang Dibutuhkan</th>
                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Tingkat</th>
                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Jumlah Kebutuhan</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Lampiran</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Tanggal Upload Dokumen Terakhir</th>
                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody style="font-size: 12px;color:black;"> 
                    @php $b=1; @endphp @foreach($rc_fptk as $data) {{-- {{dd($data)}} --}} 
                      <tr>
                        <td>{{$b++;}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->nama_pemohon}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->no_dokumen}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->jabatan_kepegawaian}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->tingkat_kepegawaian}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->jumlah_kebutuhan}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->lampiran_file}}</td>
                        <td style="font-size: 12px;color:black;">{{$data->tanggal_upload_dokumen_terakhir}}</td>
                        <td>
                          <a href="{{ URL::to('/list_fptk_r/detail/'.$data->id_fptk) }}" class="">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        </td>
                      </tr> 
                    @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
      <script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
      <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> @endsection @section('add-scripts') <script>
        $('#manual-ajax').click(function(event) {
          event.preventDefault();
          this.blur();
          /
          $.get(this.href, function(html) {
            $(html).appendTo('body').modal();
          });
        });
      </script>
      <script>
        $('#nama_grup_lokasi_kerja').change(function() {
          $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
        })
      </script>
      <script>
        $('#nama_lokasi_kerja').change(function() {
          $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
        })
      </script>
      <script>
        $(function() {
          //hang on event of form with id=myform
          $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
              title: ' < h4 > Anda yakin ingin menghapus ? < /h4>',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              cancelButtonText: "Batal",
              confirmButtonText: " < i > Ya,
              hapus! < /i>",
              // buttonsStyling: false
            }).then((result) => {
              if (result.isConfirmed) {
                $.ajax({
                  url: actionurl,
                  type: 'post',
                  dataType: 'JSON',
                  cache: false,
                  data: $("#form_delete").serialize(),
                  success: function(res) {
                    if (res.status) {
                      Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Data berhasil dihapus.",
                      });
                    }
                  },
                  error: function(e) {
                    console.log(e);
                    Swal.fire({
                      icon: "error",
                      title: "Gagal!",
                      text: "Terjadi kesalahan saat menghapus data.",
                    });
                  },
                  complete: function(jqXhr, msg) {
                    setTimeout(() => {
                      location.reload();
                    }, 800);
                    // console.log(jqXhr, msg);
                  }
                });
              }
            })
          });
        });
      </script> 
@endsection