@extends('re.process_re.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Tambah Process Rekruitment</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Process Rekruitment</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_pro')}}" method="post" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Jabatan</label>
                                    <div class="col-sm-10">
                                        <input type="text" style="font-size:11px;" name="nama_jabatan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                    </div>
                                </div>
							<hr>
							{{ csrf_field() }}
                            <div class="form-group row" id="qualificationInputContainer">
                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
                                <div class="col-sm-2">
                                    <input type="text" style="font-size:11px;" name="nama_step[]" class="form-control form-control-sm" required>
                                </div>
                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
                                <div class="col-sm-2">
                                    <input type="text" style="font-size:11px;" name="no_urut[]" class="form-control form-control-sm" required>
                                </div>
                                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
                                <div class="col-sm-2">
                                    <textarea type="text" style="font-size:11px;" name="keterangan[]" class="form-control form-control-sm" required></textarea>
                                </div>            
                                <div class="col-sm-1">
                                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
                                        <i class="fa-solid fa-plus"></i>
                                    </button>
                                </div>
                            </div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
										<input type="text">
									</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_process') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@include('js.step_recruit.tambah')

@endsection

