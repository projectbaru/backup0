@extends('re.process_re.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Process Rekruitment</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">Rekruitmen</a></li>
                                        <li class="breadcrumb-item"><a href="#!">Proses Rekruitment</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <!-- <div class="card-header">
                                    <h5>Per Table</h5>
                                    <a href="{{route('tambah_jpr')}}"  class="btn btn-success"><span class="d-block m-t-5"> <code style="color:white;">Tambah Jabatan Proses Rekruitment</code></span></a>
                                </div> -->
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                    <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                        <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                      
                                        <div style="border:1px solid black;">
                                            <form action="{{ URL::to('/list_process/hapus_banyak') }}" method="POST" id="form_delete" style="margin:30px;">
                                                @csrf
                                                <table class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead style="color:black;font-size: 12px; ">
                                                        <tr>
                                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">No</th>
                                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">Kode Kandidat</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; padding-right:-60px;" scope="col">No Dokumen FPTK</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Kode CV Pelamar</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Kode Data Personal Pelamar</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Nama Kandidat</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Jabatan yang dilamar</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Nama Posisi</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">No HP</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Email Pribadi</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="font-size: 11px;">
                                                    @php $b=1; @endphp
                                                        @foreach($rc_dpp as $data)  
                                                        {{-- {{dd($data)}} --}}
                                                            <tr>
                                                                <td>{{$b++;}}</td>
                                                                <td style="padding-right:-60px;">{{$data->kode_process_recruitment}}</td>
                                                                <td>{{$data->no_dokumen}}</td>
                                                                <td>{{$data->kode_cv_pelamar}}</td>
                                                                <td>{{$data->kode_data_pelamar}}</td>
                                                                <td>{{$data->nama_kandidat}}</td>
                                                                <td>{{$data->jabatan_dilamar}}</td>
                                                                <td>{{$data->nama_posisi}}</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <a href="{{ URL::to('/list_process/detail/'.$data->id_process_recruitment) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                    <a href="{{ URL::to('/list_process/edit/'.$data->id_process_recruitment) }}" class="">Edit</a>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" name="multiDelete[]" value="{{ $data->id_process_recruitment }}" id="multiDelete" >
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('ubah_jpr', $data->id_process_recruitment)}}">Ubah</a></td>
                                                        <td>
                                                            <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="{{route('hapus_step',  $data->id_process_recruitment)}}">Hapus</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                            
                                        </div>
                                        <br>
                                        
                                       
                                    <!-- </div> -->
                                    </div>
                                </div>
                            <br>
                                <br>
                                       
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection