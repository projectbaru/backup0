@extends('re.personal.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Detail Data Personal Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data Personal Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($dpp as $data)
								<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_data_pelamar }}" />

								<div class="form-group" style="width:95%;">
									<div class="">
										<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
											<p>Tambah Data CV Pelamar</p>
										</div> -->
										<b>Formulir Data Pelamar</b><br><br>
										<p>Data Pribadi</p>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<div class="form-group row">
														
														<tr>
															<td>Kode CV Pelamar</td>
															<td>:</td>
															<td><input type="hidden" name="kode_cv_pelamar" value="{{$data->kode_cv_pelamar}}">{{$data->kode_cv_pelamar}}</td>
														</tr>
														<tr>
															<td>Posisi yang dilamar</td>
															<td>:</td>
															<td><input type="hidden" name="posisi_lamaran" value="{{$data->posisi_lamaran}}">{{$data->posisi_lamaran}}</td>
														</tr>   
														<tr>
															<td>Nama Lengkap (KTP)</td>
															<td>:</td>
															<td><input type="hidden" name="nama_lengkap" value="{{$data->nama_lengkap}}">{{$data->nama_lengkap}}</td>
														</tr> 
														<tr>
															<td>Nama Penggilan</td>
															<td>:</td>
															<td><input type="hidden" name="nama_panggilan" value="{{$data->nama_panggilan}}">{{$data->nama_panggilan}}</td>
														</tr>   
														<tr>
															<td>Tempat & tgl lahir</td>
															<td>:</td>
															<td><input type="hidden" name="tempat_lahir" value="{{$data->tempat_lahir}}">{{$data->tempat_lahir}}</td>
														</tr>   
														<tr>
															<td>Agama</td>
															<td>:</td>
															<td><input type="hidden" name="agama" value="{{$data->agama}}">{{$data->agama}}</td>
														</tr>  
														<tr>
															<td>Kebangsaan</td>
															<td>:</td>
															<td>
																<input type="hidden" name="kebangsaan" value="{{$data->kebangsaan}}">{{$data->kebangsaan}}</td>
														</tr>  
														<tr>
															<td>Jenis Kelamin</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" type="radio" name="jenis_kelamin"  value="laki-laki" {{ $data->jenis_kelamin == 'laki-laki' ? 'checked' : NULL }}>
																		Laki-laki&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
																		Perempuan
																</div>
															</td>
														</tr>      
														<tr>
															<td>BB & TB</td>
															<td>:</td>
															<td>
																<div class="row">
																<div class="col-sm-4">
																	<input type="text" readonly required name="berat_badan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" value="{{$data->berat_badan}}">Kg
																</div>
																<div class="col-sm-5">
																	<input type="text" readonly required name="tinggi_badan" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" value="{{$data->tinggi_badan}}">Cm
																</div>
																</div>
															</td>
														</tr> 
														<tr>
															<td>Status Kendaraan</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<div class="form-check">
																			<input class="form-check-input" type="radio" name="status_kendaraan"  value="milik sendiri" {{ $data->status_kendaraan == 'milik sendiri' ? 'checked' : NULL }}>
																			Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="kantor" {{ $data->status_kendaraan == 'kantor' ? 'checked' : NULL }}>
																			Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="orangtua" {{ $data->status_kendaraan == 'orangtua' ? 'checked' : NULL }}>
																			Orangtua&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="lainnya" {{ $data->status_kendaraan == 'lainnya' ? 'checked' : NULL }}>
																			Lainnya
																	</div>
																</div>
															</td>
														</tr>   
														<tr>
															<td>Jenis Kendaraan/Merk</td>
															<td>:</td>
															<td><input type="hidden" name="jenis_merk_kendaraan" value="{{$data->jenis_merk_kendaraan}}">{{$data->jenis_merk_kendaraan}}
															</td>
														</tr>   
														<tr>
															<td>Alamat didalam kota [Lengkap]</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_didalam_kota" value="{{$data->alamat_didalam_kota}}">{{$data->alamat_didalam_kota}}
															</td>
														</tr>
														<tr>
															<td>Kode Pos</td>
															<td>:</td>
															<td><input type="hidden" name="kode_pos_1" value="{{$data->kode_pos_1}}">{{$data->kode_pos_1}}
															</td>
														</tr>                                   
													</div>
												</table>
											</div>
											<div class="col" style="font-size: 10px;">
												<table>
													<div class="form-group row">
														<tr>
															<td>No. KTP</td>
															<td>:</td>
															<td><input type="hidden" name="no_ktp" value="{{$data->no_ktp}}">{{$data->no_ktp}}</td>
														</tr>
														<tr>
															<td>Email Pribadi</td>
															<td>:</td>
															<td><input type="hidden" name="email_pribadi" value=""></td>
														</tr>
														<tr>
															<td>Alamat sosmed pribadi</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_sosmed" value="{{$data->alamat_sosmed}}">{{$data->alamat_sosmed}}</td>
														</tr>        
														<tr>
															<td>SIM yang dimiliki</td>
															<td>:</td>
															<td><input type="hidden" name="kategori_sim" value="{{$data->kategori_sim}}">{{$data->kategori_sim}}&nbsp;&nbsp;&nbsp;&nbsp;No<input type="hidden" name="no_sim" value="{{$data->no_sim}}">{{$data->no_sim}}</td>
														</tr> 
														<tr>
															<td>No. HP</td>
															<td>:</td>
															<td><input type="hidden" name="no_hp" value="{{$data->no_hp}}">{{$data->no_hp}}</td>
														</tr>   
														<tr>
															<td>No. Telp</td>
															<td>:</td>
															<td><input type="hidden" name="no_telepon" value="{{$data->no_telepon}}">{{$data->no_telepon}}</td>
														</tr>
														<tr>
															<td>Tempat tinggal</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal"  value="milik sendiri" {{ $data->tempat_tinggal == 'milik sendiri' ? 'checked' : NULL }} >Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal" value="ikut orangtua" {{ $data->tempat_tinggal == 'ikut orangtua' ? 'checked' : NULL }}>Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal" value="kost/kontrak" {{ $data->tempat_tinggal == 'kost/kontrak' ? 'checked' : NULL }}>Orangtua
																</div>	
															</td>
														</tr>   
														<tr>
															<td>Status Perkawinan</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan"  value="menikah" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}>
																		Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan" value="janda/duda" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}>
																		Janda/Duda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan" value="tidak menikah" {{ $data->status_perkawinan == 'tidak menikah' ? 'checked' : NULL }}>
																		Tidak Menikah
																</div>
															</td>
														</tr>   
														<tr>
															<td>Alamat diluar kota(Lengkap)</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_diluar_kota" value="{{$data->alamat_diluar_kota}}">{{$data->alamat_diluar_kota}}</td>
														</tr>   
														<tr>
															<td>Kode Pos</td>
															<td>:</td>
															<td><input type="hidden" name="kode_pos_2" value="{{$data->kode_pos_2}}">{{$data->kode_pos_2}}</td>
														</tr> 
														<tr>
															<td>Photo</td>
															<td>:</td>
															<td><input type="hidden" name="pas_foto" value="{{$data->pas_foto}}">{{$data->pas_foto}} <a href="" download>download</a></td>
														</tr>                         
													</div>
												</table>
											</div>
										</div>
											<br>
											<b>Susunan Keluarga (termasuk pelamar)</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 8px;">
												<table class="table" style="font-size: 8px;">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" ></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col" colspan="2" style="border:1px solid #dee2e6;">Pekerjaan Terakhir</th>
															<th scope="col"></th>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Hubungan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Nama</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Laki-laki/Perempuan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Usia(Tahun Lahir)</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Pendidikan Terakhir</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Jabatan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Perusahaan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Keterangan</th>
														</tr>
													</thead>
													<tbody class="table table-bordered" style="">
														@foreach($dpp_k as $data_k)
														<tr>
															<td><input type="text" value="{{$data_k->hubungan_keluarga}}" name="" disabled class="form-control form-control-sm"></td>
															<td><input type="text" value="{{$data_k->nama_keluarga}}" disabled class="form-control form-control-sm"></td>
															<td><input type="text" value="{{$data_k->jenis_kelamin}}" disabled class="form-control form-control-sm"></td>
															<td><input type="text" value="{{$data_k->usia_tahun_lahir}}" disabled class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select name="{{$data->pendidikan_terakhir}}" id="name_p" disabled class="form-control form-control-sm">
																		<option value="" data-kode ="" >SD</option>
																		<option value="" data-kode ="" >SMP/Sederajat</option>
																		<option value="" data-kode ="" >SMK/Sederajat</option>
																		<option value="" data-kode ="" >D1</option>
																		<option value="" data-kode ="" >D2</option>
																		<option value="" data-kode ="" >D3</option>
																		<option value="" data-kode ="" >S1</option>
																		<option value="" data-kode ="" >S2</option>
																		<option value="" data-kode ="" >S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" disabled class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" disabled class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" disabled class="form-control form-control-sm"></td>
														</tr>
														@endforeach
													</tbody>
													</table>
												</div>
											</div>
											<!-- Latar belakang -->
											<br>
											<b>Latar Belakang Pendidikan Formal / Informal</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<div class="form-group row">
												<table class="table">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tingkat</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Nama Sekolah</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tempat/Kota</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Jurusan</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tahun</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Lulus/Tidak</td>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
														</tr>
													</thead>
													<tbody class="table table-bordered">
														@foreach($dpp_lb as $data)
														<tr>
															<td><input type="text" class="form-control form-control-sm" readonly value="{{$data->tingkat_pendidikan}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->nama_sekolah}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->tempat_kota}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->jurusan}}"></td>
															<td colspan="2">
																<td><input type="text" class="form-control form-control-sm" value="{{$data->tahun_masuk}}"></td>
																<td><input type="text" class="form-control form-control-sm" value="{{$data->tahun_keluar}}"></td>
															</td>
															<td>
																<div class="container form-group row">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="status_kelulusan" id="inlineRadio1" value="L" {{ $data->status_kelulusan == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="status_kelulusan" id="inlineRadio2" value="T" {{ $data->status_kelulusan == 'T' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">T</label>
																	</div>
																</div>
															</td>
														</tr>
														@endforeach
														
													</tbody>
												</table>
												</div>

												</div>
											</div>
											<br>
											<b>Kursus/Pelatihan yang pernah diikuti</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<div class="form-group row">
												<table class="table table-bordered">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bidang/Jenis</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Penyelenggara</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tempat/Kota</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Lama Kursus</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Dibiayai Oleh</th>
														</tr>
													</thead>
													<tbody>
													@foreach($dpp_ku as $data)
														<tr>
															<td><input type="text" class="form-control form-control-sm">{{$data->bidang_pelatihan}}</td>
															<td><input type="text" class="form-control form-control-sm">{{$data->penyelenggara}}</td>
															<td><input type="text" class="form-control form-control-sm">{{$data->tempat_kota}}</td>
															<td><input type="text" class="form-control form-control-sm">{{$data->lama_pelatihan}}</td>
															<td><input type="text" class="form-control form-control-sm">{{$data->tahun_pelatihan}}</td>
															<td><input type="text" class="form-control form-control-sm">{{$data->dibiayai_oleh}}</td>
														</tr>
													@endforeach
													</tbody>
												</table>
												</div>

												</div>
											</div>
											<br>
											<b>Pengetahuan Bahasa Asing</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<div class="form-group row">
												<table class="table table-bordered">
													<thead>
														
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bahasa</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Mendengar</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Membaca</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Berbicara</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Menulis</th>
														</tr>
														
													</thead>
													<tbody>
													@foreach($dpp_bah as $data_bah)
														<tr>
															<th scope="row">
																<input type="text" value="{{$data->bahasa}}" name="bahasa" class="form-control form-control-sm">
															</th>
															<td>
																<div class="col-sm-10">
																	<select name="mendengar" id="name_p" class="form-control form-control-sm">
																		<option value="Kurang" disabled {{ $data->mendengar == 'Kurang' ? 'selected' : NULL }}>Kurang</option>
																		<option value="Baik"  disabled {{ $data->mendengar == 'Baik' ? 'selected' : NULL }}>Baik</option>
																		<option value="Sangat Baik" disabled {{ $data->mendengar == 'Sangat Baik' ? 'selected' : NULL }}>Sangat Baik</option>
																	</select>	
																</div>
															</td>
															<td>
																<div class="col-sm-10">
																	<select name="membaca" id="name_p" class="form-control form-control-sm">
																		<option value="Kurang" disabled {{ $data->membaca == 'Kurang' ? 'checked' : NULL }}>Kurang</option>
																		<option value="Baik" data-kode ="" disabled {{ $data->membaca == 'Baik' ? 'checked' : NULL }}>Baik</option>
																		<option value="Sangat Baik" data-kode ="" disabled {{ $data->membaca == 'Sangat Baik' ? 'checked' : NULL }}>Sangat Baik</option>
																	</select>	
																</div>
															</td>
															<td>
																<div class="col-sm-10">
																	
																	<select name="berbicara" id="name_p" class="form-control form-control-sm">
																		<option value="Kurang" disabled {{ $data->membaca == 'Kurang' ? 'checked' : NULL }}>Kurang</option>
																		<option value="Baik" data-kode ="" disabled {{ $data->membaca == 'Baik' ? 'checked' : NULL }}>Baik</option>
																		<option value="Sangat Baik" data-kode ="" disabled {{ $data->membaca == 'Sangat baik' ? 'checked' : NULL }}>Sangat Baik</option>
																	</select>	
																</div>
															</td>
															<td>
																<div class="col-sm-10">
																	<select name="menulis" id="name_p" class="form-control form-control-sm">
																		<option value="Kurang" disabled {{ $data->membaca == 'Kurang' ? 'checked' : NULL }}>Kurang</option>
																		<option value="Baik" data-kode ="" disabled {{ $data->membaca == 'Baik' ? 'checked' : NULL }}>Baik</option>
																		<option value="Sangat Baik" data-kode ="" disabled {{ $data->membaca == 'Sangat baik' ? 'checked' : NULL }}>Sangat Baik</option>
																	</select>	
																</div>
															</td>
														</tr>
													@endforeach
													</tbody>
												</table>
												</div>

												</div>
											</div>
											<br>
											<b>Kegiatan Sosial</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<div class="form-group row">
												<table class="table table-bordered">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Organisasi</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jenis Kegiatan</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jabatan</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
														</tr>
														
													</thead>
													<tbody>
														@foreach($dpp_o as $data)
														<tr>
															<th scope="row"><input type="text" class="form-control form-control-sm" value="{{$data->nama_organisasi}}">{{$data->nama_organisasi}}</th>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->jenis_organisasi}}">{{$data->jenis_organisasi}}</td>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->jabatan}}">{{$data->jabatan}}</td>
															<td><input type="text" class="form-control form-control-sm" value="{{$data->tahun}}">{{$data->tahun}}</td>
														</tr>
														@endforeach
													</tbody>
												</table>
												</div>

												</div>
											</div>
											<br>
											<b>Hobby dan Kegiatan Waktu Luang</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<textarea name="hoby_dan_kegiatan" class="form-control" id="exampleFormControlTextarea1" value="{{$data->hoby_dan_kegiatan}}" cols="30" rows="5" placeholder="Hoby Saya ...">{{$data->hoby_dan_kegiatan}}</textarea>
												</div>

												</div>
											</div>
											<br>
											<b>Kegiatan Membaca</b>
											<hr>
											<div class="row">
												<div class="col">
												<p>Dibanding seluruh aktifitas waktu yang anda luangkan</p>
												<textarea name="pokok_yang_dibaca" class="form-control" value="{{$data->pokok_yang_dibaca}}" readonly id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Dibanding seluruh aktifitas waktu yang anda luangkan ...">{{$data->pokok_yang_dibaca}}</textarea>
												<p></p>
												</div>
												<div class="col">
												<p>Pokok-pokok yang dibaca</p>
												<textarea name="pokok_yang_dibaca" class="form-control" value="{{$data->pokok_yang_dibaca}}" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Pokok-pokok yang dibaca ...">{{$data->pokok_yang_dibaca}}</textarea>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<br>
											<b>Media yang dibaca</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
												<table>
													<tr>
														<td>Surat Kabar</td>
														<td>:</td>
														<td><input type="text" name="surat_kabar" value="{{$data->surat_kabar}}" class=""></td>
													</tr>
													<tr>
														<td>Majalah</td>
														<td>:</td>
														<td><input type="text" name="majalah" value="{{$data->majalah}}" class=""></td>
													</tr>
												</table>
												</div>
											</div>
											<br>
											<b>Riwayat Pekerjaan (mulai dari pekerjaan sekarang s/d terakhir)</b>
											<hr>
											<p>*isi tanda "-" jika belum memiliki pengalaman kerja atau magang</p>
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<table>
														<tr>
															<td>Nama Perusahaan</td>
															<td>:</td>
															<td><input type="text" name="nama_perusahaan" value="{{$data->nama_perusahaan}}" class=""></td>
														</tr>
														<tr>
															<td>Alamat</td>
															<td>:</td>
															<td><input type="text" name="alamat" value="{{$data->alamat}}"></td>
														</tr>
														<tr>
															<td>Lama Bekerja</td>
															<td>:</td>
															<td><input type="text" name="lama_bekerja" value="{{$data->lama_bekerja}}"></td>
														</tr>
														<tr>
															<td>Nama Atasan Langsung</td>
															<td>:</td>
															<td><input type="text" name="nama_atasan" value="{{$data->nama_atasan}}"></td>
														</tr>
														<tr>
															<td>Gaji Terakhir</td>
															<td>:</td>
															<td><input type="text" name="gaji_terakhir" value="{{$data->gaji_terakhir}}"></td>
														</tr>
														<tr>
															<td>Jenis Usaha</td>
															<td>:</td>
															<td><input type="text" name="jenis_usaha" value="{{$data->jenis_usaha}}"></td>
														</tr>
														<tr>
															<td>Alasan Berhenti</td>
															<td>:</td>
															<td><input type="text" name="alasan_berhenti" value="{{$data->alasan_berhenti}}"></td>
														</tr>
													</table>
												</div>
												<div class="col" style="font-size: 10px;">
													<table>
														<tr>
															<td>Jabatan Awal</td>
															<td>:</td>
															<td><input type="text" name="jabatan_awal" value="{{$data->jabatan_awal}}"></td>
														</tr>
														<tr>
															<td>Jabatan Akhir</td>
															<td>:</td>
															<td><input type="text" name="jabatan_akhir" value="{{$data->jabatan_akhir}}"></td>
														</tr>
														<tr>
															<td>Telp Perusahaan</td>
															<td>:</td>
															<td><input type="text" name="telp_perusahaan" value="{{$data->telp_perusahaan}}"></td>
														</tr>
														<tr>
															<td>Nama Direktur</td>
															<td>:</td>
															<td><input type="text" name="nama_direktur" value="{{$data->nama_direktur}}"></td>
														</tr>
														
													</table>
												</div>
											</div>
											<br>
											<b>Referensi (Kepada siapa kami dapat menanyakan diri anda lebih lengkap)</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th>NAMA</th>
																<th>ALAMAT/TELEPON</th>
																<th>PEKERJAAN</th>
																<th>HUBUNGAN</th>
															</tr>
														</thead>
														<tbody>
															@foreach($dpp_kr as $dpp_kr)
															<tr>
																<td><input value="{{$dpp_kr->nama_lengkap}}" type="text" class="form-control form-control-sm"></td>
																<td><input value="{{$dpp_kr->alamat_telpon}}" type="text" class="form-control form-control-sm"></td>
																<td><input value="{{$dpp_kr->pekerjaan}}" type="text" class="form-control form-control-sm"></td>
																<td><input value="{{$dpp_kr->hubungan}}" type="text" class="form-control form-control-sm"></td>
															</tr>
															@endforeach
														</tbody>
														
													</table>
												</div>
											</div>
											<br>
											<b>Orang yang dapat dihubungi dalam keadaan mendesak</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<table class="table table-bordered">
														<thead>
														
															<tr>
																<th>NAMA</th>
																<th>ALAMAT/TELEPON</th>
																<th>PEKERJAAN</th>
																<th>HUBUNGAN</th>
															</tr>
														
														</thead>
														<tbody>
														@foreach($dpp_kd as $dpp_kr)
															<tr>
																<td><input type="text" value="{{$dpp_kd->nama_lengkap}}" class="form-control form-control-sm"></td>
																<td><input type="text" value="{{$dpp_kd->alamat_telpon}}" class="form-control form-control-sm"></td>
																<td><input type="text" value="{{$dpp_kd->pekerjaan}}" class="form-control form-control-sm"></td>
																<td><input type="text" class="form-control form-control-sm" value="{{$dpp_kd->hubungan}}"></td>
															</tr>
														@endforeach
														</tbody>
													</table>
												</div>
											</div>
											<br>
											<b>Uraikan tugas & tanggung jawab Anda pada jabatan yang terakhir</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<textarea name="tugas_jabatan_terakhir" class="form-control" value="{{$data->tugas_jabatan_terakhir}}"  id="" cols="30" rows="7" placeholder="Tugas saya adalah..........">{{$data->tugas_jabatan_terakhir}}</textarea>
													<br>*isi Fresh graduate jika belum miliki pengalaman
												</div>
											</div>
											<br>
											<b>Uraikan hal-hal positif yang menjadi kekuatan anda & hal negatif dalam diri anda yang ingin anda kembangkan</b>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<table class="" style="border:none;">
														<thead style="border:1px solid black;">
															<tr>
																<th></th>
																<th>Hal-hal Positif</th>
																<th></th>
																<th></th>
																<th>Hal-hal Negatif</th>
															</tr>
														</thead>
														<tbody style="border:1px solid black;">
														@php $b=1; @endphp
														@foreach($uraian as )
															<tr>
																<td>{{$b++;}}</td>
																<td><input type="text" value="{{$data->}}" class="form-control form-control-sm"></td>
																<td></td>
																<td>1</td>
																<td><input type="text" value="{{$data->}}" class="form-control form-control-sm"></td>
															</tr>
															
														@endforeach
														</tbody>
													</table>
												</div>
											</div>
											<br>
											<b>Gambarkan secara jelas denah tempat tinggal anda dengan patokan jalan utama</b><br>(digambar pada saat anda diundang interview)
											<hr>
											<a href="" download class="btn btn-primary btn-sm">Download Gambar Denah Tempat Tinggal</a>
											<br>
											<div style="width: 100%;text-align:center;">
												<table>
												<tr>
													<td>
														<a href="{{ URL::to('/list_dpp/edit/'.$data->id_data_pelamar) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
													</td>
													
													<td>
														<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_dpp', $data->id_data_pelamar)}}"  id="btn_delete">Hapus</a>
													</td>
													<td>
														<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
													</td>
												</tr>
												</table>
											</div>
											<hr>
											<div class="row">
												<div class="col" style="font-size: 14px;">
													<table class="table table-bordered">
														<thead style="border:1px solid black;">
															<tr>
																<th rowspan="2" style="text-align:center;">No</th>
																<th rowspan="2" style="text-align:center;">PERNYATAAN</th>
																<th colspan="3" style="text-align:center;">JAWABAN</th>
																
															</tr>
															<tr>
																<th rowspan="2" style="text-align:center;">Ya</th>
																<th rowspan="2" style="text-align:center;">Tidak</th>
																<th colspan="3" style="text-align:center;">Penjelasan</th>
															</tr>
														</thead>
														<tbody style="border:1px solid black;">
														@foreach($dpp)
															<tr>
																<td>1</td>
																<td>Apakah anda pernah melamar di group /perusahaan ini sebelumnya? Kapan dan sebagai apa?</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="status_perkawinan"  value="YA" {{ $data->status_perkawinan == 'YA' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="status_perkawinan"  value="TIDAK" {{ $data->status_perkawinan == 'TIDAK' ? 'checked' : NULL }}>
																	</td>
																</div>
																<td>
																	<input type="text" class="form-control" name="penjelasan" value="">																
																</td>
															</tr>
														@endforeach	
														</tbody>
													</table>
													<div class="row">
														<div class="col" style="border:1px solid black;">
															Dengan ini, saya menyatakan bahwa keterangan yang saya berikan diatas benar isinya. Dengan mendatangani formulir ini 
															saya menyatakan bersedia untuk diberhentikan apabila yang saya tuliskan dalam formulir lamaran ini tidak benar.
														</div>
														<div class="col">
															<label for="">Tempat Tanda Tangan</label>
															<input type="text" readonly disabled name="{{$data->tempat_ttd}}">
															<input type="text" disabled name="{{$data->tanggal_lamar}}">
															<label for="">Nama Lengkap</label>
															<input type="text" readonly disabled name="{{$data->tempat_ttd}}">
															
														</div>
													</div>
												</div>
											</div>	
											<br>
											<div style="width: 100%;text-align:center;">
												<table>
												<tr>
													<td>
														<a href="{{ URL::to('/list_dpp/edit/'.$data->id_data_pelamar) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
													</td>
													
													<td>
														<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_dpp', $data->id_data_pelamar)}}"  id="btn_delete">Hapus</a>
													</td>
													<td>
														<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
													</td>
												</tr>
												</table>
											</div>
											<br>
											<b>MARSTON MODEL INDONESIA (MMI)</b>

											<table>
												<tr>
													<td>Nama Lengkap</td>
													<td style="border:1px solid black;">
														</input name="nama_lengkap" value="{{$data->nama_lengkap}}">
													</td>
												</tr>
											</table><br>
											<div class="row">
												<div class="col">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th style="text-align:center;">P</th>
																<th style="text-align:center;">K</th>
																<th style="text-align:center;">Contoh</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>

																</div>
																
																<td>Mudah bergaul, ramah</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																<td>Penuh kepercayaan, percaya kpd orang lain</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Petualang, pengambil resiko</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Toleran, penuh hormat</td>
															</tr>
														</tbody>
													</table>

												</div>
												<div class="col">

													PETUNJUK: Bayangkan anda berada dalam salah satu setting lingkungan(kerja, keluarga, sekolah atau lainnya) 
													sesuai dengan tujuan pemeriksaan. Tugas anda membaca 4 kalimat yang terdapat di dalam
													masing-masing kotak. Klik pilihan pada kolom "P" kalimat yang PALING menggambarkan diri anda dalam setting yang sudah ditentukan tersebut.
													Kemudian klik pada kolom "K" disamping kalimat yang KURANG menggambarkan diri anda dalam setting tersebut, untuk masing-masing kotak,
													pilih satu respon PALING atau KURANG.
												</div>
											</div>
											<div class="row">
												<div class="col">
												<table class="table table-bordered">
														<thead>
															<tr>
																<th style="text-align:center;">P</th>
																<th style="text-align:center;">K</th>
																<th style="text-align:center;">Contoh</th>
																<th style="text-align:center;">P</th>
																<th style="text-align:center;">K</th>
																<th style="text-align:center;">Contoh</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																<td>Mudah bergaul, ramah</td>

																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>

																</div>
																
																<td>Yang penting adalah hasil</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Penuh kepercayaan, percaya kpd orang lain</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Melakukan dengan benar, ketepatan dihitung</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Petualangan, percaya kpd orang lain</td>
													
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Buat menjadi menyenangkan</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Toleran, penuh hormat</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																
																<td>Mari melakukan bersama</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																	<b>
																		P
																	</b>
																		<!-- <input class="form-check-input" type="radio" name="status_perkawinan"  value="menikah" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}> -->
																	</td>
																	<td style="text-align:center;">
																	<b>
																		K
																	</b>
																		<!-- <input class="form-check-input" type="radio" name="status_perkawinan" value="janda/duda" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}> -->
																	</td>
																</div>
																
																<td></td>
																<div class="form-check" >
																	<td style="text-align:center;">
																	<b>
																	P
																	</b>
																		<!-- <input class="form-check-input" type="radio" name="status_perkawinan"  value="menikah" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}> -->
																	</td>
																	<td style="text-align:center;">
																	<b>
																	K
																	</b>
																		<!-- <input class="form-check-input" type="radio" name="status_perkawinan" value="janda/duda" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}> -->
																	</td>
																</div>
																
																<td></td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>

																</div>
																
																<td>Lembut, pendiam</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>	
																<td>Akan melakukan tanpa kontrol</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>

																</div>
																
																<td>Optimis, pengkhayal</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>	
																<td>Akan membeli berdasarkan hasrat</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>

																</div>
																
																<td>Pusat perhatian, mudah bersosialisasi</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>	
																<td>Akan menunggu, tidak ada tekanan</td>
															</tr>
															<tr>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->jawaban == 'p' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->jawaban == 'k' ? 'checked' : NULL }}>
																	</td>
																</div>
																<td>Pembuat perdamaian, membawa ketenangan</td>
																<div class="form-check" >
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban"  value="p" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}>
																	</td>
																	<td style="text-align:center;">
																		<input class="form-check-input" type="radio" name="jawaban" value="k" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}>
																	</td>
																</div>	
																<td>Akan membelanjakan apa yang saya inginkan</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>



										</div>
										
										
									</div>
									
								</div>
								<hr>
								<div style="width: 100%;">
									<table>
									<tr>
										<td>
											<a href="{{ URL::to('/list_dpp/edit/'.$data->id_data_pelamar) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
										</td>
										<td>
											<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
										</td>
										<td>
											<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_dpp', $data->id_data_pelamar)}}"  id="btn_delete">Hapus</a>
										</td>
									</tr>
									</table>
								</div>
								<br>
							@endforeach
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection