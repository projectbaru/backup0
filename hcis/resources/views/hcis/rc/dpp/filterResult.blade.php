@extends('re.cv.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Data Personal Pelamar</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">recruitment</a></li>
                                                <li class="breadcrumb-item"><a href="#!">data personal pelamar</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xl-12">
                                    <div class="card" style="border-radius:8px;border:none;">
                                        <div class="card-header">
                                            <!-- <h5>Per Table</h5> -->
                                            <a href="{{route('ubah_tamalamat')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                        </div>
                                        <div class="card-body table-border-style">
                                            <div class="table-responsive-xl"  style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                            <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
                                                @csrf
                                                
                                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9; ">
                                                        <thead style="color:black;font-size:12px;">
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                                                            @for($i = 0; $i < count($th); $i++)
                                                                @if($th[$i] == 'kode_cv_pelamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode CV Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'kode_data_pelamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'posisi_lamaran')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Pekerjaan</th>
                                                                @endif
                                                                @if($th[$i] == 'nama_lengkap')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'nama_panggilan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Resume CV</th>
                                                                @endif
                                                                @if($th[$i] == 'tempat_lahir')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lengkap</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_lahir')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Email</th>
                                                                @endif
                                                                @if($th[$i] == 'agama')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No Hp</th>
                                                                @endif
                                                                @if($th[$i] == 'kebangsaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'jenis_kelamin')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'berat_badan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lama Pengalaman Bekerja</th>
                                                                @endif
                                                                @if($th[$i] == 'tinggi_badan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Lahir</th>
                                                                @endif
                                                                @if($th[$i] == 'no_ktp')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Kelamin</th>
                                                                @endif
                                                                
                                                                @if($th[$i] == 'email_pribadi')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pendidikan Tertinggi</th>
                                                                @endif
                                                                @if($th[$i] == 'alamat_sosmed')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Sekolah</th>
                                                                @endif
                                                                @if($th[$i] == 'kategori_sim')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Program Studi Jurusan</th>
                                                                @endif
                                                                @if($th[$i] == 'no_sim')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Wisuda</th>
                                                                @endif
                                                                @if($th[$i] == 'no_hp')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nilai Rata-rata</th>
                                                                @endif
                                                                @if($th[$i] == 'no_telepon')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nilai Skala</th>
                                                                @endif 
                                                                @if($th[$i] == 'status_kendaraan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Informasi Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'jenis_merk_kendaraan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan Informasi Pilihan</th>
                                                                @endif
                                                                @if($th[$i] == 'tempat_tinggal')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pesan Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_melamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'status_perkawinan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'alamat_didalam_kota')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'kode_pos_1')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'alamat_diluar_kota')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'kode_pos_2')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'pas_foto')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'hoby_dan_kegiatan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'kegiatan_membaca')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'surat_kabar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'majalah')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'pokok_yang_dibaca')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'tugas_jabatan_terakhir')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_lamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'gambar_denah')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'tempat_ttd')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'tanda_tangan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'status_rekaman')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                @if($th[$i] == 'waktu_user_input')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                             
                                                                @if($i == count($th) - 1)
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                                @endif
                                                            @endfor
                                                        </thead>
                                                        <tbody style="font-size:11px;">
                                                            @php $b = 1; @endphp  @foreach($query as $row)
                                                                <tr>
                                                                    <td>{{$b++}}</td>
                                                                    @for($i = 0; $i < count($th); $i++)
                                                                        @if($th[$i] == 'kode_cv_pelamar')
                                                                            <td>{{ $row->kode_cv_pelamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kode_data_pelamar')
                                                                            <td>{{ $row->kode_data_pelamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'posisi_lamaran')
                                                                            <td>{{ $row->posisi_lamaran ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nama_lengkap')
                                                                            <td>{{ $row->nama_lengkap ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nama_panggilan')
                                                                            <td>{{ $row->nama_panggilan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tempat_lahir')
                                                                            <td>{{ $row->tempat_lahir ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_lahir')
                                                                            <td>{{ $row->tanggal_lahir ?? 'NO DATA', $row->tanggal_lahir ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'agama')
                                                                            <td>{{ $row->agama ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kebangsaan')
                                                                            <td>{{ $row->kebangsaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jenis_kelamin')
                                                                            <td>{{ $row->jenis_kelamin ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'berat_badan')
                                                                            <td>{{ $row->berat_badan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tinggi_badan')
                                                                            <td>{{ $row->tinggi_badan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'no_ktp')
                                                                            <td>{{ $row->no_ktp ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'email_pribadi')
                                                                            <td>{{ $row->email_pribadi ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'alamat_sosmed')
                                                                            <td>{{ $row->alamat_sosmed ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kategori_sim')
                                                                            <td>{{ $row->kategori_sim ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'no_sim')
                                                                            <td>{{ $row->no_sim ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'no_hp')
                                                                            <td>{{ $row->no_hp ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'no_telepon')
                                                                            <td>{{ $row->no_telepon ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_kendaraan')
                                                                            <td>{{ $row->status_kendaraan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jenis_merk_kendaraan')
                                                                            <td>{{ $row->jenis_merk_kendaraan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tempat_tinggal')
                                                                            <td>{{ $row->tempat_tinggal ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_perkawinan')
                                                                            <td>{{ $row->status_perkawinan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'alamat_didalam_kota')
                                                                            <td>{{ $row->alamat_didalam_kota ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kode_pos_1')
                                                                            <td>{{ $row->kode_pos_1 ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'alamat_diluar_kota')
                                                                            <td>{{ $row->alamat_diluar_kota ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kode_pos_2')
                                                                            <td>{{ $row->kode_pos_2 ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pas_foto')
                                                                            <td>{{ $row->pas_foto ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'hoby_dan_kegiatan')
                                                                            <td>{{ $row->hoby_dan_kegiatan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kegiatan_membaca')
                                                                            <td>{{ $row->kegiatan_membaca ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'surat_kabar')
                                                                            <td>{{ $row->surat_kabar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'majalah')
                                                                            <td>{{ $row->majalah ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pokok_yang_dibaca')
                                                                            <td>{{ $row->pokok_yang_dibaca ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tugas_jabatan_terakhir')
                                                                            <td>{{ $row->tugas_jabatan_terakhir ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_lamar')
                                                                            <td>{{ $row->tanggal_lamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'gambar_denah')
                                                                            <td>{{ $row->gambar_denah ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tempat_ttd')
                                                                            <td>{{ $row->tempat_ttd ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanda_tangan')
                                                                            <td>{{ $row->tanda_tangan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_rekaman')
                                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'waktu_user_input')
                                                                            <td>{{ $row->waktu_user_input ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                       
                                                                        @if($i == count($th) - 1)
                                                                            <td>
                                                                                <a href="{{ URL::to('/list_dpp/detail/'.$row->id_data_pelamar) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                <a href="{{ URL::to('/list_dpp/edit/'.$row->id_data_pelamar) }}" class="">Edit</a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_data_pelamar }}">
                                                                            </td>
                                                                        @endif
                                                                    @endfor
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_dpp')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                            <td>
                                                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                            </form>
                                
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection