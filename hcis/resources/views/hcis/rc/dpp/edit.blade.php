@extends('re.personal.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Edit Data CV Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($dpp)
							<input type="hidden" name="temp_id" id="temp_id" value="" />

							<div class="form-group" style="width:95%;">
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
										<div class="row">
											<div class="col-md-6">
											<b>Data Pribadi</b>

											</div>
											<div class="col-md-6">Halaman 1-0</div>
										</div>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Pelamar</label>
													<!-- <div class="col-sm-9">
													<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div> -->
													<div class="col-sm-9">
														<input type="text" required name="kode_cv_pelamar" value="{{$data->kode_cv_pelamar}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Posisi yang dilamar</label>
													<div class="col-sm-9">
														<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
															<input type="text" required name="posisi_lamaran" value="{{$data->posisi_lamaran}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
											
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama lengkap (KTP)</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_lengkap" value="{{$data->nama_lengkap}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama panggilan</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_panggilan" value="{{$data->nama_panggilan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
			
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat & Tgl lahir</label>
													<div class="col-sm-9">
														<div class="row">
														<div class="col-sm-4">
															<input type="text" required name="tempat_lahir" value="{{$data->tempat_lahir}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
														</div>	
														<div class="col-sm-4">
															<input type="date" required name="tanggal_lahir" value="{{$data->tanggal_lahir}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
														</div>	
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Agama</label>
														<div class="col-sm-9">
															<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
															<select name="agama" id="agama" required class="form-control form-control-sm" style="font-size:11px;">
																<option value="islam" {{ $data->agama == 'islam' ? 'selected' : NULL }}>Islam</option>
																<option value="kristen" {{ $data->agama == 'kristen' ? 'selected' : NULL }}>Kristen</option>
																<option value="budha" {{ $data->agama == 'budha' ? 'selected' : NULL }}>Budha</option>
																<option value="hindu" {{ $data->agama == 'hindu' ? 'selected' : NULL }} >Hindu</option>
																<option value="konghucu"  {{ $data->agama == 'konghucu' ? 'selected' : NULL }}>Kong Hu Cu</option>
															</select>	
														</div>		
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kebangsaan</label>
														<div class="col-sm-9">
															<input type="text" required name="kebangsaan" value="{{$data->kebangsaan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
														</div>		
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
														<div class="col-sm-9">
															<div class="form-check">
																<input class="form-check-input" required type="radio" name="jenis_kelamin"  value="laki-laki" {{ $data->jenis_kelamin == 'laki-laki' ? 'checked' : NULL }} >
																<label class="form-check-label" for="flexRadioDefault1">
																	Laki-laki
																</label>
															</div>&nbsp;&nbsp;&nbsp;
															<div class="form-check">
																<input class="form-check-input" required type="radio" name="jenis_kelamin"  value="perempuan" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
																<label class="form-check-label" for="flexRadioDefault1">
																	Perempuan
																</label>
															</div>
														</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">BB & TB</label>
														<div class="col-sm-4">
															<input type="text" required name="berat_badan" value="{{$data->berat_badan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Kg
														</div>	
														<div class="col-sm-4">
															<input type="text" required name="tinggi_badan" value="{{$data->tinggi_badan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Cm
														</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kendaraan</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input" type="radio" name="status_kendaraan" value="milik sendiri" readonly {{ $data->jenis_kelamin == 'milik sendiri' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Milik Sendiri
															</label>
														</div>&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" type="radio" name="status_kendaraan" value="orang tua" readonly {{ $data->jenis_kelamin == 'orang tua' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Orang Tua
															</label>
														</div>&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" type="radio" name="status_kendaraan"  value="kantor" readonly {{ $data->jenis_kelamin == 'kantor' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Kantor
															</label>
														</div>
														&nbsp;&nbsp;&nbsp;
														<div class="form-check">
															<input class="form-check-input" type="radio" name="status_kendaraan"  value="lainnya" readonly {{ $data->jenis_kelamin == 'lainnya' ? 'checked' : NULL }}>
															<label class="form-check-label" for="flexRadioDefault1">
																Lainnya
															</label>
														</div>
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis kendaraan/merk</label>
													<div class="col-sm-9">
														<input type="text" required name="jenis_merk_kendaraan" value="{{$data->jenis_merk_kendaraan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat didalam kota(Lengkap)</label>
													<div class="col-sm-9">
														<textarea type="text" required name="alamat_didalam_kota" value="{{$data->alamat_didalam_kota}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
													<div class="col-sm-9">
														<input type="text" required name="kode_pos" value="{{$data->kode_pos}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.KTP</label>
													<div class="col-sm-9">
														<input type="text" required name="no_ktp" value="{{$data->no_ktp}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
													<div class="col-sm-9">
														<input type="email" required name="email" value="{{$data->email}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat Sosmed</label>
													<div class="col-sm-9">
														<input type="text" required name="alamat_sosmed" value="{{$data->alamat_sosmed}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">SIM yang dimiliki</label>
														<div class="col-sm-4">
															<select name="kategori_sim" id="kategori_sim" class="form-control form-control-sm">
																<option value="None" {{ $data->jenis_kelamin == 'None' ? 'selected' : NULL }}>None</option>
																<option value="SIM A" {{ $data->jenis_kelamin == 'SIM A' ? 'selected' : NULL }}>SIM A</option>
																<option value="SIM B" {{ $data->jenis_kelamin == 'SIM B' ? 'selected' : NULL }}>SIM B</option>
																<option value="SIM B1" {{ $data->jenis_kelamin == 'SIM B1' ? 'selected' : NULL }}>SIM B1</option>
																<option value="SIM B2" {{ $data->jenis_kelamin == 'SIM B2' ? 'selected' : NULL }}>SIM B2</option>
																<option value="SIM C" {{ $data->jenis_kelamin == 'SIM C' ? 'selected' : NULL }}>SIM C</option>
																<option value="SIM D" {{ $data->jenis_kelamin == 'SIM D' ? 'selected' : NULL }}>SIM D</option>
															</select>		
														</div>
														<div class="col-sm-4">
															<input type="text" required name="no_sim" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >No
														</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No HP</label>
													<div class="col-sm-9">
														<input type="text" required name="no_hp" value="{{$data->no_hp}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No Telepon</label>
													<div class="col-sm-9">
														<input type="text" required name="no_telepon" value="{{$data->no_telepon}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No Telepon</label>
													<div class="col-sm-9">
														<input type="text" required name="no_telepon" value="{{$data->no_telepon}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" >
													</div>	
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat tinggal</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input"  type="radio" name="tempat_tinggal"  value="milik sendiri" {{ $data->tempat_tinggal == 'milik sendiri' ? 'checked' : NULL }}>Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="tempat_tinggal" value="ikut orangtua" {{ $data->tempat_tinggal == 'ikut orangtua' ? 'checked' : NULL }}>Kost/Kontrak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="tempat_tinggal" value="kost/kontrak" {{ $data->tempat_tinggal == 'kost/kontrak' ? 'checked' : NULL }}>Ikut Orangtua
														</div>
													</div>
												</div>	
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status perkawinan</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input"  type="radio" name="status_perkawinan"  value="Menikah" {{ $data->status_perkawinan == 'Menikah' ? 'checked' : NULL }}>Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="status_perkawinan" value="Janda/Duda" {{ $data->status_perkawinan == 'Janda/Duda' ? 'checked' : NULL }}>Janda/Duda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="status_perkawinan" value="Tidak Menikah" {{ $data->status_perkawinan == 'Tidak Menikah' ? 'checked' : NULL }}>Tidak Menikah
														</div>
													</div>
												</div>	
											</div>
										</div>
										<hr>
										<b>Susunan Keluarga (termasuk pelamar)</b>
										<div class="row">
											<div class="col" style="font-size: 8px;">
												<table class="table" style="font-size: 8px;">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" ></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col" colspan="2" style="border:1px solid #dee2e6;">Pekerjaan Terakhir</th>
															<th scope="col"></th>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Hubungan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Nama</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Laki-laki/Perempuan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Usia(Tahun Lahir)</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Pendidikan Terakhir</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Jabatan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Perusahaan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Keterangan</th>
														</tr>
													</thead>
													<tbody class="table table-bordered" style="">
													@foreach($dpp_k as $data_k)
														<tr>
															<td><input type="text"  name="hubungan_keluarga" value="{{$data_k->hubungan_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" required name="nama_keluarga" value="{{$data_k->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" required name="jenis_kelamin" value="{{$data_k->jenis_kelamin}}" class="form-control form-control-sm"></td>
															<td><input type="text" required value="usia_tahun_lahir" name="{{$data_k->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select name="{{$data_k->pendidikan_terakhir}}" required class="form-control form-control-sm" style="font-size:11px;">
																		<option value="SD" {{ $data_k->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP" {{ $data_k->pendidikan_terakhir == 'SMP' ? 'selected' : NULL }}>SMP</option>
																		<option value="D1" {{ $data_k->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data_k->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data_k->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data_k->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data_k->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data_k->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data_k->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data_k->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data_k->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
										<hr>
										<b>Latar Belakang Pendidikan Formal / Informal</b>
										<br>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<table class="table">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tingkat</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Nama Sekolah</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tempat/Kota</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Jurusan</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tahun</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">Lulus/Tidak</td>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
														</tr>
													</thead>
													<tbody class="table table-bordered">
														@foreach($dpp_lb as $dpp_lb)
														<tr>
															<td><input type="text" class="form-control form-control-sm" readonly value="{{$dpp_lb->tingkat_pendidikan}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_lb->nama_sekolah}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_lb->tempat_kota}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_lb->jurusan}}"></td>
															<td colspan="2">
																<td><input type="text" class="form-control form-control-sm" value="{{$dpp_lb->tahun_masuk}}"></td>
																<td><input type="text" class="form-control form-control-sm" value="{{$dpp_lb->tahun_keluar}}"></td>
															</td>
															<td>
																<div class="container form-group row">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="status_kelulusan" id="inlineRadio1" value="L" {{ $dpp_lb->status_kelulusan == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="status_kelulusan" id="inlineRadio2" value="T" {{ $dpp_lb->status_kelulusan == 'T' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">T</label>
																	</div>
																</div>
															</td>
														</tr>
														@endforeach
														
													</tbody>
												</table>
											</div>
											</div>
										</div>
										<hr>
										<b style="font-size:11px;">Kursus/Pelatihan yang pernah diikuti</b>
										<br>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<table class="table">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">BIDANG/JENIS</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">PENYELENGGARA</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">TEMPAT/KOTA</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">LAMA KURSUS</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">TAHUN</td>
															<td scope="col" style="border:1px solid #dee2e6;" rowspan="2">DIBIAYAI OLEH</td>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
															<th scope="col" style="border:1px solid #dee2e6;"></th>
														</tr>
													</thead>
													<tbody class="table table-bordered">
														@foreach($dpp_ku as $dpp_ku)
														<tr>
															<td><input type="text" class="form-control form-control-sm" readonly value="{{$dpp_ku->bidang_pelatihan}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_ku->penyelenggara}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_ku->tempat_kota}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_ku->lama_pelatihan}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_ku->tahun_pelatihan}}"></td>
															<td><input type="text" class="form-control form-control-sm" value="{{$dpp_ku->dibiayai_oleh}}"></td>
														</tr>
														@endforeach
														
													</tbody>
												</table>
											</div>
											</div>
										</div>
										<hr>
										<b>Pengetahuan Bahasa Asing</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="bahasa">
											<table class="table table-bordered">
												<thead>
													
													<tr style="border:1px solid #dee2e6;">
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2" >Bahasa</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Mendengar</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Membaca</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Berbicara</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Menulis</th>
													</tr>
													
												</thead>
												<tbody>
													@foreach($dpp_bah as $dpp_bah)
													<tr>
														<td scope="row">
															<input type="text" required class="form-control form-control-sm" value="{{$dpp_bah->bahasa}}" name="bahasa">
														</td>
														<td>
															<div class="col-sm-10">
																<select name="mendengar" required id="mendengar" class="form-control form-control-sm">
																	<option value="kurang"  {{ $dpp_bah->mendengar == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" data-kode ="" {{ $dpp_bah->mendengar == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" data-kode ="" {{ $dpp_bah->mendengar == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="membaca" required id="membaca" class="form-control form-control-sm">
																	<option value="kurang" {{ $dpp_bah->membaca == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" {{ $dpp_bah->membaca == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $dpp_bah->membaca == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="berbicara" required id="berbicara" class="form-control form-control-sm">
																	<option value="kurang" {{ $dpp_bah->berbicara == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" {{ $dpp_bah->berbicara == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $dpp_bah->berbicara == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="menulis" required id="menulis" class="form-control form-control-sm">
																	<option value="kurang" {{ $dpp_bah->menulis == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik"  {{ $dpp_bah->menulis == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $dpp_bah->menulis == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-1">
																<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addBahasaInput">
																	<i class="fa-solid fa-plus"></i>
																</button>
															</div>
														</td>
													</tr>			
													@endforeach											
												</tbody>
											</table>
											</div>

											</div>
										</div>
										<br>
										<hr>
										<b>Kegiatan Sosial</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="bahasa">
											<table class="table table-bordered">
												<thead>
													
													<tr style="border:1px solid #dee2e6;">
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2" >ORGANISASI</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">JENIS KEGIATAN</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">JABATAN</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">TAHUN</th>
													</tr>
													
												</thead>
												<tbody>
													@foreach($dpp_bah as $dpp_bah)
													<tr>
														<td scope="row">
															<input type="text" required class="form-control form-control-sm" value="{{$dpp_bah->nama_organisasi}}" name="nama_organisasi">
														</td>
														<td>
															<input type="text" required class="form-control form-control-sm" value="{{$dpp_bah->jenis_kegiatan}}" name="jenis_kegiatan">
														</td>
														<td>
															<input type="text" required class="form-control form-control-sm" value="{{$dpp_bah->jabatan}}" name="jabatan">
														</td>
														<td>
															<input type="text" required class="form-control form-control-sm" value="{{$dpp_bah->tahun}}" name="tahun">
														</td>
														<td>
															<div class="col-sm-1">
																<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addBahasaInput">
																	<i class="fa-solid fa-plus"></i>
																</button>
															</div>
														</td>
													</tr>			
													@endforeach											
												</tbody>
											</table>
											</div>

											</div>
										</div>
										<br>
										<hr>
										<b>Hoby dan Kegiatan Waktu Luang</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row" id="hoby_dan_kegiatan">
													<textarea name="hoby_dan_kegiatan" id="" cols="30" rows="10" required>{{$data->hoby_dan_kegiatan}}</textarea>
												</div>
											</div>
										</div>
										<br>
										<hr>
										<b>Kegiatan Membaca</b>
										<hr>
										<div class="row">
											<div class="col">
												<p>Dibanding seluruh aktifitas waktu yang anda luangkan</p>
												<select name="kegiatan_membaca" required id="kegiatan_membaca" class="form-control form-control-sm">
													<option value="sedikit">Sedikit</option>
													<option value="sedang">Sedang</option>
													<option value="banyak">Banyak</option>
												</select>	
											</div>
											<div class="col">
												<p>Pokok-pokok yang dibaca</p>
												<textarea name="pokok_yang_dibaca" class="form-control" value="{{$data->pokok_yang_dibaca}}" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Pokok-pokok yang dibaca ...">{{$data->pokok_yang_dibaca}}</textarea>
											</div>
										</div>
										<br>
										<b>Media yang dibaca</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<tr>
														<td>Surat Kabar</td>
														<td>:</td>
														<td><input type="text" required name="surat_kabar" value="{{$data->surat_kabar}}" class=""></td>
													</tr>
													<tr>
														<td>Majalah</td>
														<td>:</td>
														<td><input type="text" required name="majalah" value="{{$data->majalah}}" class=""></td>
													</tr>
												</table>
											</div>
										</div>
										<div style="width: 100%;">
											<table>
												<tr>
													<td>
														<a href="{{route('list_dpp')}}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
													</td>
												</tr>
											</table>
										</div>
										<br>
										<b>Riwayat Pekerjaan (mulai dari pekerjaan sekarang s/d terakhir)</b>
										<hr>
										<p>*isi tanda "-" jika belum memiliki pengalaman kerja atau magang</p>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<tr>
														<td>Nama Perusahaan</td>
														<td>:</td>
														<td><input type="text" required name="nama_perusahaan" value="{{$data->nama_perusahaan}}" class=""></td>
													</tr>
													<tr>
														<td>Alamat</td>
														<td>:</td>
														<td><input type="text" required name="alamat" value="{{$data->alamat}}"></td>
													</tr>
													<tr>
														<td>Lama Bekerja</td>
														<td>:</td>
														<td><input type="text" required name="lama_bekerja" value="{{$data->lama_bekerja}}"></td>
													</tr>
													<tr>
														<td>Nama Atasan Langsung</td>
														<td>:</td>
														<td><input type="text" required name="nama_atasan" value="{{$data->nama_atasan}}"></td>
													</tr>
													<tr>
														<td>Gaji Terakhir</td>
														<td>:</td>
														<td><input type="text" required name="gaji_terakhir" value="{{$data->gaji_terakhir}}"></td>
													</tr>
													<tr>
														<td>Jenis Usaha</td>
														<td>:</td>
														<td><input type="text" required name="jenis_usaha" value="{{$data->jenis_usaha}}"></td>
													</tr>
													<tr>
														<td>Alasan Berhenti</td>
														<td>:</td>
														<td><input type="text" required name="alasan_berhenti" value="{{$data->alasan_berhenti}}"></td>
													</tr>
												</table>
											</div>
											<div class="col" style="font-size: 10px;">
												<table>
													<tr>
														<td>Jabatan Awal</td>
														<td>:</td>
														<td><input type="text" required name="jabatan_awal" value="{{$data->jabatan_awal}}"></td>
													</tr>
													<tr>
														<td>Jabatan Akhir</td>
														<td>:</td>
														<td><input type="text" required name="jabatan_akhir" value="{{$data->jabatan_akhir}}"></td>
													</tr>
													<tr>
														<td>Telp Perusahaan</td>
														<td>:</td>
														<td><input type="text" required name="telp_perusahaan" value="{{$data->telp_perusahaan}}"></td>
													</tr>
													<tr>
														<td>Nama Direktur</td>
														<td>:</td>
														<td><input type="text" required name="nama_direktur" value="{{$data->nama_direktur}}"></td>
													</tr>
													
												</table>
											</div>
											<div>
												<a href="" class="btn btn-primary">Tambah Riwayat Pekerjaan</a>
											</div>
										</div>
										<br>
										<b>Referensi (Kepada siapa kami dapat menanyakan diri anda lebih lengkap)</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<thead>
														<th>
															<td>NAMA</td>
															<td>ALAMAT/TELEPON</td>
															<td>PEKERJAAN</td>
															<td>HUBUNGAN</td>
														</th>
													</thead>
													<tbody>
														@foreach($dpp_kr as $dpp_kr)
														<tr>
															<td><input type="text" required name="nama_lengkap" value="{{$dpp_kr->nama_lengkap}}" class=""></td>
															<td><input type="text" required name="alamat_telpon" value="{{$dpp_kr->alamat_telpon}}" class=""></td>
															<td><input type="text" required name="pekerjaan" value="{{$dpp_kr->pekerjaan}}" class=""></td>
															<td><input type="text" required name="hubungan" value="{{$dpp_kr->hubungan}}" class=""></td>
															<td>
																<div class="col-sm-1">
																	<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addReferensiInput">
																		<i class="fa-solid fa-plus"></i>
																	</button>
																</div>
															</td>
														</tr>
														@endforeach
													</tbody>
												</table>
												<a href="">Batal Simpan Data</a>
											</div>
										</div>
										<br>
										<b>Orang yang dapat dihubungi dalam keadaaan mendesak</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<thead>
														<th>
															<td>NAMA</td>
															<td>ALAMAT/TELEPON</td>
															<td>PEKERJAAN</td>
															<td>HUBUNGAN</td>
														</th>
													</thead>
													<tbody>
														@foreach($dpp_kd as $dpp_kd)
														<tr>
															<td><input type="text" required name="nama_lengkap" value="{{$dpp_kd->nama_lengkap}}" class=""></td>
															<td><input type="text" required name="alamat_telpon" value="{{$dpp_kd->alamat_telpon}}" class=""></td>
															<td><input type="text" required name="pekerjaan" value="{{$dpp_kd->pekerjaan}}" class=""></td>
															<td><input type="text" required name="hubungan" value="{{$dpp_kd->hubungan}}" class=""></td>
															<td>
																<div class="col-sm-1">
																	<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addReferensiInput">
																		<i class="fa-solid fa-plus"></i>
																	</button>
																</div>
															</td>
														</tr>
														@endforeach
													</tbody>
												</table>
												<a href="">Batal Simpan Data</a>
											</div>
										</div>
										<br>
										<b>Uraikan tugas & tanggung jawab anda pada jabatan anda yang terakhir</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<textarea readonly name="tugas_jabatan_terakhir" id="" value="{{$data->tugas_jabatan_terakhir}}" cols="30" rows="10">{{$data->tugas_jabatan_terakhir}}</textarea>
											</div>
											*isi Fresh Graduate jika belum memiliki pengalaman
										</div>
										<br>
										<b>Uraikan hal-hal positif yang menjadi kekuatan anda & hal negatif dalam diri anda yang ingin anda kembangkan</b>
										<hr>
										<div class="row">
										@php $b=1; @endphp
											@foreach($uraian as )
											<div class="col" style="font-size: 10px;">
												<table>
													<thead style="border:1px solid black;">
														<tr>
															<th></th>
															<th>Hal-hal Positif</th>
															<th></th>
															<th></th>
															<th>Hal-hal Negatif</th>
														</tr>
													</thead>
													<tbody style="border:1px solid black;">
													
														<tr>
															<td>{{$b++;}}</td>
															<td><input type="text" value="{{$data->}}" class="form-control form-control-sm"></td>
															<td></td>
															<td>1</td>
															<td><input type="text" value="{{$data->}}" class="form-control form-control-sm"></td>
														</tr>
														
													</tbody>
												</table>
											</div>
										@endforeach
										</div>
										

										
								</div>
							</div>
							<hr>
							
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>


<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection