<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard Lowongan Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

</head>
<body>
<nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler container" style="color:white;" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class=""><b>DASHBOARD</b></span>
    </button>
  </div>
</nav>
<br>
<div class="row container mt-5">
    <div class="col">
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row g-0">
                <div class="col-md-4">
                <img src="..." class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">DATA LOWONGAN</h5>
                    <p class="card-text">5</p>
                    <p class="card-text"><small class="text-muted">..</small></p>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row g-0">
                <div class="col-md-4">
                <img src="..." class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">DATA PELAMAR</h5>
                    <p class="card-text">4</p>
                    <p class="card-text"><small class="text-muted">..</small></p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div style="background-color:#efefef;border-radius:2px;">
        <div class="row">
            <h5 class="m-3">LOWONGAN YANG RELEASE</h5>
            <div class="row m-5">
                <b>Kategori Pekerjaan</b><br><br>
                <div class="col-4">
                    <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-home-list" data-bs-toggle="list" href="#list-marketing" role="tab" aria-controls="list-home">Marketing</a>
                        <a class="list-group-item list-group-item-action" id="list-profile-list" data-bs-toggle="list" href="#list-it" role="tab" aria-controls="list-profile">IT</a>
                        <a class="list-group-item list-group-item-action" id="list-messages-list" data-bs-toggle="list" href="#list-hrd" role="tab" aria-controls="list-messages">HRD</a>
                        <a class="list-group-item list-group-item-action" id="list-settings-list" data-bs-toggle="list" href="#list-teknisi" role="tab" aria-controls="list-settings">Teknisi</a>
                        <a class="list-group-item list-group-item-action" id="list-settings-list" data-bs-toggle="list" href="#list-akutansi" role="tab" aria-controls="list-settings">Akutansi</a>
                        <a class="list-group-item list-group-item-action" id="list-settings-list" data-bs-toggle="list" href="#list-semua" role="tab" aria-controls="list-settings">Semua</a>
                    </div>
                </div>
                <div class="col-7">
                    <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-marketing" role="tabpanel" aria-labelledby="list-home-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                               @foreach($list_dm as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                                
                            </table>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="list-it" role="tabpanel" aria-labelledby="list-profile-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_it as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-hrd" role="tabpanel" aria-labelledby="list-messages-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_hrd as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-teknisi" role="tabpanel" aria-labelledby="list-settings-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_t as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-akutansi" role="tabpanel" aria-labelledby="list-settings-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_a as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-semua" role="tabpanel" aria-labelledby="list-settings-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_s as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
            
        });
    </script>
</body>
</html>