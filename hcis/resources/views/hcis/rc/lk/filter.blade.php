@extends('re.lo.side')
@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Data FPTK Report</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">FPTK Report</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xl-12">
                                    <div class="card" style="border-radius:8px;border:none;">
                                        <div class="card-header">
                                            <!-- <h5>Per Table</h5> -->
                                            <a href="{{route('ubah_fptk_r')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                        </div>
                                        <div class="card-body table-border-style">
                                            <div class="table-responsive-xl"  style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                            <form action="{{ URL::to('/list_fptk_r/hapus_banyak') }}" method="POST" id="form_delete">
                                                @csrf
                                                
                                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9; ">
                                                        <thead style="color:black;font-size:12px;">
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                            @for($i = 0; $i < count($th); $i++)
                                                                @if($th[$i] == 'kode_kategori_lowongan_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode Kategori Lowongan Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'nomer_dokumen_fptk')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Dokumen FPTK</th>
                                                                @endif
                                                                @if($th[$i] == 'kategori_pekerjaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kategori Pekerjaan</th>
                                                                @endif
                                                                @if($th[$i] == 'jabatan_lowongan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'lokasi_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'tingkat_kerjaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Kerjaan</th>
                                                                @endif
                                                                @if($th[$i] == 'pengalaman_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'status_kepegawaian')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Kepegawaian</th>
                                                                @endif
                                                                @if($th[$i] == 'minimal_pendidikan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Minimal Pendidikan</th>
                                                                @endif
                                                                @if($th[$i] == 'peran_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Peran Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'deskripsi_pekerjaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Deskripsi Pekerjaan</th>
                                                                @endif
                                                                @if($th[$i] == 'syarat_pengalaman')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Syarat Pengalaman</th>
                                                                @endif
                                                                @if($th[$i] == 'syarat_kemampuan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Syarat Kemampuan</th>
                                                                @endif
                                                                @if($th[$i] == 'syarat_lainnya')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Syarat Lainnya</th>
                                                                @endif
                                                                @if($th[$i] == 'jumlah_karyawan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jumlah Karyawan</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_upload')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Upload</th>
                                                                @endif
                                                                @if($th[$i] == 'status_lowongan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'status_rekaman')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                                @endif
                                                                
                                                                @if($i == count($th) - 1)
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                                @endif
                                                            @endfor
                                                        </thead>
                                                        <tbody style="font-size:11px;">
                                                            @php $b = 1; @endphp
                                                            @foreach($query as $row)
                                                                <tr>
                                                                    <td>{{$b++}}</td>
                                                                    @for($i = 0; $i < count($th); $i++)
                                                                        @if($th[$i] == 'kode_kategori_lowongan_kerja')
                                                                            <td>{{ $row->kode_kategori_lowongan_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nomer_dokumen_fptk')
                                                                            <td>{{ $row->nomer_dokumen_fptk ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kategori_pekerjaan')
                                                                            <td>{{ $row->kategori_pekerjaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jabatan_lowongan')
                                                                            <td>{{ $row->jabatan_lowongan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'lokasi_kerja')
                                                                            <td>{{ $row->lokasi_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tingkat_kerjaan')
                                                                            <td>{{ $row->tingkat_kerjaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pengalaman_kerja')
                                                                            <td>{{ $row->pengalaman_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_kepegawaian')
                                                                            <td>{{ $row->status_kepegawaian ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'minimal_pendidikan')
                                                                            <td>{{ $row->minimal_pendidikan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'peran_kerja')
                                                                            <td>{{ $row->peran_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'deskripsi_pekerjaan')
                                                                            <td>{{ $row->deskripsi_pekerjaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'syarat_pengalaman')
                                                                            <td>{{ $row->syarat_pengalaman ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'syarat_kemampuan')
                                                                            <td>{{ $row->syarat_kemampuan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'syarat_lainnya')
                                                                            <td>{{ $row->syarat_lainnya ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jumlah_karyawan')
                                                                            <td>{{ $row->jumlah_karyawan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_upload')
                                                                            <td>{{ $row->tanggal_upload ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_lowongan')
                                                                            <td>{{ $row->status_lowongan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_rekaman')
                                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                                            <td>{{ $row->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                                            <td>{{ $row->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($i == count($th) - 1)
                                                                            <td>
                                                                                <a href="{{ URL::to('/list_loker/detail/'.$row->id_lowongan_kerja) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                <a href="{{ URL::to('/list_loker/edit/'.$row->id_lowongan_kerja) }}" class="">Edit</a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_lowongan_kerja }}">
                                                                            </td>
                                                                        @endif
                                                                    @endfor
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_lo')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                            <td>
                                                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                            </form>
                                
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection