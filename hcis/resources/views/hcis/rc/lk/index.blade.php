@extends('re.lo.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Lowongan Kerja</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">Rekruitmen</a></li>
                                        <li class="breadcrumb-item"><a href="#!">Proses Rekruitment</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <!-- <div class="card-header">
                                    <h5>Per Table</h5>
                                    <a href="{{route('tambah_jpr')}}"  class="btn btn-success"><span class="d-block m-t-5"> <code style="color:white;">Tambah Jabatan Proses Rekruitment</code></span></a>
                                </div> -->
                                <div class="card-body table-border-style ">
                                <div class="table-responsive-xl " style="">
                                        <form action="{{ route('filter_dua_dcp')}}" method="POST">
                                            @csrf
                                            <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                                                    <p>DATA LOWONGAN KERJA</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 p-3 mb-1" style="width:70%; border-radius:5px;">
                                                        <div class="row">
                                                            <div class="col" style="font-size: 10px;">
                                                                <div class="container form-group row">
                                                                    <table class="">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" class="form-control form-control-sm">
                                                                            </td>
                                                                            <td>
                                                                                <a href="" class="btn btn-primary btn-sm">Cari</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>    
                                                                </div>
                                                                
                                                            </div>
                                                          
                                                        </div>
                                                        <div class="container" style="text-align:center;width:20%;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <form action="" method="POST" >
                                                            <div class="row m-2" style="border:2px solid #f3f3f3;width:95%;">
                                                                <div class="col-md-2 m-2" style="font-size: 10px;">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="jabatan" style="font-size:11px;" readonly class="form-control form-control-sm" id="jabatan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Jabatan</label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kategori_pekerjaan" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Kategori Pekerjaan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="lokasi" style="font-size:11px;" readonly class="form-control form-control-sm" id="lokasi" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Lokasi</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="pengalaman_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Pengalaman Kerja</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="tingkat_kerjaan" style="font-size:11px;" readonly class="form-control form-control-sm" id="tingkat_kerjaan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Tingkat</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 m-2">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kategori_pekerjaan" style="font-size:11px;" readonly class="form-control form-control-sm" id="jenis_pekerjaan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Jenis Pekerjaan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="minimal_pendidikan" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Minimal Pendidikan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="peran_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="peran_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Tentang Peran Pekerjaan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="deskripsi_pekerjaan" style="font-size:11px;" readonly class="form-control form-control-sm" id="deskripsi_pekerjaan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Deskripsi Pekerjaan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="syarat_pekerjaan" style="font-size:11px;" readonly class="form-control form-control-sm" id="syarat_pekerjaan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Syarat Pekerjaan</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 m-2">
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="tanggal_upload" style="font-size:11px;" readonly class="form-control form-control-sm" id="tanggal_upload" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Tanggal Upload</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="status_lowongan" style="font-size:11px;" readonly class="form-control form-control-sm" id="status_lowongan" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Status Lowongan</label>
                                                                    </div>
                                                                    <div class=" row">
                                                                        <div class=""> 
                                                                            <input type="checkbox" style="font-size:3px;" name="kode_lowongan_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lowongan_kerja" placeholder="">
                                                                        </div>&nbsp;
                                                                        <label for="colFormLabelSm" class="" style="font-size:09px;">Kode Lowongan Kerja</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col row mt-2">
                                                                    <div>
                                                                        <a type="text">
                                                                    </div>
                                                                    <div class=""> 
                                                                        <button type="submit" class="btn btn-primary btn-sm" id="kode_lokasi_kerja" value="filter">Tampil Data</button>
                                                                    </div>&nbsp;
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                    <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                        <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                        
                                        <div style="">
                                            <form action="{{ URL::to('/list_loker/hapus_banyak') }}" method="POST" id="form_delete" style="margin:30px;">
                                                @csrf
                                                <table id="data-tables" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;" >
                                                    <thead style="color:black;font-size: 12px; ">
                                                        <tr>
                                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">No</th>
                                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">Jabatan</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Kategori Pekerjaan</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Lokasi</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Pengalaman Kerja</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Tingkat</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Action</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">V</th>
                                                        </tr>
                                                    </thead>
                                                    @php $b=1; @endphp
                                                        @foreach($rc_loker as $data) 
                                                            <tbody style="font-size: 11px;">
                                                                {{-- {{dd($data)}} --}}
                                                                    <tr>
                                                                        <td>{{$b++;}}</td>
                                                                        <td style="padding-right:-60px;">{{$data->jabatan_lowongan}}</td>
                                                                        <td>{{$data->kategori_pekerjaan}}</td>
                                                                        <td>{{$data->lokasi_kerja}}</td>
                                                                        <td>{{$data->pengalaman_kerja}}</td>
                                                                        <td>{{$data->tingkat_kerjaan}}</td>
                                                                        <td>
                                                                            <a href="{{ URL::to('/list_loker/detail/'.$data->id_lowongan_kerja) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                            <a href="{{ URL::to('/list_loker/edit/'.$data->id_lowongan_kerja) }}" class="">Edit</a>
                                                                        </td>
                                                                        <td>
                                                                            <input type="checkbox" name="multiDelete[]" value="{{ $data->id_lowongan_kerja }}" id="multiDelete" >
                                                                        </td>
                                                                    </tr>
                                                            </tbody>
                                                        @endforeach
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_lo')}}">Tambah</a></td>
                                                        <td>
                                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                            
                                        </div>
                                        <br>
                                    <!-- </div> -->
                                    </div>
                                </div>
                            <br>
                                <br>
                                       
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection
