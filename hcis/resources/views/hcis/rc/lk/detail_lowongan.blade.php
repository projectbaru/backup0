<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Lowongan Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body>
@foreach($list_loker as $data)
<nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler container" style="color:white;" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class=""><b></b></span>
    </button>
  </div>
</nav>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="background-color:#e7e7e7;">
  <div class="carousel-inner" style="background-color:#e7e7e7;height: 400px;">
    <div class="carousel-item active">
      <img class="d-block w-100" src="" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <div class="m-4" style="background-color:">
        <h4>
            {{$data->jabatan_lowongan}}
        </h4>
    </div>
</div>
<br>
<div class="input-group mb-3 ">
    <input type="text" class="form-control" placeholder="Jabatan, Lokasi, Minimal Pendidikan" aria-label="Recipient's username" aria-describedby="basic-addon2">
    <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="button">Cari</button>
    </div>
</div>
<div class="">
    <div style="border-radius:2px;">
        <div class="row">
            <div class="row m-5">
                <div class="col-4">
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Lokasi</h5>
                            <p class="card-text">{{$data->lokasi_kerja}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Kategori Pekerjaan</h5>
                            <p class="card-text">{{$data->kategori_pekerjaan}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Minimal Pendidikan</h5>
                            <p class="card-text">{{$data->minimal_pendidikan}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Pengalaman Kerja</h5>
                            <p class="card-text">{{$data->pengalaman_kerja}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Tipe Kepegawaian</h5>
                            <p class="card-text">{{$data->status_kepegawaian}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Tingkat</h5>
                            <p class="card-text">{{$data->tingkat_kerjaan}}</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-7">
                    <div>
                        <h3>Tentang Peran Pekerjaan</h3>
                    </div>
                    <div>
                        {{$data->deskripsi_pekerjaan}}
                    </div>
                    <div style="background-color:#e7e7e7;">
                        <div class="m-3"><br>
                            <div>
                                <h6>Deskripsi Pekerjaan</h6>
                                <p>
                                {{$data->syarat_pengalaman}}
                                </p>
                            </div>
                            
                            <div>
                                <h6>Pengalaman</h6>
                                <p>
                                {{$data->syarat_pengalaman}}
                                </p>
                            </div>
                            <div>
                                <h6>Kemampuan</h6>
                                <p>
                                {{$data->syarat_kemampuan}}
                                </p>
                            </div>
                            <div>
                                <h6>Lainnya</h6>
                                <p>
                                {{$data->syarat_lainnya}}
                                </p>
                            </div>
                        
                            <br>
                            <br>
                        </div>
                    
                    </div>
                    <a href="" class="btn btn-primary">
                            Lamar Sekarang
                        </a>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer bg-primary">
    <div>
        <p>PT.SRU</p>
    </div>
    <div>
        <a href=""></a>
    </div>
</footer>
@endforeach


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
            
        });
    </script>
</body>
</html>