<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lowongan Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body>
<nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler container" style="color:white;" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class=""><b></b></span>
    </button>
  </div>
</nav>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="..." alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<div class="input-group mb-3 ">
    <input type="text" class="form-control" placeholder="Jabatan, Lokasi, Minimal Pendidikan" aria-label="Recipient's username" aria-describedby="basic-addon2">
    <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="button">Cari</button>
    </div>
</div>
<div class=" mt-5">
    <div style="background-color:#efefef;border-radius:2px;">
        <div class="row">
            <!-- <h5 class="m-3">LOWONGAN YANG RELEASE</h5> -->
            <div class="row m-5">
                <b>Kategori Pekerjaan</b><br><br>
                <div class="col-4">
                    <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-marketing-list" data-bs-toggle="list" href="#list-marketing" role="tab" aria-controls="list-marketing">Marketing</a>
                        <a class="list-group-item list-group-item-action" id="list-it-list" data-bs-toggle="list" href="#list-it" role="tab" aria-controls="list-it">IT</a>
                        <a class="list-group-item list-group-item-action" id="list-hrd-list" data-bs-toggle="list" href="#list-hrd" role="tab" aria-controls="list-hrd">HRD</a>
                        <a class="list-group-item list-group-item-action" id="list-teknisi-list" data-bs-toggle="list" href="#list-teknisi" role="tab" aria-controls="list-teknisi">Teknisi</a>
                        <a class="list-group-item list-group-item-action" id="list-akutansi-list" data-bs-toggle="list" href="#list-akutansi" role="tab" aria-controls="list-akutansi">Akutansi</a>
                        <a class="list-group-item list-group-item-action" id="list-semua-list" data-bs-toggle="list" href="#list-semua" role="tab" aria-controls="list-semua">Semua</a>
                    </div>
                </div>
                <div class="col-7">
                    <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-marketing" role="tabpanel" aria-labelledby="list-marketing-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                               @foreach($list_dm as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-it" role="tabpanel" aria-labelledby="list-it-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_it as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-hrd" role="tabpanel" aria-labelledby="list-hrd-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_hrd as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-teknisi" role="tabpanel" aria-labelledby="list-teknisi-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_t as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-akutansi" role="tabpanel" aria-labelledby="list-akutansi-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_a as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-semua" role="tabpanel" aria-labelledby="list-semua-list">
                        <div class="table-responsive container m-2" style="background-color:white;">
                            <table class="table" style="width:100%; font-size: 12px;" id="example">
                                @foreach($list_s as $data)
                                <tr>
                                    <td>
                                        <h5 style="color:blue;">{{$data->jabatan_lowongan}}</h5>
                                        {{$data->lokasi_kerja}}|mis|{{$data->pengalaman_kerja}} <br>
                                        <p>{{$data->deskripsi_pekerjaan}}</p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="">{{$data->tanggal_mulai_efektif}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer" style="background-color:primary;">
    <div>
        <p>PT.SRU</p>
    </div>
    <div>
        
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
            
        });
    </script>
</body>
</html>