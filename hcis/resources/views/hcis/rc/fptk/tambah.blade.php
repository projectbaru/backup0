@extends('re.cv.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Tambah Formulir Permintaan Tenaga Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							<div class="form-group" style="width:95%;">
								No ISO: 
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
									<b>Pemohon Tenaga Kerja</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pemohon</label>
												<!-- <div class="col-sm-9">
												<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div> -->
												<div class="col-sm-9">
													<select name="nama_pemohon" required id="nama_pemohon" class="form-control form-control-sm" style="font-size:11px;">
														<option value="" selected disabled>--Tingkat--</option>
														<option value="staff" >-</option>
													</select>
													<!-- <input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
												<div class="col-sm-5">
													<select name="jabatan_kepegawaian" required id="jabatan_kepegawaian" class="form-control form-control-sm" style="font-size:11px;">
														<option value="" selected disabled>--Tingkat--</option>
														<option value="staff" >-</option>
													</select>
													<!-- <input type="text" required name="jabatan_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
												<div class="col-sm-5">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio1" value="staff">
														<label class="form-check-label" for="inlineRadio1">Staff</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio2" value="middle">
														<label class="form-check-label" for="inlineRadio2">Middle</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio2" value="senior">
														<label class="form-check-label" for="inlineRadio2">Senior</label>
													</div>
													<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
												<div class="col-sm-5">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio1" value="staff">
														<label class="form-check-label" for="inlineRadio1">Orang</label>
													</div>
													
													<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Bekerja</label>
												<div class="col-sm-5">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="date" name="tanggal_mulai_bekerja" id="inlineRadio1">														
													</div>
													
													<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
												<div class="col-sm-5">
													<div class="form-check form-check-inline">
														<select name="lokasi_kerja" required id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
															<option value="" selected disabled>--</option>
															<option value="staff" >-</option>
														</select>	
														<!-- <input class="form-check-input" type="date" name="tanggal_mulai_bekerja" id="inlineRadio1">														 -->
													</div>
													
													<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
												</div>
											</div>
							
										</div>
										
										<div class="col" style="font-size: 10px;">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alasan Penambahan Tenaga Kerja</label>
											<div class="col-sm-9">
												<textarea class="form-check-input" type="text" name="alasan_penambahan_tenaga_kerja" id="inlineRadio1" value=""></textarea>														
												<!-- <input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
											</div>
										</div>
									</div>
									<hr>
									<b>Kualifikasi Tenaga Kerja</b>
									<div class="row">
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
												<div class="col-sm-5">
													<input type="text" required name="jenis_kelamin" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Usia</label>
												<div class="col-sm-5">
													<input type="text" required name="usia_minimal" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tahun S/d</label>
												<div class="col-sm-5">
													<input type="text" required name="usia_maksimal" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> Tahun												
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status</label>
												<div class="col-sm-5">
													<div class="form-check form-check-inline">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
													<label class="form-check-label" for="inlineCheckbox1">Lajang</label>
													</div>
													<div class="form-check form-check-inline">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
													<label class="form-check-label" for="inlineCheckbox2">Menikah</label>
													</div>
													
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
												<div class="col-sm-5">
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="SMU/Sederajat">
													<label class="form-check-label" for="inlineCheckbox1">SMU/Sederajat</label>
													</div>
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D1">
													<label class="form-check-label" for="inlineCheckbox2">D1</label>
													</div>
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2">
													<label class="form-check-label" for="inlineCheckbox2">D2</label>
													</div>
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2">
													<label class="form-check-label" for="inlineCheckbox2">D3</label>
													</div>
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2">
													<label class="form-check-label" for="inlineCheckbox2">S1</label>
													</div>
													<div class="form-check">
													<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2">
													<label class="form-check-label" for="inlineCheckbox2">S2</label>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col">
											<div class="row">
												<div class="form-check">
													<label class="form-check-label" for="inlineCheckbox2">Pengalaman</label>
													<textarea class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2"></textarea>
												</div>
											</div>
											<div>
												<div class="form-check">
													<label class="form-check-label" for="inlineCheckbox2">Kemampuan Lainnya</label>
													<textarea class="form-check-input" type="checkbox" id="inlineCheckbox2" value="D2"></textarea>
												</div>
											</div>
										</div>
									</div>
									<hr>
									<b>Kopensasi Tenaga Kerja</b>
										<div class="col">
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault1">
													Gaji
												</label>
												<input class="form-check-input" type="text" name="gaji" id="flexRadioDefault1">
											</div>
										</div>
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault2">
													Tunjangan Kesehatan:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_kesehatan" id="flexRadioDefault2" checked>
											</div>
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault2">
													Tunjangan Transport:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_transport" id="flexRadioDefault2" checked>
											</div>
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault2">
													Tunjangan Makan:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_makan" id="flexRadioDefault2" checked>
												
											</div>
											<div class="form-check">
											<label class="form-check-label" for="flexRadioDefault2">
													Tunjangan Komunikasi:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_komunikasi" id="flexRadioDefault2" checked>
												
											</div>
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault2">
													Hari Kerja:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_komunikasi" id="flexRadioDefault2" checked>
												
											</div>
											<div class="form-check">
												<label class="form-check-label" for="flexRadioDefault2">
													Jam Kerja:
												</label>
												<input class="form-check-input" type="text" name="tunjangan_komunikasi" id="flexRadioDefault2" checked>
											</div>
										</div>
									</div>
										<hr>
										<label class="form-check-label" for="flexRadioDefault2">
											Lampiran File
										</label>
										<input class="form-check-input" type="file" name="tunjangan_komunikasi" id="flexRadioDefault2" checked>

										<input type="text">
										<b>Persetujuan Permintaan Tenaga Kerja</b>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diajukan</label>
													<div class="col-sm-9">
														<input type="text" value="">Diajukan</input>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diperiksa</label>
													<div class="col-sm-9">
														<input type="text" value="">Diperiksa</input>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Disetujui</label>
													<div class="col-sm-9">

														<input type="text" value="">Disetujui</input>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Disetujui</label>
													<div class="col-sm-9">
														<input type="text" value="">Diperiksa/ Dianalisa</input>
													</div>
												</div>
												
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
													<div class="col-sm-9">
														<input type="text" value="">Diajukan</input>
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
													<div class="col-sm-9">
														<input type="text" value="GM HD/GM NHD/GM Operasional/Corporate Service">Default Jabatannya</input>
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
													<div class="col-sm-9">
														<input type="text" value="GM HD/GM NHD/GM Operasional/Corporate Service">Default Jabatannya</input>
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
													<div class="col-sm-9">
														<input type="text" value="GM HD/GM NHD/GM Operasional/Corporate Service">GM/HD/GM NHD/GM Operasional/Corporate Service</input>
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diperiksa/ Dianalisa</label>
													<div class="col-sm-9">
														<input type="text" value="HR Dept. Head">HR Dept. Head</input>
													</div>
												</div>
											</div>
											
										</div>
										<div>
											
										</div>
										<hr>

										<div class="">
											<table>
												<thead>
													<tr>
														<th>No</th>
														<th>Nama Pemohon</th>
														<th>No Dokumen</th>
														<th>Jabatan Yang Dibutuhkan</th>
														<th>Tingkat</th>
														<th>Jumlah Kebutuhan</th>
														<th>Lampiran</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</table>
										</div>
								</div>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
										<input type="text">
									</td> -->
									<td>
										<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
									<td>
										<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Ubah</a>
									</td>
									<td>
										<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endsection

