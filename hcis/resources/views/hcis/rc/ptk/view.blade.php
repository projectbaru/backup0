@extends('re.ptk.side')
<style href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"></style>
<style href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css"></style>

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header mb-1">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">

                                <div class="card-body table-border-style">
                                    <div class="table-responsive-xl" style="overflow-x:auto;">
                                        <form action="{{ URL::to('/list_data/filter') }}" method="POST" id="form_delete">
                                        @csrf
                                            <div class="header p-3 mb-1" style="width:70%; border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container" style="background-color:#3949ab;color:white;text-align:center;">
                                                    <p>Filtering Data</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col" style="font-size: 10px;">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK(Dept)</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="nama_ptk" id="nama_ptk" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d )
                                                                     <option value="{{$d->nama_ptk}}" >{{$d->nama_ptk}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Jabatan</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="tingkat_jabatan" id="tingkat_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d)
                                                                        <option value="{{$d->nama_posisi}}">{{$d->nama_posisi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col" style="font-size: 10px;">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Grup Lokasi Kerja</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d )
                                                                    <option value="{{$d->nama_grup_lokasi_kerja}}" >{{$d->nama_grup_lokasi_kerja}}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Periode PTK</label>
                                                            <div class="col-sm-5">
                                                                
                                                                <input type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="container" style="text-align:center;width:20%;">
                                                    <button type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">Filter</button>
                                                </div>
                                            </div>
                                            <div class="card" style="width:70%;">
                                    <!-- <h5>Per Table</h5> -->
                                                <div class="m-1" >
                                                     <a href="{{route('ubah_tamptk')}}" ><span class="d-block m-t-5"> <code style="color:#3949ab;">Buat Tampilan Baru</code></span></a>
                                                </div>
                                            </div>
                                            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2 display" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                <thead style="color:black;font-size:12px;">
                                                    <tr>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">No</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Kode PTK</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Nama PTK(Departemen)</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Posisi</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Tingkat Jabatan</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Grup Lokasi Kerja</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Lokasi Kerja</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Tenaga Kerja Saat Di Perusahaan</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Periode PTK</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">V</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="font-size:11px;">
                                                    @php $b=1; @endphp
                                                    @foreach ($data as $d)
                                                    <tr>
                                                        <td>{{ $b++; }}</td>
                                                        <td>{{ $d->kode_ptk }}</td>
                                                        <td>{{ $d->nama_ptk }}</td>
                                                        <td>{{ $d->nama_posisi }}</td>
                                                        <td>{{ $d->tingkat_jabatan }}</td>
                                                        <td>{{ $d->nama_grup_lokasi_kerja }}</td>
                                                        <td>{{ $d->nama_lokasi_kerja }}</td>
                                                        <td>{{ $d->tksi_perusahaan }}</td>
                                                        <td>{{ $d->periode_ptk }}</td>
                                                        <td>
                                                            <a href="{{ URL::to('/list_ptk/detail/'.$d->id_ptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                            <a href="{{ URL::to('/list_ptk/edit/'.$d->id_ptk) }}" class="">Edit</a>
                                                        </td>
                                                        <td><input type="checkbox" name="multiDelete[]" id="multiDelete" value="{{ $d->id_ptk }}"></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                        </form>
                                        <table>
                                            <tr>

                                                <td>
                                                    <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>


@section('add-scripts')

<script>
    // $(function() {
    //     //hang on event of form with id=myform
    //     $("#form_delete").submit(function(e) {
    //         e.preventDefault();
    //         var actionurl = e.currentTarget.action;
    //         Swal.fire({
    //             title: '<h4>Anda yakin ingin menghapus?</h4>',
    //             icon: 'warning',
    //             showCancelButton: true,
    //             confirmButtonColor: "#3085d6",
    //             cancelButtonColor: "#d33",
    //             cancelButtonText: "Batal",
    //             confirmButtonText: "<i>Ya, hapus!</i>",
    //             // buttonsStyling: false
    //         }).then((result) => {
    //             if (result.isConfirmed) {
    //                 $.ajax({
    //                     url: actionurl,
    //                     type: 'post',
    //                     dataType: 'JSON',
    //                     cache: false,
    //                     data: $("#form_delete").serialize(),
    //                     success: function(res) {
    //                         if (res.status) {
    //                             Swal.fire({
    //                                 icon: "success",
    //                                 title: "Berhasil!",
    //                                 text: "Data telah terhapus.",
    //                             });
    //                         }
    //                     },
    //                     error: function(e) {
    //                         console.log(e);
    //                         Swal.fire({
    //                             icon: "error",
    //                             title: "Gagal!",
    //                             text: "Terjadi kesalahan saat menghapus data.",
    //                         });
    //                     },
    //                     complete: function(jqXhr, msg) {
    //                         setTimeout(() => {
    //                             location.reload();
    //                         }, 800);
    //                         // console.log(jqXhr, msg);
    //                     }
    //                 });
    //             }
    //         })
    //     });
    // });
</script>
@endsection
