@extends('re.ptk.side')
@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- <div class="page-header mb-1">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <br>
                    <br>
                    <div class="row ">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-body  table-border-style">
                                    <div class="table-responsive-xl " style="overflow-x:auto;">
                                        <form action="{{route('ptk_submit')}}" method="POST" >
                                        @csrf
                                            <div class="container p-3 mb-1" style="width:70%; border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container" style="background-color:#3949ab;color:white;text-align:center;">
                                                    <p>Olah Data PTK</p>
                                                </div>

                                                <div class="row">

                                                    <div class="col" style="font-size: 10px;">

                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" id="kode_grup_lokasi_kerja" readonly  name="kode_grup_lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
                                                            <div class="col-sm-9">
                                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($po['looks'] as $p )
                                                                    <option value="{{$p->nama_grup_lokasi_kerja}}" >{{$p->nama_grup_lokasi_kerja}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
                                                            <div class="col-sm-9">
                                                                <input type="text"  name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="kode_lokasi_kerja" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                                            <div class="col-sm-9">
                                                                <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($po['looks'] as $p )
                                                                    <option value="{{$p->lokasi_kerja}}" >{{$p->lokasi_kerja}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <!-- <input type="text"  name="nama_lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun Periode</label>
                                                            <div class="col-sm-5">
                                                            <select class="form-select" name="tahun" aria-label="Default select example">
                                                                <option selected>Open this select menu</option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                            </select>
                                                                <!-- <input type="text"  name="periode_ptk" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="container" style="text-align:center;width:20%;">
                                                    <button type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">Olah Data</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
    $(function() {
        //hang on event of form with id=myform
    //     $("#form_delete").submit(function(e) {
    //         e.preventDefault();
    //         var actionurl = e.currentTarget.action;
    //         Swal.fire({
    //             title: '<h4>Anda yakin ingin menghapus?</h4>',
    //             icon: 'warning',
    //             showCancelButton: true,
    //             confirmButtonColor: "#3085d6",
    //             cancelButtonColor: "#d33",
    //             cancelButtonText: "Batal",
    //             confirmButtonText: "<i>Ya, hapus!</i>",
    //             // buttonsStyling: false
    //         }).then((result) => {
    //             if (result.isConfirmed) {
    //                 $.ajax({
    //                     url: actionurl,
    //                     type: 'post',
    //                     dataType: 'JSON',
    //                     cache: false,
    //                     data: $("#form_delete").serialize(),
    //                     success: function(res) {
    //                         if (res.status) {
    //                             Swal.fire({
    //                                 icon: "success",
    //                                 title: "Berhasil!",
    //                                 text: "Data telah terhapus.",
    //                             });
    //                         }
    //                     },
    //                     error: function(e) {
    //                         console.log(e);
    //                         Swal.fire({
    //                             icon: "error",
    //                             title: "Gagal!",
    //                             text: "Terjadi kesalahan saat menghapus data.",
    //                         });
    //                     },
    //                     complete: function(jqXhr, msg) {
    //                         setTimeout(() => {
    //                             location.reload();
    //                         }, 800);
    //                         // console.log(jqXhr, msg);
    //                     }
    //                 });
    //             }
    //         })
    //     });
    // });
</script>
@endsection
