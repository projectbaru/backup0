@extends('re.ptk.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Detail PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
										<li class="breadcrumb-item"><a href="#!">detail ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <form action="{{route('update_ptk')}}" method="post" enctype="multipart/form-data"> {{ csrf_field() }}
                        <hr>
							{{ csrf_field() }}
                            @foreach($data_ptk as $data)
                            <div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('view_ptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
									<td>
										<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
									</td>
									<td>
										<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Proses Data</a>
									</td>
								</tr>
								</table>
							</div>
                            <br>
							<div class="form-group" style="width:95%;">
								<div class="">
									<b>Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                           
                                                <tr>
                                                    <td>Periode PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="periode_ptk" value="{{$data->periode_ptk}}">{{$data->periode_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Awal</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_awal" value="{{$data->tanggal_awal}}">{{$data->tanggal_awal}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_akhir" value="{{$data->tanggal_akhir}}">{{$data->tanggal_akhir}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Awal</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_awal" value="{{$data->ptk_awal}}">{{$data->ptk_awal}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Yang Digantikan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_yang_digantikan" value="{{$data->ptk_yang_digantikan}}">{{$data->ptk_yang_digantikan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Tambahan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_tambahan" value="{{$data->ptk_tambahan}}">{{$data->ptk_tambahan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_akhir" value="{{$data->ptk_akhir}}">{{$data->ptk_akhir}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_perusahaan" value="{{$data->tksi_perusahaan}}">{{$data->tksi_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Pengganti</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_pengganti" value="{{$data->tksi_pengganti}}">{{$data->tksi_pengganti}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_final" value="{{$data->tksi_final}}">{{$data->tksi_final}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Selisih</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="selisih" value="{{$data->selisih}}">{{$data->selisih}}</td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>

                                                <tr>
                                                    <td>Kode PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_ptk" value="{{$data->kode_ptk}}">{{$data->kode_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_ptk" value="{{$data->nama_ptk}}">{{$data->nama_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Grup Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_grup_lokasi_kerja" value="{{$data->kode_grup_lokasi_kerja}}">{{$data->kode_grup_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Grup Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_grup_lokasi_kerja" value="{{$data->nama_grup_lokasi_kerja}}">{{$data->nama_grup_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_lokasi_kerja" value="{{$data->kode_lokasi_kerja}}">{{$data->kode_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_lokasi_kerja" value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tingkat Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_jabatan" value="{{$data->tingkat_jabatan}}">{{$data->tingkat_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <b>Rekaman Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr>
                                            </table> 
                                        </div>
                                    </div>
                                </div>
							</div>
							<hr>
							
							<br>
                            @endforeach
                    </form>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection