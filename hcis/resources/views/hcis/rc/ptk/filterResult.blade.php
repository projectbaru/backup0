@extends('re.transaksi_ptk.side')
@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                        <form action="{{ URL::to('/list_ptk/hapus_banyak') }}" method="POST"  id="form_delete">
                                            @csrf
                                            <div class="header p-3 mb-1" style="width:70%; border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container" style="background-color:#3949ab;color:white;text-align:center;">
                                                    <p>Filtering Data</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col" style="font-size: 10px;">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK(Dept)</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="nama_ptk" id="nama_ptk" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d )
                                                                     <option value="{{$d->nama_ptk}}" >{{$d->nama_ptk}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Jabatan</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="tingkat_jabatan" id="tingkat_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d)
                                                                        <option value="{{$d->nama_posisi}}">{{$d->nama_posisi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col" style="font-size: 10px;">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Grup Lokasi Kerja</label>
                                                            <div class="col-sm-5" style="font-size:11px;">
                                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($ptk_detail as $d )
                                                                    <option value="{{$d->nama_grup_lokasi_kerja}}" >{{$d->nama_grup_lokasi_kerja}}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Periode PTK</label>
                                                            <div class="col-sm-5">
                                                                <input type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="container" style="text-align:center;width:20%;">
                                                    <button type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">Filter</button>
                                                </div>
                                                
                                            </div>
                                            <div class="card" style="width:70%;">
                                    <!-- <h5>Per Table</h5> -->
                                                <div class="m-1" >
                                                     <a href="{{route('ubah_tamptk')}}" ><span class="d-block m-t-5"> <code style="color:#3949ab;">Buat Tampilan Baru</code></span></a>
                                                </div>
                                            </div>
                                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead style="color:black;">
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                                                        @for($i = 0; $i < count($th); $i++)
                                                            <!-- @if($th[$i] == 'id_perusahaan')
                                                                <th></th>
                                                            @endif -->
                                                            @if($th[$i] == 'kode_ptk')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode PTK</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_ptk')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama PTK</th>
                                                            @endif
                                                            @if($th[$i] == 'periode_ptk')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Periode PTK</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_grup_lokasi_kerja')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Grup Lokasi Kerja</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_grup_lokasi_kerja')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Grup Lokasi Kerja</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_lokasi_kerja')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Lokasi Kerja</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_lokasi_kerja')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_jabatan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Jabatan</th>
                                                            @endif
                                                            @if($th[$i] == 'tingkat_jabatan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Jabatan</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'ptk_awal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">PTK Awal</th>
                                                            @endif
                                                            @if($th[$i] == 'ptk_yang_digantikan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">PTK Yang Digantikan</th>
                                                            @endif
                                                            @if($th[$i] == 'ptk_tambahan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">PTK Tambahan</th>
                                                            @endif
                                                            @if($th[$i] == 'ptk_akhir')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">PTK Akhir</th>
                                                            @endif
                                                            @if($th[$i] == 'tksi_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">TKSI Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'tksi_pengganti')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">TKSI Pengganti</th>
                                                            @endif
                                                            @if($th[$i] == 'tksi_final')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">TKSI Final</th>
                                                            @endif
                                                            @if($th[$i] == 'selisih')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Selisih</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_awal')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Awal</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_akhir')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Akhir</th>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                            @endif
                                                            @if($th[$i] == 'status_rekaman')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                            @endif
                                                            @if($th[$i] == 'waktu_user_input')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu User Input</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                            @endif
                                                           
                                                            @if($i == count($th) - 1)
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                            @endif
                                                        @endfor
                                                    </thead>
                                                    <tbody style="font-size:11px;">
                                                        @php $b=1 @endphp @foreach($query as $row)
                                                            <tr>
                                                                <td style="">{{$b++}}</td>
                                                                @for($i = 0; $i < count($th); $i++)
                                                                    <!-- @if($th[$i] == 'id_perusahaan')
                                                                        <td>{{ $row->id_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif -->
    
                                                                    @if($th[$i] == 'kode_ptk')
                                                                        <td>{{ $row->kode_ptk ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_ptk')
                                                                        <td>{{ $row->nama_ptk ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'periode_ptk')
                                                                        <td>{{ $row->periode_ptk ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_grup_lokasi_kerja')
                                                                        <td>{{ $row->kode_grup_lokasi_kerja ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_grup_lokasi_kerja')
                                                                        <td>{{ $row->nama_grup_lokasi_kerja ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_lokasi_kerja')
                                                                        <td>{{ $row->kode_lokasi_kerja ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_lokasi_kerja')
                                                                        <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_jabatan')
                                                                        <td>{{ $row->kode_jabatan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tingkat_jabatan')
                                                                        <td>{{ $row->tingkat_jabatan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_posisi')
                                                                        <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_posisi')
                                                                        <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'ptk_awal')
                                                                        <td>{{ $row->ptk_awal ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'ptk_yang_digantikan')
                                                                        <td>{{ $row->ptk_yang_digantikan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'ptk_tambahan')
                                                                        <td>{{ $row->ptk_tambahan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'ptk_akhir')
                                                                        <td>{{ $row->ptk_akhir ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tksi_perusahaan')
                                                                        <td>{{ $row->tksi_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tksi_pengganti')
                                                                        <td>{{ $row->tksi_pengganti ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tksi_final')
                                                                        <td>{{ $row->tksi_final ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'selisih')
                                                                        <td>{{ $row->selisih ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_awal')
                                                                    <td>{{ $row->tanggal_awal ? date('d-m-Y', strtotime($row->tanggal_awal)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_akhir')
                                                                    <td>{{ $row->tanggal_akhir ? date('d-m-Y', strtotime($row->tanggal_akhir)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'keterangan')
                                                                        <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'status_rekaman')
                                                                        <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'waktu_user_input')
                                                                        <td>{{ $row->waktu_user_input ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_mulai_efektif')
                                                                    <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_selesai_efektif')
                                                                    <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($i == count($th) - 1)
                                                                        <td> 
                                                                            <a href="{{ URL::to('/list_ptk/detail/'.$row->id_ptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                            <a href="{{ URL::to('/list_ptk/edit/'.$row->id_ptk) }}" class="">Edit</a>
                                                                        </td>
                                                                        <td>
                                                                        <input type="checkbox" name="multiDelete[]" value="{{ $row->id_ptk }}"  id="multiDelete">                                  
                                                                        </td>
                                                                    @endif
                                                                @endfor
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_tptk_peng')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                        <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                        <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                        </td>
                                                        <!-- <td>
                                                            <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Print</button>
                                                        </td> -->
                                                    </tr>
                                                </table>
                                        </form>
            
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        var pathname = window.location.pathname;
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_a') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
    });
</script>
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection