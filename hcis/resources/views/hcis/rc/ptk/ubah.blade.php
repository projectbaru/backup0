@extends('re.ptk.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Ubah PTK</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">ptk</a></li>
										<li class="breadcrumb-item"><a href="#!">ubah ptk</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <form action="{{route('update_ptk')}}" method="post"  id="formD" name="formD" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
							{{ csrf_field() }}
							@foreach($rc_ptk_detail as $data)
							<div class="form-group" style="width:95%;">
								<div class="">
									<b>Informasi</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Periode PTK</label>
												<div class="col-sm-9">
													<input type="text" required value="{{$data->periode_ptk}}" readonly name="periode_ptk" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Awal</label>
												<div class="col-sm-9">
													<input type="date" name="tanggal_awal" style="font-size:11px;" value="{{$data->tanggal_awal}}"  class="form-control form-control-sm startDates" id="tanggal_awal" placeholder="Tanggal awal">

													<!-- <input type="date" name="tanggal_awal" value="{{$data->tanggal_awal}}" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_awal" placeholder="Tanggal awal"> -->

													<!-- <input type="date"  value="{{$data->tanggal_awal}}" name="tanggal_awal" style="font-size:11px;"  class="form-control form-control-sm startDates" id="colFormLabelSm" placeholder=""> -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Akhir</label>
												<div class="col-sm-9">
												<input type="date" name="tanggal_akhir" style="font-size:11px;" value="{{$data->tanggal_akhir}}" class="form-control form-control-sm endDates" id="tanggal_akhir" placeholder="Tanggal akhir"/>

													<!-- <input type="date"  name="tanggal_akhir" value="{{$data->tanggal_akhir}}" style="font-size:11px;"  class="form-control form-control-sm endDates" id="colFormLabelSm" placeholder="Tanggal awal"> -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Awal</label>
												<div class="col-sm-9">
												<input type="text"  name="ptk_awal" value="" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)"  style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Yang Digantikan</label>
												<div class="col-sm-9">
												<input type="text"  name="ptk_yang_digantikan"  onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Tambahan</label>
												<div class="col-sm-9">
												<input type="text"  name="ptk_tambahan" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Akhir</label>
												<div class="col-sm-9">
												<input type="text"  name="ptk_akhir"  value="" id="ptk_akhir" readonly style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Perusahaan</label>
												<div class="col-sm-9">
												<input type="text"  name="tksi_perusahaan" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="{{$data->tksi_perusahaan}}" readonly style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Pengganti</label>
												<div class="col-sm-9">
												<input type="text"  name="tksi_pengganti" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="{{$data->tksi_pengganti}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Akhir</label>
												<div class="col-sm-9">
												<input type="text"  name="tksi_final" value="{{$data->tksi_final}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Selisih</label>
												<div class="col-sm-9">
												<input type="text"  name="selisih" value="{{$data->selisih}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											
										</div>
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode PTK</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->kode_ptk}}" name="kode_ptk" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->nama_ptk}}" name="nama_ptk" style="font-size:11px;" readonly  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->kode_grup_lokasi_kerja}}" name="kode_grup_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->nama_grup_lokasi_kerja}}" name="nama_grup_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->kode_lokasi_kerja}}" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->nama_lokasi_kerja}}" name="nama_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Jabatan</label>
												<div class="col-sm-9">
												<input type="text"  value="{{$data->tingkat_jabatan}}" name="tingkat_jabatan" style="font-size:11px;" readonly  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
												<div class="col-sm-9">
												<input type="text" value="{{$data->nama_posisi}}" name="nama_posisi" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div>
											</div>
										</div>
									</div>
									<hr>
									<b>Rekaman Informasi</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
												<div class="col-sm-10">
													<input type="date" value="{{$data->tanggal_mulai_efektif}}" required name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDate" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

													<!-- <input type="text" required name="tanggal_mulai_efektif" value="" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
												<div class="col-sm-10">
													<input type="date" value="{{$data->tanggal_selesai_efektif}}" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDate" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

													<!-- <input type="text" name="tanggal_selesai_efektif" style="font-size:11px;" value="" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
												<div class="col-sm-10">
													<textarea type="text" rows="6" cols="50" name="keterangan" style="font-size:11px;" value="{{$data->keterangan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
												</div>
											</div>
										</div>
									
									</div>
									
								</div>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('view_ptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
									<td>
										<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
									</td>
									<td>
										<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Proses Data</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						@endforeach
                    </form>
                    </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" language="Javascript">
   ptk_awal = document.formD.ptk_awal.value;
   document.formD.ptk_akhir.value = ptk_awal;
   peng = document.formD.ptk_yang_digantikan.value;
   document.formD.ptk_akhir.value = peng;
   tamb = document.formD.ptk_tambahan.value;
   document.formD.ptk_akhir.value = tamb;


   tksi_perusahaan = document.formD.tksi_perusahaan.value;
   document.formD.txtDisplay_tksi.value = tksi_perusahaan;
   tksi_penggantian = document.formD.tksi_penggantian.value;
   document.formD.txtDisplay_tksi.value = tksi_penggantian;
   function OnChange(value){
	var ptk_awal = parseFloat(document.formD.ptk_awal.value);
	var peng = parseFloat(document.formD.ptk_yang_digantikan.value);
	var tamb = parseFloat(document.formD.ptk_tambahan.value);
	var ptk_akhir = ptk_awal+peng+tamb;
	document.formD.ptk_akhir.value = ptk_akhir;

	var tksi_perusahaan = parseFloat(document.formD.tksi_perusahaan.value);
	var tksi_penggantian = parseFloat(document.formD.tksi_penggantian.value);
	var tksi_final = tksi_perusahaan+tksi_penggantian;
	document.formD.tksi_final.value = tksi_final;

	var selisih = ptk_akhir+tksi_final;
	document.formD.selisih.value = selisih;
   }
 </script>

<script>
$('#nama_lokasi').change(function(){
	document.getElementById("ptk_akhir").innerHTML = $('#ptk_akhir').val($('#'));
})
</script>
@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection