@extends('re.step-step.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Step-step Process Rekruitmen</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">Rekruitmen</a></li>
                                        <li class="breadcrumb-item"><a href="#!">Step-step Process Rekruitmen</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('tambah_jpr')}}"  class="btn btn-success"><span class="d-block m-t-5"> <code style="color:white;">Tambah Jabatan Proses Rekruitment</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                    <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                        <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->

                                        @php $b=1; @endphp
                                        @foreach ($step as $data)
                                        <div style="border:1px solid black;">
                                            <form action="{{ URL::to('/list_step/hapus_banyak') }}" method="POST" id="form_delete" style="margin:30px;">
                                            @csrf
                                                <input type="hidden" name="id" value="{{$data->id_step_process_recruitment}}" class="form-control form-control-sm">
                                                <b>Nama Jabatan: {{$data->nama_jabatan}}</b>
                                                <table class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead style="color:black;font-size: 12px; ">
                                                        <tr>
                                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">Urutan</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Nama Step</th>
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5; " scope="col">Keterangan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="font-size: 11px;">
                                                        @foreach ($data->details as $detail)
                                                            <tr>
                                                                <td style="padding-right:-60px;">{{$detail->no_urut}}</td>
                                                                <td style="padding-right:-60px;">{{$detail->nama_step}}</td>
                                                                <td style="padding-right:-60px;">{{$detail->keterangan}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('ubah_jpr', $data->id_step_process_recruitment)}}">Ubah</a></td>
                                                        <td>
                                                            <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                                            <button data-id="{{$data->id_step_process_recruitment}}" id="delete-step" class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;">Hapus</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                        </div>
                                        <br>
                                        @endforeach
                                       
                                    <!-- </div> -->
                                    </div>
                                </div>
                            <br>
                                       
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script>
  $(document).ready(function() {
        //hang on event of form with id=myform
        $("#form_delete").on('submit', function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            // console.log($("input[name=id]").val())
            // console.log($(this).data("id"))
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'POST',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection