@extends('re.step-step.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Edit Step-step rekruitmen</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Step-step Rekruitment</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('update')}}" method="post" enctype="multipart/form-data">
							@foreach($step as $data)
							<hr>
							{{ csrf_field() }}
							<input type="hidden" name="id_step_process_recruitment" value="{{$data->id_step_process_recruitment}}">
								<div class="form-group row">
									<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Jabatan</label>
									<div class="col-sm-10">
										<input type="text" style="font-size:11px;" name="nama_jabatan" value="{{$data->nama_jabatan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<input type="hidden" name="temp_id" id="temp_id" value="" />
									<div class="form-group row" id="qualificationInputContainer">
										<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
										<div class="col-sm-2">
											<input type="text" style="font-size:11px;" name="nama_step[]" value="{{$data->nama_step}}" class="form-control form-control-sm" required>
										</div>
										<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
										<div class="col-sm-2">
											<input type="text" style="font-size:11px;" name="no_urut[]" value="{{$data->no_urut}}" class="form-control form-control-sm" required>
										</div>
										<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
										<div class="col-sm-2">
											<textarea type="text" style="font-size:11px;" name="keterangan[]" value="{{$data->keterangan}}" class="form-control form-control-sm" required></textarea>
										</div>            
										<div class="col-sm-1">
											<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
												<i class="fa-solid fa-plus"></i>
											</button>
										</div>
									</div>
								<hr>
								<div style="width: 100%;">
									<table>
										<tr>
											<!-- <td>
												<input type="text">
											</td> -->
											<td>
												<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
											</td>
											<td>
												<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>
											<td>
												<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_step', $data->id_step_process_recruitment)}}" id="btn_delete">Hapus</a>
											</td>

										</tr>
									</table>
								</div>
							@endforeach
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection