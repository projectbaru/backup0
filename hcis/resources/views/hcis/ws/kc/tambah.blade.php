@extends('wskelascabang.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Kelas Cabang</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">kelas cabang</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
							<form action="{{route('simpan_kc')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
								<hr>
								<div class="form-group" style="width:95%;">
									<div class="">
										<div class="row">
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelas Cabang</label>
													<div class="col-sm-10">
													<input type="text" name="kode_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$random}}" readonly required>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelas Cabang</label>
													<div class="col-sm-10">
													<input type="text" name="nama_kelas_cabang" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Area Kelas Cabang</label>
													<div class="col-sm-10">
														<div class="form-check">
															<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1">
															<label class="form-check-label" for="exampleRadios1" checked>
																<select disabled name="area_kelas_cabang" style="font-size:11px;" class="form-control form-control-sm" id="select_area_kelas_cabang">
																	<option value="" selected>--- Pilih ---</option>
																	@foreach ($dataakc['looks'] as $v )
																	<option value="{{$v->area_kelas_cabang}}">{{$v->area_kelas_cabang}}</option>
																	@endforeach
																</select>
															</label>
														</div><br>
														<div class="form-check">
															<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" checked>
															<label class="form-check-label" for="exampleRadios2">
																<input type="text" name="area_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;"  id="input_area_kelas_cabang" placeholder="">
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
													<div class="col-sm-10">
													<textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal Mulai</label>
													<div class="col-sm-10">
													<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal akhir</label>
													<div class="col-sm-10">
													<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

													<!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->
													</div>
												</div>

											</div>                                                                                                 
										</div><hr>
									</div>
								</div>
								<div>
									<table>
										<tr>
											<!-- <td>
											<a href="" class="btn">Hapus</a>
										</td> -->
											<!-- <td>
											<a href="" class="btn">Hapus</a>
										</td> -->
											<td>
												<button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
											</td>
											<td>
												<a href="{{ route('list_kc') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
											</td>
											
										</tr>
									</table>
									<br>
								</div>

							</form>
							</div>
			</div>
		</div>
	</div>
</div>



			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			<script>
				$('#exampleRadios1').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", false);
					$("#input_area_kelas_cabang").attr("disabled", true);
					$("#select_area_kelas_cabang").attr("required", true);
					$("#input_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").val("");
				})
				$('#exampleRadios2').click(function(){
					$("#select_area_kelas_cabang").attr("disabled", true);
					$("#input_area_kelas_cabang").attr("disabled", false);
					$("#select_area_kelas_cabang").attr("required", false);
					$("#input_area_kelas_cabang").attr("required", true);
					$("#select_area_kelas_cabang").val("");
				})
			</script>
@endsection