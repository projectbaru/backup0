@extends('wskelascabang.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Edit Kelas Cabang</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">kelas cabang</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div> 
                            <form action="{{route('update_kc')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                <hr>
                                <div class="form-group">
                                    <div class="">
                                        @foreach($wskelascabang as $data)
                                        <div class="row">
                                            <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                        <div class="col-sm-10">
                                                        <input type="hidden" name="id_kelas_cabang" value="{{ $data->id_kelas_cabang }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelas Cabang</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="kode_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;" readonly id="colFormLabelSm" placeholder="" value="{{$data->kode_kelas_cabang}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelas Cabang</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="nama_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_kelas_cabang}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Area Kelas Cabang</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1">
                                                                <label class="form-check-label" for="exampleRadios1" checked>
                                                                    <select name="area_kelas_cabang" style="font-size:11px;" class="form-control form-control-sm" id="select_area_kelas_cabang">
                                                                        <option value="" selected>--- Pilih ---</option>
                                                                        @foreach ($dataakc['looks'] as $v )
                                                                        <option value="{{$v->area_kelas_cabang}}" {{ $v->area_kelas_cabang == $data->area_kelas_cabang ? 'selected' : NULL }}>{{$v->area_kelas_cabang}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </label>
                                                            </div><br>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" checked>
                                                                <label class="form-check-label" for="exampleRadios2">
                                                                    <input type="text" name="area_kelas_cabang" value="{{$data->area_kelas_cabang}}" class="form-control form-control-sm" style="font-size:11px;"  id="input_area_kelas_cabang" placeholder="">
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Area Kelas Cabang</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="area_kelas_cabang" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->area_kelas_cabang}}">
                                                        </div>
                                                    </div> -->
                                            </div>
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                    <div class="col-sm-10">
                                                    <textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->keterangan}}" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal Mulai</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">
                                                    <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Efektif Tanggal Akhir</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}" placeholder="Tanggal selesai efektif"/>

                                                    <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                            </div> 
                                        </div><hr>
                                        
                                        @endforeach
                                        <div>
                                            <table>
                                                <tr>
                                                <!-- <td>
                                                    <a href="" class="btn">Hapus</a>
                                                </td> -->
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm" href="{{route('hapus_kc',$data->id_kelas_cabang)}}" class="btn" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete">Hapus</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('list_kc') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_kc') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection
