@extends('wskelascabang.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Kelas Cabang</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">kelas cabang</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
                    <form action="{{route('simpan_kc')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
						<hr>
						<div>
							<div class="" style="width:95%" >
                           
                            @foreach($wskelascabang as $data)
                            <table>
                                <tr>
                                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_kc', $data->id_kelas_cabang)}}">Ubah</a></td>
                                    <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
                                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_kc', $data->id_kelas_cabang)}}">Hapus</a></td>
                                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_kc')}}">Batal</a></td>
                                    <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                    <!-- <td>
                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                    </td> -->
                                </tr>
                            </table><br>
                            <!-- <b style="color:black;">Informasi Perusahaan</b> -->
                            <div class="row">
                                <div class="col" >
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Kode Kelas Cabang</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="kode_kelas_cabang" value="KKC-{{now()->isoFormat('dmYYs')}}">
                                                {{$data->kode_kelas_cabang}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Kelas Cabang</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_kelas_cabang" value="{{$data->nama_kelas_cabang}}">{{$data->nama_kelas_cabang}}</td>
                                        </tr>
                                        <tr>
                                            <td>Area Kelas Cabang</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="area_kelas_cabang" value="{{$data->area_kelas_cabang}}">{{$data->area_kelas_cabang}}</td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div class="col">
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Keterangan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">
                                                    {{$data->keterangan}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Efektif Tanggal Mulai</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                            </tr>
                                            <tr>
                                                <td>Efektif Tanggal Akhir</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                            </tr>
                                        </table><br><br>
                                    </div>                                         
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </form>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
