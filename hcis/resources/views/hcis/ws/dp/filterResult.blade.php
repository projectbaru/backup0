@extends('wsdeskripsipekerjaan.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Deskripsi Pekerjaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">deskripsi pekerjaan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamdp')}}" ><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                        <form action="{{ URL::to('/list_dp/hapus_banyak') }}" method="POST" id="form_delete">
                                            @csrf
                                            <table id="example" class="table-responsive-xl table-bordered table-striped table-hover table-xl" style="width: 100%;font-size: 12px; border:1px solid #d9d9d9;">
                                                <thead>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                    @for($i = 0; $i < count($th); $i++) <!-- @if($th[$i]=='id_deskripsi_pekerjaan' ) <th>
                                                        </th>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_deskripsi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Deskripsi</th>
                                                        @endif
                                                        @if($th[$i] == 'nama_posisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi</th>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                        @endif

                                                        @if($th[$i] == 'deskripsi_pekerjaan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Deskripsi Pekerjaan</th>
                                                        @endif


                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                        @endif
                                                        @if($th[$i] == 'nomor_dokumen')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Dokumen</th>
                                                        @endif
                                                        @if($th[$i] == 'edisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Edisi</th>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_edisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Edisi</th>
                                                        @endif
                                                        @if($th[$i] == 'nomor_revisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Revisi</th>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_revisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Revisi</th>
                                                        @endif
                                                        @if($th[$i] == 'nama_jabatan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Jabatan</th>
                                                        @endif
                                                        @if($th[$i] == 'nama_karyawan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Karyawan</th>
                                                        @endif
                                                        @if($th[$i] == 'divisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Divisi</th>
                                                        @endif
                                                        @if($th[$i] == 'lokasi_kerja')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Kerja</th>
                                                        @endif
                                                        @if($th[$i] == 'dibuat_oleh')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Dibuat Oleh</th>
                                                        @endif
                                                        @if($th[$i] == 'diperiksa_oleh')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Diperiksa Oleh</th>
                                                        @endif
                                                        @if($th[$i] == 'fungsi_jabatan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Fungsi Jabatan</th>
                                                        @endif
                                                        @if($th[$i] == 'tanggung_jawab')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tangguna Jawab</th>
                                                        @endif
                                                        @if($th[$i] == 'departemen')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Departemen</th>
                                                        @endif
                                                        @if($th[$i] == 'kode_posisi')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Posisi</th>
                                                        @endif
                                                        @if($th[$i] == 'nama_lokasi_kerja')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja</th>
                                                        @endif
                                                        @if($th[$i] == 'nama_pengawas')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Pengawas</th>
                                                        @endif
                                                        @if($th[$i] == 'lingkup_aktivitas')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Lingkup Aktivitas</th>
                                                        @endif
                                                        @if($th[$i] == 'id_logo_perusahaan')
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">ID Logo Perusahaan</th>
                                                        @endif

                                                        @if($i == count($th) - 1)
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                        @endif
                                                        @endfor
                                                </thead>
                                                <tbody style="font-size:11px;">
                                                    @php $b=1; @endphp
                                                    @foreach($query as $row)
                                                    <tr>
                                                        <td>{{$b++;}}</td>
                                                        @for($i = 0; $i < count($th); $i++) @if($th[$i]=='kode_deskripsi' ) <td>{{ $row->kode_deskripsi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi')
                                                            <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'deskripsi_pekerjaan')
                                                            <!-- <td>{{ $row->deskripsi_pekerjaan ?? 'NO DATA' }}</td> -->
                                                            <td><?= $row->detail_deskripsi ?></td>
                                                            @endif


                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nomor_dokumen')
                                                            <td>{{ $row->nomor_dokumen ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'edisi')
                                                            <td>{{ $row->edisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_edisi')
                                                            <td>{{ $row->tanggal_edisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nomor_revisi')
                                                            <td>{{ $row->nomor_revisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_revisi')
                                                            <td>{{ $row->tanggal_revisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_jabatan')
                                                            <td>{{ $row->nama_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_karyawan')
                                                            <td>{{ $row->nama_karyawan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'divisi')
                                                            <td>{{ $row->divisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'lokasi_kerja')
                                                            <td>{{ $row->lokasi_kerja ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'dibuat_oleh')
                                                            <td>{{ $row->dibuat_oleh ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'diperiksa_oleh')
                                                            <td>{{ $row->diperiksa_oleh ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'fungsi_jabatan')
                                                            <td>{{ $row->fungsi_jabatan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'tanggung_jawab')
                                                            <td>{{ $row->tanggung_jawab ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'departemen')
                                                            <td>{{ $row->departemen ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'kode_posisi')
                                                            <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_lokasi_kerja')
                                                            <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'nama_pengawas')
                                                            <td>{{ $row->nama_pengawas ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'lingkup_aktivitas')
                                                            <td>{{ $row->lingkup_aktivitas ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($th[$i] == 'id_logo_perusahaan')
                                                            <td>{{ $row->id_logo_perusahaan ?? 'NO DATA' }}</td>
                                                            @endif
                                                            @if($i == count($th) - 1)
                                                            <td>
                                                                <a href="{{ URL::to('/list_dp/detail/'.$row->id_deskripsi_pekerjaan) }}">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                <a href="{{ URL::to('/list_dp/edit/'.$row->id_deskripsi_pekerjaan) }}">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_deskripsi_pekerjaan }}" id="multiDelete">
                                                            </td>
                                                            @endif
                                                            @endfor
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td><a class="btn btn-success btn-sm" href="{{route('tambah_dp')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                    <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                    <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection