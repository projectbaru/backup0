@extends('wsdeskripsipekerjaan.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Deskripsi Pekerjaan</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">deskripsi pekerjaan</a></li>
										<li class="breadcrumb-item"><a href="#!">detail deskripsi pekerjaan</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div> 
                <form action="{{route('simpan_dp')}}" method="post"> {{ csrf_field() }}
                    <hr>
                    <table>
                        <tr>
                            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_dp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Ubah</a></td>
                            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_dp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Hapus</a></td>
                            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_dp')}}">Batal</a></td>
                        </tr>
                    </table>
                        <div>
                            <b>Informasi Deskripsi Pekerjaan</b>
                            <div class="row">
                                <div class="col-6">
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Kode Posisi</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="kode_posisi" value="{{$wsdeskripsipekerjaan->kode_posisi}}">{{ $wsdeskripsipekerjaan->kode_posisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Posisi</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_posisi" value="{{$wsdeskripsipekerjaan->nama_posisi}}">{{ $wsdeskripsipekerjaan->nama_posisi ?? 'NO DATA' }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><br></td>
                                            <td></td>
                                        </tr>
                                        {{-- {{dd($deskripsiPekerjaan)}} --}}
                                        {{-- <tr>
                                                <td>Deskripsi Pekerjaan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$deskripsiPekerjaan}}">{{ $deskripsiPekerjaan ?? 'NO DATA' }}</td>
                                                <td></td>
                                                <td></td>
                                                <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$loop->iteration }}">{{ $loop->iteration }}</td>
                                        </tr> --}}
                                            <br>
                                        @foreach ($deskripsiPekerjaan as $descPekerjaan)
                                            <tr>
                                                <td>Deskripsi Pekerjaan {{ $loop->iteration }}</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="deskripsi_pekerjaan[]" value="{{ $descPekerjaan['deskripsi'] }}">{{ $descPekerjaan['deskripsi'] ?? 'NO DATA' }}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>PDAC:</td>
                                                <td><input type="hidden" name="pdca[]" value="{{ $descPekerjaan['pdca'] }}">{{ $descPekerjaan['pdca'] ?? 'NO DATA' }}</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>BSC:</td>
                                                <td><input type="hidden" name="bsc[]" value="{{ $descPekerjaan['bsc'] }}">{{ $descPekerjaan['bsc'] ?? 'NO DATA' }}</td>
                                            </tr>
                                            <tr>                                                
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td><br></td>
                                            <td></td>
                                        </tr>
                                        <!-- {{-- <tr>
                                            <td>Kualifikasi Jabatan</td>
                                            <td>:</td>
                                            <td>{{ $kualifikasiJabatan ?? 'NO DATA' }}</td>
                                        </tr> --}}
                                        @foreach ($kualifikasiJabatan as $jabatan)
                                            <tr>
                                                <td>Kualifikasi Jabatan {{ $loop->iteration }}</td>
                                                <td>:</td>
                                                <td>{{ $jabatan['jabatan'] ?? 'NO DATA' }}</td>
                                            </tr>
                                        @endforeach
                                        {{-- {{dd($tanggungJawab)}} --}}
                                        @foreach ($tanggungJawab as $tanggung)
                                            <tr>
                                                <td>Tanggung Jawab {{ $loop->iteration }}</td>
                                                <td>:</td>
                                                <td>{{ $tanggung ?? 'NO DATA' }}</td>
                                            </tr>
                                        @endforeach
                                        @foreach ($wewenang as $w)
                                            <tr>
                                                <td>Wewenang {{ $loop->iteration }}</td>
                                                <td>:</td>
                                                <td>{{ $w ?? 'NO DATA' }}</td>
                                            </tr>
                                        @endforeach -->
                                        
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <b>Rekaman Informasi</b>
                                <table style="font-size:12px;">
                                <tr>
                                    <td>Tanggal Mulai Efektif</td>
                                    <td>:</td>
                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$wsdeskripsipekerjaan->tanggal_mulai_efektif}}">{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Selesai Efektif</td>
                                    <td>:</td>
                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$wsdeskripsipekerjaan->tanggal_selesai_efektif}}">{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                </tr>
                                <tr>
                                    <td>Keterangan</td>
                                    <td>:</td>
                                    <td><input type="hidden" name="keterangan" value="{{$wsdeskripsipekerjaan->keterangan}}">{{ $wsdeskripsipekerjaan->keterangan ?? 'NO DATA' }}</td>
                                </tr>
                                </table>               
                        </div>
                     
                </form>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection