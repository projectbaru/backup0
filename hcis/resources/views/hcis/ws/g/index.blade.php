@extends('wsgolongan.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Golongan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">golongan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamg')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                <form action="{{ URL::to('/list_g/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                    <thead style="color:black;font-size: 12px; ">
                                        <tr>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Kode Golongan</th>
                                            <!-- {{-- <th scope="col">Logo Perusahaan</th> --}} -->
                                            <th  style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Nama Golongan</th>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Nama Perusahaan</th>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;"scope="col">V</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 11px; ">
                                        @php $b=1; @endphp
                                        @foreach ($wsgolongan as $data)
                                        {{-- {{dd($data)}} --}}
                                            <tr>
                                                <td>{{ $b++; }}</td>
                                                <td>{{ $data->kode_golongan }}</td>
                                                <!-- {{-- <td>{{$data -> logo_perusahaan}}</td> --}} -->
                                                <td>{{ $data->nama_golongan }}</td>
                                                <td>{{ $data->nama_perusahaan }}</td>
                                                <td>
                                                    <a href="{{ URL::to('/list_g/detail/'.$data->id_golongan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a href="{{ URL::to('/list_g/edit/'.$data->id_golongan) }}" class="">Edit</a>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="multiDelete[]" value="{{ $data->id_golongan }}" id="multiDelete">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_g')}}">Tambah</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                        </td>
                                    </tr>
                                </table>
                                </form>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection