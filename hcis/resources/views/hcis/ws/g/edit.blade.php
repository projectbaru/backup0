@extends('wsgolongan.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Edit Golongan</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">Golongan</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                            <form action="{{route('update_g')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                <hr>
                                <div class="form-group" style="width:95%;">
                                    @foreach($wsgolongan as $data)
                                    <div class="row">
                                        <div class="col">
                                        <b>Informasi Golongan</b>

                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                    <div class="col-sm-10">
                                                    <input type="hidden" name="id_golongan"  id="tg_id"  value="{{ $data->id_golongan }}" class="form-control form-control-sm"  name="tg_id" id="tg_id" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                    <div class="col-sm-10">
                                                    <select name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                                        <option value=""></option>
                                                        @foreach ($np['looks'] as $np)
                                                            <option value="{{$np->nama_perusahaan}}" {{ $np->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$np->nama_perusahaan}}</option>
                                                        @endforeach
                                                    </select>
                                                    <!-- <input type="text" required name="tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tingkat_golongan}}"> -->
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Golongan</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" readonly required name="kode_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_golongan}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Golongan</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" required name="nama_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_golongan}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Golongan</label>
                                                    <div class="col-sm-10">
                                                    <select name="tingkat_golongan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                                        <option value=""></option>
                                                        @foreach ($tg['looks'] as $tg)
                                                            <option value="{{$tg->nama_tingkat_golongan}}" {{ $tg->nama_tingkat_golongan == $data->tingkat_golongan ? 'selected' : NULL }}>{{$tg->nama_tingkat_golongan}}</option>
                                                        @endforeach
                                                    </select>
                                                    <!-- <input type="text" required name="tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tingkat_golongan}}"> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Golongan</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" required name="urutan_golongan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_golongan}}" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                                    <div class="col-sm-10">
                                                    <input type="number" required name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tampilan}}" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                                    <div class="col-sm-10">
                                                    <select name="tingkat_posisi" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                                        <option value=""></option>
                                                        @foreach ($tp['looks'] as $tp)
                                                            <option value="{{$tp->tingkat_posisi}}" {{ $tp->tingkat_posisi == $data->tingkat_posisi ? 'selected' : NULL }}>{{$tp->tingkat_posisi}}</option>
                                                        @endforeach
                                                    </select>
                                                    <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                                                    <div class="col-sm-10">
                                                    <select name="jabatan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                                        <option value=""></option>
                                                        @foreach ($dj['looks'] as $j)
                                                            <option value="{{$j->nama_jabatan}}" {{ $j->nama_jabatan == $data->jabatan ? 'selected' : NULL }}>{{$j->nama_jabatan}}</option>
                                                        @endforeach
                                                    </select>
                                                    <!-- <select name="jabatan" style="font-size:11px;" class="form-control form-control-sm vendorId" >
                                                        <option value=""></option>
                                                        
                                                    </select> -->

                                                    <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->jabatan}}" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                                                    <div class="col-sm-10">
                                                    <textarea type="text" name="deskripsi" rows="6" cols="50" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi}}" id="colFormLabelSm" placeholder="">{{$data->deskripsi}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <b>Rekaman Informasi</b>
                                        <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                <div class="col-sm-10">
                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}"  placeholder="Tanggal mulai efektif">

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                <div class="col-sm-10">
                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai_efektif}}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                <div class="col-sm-10">
                                                <textarea type="text" name="keterangan" rows="6" cols="50" class="form-control form-control-sm" value="{{$data->keterangan}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="" style="">
                                        <table>
                                            <tr>
    
                                            <!-- <td>
                                                <a href="" class="btn">Hapus</a>
                                            </td> -->
                                                <td>
                                                    <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                </td>
                                                <td>
                                                    <a class="btn btn-danger btn-sm" href="{{route('hapus_g',$data->id_golongan)}}" id="btn_delete" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('list_g') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div><br>
                                </div>
                            </form>
                            </div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>

@endsection

@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_g') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection