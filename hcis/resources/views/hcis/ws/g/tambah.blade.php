@extends('wsgolongan.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Golongan</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">Golongan</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
                                    <form action="{{route('simpan_g')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group" style="width:95%;">
                                            <div class="row">
                                                <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Golongan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="kode_golongan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""  value="{{$random}}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                            <div class="col-sm-10">
                                                            <select name="nama_perusahaan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($np['looks'] as $np )
																<option value="{{$np->nama_perusahaan}}">{{$np->nama_perusahaan}}</option>
																@endforeach
															</select>	
                                                            <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Golongan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="nama_golongan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Golongan</label>
                                                            <div class="col-sm-10">
                                                            <select name="tingkat_golongan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($tg['looks'] as $tg )
																<option value="{{$tg->nama_tingkat_golongan}}">{{$tg->nama_tingkat_golongan}}</option>
																@endforeach
															</select>	
                                                            <!-- <input type="text" name="tingkat_golongan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Golongan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="urutan_golongan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                                            <div class="col-sm-10">
                                                            <input type="number" name="urutan_tampilan" class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                                            <div class="col-sm-10">
                                                            <select name="tingkat_posisi" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($tp['looks'] as $tp )
																<option value="{{$tp->tingkat_posisi}}">{{$tp->tingkat_posisi}}</option>
																@endforeach
															</select>	
                                                            <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                                                            <div class="col-sm-10">
                                                            <select name="jabatan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($dj['looks'] as $j )
																<option value="{{$j->nama_jabatan}}">{{$j->nama_jabatan}}</option>
																@endforeach
															</select>	
                                                            <!-- <input type="text" name="jabatan" class="form-control form-control-sm" style="font-size:11px;"  id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" name="deskripsi" class="form-control form-control-sm" style="font-size:11px;" rows="6" cols="50" id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b><br><br>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" name="keterangan" rows="6" cols="50" class="form-control form-control-sm"  style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                        <hr>                            
                                                    </div>
                                                </div>
                                            </div>
                                            <div >
                                                <table>
                                                    <tr>
            
                                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_g') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div><br>
                                        </div>
                                    
                                    </form>
                                    </div>
			</div>
		</div>
	</div>
</div>


@endsection
