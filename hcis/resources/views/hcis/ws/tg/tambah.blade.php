@extends('wstingkatgolongan.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Tambah Tingkat Golongan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">tingkat golongan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('simpan_tg')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="items" id="items">
                        <hr>
                        <div class="form-group " style="width:95%;">
                            <div class="">
                                <b style="color:black;">Informasi Tingkat Golongan</b><br><br>
                                <div class="row" style="color:black;">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                            <div class="col-sm-10">
                                                <select name="nama_perusahaan" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($np['looks'] as $np )
                                                    <option value="{{$np->nama_perusahaan}}">{{$np->nama_perusahaan}}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input type="text" name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Golongan</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="kode_tingkat_golongan" class="form-control form-control-sm" value="{{$random}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Golongan</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nama_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                            <div class="col-sm-10">
                                                <input type="number" name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>


                                                        </div> -->
                                </div>
                                <hr>
                                <b style="color:black;">List Tingkat Golongan</b>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-5">
                                        <select id="defaultList" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                            @foreach($items as $item)
                                            <option value="{{ $item->id_golongan }}">{{ $item->nama_golongan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <div style="margin-top: 70px;border-radius:5px;">
                                            <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                                <i class="fa-solid fa-arrow-right fa-sm" style="color:black;"></i>
                                            </button>
                                            <br>
                                            <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                                <i class="fa-solid fa-arrow-left fa-sm" style="color:black;"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;"></select>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->

                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_tg') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
@include('js.wstingkatgolongan.tambah')

@endsection