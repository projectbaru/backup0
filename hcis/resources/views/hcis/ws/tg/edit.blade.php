@extends('wstingkatgolongan.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Edit Tingkat Golongan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">tingkat golongan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('update_tg') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="items" id="items" value="<?= $selectedItems ?>">
                        <input type="hidden" name="$id_tingkat_golongan" id="$id_tingkat_golongan" value="{{ $wstingkatgolongan->id_tingkat_golongan }}">
                        <hr>
                        <div class="form-group" style="width:95%;">
                            <b style="color:black;">Informasi Tingkat Golongan</b><br><br>
                            <div class="row" style="color:black;">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;"></label>
                                        <div class="col-sm-10">
                                            <input type="hidden" name="id_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->id_tingkat_golongan }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                        <div class="col-sm-10">
                                            <select name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm vendorId">
                                                <option value=""></option>
                                                @foreach ($nama['looks'] as $nm)
                                                <option value="{{$nm->nama_perusahaan}}" {{ $nm->nama_perusahaan == $wstingkatgolongan->nama_perusahaan ? 'selected' : NULL }} >{{$nm->nama_perusahaan}}</option>
                                                @endforeach
                                            </select>
                                            <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->nama_perusahaan }}"> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Golongan</label>
                                        <div class="col-sm-10">
                                            <input type="text" readonly required name="kode_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="kode_tingkat_golongan" value="{{ $wstingkatgolongan->kode_tingkat_golongan }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Golongan</label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="nama_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="nama_tingkat_golongan" value="{{ $wstingkatgolongan->nama_tingkat_golongan }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                        <div class="col-sm-10">
                                            <input type="number" required name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" id="urutan_tampilan" value="{{ $wstingkatgolongan->urutan_tampilan }}">
                                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                        </div>
                                    </div>
                                </div>
                  
                            </div>
                            <hr>
                            <b style="color:black;">List Tingkat Golongan</b>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-5">
                                    <select id="defaultList" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                        @foreach($items as $item)
                                        <option value="{{ $item->id_golongan }}">{{ $item->nama_golongan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div>
                                    <div style="margin-top: 70px;border-radius:5px;">
                                        <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-right fa-sm" style="color:black;"></i>
                                        </button>
                                        <br>
                                        <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-left fa-sm" style="color:black;"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    @if(count($selectedOptionItems) > 0)
                                    <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                        @for($i = 0; $i < count($selectedOptionItems); $i++) @foreach($items as $item) @if($item->id_golongan == $selectedOptionItems[$i])
                                            <option value="{{ $item->id_golongan }}">{{ $item->nama_golongan }}</option>
                                            @endif
                                            @endforeach
                                            @endfor
                                    </select>
                                    @else
                                    <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;"></select>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <b style="color:black;">Rekaman Informasi</b>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" value="{{ $wstingkatgolongan->tanggal_mulai_efektif }}" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

                                            <!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wstingkatgolongan->tanggal_mulai_efektif }}"> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" value="{{ $wstingkatgolongan->tanggal_selesai_efektif }}"/>

                                            <!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wstingkatgolongan->tanggal_selesai_efektif }}"> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->keterangan }}">{{$wstingkatgolongan->keterangan}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_tg') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
@include('js.wstingkatgolongan.edit')
@endsection