@extends('wskelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Tambah Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">kelompok jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                                        <form action="{{route('simpan_kj')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            
                                            <div class="form-group">
                                                <div class="">
                                                    <hr>
                                                    <b style="color:black;">Informasi</b>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_kelompok_jabatan" value="{{$random}}" readonly   required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nama_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="deskripsi_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="dari_golongan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datang['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" name="dari_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="sampai_golongan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datang['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <!-- <input type="text" name="sampai_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select name="grup_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="-">Tidak Ada</option>
                                                                        <option value="admin">Admin</option>
                                                                        <option value="operasional">Operasional</option>
                                                                    </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tipe_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="-">Tidak Ada</option>
                                                                        <option value="sales">Sales</option>
                                                                        <option value="non_sales">Non Sales</option>
                                                                        <option value="support">Support</option>
                                                                        <option value="operation">Operation</option>
                                                                    </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="deskripsi_pekerjaan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datas['looks'] as $p )
                                                                        <option value="{{$p->deskripsi_pekerjaan}}">{{$p->deskripsi_pekerjaan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">Rekaman Informasi</b>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <hr>                              
                                                </div>
                                            </div>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                    <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_kj') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
            
                                        </form>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection