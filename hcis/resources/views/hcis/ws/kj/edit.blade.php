@extends('wskelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Edit Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">kelompok jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                                    <form action="{{route('update_kj')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        @foreach($wskelompokjabatan as $data)

                                        <div class="form-group" style="width:95%;">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                            <div class="col-sm-10">
                                                            <input type="hidden" name="id_kelompok_jabatan" id="colFormLabelSm"  value="{{ $data->id_kelompok_jabatan }}" class="form-control form-control-sm" id="tg_id" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required readonly name="kode_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_kelompok_jabatan}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="nama_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_kelompok_jabatan}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Kelompok Jabatan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="deskripsi_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi_kelompok_jabatan}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select required style="font-size:11px;" name="dari_golongan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datadg['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" name="dari_golongan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->dari_golongan}}" id="colFormLabelSm" placeholder=""></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select required style="font-size:11px;" name="sampai_golongan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datasg['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <select name="grup_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="-" {{ $data->grup_jabatan == '' ? 'selected' : NULL }}>Tidak Ada</option>
                                                                    <option value="admin" {{ $data->grup_jabatan == 'admin' ? 'selected' : NULL }}>Admin</option>
                                                                    <option value="operasional" {{ $data->grup_jabatan == 'operasional' ? 'selected' : NULL }}>Operasional</option>
                                                                </select>
                                                            <!-- <input type="text" name="grup_jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->grup_jabatan}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <select name="tipe_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="-" {{ $data->tipe_jabatan == '' ? 'selected' : NULL }}>Tidak Ada</option>
                                                                    <option value="sales" {{ $data->tipe_jabatan == 'sales' ? 'selected' : NULL }}>Sales</option>
                                                                    <option value="non_sales" {{ $data->tipe_jabatan == 'non_sales' ? 'selected' : NULL }}>Non Sales</option>
                                                                    <option value="support" {{ $data->tipe_jabatan == 'support' ? 'selected' : NULL }}>Support</option>
                                                                    <option value="operation" {{ $data->tipe_jabatan == 'operation' ? 'selected' : NULL }}>Operation</option>
                                                                </select>
                                                            <!-- <input type="text" name="tipe_jabatan" class="form-control form-control-sm" value="{{$data->tipe_jabatan}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan</label>
                                                            <div class="col-sm-10">
                                                                <select required style="font-size:11px;" name="deskripsi_pekerjaan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datas['looks'] as $p )
                                                                        <option value="{{$p->deskripsi_pekerjaan}}" {{ $p->deskripsi_pekerjaan == $data->deskripsi_pekerjaan ? 'selected' : NULL }}>{{$p->deskripsi_pekerjaan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" id="colFormLabelSm"  value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai_efektif}}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />

                                                            <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" value="{{$data->keterangan}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div> 
                                                </div>
                                                <div >
                                                    <table>
                                                        <tr>
                
                                                        <!-- <td>
                                                            <a href="" class="btn">Hapus</a>
                                                        </td> -->
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_kj', $data->id_kelompok_jabatan)}}" style="font-size:11px;border:none;border-radius:5px;" id="btn_delete">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_kj') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </form>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>

@endsection
@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_kj') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection
              