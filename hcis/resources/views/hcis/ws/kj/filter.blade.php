@extends('wskelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Filter Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">Kelompok Jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>                      
                        <form action="{{route('update_tamkj')}}" class="" method="POST">
                            @csrf
                            <input type="hidden" name="displayedColumn" id="displayedColumnInput" value="kode_kelompok_jabatan,nama_kelompok_jabatan,deskripsi_kelompok_jabatan">
                            <div class="my-3">
                                <div style="">
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 1. Masukkan Nama</h6>
                                    <br>
                                    <div class="row mb-3">
                                        <label for="nama_kelompok_jabatan" class="col-sm-2 col-form-label" style="font-size:12px;">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="text" style="font-size:10px;" name="nama_kelompok_jabatan" class="form-control form-control-sm" id="nama_kelompok_jabatan" value="{{$filters ? $temp->nama_kelompok_jabatan : ''}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="kode_kelompok_jabatan" class="col-sm-2 col-form-label" style="font-size:12px;">Kode</label>
                                        <div class="col-sm-10">
                                            <input type="text" style="font-size:10px;" name="kode_kelompok_jabatan" class="form-control form-control-sm" id="kode_kelompok_jabatan" value="{{$filters ? $temp->kode_kelompok_jabatan : ''}}">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 2. Spesifikasi Kriteria Pencarian</h6>
                                    <table class="table table-borderless">
                                        <thead style="font-size:12px;">
                                            <th>Halaman</th>
                                            <th>Operator</th>
                                            <th>Nilai</th>
                                            <th>Aksi</th>
                                        </thead>
                                        <tbody id="queryInputContainer" style="font-size:10px;">
                                            @if($filters && !empty($temp->query_field))
                                            @for($k= 0; $k < count($temp->query_field); $k++)
                                                <tr>
                                                    <td>
                                                        <select name="queryField[]" id="queryField1" class="queryField form-control" data-number="1" style="font-size:10px;height: calc(2.28rem + 2px);">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++) <option class="form-control" style="font-size:10px;" value="{{ $fields[$i]['value'] }}" <?= $fields[$i]['value'] == $temp->query_field[$k] ? 'selected' : '' ?>>{{ $fields[$i]['text'] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="queryOperator[]" id="queryOperator1" class="queryOperator form-control " style="font-size:10px;height: calc(2.28rem + 2px);" data-number="{{($k+1)}}">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < count($operators); $i++) <option class="form-control" style="font-size:10px;" value="{{ $operators[$i] }}" <?= $operators[$i] == $temp->query_operator[$k] ? 'selected' : '' ?>>{{ $operators[$i] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td id="queryValueContainer{{($k+1)}}">
                                                        <input type="text" name="queryValue[]" class="form-control form-control-sm" style="font-size:10px;height: calc(2.28rem + 2px);" id="queryValue{{($k+1)}}" value="{{ $temp->query_value[$k] }}">
                                                    </td>
                                                    <td>
                                                        @if($k == 0)
                                                        <button type="button" class="btn btn-sm btn-primary addQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-plus"></i>
                                                        </button>
                                                        @else
                                                        <button type="button" class="btn btn-sm btn-danger removeQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-minus"></i>
                                                        </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endfor
                                                @else
                                                <tr>
                                                    <td>
                                                        <select name="queryField[]" id="queryField1" class="queryField form-control" data-number="1" style="font-size:10px;height: calc(2.28rem + 2px);">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++) <option class="form-control" style="font-size:10px;" value="{{ $fields[$i]['value'] }}">{{ $fields[$i]['text'] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="queryOperator[]" id="queryOperator1" class="queryOperator form-control " style="font-size:10px;height: calc(2.28rem + 2px);" data-number="1">
                                                            <option class="form-control " style="font-size:10px;" value="" selected null>-- kosong --</option>
                                                            @for($i = 0; $i < count($operators); $i++) <option class="form-control" style="font-size:10px;" value="{{ $operators[$i] }}">{{ $operators[$i] }}</option>
                                                                @endfor
                                                        </select>
                                                    </td>
                                                    <td id="queryValueContainer1">
                                                        <input type="text" name="queryValue[]" class="form-control form-control-sm" style="font-size:10px;height: calc(2.28rem + 2px);" id="queryValue1">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-primary addQueryInput" style="border-radius:5px;border:1px solid white;font-size:10px;">
                                                            <i class="fa-solid fa-plus"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="">
                                    <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 3. Pilih halaman yang ditampilkan</h6>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h5 style="font-size:12px;text-align:center;color:black;">Halaman Tersedia</h5>
                                            <select id="availableColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                @for($p = 0; $p < count($damn); $p++) 
                                                <option value="{{ $damn[$p]['value'] }}" style="<?= $damn[$p]['style'] ?>">{{ $damn[$p]['text'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="">
                                            <div style="margin-top: 70px;border-radius:5px;">
                                                <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-right fa-sm" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-left fa-sm" style="color:black;"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h5 style="font-size:12px;text-align:center;color:black;">Halaman yang dipilih</h5>
                                            <select id="displayedColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                @if($filters)
                                                @foreach($temp->select as $s)
                                                <option value="{{ $s}}"><?= ucwords(str_replace("_", " ", $s)) ?></option>
                                                @endforeach
                                                @else
                                                <option value="kode_kelompok_jabatan">Kode Kelompok Jabatan</option>
                                                <option value="nama_kelompok_jabatan">Nama Kelompok Jabatan</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="">
                                            <div style="margin-top: 50px;border-radius:5px;">
                                                <button class=" btn btn-sm" type="button" id="movetoFirst" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrows-up-to-line fa-sm" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="moveup" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-up fa-1x" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="movedown" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrow-down fa-1x" style="color:black;"></i>
                                                </button>
                                                <br>
                                                <button class=" btn btn-sm" type="button" id="movetoLast" style="border-bottom:1px solid white;">
                                                    <i class="fa-solid fa-arrows-down-to-line fa-sm" style="color:black;"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="container-fluid form-group form-group-sm">
                                            <br>
                                            <div style="font-size: 11px;">
                                                <input type="submit" value="Submit" class="btn btn-primary btn-sm" style="border-radius:5px;font-size:11px;">
                                                <a href="{{ route('list_kj') }}" value="batal" class="btn btn-danger btn-sm" style="border-radius:5px;font-size:11px;">Batal</a>
                                            </div>
                                        </div>
                                    </div><br>
                                </div><br>
                            </div><br>
                                </div><br>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@include('js.wsperusahaan.filter')
@endsection

