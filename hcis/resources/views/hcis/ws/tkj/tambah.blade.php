@extends('wstingkatkelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Tambah Tingkat Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">tingkat kelompok jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                                        <form action="{{route('simpan_tkj')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            <hr>
                                            <div class="form-group">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                
                                                                    <input type="text"  name="kode_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$random}}" readonly required>             
                                                                
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="nama_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">Kelompok jabatan</b>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Kelompok Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <select name="kode_kelompok_jabatan" id="kode_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datas['looks'] as $k )
                                                                    <option value="{{$k->kode_kelompok_jabatan}}" data-name ="{{$k->nama_kelompok_jabatan}}">{{$k->kode_kelompok_jabatan}}</option>
                                                                    @endforeach
                                                                </select>		
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Kelompok Jabatan</label>
                                                            <div class="col-sm-10">
                                                            {{-- <input type="text" required name="nama_kelompok_jabatan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                                                            <select name="nama_kelompok_jabatan" id="nama_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                                                <option value="" selected disabled>--- Pilih ---</option>
                                                                @foreach ($datas['looks'] as $p )
                                                                <option value="{{$p->nama_kelompok_jabatan}}" data-kode ="{{$p->kode_kelompok_jabatan}}">{{$p->nama_kelompok_jabatan}}</option>
                                                                @endforeach
                                                            </select>	
                                                            </div>
                                                        </div>


                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="kode_kelompok_jabatan" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datas['looks'] as $p )
                                                                        <option value="{{$p->kode_kelompok_jabatan}}">{{$p->kode_kelompok_jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="nama_kelompok_jabatan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datakj['looks'] as $p )
                                                                        <option value="{{$p->nama_kelompok_jabatan}}">{{$p->nama_kelompok_jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="nama_kelompok_jabatan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                                <div class="col-sm-10">
                                                                <!-- <input type="text" required name="dari_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                    <select required name="dari_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datang['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="sampai_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datang['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                            
                                                        </div>
                                                       
                                                    </div>  
                                                    <hr>
                                                    <b style="color:black;">Informasi Lainnya</b>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_mulai" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai" placeholder="Tanggal mulai">

                                                                <!-- <input type="date" required name="tanggal_mulai" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai" placeholder="Tanggal selesai"/>

                                                                <!-- <input type="date" name="tanggal_selesai" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan"  id="colFormLabelSm" class="form-control form-control-sm" rows="4" cols="50"  style="font-size:11px;" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_tkj') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                                <br>
                                            </div>
            
                                        </form>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script>
        $('#kode_kelompok_jabatan').change(function(){
            $('#nama_kelompok_jabatan').val($('#kode_kelompok_jabatan option:selected').data('name'));
        })
        $('#nama_kelompok_jabatan').change(function(){
            $('#kode_kelompok_jabatan').val($('#nama_kelompok_jabatan option:selected').data('kode'));
        })
    </script>
@endsection

