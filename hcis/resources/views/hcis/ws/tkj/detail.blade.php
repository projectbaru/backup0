@extends('wstingkatkelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Detail Tingkat Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">tingkat kelompok jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <form action="{{route('simpan_tkj')}}" method="post">
                                <hr>
                                {{ csrf_field() }}
                                @foreach($wstingkatkelompokjabatan as $data)
                                <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_tingkat_kelompok_jabatan }}">

                                <table>
                                    <tr>
                                        
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_tkj', $data->id_tingkat_kelompok_jabatan)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_tkj', $data->id_tingkat_kelompok_jabatan)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_tkj')}}">Batal</a></td>

                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                
                                <div class="row">
                                    <div class="col" >
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Kode Tingkat Kelompok Jabatan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="kode_tingkat_kelompok_jabatan" value="TKJ-{{now()->isoFormat('dmYYs')}}">{{$data->kode_tingkat_kelompok_jabatan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Tingkat Kelompok Jabatan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="nama_tingkat_kelompok_jabatan" value="{{$data->nama_tingkat_kelompok_jabatan}}">{{$data->nama_tingkat_kelompok_jabatan}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Kelompok Jabatan</b>
                                <div class="row">
                                    <div class="col" >
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Kode Tingkat Kelompok Jabatan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="kode_kelompok_jabatan" value="{{$data->kode_kelompok_jabatan}}">{{$data->kode_kelompok_jabatan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Kelompok Jabatan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="nama_kelompok_jabatan" value="{{$data->nama_kelompok_jabatan}}">{{$data->nama_kelompok_jabatan}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col">
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Dari Golongan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data->dari_golongan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Sampai Golongan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data->sampai_golongan}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <div class="row">
                                    <div class="col" >
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Tanggal Mulai Efektif</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_mulai" value="{{$data->tanggal_mulai}}">{{$data->tanggal_mulai}}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Selesai Efektif</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_selesai" value="{{$data->tanggal_selesai}}">{{$data->tanggal_selesai}}</td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                            </tr>
                                        </table>
                                    </div>                     
                                </div>
                                @endforeach
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_tkj') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection