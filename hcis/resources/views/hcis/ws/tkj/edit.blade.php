@extends('wstingkatkelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Edit Tingkat Kelompok Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">tingkat kelompok jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>     
                                        <form action="{{route('update_tkj')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                           @foreach ($wstingkatkelompokjabatan as $data)
                                            <hr>
                                            <div class="form-group">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                                <div class="col-sm-10">
                                                                <input type="hidden" name="id_tingkat_kelompok_jabatan" value="{{ $data->id_tingkat_kelompok_jabatan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="kode_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data -> kode_tingkat_kelompok_jabatan}}" readonly>
                                                            </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="nama_tingkat_kelompok_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data -> nama_tingkat_kelompok_jabatan}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">Kelompok jabatan</b>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select name="kode_kelompok_jabatan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                        <option value=""></option>
                                                                        @foreach ($dataa['looks'] as $v)
                                                                            <option value="{{$v->kode_kelompok_jabatan}}" {{ $v->kode_kelompok_jabatan == $data->kode_kelompok_jabatan ? 'selected' : NULL }}>{{$v->kode_kelompok_jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select  name="nama_kelompok_jabatan" style="font-size:11px;" required class="form-control form-control-sm">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datab['looks'] as $p )
                                                                        <option value="{{$p->nama_kelompok_jabatan}}" {{ $p->nama_kelompok_jabatan == $data->nama_kelompok_jabatan ? 'selected' : NULL }}>{{$p->nama_kelompok_jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="nama_kelompok_jabatan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                                <div class="col-sm-10">
                                                                <!-- <input type="text" required name="dari_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                    <select required name="dari_golongan" id="produk00" style="font-size:11px;" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datac['looks'] as $ng )
                                                                        <option value="{{$ng->nama_golongan}}" {{ $ng->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$ng->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select  name="sampai_golongan" id="produk00" style="font-size:11px;" required class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datac['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                            <hr>
                                                        </div>
                                                    </div>  
                                                    <hr>
                                                    <b style="color:black;">Informasi Lainnya</b>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date"  name="tanggal_mulai" id="colFormLabelSm" class="form-control form-control-sm startDates"  style="font-size:11px;" placeholder="" value="{{$data -> tanggal_mulai}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai" id="colFormLabelSm" class="form-control form-control-sm endDates" style="font-size:11px;" placeholder="" value="{{$data -> tanggal_selesai}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan"  id="colFormLabelSm" class="form-control form-control-sm" rows="4" cols="50"  style="font-size:11px;" placeholder="" value="{{$data -> keterangan}}">{{$data -> keterangan}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div >
                                                <table>
                                                    <tr>
                                                        
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_tkj') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                            </div>
                                            @endforeach

                                        </form>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection