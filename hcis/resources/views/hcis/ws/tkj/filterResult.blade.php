@extends('wstingkatkelompokjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Tingkat Kelompok Jabatan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">tingkat kelompok jabatan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamtkj')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                            <form action="{{ URL::to('/list_tkj/hapus_banyak') }}" method="POST"  id="form_delete">
                                @csrf
                                    <table id="example" class="table-responsive-xl table-bordered table-striped table-hover table-xl" style="width:100%;font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_tingkat_kelompok_jabatan')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'kode_tingkat_kelompok_jabatan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Tingkat Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'nama_tingkat_kelompok_jabatan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Tingkat kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'kode_kelompok_jabatan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'nama_kelompok_jabatan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Kelompok Jabatan</th>
                                                @endif
                                                @if($th[$i] == 'dari_golongan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Dari Golongan</th>
                                                @endif
                                                @if($th[$i] == 'sampai_golongan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Sampai Golongan</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->id_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->kode_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_tingkat_kelompok_jabatan')
                                                            <td>{{ $row->nama_tingkat_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_kelompok_jabatan')
                                                            <td>{{ $row->kode_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_kelompok_jabatan')
                                                            <td>{{ $row->nama_kelompok_jabatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'dari_golongan')
                                                            <td>{{ $row->dari_golongan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'sampai_golongan')
                                                            <td>{{ $row->sampai_golongan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai')
                                                            <td>{{ $row->tanggal_mulai ? date('d-m-Y', strtotime($row->tanggal_mulai)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai')
                                                            <td>{{ $row->tanggal_selesai ? date('d-m-Y', strtotime($row->tanggal_selesai)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                        <td> 
                                                            <a href="{{ URL::to('/list_tkj/detail/'.$row->id_tingkat_kelompok_jabatan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;                                        
                                                            <a href="{{ URL::to('/list_tkj/edit/'.$row->id_tingkat_kelompok_jabatan) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                            <input type="checkbox" name="multiDelete[]" value="{{ $row->id_tingkat_kelompok_jabatan }}" id="multiDelete">
                                                        </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_tkj')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection


