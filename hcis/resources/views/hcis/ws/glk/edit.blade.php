@extends('wsgruplokasikerja.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Edit Grup Lokasi Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">grup lokasi kerja</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                            <form action="{{route('update_glk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                <hr>
                                @foreach($wsgruplokasikerja as $data)
                                <div class="form-group" style="width:95%;">
                                    <div class="">
                                        <div class="row">
                                            <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                        <div class="col-sm-10">
                                                            <input type="hidden" name="id_grup_lokasi_kerja" value="{{ $data->id_grup_lokasi_kerja }}" class="form-control form-control-sm" id="tg_id" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" readonly name="kode_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_grup_lokasi_kerja}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_grup_lokasi_kerja}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Grup Lokasi Kerja</label>
                                                        <div class="col-sm-10">
                                                            <select name="tipe_grup_lokasi_kerja" value="tipe_grup_lokasi_kerja" id="tipe_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" required>
                                                                <option value="-" {{ $data->tipe_grup_lokasi_kerja == '' ? 'selected' : NULL }}>--Belum Dipilih--</option>
                                                                <option value="lokasi_kerja" {{ $data->tipe_grup_lokasi_kerja == 'lokasi kerja' ? 'selected' : NULL }}>Lokasi Kerja</option>
                                                                <option value="regional" {{ $data->tipe_grup_lokasi_kerja == 'regional' ? 'selected' : NULL }}>Regional</option>
                                                            </select>
                                                        <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                        <div class="col-sm-10">
                                                            <select name="lokasi_kerja" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                <option value=""></option>
                                                                @foreach ($datas['looks'] as $v)
                                                                    <option value="{{$v->nama_lokasi_kerja}}" {{ $v->nama_lokasi_kerja == $data->lokasi_kerja ? 'selected' : NULL }}>{{$v->nama_lokasi_kerja}}</option>
                                                                @endforeach
                                                            </select>
                                                        <!-- <input type="text" name="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->lokasi_kerja}}"> -->
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                    <div class="col-sm-10">
                                                    <textarea type="text" name="keterangan" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->keterangan}}" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">

                                                    <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{$data->tanggal_selesai_efektif}}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

                                                    <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                    </div>
                                                </div>
                                            </div> 
                                        </div><hr>
                                        <div>
                                            <table>
                                                <tr>
                                                <!-- <td>
                                                    <a href="" class="btn">Hapus</a>
                                                </td> -->
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm" href="{{route('hapus_glk',$data->id_grup_lokasi_kerja)}}" class="btn" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete" >Hapus</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('list_glk') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </form>
                            </div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_glk') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection
