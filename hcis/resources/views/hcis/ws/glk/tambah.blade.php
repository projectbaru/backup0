@extends('wsgruplokasikerja.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Grup Lokasi Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">grup lokasi kerja</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                                        <form action="{{route('simpan_glk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            <hr>
                                            <div class="form-group">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required value="{{$random}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Grup Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tipe_grup_lokasi_kerja" id="tipe_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" >
                                                                        <option value="-">--Belum Dipilih--</option>
                                                                        <option value="lokasi kerja">Lokasi Kerja</option>
                                                                        <option value="regional">Regional</option>
                                                                    </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                    <select name="lokasi_kerja" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($data['looks'] as $p )
                                                                        <option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" name="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" rows="4" cols="50" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"> -->

                                                                <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

                                                                <!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->

                                                                <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required> -->
                                                                </div>
                                                            </div>
                                                        </div>                                      
                                                    </div> 
                                                    <hr>                                                                       
                                                </div>
                                            </div>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_glk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
            
                                        </form>
                                        </div>
			</div>
		</div>
	</div>
</div>

@endsection
