@extends('wsgruplokasikerja.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Grup Lokasi Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">grup lokasi kerja</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div> 
                    <form action="{{route('simpan_glk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
						<div>
							<div class="">
                                <!-- <h5 style="color:black;">Detail Grup Lokasi Kerja</h5>	 -->
                                <hr>		
                                @foreach($wsgruplokasikerja as $data)
                                <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_grup_lokasi_kerja }}" />

                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_glk', $data->id_grup_lokasi_kerja)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_glk', $data->id_grup_lokasi_kerja)}}" id="btn_delete">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_glk')}}">Batal</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                <!-- <b style="color:black;">Informasi Perusahaan</b> -->
                                <div class="row">
                                    <div class="col" >
                                        <table style="font-size:12px;">
                                            <tr>
                                                <td>Kode Grup Lokasi Kerja</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="kode_grup_lokasi_kerja" value="GLK-{{now()->isoFormat('dmYYs')}}">
                                                    {{$data->kode_grup_lokasi_kerja}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lokasi Kerja</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="nama_grup_lokasi_kerja" value="{{$data->nama_grup_lokasi_kerja}}">{{$data->nama_grup_lokasi_kerja}}</td>
                                            </tr>
                                            <tr>
                                                <td>Tipe Grup Lokasi Kerja</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tipe_grup_lokasi_kerja" value="{{$data->tipe_grup_lokasi_kerja}}">{{$data->tipe_grup_lokasi_kerja}}</td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi Kerja</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="lokasi_kerja" value="{{$data->lokasi_kerja}}">{{$data->lokasi_kerja}}</td>
                                            </tr>
                                        </table>
                                        </div>
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">
                                                        {{$data->keterangan}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                </tr>
                                                
                                            </table><br><br>
                                        </div>                                         
                                    </div>
                                </div>
                                 @endforeach
                            </div>
                        </div><br>
                    </form>
					</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_glk') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection