@extends('wskantorcabang.side')

<style>
	input {
  font-family: monospace;
}
label {
  display: block;
}
div {
  margin: 0 0 1rem 0;
}
</style>
@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Kantor Cabang</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">kantor cabang</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
							<form action="{{route('simpan_kac')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
								<hr>
								<div class="form-group" style="width:95%;">
									<div class="">
										<div class="row">
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kantor</label>
													<div class="col-sm-10">
													<input type="text" name="kode_kantor" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""  readonly  required value="{{$random}}" >
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kantor</label>
													<div class="col-sm-10">
													<input type="text" name="nama_kantor" required class="form-control form-control-sm" required style="font-size:11px;" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Lokasi Kerja</label>
													<div class="col-sm-10">
														<select name="kode_lokasi_kerja" id="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
															<option value="" selected disabled>--- Pilih ---</option>
															@foreach ($dataklk['looks'] as $k )
															<option value="{{$k->kode_lokasi_kerja}}" data-name ="{{$k->nama_lokasi_kerja}}">{{$k->kode_lokasi_kerja}}</option>
															@endforeach
														</select>		
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Lokasi Kerja</label>
													<div class="col-sm-10">
													{{-- <input type="text" required name="nama_lokasi_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
													<select name="nama_lokasi_kerja" id="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
														<option value="" selected disabled>--- Pilih ---</option>
														@foreach ($dataklk['looks'] as $n )
														<option value="{{$n->nama_lokasi_kerja}}" data-kode ="{{$n->kode_lokasi_kerja}}">{{$n->nama_lokasi_kerja}}</option>
														@endforeach
													</select>	
													</div>
												</div>
												<!-- <div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
													<div class="col-sm-10">

													
														<input readonly type="text" name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
													<div class="col-sm-10">
														<select name="nama_lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" required>
															<option value="" selected>--- Pilih ---</option>
															@foreach ($dataklk['looks'] as $p )
															<option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
															@endforeach
														</select>
														<input type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
													</div>
												</div> -->
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Kantor</label>
													<div class="col-sm-10">
														<select name="tipe_kantor" required id="tipe_kantor" class="form-control form-control-sm" style="font-size:11px;">
															<option value="pusat">Pusat</option>
															<option value="cabang">Cabang</option>
															<option value="ro">RO</option>
														</select>
														<!-- <input type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">UMK / UMP</label>
													<div class="col-sm-10">
								
														<input type="number" name="umk_ump" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor NPWP</label>
													<div class="col-sm-10">
													<!-- <input type="text" id="npwp" name="nomor_npwp" placeholder="8 600 00 000" required class="form-control form-control-sm" pattern="(8 6)\d{2} \d{2} \d{3}" /> -->
														<input id="npwp" name="nomor_npwp" data-inputmask="'mask': '99.999.999.9-999.999'" style="font-size:11px;" placeholder="" required  class="form-control form-control-sm" />
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Ttd SPT (ID PIC)</label>
													<div class="col-sm-10">
													<input type="text" name="ttd_spt"  class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" required>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
													<div class="col-sm-10">
													<textarea type="text" name="keterangan" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" required placeholder=""></textarea>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
													<div class="col-sm-10">
													<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

													<!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif"> -->
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
													<div class="col-sm-10">
													<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>

													<!-- <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" readonly/> -->
													</div>
												</div>

											</div>                                                                                                 
										</div><hr>
									</div>
								</div>
								<div>
									<table>
										<tr>
											<!-- <td>
											<a href="" class="btn">Hapus</a>
										</td> -->
											<!-- <td>
											<a href="" class="btn">Hapus</a>
										</td> -->
											
											<td>
												<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
											</td>
											<td>
												<a href="{{ route('list_kac') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>
										</tr>
									</table>
									<br>
								</div>

							</form>
							</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
			<script> 
				$(document).ready(function() {
					$('#npwp').inputmask();
				});
			</script>
			<script>
				$('#kode_lokasi_kerja').change(function(){
					$('#nama_lokasi_kerja').val($('#kode_lokasi_kerja option:selected').data('name'));
				})
				$('#nama_lokasi_kerja').change(function(){
					$('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode'));
				})
			</script>
		@endsection