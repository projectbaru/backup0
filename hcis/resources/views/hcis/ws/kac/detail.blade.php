@extends('wskantorcabang.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Kantor Cabang</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">kantor cabang</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                    <form action="{{route('simpan_kac')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
					<hr>
							<div class="" style="width:95%" >
                            	
								@foreach($wskantorcabang as $data)
								<table>
									<tr>
										<td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_kac', $data->id_kantor)}}">Ubah</a></td>
										<td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
										<td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_kac', $data->id_kantor)}}">Hapus</a></td>
										<td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_kac')}}">Batal</a></td>
										<!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
										<!-- <td>
											<button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
										</td> -->
									</tr>
								</table><br>
								<!-- <b style="color:black;">Informasi Perusahaan</b> -->
								<div class="row">
									<div class="col" >
										<table style="font-size:12px;">
											<tr>
												<td>Kode Kantor</td>
												<td>:</td>
												<td><input type="hidden" name="kode_kantor" value="{{$data->kode_kantor}}">
													{{$data->kode_kantor}}</td>
											</tr>
											<tr>
												<td>Nama Kantor</td>
												<td>:</td>
												<td><input type="hidden" name="nama_kantor" value="{{$data->nama_kantor}}">{{$data->nama_kantor}}</td>
											</tr>
											<tr>
												<td>Kode Lokasi Kerja</td>
												<td>:</td>
												<td><input type="hidden" name="kode_lokasi_kerja" value="{{$data->kode_lokasi_kerja}}">{{$data->kode_lokasi_kerja}}</td>
											</tr>
											<tr>
												<td>Nama Lokasi Kerja</td>
												<td>:</td>
												<td><input type="hidden" name="nama_lokasi_kerja" value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</td>
											</tr>
											<tr>
												<td>Tipe Kantor</td>
												<td>:</td>
												<td><input type="hidden" name="tipe_kantor" value="{{$data->tipe_kantor}}">{{$data->tipe_kantor}}</td>
											</tr>
											<tr>
												<td>UMK UMP</td>
												<td>:</td>
												<td><input type="hidden" name="umk_ump" value="{{$data->umk_ump}}">{{$data->umk_ump}}</td>
											</tr>
										</table>
										</div>
										<div class="col">
											<table style="font-size:12px;">
												<tr>
													<td>Nomor NPWP</td>
													<td>:</td>
													<td><input type="hidden" name="nomor_npwp" value="{{$data->nomor_npwp}}">
														{{$data->nomor_npwp}}
													</td>
												</tr>
												<tr>
													<td>Ttd SPT (ID PIC)</td>
													<td>:</td>
													<td><input type="hidden" name="ttd_spt" value="{{$data->ttd_spt}}">{{$data->ttd_spt}}</td>
												</tr>
												<tr>
													<td>Keterangan</td>
													<td>:</td>
													<td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
												</tr>
												<tr>
													<td>Tanggal Mulai Efektif</td>
													<td>:</td>
													<td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
												</tr>
												<tr>
													<td>Tanggal Selesai Efektif</td>
													<td>:</td>
													<td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
												</tr>
											</table><br><br>
										</div>                                         
									</div>
								</div>
								@endforeach
                        	</div>
                    </form>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_kac') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection