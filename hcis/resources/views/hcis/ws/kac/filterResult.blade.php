@extends('wskantorcabang.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Kantor Cabang</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">kantor cabang</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamkac')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                            <form action="{{ URL::to('/list_kac/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl" style="width:100%;font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_kantor')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'kode_kantor')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Kantor</th>
                                                @endif
                                                @if($th[$i] == 'nama_kantor')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Kantor</th>
                                                @endif
                                                @if($th[$i] == 'kode_kelas')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Kelas</th>
                                                @endif
                                                @if($th[$i] == 'tipe_kantor')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Kantor</th>
                                                @endif
                                                @if($th[$i] == 'kode_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'nama_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'nomor_npwp')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor NPWP</th>
                                                @endif
                                                @if($th[$i] == 'umk_ump')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">UMK/UMP</th>
                                                @endif
                                                @if($th[$i] == 'ttd_spt')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Ttd SPT (ID PIC)</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size: 11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_kantor')
                                                            <td>{{ $row->id_kantor ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_kantor')
                                                            <td>{{ $row->kode_kantor ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_kantor')
                                                            <td>{{ $row->nama_kantor ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_kelas')
                                                            <td>{{ $row->kode_kelas ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tipe_kantor')
                                                            <td>{{ $row->tipe_kantor ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_lokasi_kerja')
                                                            <td>{{ $row->kode_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_lokasi_kerja')
                                                            <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_npwp')
                                                            <td>{{ $row->nomor_npwp ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'umk_ump')
                                                            <td>{{ $row->umk_ump ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'ttd_spt')
                                                            <td>{{ $row->ttd_spt ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                       
                                                        @if($i == count($th) - 1)
                                                        <td>
                                                            <a href="{{ URL::to('/list_kac/detail/'.$row->id_kantor) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                            <a href="{{ URL::to('/list_kac/edit/'.$row->id_kantor) }}" class="">Edit</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="multiDelete[]" value="{{ $row->id_kantor }}" id="multiDelete">
                                                        </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_kac')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <td></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td></td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection

