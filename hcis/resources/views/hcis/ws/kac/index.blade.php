@extends('wskantorcabang.side')
@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Kantor Cabang</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">kantor cabang</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamkac')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                <form action="{{ URL::to('/list_kac/hapus_banyak') }}" method="POST">
                                    @csrf
                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead style="color:black;">
                                            <tr>
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                <th scope="col" style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Kantor</th>
                                                <!-- {{-- <th scope="col">Logo Perusahaan</th> --}} -->
                                                <th scope="col" style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Kantor</th>
                                                <th scope="col" style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Kantor</th>
                                                <th scope="col" style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                <th scope="col" style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 11px;">
                                            @php $b=1; @endphp
                                            @foreach ($wskantorcabang as $data)
                                            {{-- {{dd($data)}} --}}
                                                <tr>
                                                    <td>{{ $b++; }}</td>
                                                    <td>{{ $data->kode_kantor }}</td>
                                                    <!-- {{-- <td>{{$data -> logo_perusahaan}}</td> --}} -->
                                                    <td>{{ $data->nama_kantor }}</td>
                                                    <td>{{ $data->tipe_kantor }}</td>
                                                    <td>
                                                        <a href="{{ URL::to('/list_kac/detail/'.$data->id_kantor) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <a href="{{ URL::to('/list_kac/edit/'.$data->id_kantor) }}" class="">Edit</a>
                                                    </td>
                                                    <td><input type="checkbox" name="multiDelete[]" value="{{ $data->id_kantor }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_kac')}}">Tambah</a></td>
                                            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                            <td>
                                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                             </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>


