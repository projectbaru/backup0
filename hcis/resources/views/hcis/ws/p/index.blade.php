@extends('../layouts.master')
@section('title') Dashboard @endsection

@section('css')
    <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

    @section('content')
        @component('components.breadcrumb')
            @slot('li_1') Dastone @endslot
            @slot('li_2') Tables @endslot
            @slot('li_3') Datatables @endslot
            @slot('title') Datatables @endslot
        @endcomponent
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Perusahaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">perusahaan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamp')}}"><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style">
                                    <div class="table-responsive-xl" style="overflow-x:auto;">
                                        <form action="{{ URL::to('/list_p/hapus_banyak') }}" method="POST" id="form_delete">
                                            @csrf
                                            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                <thead style="color:black;font-size:12px;">
                                                    <tr>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Nama Perusahaan</th>
                                                        {{-- <th scope="col">Logo Perusahaan</th> --}}
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Visi Perusahaan</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Misi Perusahaan</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">V</th>

                                                    </tr>
                                                </thead>
                                                <tbody style="font-size:11px;">
                                                    @php $b=1; @endphp
                                                    @foreach ($wsperusahaan as $data)
                                                    
                                                    {{-- {{dd($data)}} --}}
                                                    <tr>
                                                        <td>{{ $b++; }}</td>
                                                        <td>{{ $data->nama_perusahaan }}</td>
                                                        {{-- <td><img src="{{url('/data_file/'.$data -> perusahaan_logo)}}" width="80px" alt=""></td> --}}
                                                        <td>{{ $data->visi_perusahaan }}</td>
                                                        <td>{{ $data->misi_perusahaan }}</td>
                                                        <td>
                                                            <a href="{{ URL::to('/list_p/detail/'.$data->id_perusahaan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                            <a href="{{ URL::to('/list_p/edit/'.$data->id_perusahaan) }}" class="">Edit</a>
                                                            <!-- <a href="{{ URL::to('/list_wsperusahaan/hapus/'.$data->id_perusahaan) }}" class="">Hapus</a> -->
                                                        </td>
                                                        <td><input type="checkbox" name="multiDelete[]" id="multiDelete" value="{{ $data->id_perusahaan }}"></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_p')}}">Tambah</a></td>
                                                    <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                                    <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection

@endsection
@section('script')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
@endsection
