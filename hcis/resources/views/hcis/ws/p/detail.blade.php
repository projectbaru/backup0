@extends('wsperusahaan.side')
@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Detail Perusahaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">perusahaan</a></li>
                                    </ul>
                                </div>
                                <div>
                                <!-- <button type="button" class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom" style="background-color:black;">
                                        ?
                                </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('simpan_pd')}}" method="post">
                        {{ csrf_field() }}
                        <hr>
                        @php $b=1; @endphp
                        @foreach($wsperusahaan as $data)
                        <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_perusahaan }}" />
                        <table>
                            <tr>
                                <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="">Print</a></td>
                                <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_p', $data->id_perusahaan)}}">Ubah</a></td>
                                <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_p', $data->id_perusahaan)}}" id="btn_delete">Hapus</a></td>
                                <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_p')}}">Batal</a></td>

                                <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                            </tr>
                        </table><br>
                        <b style="color:black;">Informasi Perusahaan</b><br><br>
                        <div class="row">
                            <div class="col">
                                <table style="font-size:12px;">
                                    <tr>
                                        <td>Logo Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="perusahaan_logo" value="{{$data->perusahaan_logo}}"><img src="{{url('/data_file/'.$data -> perusahaan_logo)}}" name="perusahaan_logo" value="{{$data->perusahaan_logo}}" width="100px" alt=""></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kode Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="kode_perusahaan" value="{{$data->kode_perusahaan}}">{{$data->kode_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Singkatan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="singkatan" value="{{$data->singkatan}}">{{$data->singkatan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Visi Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="visi_perusahaan" value="{{$data->visi_perusahaan}}">{{$data->visi_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Misi Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="misi_perusahaan" value="{{$data->misi_perusahaan}}">{{$data->misi_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Nilai Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="nilai_perusahaan" value="{{$data->nilai_perusahaan}}">{{$data->nilai_perusahaan}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col" style="font-size:12px;">
                                <table>
                                    <!-- <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr> -->
                                    <tr>
                                        <td>Tanggal Mulai Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="tanggal_mulai_perusahaan" value="{{$data->tanggal_mulai_perusahaan}}">{{$data->tanggal_mulai_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Selesai Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="tanggal_selesai_perusahaan" value="{{$data->tanggal_selesai_perusahaan}}">{{$data->tanggal_selesai_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="jenis_perusahaan" value="{{$data->jenis_perusahaan}}">{{$data->jenis_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Bisnis Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="jenis_bisnis_perusahaan" value="{{$data->jenis_bisnis_perusahaan}}">{{$data->jenis_bisnis_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Karyawan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="jumlah_karyawan" value="{{$data->jumlah_karyawan}}">{{$data->jumlah_karyawan}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <b style="color:black;">Detail Pajak Perusahaan</b>
                        <div class="row">
                            <div class="col">
                                <table style="font-size:12px;">
                                    <tr>
                                        <td>Nomor NPWP Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="nomor_npwp_perusahaan" value="{{$data->nomor_npwp_perusahaan}}">{{$data -> nomor_npwp_perusahaan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Lokasi Pajak</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="lokasi_pajak" value="{{$data->lokasi_pajak}}">{{$data -> lokasi_pajak}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col" style="font-size:12px;">
                                <table>
                                    <tr>
                                        <td>NPP</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="npp" value="{{$data->npp}}">{{$data -> npp}}</td>
                                    </tr>
                                    <tr>
                                        <td>NPKP</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="npkp" value="{{$data->npkp}}">{{$data -> npkp}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <b style="color:black;">ID Logo</b>
                        <div class="row">
                            <div class="col">
                                <table style="font-size:12px;">
                                    <tr>
                                        <td>ID Logo Perusahaan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="id_logo_perusahaan" value="{{$data->id_logo_perusahaan}}">{{$data -> id_logo_perusahaan}}</td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <hr>
                        <b style="color:black;">Rekaman Informasi</b>
                        <div class="row">
                            <div class="col">
                                <table style="font-size:12px;">
                                    <tr>
                                        <td>Tanggal Mulai Efektif</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data -> tanggal_mulai_efektif}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Selesai Efektif</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data -> tanggal_selesai_efektif}}</td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td>:</td>
                                        <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                                    </tr>
                                </table><br>
                            </div>
                        </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection