@extends('wsperusahaan.side')
<style>
	input {
  font-family: monospace;
}
label {
  display: block;
}
div {
  margin: 0 0 1rem 0;
}
</style>
@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Perusahaan</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">perusahaan</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
					
					
						<form action="{{route('simpan_p')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							<div class="form-group " style="width:95%;">
								<div class="">
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Logo Perusahaan</label>
												<div class="col-sm-10">
													<input type="file" name="logo_perusahaan" style="font-size:11px;" id="" placeholder="">
												</div>
											</div>
											<div class="form-group row" >
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>												
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="nama_perusahaan" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="" required>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="kode_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required  value="{{$randomId}}" readonly>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Singkatan</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="singkatan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Visi Perusahaan</label>
												<div class="col-sm-10">
													<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="visi_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Misi Perusahaan</label>
												<div class="col-sm-10">
													<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="misi_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" required></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nilai Perusahaan</label>
												<div class="col-sm-10">
													<textarea type="textarea" style="font-size:11px;"  rows="8" cols="50" name="nilai_perusahaan" required class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
												</div>
											</div>
										</div>
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Perusahaan</label>
												<div class="col-sm-10">
													<input type="date" name="tanggal_mulai_perusahaan" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_perusahaan" placeholder="Tanggal mulai perusahaan"></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Perusahaan</label>
												<div class="col-sm-10">
													<input type="date" name="tanggal_selesai_perusahaan" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_perusahaan" placeholder="Tanggal selesai perusahaan"/></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Perusahaan</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="jenis_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Bisnis Perusahaan</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="jenis_bisnis_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jumlah Karyawan</label>
												<div class="col-sm-10">
													<input type="number" style="font-size:11px;" name="jumlah_karyawan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
										</div>
									</div>
									<hr>
									<b style="color:black;">Detail Pajak Perusahaan</b>
									<div class="row">
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nomor NPWP Perusahaan</label>
												<div class="col-sm-10">
													<input id="npwp" name="nomor_npwp_perusahaan" data-inputmask="'mask': '99.999.999.9-999.999'" style="font-size:11px;" placeholder="" required class="form-control form-control-sm" />
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Pajak</label>
												<div class="col-sm-10">
													<input type="text" required style="font-size:11px;" name="lokasi_pajak" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
										</div>
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">NPP</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="npp" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">NPKP</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="npkp" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
										</div>
									</div>
									<hr>
									<b style="color:black;">ID</b>
									<div class="row">
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">ID Logo Perusahaan</label>
												<div class="col-sm-10">
													<input type="text" style="font-size:11px;" name="id_logo_perusahaan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
											</div>
										</div>
									</div>
									<hr>
									<b style="color:black;">Rekaman Informasi</b>
									<div class="row">
										<div class="col">
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
												<div class="col-sm-10">
													<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
												<div class="col-sm-10">
													<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
												<div class="col-sm-10">
													<textarea type="text" style="font-size:11px;" rows="8" cols="50" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
												</div>
											</div>
										</div>
									</div>
									<hr>

								</div>
							</div>
							<div style="width: 90%;">
								<table>
								<tr>
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_p') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
						</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>

@endsection



