@extends('layouts.master')
@section('title') Dashboard @endsection

@section('css')
    <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

    @section('content')
        @component('components.breadcrumb')
            @slot('li_1') Dastone @endslot
            @slot('li_2') Tables @endslot
            @slot('li_3') Datatables @endslot
            @slot('title') Datatables @endslot
        @endcomponent
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Perusahaan</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">perusahaan</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamp')}}"><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style">
                                    <div class="table-responsive-xl" style="overflow-x:auto">

                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                        <form action="{{ URL::to('/list_p/hapus_banyak') }}" method="POST"  id="form_delete">
                                            @csrf
                                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead style="color:black;">
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                                                        @for($i = 0; $i < count($th); $i++)
                                                            <!-- @if($th[$i] == 'id_perusahaan')
                                                                <th></th>
                                                            @endif -->
                                                            @if($th[$i] == 'perusahaan_logo')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Logo Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'singkatan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Singkatan</th>
                                                            @endif
                                                            @if($th[$i] == 'visi_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Visi Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'misi_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Misi Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'nilai_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nilai Perusahaan</th>
                                                            @endif
                                                            <!-- @if($th[$i] == 'keterangan_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan Perusahaan</th>
                                                            @endif -->
                                                            @if($th[$i] == 'tanggal_mulai_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'jenis_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'jenis_bisnis_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Bisnis Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'jumlah_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Jumlah Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'nomor_npwp_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor NPWP Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'lokasi_pajak')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Pajak</th>
                                                            @endif
                                                            @if($th[$i] == 'npp')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">NPP</th>
                                                            @endif
                                                            @if($th[$i] == 'npkp')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">NPKP</th>
                                                            @endif
                                                            @if($th[$i] == 'id_logo_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">ID Logo Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                            @endif
                                                            @if($th[$i] == 'status_rekaman')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                            @endif
                                                            @if($th[$i] == 'jumlah_karyawan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Jumlah Karyawan</th>
                                                            @endif
                                                            @if($i == count($th) - 1)
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                            @endif
                                                        @endfor
                                                    </thead>
                                                    <tbody style="font-size:11px;">
                                                        @php $b=1 @endphp @foreach($query as $row)
                                                            <tr>
                                                                <td>{{$b++}}</td>
                                                                @for($i = 0; $i < count($th); $i++)
                                                                    <!-- @if($th[$i] == 'id_perusahaan')
                                                                        <td>{{ $row->id_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif -->
                                                                    @if($th[$i] == 'perusahaan_logo')
                                                                        <td><img src="{{url('/data_file/'.$row -> perusahaan_logo)}}" width="35px" alt=""></td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_perusahaan')
                                                                        <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_perusahaan')
                                                                        <td>{{ $row->kode_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'singkatan')
                                                                        <td>{{ $row->singkatan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'visi_perusahaan')
                                                                        <td>{{ $row->visi_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'misi_perusahaan')
                                                                        <td>{{ $row->misi_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nilai_perusahaan')
                                                                        <td>{{ $row->nilai_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    <!-- @if($th[$i] == 'keterangan_perusahaan')
                                                                        <td>{{ $row->keterangan_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif -->
                                                                    @if($th[$i] == 'tanggal_mulai_perusahaan')
                                                                        <td>{{ $row->tanggal_mulai_perusahaan ? date('d-m-Y', strtotime($row->tanggal_mulai_perusahaan)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_selesai_perusahaan')
                                                                        <td>{{ $row->tanggal_selesai_perusahaan ? date('d-m-Y', strtotime($row->tanggal_selesai_perusahaan)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'jenis_perusahaan')
                                                                        <td>{{ $row->jenis_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'jenis_bisnis_perusahaan')
                                                                        <td>{{ $row->jenis_bisnis_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'jumlah_perusahaan')
                                                                        <td>{{ $row->jumlah_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nomor_npwp_perusahaan')
                                                                        <td>{{ $row->nomor_npwp_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'lokasi_pajak')
                                                                        <td>{{ $row->lokasi_pajak ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'npp')
                                                                        <td>{{ $row->npp ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'npkp')
                                                                        <td>{{ $row->npkp ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'id_logo_perusahaan')
                                                                        <td>{{ $row->id_logo_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'keterangan')
                                                                        <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'status_rekaman')
                                                                        <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_mulai_efektif')
                                                                        <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_selesai_efektif')
                                                                        <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                                    @endif
                                                                    @if($th[$i] == 'jumlah_karyawan')
                                                                        <td>{{ $row->jumlah_karyawan.' karyawan' ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($i == count($th) - 1)
                                                                        <td> 
                                                                            <a href="{{ URL::to('/list_p/detail/'.$row->id_perusahaan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                            <a href="{{ URL::to('/list_p/edit/'.$row->id_perusahaan) }}" class="">Edit</a>
                                                                        </td>
                                                                        <td>
                                                                        <input type="checkbox" name="multiDelete[]" value="{{ $row->id_perusahaan }}"  id="multiDelete">                                  

                                                                        </td>
                                                                    @endif
                                                                @endfor
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_p')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                        <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                        <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                        </td>
                                                        <!-- <td>
                                                            <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Print</button>
                                                        </td> -->
                                                    </tr>
                                                </table>
                                        </form>
                               
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection
@section('script')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
@endsection

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection