@extends('wsalamat.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Edit Alamat Perusahaan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">perusahaan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <form action="{{route('update_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                @foreach($wsalamat as $data)
                                <hr>
                                <div class="form-group" style="width:95%;">
                                    <div class="">
                                        <div class="row">
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                    <div class="col-sm-10">
                                                        <input type="hidden" id="tg_id" name="id_alamat"  value="{{ $data->id_alamat }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Alamat</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" readonly name="kode_alamat" required style="font-size:11px;" value="{{ $data->kode_alamat }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Perusahaan</label>
                                                    <div class="col-sm-10">
                                                        <select name="kode_perusahaan" id="kode_perusahaan" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                            @foreach ($datap['looks'] as $p )
                                                            <option data-name ="{{$p->nama_perusahaan}}" value="{{$p->kode_perusahaan}}" {{ $p->kode_perusahaan == $data->kode_perusahaan ? 'selected' : NULL }} >{{$p->kode_perusahaan}}</option>
                                                            @endforeach
                                                        </select>		
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Perusahaan</label>
                                                    <div class="col-sm-10">
                                                    {{-- <input type="text" required name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
                                                    <select name="nama_perusahaan" id="name_p" class="form-control form-control-sm" style="font-size:11px;">
                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                        @foreach ($datap['looks'] as $np )
                                                        <option value="{{$np->nama_perusahaan}}" data-kode ="{{$np->kode_perusahaan}}" {{ $np->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$np->nama_perusahaan}}</option>
                                                        @endforeach
                                                    </select>	
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Alamat Untuk SPT</label>
                                                    <div class="col-sm-10">
                                                        <select name="status_alamat_untuk_spt" id="status_alamat_untuk_spt" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="ya">Ya</option>
                                                            <option value="tidak">Tidak</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Alamat</label>
                                                    <div class="col-sm-10">
                                                        <select name="jenis_alamat" id="jenis_alamat" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="Alamat Kantor Pusat">Alamat Kantor Pusat</option>
                                                            <option value="Alamat Cabang">Alamat Cabang</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat</label>
                                                    <div class="col-sm-10">
                                                        <textarea type="text" name="alamat" rows="8" cols="50" required value="" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->alamat }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" name="kota" style="font-size:11px;" value="{{ $data->kota }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Pos</label>
                                                    <div class="col-sm-10">
                                                    <input type="number" style="font-size:11px;" required name="kode_pos" value="{{ $data->kode_pos }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Telepon</label>
                                                    <div class="col-sm-10">
                                                    <input type="number" style="font-size:11px;" name="telepon" value="{{ $data->telepon }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                
                                            </div>
                                        </div>  
                                        <hr>
                                        <b style="color:black;">Rekaman Informasi</b>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" value="{{ $data->tanggal_mulai_efektif }}" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}" placeholder="Tanggal selesai efektif"/>

                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                                                    <div class="col-sm-10">
                                                    <textarea type="text" style="font-size:11px;" rows="8" cols="50" name="keterangan" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->keterangan }}</textarea>
                                                    </div>
                                                </div>
                                                <hr>      
                                            </div>
                                        </div>
                                        <div>
                                            <table>
                                                <tr>
                                                <!-- <td>
                                                    <a href="" class="btn">Hapus</a>
                                                </td> -->
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm" href="{{route('hapus_a',$data->id_alamat)}}" class="btn" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete" >Hapus</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('list_a') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> 
                                </div> @endforeach
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>

            <script>
				$('#kode_perusahaan').change(function(){
					$('#name_p').val($('#kode_perusahaan option:selected').data('name'));
				})
				$('#name_p').change(function(){
					$('#kode_perusahaan').val($('#name_p option:selected').data('kode'));
				})
			</script>
            
@endsection
@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_a') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection
                        