@extends('wsalamat.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Alamat Perusahaan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">alamat perusahaan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xl-12">
                                    <div class="card" style="border-radius:8px;border:none;">
                                        <div class="card-header">
                                            <!-- <h5>Per Table</h5> -->
                                            <a href="{{route('ubah_tamalamat')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                        </div>
                                        <div class="card-body table-border-style">
                                            <div class="table-responsive-xl"  style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                            <form action="{{ URL::to('/list_a/hapus_banyak') }}" method="POST" id="form_delete">
                                                @csrf
                                                
                                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9; ">
                                                        <thead style="color:black;font-size:12px;">
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                                                            @for($i = 0; $i < count($th); $i++)
                                                                @if($th[$i] == 'kode_alamat')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode Alamat</th>
                                                                @endif
                                                                @if($th[$i] == 'kode_perusahaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode Perusahaan</th>
                                                                @endif
                                                                @if($th[$i] == 'nama_perusahaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Perusahaan</th>
                                                                @endif
                                                                @if($th[$i] == 'status_alamat_untuk_spt')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Alamat Untuk SPT</th>
                                                                @endif
                                                                @if($th[$i] == 'jenis_alamat')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Alamat</th>
                                                                @endif
                                                                @if($th[$i] == 'alamat')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Alamat</th>
                                                                @endif
                                                                @if($th[$i] == 'kota')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kota</th>
                                                                @endif
                                                                @if($th[$i] == 'kode_pos')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode Pos</th>
                                                                @endif
                                                                @if($th[$i] == 'telepon')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Telepon</th>
                                                                @endif
                                                                @if($th[$i] == 'keterangan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                                @endif
                                                                @if($th[$i] == 'status_rekaman')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                                @endif
                                                                @if($th[$i] == 'pengguna_masuk')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Masuk</th>
                                                                @endif
                                                                @if($th[$i] == 'waktu_masuk')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Masuk</th>
                                                                @endif
                                                                @if($th[$i] == 'pengguna_ubah')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Ubah</th>
                                                                @endif
                                                                @if($i == count($th) - 1)
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                                @endif
                                                            @endfor
                                                        </thead>
                                                        <tbody style="font-size:11px;">
                                                            @php $b = 1; @endphp  @foreach($query as $row)
                                                                <tr>
                                                                    <td>{{$b++}}</td>
                                                                    @for($i = 0; $i < count($th); $i++)
                                                                        @if($th[$i] == 'kode_alamat')
                                                                            <td>{{ $row->kode_alamat ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kode_perusahaan')
                                                                            <td>{{ $row->kode_perusahaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nama_perusahaan')
                                                                            <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_alamat_untuk_spt')
                                                                            <td>{{ $row->status_alamat_untuk_spt ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jenis_alamat')
                                                                            <td>{{ $row->jenis_alamat ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'alamat')
                                                                            <td>{{ $row->alamat ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kota')
                                                                            <td>{{ $row->kota ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'kode_pos')
                                                                            <td>{{ $row->kode_pos ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'telepon')
                                                                            <td>{{ $row->telepon ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'keterangan')
                                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_rekaman')
                                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pengguna_masuk')
                                                                            <td>{{ $row->pengguna_masuk ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'waktu_masuk')
                                                                            <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pengguna_ubah')
                                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($i == count($th) - 1)
                                                                            <td>
                                                                                <a href="{{ URL::to('/list_a/detail/'.$row->id_alamat) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                <a href="{{ URL::to('/list_a/edit/'.$row->id_alamat) }}" class="">Edit</a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_alamat }}">
                                                                            </td>
                                                                        @endif
                                                                    @endfor
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_a')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                            <td>
                                                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                            </form>
                                
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection