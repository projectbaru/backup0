@extends('wsalamat.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Tambah Alamat Perusahaan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">alamat perusahaan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                                <form action="{{route('simpan_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                    <hr>
                                    <div class="form-group" style="width:95%;">
                                        <div class="">
                                            <div class="row">
                                                <div class="col" style="font-size: 10px;">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Alamat</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" required name="kode_alamat" style="font-size:11px;"  value="{{$randomKode}}" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Perusahaan</label>
														<div class="col-sm-10">
															<select name="kode_perusahaan" id="kode_perusahaan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($data['looks'] as $p )
																<option  value="{{$p->kode_perusahaan}}" data-name ="{{$p->nama_perusahaan}}" >{{$p->kode_perusahaan}}</option>
																@endforeach
															</select>		
														</div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm"  style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Perusahaan</label>
                                                        <div class="col-sm-10">
                                                        {{-- <input type="text" required name="nama_perusahaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}}
														<select name="nama_perusahaan" id="name_p" class="form-control form-control-sm" style="font-size:11px;">
															<option value="" selected disabled>--- Pilih ---</option>
															@foreach ($data['looks'] as $np )
															<option value="{{$np->nama_perusahaan}}" data-kode ="{{$np->kode_perusahaan}}">{{$np->nama_perusahaan}}</option>
															@endforeach
														</select>	
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Status Alamat Untuk SPT</label>
                                                        <div class="col-sm-10">
															<select name="status_alamat_untuk_spt" id="status_alamat_untuk_spt" class="form-control form-control-sm" style="font-size:11px;">
																<option value="ya">Ya</option>
																<option value="tidak">Tidak</option>
															</select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Alamat</label>
                                                        <div class="col-sm-10">
															<select name="jenis_alamat" id="jenis_alamat" class="form-control form-control-sm" style="font-size:11px;">
																<option value="Alamat Kantor Pusat">Alamat Kantor Pusat</option>
																<option value="Alamat Cabang">Alamat Cabang</option>
															</select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Alamat</label>
                                                        <div class="col-sm-10">
                                                        <textarea type="text" rows="8" cols="50" required name="alamat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Provinsi</label>
                                                        <div class="col-sm-10">
                                                        	<input type="text" name="kota" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Pos</label>
                                                        <div class="col-sm-10">
                                                        <input type="number" required name="kode_pos" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Telepon</label>
                                                        <div class="col-sm-10">
                                                        <input type="number" name="telepon" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <b style="color:black;">Rekaman Informasi</b><br>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
                                                        <div class="col-sm-10">
                                                        <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
                                                        <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" rows="8" cols="50" style="font-size:11px;" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                                        </div>
                                                    </div>                  
                                                </div>
                                            </div><hr>
                                        </div>
                                    </div>
                                    <div class="">
                                        <table>
                                            <tr>
												<td>
                                                    <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                </td>
                                                <td>
                                                    <a href="{{ route('list_a') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br>
                                </form>
						</div>
                    </div>
                </div>
            </div>
        </div>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
			<script>
				
				$('#kode_perusahaan').change(function(){
					$('#name_p').val($('#kode_perusahaan option:selected').data('name'));
				})
				$('#name_p').change(function(){
					$('#kode_perusahaan').val($('#name_p option:selected').data('kode'));
				})
			</script>
@endsection