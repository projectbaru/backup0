@extends('wsalamat.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Alamat Perusahaan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">alamat perusahaan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                    <form action="{{route('duplicate_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
						<div>
                            <hr>		
                                @foreach($wsalamat as $data)
                                <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_alamat }}" />

                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_a', $data->id_alamat)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_a', $data->id_alamat)}}" id="btn_delete">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_a')}}">Batal</a></td>
                                    </tr>
                                </table><br>
                                <!-- <b style="color:black;">Informasi Perusahaan</b> -->
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Alamat</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_alamat" value="{{$randomKode}}">{{$data->kode_alamat}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_perusahaan" value="{{$data->kode_perusahaan}}">{{$data->kode_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status Alamat Untuk SPT</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="status_alamat_untuk_spt" value="{{$data->status_alamat_untuk_spt}}">{{$data->status_alamat_untuk_spt}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Alamat</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="jenis_alamat" value="{{$data->jenis_alamat}}">{{$data->jenis_alamat}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>
                                                <tr>
                                                    <td>Alamat Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="alamat" value="{{$data->alamat}}">{{$data->alamat}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Provinsi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kota" value="{{$data->kota}}">{{$data->kota}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Pos</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_pos" value="{{$data->kode_pos}}">{{$data->kode_pos}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Telepon</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="telepon" value="{{$data->telepon}}">{{$data->telepon}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data -> tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data -> tanggal_selesai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> <br>       
                                @endforeach
							<br><br>

                        </div>
                    </form>
					</div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_a') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection