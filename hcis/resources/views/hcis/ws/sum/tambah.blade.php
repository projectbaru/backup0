@extends('wsstandarupahminimum.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Tambah Standar Upah Minimum</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">standar upah minimum</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
								<form action="{{route('simpan_usm')}}" method="post" enctype="multipart/form-data">
									<hr>
									{{ csrf_field() }}
									<div class="form-group">
										<div class="">
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Posisi</label>
														<div class="col-sm-10">
															<input type="text" readonly value="{{$random}}" name="kode_posisi" style="font-size:11px;" id="colFormLabelSm" class="form-control form-control-sm" placeholder="">
														</div>
													</div>
													<div class="form-group row" >
														<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
														<div class="col-sm-10">
															<select style="font-size:11px;" required name="nama_posisi" id="produk00" class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($posisi['looks'] as $p )
																<option value="{{$p->nama_posisi}}">{{$p->nama_posisi}}</option>
																@endforeach
															</select>															
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
														<div class="col-sm-10">
															<select name="tingkat_pendidikan" required id="pendidikan" class="form-control form-control-sm" style="font-size:11px;">
                                                                <option value="SD">SD</option>
                                                                <option value="SLTP">SLTP</option>
                                                                <option value="SLTA">SLTA</option>
                                                                <option value="D1">D1</option>
                                                                <option value="D2">D2</option>
                                                                <option value="D3">D3</option>
                                                                <option value="S1">S1</option>
                                                                <option value="S2">S2</option>
                                                                <option value="S3">S3</option>
															</select>
															<!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
														</div>													
													</div>
													
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Lokasi</label>
														<div class="col-sm-10">
															<!-- <select name="kode_lokasi" id="produk00" style='font-size:11px;' required class="form-control form-control-sm" onchange="getProduk('00')" data-number="00">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($lokasi['looks'] as $p )
																<option value="{{$p->kode_grup_lokasi_kerja}}">{{$p->kode_grup_lokasi_kerja}}</option>
																@endforeach
															</select>	 -->
															
															<!-- {{-- <input type="text" readonly name="kode_lokasi" style="font-size:11px;" value="{{$data->lokasi_kerja}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
															<input type="text" readonly name="kode_lokasi" style="font-size:11px;" value="" class="form-control form-control-sm" id="kode_lokasi" placeholder="">
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Lokasi</label>
														<div class="col-sm-10">
															<select name="nama_lokasi" id="nama_lokasi" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
																<option value="" selected disabled>--- Pilih ---</option>
																@foreach ($lokasi['looks'] as $p ) 
																<option value="{{$p->lokasi_kerja}}" data-kode_lokasi="{{$p->kode_grup_lokasi_kerja}}">{{$p->lokasi_kerja}}</option>
																@endforeach
															</select>															
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Upah Minimum</label>
														<div class="col-sm-10">
															<input type="number" required style="font-size:11px;" name="upah_minimum" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
															<span style="font-size:10px;">*ketikkan nomor</span>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<b style="color:black;">Informasi Lainnya</b>
											<div class="row">
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
														<div class="col-sm-10">
															<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" required class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
														<div class="col-sm-10">
														<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
														</div>
													</div>
												</div>												
											</div><hr>
										</div>
									</div>
                                    <div>
										<table>
										<tr>
										
											
											<td>
												<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
											</td>
											<td>
												<a href="{{ route('list_usm') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>
										</tr>
										</table>
									</div>
                                    <br>
								</form>
								</div>
                    </div>
                </div>
            </div>
        </div>

			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			<script>
				$('#nama_lokasi').change(function(){
					$('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
				})
			</script>

@endsection