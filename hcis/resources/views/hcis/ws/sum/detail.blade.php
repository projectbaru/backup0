@extends('wsstandarupahminimum.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Standar Upah Minimum</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">deskripsi pekerjaan</a></li>
										<li class="breadcrumb-item"><a href="#!">standar upah minimum</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>	
                            <form action="{{route('simpan_usm')}}" method="post">
                            <hr>    
                            {{ csrf_field() }}
                                @foreach($wsusm as $data)
                                <input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_standar_upah_minimum }}">

                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_usm', $data->id_standar_upah_minimum)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;"  id="btn_delete" href="{{route('hapus_usm', $data->id_standar_upah_minimum)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_usm')}}">Batal</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_posisi" value="KP-{{now()->isoFormat('dmYYs')}}">{{$data->kode_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Pendidikan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_pendidikan" value="{{$data->tingkat_pendidikan}}">{{$data->tingkat_pendidikan}}</td>
                                                </tr>
                                            </table>
                                        </div>
										
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Lokasi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_lokasi" value="{{$data->kode_lokasi}}">{{$data->kode_lokasi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Lokasi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_lokasi" value="{{$data->nama_lokasi}}">{{$data->nama_lokasi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Upah Minimum</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="upah_minimum" value="{{$data->upah_minimum}}">{{$data->upah_minimum}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <b style="color:black;">Informasi Lainnya</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                            </form>
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_usm') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection