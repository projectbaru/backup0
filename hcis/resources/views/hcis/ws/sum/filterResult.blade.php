@extends('wsstandarupahminimum.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Standar Upah Minimum</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">standar upah minimum</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_sum')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                            <form action="{{ URL::to('/list_usm/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_standar_upah_perusahaan')
                                                    <th></th>
                                                @endif -->
                                                
                                                @if($th[$i] == 'kode_posisi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Posisi</th>
                                                @endif
                                                @if($th[$i] == 'nama_posisi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi</th>
                                                @endif
                                                @if($th[$i] == 'tingkat_pendidikan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Pendidikan</th>
                                                @endif
                                                @if($th[$i] == 'kode_lokasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Lokasi</th>
                                                @endif
                                                @if($th[$i] == 'nama_lokasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi</th>
                                                @endif
                                                @if($th[$i] == 'upah_minimum')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Upah Minimum</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_masuk')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Masuk</th>
                                                @endif
                                                @if($th[$i] == 'waktu_masuk')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Masuk</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_ubah')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Ubah</th>
                                                @endif
                                                @if($th[$i] == 'waktu_ubah')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Ubah</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_hapus')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Hapus</th>
                                                @endif
                                                @if($th[$i] == 'waktu_hapus')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Hapus</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_standar_upah_minimum')
                                                            <td>{{ $row->id_standar_upah_minimum ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_posisi')
                                                            <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_posisi')
                                                            <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tingkat_pendidikan')
                                                            <td>{{ $row->tingkat_pendidikan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_lokasi')
                                                            <td>{{ $row->kode_lokasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_lokasi')
                                                            <td>{{ $row->nama_lokasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'upah_minimum')
                                                            <td>{{ $row->upah_minimum ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_masuk')
                                                            <td>{{ $row->pengguna_masuk ? date('d-m-Y', strtotime($row->tanggal_selesai_perusahaan)) : 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_masuk')
                                                            <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_ubah')
                                                            <td>{{ $row->waktu_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_hapus')
                                                            <td>{{ $row->waktu_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                            <td>  
                                                                <a href="{{ URL::to('/list_usm/detail/'.$row->id_standar_upah_minimum) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;                                                                                                  
                                                                <a href="{{ URL::to('/list_usm/edit/'.$row->id_standar_upah_minimum) }}" class="">Edit</a>
                                                                <!-- <a href="{{ URL::to('/list_wsperusahaan/hapus/'.$row->id_standar_upah_minimum) }}" class="btn btn-danger btn-xs mr-2">Hapus</a> -->
                                                            </td>
                                                            <td>
                                                                <input id="multiDelete" type="checkbox" name="multiDelete[]" value="{{ $row->id_standar_upah_minimum }}">

                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_usm')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <td></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td></td>
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection