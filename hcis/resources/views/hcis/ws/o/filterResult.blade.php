@extends('wsorganisasi.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Organisasi</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">organisasi</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamo')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                            <form action="{{ URL::to('/list_o/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;" >
                                        <thead style="font-size:12px;">
                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_organisasi')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'nama_perusahaan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Perusahaan</th>
                                                @endif
                                                @if($th[$i] == 'kode_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nama_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'tipe_area')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Area</th>
                                                @endif
                                                @if($th[$i] == 'grup_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Grup Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'unit_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Unit Kerja</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai</th>
                                                @endif
                                                @if($th[$i] == 'id_induk_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">ID Induk Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'induk_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Induk Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nama_struktur_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Struktur Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'versi_struktur_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Versi Struktur Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'kode_tingkat_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Tingkat Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'nomor_indeks_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Indeks Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'tingkat_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'urutan_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Urutan Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'keterangan_organisasi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan Organisasi</th>
                                                @endif
                                                @if($th[$i] == 'leher_struktur')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Leher Struktur</th>
                                                @endif
                                                @if($th[$i] == 'aktif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aktif</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_masuk')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Pengguna Masuk</th>
                                                @endif
                                                @if($th[$i] == 'waktu_masuk')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Masuk</th>
                                                @endif
                                                @if($th[$i] == 'pengguna_ubah')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Waktu Masuk</th>
                                                @endif
                                                @if($i == count($th) - 1)
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_organisasi')
                                                            <td>{{ $row->id_organisasi ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'nama_perusahaan')
                                                            <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_organisasi')
                                                            <td>{{ $row->kode_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_organisasi')
                                                            <td>{{ $row->nama_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tipe_area')
                                                            <td>{{ $row->tipe_area ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'grup_organisasi')
                                                            <td>{{ $row->grup_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'unit_kerja')
                                                            <td>{{ $row->unit_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai')
                                                            <td>{{ $row->tanggal_mulai ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai')
                                                            <td>{{ $row->tanggal_selesai ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_induk_organisasi')
                                                            <td>{{ $row->id_induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'induk_organisasi')
                                                            <td>{{ $row->induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_struktur_organisasi')
                                                            <td>{{ $row->nama_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'versi_struktur_organisasi')
                                                            <td>{{ $row->versi_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_tingkat_organisasi')
                                                            <td>{{ $row->kode_tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_indeks_organisasi')
                                                            <td>{{ $row->nomor_indeks_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tingkat_organisasi')
                                                            <td>{{ $row->tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'urutan_organisasi')
                                                            <td>{{ $row->urutan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan_organisasi')
                                                            <td>{{ $row->keterangan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'leher_struktur')
                                                            <td>{{ $row->leher_struktur ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'aktif')
                                                            <td>{{ $row->aktif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_masuk')
                                                            <td>{{ $row->pengguna_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_masuk')
                                                            <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                            <td>                                                                                                        
                                                                <a href="{{ URL::to('/list_o/detail/'.$row->id_organisasi) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                <a href="{{ URL::to('/list_o/edit/'.$row->id_organisasi) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_organisasi }}" id="multiDelete">
                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_o')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <td></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td></td>
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                 
                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection