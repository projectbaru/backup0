@extends('wstingkatposisi.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Tingkat Posisi</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">tingkat posisi</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                                    <form action="{{route('update_tp')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group" style="width:95%;"><br>
                                            <div class="">
                                                @foreach($wstingkatposisi as $data)
                                                <div class="row">
                                                    <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                                <div class="col-sm-10">
                                                                <input type="hidden" name="id_tingkat_posisi" value="{{ $data->id_tingkat_posisi }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Posisi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="kode_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" readonly value="{{$data->kode_tingkat_posisi}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Posisi</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="nama_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_tingkat_posisi}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Urutan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" required name="urutan_tingkat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tingkat}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="urutan_tampilan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tampilan}}" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                	<textarea  rows="4" cols="50" type="text" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->keterangan}}" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">

                                                                	<!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tanggal_mulai_efektif}}" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}" placeholder="Tanggal selesai efektif" />

                                                                	<!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tanggal_selesai_efektif}}" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													<hr>
                                                </div>
                                               
                                                @endforeach
                                                <div>
                                                    <table>
                                                        <tr>
                
                                                        <!-- <td>
                                                            <a href="" class="btn">Hapus</a>
                                                        </td> -->
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_tp',$data->id_tingkat_posisi)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_tp') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
									</div>
            </div>
        </div>
    </div>
</div>
@endsection
