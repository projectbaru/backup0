@extends('wslokasikerja.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Lokasi Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">lokasi kerja</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                            <form action="{{route('simpan_duplicate')}}" method="post">{{ csrf_field() }}
                                <hr>
                                @foreach($wslokasikerja as $data)
                                <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_lokasi_kerja }}" />

                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_lk', $data->id_lokasi_kerja)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_lk', $data->id_lokasi_kerja)}}" id="btn_delete">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_lk')}}">Batal</a></td>                
                                    </tr>
                                </table><br>
                                <!-- <b style="color:black;">Informasi Perusahaan</b> -->
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_lokasi_kerja" value="LK-{{now()->isoFormat('dmYYs')}}">{{$data->kode_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_lokasi_kerja" value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Grup Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_grup_lokasi_kerja" value="{{$data->nama_grup_lokasi_kerja}}">{{$data->nama_grup_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>ID Kantor Cabang</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="id_kantor" value="{{$data->id_kantor}}">{{$data->id_kantor}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Zona Waktu</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="zona_waktu" value="{{$data->zona_waktu}}">{{$data->zona_waktu}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="alamat" value="{{$data->alamat}}">{{$data->alamat}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Provinsi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="provinsi" value="{{$data->prov_id}}">{{$data->prov_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kecamatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kecamatan" value="{{$data->dis_id}}">{{$data->dis_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kelurahan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kelurahan" value="{{$data->subdis_id}}">{{$data->subdis_name}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>
                                                <tr>
                                                    <td>Kabupaten / Kota</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kabupaten_kota" value="{{$data->city_id}}">{{$data->city_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Pos</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_pos" value="{{$data->kode_pos}}">{{$data->kode_pos}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Negara</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="negara" value="{{$data->negara}}">{{$data->negara}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nomor Telepon</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nomor_telepon" value="{{$data->nomor_telepon}}">{{$data->nomor_telepon}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="email" value="{{$data->email}}">{{$data->email}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Fax</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="fax" value="{{$data->fax}}">{{$data->fax}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div><br>
                                @endforeach
                            </form>
							</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_lk') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection