@extends('wslokasikerja.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Tambah Lokasi Kerja</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">lokasi kerja</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('simpan_lk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
                        <div class="form-group">
                            <div class="">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <input required type="text" value="{{$random}}" readonly name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($glk['looks'] as $glk )
                                                    <option value="{{$glk->nama_grup_lokasi_kerja}}" data-kode_lokasi="{{$glk->kode_grup_lokasi_kerja}}">{{$glk->nama_grup_lokasi_kerja }}</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input required type="text" name="id_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-grup row">
                                            <input type="hidden"  name="id_grup_lokasi_kerja" id="kode_lokasi" style="font-size:11px;" value="" class="form-control form-control-sm" placeholder="">

                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Kantor Cabang</label>
                                            <div class="col-sm-10">
                                                <select name="id_kantor" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                    @foreach ($ik['looks'] as $ik )
                                                    <option value="{{$ik->kode_kantor}}">{{$ik->kode_kantor}}&nbsp;[{{$ik->nama_kantor}}]</option>
                                                    @endforeach
                                                </select>
                                                <!-- <input required type="text" name="id_kantor" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Zona Waktu</label>
                                            <div class="col-sm-6">
                                                <input required type="number" name="zona_waktu" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                       
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat Perusahaan</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="alamat" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="province_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="province_id" id="province_id" style="font-size:11px;" class="form-control-select select2" onchange="getDropdown(this, 'dropDownCity', 'city_id');">
                                                    <option value=""  style="font-size:11px;" selected disabled>--Pilih disini--</option>
                                                    @foreach($prov as $k)
                                                    <option value="{{$k->prov_id}}"  style="font-size:11px;">{{ $k->prov_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kabupaten/Kota</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="city_id" id="city_id"  style="font-size:11px;" class="form-control-select select2" disabled onchange="getDropdown(this, 'dropDownDistrict', 'dis_id');">
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="dis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kecamatan</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <select name="dis_id" id="dis_id"  style="font-size:11px;" class="form-control-select select2" disabled onchange="getDropdown(this, 'dropDownSubDistrict', 'subdis_id');">
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="subdis_id" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kelurahan</label>
                                            <div class="col-sm-10">
                                                <select name="subdis_id" id="subdis_id"  style="font-size:11px;" class="form-control-select select2" disabled>
                                                    <option value="" selected disabled  style="font-size:11px;">--Pilih disini--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
                                            <div class="col-sm-10"  style="font-size:11px;">
                                                <input required type="number"  style="font-size:11px;" name="kode_pos" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Negara</label>
                                            <div class="col-sm-10">
                                                <select required name="negara" id="negara" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="" disabled selected>--Belum Dipilih--</option>
                                                    <option value="indonesia">Indonesia</option>
                                                    <option value="lainnya">Lainnya</option>
                                                </select>
                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Telepon</label>
                                            <div class="col-sm-10">
                                                <input required type="text" name="nomor_telepon" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Fax</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="fax" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                            <div class="col-sm-10">
                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_lk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')

<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_lokasi').val($('#nama_grup_lokasi_kerja option:selected').data('kode_lokasi'));
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });

    async function getDropdown(sel, urlParams, elm) {
        // console.log(sel.value);
        // var prov = $("#province_id").val();
        if (sel.value !== "") {
            var param = {
                params: sel.value
            };
            await ajaxData(param, urlParams, elm);
        }
    }

    function ajaxData(param, urlAjax, elm) {
        let url = `{{ url('${urlAjax}') }}`;

        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            dataType: 'JSON',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(res) {
                if (res.status) {
                    $(`#${elm}`).removeAttr('disabled');
                    resovlData(res.data, elm);
                }
            },

            error: function(err) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal!",
                    text: "Terjadi kesalahan saat menghapus data.",
                });
            },
        });
    }

    function resovlData(data, elm) {
        if (data) {
            var html = '';
            html += '<option value="" disabled style="font-size:11px;" selected>--Belum dipilih--</option>';
            data.forEach(el => {
                html += '<option style="font-size:11px;" value="' + el.id + '">' + el.text + '</option>';
            });

            $(`#${elm}`).html(html);
        }
    }
</script>
@endsection