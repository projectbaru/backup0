@extends('wslokasikerja.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Lokasi Kerja</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">lokasi kerja</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamlk')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                            <form action="{{ URL::to('/list_lk/hapus_banyak') }}" method="POST" id="form_delete">
                                @csrf
                                    <table id="example" class="table-responsive-xl table-xl table-striped table-bordered mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead>
                                            <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_lokasi_kerja')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'kode_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'nama_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'id_grup_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Id Grup Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'nama_grup_lokasi_kerja')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Grup Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'id_kantor')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Id Kantor</th>
                                                @endif
                                                @if($th[$i] == 'zona_waktu')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Zona Waktu</th>
                                                @endif
                                                @if($th[$i] == 'alamat')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Alamat Perusahaan</th>
                                                @endif
                                                @if($th[$i] == 'provinsi')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Provinsi</th>
                                                @endif
                                                @if($th[$i] == 'kecamatan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kecamatan</th>
                                                @endif
                                                @if($th[$i] == 'kelurahan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kelurahan</th>
                                                @endif
                                                @if($th[$i] == 'kabupaten_kota')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kabupaten Kota</th>
                                                @endif
                                                @if($th[$i] == 'kode_pos')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Pos</th>
                                                @endif
                                                @if($th[$i] == 'negara')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Negara</th>
                                                @endif
                                                @if($th[$i] == 'nomor_telepon')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Telepon</th>
                                                @endif
                                                @if($th[$i] == 'email')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Email</th>
                                                @endif
                                                @if($th[$i] == 'fax')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Fax</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                    <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_lokasi_kerja')
                                                            <td>{{ $row->id_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_lokasi_kerja')
                                                            <td>{{ $row->kode_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_lokasi_kerja')
                                                            <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_grup_lokasi_kerja')
                                                            <td>{{ $row->id_grup_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_grup_lokasi_kerja')
                                                            <td>{{ $row->nama_grup_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_kantor')
                                                            <td>{{ $row->id_kantor ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'zona_waktu')
                                                            <td>{{ $row->zona_waktu ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'alamat')
                                                            <td>{{ $row->alamat ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'provinsi')
                                                            <td>{{ $row->prov_name ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kecamatan')
                                                            <td>{{ $row->dis_name ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kelurahan')
                                                            <td>{{ $row->subdis_name ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kabupaten_kota')
                                                            <td>{{ $row->city_name ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_pos')
                                                            <td>{{ $row->kode_pos ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'negara')
                                                            <td>{{ $row->negara ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_telepon')
                                                            <td>{{ $row->nomor_telepon ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'email')
                                                            <td>{{ $row->email ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'fax')
                                                            <td>{{ $row->fax ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        
                                                        @if($i == count($th) - 1)
                                                            <td> 
                                                                <a href="{{ URL::to('/list_lk/detail/'.$row->id_lokasi_kerja) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;                                                                                                   
                                                                <a href="{{ URL::to('/list_lk/edit/'.$row->id_lokasi_kerja) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_lokasi_kerja }}" id="multiDelete">

                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_lk')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection

