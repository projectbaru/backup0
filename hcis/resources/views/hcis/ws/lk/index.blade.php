@extends('wslokasikerja.side')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">

                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Lokasi Kerja</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">lokasi kerja</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tamlk')}}"><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                <form action="{{ URL::to('/list_lk/hapus_banyak') }}" method="POST">
                                    @csrf
                                    <table id="example" class="table-responsive-xl table-xl table-striped table-bordered mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                        <thead style="color:black;">
                                            <tr>
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Kode Lokasi Kerja</th>
                                                <!-- {{-- <th scope="col">Logo Perusahaan</th> --}} -->
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Nama Lokasi Kerja</th>
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Aksi</th>
                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size:11px;">
                                            @php $b=1; @endphp
                                            @foreach ($wslokasikerja as $data)
                                            {{-- {{dd($data)}} --}}
                                                <tr>
                                                    <td>{{ $b++; }}</td>
                                                    <td>{{ $data->kode_lokasi_kerja }}</td>
                                                    <!-- {{-- <td>{{$data -> logo_perusahaan}}</td> --}} -->
                                                    <td>{{ $data->nama_lokasi_kerja }}</td>
                                                    <td>
                                                        <a href="{{ URL::to('/list_lk/detail/'.$data->id_lokasi_kerja) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <a href="{{ URL::to('/list_lk/edit/'.$data->id_lokasi_kerja) }}" class="">Edit</a>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="multiDelete[]" value="{{ $data->id_lokasi_kerja }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_lk')}}">Tambah</a></td>
                                            <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                            <td>
                                            <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" style="border-radius:5px; font-size:11px;" data-bs-target="#deleteModal">
                                                Hapus
                                            </button>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                        <div class="modal-body text-center">
                                                            <i class="fa-regular fa-circle-exclamation text-warning" style="font-size: 58px;"></i>
                                                            <br><br>
                                                            <p>Hapus list yang dipilih?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
                                                            <button type="submit" class="btn btn-primary">Ya</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

