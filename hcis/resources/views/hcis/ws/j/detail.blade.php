@extends('wsjabatan.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;">
                                            
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Detail Jabatan</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">jabatan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <form action="{{route('simpan_j')}}" method="post">{{ csrf_field() }}
                                <hr>
                                @foreach($wsjabatan as $data)
                                <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_jabatan }}" />

                                <table>
                                    <tr>
                                        
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_j', $data->id_jabatan)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_j', $data->id_jabatan)}}" id="btn_delete">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_j')}}">Batal</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                <b style="color:black;">Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_jabatan" value="J-{{now()->isoFormat('dmYYs')}}">{{$data->kode_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_jabatan" value="{{$data->nama_jabatan}}">{{$data->nama_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Kelompok Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_kelompok_jabatan" value="{{$data->kode_kelompok_jabatan}}">{{$data->kode_kelompok_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Kelompok Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_kelompok_jabatan" value="{{$data->nama_kelompok_jabatan}}">{{$data->nama_kelompok_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Grup Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="grup_jabatan" value="{{$data->grup_jabatan}}">{{$data->grup_jabatan}}</td>
                                                </tr>
                                
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai" value="{{$data->tanggal_mulai}}">{{$data->tanggal_mulai}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai" value="{{$data->tanggal_selesai}}">{{$data->tanggal_selesai}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Magang</td>
                                                    <td>:</td>
                                                    <td><input type="checkbox" name="magang" onclick="return false" value="{{$data->magang}}" {{ $data->magang == 'ya' ? 'checked' : NULL }}></td>
                                                </tr>
                                                <tr>
                                                    <td>Tipe Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tipe_jabatan" value="{{$data->tipe_jabatan}}">{{$data->tipe_jabatan}}</td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </div>
                                <hr>
                                <b style="color:black;">Informasi Lainnya</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Dari Golongan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data -> dari_golongan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sampai Golongan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data -> sampai_golongan}}</td>
                                                </tr>
                                               
                                            </table>
                                        </div>
                                    </div>
                                <hr>  
                                <b style="color:black;">Rekaman Informasi</b>
                                <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data -> tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data -> tanggal_selesai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                <hr>     
                                @endforeach
                            </form>
							</div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_j') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection