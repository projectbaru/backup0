@extends('wstingkat_org.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Ubah Tingkat Organisasi</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">tingkat Organisasi</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                                    <form action="{{route('update_to')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group" style="width:95%;">
                                            <div class="">
                                                @foreach($wstingkatorganisasi as $data)
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                            <div class="col-sm-10">
                                                            <input type="hidden" name="id_tingkat_organisasi" value="{{ $data->id_tingkat_organisasi }}" name="temp_id" id="temp_id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="kode_tingkat_organisasi" readonly class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_tingkat_organisasi}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="nama_tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_tingkat_organisasi}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Urutan</label>
                                                            <div class="col-sm-10">
                                                            <input type="number" name="urutan_tingkat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tingkat}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                                            <div class="col-sm-10">
                                                            <input type="number" name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->urutan_tampilan}}" id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" rows="4" cols="50" name="keterangan" value="{{ $data->keterangan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" style="font-size:11px;">{{ $data->keterangan }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">

                                                            <!-- <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tanggal_mulai_efektif}}"> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai  Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" value="{{ $data->tanggal_selesai_efektif }}" />

                                                            <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->tanggal_selesai_efektif}}"> -->
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div><hr>
                                                @endforeach
                                                <div class="" style="">
                                                    <table>
                                                        <tr>
                
                                                        <!-- <td>
                                                            <a href="" class="btn">Hapus</a>
                                                        </td> -->
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_to',$data->id_tingkat_organisasi)}}" class="btn"  id="btn_delete" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_to') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_to') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection