@extends('wstingkat_org.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Tambah Tingkat Organisasi</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">tingkat Organisasi</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>    
                                <form action="{{route('simpan_to')}}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<hr>
									<div class="form-group" style="width:95%;">
										<div class="">
											<div class="row">
												<div class="col" style="font-size: 10px;">
													<!-- <div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Tingkat Organisasi</label>
														<div class="col-sm-10">
															<input type="text" required name="tingkat_organisasi"  value="KTO-{{now()->isoFormat('dmYYs')}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="" class="form-control form-control-sm"></div>
													</div> -->
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Tingkat Organisasi</label>
														<div class="col-sm-10">
															<input type="text" required name="kode_tingkat_organisasi"  value="{{$random}}" readonly style="font-size:11px;" id="colFormLabelSm" placeholder="" class="form-control form-control-sm"></div>
													</div>
													<div class="form-group row" >
														<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Organisasi</label>
														<div class="col-sm-10">
															<input type="text" required style="font-size:11px;" name="nama_tingkat_organisasi" class="form-control form-control-sm" require id="colFormLabelSm" placeholder=""></div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Urutan</label>
														<div class="col-sm-10">
															<input type="number" required style="font-size:11px;" name="urutan_tingkat" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
														<div class="col-sm-10">
															<input type="number" required style="font-size:11px;" name="urutan_tampilan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></div>
													</div>
													
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
														<div class="col-sm-10">
															<textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                                        </div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Mulai Efektif</label>
														<div class="col-sm-10">
															<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
														<div class="col-sm-10">
															<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif"/>
														</div>
													</div>
												</div>
											</div>
											<hr>
										
										</div>
									</div>
                                    <div class="" style="">
										<table>
										<tr>
						
											
											<td>
												<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
											</td>
											<td>
												<a href="{{ route('list_to') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>
										</tr>
										</table>
									</div>
                                    <br>
								</form>
								</div>
			</div>
		</div>
	</div>
</div>
@endsection

