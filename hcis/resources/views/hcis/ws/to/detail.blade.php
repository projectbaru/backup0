@extends('wstingkat_org.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									
									<div class="page-header-title">
										<h5 class="m-b-10">Detail Tingkat Organisasi</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">work strukture</a></li>
										<li class="breadcrumb-item"><a href="#!">tingkat Organisasi</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>   
                                <form action="{{route('simpan_to')}}" method="post">{{ csrf_field() }}
                                    <hr>
                                    @foreach($wstingkatorganisasi as $data)
                                    <input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_tingkat_organisasi }}" />

                                    <table>
                                        <tr>
                                            <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_to', $data->id_tingkat_organisasi)}}">Ubah</a></td>
                                            <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" id="btn_delete" href="{{route('hapus_to', $data->id_tingkat_organisasi)}}">Hapus</a></td>
                                            <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_to')}}">Batal</a></td>
                                        </tr>
                                    </table><br>
                                            <div class="row">
                                                <div class="col">
                                                    <table style="font-size:12px;">
                                                        <tr>
                                                            <td>Kode Tingkat Organisasi</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="kode_tingkat_organisasi" value="{{$random}}">{{$data->kode_tingkat_organisasi}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Tingkat Organisasi</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="nama_tingkat_organisasi" value="{{$data->nama_tingkat_organisasi}}">{{$data->nama_tingkat_organisasi}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tingkat Urutan</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="urutan_tingkat" value="{{$data->urutan_tingkat}}">{{$data->urutan_tingkat}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Urutan Tampilan</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="urutan_tampilan" value="{{$data->urutan_tampilan}}">{{$data->urutan_tampilan}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col" style="font-size:12px;">
                                                    <table>
                                                        <tr>
                                                            <td>Keterangan</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Mulai Efektif</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Selesai Efektif</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        
                                    <br>
                                    @endforeach
                                </form>
                                </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_to') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection