@extends('wsposisi.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Edit Posisi</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">posisi</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <form action="{{route('update_po')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                        <hr>
                        <div class="form-group" style="width:95%;">
                            <div class="">
                                @foreach($wsposisi as $data)
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                            <div class="col-sm-10">
                                            <input type="hidden" name="id_posisi" value="{{ $data->id_posisi }}" class="form-control form-control-sm" id="colFormLabelSm" name="temp_id" id="temp_id"  placeholder="">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
                                            <div class="col-sm-10">
                                            <input type="text" name="kode_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_perusahaan}}">
                                            </div>
                                        </div> -->
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                            <div class="col-sm-10">
                                                <select name="nama_perusahaan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datap['looks'] as $p)
                                                        <option value="{{$p->nama_perusahaan}}" {{ $p->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$p->nama_perusahaan}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_perusahaan}}"> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                                            <div class="col-sm-10">
                                            <input type="text" required name="kode_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_posisi}}" readonly id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                            <div class="col-sm-10">
                                            <input type="text" required name="nama_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_posisi}}" id="colFormLabelSm" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode MPP</label>
                                            <div class="col-sm-10">
                                            <input type="text" required name="kode_mpp" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_mpp}}" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="tingkat_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datatp['looks'] as $tp)
                                                        <option value="{{$tp->urutan_tingkat}}" {{ $tp->urutan_tingkat == $data->tingkat_posisi ? 'selected' : NULL }}>{{$tp->urutan_tingkat}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Detail Tingkat Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="detail_tingkat_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datatp['looks'] as $dtp)
                                                        <option value="{{$dtp->nama_tingkat_posisi}}" {{ $dtp->nama_tingkat_posisi == $data->detail_tingkat_posisi ? 'selected' : NULL }}>{{$dtp->nama_tingkat_posisi}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" name="detail_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->detail_tingkat_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                                            <div class="col-sm-10">
                                                <select name="nama_jabatan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($dataj['looks'] as $j)
                                                        <option value="{{$j->nama_jabatan}}" {{ $j->nama_jabatan == $data->nama_jabatan ? 'selected' : NULL }}>{{$j->nama_jabatan}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" required name="nama_jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_jabatan}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                            <div class="col-sm-10">
                                                <select name="nama_organisasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datao['looks'] as $o)
                                                        <option value="{{$o->nama_organisasi}}" {{ $o->nama_organisasi == $data->nama_organisasi ? 'selected' : NULL }}>{{$o->nama_organisasi}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" required name="nama_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_organisasi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                                            <div class="col-sm-10">
                                                <select name="tingkat_organisasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datato['looks'] as $ut)
                                                        <option value="{{$ut->urutan_tingkat}}" {{ $ut->urutan_tingkat == $data->tingkat_organisasi ? 'selected' : NULL }}>{{$ut->urutan_tingkat}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" required name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_organisasi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Posisi</label>
                                            <div class="col-sm-10">
                                                <select name="tipe_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    <option value="none" {{ $data->tipe_posisi == 'none' ? 'selected' : NULL }}>None</option>
                                                    <option value="tunggal" {{ $data->tipe_posisi == 'tunggal' ? 'selected' : NULL }}>Tunggal</option>
                                                    <option value="banyak" {{ $data->tipe_posisi == 'banyak' ? 'selected' : NULL }}>Banyak</option>
                                                </select>
                                            <!-- <input type="text" name="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tipe_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan Posisi</label>
                                            <div class="col-sm-10">
                                            <textarea type="text" name="deskripsi_pekerjaan_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi_pekerjaan_posisi}}" id="colFormLabelSm" placeholder="">{{$data->deskripsi_pekerjaan_posisi}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi Atasan</label>
                                            <div class="col-sm-10">
                                            <input type="text" name="nama_posisi_atasan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_posisi_atasan}}" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                    </div>                                                    
                                </div>
                                <hr>
                                <b style="color:black;">Informasi Lainnya</b>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                            <div class="col-sm-10">
                                                <select name="dari_golongan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datadg['looks'] as $v)
                                                        <option value="{{$v->nama_golongan}}" {{ $v->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$v->nama_golongan}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" name="dari_golongan" style="font-size:11px;" value="{{$data->dari_golongan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                            <div class="col-sm-10">
                                                <select name="sampai_golongan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                    @foreach ($datadg['looks'] as $p)
                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                    @endforeach
                                                </select>
                                            <!-- <input type="text" name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->sampai_golongan}}" id="colFormLabelSm" placeholder=""> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Area</label>
                                            <div class="col-sm-10">      
                                                <select name="tipe_area" id="tipe_area" class="form-control form-control-sm" style="font-size:11px;">
                                                    <option value="BO" {{ $data->tipe_area == 'BO' ? 'selected' : NULL }}>BO</option>
                                                    <option value="HO" {{ $data->tipe_area == 'HO' ? 'selected' : NULL }}>HO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    @foreach($dataglk['looks'] as $lk) 
                                                        <option value="{{$lk->lokasi_kerja}}" {{ $lk->lokasi_kerja == $data->lokasi_kerja ? 'selected' : NULL }}>{{$lk->lokasi_kerja}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Lokasi Kerja</label>
                                            <div class="col-sm-10">
                                                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                                    @foreach($dataglk['looks'] as $glk)
                                                        <option value="{{$glk->nama_grup_lokasi_kerja}}" {{ $data->nama_grup_lokasi_kerja ==  $glk->nama_grup_lokasi_kerja ? 'selected' : NULL }}>{{$glk->nama_grup_lokasi_kerja}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>  
                                    </div>
                                </div> 
                                <hr> 
                                <b style="color:black;">Flag Posisi</b>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Posisi Aktif</label>
                                            <div class="col-sm-5">
                                                
                                            <input type="checkbox" name="posisi_aktif" style="font-size:11px;" class="" id="colFormLabelSm" value="yes" placeholder="" {{ $data->posisi_aktif == 'yes' ? 'checked' : NULL }}>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Komisi</label>
                                            <div class="col-sm-5">
                                            <input type="checkbox" name="komisi" class="" id="colFormLabelSm" style="font-size:11px;" placeholder="" value="yes" placeholder="" {{ $data->komisi == 'yes' ? 'checked' : NULL }}>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kepala Fungsional</label>
                                            <div class="col-sm-5">
                                            <input type="checkbox" name="kepala_fungsional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="" value="yes" placeholder="" {{ $data->kepala_fungsional == 'yes' ? 'checked' : NULL }}> 
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Flag Operasional</label>
                                            <div class="col-sm-5">
                                            <input type="checkbox" name="flag_operasional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="" value="yes" placeholder="" {{ $data->flag_operasional == 'yes' ? 'checked' : NULL }}>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Posisi</label>
                                            <div class="col-sm-5">
                                                <div class="form-check col">
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios1" value="kosong" {{ $data->status_posisi == 'kosong' ? 'checked' : NULL }}>
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        Kosong
                                                    </label><br>
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios2" value="terisi" {{ $data->status_posisi == 'terisi' ? 'checked' : NULL }}>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Terisi
                                                    </label><br>
                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)" {{ $data->status_posisi == 'terisi(pjt)' ? 'checked' : NULL }}>
                                                    <label class="form-check-label" for="exampleRadios3">
                                                        Terisi (PJT)
                                                    </label><br>
                                                </div>     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Surat</label>
                                            <div class="col-sm-10">
                                            <input type="text" name="nomor_surat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nomor_surat}}" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                                            <div class="col-sm-10">
                                            <input type="number" name="jumlah_karyawan_dengan_posisi_ini" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->jumlah_karyawan_dengan_posisi_ini}}" id="colFormLabelSm" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                <div class="col-sm-10">
                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}" placeholder="Tanggal mulai efektif">
                                                    <!-- <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm"  value="{{$data->tanggal_mulai_efektif}}" id="colFormLabelSm" placeholder=""> -->
                                                </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}" placeholder="Tanggal selesai efektif"/>

                                                    <!-- <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm"  id="colFormLabelSm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                    <div class="col-sm-10">
                                                    <textarea rows="4" cols="50" type="text" name="keterangan" class="form-control form-control-sm" value="{{$data->keterangan}}"   id="colFormLabelSm" style="font-size:11px;" placeholder="">{{$data->keterangan}}</textarea>
                                                    </div>
                                                </div>
                                                <hr>
                                        </div>
                                        
                                    </div>
                                </div>
                                @endforeach
                                <div>
                                    <table>
                                        <tr>

                                        <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                            <td>
                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_po',$data->id_posisi)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;" id="btn_delete" >Hapus</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('list_po') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection