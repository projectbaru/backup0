@extends('wsposisi.side')

@section('content')
<div class="pcoded-wrapper" >
    <div class="pcoded-content" >
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Posisi</h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <li class="breadcrumb-item"><a href="#!">posisi</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-header">
                                    <!-- <h5>Per Table</h5> -->
                                    <a href="{{route('ubah_tampo')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                </div>
                                <div class="card-body table-border-style ">
                                    <div class="table-responsive-xl" style="overflow-x:auto">
                                        <form action="{{ URL::to('/list_po/hapus_banyak') }}" method="POST" id="form_delete">
                                            @csrf
                                                <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                                                    <thead>
                                                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">No</th>
                                                        @for($i = 0; $i < count($th); $i++)
                                                            <!-- @if($th[$i] == 'id_posisi')
                                                                <th></th>
                                                            @endif -->
                                                            @if($th[$i] == 'kode_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_perusahaan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Perusahaan</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_mpp')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode MPP</th>
                                                            @endif
                                                            @if($th[$i] == 'tingkat_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'detail_tingkat_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Detail Tingkat Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_jabatan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Jabatan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_jabatan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Jabatan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_organisasi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Organisasi</th>
                                                            @endif
                                                            @if($th[$i] == 'tingkat_organisasi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Organisasi</th>
                                                            @endif
                                                            @if($th[$i] == 'tipe_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'deskripsi_pekerjaan_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Deskripsi Pekerjaan Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'kode_posisi_atasan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kode Posisi Atasan</th>
                                                            @endif
                                                            @if($th[$i] == 'nama_posisi_atasan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nama Posisi Atasan</th>
                                                            @endif
                                                            @if($th[$i] == 'tipe_area')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tipe Area</th>
                                                            @endif
                                                            @if($th[$i] == 'dari_golongan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Dari Golongan</th>
                                                            @endif
                                                            @if($th[$i] == 'sampai_golongan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Sampai Golongan</th>
                                                            @endif
                                                            @if($th[$i] == 'posisi_aktif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Posisi Aktif</th>
                                                            @endif
                                                            @if($th[$i] == 'komisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Komisi</th>
                                                            @endif
                                                            @if($th[$i] == 'kepala_fungsional')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Kepala Fungsional</th>
                                                            @endif
                                                            @if($th[$i] == 'flag_operasional')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Flag Operasional</th>
                                                            @endif
                                                            @if($th[$i] == 'status_posisi')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Posisi</th>
                                                            @endif
                                                            @if($th[$i] == 'nomor_surat')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Nomor Surat</th>
                                                            @endif
                                                            @if($th[$i] == 'jumlah_karyawan_dengan_posisi_ini')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Jumlah Karyawan Posisi Ini</th>
                                                            @endif
                                                            @if($th[$i] == 'keterangan')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan</th>
                                                            @endif
                                                            @if($th[$i] == 'status_rekaman')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_mulai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Efektif</th>
                                                            @endif
                                                            @if($th[$i] == 'tanggal_selesai_efektif')
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Selesai Efektif</th>
                                                            @endif
                                                            
                                                            @if($i == count($th) - 1)
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                            @endif
                                                        @endfor
                                                    </thead>
                                                    <tbody style="font-size:11px;">             
                                                        @php $b=1; @endphp
                                                        @foreach($query as $row)
                                                            <tr>
                                                                <td>{{$b++;}}</td>

                                                                @for($i = 0; $i < count($th); $i++)
                                                                    <!-- @if($th[$i] == 'id_posisi')
                                                                        <td>{{ $row->id_posisi ?? 'NO DATA' }}</td>
                                                                    @endif -->
                                                                    @if($th[$i] == 'kode_perusahaan')
                                                                        <td>{{ $row->kode_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_perusahaan')
                                                                        <td>{{ $row->nama_perusahaan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_posisi')
                                                                        <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_posisi')
                                                                        <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_mpp')
                                                                        <td>{{ $row->kode_mpp ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tingkat_posisi')
                                                                        <td>{{ $row->tingkat_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'detail_tingkat_posisi')
                                                                        <td>{{ $row->detail_tingkat_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_jabatan')
                                                                        <td>{{ $row->kode_jabatan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_jabatan')
                                                                        <td>{{ $row->nama_jabatan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_organisasi')
                                                                        <td>{{ $row->nama_organisasi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tingkat_organisasi')
                                                                        <td>{{ $row->tingkat_organisasi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tipe_posisi')
                                                                        <td>{{ $row->tipe_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'deskripsi_pekerjaan_posisi')
                                                                        <td>{{ $row->deskripsi_pekerjaan_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kode_posisi_atasan')
                                                                        <td>{{ $row->kode_posisi_atasan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nama_posisi_atasan')
                                                                        <td>{{ $row->nama_posisi_atasan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tipe_area')
                                                                        <td>{{ $row->tipe_area ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'dari_golongan')
                                                                        <td>{{ $row->dari_golongan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'sampai_golongan')
                                                                        <td>{{ $row->sampai_golongan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'posisi_aktif')
                                                                        <td>{{ $row->posisi_aktif ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'komisi')
                                                                        <td>{{ $row->komisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'kepala_fungsional')
                                                                        <td>{{ $row->kepala_fungsional ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'flag_operasional')
                                                                        <td>{{ $row->flag_operasional ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'status_posisi')
                                                                        <td>{{ $row->status_posisi ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'nomor_surat')
                                                                        <td>{{ $row->nomor_surat ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'jumlah_karyawan_dengan_posisi_ini')
                                                                        <td>{{ $row->jumlah_karyawan_dengan_posisi_ini ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'keterangan')
                                                                        <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'status_rekaman')
                                                                        <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_mulai_efektif')
                                                                        <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }} </td>
                                                                    @endif
                                                                    @if($th[$i] == 'tanggal_selesai_efektif')
                                                                        <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                                    @endif
                                                                    
                                                                    @if($i == count($th) - 1)
                                                                        <td> 
                                                                            <a href="{{ URL::to('/list_po/detail/'.$row->id_posisi) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                            <a href="{{ URL::to('/list_po/edit/'.$row->id_posisi) }}" class="">Edit</a>
                                                                        </td>
                                                                        <td>
                                                                        <input type="checkbox" name="multiDelete[]" value="{{ $row->id_posisi }}" id="multiDelete">                                  

                                                                        </td>
                                                                    @endif
                                                                @endfor
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_po')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                        <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                        <td>
                                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection
