
@extends('re.cv.side')
@section('content')


<div class="pcoded-wrapper">
    <!-- Link to open the modal -->
   
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <br>
                    <br>
                    <div class="row ">
                        <div class="col-xl-12">
                            <div class="card" style="border-radius:8px;border:none;">
                                <div class="card-body  table-border-style">
                                    <div class="table-responsive-xl " style="">
                                        <form action="{{ route('filter_dua_dcp')}}" method="POST">
                                            @csrf
                                            <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                                                <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                                                    <p>List FPTK Report</p>
                                                </div>
                                      
                                            </div>                                          
                                        </form>
                                    </div>
                                    <div class="mb-6">
                                        <div class="card-header">
                                            <!-- <h5>Per Table</h5> -->
                                            <a href="{{route('ubah_tamalamat')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                        </div><br>
                                        <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
                                            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                                                <thead style="color:black;font-size: 14px; ">
                                                    <tr>
                                                        <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">No</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Nama Pemohon</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">No Dokumen</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Jabatan Yang Dibutuhkan</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Tingkat</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Jumlah Kebutuhan</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Lampiran</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Tanggal Upload Dokumen Terakhir</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Action</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">V</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="font-size: 12px;color:black;">
                                                @php $b=1; @endphp
                                                    @foreach($rc_fptk as $data)
                                                    {{-- {{dd($data)}} --}}
                                                        <tr>
                                                            <td>{{$b++;}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->nama_pemohon}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->no_dokumen}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->jabatan_pemohon}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->tingkat_kepegawaian}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->jumlah_kebutuhan}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->lampiran_file}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->tanggal_upload_dokumen_terakhir}}</td>
                                                            <td>
                                                                <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                                <a href="{{ URL::to('/list_fptk/detail/'.$data->id_fptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                <a href="{{ URL::to('/list_fptk/edit/'.$data->id_fptk) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                            <input type="checkbox" name="multiDelete[]" value="{{ $data->id_fptk }}" id="multiDelete" >
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_dpp')}}">Tambah</a></td>
                                                    <td>
                                                        <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                                        <button class="btn btn-danger btn-sm"  style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                        
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <!-- <p><a href="#ex1" rel="modal:open">Open Modal</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <p><a href="#ex1" rel="modal:open">Open--</a></p> -->

<!-- <div id="ex1" class="">
  <p>Thanks for clicking. That felt good.</p>
  <a href="#" rel="modal:close">Close</a>
</div> -->
<!-- <p><a href="#ex1" rel="modal:open">Open Modal</a></p> -->
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

   <!-- modals -->

    <!-- Remember to include jQuery :) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


    
            
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
@endsection

@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection