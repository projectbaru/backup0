<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    </style>
</head>
<body>
    <div class="container-fluid"><br>
        <button class="btn btn-danger btn-sm no-print" style="border-radius:5px; font-size:10px;" onclick="window.print();">Print</button><br>
        <b style="font-size:14px;">Recruitment Cost Monitoring Template</b><br>
        <small>Cost Center based on interview with HR</small>

        <br><br>

        @foreach ($semesters as $sem)
        <b style="font-size:9px;">{{ $sem["name"] }} _ {{ $br->tahun_budgeting_recruitment }}</b>
        <table class="table-bordered" style="font-size:8px;text-align:center;">
            <thead>
                <tr>
                    <th rowspan="2" style="background-color:rgb(251 228 215)">No</th>
                    <th rowspan="2" style="background-color:rgb(251 228 215)">Cost Center</th>
                    <th rowspan="2" style="background-color:rgb(251 228 215)">Budget Year</th>
                    @foreach ($sem["months"] as $mo)
                        <th colspan="3" style="backgrond-color:rgb(219 225 240)">{{ $mo }}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($sem["months"] as $mo)
						@if (substr($mo, 0, 1) != "Q")
							<th style="backgrond-color:rgb(219 225 240)">Actual</th>
							<th style="backgrond-color:rgb(219 225 240)">% Act (Month)</th>
							<th style="backgrond-color:rgb(219 225 240)">% Act (Kum)</th>
						@else
							<th style="backgrond-color:rgb(219 225 240)">Actual {{ $mo }}</th>
							<th style="backgrond-color:rgb(219 225 240)">Remainig Budget</th>
							<th style="backgrond-color:rgb(219 225 240)">% Act (Kum)</th>
						@endif
					@endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $value)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ ucwords(str_replace('_', ' ', $key)) }}</td>
                    <td>{{ $byear[$key] }}</td>
                    @foreach($sem["months"] as $mo)
						@if (substr($mo, 0, 1) != "Q")
                            <td>{{ $value[$mo]["actual"] }}</td>
                            <td>{{ $value[$mo]["actual_persen_month"] }}</td>
                            <td>{{ $value[$mo]["actual_persen_kum"] }}</td>
						@else
                            <td>{{ $value[""][$mo]["actual_quarter"] }}</td>
                            <td>{{ $value[""][$mo]["remaining_budget"] }}</td>
                            <td>{{ $value[""][$mo]["actual_persen_kum_quarter"] }}</td>
						@endif
					@endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
        @endforeach
<br>
        <b>Note</b>
        <table style="font-size:10px;">
            <tr>
                <td>Position</td>
                <td>:</td>
                <td>Data vacant position yang mempunyai tingkat turnover tinggi</td>
            </tr>
            <tr>
                <td>Total Vacant</td>
                <td>:</td>
                <td>Jumlah karyawan resign tahun 2020</td>
            </tr>
            <tr>
                <td>Target Buffer</td>
                <td>:</td>
                <td>Target pencarian kandidat jika posisi vacant(rasio 1:2)</td>
            </tr>
            <tr>
                <td>Target 2021</td>
                <td>:</td>
                <td>Jumlah target kandidat yang dicari per kuartal (Target buffer dibagi 4 kuartal)</td>
            </tr>
            <tr>
                <td>Deadline</td>
                <td>:</td>
                <td>Pengumpulan kandidat setiap setiap bulan di awal kuartal harus tersedia</td>
            </tr>
        </table>
    </div>
</body>
</html>