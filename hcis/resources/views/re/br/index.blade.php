@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    .accordion-button::after {
      background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='%23333' xmlns='http://www.w3.org/2000/svg'%3e%3cpath fill-rule='evenodd' d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z' clip-rule='evenodd'/%3e%3c/svg%3e");
      transform: scale(.7) !important;
    }
    .accordion-button:not(.collapsed)::after {
      background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='%23333' xmlns='http://www.w3.org/2000/svg'%3e%3cpath fill-rule='evenodd' d='M0 8a1 1 0 0 1 1-1h14a1 1 0 1 1 0 2H1a1 1 0 0 1-1-1z' clip-rule='evenodd'/%3e%3c/svg%3e");
    }
</style>
@endpush


@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Budget Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">List Report Budget Recruitment</b>
    </div>
    <div class="card-body">
        <form action="{{ URL::to('/list_br/hapus_banyak') }}" id="form_delete" method="POST" >
        @csrf
        <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead>
                <tr>
                    <th style="">No</th>
                    <th style="">Kode Report</th>
                    <th style="" scope="col">Tahun</th>
                    <th style="" scope="col">Job Posting</th>
                    <th style="" scope="col">Training Karyawan Baru</th>
                    <th style="" scope="col">Nama Pembuat</th>
                    <th style="" scope="col">Tgl Dibuat</th>
                    <th style="" scope="col">Action</th>
                    <th style="" scope="col">V</th>
                </tr>
            </thead>
            <tbody style="font-size: 11px;">
                @php $b=1; @endphp
                @foreach($br as $data)
                    <tr>
                        <td style="">{{ $b++; }}</td>
                        <td style="">BRB-{{ str_pad($data->id_budgeting_recruitment, 3, '0', STR_PAD_LEFT) }}</td>
                        <td style="">{{ $data->tahun_budgeting_recruitment }}</td>
                        <td style="">{{ $data->job_posting ? $data->job_posting->budgeting_year : '-' }}</td>
                        <td style="">{{ $data->training_karyawan_baru ? $data->training_karyawan_baru->budgeting_year : '-' }}</td>
                        <td style="">{{ $data->nama_user_input ?? "-" }}</td>
                        <td style="">{{date('d-m-Y', strtotime($data->tanggal_input))}}</td>
                        <td>
                            <a href="{{ route('edit_br',$data->id_budgeting_recruitment) }}" class="btn btn-success btn-sm">Ubah</a>
                            <a href="{{ route('detail_br',$data->id_budgeting_recruitment) }}" class="btn btn-secondary btn-sm">Detail</a>
                        </td>
                        <td>
                            <input type="checkbox" name="multiDelete[]" value="{{ $data->id_budgeting_recruitment }}" id="multiDelete" >
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <table>
            <tr>
                <td>
                    <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_br')}}">Tambah</a></td>
                </td>
                <td>
                    <button class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="javascript:;">Hapus</button>
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
                
                                 
                                
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush


@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        // console.log("masuk");
        $("#example").DataTable();
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_filter").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_filter").serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
<script type="text/javascript">
function deleteItem(id) {
    Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                var actionurl = "{{ URL::to('/list_br/hapus/:id')}}"
                actionurl = actionurl.replace(':id', id);
                
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        data: {
				        	"_token": "{{ csrf_token() }}",
				        },
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_br') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
}
</script>
@endsection