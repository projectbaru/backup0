@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitment @endslot
@slot('li_3') Budgeting Recruitment @endslot
@slot('title') Rekruitment @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">List Report Budget Recruitment</b>
    </div>
    <div class="card-body">
		<form action="{{route('simpan_br')}}" method="post" enctype="multipart/form-data">
		<hr>
		{{ csrf_field() }}

		<div class="form-group container-fluid" style="width:100%;">
			<b>Cost Center</b>
			<br>
			<div class="row">
				<div class="col">
					<div class="form-group row" >
						<label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tahun</label>
						<div class="col-sm-2">
							<select name="tahun_budgeting_recruitment" id="tahun_budgeting_recruitment" class="form-control form-control-sm" >
								<option value="" {{ $tahun_budget == null ? "selected" : "" }} disabled>-- Pilih Tahun --</option>
								@php
									$year= date('Y');
									$min = $year - 60;
									$max = $year + 5;
									for($i=$max; $i>=$min; $i--){
									$selected = $tahun_budget == $i ? "selected" : "";
									echo'<option value='.$i.' '.$selected.'>'.$i.'</option>';
									}
								@endphp
							</select>	
							<!-- <input type="text" name="tahun_budgeting_recruitment" class="form-control form-control-sm" required> -->
						</div>
					</div>
				</div>
				<!-- <div class="col">
					<div class="form-group row">
						<div class="col-sm-9">
							<input type="text" name="nama_divisi" class="btn btn-primary btn-sm" required>
						</div>
					</div>
				</div> -->
			</div>
			
			<div class="">
				<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
					<p>Tambah Data CV Pelamar</p>
				</div> -->
				
				<div class="row" style="">
				
					@foreach ($cost_centers as $cc)
					<div class="card accordion open" id="myAccordion{{ $loop->index }}">
						<div class="" style="">
							<div class="card-header" style="">
								<button class="accordion-button collapsed" style="margin: -12px;background-color: rgb(42 117 183); color:white;" data-bs-toggle="collapse" data-bs-target="#collapseThree{{ $loop->index }}"><b>{{  $cc["label"] }}</b></button>
							</div>
							<br>
							<div id="collapseThree{{ $loop->index }}" class="accordion-collapse collapse" data-bs-parent="#myAccordion{{ $loop->index }}" style="width:100%;">
								<div class="card-header">
									<div class="form-group row" style="text-align:left;">
										<label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Budget Year</label>
										<div class="col-sm-2">
											<input type="text" onkeyup="return updateField(this);" name="cost_centers[{{ $cc['value'] }}][budgeting_year]" class="form-control form-control-sm" required>
										</div>
									</div>

									@foreach ($semesters as $s)
									<div class="" style="border:1px solid #c1c1c1; ">
										<b>{{  $s["name"]  }}</b> 

										@foreach ($s["quarters"] as $q)
										<table class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 10px; border:1px solid #d9d9d9;"  >
											<thead>
												<tr>
													@foreach($q["months"] as $qm)
														<th colspan="3" style="text-align:center; ">{{ $qm }}</th>
													@endforeach
												</tr>
												<tr>
													@foreach($q["months"] as $qm)
														@if ($qm != $q["name"])
															<th>Actual</th>
															<th>% Act (Month)</th>
															<th>% Act (Kum)</th>
														@else
															<th>Actual {{$qm}}</th>
															<th>Remainig Budget</th>
															<th>% Act (Kum)</th>
														@endif
													@endforeach
												</tr>
											</thead>
											<tbody>
												<tr>
													@foreach($q["months"] as $qm)
														@if ($qm != $q["name"])
															<td><input type="number" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][actual]" style="text-align:center; font-size:10px;" class="form-control form-control-sm" value="0" required></td>
															<td><input type="text"  readonly id="act_m" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][actual_persen_month]" style="text-align:center; font-size:10px;" class="form-control form-control-sm" required></td>
															<td><input type="text"  readonly id="act_k" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][actual_persen_kum]" style="text-align:center; font-size:10px;" class="form-control form-control-sm" required></td>															
														@else
															<td><input type="number" readonly onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][actual_quarter]" style="text-align:center; font-size:10px;" class="form-control form-control-sm"  value="0" required></td>
															<td><input type="text" readonly id="act_m" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][remaining_budget]" style="text-align:center; font-size:10px;" class="form-control form-control-sm" required></td>
															<td><input type="text" readonly id="act_k" name="cost_centers[{{ $cc['value'] }}][months][{{ $qm }}][actual_persen_kum_quarter]" style="text-align:center; font-size:10px;" class="form-control form-control-sm" required></td>			
														@endif
													@endforeach
												</tr>
											</tbody>
										</table>
										@endforeach
									</div><br>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			
			</div>
		</div>
		<hr>
		<div style="width: 100%;">
			<table>
				<tr>
					<td>
						<button type="submit" id="btn_submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
					</td>
					<td>
						<a href="{{route('list_br')}}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
					</td>
				</tr>
			</table>
		</div>
		<br>
		</form>
	</div>	
</div>	
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<script>
		 $(document).ready(function() {
			 // Select2 Multiple
			 $('.select2-multiple').select2({
				 placeholder: "Lokasi Kerja",
				 allowClear: true
			 });
 
		 });
	 </script>

<script>
	function informasi(that) {
    if (that.value == "Lainnya") {
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
		}
</script>

<script type="text/javascript" language="Javascript">
	
   cost_centers = document.formD.cost_centers.value;
   document.formD.cost_centers.value = cost_centers;
   
   cost_centers = document.formD.cost_centers.value;
   document.formD.txtDisplay_cc.value = cost_centers;
   
   function OnChange(value){
	var cost_centers = parseFloat(document.formD.cost_centers.value);
	
	document.formD.cost_centers.value = cost_centers;
	var cost_centers = cost_centers;

	
	document.formD.selisih.value = selisih;
   }
 </script>
<script>
	$(document).ready(function(){
   $("#file_cv").change(function(){
     fileobj = document.getElementById('file_cv').files[0];
     var fname = fileobj.name;
     var ext = fname.split(".").pop().toLowerCase();
     if(ext == "pdf" || ext == "jpeg" || ext == "png" || ext == "jpg"){
        $("#info_img_file").html(fname);
     }else{
        alert("Hanya untuk file pdf, jpg, jpeg dan png saja..");
        $("#file_cv").val("");
        $("#info_img_file").html("Tidak ada file yag dimasukkan");
        return false;
     }
   });
//    $("#audio_file").change(function(){
//      fileobj = document.getElementById('audio_file').files[0];
//      var fname = fileobj.name;
//      var ext = fname.split(".").pop().toLowerCase();
//      if(ext == "mp3" || ext == "mp4" || ext == "wav"){
//         $("#info_audio_file").html(fname);
//      }else{
//         alert("Accepted file mp3, mp4 and wav only..");
//         $("#aud_file").val("");
//         $("#info_audio_file").html("No file selected");
//         return false;
//      }
//    });
   $("#btn_submit").click(function(){
     var img_file = $('#file_cv').val();
     
     if(img_file =="" || aud_file ==""){
         alert('Please select the file');
        return false;
     }
   });
});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
<script>
	// format rupiah
    
	function formatRupiah(angka, prefix) {
		var num_str = angka.replace(/[^,\d]/g, '').toString(),
			split = num_str.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function updateField(el) {
		let val = el.value;
		el.value = formatRupiah(val);
	}
</script>


@endpush

