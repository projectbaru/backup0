@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('li_3') Perencanaan Tenaga Kerja (PTK) @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="text-align: center;">
     <h5>Olah Data PTK</h5>
    </div>
    <div class="card-body">
        <form action="{{route('ptk_submit')}}" method="POST" >
        @csrf
            <div class="container p-3 mb-1" style="width:70%; border-radius:5px;">

            <div class="row">
    <div class="col" style="font-size: 10px;">
        <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
            <div class="col-sm-9">
                <input type="text" id="kode_grup_lokasi_kerja" readonly name="kode_grup_lokasi_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
            </div>
        </div>
        <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
            <div class="col-sm-9">
                <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                    <option value="" selected disabled>--- Pilih ---</option>
                    @foreach ($grupLokasiKerja as $p)
                    <option data-kode="{{$p->kode_grup_lokasi_kerja}}" value="{{$p->nama_grup_lokasi_kerja}}">{{$p->nama_grup_lokasi_kerja}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
            <div class="col-sm-9">
            <input type="text" id="kode_lokasi_kerja" readonly name="kode_lokasi_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
            </div>
        </div>
    </div>
</div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                            <div class="col-sm-9">
                            <select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                    <option value="" selected disabled>--- Pilih ---</option>
                                    @foreach ($lokasiKerja as $p )
                                    <option data-kode="{{$p->kode_lokasi_kerja}}" value="{{$p->lokasi_kerja}}" >{{$p->lokasi_kerja}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text"  name="nama_lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun Periode</label>
                            <div class="col-sm-5">

                            <select name="tahun" id="tahun" class="form-control form-control-sm" >
                                <option value="" disabled>-- Pilih Tahun --</option>
                                @php
                                    $year= date('Y');
                                    $min = $year - 60;
                                    $max = $year;
                                    for($i=$max; $i>=$min; $i--){
                                    
                                    echo'<option value='.$i.' style="font-size:11px;">'.$i.'</option>';
                                    }
                                @endphp
                            </select>	
                            <!-- <select class="form-select" name="tahun" aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                            </select> -->
                                <!-- <input type="text"  name="periode_ptk" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                            </div>
                        </div>

                    </div>
                </div>

                <div class="container" style="text-align:center;width:20%;">
                    <button type="submit" class="btn btn-primary btn-sm btn-block" style="font-size:13px;color:white;">Olah Data</button>
                </div>
            </div>

        </form>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
  $(document).ready(function() {
    $('#nama_grup_lokasi_kerja').change(function() {
        var selectedValue = $(this).val();

        $('#kode_grup_lokasi_kerja').val($(this).find(':selected').data('kode'));

        var url = `{{ URL::to('/list_ptk/lokasi-kerja/:nama') }}`;
        url = url.replace(':nama', selectedValue);

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                _token: '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);

                var lokasiKerjaSelect = $('#lokasi_kerja');
                lokasiKerjaSelect.empty()

                lokasiKerjaSelect.append($('<option selected disabled></option>').val("").text("--- Pilih ---"));
                $.each(data, function(index, option) {
                    lokasiKerjaSelect.append($(`<option data-kode='${option.kode_lokasi_kerja}'></option>`).val(option.lokasi_kerja).text(option.lokasi_kerja));
                });
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });

    $('#lokasi_kerja').change(function() {
        var selectedValue = $(this).val();

        $('#kode_lokasi_kerja').val($(this).find(':selected').data('kode'));

        var url = `{{ URL::to('/list_ptk/lokasi-kerja-detail/:nama') }}`;
        url = url.replace(':nama', selectedValue);

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                _token: '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                var groupLokasiKerjaSelect = $('#nama_grup_lokasi_kerja');
                groupLokasiKerjaSelect.val(data.nama_grup_lokasi_kerja)
                $('#kode_grup_lokasi_kerja').val(data.kode_grup_lokasi_kerja)
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });
});
</script>
<script>
      console.log("masuk");
            $("#example").DataTable();
    // $(function() {
        //hang on event of form with id=myform
    //     $("#form_delete").submit(function(e) {
    //         e.preventDefault();
    //         var actionurl = e.currentTarget.action;
    //         Swal.fire({
    //             title: '<h4>Anda yakin ingin menghapus?</h4>',
    //             icon: 'warning',
    //             showCancelButton: true,
    //             confirmButtonColor: "#3085d6",
    //             cancelButtonColor: "#d33",
    //             cancelButtonText: "Batal",
    //             confirmButtonText: "<i>Ya, hapus!</i>",
    //             // buttonsStyling: false
    //         }).then((result) => {
    //             if (result.isConfirmed) {
    //                 $.ajax({
    //                     url: actionurl,
    //                     type: 'post',
    //                     dataType: 'JSON',
    //                     cache: false,
    //                     data: $("#form_delete").serialize(),
    //                     success: function(res) {
    //                         if (res.status) {
    //                             Swal.fire({
    //                                 icon: "success",
    //                                 title: "Berhasil!",
    //                                 text: "Data telah terhapus.",
    //                             });
    //                         }
    //                     },
    //                     error: function(e) {
    //                         console.log(e);
    //                         Swal.fire({
    //                             icon: "error",
    //                             title: "Gagal!",
    //                             text: "Terjadi kesalahan saat menghapus data.",
    //                         });
    //                     },
    //                     complete: function(jqXhr, msg) {
    //                         setTimeout(() => {
    //                             location.reload();
    //                         }, 800);
    //                         // console.log(jqXhr, msg);
    //                     }
    //                 });
    //             }
    //         })
    //     });
    // });
</script>
@endsection
