@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') PTK @endslot
@slot('title') Workstrukture @endslot
@endcomponent
                    
<form action="{{route('update_ptk')}}" method="post" enctype="multipart/form-data"> {{ csrf_field() }}
                        <hr>
							{{ csrf_field() }}
                            @foreach($data_ptk as $data)
                        <input type="hidden" name="id_ptk" id="tg_id" value="{{ $data->id_ptk }}" />
                            <div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
									<!-- <td>
									<a href="" class="btn">Hapus</a>
								</td> -->
                                    
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Salin</button>
									</td>

									
									<td>
										<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Proses Data</a>
									</td>
                                    <td>
                                        <a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_ptk', $data->id_ptk)}}" id="btn_delete">Hapus</a>
										
									</td>
                                    <td>
										<a href="{{ route('view_ptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
                            <br>
							<div class="form-group" style="width:95%;">
								<div class="">
									<b>Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Periode PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="periode_ptk" value="{{$data->periode_ptk}}">{{$data->periode_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Awal</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_awal" value="{{$data->tanggal_awal}}">{{$data->tanggal_awal}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_akhir" value="{{$data->tanggal_akhir}}">{{$data->tanggal_akhir}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Awal</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_awal" value="{{$data->ptk_awal}}">{{$data->ptk_awal}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Yang Digantikan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_yang_digantikan" value="{{$data->ptk_yang_digantikan}}">{{$data->ptk_yang_digantikan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Tambahan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_tambahan" value="{{$data->ptk_tambahan}}">{{$data->ptk_tambahan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>PTK Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="ptk_akhir" value="{{$data->ptk_akhir}}">{{$data->ptk_akhir}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_perusahaan" value="{{$data->tksi_perusahaan}}">{{$data->tksi_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Pengganti</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_pengganti" value="{{$data->tksi_pengganti}}">{{$data->tksi_pengganti}}</td>
                                                </tr>
                                                <tr>
                                                    <td>TKSI Akhir</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tksi_final" value="{{$data->tksi_final}}">{{$data->tksi_final}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Selisih</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="selisih" value="{{$data->selisih}}">{{$data->selisih}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>

                                                <tr>
                                                    <td>Kode PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_ptk" value="{{$data->kode_ptk}}">{{$data->kode_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama PTK</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_ptk" value="{{$data->nama_ptk}}">{{$data->nama_ptk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Grup Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_grup_lokasi_kerja" value="{{$data->kode_grup_lokasi_kerja}}">{{$data->kode_grup_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Grup Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_grup_lokasi_kerja" value="{{$data->nama_grup_lokasi_kerja}}">{{$data->nama_grup_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_lokasi_kerja" value="{{$data->kode_lokasi_kerja}}">{{$data->kode_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Lokasi Kerja</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_lokasi_kerja" value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tingkat Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_jabatan" value="{{$data->tingkat_jabatan}}">{{$data->tingkat_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <b>Rekaman Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr>
                                            </table> 
                                        </div>
                                    </div>
                                </div>
							</div>
							<hr>
							
							<br>
                            @endforeach
                    </form>
                    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection