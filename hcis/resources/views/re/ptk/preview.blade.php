

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RC-PTK</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        @media print{
            .print-button{
                display: none;
            }
        }
    </style>
</head>
<body>
    <div class="container-fluid">
    <br>
    <button type="button" class="btn btn-sm btn-secondary print-button" placeholder="print" style="margin-bottom: 20px" onclick="window.print()"><i class="fa-solid fa-print"></i></button>

    
    <table style="width: 100%" class="">
        <thead>
            <tr style="text-align:center;font-size:14px;">
                
                <th style="border: none; text-align:left;"  colspan="19">
                    <p>
                    <h5 style="text-align: center">Perencanaan Tenaga Kerja</h5>
                    </p>
                </th>
                
                
            </tr>
            <tr style="text-align:center;font-size:12px;">
                
                <th style=" text-align:left;"  colspan="6">
                    <p>
                        <span>Periode</span>
                        <span>: </span>
                        <span>
                            @if(!empty($request->deskripsi))
                                {{ $request->deskripsi }}

                            @else
                                @foreach($tahun as $tahuns)
                                    {{ $tahuns->tahun }}
                                @endforeach
                            @endif
                        </span>
                    </p>
                </th>
                
                <th colspan="4">PTK</th>
                <th colspan="4">TKSI</th>
                
            </tr>

            <tr style="text-align:center;font-size:11px;">
                @php
                    $arrHead = [
                        "Departemen", "Tingkat Jabatan", "Posisi", "Group Lokal Kerja", "Lokasi Kerja", "Bulan", "Awal", "Digantikan", "Tambahan", "Akhir" , "Awal", "Pengganti", "Akhir", "selisih"
                    ];
                @endphp
    
                @foreach ($arrHead as $item)
                    <th>{{ $item }}</th>
                @endforeach
            </tr>

        </thead>

        <tbody>

            @foreach ($rc_ptk_detail as $grup)
                @php

                    $detail = DB::table('rc_ptk_detail');
                    $detail->where('kode_ptk', $grup->kode_ptk);
                    $detail->whereRaw("LEFT(periode_ptk, 4) = {$grup->tahun_ptk}");
                    $detail = $detail->get();

                    $totalRowSpan = count($detail) + 1;
                @endphp
                <tr style="text-align:left;font-size:10px;">
                    <td rowspan="{{ $totalRowSpan }}">{{ $grup->nama_ptk }}</td>
                    <td rowspan="{{ $totalRowSpan }}">{{ $grup->tingkat_jabatan }}</td>
                    <td rowspan="{{ $totalRowSpan }}">{{ $grup->nama_posisi }}</td>
                    <td rowspan="{{ $totalRowSpan }}">{{ $grup->nama_grup_lokasi_kerja }}</td>
                    <td rowspan="{{ $totalRowSpan }}">{{ $grup->nama_lokasi_kerja }}</td>
                </tr>

                @foreach ($detail as $details)
                    <tr style="text-align:right;font-size:10px;">
                        <td style="text-align:left;">
                            {{ Carbon\Carbon::createFromFormat('Ym', $details->periode_ptk)->format('F')}}
                            {{ Carbon\Carbon::createFromFormat('Ym', $details->periode_ptk)->format('Y')}}
                        </td>
                        <td>
                            {{ $details->ptk_awal }}
                        </td>
                        <td>
                            {{ $details->ptk_yang_digantikan }}
                        </td>
                        <td>
                            {{ $details->ptk_tambahan }}
                        </td>
                        <td>
                            {{ $details->ptk_akhir }}
                        </td>
                            <td>
                            {{ $details->tksi_perusahaan }}
                        </td>
                        <td>
                            {{ $details->tksi_pengganti }}
                        </td>
                            <td>
                            {{ $details->tksi_final }}
                        </td>
                            <td>
                            {{ $details->selisih }}
                        </td>
                    </tr>
                @endforeach
            @endforeach

        </tbody>
        

        {{-- @foreach ($item->periode_ptk as $ptk)
            @foreach ($row_kode_ptk ?? '' as $item)
                <tr>
                    <td>
                        {{ $item->kode_ptk }}
                    </td>
                </tr>
            @endforeach
        @endforeach --}}

        {{-- @foreach ($row_kode_ptk ?? '' as $item)
            @php
                $totalRowspan = 13;
                echo $totalRowspan;
            @endphp
        
            <tr>
                <td rowspan="{{ $totalRowspan }}">
                    {{$item->kode_ptk}}
                </td>
                <td rowspan="{{ $totalRowspan }}">
                    {{$item->tingkat_jabatan}}
                </td>
                <td rowspan="{{ $totalRowspan }}">
                    {{$item->nama_posisi}}
                </td>
                <td rowspan="{{ $totalRowspan }}">
                    {{$item->nama_grup_lokasi_kerja}}
                </td>
                <td rowspan="{{ $totalRowspan }}">
                    {{$item->nama_lokasi_kerja}}
                </td>
            </tr>
            @foreach ($item->periode_ptk as $ptk)
                <tr>
                    <td>
                        {{ Carbon\Carbon::createFromFormat('Ym', $ptk->periode_ptk)->format('F')}}
                        {{ Carbon\Carbon::createFromFormat('Ym', $ptk->periode_ptk)->format('Y')}}
                    </td>
                     <td>
                        {{ $ptk->ptk_awal }}
                    </td>
                    <td>
                        {{ $ptk->ptk_yang_digantikan }}
                    </td>
                    <td>
                        {{ $ptk->ptk_tambahan }}
                    </td>
                    <td>
                        {{ $ptk->ptk_akhir }}
                    </td>
                      <td>
                        {{ $ptk->tksi_perusahaan }}
                    </td>
                    <td>
                        {{ $ptk->tksi_pengganti }}
                    </td>
                     <td>
                        {{ $ptk->ptk_akhir }}
                    </td>
                     <td>
                        {{ $ptk->selisih }}
                    </td>
                </tr>
            @endforeach
        @endforeach --}}
    </table>
    </div>

    <script>

    </script>
</body>
</html>
