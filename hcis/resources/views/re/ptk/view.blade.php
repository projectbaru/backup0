@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header">
    <a>Perencanaan Tenaga Kerja</a>
    </div>
    <div class="card-body">
   
        <div class="card">
            <div class="card-header" style="background-color: rgb(47 116 181);text-align: center;">
            <b style="color:white;"> Filtering Data</b>
            </div>
            <div class="card-body">
                <form action="{{ route('list_ptk') }}" method="get" id="form-filter-ptk">
                    @csrf
                    <div class="row">
                        <div class="col" style="font-size: 10px;">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK(Dept)</label>
                                <div class="col-sm-5" style="font-size:11px;">
                                    <select name="nama_ptk" id="nama_ptk" class="form-control form-control-sm" style="font-size:11px;">
                                        <option value="" selected disabled>--- Pilih ---</option>
                                        @foreach ($look_nptk['looks'] as $d )
                                            <option value="{{$d->nama_ptk}}" >{{$d->nama_ptk}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Jabatan</label>
                                <div class="col-sm-5" style="font-size:11px;">
                                    <select name="tingkat_jabatan" id="tingkat_jabatan" class="form-control form-control-sm" style="font-size:11px;">
                                        <option value="" selected disabled>--- Pilih ---</option>
                                        @foreach ($look_po['looks'] as $d)
                                            <option value="{{$d->tingkat_jabatan}}">{{$d->tingkat_jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col" style="font-size: 10px;">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Grup Lokasi Kerja</label>
                                <div class="col-sm-5" style="font-size:11px;">
                                    <select name="nama_grup_lokasi_kerja" id="nama_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
                                        <option value="" selected disabled>--- Pilih ---</option>
                                        @foreach ($look_glk['looks'] as $d )
                                        <option value="{{$d->nama_grup_lokasi_kerja}}" >{{$d->nama_grup_lokasi_kerja}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Periode PTK</label>
                                <div class="col-sm-5">
                                    <select name="deskripsi" id="deskripsi" style="font-size:11px;" class="form-control form-control-sm">
                                        @php
                                            $year= date('Y');
                                            $min = $year - 60;
                                            $max = $year;
                                            for($i=$max; $i>=$min; $i--){
                                            echo'<option value='.$i.'>'.$i.'</option>';
                                            }
                                            
                                        @endphp
                                    </select>
                                    <!-- <input type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="container" style="text-align:center;width:20%;">
                    <button form="form-filter-ptk" type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">
                        Filter
                    </button>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <div class="m-1" >
                            <a href="{{route('ubah_tamptk')}}" class="btn btn-primary btn-sm" style="color: white;"><span class="d-block m-t-5"> Buat Tampilan Baru</span></a>
                            <a href="{{route('view_ptk')}}" class="btn btn-success btn-sm" style="color: white;"><span class="d-block m-t-5"> Olah Data</span></a>
                        </div>
                    </div>
                    <div class="col" style="text-align: right;">
                        <div class="">
                            <form action="{{ route('preview_ptk') }}" method="get" id="print-form" target="_blank">
                                @csrf
                                <input type="hidden" name="nama_ptk" class="nama-ptk" value="{{ $request->nama_ptk ?? '' }}">
                                <input type="hidden" name="tingkat_jabatan" class="tingkat-jabatan" value="{{ $request->tingkat_jabatan ?? '' }}">
                                <input type="hidden" name="nama_grup_lokasi_kerja" class="nama-grup-lokasi-kerja" value="{{ $request->nama_grup_lokasi_kerja ?? '' }}">
                                <input type="hidden" name="deskripsi" class="deskripsi" value="{{ $request->deskripsi }}">
                            </form>
                            <button
                                type='submit'
                                form='print-form'
                                class="btn btn-primary btn-sm" 
                            >
                                Print Data (Preview)
                            </button>
                            
                            </div>
                        </div>
                </div>
            </div>
            <div class="card-body">
                <!-- <h5>Per Table</h5> -->
            <form action="{{ URL::to('/list_table/hapus_banyak_table') }}" method="POST" id="form_delete">
                @csrf
                <div class="table-responsive mb-0">
                <table id="example" class="table table-sm table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead style="">
                        <tr class="table-light">
                            <th style="">No</th>
                            <th style="">Kode PTK</th>
                            <th style="">Nama PTK(Departemen)</th>
                            <th style="">Posisi</th>
                            <th style="">Tingkat Jabatan</th>
                            <th style="">Grup Lokasi Kerja</th>
                            <th style="">Lokasi Kerja</th>
                            <th style="">Tenaga Kerja Saat Di Perusahaan</th>
                            <th style="">Periode PTK</th>
                            <th style="">Aksi</th>
                            <th style=""><i class="fa-solid fa-check"></i></th>
                        </tr>
                    </thead>
                    <tbody style="font-size:11px;">
                        @php $b=1; @endphp
                        @foreach ($data as $d)
                        <tr>
                            <td>{{ $b++; }}</td>
                            <td>{{ $d->kode_ptk }}</td>
                            <td>{{ $d->nama_ptk }}</td>
                            <td>{{ $d->nama_posisi }}</td>
                            <td>{{ $d->tingkat_jabatan }}</td>
                            <td>{{ $d->nama_grup_lokasi_kerja }}</td>
                            <td>{{ $d->nama_lokasi_kerja }}</td>
                            <td>{{ $d->tksi_perusahaan }}</td>
                            <td>{{ $d->periode_ptk }}</td>
                            <td>
                                <!-- <a href="{{ URL::to('/list_ptk/detail/'.$d->id_ptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                <a href="{{ URL::to('/list_ptk/edit/'.$d->id_ptk) }}" style="" class="btn btn-secondary btn-sm">Edit</a>
                            </td>
                            <td><input type="checkbox" name="multiDelete[]" id="multiDelete" value="{{ $d->id_ptk }}"></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td>
                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                        </td>
                    </tr>
                </table>
                </div>
            </form>
            </div>
            
        </div>    
       
    </div>
    
</div>
   
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')

<script>
      console.log("masuk");
        $("#example").DataTable();
    // $(function() {
    //     //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    // });
</script>
@endsection
