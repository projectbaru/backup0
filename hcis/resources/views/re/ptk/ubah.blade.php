@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') PTK @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{route('update_ptk')}}" method="post" id="formD" name="formD" enctype="multipart/form-data">
	{{ csrf_field() }}
	<hr>
	<input type="hidden" name="id_ptk" value="{{$data->id_ptk}}">
	<div class="form-group" style="width:95%;">
		<div class="">
			<b>Informasi</b>
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Periode PTK</label>
						<div class="col-sm-9">
							<input type="text" required value="{{$data->periode_ptk}}" readonly name="periode_ptk" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Awal</label>
						<div class="col-sm-9">
							<input type="date" name="tanggal_awal" style="font-size:11px;" value="{{$data->tanggal_awal}}" class="form-control form-control-sm startDates" id="tanggal_awal" placeholder="Tanggal awal">

							<!-- <input type="date" name="tanggal_awal" value="{{$data->tanggal_awal}}" style="font-size:11px;"  class="form-control form-control-sm startDates" id="tanggal_awal" placeholder="Tanggal awal"> -->

							<!-- <input type="date"  value="{{$data->tanggal_awal}}" name="tanggal_awal" style="font-size:11px;"  class="form-control form-control-sm startDates" id="colFormLabelSm" placeholder=""> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Akhir</label>
						<div class="col-sm-9">
							<input type="date" name="tanggal_akhir" style="font-size:11px;" value="{{$data->tanggal_akhir}}" class="form-control form-control-sm endDates" id="tanggal_akhir" placeholder="Tanggal akhir" />

							<!-- <input type="date"  name="tanggal_akhir" value="{{$data->tanggal_akhir}}" style="font-size:11px;"  class="form-control form-control-sm endDates" id="colFormLabelSm" placeholder="Tanggal awal"> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Awal</label>
						<div class="col-sm-9">
							<input type="text" name="ptk_awal" value="{{$data->ptk_awal ?? 0}}" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Yang Digantikan</label>
						<div class="col-sm-9">
							<input type="text" name="ptk_yang_digantikan" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="{{$data->ptk_yang_digantikan ?? 0}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Tambahan</label>
						<div class="col-sm-9">
							<input type="text" name="ptk_tambahan" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="{{$data->ptk_tambahan ?? 0}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="ptk_akhir" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">PTK Akhir</label>
						<div class="col-sm-9">
							<input type="text" name="ptk_akhir" value="{{$data->ptk_akhir}}" id="ptk_akhir" readonly style="font-size:11px;" class="form-control form-control-sm" id="ptk_akhir" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="tksi_perusahaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Perusahaan</label>
						<div class="col-sm-9">
							<input type="text" name="tksi_perusahaan" value="{{$data->tksi_perusahaan ?? 0}}" readonly style="font-size:11px;" class="form-control form-control-sm" id="tksi_perusahaan" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="tksi_pengganti" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Pengganti</label>
						<div class="col-sm-9">
							<input type="text" name="tksi_pengganti" onkeyup="OnChange(this.value)" onKeyPress="return isNumberKey(event)" value="{{$data->tksi_pengganti ?? 0}}" style="font-size:11px;" class="form-control form-control-sm" id="tksi_pengganti" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="tksi_final" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">TKSI Akhir</label>
						<div class="col-sm-9">
							<input type="text" name="tksi_final" value="{{$data->tksi_final}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="tksi_final" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Selisih</label>
						<div class="col-sm-9">
							<input type="text" name="selisih" value="{{$data->selisih}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>

				</div>
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode PTK</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->kode_ptk}}" name="kode_ptk" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->nama_ptk}}" name="nama_ptk" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Grup Lokasi Kerja</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->kode_grup_lokasi_kerja}}" name="kode_grup_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Grup Lokasi Kerja</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->nama_grup_lokasi_kerja}}" name="nama_grup_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->kode_lokasi_kerja}}" name="kode_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->nama_lokasi_kerja}}" name="nama_lokasi_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Jabatan</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->tingkat_jabatan}}" name="tingkat_jabatan" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
						<div class="col-sm-9">
							<input type="text" value="{{$data->nama_posisi}}" name="nama_posisi" style="font-size:11px;" readonly class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
				</div>
			</div>
			<hr>
			<b>Rekaman Informasi</b>
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
						<div class="col-sm-10">
							<input type="date" value="{{$data->tanggal_mulai_efektif}}" required name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDate" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

							<!-- <input type="text" required name="tanggal_mulai_efektif" value="" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
						<div class="col-sm-10">
							<input type="date" value="{{ $data->tanggal_selesai_efektif }}" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDate" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />

							<!-- <input type="text" name="tanggal_selesai_efektif" style="font-size:11px;" value="" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
						<div class="col-sm-10">
							<textarea type="text" rows="6" cols="50" name="keterangan" style="font-size:11px;" value="{{$data->keterangan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div style="width: 100%;">
		<table>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
				</td>
				<td>
					<a href="{{ route('view_ptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
				</td>
				<td>
					<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
				</td>
				<td>
					<a href="" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Proses Data</a>
				</td>
			</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46)
			return true;
		else
			return false;
	}

	$(document).ready(function() {
		$('#npwp').inputmask();
	});
</script>
<script type="text/javascript" language="Javascript">
	function cekVal(val) {
		return !isNaN(val);
	}

	OnChange(null);
	
	


	function OnChange(value) {
		var ptk_awal = parseFloat(document.formD.ptk_awal.value);
		var peng = parseFloat(document.formD.ptk_yang_digantikan.value);
		var tamb = parseFloat(document.formD.ptk_tambahan.value);

		var ptk_akhir = (ptk_awal - peng + tamb);
		document.formD.ptk_akhir.value = !isNaN(ptk_akhir) ? ptk_akhir : "";

		var tksi_perusahaan = parseFloat($("#tksi_perusahaan").val());
		var tksi_pengganti = parseFloat($("#tksi_pengganti").val());

		var tksi_final = !isNaN(tksi_perusahaan) ? tksi_perusahaan + tksi_pengganti : tksi_pengganti;
		document.formD.tksi_final.value = !isNaN(tksi_final) ? tksi_final : "";

		var selisih = !isNaN(ptk_akhir) ? (ptk_akhir - tksi_final) : tksi_final;
		document.formD.selisih.value = !isNaN(selisih) ? selisih : null;
	}

	// function OnChangeTksiPengganti(val) {
	// 	let valThis = parseFloat(val);
	// 	let valtksiP = parseFloat($("#tksi_perusahaan").val());
	// }
</script>
<script>
	$('#nama_lokasi').change(function() {
		document.getElementById("ptk_akhir").innerHTML = $('#ptk_akhir').val($('#'));
	})
</script>
@endpush


@section('add-scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#btn_delete").click(function(e) {
			e.preventDefault();
			var link = $(this);
			var linkHref = link.attr('href');
			console.log(linkHref);

			Swal.fire({
				title: '<h4>Anda yakin ingin menghapus?</h4>',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				cancelButtonText: "Batal",

				confirmButtonText: "<i>Ya, hapus!</i>",
				// buttonsStyling: false
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						url: linkHref,
						type: 'GET',
						dataType: 'JSON',
						cache: false,
						success: function(res) {
							if (res.status) {
								Swal.fire({
									icon: "success",
									title: "Berhasil!",
									text: "Data telah terhapus.",
								}).then((result) => {
									if (result.value) {
										location.href = "{{ URL::to('list_o') }}";
									}
								});
							}
						},
						error: function(e) {
							Swal.fire({
								icon: "error",
								title: "Gagal!",
								text: "Terjadi kesalahan saat menghapus data.",
							});
						},
					});
				}
			})
		});
	});
</script>
@endsection