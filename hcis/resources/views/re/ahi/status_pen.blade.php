@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Analisa Hasil Interview @endslot
@slot('title') Recruitment @endslot
@endcomponent
						<form action="{{route('simpan_an')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							<div class="form-group" style="width:95%;">
								<div class="">
									
                                    <div>
                                        Tambah Analisa Hasil Interview
                                        
									</div>
                                    <hr>
									<div class="row">
										<div class="col" style="font-size: 10px;">
                                            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                                                <thead style="color:black;font-size: 14px;">
                                                    <tr>
														<th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">No</th>
                                                        <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">Nama Penilai</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5; padding-right:-60px;" scope="col">Hirarki TTD</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Tanggal Penilaian</th>
                                                        <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Status</th>
                                                        <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Komentar</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody style="font-size: 12px;color:black;">
                                                @php $b=1; 
												$c=1; 
												
												@endphp
                                                    @foreach($rc_an as $data)
                                                    {{-- {{dd($data)}} --}}
                                                        <tr>
                                                            <td>{{$b++;}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->nama_karyawan}}</td>
                                                            <td style="font-size: 12px;color:black;">Wawancara {{$c++;}}</td>
                                                            <td style="font-size: 12px;color:black;">{{$data->waktu_tanda_tangan}}</td>
                                                            <td style="font-size: 12px;color:black;"></td>
                                                            <td style="font-size: 12px;color:black;">{{$data->komentar}}</td>
                                                           
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
										</div>
									</div>
									
								</div>
							</div>
							
							<div style="width: 100%;">
								<table>
								<tr>
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_an') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
						@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endpush

