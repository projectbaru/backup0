@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Analisa Hasil Interview @endslot
@slot('title') Recruitment @endslot
@endcomponent
<form action="{{route('simpan_an')}}" method="post" enctype="multipart/form-data">
	<hr>
	{{ csrf_field() }}
	<div class="form-group" style="width:95%;">
		<div class="row">
			<div class="col">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label>
					<div class="col-sm-9">
						<input name="kode_analisa_hasil_interview" value="" required id="kode_analisa_hasil_interview" class="form-control form-control-sm" style="font-size:11px;"/>		
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Posisi yang Dilamar</label>
					<div class="col-sm-9">
						<input name="posisi_yang_dilamar" value="" required id="posisi"  class="form-control form-control-sm" style="font-size:11px;"/>		
					</div>
				</div>
            </div>
            <div class="col">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
					<div class="col-sm-9">
						<select name="kode_wawancara_kandidat" required id="kode_wawancara_kandidat" class="form-control form-control-sm" style="font-size:11px;">
							<option value="">SD</option>
                            <option value="">SMP</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal</label>
					<div class="col-sm-9">
						<input type="date" name="tanggal_interview" style="font-size:11px;"   class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
					</div>
				</div>
				<div class="form-group row">
                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Marketing</label>
                    <div class="col-sm-9">
                        <input class="form-check-input" name="status_marketing" type="checkbox" value="" id="flexCheckDefault">
                    </div>
				</div>
                
			</div>
			
		</div>
	</div>
    <div class="form-group" style="width:95%;">
        <b>Penilaian 1</b>
		<div class="row">
            <div  class="border:1px solid black;">
                <div class="col">
                    1. PENDIDIKAN FORMAL : Kesesuaian & tingkat pendidikan format yang menunjang dengan posisi yang dilamar
                    <div class="form-group row">
                        <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault11">
                                <label class="form-check-label" for="flexRadioDefault10">
                                1
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault12">
                                <label class="form-check-label" for="flexRadioDefault10">
                                2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault13">
                                <label class="form-check-label" for="flexRadioDefault10">
                                3
                                </label>
                            </div>
                        </div>
                    </div>
                    2. PENGALAMAN KERJA : Kesesuaian antara pekerjaan sebelumnya dengan pekerjaan yang dilamar
                    <div class="form-group row">
                        <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flex" id="flexDefault1">
                                <label class="form-check-label" for="flexDefault4">
                                1
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flex" id="flexDefault2">
                                <label class="form-check-label" for="flexDefault4">
                                2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flex" id="flexDefault3">
                                <label class="form-check-label" for="flexDefault4">
                                3
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <b>Penilaian Khusus Marketing</b>
		<div class="row">
            <div  class="border:1px solid black;">
                <div class="col">
                    1. KEMAMPUAN PERSUASIF : Antusias dalam bicara & mampu meyakinkan lawan bicara
                    <div class="form-group row">
                        <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="flexRadioDefault1">
                                <label class="form-check-label" for="flexRadioDefault1">
                                1
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                3
                                </label>
                            </div>
                        </div>
                    </div>
                    2. PERCAYA DIRI: Bersikap meyakinkan, berani berpendapat dan mampu mengungkapkan kelebihan diri
                    <div class="form-group row">
                        <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                        <div class="col-sm-9">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="diri" id="flexRadioDefault1">
                                <label class="form-check-label" for="flexRadioDefault1">
                                1
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="diri" id="flexRadioDefault2" checked>
                                <label class="form-check-label" for="flexRadioDefault2">
                                2
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="diri" id="flexRadioDefault2" checked>
                                <label class="form-check-label" for="flexRadioDefault2">
                                3
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <b>Keputusan</b>
                            <div class="form-group row">
                                <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                                <div class="col-sm-9">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kepu" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                        Disarankan
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kepu" id="flexRadioDefault2" checked>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                        Cadangan
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kepu" id="flexRadioDefault2" checked>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                        Tidak disarankan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <b>Komentar</b>
                            <div class="form-group row">
                                <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label> -->
                                <div class="col-sm-9">
                                    <div class="form-check">
                                        <textarea class="form-check-input" type="radio" name="kepu" id="flexRadioDefault1"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<hr>
	<div style="width: 100%;">
		<table>
		<tr>
			<td>
				<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
				<a href="{{ route('list_an') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
		</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

<script>			
	$('#kode_cv_pelamar').change(function(){
		$('#pendidikan').val($('#kode_cv_pelamar option:selected').data('name'));
	})
	$('#pendidikan').change(function(){
		$('#kode_cv_pelamar').val($('#pendidikan option:selected').data('kode'));
	})
</script>
@include('js.analisa.tambah')
@include('js.analisa.checked')
@endpush

