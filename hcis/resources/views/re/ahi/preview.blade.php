<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/fontawesome-all.min.css')}}">

</head>
<body >
<div class="pcoded-wrapper container" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<!-- <div class="page-header-title" >
										<h5 class="m-b-10">Detail</h5>
									</div> -->
									<!-- <ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Detail Analisa Pelamar</a></li>
									</ul> -->
								</div>
							</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col" style="text-align:left;">
                            <b>Preview Analisa Hasil Interview</b>
                        </div>
                        <div class="col" style="text-align:right;">
                            
                        </div>
                    </div>
                    @foreach($rc_an as $data)
                    <div style="width:100%;" class="container" style="border:2px solid black;">
                        <br>
                       
                        <table>
                            <tr>
                                <!-- <td>
                                    <input type="text">
                                </td> -->
                                <td>
                                    <a href="" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Ubah</a>
                                </td>
                                <td>
                                    <a href="{{route('list_an')}}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Kembali</a>
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-sm no-print " style="border-radius:5px; font-size:10px;" onclick="window.print();">Print</button>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <div style="border:2px solid black; padding:50px;">
                            <div class="row">
                                <div class="col">
                                    <img src="" alt="">
                                </div>
                                <div class="col">
                                    <div style="border:2px solid black;text-align:center;font-size:15px;">
                                        <b>FORMULIR PENILAIAN HASIL INTERVIEW</b>
                                    </div>
                                </div>
                                <div class="col" style="text-align:right;">
                                    F-SRU
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <table>
                                        <tr>
                                            <td>Nama Pelamar</td>
                                            <td>:</td>
                                            <td>
                                                {{$data->nama_pelamar}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Posisi yang dilamar</td>
                                            <td>:</td>
                                            <td>{{$data->posisi_yang_dilamar}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col">
                                    <table>
                                        <tr>
                                            <td>Pendidikan</td>
                                            <td>:</td>
                                            <td>{{$data->pendidikan}}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal</td>
                                            <td>:</td>
                                            <td>{{$data->tanggal_input}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div>
                                    <table class="table-responsive-xl table-striped table-bordered table-xl mt-2">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Dimensi & Uraian</th>
                                                <th colspan="3">Wawancara-1</th>
                                                <th colspan="3">Wawancara-2</th>
                                                <th colspan="3">Wawancara-3</th>
                                            </tr>
                                            <tr>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>PENDIDIKAN FORMAL: Kesesuaian & tingkat pendidikan format yang menunjang dengan posisi yang dilamar</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td >PENGALAMAN KERJA: Kesesuaian antara pekerjaan sebelumnya dengan pekerjaan yang dilamar</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td >PENGETAHUAN TEKNIS: Pengetahuan praktis/teoritis yang dikuasai sesuai dengan posisi yang dilamar</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>KETERAMPILAN TEKNIS: Kemampuan menguasai permasalahan yang mungkin terjadi pada posisi yang dilamar</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>MOTIVASI: Menunjukkan besarnya semangat serta minat terhadap kondisi pekerjaan yang dilamar</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>KERJASAMA: Kemampuan beradaptasi & bekerja sama dengan baik</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>ETIKA KERJA: Menunjukkan sikap sopan santun dalam bertutur kata, tindakan & sikap tubuh</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>KOMUNIKASI: Kemampuan mengungkapkan ide, gagasan serta pendapat secara kreatif, sistematis & jelas</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>PENAMPILAN DIRI: Kebersihan diri & cara berpakaian, kerapian serta keadaaan fisik secara umum</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>KEMAMPUAN MEMIMPIN: Memiliki pengalaman & kapasitas untuk mengatur, mengarahkan, memberi instruksi & matang</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>11</td>
                                                <td>SIKAP PRIBADI: Menghormati atasan & rekan kerja. Mampu menjadi individu yang menyenangkan</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>12</td>
                                                <td>PEMECAHAN MASALAH & KREATIFITAS: Kemampuan menyelesaikan masalah dengan idenya sendiri</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td colspan="9"><i>1: Kurang, 2: Cukup, 3: Baik</i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <b style="color:#007bff;">KHUSUS UNTUK MARKETING</b>
                                    <table class="table-responsive-xl table-striped table-bordered table-xl mt-2">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Dimensi & Uraian</th>
                                                <th colspan="3">Wawancara-1</th>
                                                <th colspan="3">Wawancara-2</th>
                                                <th colspan="3">Wawancara-3</th>
                                            </tr>
                                            <tr>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>KEMAMPUAN PERSUASIF: Antusias dalam bicara & mampu meyakinkan lawan bicara serta memahami lawan bicara</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>PERCAYA DIRI: Bersikap meyakinkan, berani berpendapat & mampu mengungkapkan kelebihan diri</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>INISIATIF: Proaktif & sering melakukan sesuatu sebelum diminta</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>KEPEKAAN DAGANG: Mampu mencari dan mengenali serta memanfaatkan peluang menjual produk</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>KEINGINAN BERPRESTASI: Mandiri dalam bekerja dan memiliki target penjualan yang tinggi diatas standar yang ada</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>KEULETAN: Tidak mudah menyerah & memiliki semangat yang tinggi meskipun menemui kendala</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>SENSITIF & LOYAL: Paham & mampu penuhi kebutuhan customer serta loyal terhadap apa yang dikerjakan</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>LOGIKA BERPIKIR: Mampu menganalisa dan mencari solusi atas masalah penjualan</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>THINK OUT OF BOX: Memiliki rencana, ide-ide cemerlang untuk meningkatkan & mencapai target penjualan</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                                    <br>
                                    @foreach($rc_an_d as $data)
                                    <div class="row">
                                        <div class="col">
                                            <b>Wawancara-1</b>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="{{$data->keputusan}}" id="flexCheckDefault" {{ $data->keputusan == 'disarankan' ? 'checked' : NULL }}>
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Disarankan
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="{{$data->keputusan}}" id="flexCheckChecked" {{ $data->keputusan == 'cadangan' ? 'checked' : NULL }}>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Cadangan
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="{{$data->keputusan}}" id="flexCheckChecked" {{ $data->keputusan == 'tidak disarankan' ? 'checked' : NULL }}>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Tidak disarankan
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-floating">
                                                <label for="floatingTextarea2">Komentar :</label>
                                                <textarea class="form-control" placeholder="....." id="floatingTextarea2" value="{{$data->komentar}}" style="height: 300px">{{$data->komentar}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-floating">
                                                <label for="floatingTextarea2">Tandatangan & Nama</label>
                                                <br><br><br><br><br><br><br><br><br><br>
                                                <b>{{$data->nama_karyawan}}</b>
                                                <hr>
                                                <!-- <textarea class="form-control" placeholder="" value="{{$data->nama_karyawan}}" id="floatingTextarea2" style="height: 300px">{{$data->nama_karyawan}}</textarea> -->
                                            </div>
                                        </div>
                                    </div><br>
                                    @endforeach
                                    <br>  
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
			</div>
		</div>
	</div>
</div>
</body>
</html>


<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>