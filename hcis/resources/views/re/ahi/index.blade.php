@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Analisa Hasil Interview @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="background-color: rgb(42 117 183);color:white; text-align:center;">
        <b style="font-size: 20px;"> Analisa Hasil Interview</b>
    </div>
    <div class="card-body">
    <form action="{{ URL::to('/list_an/hapus_banyak') }}" method="post" enctype="multipart/form-data" id="form_delete">
        <hr>
        {{ csrf_field() }}
        <div class="form-group" style="width:95%;">
            <div class="">
                <div>
                
                    <div class="row">
                        <div class="col" style="font-size: 10px;">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun Dibuat</label>
                                <div class="col-sm-9">
                                    <select name="tahun_dibuat" style="font-size:11px;" id="tahun_dibuat" class="form-control form-control-sm">
                                        @php
                                            $year= date('Y');
                                            $min = $year - 60;
                                            $max = $year;
                                            for($i=$max; $i>=$min; $i--){
                                            echo'<option value='.$i.' style="font-size:11px;">'.$i.'</option>';
                                            }

                                        @endphp
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Cari Data</label>
                                <div class="col-sm-9">
                                    <input type="text" name="cari_data" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
                                </div>
                            </div> -->
                        </div>
                        <div class="col">
                            <a type="submit" class="btn btn-success btn-sm" style="font-size:11px;" href="{{route('tambah_an')}}" id="colFormLabelSm" placeholder="">Tambah Analisa Hasil Interview</a>
                        </div> 
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col" style="font-size: 10px;">
                        <table id="example" class="table table-bordered dt-responsive nowrap table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;text-align:center;">
                            <thead style="color:black;font-size: 14px;">
                                <tr>
                                    <th style="">No</th>
                                    <th style=" " scope="col">Kode Analisa Hasil Interview</th>
                                    <th style="" scope="col">Nama Pelamar</th>
                                    <th style="" scope="col">Posisi yang Dilamar</th>
                                    <th style="" scope="col">Pendidikan</th>
                                    <th style="" scope="col">Tanggal Interview</th>
                                    <th style="" scope="col">Created by</th>
                                    <th style="" scope="col">Created at</th>
                                    <th style="" scope="col">Status</th>
                                    <th style="" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;color:black;">
                                @php $b=1; @endphp
                                @foreach($rc_an as $data)
                                {{-- {{dd($data)}} --}}
                                    <tr>
                                        <td>{{$b++;}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->kode_analisa_hasil_interview}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->nama_pelamar}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->posisi_yang_dilamar}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->pendidikan}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->tanggal_interview}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->nama_user_input}}</td>
                                        <td style="font-size: 12px;color:black;">{{$data->tanggal_input}}</td>
                                        
                                        @if($data->status_form === 'new')
                                        <td style="font-size: 12px;color:black;background-color: rgb(255 191 0);">{{$data->status_form}}</td>
                                        <td style="">
                                            <a href="{{ URL::to('/list_an/detail/'.$data->id_analisa_hasil_interview) }}" class="btn btn-secondary btn-sm">Detail</a>
                                            <a href="{{ route('edit_an',$data->id_analisa_hasil_interview) }}" class="btn btn-success btn-sm">Edit</a>
                                            <a href="{{ URL::to('/list_an/preview/'.$data->id_analisa_hasil_interview) }}" class="btn btn-primary btn-sm">Preview</a>
                                            <a href="javascript:void(0)" onclick="deleteItem('{{ $data->id_analisa_hasil_interview }}')" class="btn btn-danger btn-sm">Hapus</a>
                                        </td>
                                        @elseif($data->status_form === 'waiting')
                                        <td style="font-size: 12px;color:black;background-color:rgb(154 193 231);">{{$data->status_form}}</td>
                                        <td>
                                            <a href="{{ URL::to('/list_an/detail/'.$data->id_analisa_hasil_interview) }}" class="btn btn-secondary btn-sm">Detail</a>
                                        </td>
                                        @elseif($data->status_form === 'release')
                                        <td style="font-size: 12px;color:black;background-color:rgb(146 209 79);">{{$data->status_form}}</td>
                                        <td>
                                            <a href="{{ URL::to('/list_an/preview/'.$data->id_analisa_hasil_interview) }}" class="btn btn-warning btn-sm">Print</a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <br>
    </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
@endpush
@section('add-scripts')
<script type="text/javascript">
$('#example').DataTable({    
});
function deleteItem(id) {
    Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                var actionurl = "{{ URL::to('/list_an/hapus/:id')}}"
                actionurl = actionurl.replace(':id', id);
                
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        data: {
				        	"_token": "{{ csrf_token() }}",
				        },
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_an') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
}
</script>


<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>



<script>
    $(function() {
        console.log("masuk");
            $("#example").DataTable();
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection
