@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Analisa Hasil Interview @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 20px;">Ubah Analisa Hasil Interview</b>
    </div>
    <div class="card-body">
		<div class="card">
			<div class="card-header" style="background-color: rgb(42 117 183);color:white; text-align:center;">
				<b style="font-size: 17px;">Data Analisa Hasil Interview</b>
			</div>
			<div class="card-body">
				<form action="{{route('update_an')}}" method="post" enctype="multipart/form-data">
					<hr>
					{{ csrf_field() }}
					@foreach($rc_an as $data)
					<div class="form-group" style="width:95%;">
						<div class="row">
							<div class="col">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Analisa Hasil Interview</label>
									<div class="col-sm-9">
										<input name="kode_analisa_hasil_interview" value="{{$data->kode_analisa_hasil_interview}}" required id="kode_analisa_hasil_interview" readonly class="form-control form-control-sm" style="font-size:11px;"/>		
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Data Wawancara Kandidat</label>
									<div class="col-sm-9">
										<select name="kode_data_wawancara_kandidat" required id="kode_data_wawancara_kandidat" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($pr['looks'] as $pr)	
											<option value="{{$pr->kode_process_recruitment}}">{{$pr->kode_process_recruitment}}</option>
										@endforeach
										</select><a style="color:red;">*wajib isi</a>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pelamar</label>
									<div class="col-sm-9">
										<input type="text" name="nama_pelamar" readonly  style="font-size:11px;" value="{{$data->nama_pelamar}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/><a style="color:red;">*wajib isi</a>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Posisi yang Dilamar</label>
									<div class="col-sm-9">
										<input type="text" name="posisi_yang_dilamar" readonly style="font-size:11px;" value="{{$data->posisi_yang_dilamar}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Kandidat</label>
									<div class="col-sm-9">
										<select name="kode_cv_pelamar" id="kode_cv_pelamar" readonly class="form-control form-control-sm" style="font-size:11px;">
											@foreach($rc_data['looks'] as $d)
												<option value="{{$d->kode_cv_pelamar}}" data-name="{{$d->pendidikan_tertinggi}}" >{{$d->kode_cv_pelamar}}</option>
											@endforeach
										</select><a style="color:red;">*wajib isi</a>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
									<div class="col-sm-9">
										<select name="pendidikan" id="pendidikan" readonly  class="form-control form-control-sm" style="font-size:11px;">
											@foreach($rc_data['looks'] as $d)
												<option value="{{$d->pendidikan_tertinggi}}" data-kode="{{$d->kode_cv_pelamar}}">{{$d->pendidikan_tertinggi}}</option>
											@endforeach
										</select>	
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Interview</label>
									<div class="col-sm-9">
										<input type="date" name="tanggal_interview"  style="font-size:11px;" value="{{$data->tanggal_interview}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Marketing</label>
									<div class="col-sm-2">						
										<input  class="form-check-input" type="checkbox"  value="{{$data->status_marketing}}" name="status_marketing" id="flexCheckDefault" onclick="return false" {{ $data->status_marketing == 'aktif' ? 'checked' : NULL }}>
										<!-- <input type="checkbox" name="status_marketing" style="font-size:11px;" class="form-check-input" id="colFormLabelSm" placeholder=""/> -->
									</div>
								</div>
								<!-- <div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Marketing</label>
									<div class="col-sm-2">
										<input type="checkbox" onclick="return false" name="status_marketing" readonly id="isCheckPergantian" style="font-size:5px;" value="{{$data->status_marketing}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""  {{ $data->status_marketing == 'aktif' ? 'checked' : NULL }}/>
									</div>
								</div> -->
							</div>
						</div>
						<b>Dinilai Oleh</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row" id="qualificationInputContainer">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pewawancara</label>
									<div class="col-sm-8">
										<input type="text" name="" value=""  required style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/><a style="color:red;">*wajib isi</a>
									</div>
									<div class="col-sm-1">
										<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput" >
											<i class="fa-solid fa-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div style="width: 100%;">
						<table>
						<tr>
							<td>
								<button type="submit" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
							</td>
							<td>
								<a class="btn btn-danger btn-sm" href="{{route('hapus_an',$data->id_analisa_hasil_interview)}}" class="btn" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete">Hapus</a>
								<!-- <a href="{{ route('hapus_an',$data->id_analisa_hasil_interview) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a> -->
							</td>
							<td>
								<a href="{{ route('list_an') }}" class="btn btn-secondary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
							</td>
						</tr>
						</table>
					</div>
					@endforeach
					<br>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>


<script>			
	$('#kode_cv_pelamar').change(function(){
		$('#pendidikan').val($('#kode_cv_pelamar option:selected').data('name'));
	})
	$('#pendidikan').change(function(){
		$('#kode_cv_pelamar').val($('#pendidikan option:selected').data('kode'));
	})
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_an') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.analisa.tambah')
@include('js.analisa.checked')
@endpush

