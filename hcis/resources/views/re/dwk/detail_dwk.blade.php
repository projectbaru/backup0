@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot

@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">Ubah Data Wawancara Kandidat</b>
    </div>
    <div class="card-body">
		<div class="card">
			<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
				
			</div>
			<div class="card-body">
				<div class="form-group">	
					<form action="{{route('update_dwk')}}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
							<input type="hidden" name="id_data_wawancara_kandidat" readonly id="id_data_wawancara_kandidat" value="{{  $data->id_data_wawancara_kandidat }}"/>
							<input type="hidden" name="pdwk_id" readonly id="pdwk_id" value="{{  $data->id_penyiapan_dwk }}"/>
							<div class="form-group" style="width:95%;" >
								<div class="row">
									<div class="col" style="font-size: 10px;">
									<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Data Wawancara Kandidat</label>
											<div class="col-sm-9">
												<!-- <select disabled name="kode_process_recruitment_kandidat" readonly id="kode_process_recruitment_kandidat" style="font-size:11px;" class="form-control form-control-sm">
													@foreach($kprk['looks'] as $p)
														<option value="{{$p->kode_process_recruitment}}" data-kode_cv_pelamar="{{$p->kode_cv_pelamar}}" data-nama_kandidat="{{$p->nama_kandidat}}" {{ $p->kode_process_recruitment == $data->kode_process_recruitment_kandidat ? 'selected' : NULL }} >{{$p->kode_process_recruitment}}</option>
													@endforeach
												</select> -->
												<input type="text" required readonly name="kode_data_wawancara_kandidat" value="{{$data->kode_data_wawancara_kandidat}}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode PR Kandidat</label>
											<div class="col-sm-9">
												<select disabled name="kode_process_recruitment_kandidat" readonly id="kode_process_recruitment_kandidat" style="font-size:11px;" class="form-control form-control-sm">
													@foreach($kprk['looks'] as $p)
														<option value="{{$p->kode_process_recruitment}}" data-kode_cv_pelamar="{{$p->kode_cv_pelamar}}" data-nama_kandidat="{{$p->nama_kandidat}}" {{ $p->kode_process_recruitment == $data->kode_process_recruitment_kandidat ? 'selected' : NULL }} >{{$p->kode_process_recruitment}}</option>
													@endforeach
												</select>
												<!-- <input type="text" required name="kode_process_recruitment_kandidat" value="{{$data->kode_process_recruitment_kandidat}}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder="">  -->
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Kandidat</label>
											<div class="col-sm-9">
												<input type="text" required name="kode_cv_pelamar" readonly value="{{$data->kode_cv_pelamar}}" style="font-size:11px;" class="form-control form-control-sm" readonly id="kode_cv_pelamar" placeholder=""> 
											</div>
										</div>
										
									</div>
									<div class="col">
									<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kandidat</label>
											<div class="col-sm-9">
												<input type="text" required name="nama_kandidat" readonly style="font-size:11px;" value="{{$data->nama_kandidat}}" class="form-control form-control-sm" readonly id="nama_kandidat" placeholder=""> 
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Hasil Konfirmasi</label>
											<div class="col-sm-9">
												
												<div class="form-check">
													<input class="form-check-input" type="radio" disabled name="hasil_konfirmasi" value="hadir via" onchange="informasi(this);"  readonly id="exampleRadios1" {{ $data->hasil_konfirmasi == 'hadir via' ? 'checked' : NULL }}>
													<label class="form-check-label" for="exampleRadios1" checked>
														<input type="hidden" required readonly id="via_isi" name="via" style="font-size:11px;" value="{{$data->via}}"  class="form-control form-control-sm" placeholder="">
														<input type="text" name="via" value="{{$data->via}}" class="form-control form-control-sm" style="font-size:11px;"  readonly id="via_edit" placeholder="" value="{{ $data->hasil_konfirmasi == 'hadir via' ? $data->via : ''}}"  class="form-control form-control-sm" placeholder="" {{ $data->hasil_konfirmasi == 'tidak hadir' ? 'readonly' : NULL }}>
													</label>
												</div><br>
												<div class="form-check">
													<input class="form-check-input" disabled type="radio" name="hasil_konfirmasi" onchange="informasi(this);" value="tidak hadir" readonly id="exampleRadios2" {{ $data->hasil_konfirmasi == 'tidak hadir' ? 'checked' : NULL }}>
													<label class="form-check-label" for="exampleRadios2">
														Tidak Hadir
													</label>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat</label>
											<div class="col-sm-9">
												<input type="text" required name="tempat" value="{{$data->tempat}}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>	
								</div>
								<hr>
								<b>Data Interview</b>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jam</label>
											<div class="col-sm-9">
												<input type="time" min="00:00" max="24:00" required name="jam_wawancara" value="{{$data->jam_wawancara}}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
											<div class="col-sm-9">
												<input type="hidden" required name="urutan_interview" readonly value="Interview 1" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
											<div class="col-sm-9">
												<input type="hidden" required name="urutan_interview" readonly value="Interview 1" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
											<div class="col-sm-9">
												<input type="text" required name="urutan_interview" readonly value="Interview 1" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal</label>
											<div class="col-sm-9">
												
												<input type="date" required name="tanggal_interview_1" value="{{ $jadwal1->tanggal_interview }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Oleh</label>
											<div class="col-sm-9">
												<select disabled name="nama_pewawancara_1" required class="form-control form-control-sm" style="font-size:11px;">
													@foreach ($data_kar as $nk )
													<option value="{{$nk->Nama_Karyawan}}" style="font-size:11px;" {{ $nk->Nama_Karyawan==$jadwal1->nama_pewawancara  ? 'selected' : NULL }}>{{$nk->Nama_Karyawan}}</option>
													@endforeach
												</select><a style="color:red;">*wajib isi</a>
												
												<!-- <input type="text" required name="nama_pewawancara_1" value="{{ $jadwal1->nama_pewawancara }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder="">  -->
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
											<div class="col-sm-9">
												<input type="text" required name="urutan_interview" style="font-size:11px;" readonly value="Interview 2" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal</label>
											<div class="col-sm-9">
												<input type="date" name="tanggal_interview_2" value="{{ $jadwal2->tanggal_interview }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Oleh</label>
											<div class="col-sm-9">
												<select disabled name="nama_pewawancara_2" required class="form-control form-control-sm" style="font-size:11px;">
													@foreach ($data_kar as $nk )
													<option value="{{$nk->Nama_Karyawan}}" style="font-size:11px;" {{ $nk->Nama_Karyawan==$jadwal2->nama_pewawancara  ? 'selected' : NULL }}>{{$nk->Nama_Karyawan}}</option>
													@endforeach
												</select><a style="color:red;">*wajib isi</a>
												<!-- <input type="text"  name="nama_pewawancara_2" value="{{ $jadwal2->nama_pewawancara }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder="">  -->
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
											<div class="col-sm-9">
												<input type="text" required name="urutan_interview3" readonly value="Interview 3" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal</label>
											<div class="col-sm-9">
												<input type="date" name="tanggal_interview_3" value="{{ $jadwal3->tanggal_interview }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
											</div>
										</div>
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Oleh</label>
											<div class="col-sm-9">
												<select disabled name="nama_pewawancara_3" required class="form-control form-control-sm" style="font-size:11px;">
													@foreach ($data_kar as $nk )
													<option value="{{$nk->Nama_Karyawan}}" style="font-size:11px;" {{ $nk->Nama_Karyawan==$jadwal3->nama_pewawancara  ? 'selected' : NULL }}>{{$nk->Nama_Karyawan}}</option>
													@endforeach
												</select><a style="color:red;">*wajib isi</a>
												<!-- <input type="text" name="nama_pewawancara_3" value="{{ $jadwal3->nama_pewawancara }}" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder="">  -->
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<b>Hasil Interview</b>
							<div class="row">
								<div class="col">
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Atasan</label>
										<div class="col-sm-9">
											<select disabled name="nama_atasan" readonly id="nama_atasan" class="form-control form-control-sm">
												{{-- @foreach($atasan as $data)
													<option value="nama_atasan">{{$data->nama_atasan}}</option>	
												@endforeach --}}
												<option value="nama_">abc</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">DOJ</label>
										<div class="col-sm-9">
											<input type="date" value="{{$data->date_on_join}}" required name="date_on_join" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder=""> 
										</div>
									</div>
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status</label>
										<div class="col-sm-9">
											<select disabled name="status_hasil_interview" readonly id="status_hasil_interview" class="form-control form-control-sm">
												<option value="On Progress" {{ $data->status_hasil_interview == 'On Progress' ? 'selected' : NULL }}>On Progress</option>
												<option value="Done" {{ $data->status_hasil_interview == 'Done' ? 'selected' : NULL }}>Done</option>
												<option value="Cancel" {{ $data->status_hasil_interview == 'Cancel' ? 'selected' : NULL }}>Cancel</option>
												<option value="Disarankan" {{ $data->status_hasil_interview == 'Disarankan' ? 'selected' : NULL }}>Disarankan</option>
												<option value="Dicadangkan" {{ $data->status_hasil_interview == 'Dicadangkan' ? 'selected' : NULL }}>Dicadangkan</option>
												<option value="Tidak Disarankan" {{ $data->status_hasil_interview == 'Tidak Disarankan' ? 'selected' : NULL }}>Tidak Disarankan</option>
												<option value="Magang" {{ $data->status_hasil_interview == 'Magang' ? 'selected' : NULL }}>Magang</option>
												<option value="Follow Up" {{ $data->status_hasil_interview == 'Follow Up' ? 'selected' : NULL }}>Follow Up</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col">
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
										<div class="col-sm-9">
											<select disabled name="jabatan" readonly id="jabatan" class="form-control form-control-sm">
												@foreach($jabatan as $jab)
													<option value="{{$jab->nama_jabatan}}" {{ $data->jabatan==$jab->nama_jabatan ? 'selected' : NULL }}>{{$jab->nama_jabatan}}</option>	
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi</label>
										<div class="col-sm-9">
											<select disabled name="lokasi_kerja" readonly id="lokasi_kerja" class="form-control form-control-sm">
												@foreach($loker as $lok)
													<option value="{{$lok->nama_lokasi_kerja}}" {{ $data->lokasi_kerja==$lok->nama_lokasi_kerja ? 'selected' : NULL }}>{{$lok->nama_lokasi_kerja}}</option>	
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
										<div class="col-sm-9">
											<textarea type="text" required name="keterangan" style="font-size:11px;" class="form-control form-control-sm" readonly id="colFormLabelSm" placeholder="" rows="4" cols="50">
												{{$data->keterangan}}
											</textarea> 
										</div>
									</div>
								</div>
							</div>
							<hr>
						<div style="width: 100%;">
							<table>
								<tr>
									<td>
										<a href="{{ URL::to('/list_dwk/edit_dwk/'.$data->id_data_wawancara_kandidat) }}" style="border:none;border-radius:5px;font-size:11px;" class="btn btn-primary btn-sm">Ubah</a>
                                        
                                        <!-- <button type="submit" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button> -->
                                    </td>
                                    <!-- <td>
										<a href="javascript:void(0)" onclick="deleteItem('{{ $data->id_data_wawancara_kandidat }}')" style="font-size:8px;" class="btn btn-danger btn-sm">Hapus</a>
										
                                    </td> -->
									<td>
										<a href="{{ route('list_dwk', $data->id_penyiapan_dwk) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
							</table>
						</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script>
	$('#exampleRadios1').click(function(){
		$("#select_area_kelas_cabang").attr("disabled", false);
		$("#input_area_kelas_cabang").attr("disabled", true);
		$("#select_area_kelas_cabang").attr("required", true);
		$("#input_area_kelas_cabang").attr("required", false);
		$("#input_area_kelas_cabang").val("");
	})
	$('#exampleRadios2').click(function(){
		$("#select_area_kelas_cabang").attr("disabled", true);
		$("#input_area_kelas_cabang").attr("disabled", false);
		$("#select_area_kelas_cabang").attr("required", false);
		$("#input_area_kelas_cabang").attr("required", true);
		$("#select_area_kelas_cabang").val("");
	})
</script>
<script>
    
// function deleteItem(id) {

//     Swal.fire({
//                 title: '<h4>Anda yakin ingin menghapus?</h4>',
//                 icon: 'warning',
//                 showCancelButton: true,
//                 confirmButtonColor: "#3085d6",
//                 cancelButtonColor: "#d33",
//                 cancelButtonText: "Batal",      
//                 confirmButtonText: "<i>Ya, hapus!</i>",
//                 // buttonsStyling: false
//             }).then((result) => {
//                 var actionurl = "{{ URL::to('/list_dwk/hapus_dwk/:id')}}"
//                 actionurl = actionurl.replace(':id', id);
                
//                 if (result.isConfirmed) {
//                     $.ajax({
//                         url: actionurl,
//                         type: 'post',
//                         dataType: 'JSON',
//                         cache: false,
//                         data: {
// 				        	"_token": "{{ csrf_token() }}",
// 				        },
//                         success: function(res) {
//                             if (res.status) {                                
//                                 Swal.fire({
//                                     icon: "success",
//                                     title: "Berhasil!",
//                                     text: "Data berhasil dihapus.",
//                                 });
//                             }
//                         },
//                         error: function(e) {
//                             console.log(e);
//                             Swal.fire({
//                                 icon: "error",
//                                 title: "Gagal!",
//                                 text: "Terjadi kesalahan saat menghapus data.",
//                             });
//                         },
//                         complete: function(jqXhr, msg) {
//                             setTimeout(() => {
//                                 location.reload();
//                             }, 800);
//                             // console.log(jqXhr, msg);
//                         }
//                     });
//                 }
//             })
// }

function deleteItem(id) {
  
    var pdwkId = $("#pdwk_id").val(); // Get the value from the hidden input
    
    Swal.fire({
        title: '<h4>Anda yakin ingin menghapus?</h4>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",      
        confirmButtonText: "<i>Ya, hapus!</i>",
    }).then((result) => {
		var actionurl = "{{ URL::to('/list_dwk/hapus_dwk/:id')}}"
        
        
        if (result.isConfirmed) {
            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'JSON',
                cache: false,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    if (res.status) {                                
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil!",
                            text: "Data berhasil dihapus.",
                        });

                        // Redirect to the desired URL after successful deletion
                        setTimeout(() => {
                            window.location.href = "{{ URL::to('/list_dwk/') }}" + pdwkId;
                        }, 800);
                    }
                },
                error: function(e) {
                    console.log(e);
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: "Terjadi kesalahan saat menghapus data.",
                    });
                },
                complete: function(jqXhr, msg) {
                    // No need for a location.reload() here
                }
            });
        }
    });
}


</script>


<script>
$('#kode_process_recruitment_kandidat').change(function(){
	$('#kode_cv_pelamar').val($('#kode_process_recruitment_kandidat option:selected').data('kode_cv_pelamar'));
})
</script>
<script>
$('#kode_process_recruitment_kandidat').change(function(){
	$('#nama_kandidat').val($('#kode_process_recruitment_kandidat option:selected').data('nama_kandidat'));
})
</script>
<script>
function informasi(that) {
    var via_h = $("#via_isi");
    var edit_via = $("#via_edit");
    if (that.value == "hadir via") {
		edit_via.val(via_h.val());
      	edit_via.prop("readOnly", false);
      
	} else {
		edit_via.val("");
      
		edit_via.prop("readOnly", true);
      
	}
}
</script>
@endpush

