@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header">
        <b style="font-size: 18px;">Tambah Penyiapan Data Wawancara Kandidat</b>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header" style="background-color: rgb(47 116 181);">
            </div>
            <div class="card-body">
                <div class="form-group">
                    <form action="{{route('simpan_pdwk')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="temp_id" id="temp_id" />
                        <div class="form-group" style="width:95%;">
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="kode_penyiapan" style="font-size:11px;" value="{{$random}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Disiapkan Oleh</label>
                                        <div class="col-sm-9">
                                            <select name="disiapkan_oleh[]" id="select2-multiple" multiple="multiple" class="select2-multiple form-control form-control-sm" style="font-size:8px;">
                                                @foreach ($data_k['looks'] as $nk )
                                                <option value="{{$nk->Nama_Karyawan}}" style="font-size:8px;">{{$nk->Nama_Karyawan}}</option>
                                                @endforeach
                                            </select><a style="color:red;">*wajib isi</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun</label>
                                        <div class="col-sm-9">
                                            <select name="tahun" id="tahun" style="font-size:11px;" class="form-control form-control-sm">
                                                @php
                                                $year= date('Y');
                                                $min = $year - 60;
                                                $max = $year;
                                                for($i=$max; $i>=$min; $i--){
                                                echo'<option value='.$i.'>'.$i.'</option>';
                                                }

                                                @endphp
                                            </select>
                                            <a style="color:red;">*wajib isi</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="diperiksa_oleh" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diperiksa Oleh</label>
                                        <div class="col-sm-9">
                                            <select name="diperiksa_oleh[]" id="diperiksa_oleh" class="select3-multiple form-control form-control-sm" style="font-size:8px;" multiple>
                                                @foreach ($data_k['looks'] as $nk )
                                                <option value="{{$nk->Nama_Karyawan}}" style="font-size:8px;">{{$nk->Nama_Karyawan}}</option>
                                                @endforeach
                                            </select>
                                            <a style="color:red;">*wajib isi</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Revisi</label>
                                        <div class="col-sm-9">
                                            <input type="number" required name="revisi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> <a style="color:red;">*wajib isi</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div style="width: 100%;">
                            <table>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('list_pdwk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        // Select2 Multiple
        $('.select2-multiple').select2({
            placeholder: "Disiapkan Oleh",
            allowClear: true
        });
    });
    $(document).ready(function() {
        // Select2 Multiple
        $('.select3-multiple').select2({
            placeholder: "Diperiksa Oleh",
            allowClear: true
        });
    });
</script>
@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>

@endsection