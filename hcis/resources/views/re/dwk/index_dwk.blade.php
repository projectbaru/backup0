@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">List Data Wawancara Kandidat</b>
    </div>
    <div class="card-body">
        <form action="{{ URL::to('/list_dwk/hapus_banyak') }}" method="POST" id="form_delete">
            <div class="card-header" style="">
                <!-- <b>List Data Wawancara Kandidat</b> -->
            </div>
            <div class="">
                <div class="">
                    <div class="card">
                        <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                            <b style="font-size: 18px;color:white;">Informasi Penyiapan Data</b>
                        </div>
                        <div class="card-body">
                            <div class="">
                                <div class="container-fluid row">
                                    <div class="col-md-8" >
                                            <table style="font-size:10px;">
                                                <tr>
                                                    <td>Kode Penyiapan</td>
                                                    <td>:</td>
                                                    <td>{{$pdwk->kode_penyiapan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Disiapkan Oleh</td>
                                                    <td>:</td>
                                                    <td>
                                                    @php
                                                $decodedValue = json_decode($pdwk->disiapkan_oleh);

                                                if (is_array($decodedValue)) {
                                                    echo implode(", ", $decodedValue);
                                                } else {
                                                    
                                                    echo ""; 
                                                }
                                                @endphp        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun</td>
                                                    <td>:</td>
                                                    <td>{{$pdwk->tahun}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Diperiksa Oleh</td>
                                                    <td>:</td>
                                                    <td>
                                                    @php
                                                $decodedValue = json_decode($pdwk->diperiksa_oleh);

                                                if (is_array($decodedValue)) {
                                                    echo implode(", ", $decodedValue);
                                                } else {
                                                    
                                                    echo ""; 
                                                }
                                                @endphp     
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>Revisi</td>
                                                    <td>:</td>
                                                    <td>{{$pdwk->revisi}}</td>
                                                </tr>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div style="float:right;font-size:10px;">
                        <table>
                            <tr>
                                <td>
                                    <a class="btn btn-success btn-sm" style="font-size:10px;border-radius:5px;" href="{{route('tambah_dwk', ['pdwk_id' => $pdwk->id_penyiapan_dwk])}}">Tambah Data Wawancara Kandidat</a></td>
                                <td>
                                
                                <td>
                                    <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                    <a class="btn btn-info btn-sm" style="border-radius:5px; font-size:10px;" href="{{route('preview_dwk', ['id_penyiapan_dwk' => $pdwk->id_penyiapan_dwk])}}">Print</a>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-sm" style="font-size:10px;border-radius:5px;" href="{{route('list_pdwk')}}">Kembali</a></td>
                                <td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <br>
                    <div class="card">
                        <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                            <b style="font-size: 18px;color:white;">Data Kandidat yang akan Diwawancarai</b>
                        </div>
                        <div class="card-body">
                            <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Data Wawancara Kandidat</th>
                                        <th scope="col">Nama Kandidat</th>
                                        <th scope="col">Hadir</th>
                                        <th scope="col">Tidak Hadir</th>
                                        <th scope="col">Tempat</th>
                                        <th scope="col">Jabatan</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Keterangan</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody style="color:black;font-size: 10px;">
                                @php $b=1; @endphp
                                @foreach($pdwk->list_dwk as $data)
                                    <tr>
                                        <td scope="col">{{$b++}}</td>
                                        <td scope="col">{{$data->kode_data_wawancara_kandidat}}</td>
                                        <td scope="col">{{$data->nama_kandidat}}</td>
                                        <td scope="col" style="text-align:center;">
                                            @if($data->hasil_konfirmasi === 'hadir via')
                                                {{$data->via}}
                                            @else

                                            @endif
                                        </td>
                                        <td scope="col" style="text-align:center;">
                                            @if(strtolower($data->hasil_konfirmasi) === 'tidak hadir')
                                                V
                                            @else

                                            @endif
                                        </td>
                                        <td scope="col">{{$data->tempat}}</td>
                                        <td scope="col">{{$data->jabatan}}</td>
                                        <td scope="col">{{$data->status_hasil_interview}}</td>
                                        <td scope="col">{{$data->keterangan}}</td>
                                        <td>
                                            <a href="{{ URL::to('/list_dwk/edit_dwk/'.$data->id_data_wawancara_kandidat) }}" style="font-size:8px;" class="btn btn-primary btn-sm">Ubah</a>
                                            <a href="{{ URL::to('/list_dwk/detail_dwk/'.$data->id_data_wawancara_kandidat)}}" style="font-size:8px;" class="btn btn-secondary btn-sm">Detail</a>
                                            <a href="javascript:void(0)" onclick="deleteItem('{{ $data->id_data_wawancara_kandidat }}')" style="font-size:8px;" class="btn btn-danger btn-sm">Hapus</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>



<script>
    
function deleteItem(id) {
    console.log("masuk");
    $("#example").DataTable();
    Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                var actionurl = "{{ URL::to('/list_dwk/hapus_dwk/:id')}}"
                actionurl = actionurl.replace(':id', id);
                
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: {
				        	"_token": "{{ csrf_token() }}",
				        },
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
}
</script>

<script>
    function informasi(that) {
    var tahun_h = $("#tahun_hid");
    var edit_tahun = $("#edit_tahun");
    if (that.value == "Berpengalaman") {
      edit_tahun.val(tahun_h.val());
      edit_bulan.val(bulan_h.val());
      edit_tahun.prop("readOnly", false);
      edit_bulan.prop("readOnly", false);
	} else {
      edit_tahun.val("");
      edit_bulan.val("");
      edit_tahun.prop("readOnly", true);
      edit_bulan.prop("readOnly", true);
	}
}
</script>

@endpush

