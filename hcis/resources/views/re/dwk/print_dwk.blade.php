<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="icon" href="{{asset('assets')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    <style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    </style>
</head>
<body>
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <img src="" alt="">
            </div>
            
            <div class="col" style="text-align:center;">
                <br><br>

                <b style="border:1px solid black;padding:10px;">Daftar kandidat yang akan diwawancara</b>
            </div>
            <div class="col">
                <table style="font-size:12px;">
                    <tr>
                        <td>Tahun</td>
                        <td>:</td>
                        <td>{{$pdwk->tahun}}</td>
                    </tr>
                    <tr>
                        <td>Disiapkan</td>
                        <td>:</td>
                        <td>
                        @php
                        $decodedValue = json_decode($pdwk->disiapkan_oleh);

                        if (is_array($decodedValue)) {
                            echo implode(", ", $decodedValue);
                        } else {
                            
                            echo ""; 
                        }
                        @endphp         
                        </td>
                    </tr>
                    <tr>
                        <td>Diperiksa</td>
                        <td>:</td>
                        <td>
                        @php
                        $decodedValue = json_decode($pdwk->diperiksa_oleh);

                        if (is_array($decodedValue)) {
                            echo implode(", ", $decodedValue);
                        } else {
                            
                            echo ""; 
                        }
                        @endphp          
                        </td>
                    </tr>
                    <tr>
                        <td>Revisi</td>
                        <td>:</td>
                        <td>{{$pdwk->revisi ?? 0}}</td>
                    </tr>
                </table>
            </div>
            <div class="col">
                <button class="btn btn-danger btn-sm no-print " style="border-radius:5px; font-size:10px;" onclick="window.print();">Print</butt>
            </div>
        </div>
        <br>
        <table class="table table-bordered table-sm" style="width:100%; text-align:center; font-size: 12px; border:1px solid #d9d9d9;color:black;">
            <thead style="border:1px solid black;">
                <tr>
                    <th rowspan="3">No</th>
                    <th rowspan="3">Nama Kandidat</th>
                    <th colspan="2">Hasil Konfirmasi</th>
                    <th colspan="8">Interview</th>
                    <th colspan="5" rowspan="2">Hasil</th>
                    <th rowspan="2">Keterangan</th>
                </tr>
                <tr>
                    <td rowspan="2">Hadir</td>
                    <td rowspan="2">Tidak Hadir</td>
                    <td rowspan="2">Tempat</td>
                    <td rowspan="2">Jam</td>
                    <td colspan="2">1</td>
                    <td colspan="2">2</td>
                    <td colspan="2">3</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>Oleh</td>
                    <td>Tanggal</td>
                    <td>Oleh</td>
                    <td>Tanggal</td>
                    <td>Oleh</td>
                    <td>Atasan</td>
                    <td>DOJ</td>
                    <td>Jabatan</td>
                    <td>Lokasi</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>
                @php $b=1; @endphp
                @foreach($dwk as $data)
                <tr>
                    <td>{{$b++;}}</td>
                    <td>{{$data->nama_kandidat}}</td>
                    <td>
                        @if(strtolower($data->hasil_konfirmasi) !== 'tidak hadir')
                            {{$data->hasil_konfirmasi}}
                        @endif
                    </td>
                    <td>
                        @if(strtolower($data->hasil_konfirmasi) === 'tidak hadir')
                            V
                        @endif
                    </td>
                    <td>{{$data->tempat}}</td>
                    <td>{{$data->jam_wawancara}}</td>
                    <td>{{$data->list_jadwal_wawancara[0]->tanggal_interview}}</td>
                    <td>{{$data->list_jadwal_wawancara[0]->nama_pewawancara}}</td>
                    <td>{{$data->list_jadwal_wawancara[2]->tanggal_interview}}</td>
                    <td>{{$data->list_jadwal_wawancara[2]->nama_pewawancara}}</td>
                    <td>{{$data->list_jadwal_wawancara[4]->tanggal_interview}}</td>
                    <td>{{$data->list_jadwal_wawancara[4]->nama_pewawancara}}</td>
                    <td>{{$data->nama_atasan}}</td>
                    <td>{{$data->date_on_join}}</td>
                    <td>{{$data->jabatan}}</td>
                    <td>{{$data->lokasi_kerja}}</td>
                    <td style="color:red;">{{$data->status_hasil_interview}}</td>
                    <td style="color:red;">{{$data->keterangan}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>