@extends('re.dwk.side')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Daftar Kandidat Wawancara</title> -->

</head>
<body>
    <br>
    
    <div class="row" style="width:100%;">
        <div class="col">
            <img src="" alt="">
        </div>
        <div class="col">
            <b>Daftar kandidat yang akan diwawancara</b>
        </div>
        <div class="col">
            <table>
            @foreach($pdwk as $data)
                <tr>
                    <td>Tahun</td>
                    <td>:</td>
                    <td>{{$data->tahun}}</td>
                </tr>
                <tr>
                    <td>Disiapkan</td>
                    <td>:</td>
                    <td>
                    @php
                    $decodedValue = json_decode($data->disiapkan_oleh);

                    if (is_array($decodedValue)) {
                        echo implode(", ", $decodedValue);
                    } else {
                        
                        echo ""; 
                    }
                    @endphp         
                    </td>
                </tr>
                <tr>
                    <td>Diperiksa</td>
                    <td>:</td>
                    <td>
                    @php
                    $decodedValue = json_decode($data->diperiksa_oleh);

                    if (is_array($decodedValue)) {
                        echo implode(", ", $decodedValue);
                    } else {
                        
                        echo ""; 
                    }
                    @endphp          
                    </td>
                </tr>
                <tr>
                    <td>Revisi</td>
                    <td>:</td>
                    <td>{{$data->revisi}}</td>
                </tr>
            @endforeach
            </table>
        </div>
        <div class="col">
        </div>
    </div>
    <br>
    <table>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Nama Kandidat</th>
            <th colspan="2">Hasil Konfirmasi</th>
            <th colspan="8">Interview</th>
            <th colspan="5" rowspan="2">Hasil</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Nama Kandidat</th>
            <th>Hasil Konfirmasi</th>
            <th>Interview</th>
            <th>Hasil</th>
            <th>Keterangan</th>
        </tr>
    </table>
    <table class="table table-bordered" style="width:100%; text-align:center; font-size: 12px; border:1px solid #d9d9d9;color:black;">
        <thead style="border:1px solid black;">
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">Nama Kandidat</th>
                <th colspan="2">Hasil Konfirmasi</th>
                <th colspan="8">Interview</th>
                <th colspan="5" rowspan="2">Hasil</th>
                <th rowspan="2">Keterangan</th>
            </tr>
            <tr>
                <td rowspan="2">Hadir</td>
                <td rowspan="2">Tidak Hadir</td>
                <td rowspan="2">Tempat</td>
                <td rowspan="2">Jam</td>
                <td colspan="2">1</td>
                <td colspan="2">2</td>
                <td colspan="2">3</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>Oleh</td>
                <td>Tanggal</td>
                <td>Oleh</td>
                <td>Tanggal</td>
                <td>Oleh</td>
                <td>Atasan</td>
                <td>DOJ</td>
                <td>Jabatan</td>
                <td>Lokasi</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
            @php $b=1; @endphp
            @foreach($gabungan as $data)
            <tr>
                <td>{{$b++;}}</td>
                <td>{{$data->nama_kandidat}}</td>
                <td>
                    @if({{$data->hasil_konfirmasi=='hadir'}})
                        {{$data->via}}
                    @else

                    @endif
                    </td>
                <td>
                    @if({{$data->hasil_konfirmasi=='tidak hadir'}})
                        V
                    @else

                    @endif
                </td>
                <td>{{$data->tempat}}</td>
                <td>{{$data->jam_wawancara}}</td>
                <td>{{$data->tanggal_interview}}</td>
                <td>{{$data->nama_pewawancara}}</td>
                <td>{{$data->tanggal_interview}}</td>
                <td>{{$data->nama_pewawancara}}</td>
                <td>{{$data->tanggal_interview}}</td>
                <td>{{$data->nama_pewawancara}}</td>
                <td>{{$data->nama_atasan}}</td>
                <td>{{$data->date_on_join}}</td>
                <td>{{$data->jabatan}}</td>
                <td>{{$data->lokasi_kerja}}</td>
                <td>{{$data->status_hasil_interview}}</td>
                <td>{{$data->keterangan}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>