@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header">
        <b style="font-size: 18px;">List Penyiapan Data Wawancara Kandidat</b>
    </div>
    <div class="card-body">
        <form action="{{ URL::to('/list_pdwk/hapus_banyak') }}" method="POST" id="form_delete">
            <div class="">
                <div class="">
                    <div class="container">
                        <table>
                            <tr>
                                <td>Tahun</td>
                                <td>:</td>
                                <td>
                                    <select name="tahun" id="tahun" class="form-control form-control-sm" onchange="document.location.href = '/hcis/public/list_pdwk?tahun=' + this.value">
                                        <option href="{{route('list_pdwk')}}" value="" {{ $tahun == null ? "selected" : "" }}>-- Semua Tahun --</option>
                                        @php
                                        $year= date('Y');
                                        $min = $year - 60;
                                        $max = $year;
                                        for($i=$max; $i>=$min; $i--){
                                        $selected = $tahun == $i ? "selected" : "";
                                        echo'<option value='.$i.' '.$selected.'>'.$i.'</option>';
                                        }
                                        @endphp
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div style="float:right;">
                            <a href="{{ route('tambah_pdwk') }}" class="btn btn-success btn-sm">Tambah Penyiapan Data</a>
                        </div><br><br>
                        <div class="card">
                            <div class="card-header" style="background-color: rgb(42 117 183);">
                            </div>
                            <div class="card-body">
                                <table id="contoh" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Penyiapan</th>
                                            <th>Disiapkan Oleh</th>
                                            <th>Tanggal Dibuat</th>
                                            <th>Tahun</th>
                                            <th>Diperiksa</th>
                                            <th>Revisi</th>
                                            <th>Total Daftar Kandidat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $b=1; @endphp
                                        @foreach($pdwk as $data)
                                        <tr>

                                            <td>{{$b++;}}</td>
                                            <td>{{$data->kode_penyiapan}}</td>
                                            <td>
                                                @php
                                                $decodedValue = json_decode($data->disiapkan_oleh);

                                                if (is_array($decodedValue)) {
                                                echo implode(", ", $decodedValue);
                                                } else {

                                                echo "";
                                                }
                                                @endphp
                                            </td>
                                            <td>{{date('d-m-Y', strtotime($data->waktu_input))}}</td>
                                            <td>{{$data->tahun}}</td>
                                            <td>
                                                @php     
                                                $decodedValue = json_decode($data->diperiksa_oleh);
                                                if (is_array($decodedValue)) {
                                                    echo implode(", ", $decodedValue);
                                                } else {                                                    
                                                    echo ""; 
                                                }
                                                @endphp    
                                            <!-- {{$data->diperiksa_oleh}} -->
                                            </td>
                                            <td>{{$data->revisi}}</td>
                                            <td>{{ count($data->list_dwk) }}</td>
                                            <!-- <td>{{-- $data->keterangan --}}</td> -->
                                            <td>
                                                <a href="{{ URL::to('/list_pdwk/edit_pdwk/'.$data->id_penyiapan_dwk) }}" style="font-size:11px;border-radius:5px;" class="btn btn-success btn-sm">Ubah</a>
                                                <a href="{{ URL::to('/list_pdwk/detail_pdwk/'.$data->id_penyiapan_dwk) }}" style="font-size:11px;border-radius:5px;" class="btn btn-primary btn-sm">Detail</a>
                                                <a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_pdwk', $data->id_penyiapan_dwk)}}" id="btn_delete">Hapus</a>
                                                <a href="{{ URL::to('/list_dwk/'.$data->id_penyiapan_dwk) }}" style="font-size:11px;border-radius:5px;" class="btn btn-secondary btn-sm">Manage Kandidat</a>
                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

<script>
    //hang on event of form with id=myform
    function deleteItem(id) {

        Swal.fire({
            title: '<h4>Anda yakin ingin menghapus?</h4>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: "Batal",
            confirmButtonText: "<i>Ya, hapus!</i>",
            // buttonsStyling: false
        }).then((result) => {
            var actionurl = "{{ URL::to('/list_pdwk/hapus_pdwk/:id')}}"
            actionurl = actionurl.replace(':id', id);

            if (result.isConfirmed) {
                $.ajax({
                    url: actionurl,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(res) {
                        if (res.status) {
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil!",
                                text: "Data berhasil dihapus.",
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        Swal.fire({
                            icon: "error",
                            title: "Gagal!",
                            text: "Terjadi kesalahan saat menghapus data.",
                        });
                    },
                    complete: function(jqXhr, msg) {
                        setTimeout(() => {
                            location.reload();
                        }, 800);
                        // console.log(jqXhr, msg);
                    }
                });
            }
        })
    }
</script>

<script type="text/javascript">
    $(document).on('click', '#btn_delete', function(e) {
        e.preventDefault();
        var link = $(this);
        var linkHref = link.attr('href');
        console.log(linkHref);

        Swal.fire({
            title: '<h4>Anda yakin ingin menghapus?</h4>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: "Batal",
            confirmButtonText: "<i>Ya, hapus!</i>",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: linkHref,
                    type: 'GET',
                    dataType: 'JSON',
                    cache: false,
                    success: function(res) {
                        if (res.status) {
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil!",
                                text: "Data telah terhapus.",
                            }).then((result) => {
                                if (result.value) {
                                    location.href = "{{ URL::to('list_pdwk') }}";
                                }
                            });
                        }
                    },
                    error: function(e) {
                        Swal.fire({
                            icon: "error",
                            title: "Gagal!",
                            text: "Data tidak terhapus.",
                        });
                    },
                });
            }
        });
    });
</script>
@endpush