<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

</head>
<body>
    <br>
    <div class="container">
        <a href="" class="btn btn-primary btn-sm">Print</a>
    <table class="table table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 10px; border:1px solid #d9d9d9;color:black;text-align:center;">
        <thead>
            <tr>
                <th rowspan="2">Parameter</th>
                <th rowspan="2">KPI</th>
                <th rowspan="2">Polarization</th>
                <th rowspan="2">UoM</th>
                <th rowspan="2">Weight</th>
                <th colspan="4">May</th>
                <th colspan="4">June</th>
                <th colspan="4">July</th>
                <th colspan="4">Agt</th>
                <th colspan="2">Definition</th>
                <th>Output</th>
                <th>Target</th>
                <th style="background-color:orange;">Mgr</th>
                <th style="background-color:orange;">PIC</th>
                <th style="background-color:orange;">Staff</th>
            </tr>
            <tr>
                <th>Target</th>
                <th>Weight</th>
                <th>Act</th>
                <th>Ach</th>
                <th>Target</th>
                <th>Weight</th>
                <th>Act</th>
                <th>Ach</th>
                <th>Target</th>
                <th>Weight</th>
                <th>Act</th>
                <th>Ach</th>
                <th>Target</th>
                <th>Weight</th>
                <th>Act</th>
                <th>Ach</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="background-color:orange;"></th>
                <th style="background-color:orange;"></th>
                <th style="background-color:orange;"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
               <td>baru</td>
               <td>baru</td>
               <td>baru</td>
               <td>baru</td>
               <td>baru</td>
               <td>baru</td>
               <td>baru</td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3"></td>
                <td>Total</td>
                <td></td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td>%</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td colspan="3"></td>
                <td colspan="2">Total Achievement</td>
                <td colspan="4">%</td>
                <td colspan="4">%</td>
                <td colspan="4">%</td>
                <td colspan="4">%</td>
                <td colspan="7"></td>
            </tr>
        </tfoot>
    </table>
    </div>
</body>
</html>