@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Tambah Pencapaian KPI HR Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
	<form action="{{route('simpan_pkhr')}}" method="POST" enctype="multipart/form-data" class="pl-3">
		<input type="hidden" name="jmldetail" id="jmldetail">
	
		<!-- <table>
			<tr>
				<td>
					<button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Simpan</a></td>
				<td>
					<a class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="{{route('list_pkhr')}}">Batal</a>
				</td>
			</tr>
		</table> -->
		{{ csrf_field() }}
		<input type="hidden" name="temp_id" id="temp_id" />
    <div class="card-header" style="">
        <b style="font-size: 18px;">Tambah Pencapaian KPI HR Recruitmen</b>
    </div>
    <div class="card-body" style="overflow-x: scroll;">
		<div class="card" style="min-width:1050px;">
			<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
				<p style="margin: -10px;">Tahun dan Parameter Pencapaian</p>
			</div>
			<div class="card-body">			
					<div class="form-group" style="width:100%;" >
<!-- 						
						<p>Data Pribadi</p>
						<hr> -->
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tahun Pencapaian</label>
									<div class="col-sm-2">
										<select name="tahun_pencapaian" id="tahun_pencapaian" class="form-control form-control-sm">
											<option href="{{route('list_pdwk')}}" disabled selected value="">-- Pilih Tahun --</option>
											@php
												$year= date('Y');
												$min = $year - 60;
												$max = $year;
												for($i=$max; $i>=$min; $i--){
												
												echo'<option value='.$i.'>'.$i.'</option>';
												}
											@endphp
										</select>	
										<!-- <input type="text" required name="tahun_pencapaian" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a>  -->
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Parameter</label>
									<div class="col-sm-2">
										<input type="text" required name="nama_parameter" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
						<div id="pkhr_tambah">
							<div class="row" style="min-width:1050px;">
								<div class="col-md-11"  style="">
									<div class="card text-black mb-3" style="width:100%;">
										<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
											<div class="row" style="">
												<div class="col">
													<p style="margin: -10px;">KPI</p>
												</div>
											</div>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">KPI</label>
														<div class="col-sm-7">
															<select name="nama_kpi1" id="nama_kpi" required class="form-control form-control-sm">
																<option value="A/B Recruitment Cost">A/B Recruitment Cost</option>
																<option value="A/T Recruitment Rate">A/T Fullfillment Rate</option>
																<option value="% Training Delivery">% Training Delivery</option>
																<option value="Training Evaluation">Training Evaluation</option>
																<option value="Applicant Target">Applicant Target</option>
																<option value="Reporting Management">Reporting Management</option>
																<option value="Training Mandays">Training Mandays</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Polarization</label>
														<div class="col-sm-7">
															<select name="polarization1" required id="polarization" class="form-control form-control-sm">
																<option value="Lower Better">Lower Better</option>
																<option value="High Better">High Better</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">UoM</label>
														<div class="col-sm-7">
															<select name="uom1" id="uom" class="form-control form-control-sm" required>
																<option value="percent">Percent</option>
																<option value="point">Point</option>
																<option value="hour">Hour</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
														<div class="col-sm-7">
															<input type="number" name="weight1" class="form-control form-control-sm" required><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
											</div>
											<div class="row" style="width:100%">
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 1</label>
														<div class="col-sm-6">
															<textarea type="text" required name="definition1_1" style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 2</label>
														<div class="col-sm-6">
															<textarea type="text" required name="definition1_2" style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Output</label>
														<div class="col-sm-7">
															<select name="output1" id="output" class="form-control form-control-sm" required>
																<option value="Monthly (Jan – Des)">Monthly (Jan – Des)</option>
																<option value="Quarterly (Jan, Apr, Jul, Oct)">Quarterly (Jan, Apr, Jul, Oct)</option>
															</select><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
														<div class="col-sm-7">
															<input type="number" name="target1" class="form-control form-control-sm">
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<div class="col-sm-7">
															<div class="form-check">
																<input class="form-check-input" type="checkbox" value="aktif" name="manager1" id="flexCheckDefault">
																<label class="form-check-label" for="flexCheckDefault">
																	Mgr
																</label>
															</div>
															<div class="form-check">
																<input class="form-check-input" type="checkbox" value="aktif" name="pic1" id="flexCheckChecked">
																<label class="form-check-label" for="flexCheckChecked">
																	PIC
																</label>
															</div>
															<div class="form-check">
																<input class="form-check-input" type="checkbox" value="aktif" name="staff1" id="flexCheckChecked">
																<label class="form-check-label" for="flexCheckChecked">
																	Staff
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
												<div class="card text-white mb-3" style="width:100%;">
												<div class="card-body" id="descriptionInputContainer1">
													<div class="row">
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
																<div class="col-sm-7">
																	<select name="bulan1[]" id="bulan" class="form-control form-control-sm" required>
																			<option value="Januari">Januari</option>
																			<option value="Februari">Februari</option>
																			<option value="Maret">Maret</option>
																			<option value="April">April</option>
																			<option value="Mei">Mei</option>
																			<option value="Juni">Juni</option>
																			<option value="Juli">Juli</option>
																			<option value="Agustus">Agustus</option>
																			<option value="September">September</option>
																			<option value="Oktober">Oktober</option>
																			<option value="November">November</option>
																			<option value="Desember">Desember</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
																<div class="col-sm-7">
																	<input type="number"  required name="target_perbulan1[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
																<div class="col-sm-7">
																	<input type="number"  required name="weightd1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
																<div class="col-sm-7">
																	<input type="number"  value="" name="act1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
																<div class="col-sm-7">
																	<input type="number" value="" name="ach1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" readonly>
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<div class="col-sm-7">
																	<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary add_pkhr" id="add_pkhr" data-detailnum="1">
																		<i class="fa-solid fa-plus"></i>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												</div>
											</div>
											</div>
										</div>
									</div>
									
									<br>
									
								</div>
								<div class="col-md-1" style="width:10px;">
									<button type="button" style="font-size:11px;" class="btn  btn-primary btn-sm" id="add_pkhr_all">
										<i class="fa-solid fa-plus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div style="width: 100%;">
						<table>
							<tr>
								<td>
									<input type="submit" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;" value="Simpan">
								</td>
								<td>
									<a href="{{ route('list_pkhr') }}" class="btn btn-secondary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
								</td>
							</tr>
						</table>
					</div>
					<br>
				</form>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@include('js.pkhr.tambah')
@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>

@endsection

