@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
	<form action="{{route('updatepkhr')}}" method="POST" enctype="multipart/form-data" class="pl-3">
		<input type="hidden" name="jmldetail" id="jmldetail" value="{{ count($pkhr['child']) }}">
        <input type="hidden" name="header_id" value="{{ $pkhr['header_id'] }}">
	
		<!-- <table>
			<tr>
				<td>
					<button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Simpan</a></td>
				<td>
					<a class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="{{route('list_pkhr')}}">Batal</a>
				</td>
			</tr>
		</table> -->
		{{ csrf_field() }}
		<input type="hidden" name="temp_id" id="temp_id" />
    <div class="card-header" style="">
        <b style="font-size: 18px;">Detail Pencapaian KPI HR Recruitmen</b>
    </div>
    <div class="card-body" style="overflow-x: scroll;">
		<div class="card" style="min-width:1050px;">
			<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
				<p style="margin: -10px;">Tahun dan Parameter Pencapaian</p>
			</div>
			<div class="card-body">			
					<div class="form-group" style="width:100%;" >
<!-- 						
						<p>Data Pribadi</p>
						<hr> -->
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tahun Pencapaian</label>
									<div class="col-sm-2">
										<input type="text" value="{{$pkhr['tahun_pencapaian']}}" readonly required name="tahun_pencapaian" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Parameter</label>
									<div class="col-sm-2">
										<input type="text" value="{{$pkhr['nama_parameter']}}" readonly required name="nama_parameter" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
						<div id="pkhr_tambah">
							<div class="row" style="min-width:1050px;">
								<div class="col-md-11" style="">
									<div class="card text-black mb-3" style="width:100%;">
										<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
											<div class="row" style="">
												<div class="col">
													<p style="margin: -10px;">KPI</p>
												</div>
											</div>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">KPI</label>
														<div class="col-sm-7">
															<select name="nama_kpi1" id="nama_kpi" required disabled class="form-control form-control-sm">
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'A/B Recruitment Cost' ? 'selected' : ''}} value="A/B Recruitment Cost">A/B Recruitment Cost</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'A/T Recruitment Rate' ? 'selected' : ''}} value="A/T Recruitment Rate">A/T Fullfillment Rate</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == '% Training Delivery' ? 'selected' : ''}} value="% Training Delivery">% Training Delivery</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'Training Evaluation' ? 'selected' : ''}} value="Training Evaluation">Training Evaluation</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'Applicant Target' ? 'selected' : ''}} value="Applicant Target">Applicant Target</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'Reporting Management' ? 'selected' : ''}} value="Reporting Management">Reporting Management</option>
																<option {{$pkhr['child'][0]['c1']['nama_kpi'] == 'Training Mandays' ? 'selected' : ''}} value="Training Mandays">Training Mandays</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Polarization</label>
														<div class="col-sm-7">
															<select name="polarization1" required id="polarization" disabled class="form-control form-control-sm">
																<option value="Lower Better" {{$pkhr['child'][0]['c1']['polarization'] == 'Lower Better' ? 'selected' : ''}}>Lower Better</option>
																<option value="High Better" {{$pkhr['child'][0]['c1']['polarization'] == 'High Better' ? 'selected' : ''}}>High Better</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">UoM</label>
														<div class="col-sm-7">
															<select name="uom1" id="uom" class="form-control form-control-sm" disabled required>
																<option value="percent" {{$pkhr['child'][0]['c1']['uom'] == 'percent' ? 'selected' : ''}}>Percent</option>
																<option value="point" {{$pkhr['child'][0]['c1']['uom'] == 'point' ? 'selected' : ''}}>Point</option>
																<option value="hour" {{$pkhr['child'][0]['c1']['uom'] == 'hour' ? 'selected' : ''}}>Hour</option>
															</select><a style="color:red;">*wajib isi</a>	
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
														<div class="col-sm-7">
															<input value="{{$pkhr['child'][0]['c1']['weight']}}" type="number" readonly name="weight1" class="form-control form-control-sm" required><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
											</div>
											<div class="row" style="width:100%">
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 1</label>
														<div class="col-sm-6">
															<textarea type="text" required name="definition1_1" readonly disabled style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$pkhr['child'][0]['c1']['definition_1']}}</textarea><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 2</label>
														<div class="col-sm-6">
															<textarea type="text" required name="definition1_2" readonly disabled style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$pkhr['child'][0]['c1']['definition_2']}}</textarea><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Output</label>
														<div class="col-sm-7">
															<select name="output1" id="output" disabled class="form-control form-control-sm" required>
																<option value="Monthly (Jan – Des)" {{$pkhr['child'][0]['c1']['output'] == 'Monthly (Jan – Des)' ? 'selected' : ''}}>Monthly (Jan – Des)</option>
																<option value="Quarterly (Jan, Apr, Jul, Oct)" {{$pkhr['child'][0]['c1']['output'] == 'Quarterly (Jan, Apr, Jul, Oct)' ? 'selected' : ''}}>Quarterly (Jan, Apr, Jul, Oct)</option>
															</select><a style="color:red;">*wajib isi</a>
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
														<div class="col-sm-7">
															<input readonly value="{{$pkhr['child'][0]['c1']['target']}}" type="number" name="target1" class="form-control form-control-sm">
														</div>
													</div>
												</div>
												<div class="col">
													<div class="form-group row">
														<div class="col-sm-7">
															<div class="form-check">
																<input {{$pkhr['child'][0]['c1']['manager'] == 'active' ? 'checked' : ''}} class="form-check-input" disabled type="checkbox" value="aktif" name="manager1" id="flexCheckDefault">
																<label class="form-check-label" for="flexCheckDefault">
																	Mgr
																</label>
															</div>
															<div class="form-check">
																<input {{$pkhr['child'][0]['c1']['pic'] == 'active' ? 'checked' : ''}} class="form-check-input" disabled type="checkbox" value="aktif" name="pic1" id="flexCheckChecked">
																<label class="form-check-label" for="flexCheckChecked">
																	PIC
																</label>
															</div>
															<div class="form-check">
																<input {{$pkhr['child'][0]['c1']['staff'] == 'active' ? 'checked' : ''}} class="form-check-input" disabled type="checkbox" value="aktif" name="staff1" id="flexCheckChecked">
																<label class="form-check-label" for="flexCheckChecked">
																	Staff
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row" style="min-width:1020px;">
												<div class="col-sm-11" style="min-width:1000px;">
												<div class="card text-white mb-3" style="width:100%;">
												<div class="card-body" id="descriptionInputContainer1">
													<div class="row">
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
																<div class="col-sm-7">
																	<select name="bulan1[]" disabled id="bulan" class="form-control form-control-sm" disabled required>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Januari' ? 'selected' : ''}} value="Januari">Januari</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Februari' ? 'selected' : ''}} value="Februari">Februari</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Maret' ? 'selected' : ''}} value="Maret">Maret</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'April' ? 'selected' : ''}} value="April">April</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Mei' ? 'selected' : ''}} value="Mei">Mei</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Juni' ? 'selected' : ''}} value="Juni">Juni</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Juli' ? 'selected' : ''}} value="Juli">Juli</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Agustus' ? 'selected' : ''}} value="Agustus">Agustus</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'September' ? 'selected' : ''}} value="September">September</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Oktober' ? 'selected' : ''}} value="Oktober">Oktober</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'November' ? 'selected' : ''}} value="November">November</option>
                                                                        <option {{$pkhr['child'][0]['c2'][0]['bulan'] == 'Desember' ? 'selected' : ''}} value="Desember">Desember</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
																<div class="col-sm-7">
																	<input value="{{$pkhr['child'][0]['c2'][0]['target_perbulan']}}" type="number" readonly required name="target_perbulan1[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
																<div class="col-sm-7">
																	<input value="{{$pkhr['child'][0]['c2'][0]['weight']}}" type="number" readonly required name="weightd1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
																<div class="col-sm-7">
																	<input value="{{$pkhr['child'][0]['c2'][0]['act']}}" type="number" readonly name="act1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<div class="col">
															<div class="form-group row">
																<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
																<div class="col-sm-7">
																	<input value="{{$pkhr['child'][0]['c2'][0]['ach']}}" type="number" readonly name="ach1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
																	<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
																</div>
															</div>
														</div>
														<!-- <div class="col">
															<div class="form-group row">
																<div class="col-sm-7">
																	<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary add_pkhr" id="add_pkhr" data-detailnum="1">
																		<i class="fa-solid fa-plus"></i>
																	</button>
																</div>
															</div>
														</div> -->
													</div>
                                                    @for($j=1;$j<count($pkhr['child'][0]['c2']);$j++)
                                                        <div id="responsibilityClone{{($j+1)}}1" class="row">
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                                                    <div class="col-sm-7">
                                                                        <select name="bulan1[]" id="bulan" disabled class="form-control form-control-sm">
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Januari' ? 'selected' : ''}} value="Januari">Januari</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Februari' ? 'selected' : ''}} value="Februari">Februari</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Maret' ? 'selected' : ''}} value="Maret">Maret</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'April' ? 'selected' : ''}} value="April">April</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Mei' ? 'selected' : ''}} value="Mei">Mei</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Juni' ? 'selected' : ''}} value="Juni">Juni</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Juli' ? 'selected' : ''}} value="Juli">Juli</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Agustus' ? 'selected' : ''}} value="Agustus">Agustus</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'September' ? 'selected' : ''}} value="September">September</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Oktober' ? 'selected' : ''}} value="Oktober">Oktober</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'November' ? 'selected' : ''}} value="November">November</option>
                                                                            <option {{$pkhr['child'][0]['c2'][$j]['bulan'] == 'Desember' ? 'selected' : ''}} value="Desember">Desember</option>
                                                                        </select>	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][0]['c2'][$j]['target_perbulan']}}" type="number" readonly required name="target_perbulan1[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][0]['c2'][$j]['weight']}}" type="number" readonly required name="weightd1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][0]['c2'][$j]['act']}}" type="number" readonly required name="act1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][0]['c2'][$j]['ach']}}" type="number" readonly required name="ach1[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    @endfor
												</div>
												</div>
											</div>
											</div>
										</div>
									</div>
									
									<br>
									
								</div>
								
							</div>
                            @for($i=1;$i<count($pkhr['child']);$i++)
                                <div id="responsibilityClone{{($i+1)}}" class="row" style="min-width:1050px;">
                                    <div style="width:1030px;">
                                    <div class="card text-black mb-3" style="width:100%;">
                                        <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                                            <div class="row" style="">
                                                <div class="col">
                                                    <p style="margin: -10px;">KPI</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row" >
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">KPI</label>
                                                        <div class="col-sm-7">
                                                            <select name="nama_kpi{{($i+1)}}" id="nama_kpi" disabled class="form-control form-control-sm">
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'A/B Recruitment Cost' ? 'selected' : ''}} value="A/B Recruitment Cost">A/B Recruitment Cost</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'A/T Recruitment Rate' ? 'selected' : ''}} value="A/T Recruitment Rate">A/T Fullfillment Rate</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == '% Training Delivery' ? 'selected' : ''}} value="% Training Delivery">% Training Delivery</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'Training Evaluation' ? 'selected' : ''}} value="Training Evaluation">Training Evaluation</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'Applicant Target' ? 'selected' : ''}} value="Applicant Target">Applicant Target</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'Reporting Management' ? 'selected' : ''}} value="Reporting Management">Reporting Management</option>
																<option {{$pkhr['child'][$i]['c1']['nama_kpi'] == 'Training Mandays' ? 'selected' : ''}} value="Training Mandays">Training Mandays</option>
                                                            </select>	
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Polarization</label>
                                                        <div class="col-sm-7">
                                                            <select name="polarization{{($i+1)}}" id="polarization" disabled class="form-control form-control-sm">
                                                                <option {{$pkhr['child'][$i]['c1']['polarization'] == 'Lower Better' ? 'selected' : ''}} value="Lower Better">Lower Better</option>
                                                                <option {{$pkhr['child'][$i]['c1']['polarization'] == 'High Better' ? 'selected' : ''}} value="High Better">High Better</option>
                                                            </select>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">UoM</label>
                                                        <div class="col-sm-7">
                                                            <select name="uom{{($i+1)}}" readonly id="uom" disabled class="form-control form-control-sm">
                                                                <option {{$pkhr['child'][$i]['c1']['uom'] == 'percent' ? 'selected' : ''}} value="percent">Percent</option>
                                                                <option {{$pkhr['child'][$i]['c1']['uom'] == 'point' ? 'selected' : ''}} value="point">Point</option>
                                                                <option {{$pkhr['child'][$i]['c1']['uom'] == 'hour' ? 'selected' : ''}} value="hour">Hour</option>
                                                            </select>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                                        <div class="col-sm-7">
                                                            <input value="{{$pkhr['child'][$i]['c1']['weight']}}" readonly type="number" name="weight{{($i+1)}}" class="form-control form-control-sm">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 1</label>
                                                        <div class="col-sm-6">
                                                            <textarea type="text" required name="definition{{($i+1)}}_1" readonly style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$pkhr['child'][$i]['c1']['definition_1']}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Definition 2</label>
                                                        <div class="col-sm-6"> 
                                                            <textarea type="text" required name="definition{{($i+1)}}_2" readonly style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$pkhr['child'][$i]['c1']['definition_2']}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Output</label>
                                                        <div class="col-sm-7">
                                                            <select name="output{{($i+1)}}" id="output" disabled class="form-control form-control-sm">
                                                                <option {{$pkhr['child'][$i]['c1']['output'] == 'Monthly (Jan – Des)' ? 'selected' : ''}} value="Monthly (Jan – Des)">Monthly (Jan – Des)</option>
                                                                <option {{$pkhr['child'][$i]['c1']['output'] == 'Quarterly (Jan, Apr, Jul, Oct)' ? 'selected' : ''}} value="Quarterly (Jan, Apr, Jul, Oct)">Quarterly (Jan, Apr, Jul, Oct)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                                        <div class="col-sm-7">
                                                            <input value="{{$pkhr['child'][$i]['c1']['target']}}" type="number" readonly name="target{{($i+1)}}" class="form-control form-control-sm">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <div class="col-sm-7">
                                                            <div class="form-check">
                                                                <input {{$pkhr['child'][$i]['c1']['manager'] == 'active' ? 'checked' : ''}} disabled class="form-check-input" type="checkbox" value="aktif" name="manager{{($i+1)}}" id="flexCheckDefault">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Mgr
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input {{$pkhr['child'][$i]['c1']['pic'] == 'active' ? 'checked' : ''}} class="form-check-input" disabled type="checkbox" value="aktif" name="pic{{($i+1)}}" id="flexCheckChecked">
                                                                <label class="form-check-label" for="flexCheckChecked">
                                                                    PIC
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input {{$pkhr['child'][$i]['c1']['staff'] == 'active' ? 'checked' : ''}} class="form-check-input" disabled type="checkbox" value="aktif" name="staff{{($i+1)}}" id="flexCheckChecked">
                                                                <label class="form-check-label" for="flexCheckChecked">
                                                                    Staff
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <div class="card text-white mb-3" style="width:100%;">
                                                    <div class="card-body" id="descriptionInputContainer{{($i+1)}}">
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                                                    <div class="col-sm-7">
                                                                        <select name="bulan{{($i+1)}}[]" id="bulan" class="form-control form-control-sm" disabled>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Januari' ? 'selected' : ''}} value="Januari">Januari</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Februari' ? 'selected' : ''}} value="Februari">Februari</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Maret' ? 'selected' : ''}} value="Maret">Maret</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'April' ? 'selected' : ''}} value="April">April</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Mei' ? 'selected' : ''}} value="Mei">Mei</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Juni' ? 'selected' : ''}} value="Juni">Juni</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Juli' ? 'selected' : ''}} value="Juli">Juli</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Agustus' ? 'selected' : ''}} value="Agustus">Agustus</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'September' ? 'selected' : ''}} value="September">September</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Oktober' ? 'selected' : ''}} value="Oktober">Oktober</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'November' ? 'selected' : ''}} value="November">November</option>
                                                                            <option {{$pkhr['child'][$i]['c2'][0]['bulan'] == 'Desember' ? 'selected' : ''}} value="Desember">Desember</option>
                                                                        </select>	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][$i]['c2'][0]['target_perbulan'] }}" readonly type="number" required name="target_perbulan{{($i+1)}}[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][$i]['c2'][0]['weight'] }}" type="number" readonly required name="weightd{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][$i]['c2'][0]['act'] }}" type="number" readonly required name="act{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                                                    <div class="col-sm-7">
                                                                        <input value="{{$pkhr['child'][$i]['c2'][0]['ach'] }}" type="number" readonly required name="ach{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                                                        <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        <div>
                                                    </div>
                                                </div>
                                                @for($j=1;$j<count($pkhr['child'][$i]['c2']);$j++)
                                                    <div id="responsibilityClone{{($j+1)}}{{($i+1)}}" class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                                                <div class="col-sm-7">
                                                                    <select name="bulan{{($i+1)}}[]" id="bulan" disabled class="form-control form-control-sm">
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Januari' ? 'selected' : ''}} value="Januari">Januari</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Februari' ? 'selected' : ''}} value="Februari">Februari</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Maret' ? 'selected' : ''}} value="Maret">Maret</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'April' ? 'selected' : ''}} value="April">April</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Mei' ? 'selected' : ''}} value="Mei">Mei</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Juni' ? 'selected' : ''}} value="Juni">Juni</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Juli' ? 'selected' : ''}} value="Juli">Juli</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Agustus' ? 'selected' : ''}} value="Agustus">Agustus</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'September' ? 'selected' : ''}} value="September">September</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Oktober' ? 'selected' : ''}} value="Oktober">Oktober</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'November' ? 'selected' : ''}} value="November">November</option>
                                                                        <option {{$pkhr['child'][$i]['c2'][$j]['bulan'] == 'Desember' ? 'selected' : ''}} value="Desember">Desember</option>
                                                                    </select>	
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                                                <div class="col-sm-7">
                                                                    <input value="{{$pkhr['child'][$i]['c2'][$j]['target_perbulan']}}" type="number" readonly required name="target_perbulan{{($i+1)}}[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                                                <div class="col-sm-7">
                                                                    <input value="{{$pkhr['child'][$i]['c2'][$j]['weight']}}" type="number" readonly  required name="weightd{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                                                <div class="col-sm-7">
                                                                    <input value="{{$pkhr['child'][$i]['c2'][$j]['act']}}" type="number" readonly required name="act{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                                                <div class="col-sm-7">
                                                                    <input value="{{$pkhr['child'][$i]['c2'][$j]['ach']}}" type="number" readonly  required name="ach{{($i+1)}}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                                
                                </div>
                            @endfor
						</div>
					</div>
					<hr>
          <div style="width: 100%;">
            <table>
            <tr>
              <td>
                <a href="{{ URL::to('/list_pkhr/edit_pkhr/'.$pkhr['header_id'] ) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Edit</a>
              </td>
              <td>
                <a href="{{ route('list_pkhr') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
              </td>
              <td>
                <a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_pkhr', $pkhr['header_id'] )}}"  id="btn_delete">Hapus</a>
              </td>
            </tr>
            </table>
          </div>
					<!-- <div style="width: 100%;">
						<table>
							<tr>
								<td>
                    <input type="submit" class="btn btn-danger btn-sm"
                                    style="border:none;border-radius:5px;font-size:11px;" value="Ubah">
								</td>
								<td>
									<a href="{{ route('list_pkhr') }}" class="btn btn-secondary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
								</td>
							</tr>
						</table>
					</div> -->
					<br>
				</form>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@include('js.pkhr.edit_pkhr')
@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_pkhr') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>

@endsection

