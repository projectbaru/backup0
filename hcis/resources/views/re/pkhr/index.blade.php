@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
.table-content {
  display: block;
  /* max-width: -moz-fit-content; */
  /* max-width: fit-content; */
  /* margin: 0 auto; */
  overflow-x: auto;
  white-space: nowrap;
  
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">List Pencapaian KPI HR Recruitment</b>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
              <b> Total Achievement & weight</b>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl m-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                        <thead>

                        </thead>
                        <tbody style="color:black;font-size: 14px; ">
                            <tr>
                                <td >Januari</td>
                                <td  scope="col">
                                    <?php 
                                    $totalachjanuari=0;
                                    $totaljanuari=0; ?>
                                    @foreach ($jan as $data)
                                      <?php $totalachjanuari += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($jan as $data)
                                      <?php $totaljanuari += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totaljanuari == 0 && $totalachjanuari == 0) ? (0) : (number_format((($totaljanuari / $totalachjanuari) * 1), 2))  }}%</td>
                                <td scope="col">May</td>
                                <td scope="col"><?php 
                                    $totalachmei=0;
                                    $totalmei=0; ?>
                                    @foreach ($mei as $data)
                                      <?php $totalachmei += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($mei as $data)
                                      <?php $totalmei += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalmei == 0 && $totalachmei == 0) ? (0) : (number_format((($totalmei / $totalachmei) * 1), 2))  }}%</td>
                                <td scope="col">September</td>
                                <td scope="col"><?php 
                                    $totalachseptember=0;
                                    $totalseptember=0; ?>
                                    @foreach ($september as $data)
                                      <?php $totalachseptember += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($september as $data)
                                      <?php $totalseptember += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalseptember == 0 && $totalachseptember == 0) ? (0) : (number_format((($totalseptember / $totalachseptember) * 1), 2))  }}%</td>
                            </tr>
                            <tr>
                                <td >Februari</td>
                                <td  scope="col"><?php 
                                    $totalachfebruari=0;
                                    $totalfebruari=0; ?>
                                    @foreach ($februari as $data)
                                      <?php $totalachfebruari += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($februari as $data)
                                      <?php $totalfebruari += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalfebruari == 0 && $totalachfebruari == 0) ? (0) : (number_format((($totalfebruari / $totalachfebruari) * 1), 2))  }}%</td>
                                <td scope="col">Juni</td>
                                <td scope="col"><?php 
                                    $totalachjuni=0;
                                    $totaljuni=0; ?>
                                    @foreach ($juni as $data)
                                      <?php $totalachjuni += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($juni as $data)
                                      <?php $totaljuni += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totaljuni == 0 && $totalachjuni == 0) ? (0) : (number_format((($totaljuni / $totalachjuni) * 1), 2))  }}%</td>
                                <td scope="col">Oktober</td>
                                <td scope="col"><?php 
                                    $totalachoktober=0;
                                    $totaloktober=0; ?>
                                    @foreach ($oktober as $data)
                                      <?php $totalachoktober += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($oktober as $data)
                                      <?php $totaloktober += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totaloktober == 0 && $totalachoktober == 0) ? (0) : (number_format((($totaloktober / $totalachoktober) * 1), 2))  }}%</td>
                            </tr>
                            <tr>
                                <td >Maret</td>
                                <td ><?php 
                                    $totalachmaret=0;
                                    $totalmaret=0; ?>
                                    @foreach ($maret as $data)
                                      <?php $totalachmaret += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($maret as $data)
                                      <?php $totalmaret += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalmaret == 0 && $totalachmaret == 0) ? (0) : (number_format((($totalmaret / $totalachmaret) * 1), 2))  }}%</td>
                                <td scope="col">July</td>
                                <td scope="col"><?php 
                                    $totalachjuli=0;
                                    $totaljuli=0; ?>
                                    @foreach ($juli as $data)
                                      <?php $totalachjuli += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($juli as $data)
                                      <?php $totaljuli += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totaljuli == 0 && $totalachjuli == 0) ? (0) : (number_format((($totaljuli / $totalachjuli) * 1), 2))  }}%</td>
                                <td scope="col">November</td>
                                <td scope="col"><?php 
                                    $totalachnovember=0;
                                    $totalnovember=0; ?>
                                    @foreach ($november as $data)
                                      <?php $totalachnovember += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($november as $data)
                                      <?php $totalnovember += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalnovember == 0 && $totalachnovember == 0) ? (0) : (number_format((($totalnovember / $totalachnovember) * 1), 2))  }}%</td>
                            </tr>
                            <tr>
                                <td >April</td>
                                <td  scope="col"><?php 
                                    $totalachapril=0;
                                    $totalapril=0; ?>
                                    @foreach ($april as $data)
                                      <?php $totalachapril += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($april as $data)
                                      <?php $totalapril += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalapril == 0 && $totalachapril == 0) ? (0) : (number_format((($totalapril / $totalachapril) * 1), 2))  }}%</td>
                                <td scope="col">Agustus</td>
                                <td scope="col"><?php 
                                    $totalachagustus=0;
                                    $totalagustus=0; ?>
                                    @foreach ($agustus as $data)
                                      <?php $totalachagustus += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($agustus as $data)
                                      <?php $totalagustus += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totalagustus == 0 && $totalachagustus == 0) ? (0) : (number_format((($totalagustus / $totalachagustus) * 1), 2))  }}%</td>
                                <td scope="col">Desember</td>
                                <td scope="col"><?php 
                                    $totalachdesember=0;
                                    $totaldesember=0; ?>
                                    @foreach ($desember as $data)
                                      <?php $totalachdesember += $data['ach'] ?>
                                    @endforeach
                                    @foreach ($desember as $data)
                                      <?php $totaldesember += $data['weight'] ?>
                                    @endforeach
                                    {{ ($totaldesember == 0 && $totalachdesember == 0) ? (0) : (number_format((($totaldesember / $totalachdesember) * 1), 2))  }}%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                  <br>
                    <div class="form-group row m-2">
                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:15px;"><b>Total</b></label>
                        <div class="col-sm-9" style="">
                            <?php 
                              $totalweight=0;
                            ?>
                              @foreach ($jan as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($februari as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($maret as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($april as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($mei as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($juni as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($juli as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($agustus as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($september as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($oktober as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($november as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach 
                              @foreach ($desember as $data)
                                <?php $totalweight += $data['weight'] ?>
                              @endforeach  
                            <input type="text" name="total" readonly style="font-size:18px;background-color:rgb(254 217 102); width:80px;height:40px;" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $totalweight }}" placeholder=""> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
              <b>Total Weight & Ach</b>
            </div>
            <div class="card-header" >
              <div class="row">
                  <div class="col-md-8">
                      <table id="example" class="table-responsive-xl table-striped table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                          <thead>
                              <tr>
                                  <td colspan="6" style="text-align:center;background-color:rgb(244 175 133);"><b style="font-size: 14px; ">Weight</b></td>
                              </tr>
                          </thead>
                          <tbody style="color:black;font-size: 14px; " class="m-2">
                              <tr>
                                  <td>Januari</td>
                                  <td scope="col">
                                      <?php $total=0; ?>
                                      @foreach ($jan as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}
                                  </td>
                                  <td scope="col">May</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($mei as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">September</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($september as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >Februari</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($februari as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Juni</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($juni as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Oktober</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($oktober as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >Maret</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($maret as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">July</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($juli as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">November</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($november as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >April</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($april as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Agustus</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($agustus as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Desember</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($desember as $data)
                                        <?php $total += $data['weight'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                          </tbody>
                          
                      </table>
                  </div>
                  <div class="col-md-4">
                      <table id="example" class="table-responsive-xl table-striped table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                          <thead>
                              <tr>
                                  <td colspan="6" style="text-align:center;background-color:rgb(244 175 133);"><b style="font-size: 14px; ">Ach</b></td>
                              </tr>
                          </thead>
                          <tbody style="color:black;font-size: 14px; " class="m-2">
                              <tr>
                                  <td >Januari</td>
                                  <td scope="col"> <?php $total=0; ?>
                                      @foreach ($jan as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">May</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($mei as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td  scope="col">September</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($september as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >Februari</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($februari as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td  scope="col">Juni</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($juni as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Oktober</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($oktober as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >Maret</td>
                                  <td  scope="col"><?php $total=0; ?>
                                      @foreach ($maret as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">July</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($juli as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">November</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($november as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                              <tr>
                                  <td >April</td>
                                  <td ><?php $total=0; ?>
                                      @foreach ($april as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Agustus</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($agustus as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                                  <td scope="col">Desember</td>
                                  <td scope="col"><?php $total=0; ?>
                                      @foreach ($desember as $data)
                                        <?php $total += $data['ach'] ?>
                                      @endforeach
                                      {{ $total }}</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div><br>
            </div>
            </div>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group row">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tahun Pencapaian</label>
                    <div class="col-sm-6">
                        <form action="{{ route('list_pkhr') }}">
                        @csrf
                            <select name="tahun_pencapaian" id="tahun_pencapaian" class="form-control form-control-sm" onchange="this.form.submit()">
                                <option value="" selected disabled>-- Pilih Tahun --</option>
                                @php
                                  $year= date('Y');
                                  $min = $year - 60;
                                  $max = $year;
                                  for($i=$max; $i>=$min; $i--){
                                          echo'<option value=".$i."
                                            @if($tahun == $i)
                                            selected
                                            @endif
                                          >{{$i}}</option>';
                                        }
                                @endphp
                            </select>
                           
           
                        </form>	
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group row">
                    <table>
                        <tr>
                            <td>
                                <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_pkhr')}}">Tambah Data Parameter</a></td>
                            <td>
                                <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                <button class="btn btn-danger btn-sm"  style="border-radius:5px; font-size:11px;" href="javascript:;">Print</button>
                            </td>
                        </tr>
                    </table>
                  </div>
            </div>
        </div>
        
        @php $b=0;
        $c=1; @endphp
        @foreach($pkhr as $data)
          <div class="card accordion" id="myAccordion{{ $loop->index }}">
                <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                    <button class="accordion-button collapsed" style="margin: -12px;background-color: rgb(42 117 183); color:white;" data-bs-toggle="collapse" data-bs-target="#collapseThree{{ $loop->index }}"> {{ $data['header'][$b]['nama_parameter'] }}</button>
                </div>
                <div id="collapseThree{{ $loop->index }}" class="accordion-collapse collapse" data-bs-parent="#myAccordion{{ $loop->index }}" style="width:100%;">
                    <div class="card-body">    
                        <table id="example" class="table table-responsive-sm table-striped table-bordered table-sm mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                            <thead style="color:black;font-size: 14px;">
                                <input type="hidden" id="tg_id" name="id_pencapaian_kpi_hr_recruitment"  value="{{ $data['header'][$b]['id_pencapaian_kpi_hr_recruitment']}}" class="form-control form-control-sm" placeholder="">
                                <tr>
                                    <th colspan="11">Parameter: {{ $data['header'][$b]['nama_parameter'] }}</th>
                                </tr>
                                <tr>
                                    <th>KPI</th>
                                    <th scope="col">Ploarization</th>
                                    <th scope="col">UoM</th>
                                    <th scope="col" colspan="2">Definition</th>
                                    <th scope="col">Output</th>
                                    <th scope="col">Target</th>
                                    <th scope="col">Mgr</th>
                                    <th scope="col">Pic</th>
                                    <th scope="col">Staff</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;color:black;">
                            
                                @foreach($data['child'] as $r)
                                    <tr>
                                        <td>{{ $r['nama_kpi'] }}</td>
                                        <td>{{ $r['polarization'] }}</td>
                                        <td>{{ $r['uom'] }}</td>
                                        <td>{{ $r['definition_1'] }}</td>
                                        <td>{{ $r['definition_2'] }}</td>
                                        <td>{{ $r['output'] }}</td>
                                        <td>{{ $r['target'] }}</td>
                                        <td>
                                            <div class="form-check">
                                                <input  type="checkbox" onclick="return false" name="manager" id="flexCheckDefault" {{ $r['manager']== 'active' ? 'checked' : NULL }}>
                                            </div>    
                                        </td>
                                        <td>
                                            <div class="form-check">
                                                <input type="checkbox" name="pic" onclick="return false" id="flexCheckDefault" {{ $r['pic']== 'active' ? 'checked' : NULL }}>
                                            </div>    
                                        </td>
                                        <td>
                                            <div class="form-check">
                                                <input type="checkbox" name="staff" onclick="return false" id="flexCheckDefault" {{ $r['staff']== 'active' ? 'checked' : NULL }}>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot style="text-align:right;">
                                  <tr>
                                  <td colspan="11">
                                      <a href="{{ url('list_pkhr/edit_pkhr') }}/{{ $data['header'][$b]['id_pencapaian_kpi_hr_recruitment'] }}">Ubah</a> |
                                      <a href="{{ url('list_pkhr/detail') }}/{{ $data['header'][$b]['id_pencapaian_kpi_hr_recruitment'] }}">Detail</a> |
                                      <a href="javascript:void(0)" onclick="deleteItem('{{ $data['header'][$b]['id_pencapaian_kpi_hr_recruitment'] }}')" class="">Hapus</a>
                                  </td>
                                  </tr>
                        
                            </tfoot>
                        </table>
                    </div>
                </div>
          </div>
        @php $b++ @endphp
        @endforeach
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush


@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script type="text/javascript">
function deleteItem(id) {
    Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                var actionurl = "{{ URL::to('/list_pkhr/hapus/:id')}}"
                actionurl = actionurl.replace(':id', id);
                
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        data: {
				                	"_token": "{{ csrf_token() }}",
				                },
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_pkhr') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
}
</script>
@endsection