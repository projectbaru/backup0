@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Tambah Pencapaian KPI HR Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">Tambah Pencapaian KPI HR Recruitmen</b>
    </div>
    <div class="card-body">
		<div class="card">
			<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
				<b style="font-size: 17px;">Tahun dan Parameter Pencapaian</b>
			</div>
			<div class="card-body">	
                <form action="{{route('update_pkhr')}}" method="POST" enctype="multipart/form-data"
                    class="pl-3">
                    <input type="hidden" name="jmldetail" id="jmldetail">
                    <input type="hidden" name="header_id" value="{{ $pkhr['header_id'] }}">
                
                    {{ csrf_field() }}
                    <input type="hidden" name="temp_id" id="temp_id" />
                    <div class="form-group" style="width:95%;">
                        <div class="row">
                            <div class="col" style="font-size: 10px;">
                                <div class="form-group row">
                                    <label for="colFormLabelSm"
                                        class="col-sm-3 col-form-label col-form-label-sm"
                                        style="font-size:11px;">Tahun Pencapaian</label>
                                    <div class="col-sm-9">
                                        <input type="text" required name="tahun_pencapaian"
                                            style="font-size:11px;" class="form-control form-control-sm"
                                            id="colFormLabelSm" placeholder=""
                                            value="{{ $pkhr['tahun_pencapaian'] }}"><a style="color:red;">*wajib isi</a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm"
                                        class="col-sm-3 col-form-label col-form-label-sm"
                                        style="font-size:11px;">Parameter</label>
                                    <div class="col-sm-9">
                                        <input type="text" required name="nama_parameter"
                                            style="font-size:11px;" class="form-control form-control-sm"
                                            id="colFormLabelSm" placeholder=""
                                            value="{{ $pkhr['nama_parameter'] }}"><a style="color:red;">*wajib isi</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="pkhr_tambah">
                            @php $xh=1; $xd=1; @endphp
                            <input type="hidden" name="total_child" value="{{ count($pkhr['child']) }}"
                                id="totalChild">
                            @foreach ($pkhr['child'] as $item)
                            @foreach($item['c1'] as $r)
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <div class="card text-black mb-3" style="width:100%;">
                                        <div class="card-header" style="color:white;">
                                            <div class="row" id="responsibilityClone{{$xh}}"
                                                style="border:2px solid black;">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col">
                                                            <p style="margin: -10px;">KPI</p>
                                                        </div>
                                                        <div class="col">
                                                            <div class="" style="text-align:right;">
                                                                <button type="button" style="font-size:11px;margin:-10px;" class="btn  btn-primary btn-sm" id="add_pkhr_all">
                                                                    <i class="fa-solid fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">KPI</label>
                                                                <div class="col-sm-9">
                                                                    <select name="nama_kpi{{$xh}}" id="nama_kpi" required
                                                                        class="form-control form-control-sm">
                                                                        <option value="{{ $r['nama_kpi'] }}">
                                                                            {{ $r['nama_kpi'] }}</option>
                                                                        <option value="A/B Recruitment Cost">A/B Recruitment
                                                                            Cost</option>
                                                                        <option value="A/T Recruitment Rate">A/T
                                                                            Fullfillment Rate</option>
                                                                        <option value="% Training Delivery">% Training
                                                                            Delivery</option>
                                                                        <option value="Training Evaluation">Training
                                                                            Evaluation</option>
                                                                        <option value="Applicant Target">Applicant Target
                                                                        </option>
                                                                        <option value="Reporting Management">Reporting
                                                                            Management</option>
                                                                        <option value="Training Mandays">Training Mandays
                                                                        </option>
                                                                    </select><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Polarization</label>
                                                                <div class="col-sm-9">
                                                                    <select name="polarization{{$xh}}" required
                                                                        id="polarization"
                                                                        class="form-control form-control-sm">
                                                                        <option value="{{ $r['polarization'] }}">
                                                                            {{ $r['polarization'] }}</option>
                                                                        <option value="Lower Better">Lower Better</option>
                                                                        <option value="High Better">High Better</option>
                                                                    </select><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">UoM</label>
                                                                <div class="col-sm-9">
                                                                    <select name="uom{{$xh}}" id="uom"
                                                                        class="form-control form-control-sm" required>
                                                                        <option value="{{ $r['uom'] }}">{{ $r['uom'] }}
                                                                        </option>
                                                                        <option value="percent">Percent</option>
                                                                        <option value="point">Point</option>
                                                                        <option value="hour">Hour</option>
                                                                    </select>
                                                                    <a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Weight</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" name="weight{{ $xh }}"
                                                                        class="form-control form-control-sm" required
                                                                        value="{{ $r['weight'] }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-4 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Definition 1</label>
                                                                <div class="col-sm-7">
                                                                    <textarea type="text" required
                                                                        name="definition{{$xh}}_1" style="font-size:11px;"
                                                                        rows="6" cols="15"
                                                                        class="form-control form-control-sm"
                                                                        id="colFormLabelSm"
                                                                        placeholder="">{{ $r['definition_1'] }}</textarea><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-4 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Definition 2</label>
                                                                <div class="col-sm-7">
                                                                    <textarea type="text" required
                                                                        name="definition{{$xh}}_2" style="font-size:11px;"
                                                                        rows="6" cols="15"
                                                                        class="form-control form-control-sm"
                                                                        id="colFormLabelSm"
                                                                        placeholder="">{{ $r['definition_2'] }}</textarea><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Output</label>
                                                                <div class="col-sm-9">
                                                                    <select name="output{{$xh}}" id="output"
                                                                        class="form-control form-control-sm" required>
                                                                        <option value="{{ $r['output'] }}">
                                                                            {{ $r['output'] }}</option>
                                                                        <option value="Monthly (Jan – Des)">Monthly (Jan –
                                                                            Des)</option>
                                                                        <option value="Quarterly (Jan, Apr, Jul, Oct)">
                                                                            Quarterly (Jan, Apr, Jul, Oct)</option>
                                                                    </select><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm"
                                                                class="col-sm-3 col-form-label col-form-label-sm"
                                                                style="font-size:11px;">Target</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="target{{$xh}}"
                                                                    class="form-control form-control-sm"
                                                                    value="{{ $r['target'] }}">
                                                            </div>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <div class="form-group row">
                                                            <div class="col-sm-9">
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        value="aktif" name="manager{{$xh}}"
                                                                        id="flexCheckDefault" @if (!empty($r['manager']))
                                                                        checked @endif>
                                                                    <label class="form-check-label" for="flexCheckDefault">
                                                                        Mgr
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        value="aktif" name="pic{{$xh}}"
                                                                        id="flexCheckChecked" @if (!empty($r['pic']))
                                                                        checked @endif>
                                                                    <label class="form-check-label" for="flexCheckChecked">
                                                                        PIC
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        value="aktif" name="staff{{$xh}}"
                                                                        id="flexCheckChecked" @if (!empty($r['staff']))
                                                                        checked @endif>
                                                                    <label class="form-check-label" for="flexCheckChecked">
                                                                        Staff
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                
                                                <div class="form-group row" id="descriptionInputContainer{{$xh}}">
                                                @foreach ($item['c2'] as $p)
                                                    <div id="responsibilityClone{{$xh}}{{$xd}}" class="responsibilityClone mt-3 col-sm-12 row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Bulan</label>
                                                                <div class="col-sm-9">
                                                                    <select name="bulan{{$xh}}[]" id="bulan"
                                                                        class="form-control form-control-sm" required>
                                                                        <option value="{{ $p['bulan'] }}">{{ $p['bulan'] }}
                                                                        </option>
                                                                        <option value="Januari">Januari</option>
                                                                        <option value="Februari">Februari</option>
                                                                        <option value="Maret">Maret</option>
                                                                        <option value="April">April</option>
                                                                        <option value="Mei">Mei</option>
                                                                        <option value="Juni">Juni</option>
                                                                        <option value="Juli">Juli</option>
                                                                        <option value="Agustus">Agustus</option>
                                                                        <option value="September">September</option>
                                                                        <option value="Oktober">Oktober</option>
                                                                        <option value="November">November</option>
                                                                        <option value="Desember">Desember</option>
                                                                    </select><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Target</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" required
                                                                        name="target_perbulan{{$xh}}[]"
                                                                        style="font-size:11px;"
                                                                        class="form-control form-control-sm" placeholder=""
                                                                        value="{{ $p['target_perbulan'] }}"><a style="color:red;">*wajib isi</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Weight</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" required name="weightd{{$xh}}[]"
                                                                        style="font-size:11px;"
                                                                        class="form-control form-control-sm" placeholder=""
                                                                        value="{{ $p['weight'] }}"><a style="color:red;">*wajib isi</a>
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Act</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" name="act{{$xh}}[]"
                                                                        style="font-size:11px;"
                                                                        class="form-control form-control-sm" placeholder=""
                                                                        value="{{ $p['act'] }}">
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm"
                                                                    class="col-sm-3 col-form-label col-form-label-sm"
                                                                    style="font-size:11px;">Ach</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" name="ach{{$xh}}[]"
                                                                        style="font-size:11px;"
                                                                        class="form-control form-control-sm" placeholder=""
                                                                        value="{{ $p['ach'] }}">
                                                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            @if ($xd == 1)
                                                            <button type="button" style="font-size:11px;"
                                                                class="btn btn-sm btn-primary add_pkhr" id="add_pkhr"
                                                                data-detailnum="{{$xh}}">
                                                                <i class="fa-solid fa-plus"></i>
                                                            </button>
                                                            @else
                                                            <button type="button" style="font-size:11px;"
                                                                class="btn btn-sm btn-danger removeResponsibilityInputDetail"
                                                                data-header="{{$xh}}" data-number="{{$xd}}">
                                                                <i class="fa-solid fa-minus"></i>
                                                            </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                $xd++;
                                                @endphp
                                                @endforeach
                                            </div>
                                        </div>
                                    
                                        @if ($xh > 1)
                                        <div class="col-sm-1">
                                            <button type="button" style="font-size:11px;"
                                                class="btn btn-sm btn-danger removeResponsibilityInput"
                                                id="removeResponsibilityInput{{$xh}}" data-number="{{$xh}}">
                                                <i class="fa-solid fa-minus"></i>
                                            </button>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                        $xd=1;
                        $xh++;
                        @endphp
                        @endforeach
                        @endforeach
                    </div>
                    <div style="width: 100%;">
                        <table>
                            <tr>
                                <td>
                                    <input type="submit" class="btn btn-danger btn-sm"
                                        style="border:none;border-radius:5px;font-size:11px;" value="Ubah">
                                </td>
                                <td>
                                    <a href="{{ route('list_pkhr') }}" class="btn btn-danger btn-sm"
                                        style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>
                </form>
            
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $("#btn_delete").click(function (e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function (res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href =
                                            "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.pkhr.edit')
@endsection