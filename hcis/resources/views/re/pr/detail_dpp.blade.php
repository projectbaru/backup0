@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot

@slot('title') Recruitment @endslot
@endcomponent
<br>


<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<!-- <div class="page-header-title" >
										<h5 class="m-b-10">Detail Data Personal Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data Personal Pelamar</a></li>
									</ul> -->
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dpp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($dpp as $data)
								<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_data_pelamar }}" />

								<div class="form-group" style="width:95%;">
									<div class="">
										<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
											<p>Tambah Data CV Pelamar</p>
										</div> -->
										<b>Formulir Data Pelamar</b><br><br>
										<p>Data Pribadi</p>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table>
													<div class="form-group row">
														<tr>
															<td>Kode CV Pelamar</td>
															<td>:</td>
															<td><input type="hidden" name="kode_cv_pelamar" value="{{$data->kode_cv_pelamar}}">{{$data->kode_cv_pelamar}}</td>
														</tr>
														<tr>
															<td>Posisi yang dilamar</td>
															<td>:</td>
															<td><input type="hidden" name="posisi_lamaran" value="{{$data->posisi_lamaran}}">{{$data->posisi_lamaran}}</td>
														</tr>   
														<tr>
															<td>Nama Lengkap (KTP)</td>
															<td>:</td>
															<td><input type="hidden" name="nama_lengkap" value="{{$data->nama_lengkap}}">{{$data->nama_lengkap}}</td>
														</tr> 
														<tr>
															<td>Nama Penggilan</td>
															<td>:</td>
															<td><input type="hidden" name="nama_panggilan" value="{{$data->nama_panggilan}}">{{$data->nama_panggilan}}</td>
														</tr>   
														<tr>
															<td>Tempat & tgl lahir</td>
															<td>:</td>
															<td><input type="hidden" name="tempat_lahir" value="{{$data->tempat_lahir}}">{{$data->tempat_lahir}}</td>
														</tr>   
														<tr>
															<td>Agama</td>
															<td>:</td>
															<td><input type="hidden" name="agama" value="{{$data->agama}}">{{$data->agama}}</td>
														</tr>  
														<tr>
															<td>Kebangsaan</td>
															<td>:</td>
															<td>
																<input type="hidden" name="kebangsaan" value="{{$data->kebangsaan}}">{{$data->kebangsaan}}</td>
														</tr>  
														<tr>
															<td>Jenis Kelamin</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" type="radio" name="jenis_kelamin"  value="laki-laki" {{ $data->jenis_kelamin == 'laki-laki' ? 'checked' : NULL }}>
																		Laki-laki&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
																		Perempuan
																</div>
															</td>
														</tr>      
														<tr>
															<td>BB & TB</td>
															<td>:</td>
															<td>
																<div class="row">
																<div class="col-sm-4">
																	<input type="text" readonly required name="berat_badan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" value="{{$data->berat_badan}}">Kg
																</div>
																<div class="col-sm-5">
																	<input type="text" readonly required name="tinggi_badan" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" value="{{$data->tinggi_badan}}">Cm
																</div>
																</div>
															</td>
														</tr> 
														<tr>
															<td>Status Kendaraan</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<div class="form-check">
																			<input class="form-check-input" type="radio" name="status_kendaraan"  value="milik sendiri" {{ $data->status_kendaraan == 'milik sendiri' ? 'checked' : NULL }}>
																			Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="kantor" {{ $data->status_kendaraan == 'kantor' ? 'checked' : NULL }}>
																			Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="orangtua" {{ $data->status_kendaraan == 'orangtua' ? 'checked' : NULL }}>
																			Orangtua&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<input class="form-check-input" type="radio" name="status_kendaraan" value="lainnya" {{ $data->status_kendaraan == 'lainnya' ? 'checked' : NULL }}>
																			Lainnya
																	</div>
																</div>
															</td>
														</tr>   
														<tr>
															<td>Jenis Kendaraan/Merk</td>
															<td>:</td>
															<td><input type="hidden" name="jenis_merk_kendaraan" value="{{$data->jenis_merk_kendaraan}}">{{$data->jenis_merk_kendaraan}}
															</td>
														</tr>   
														<tr>
															<td>Alamat didalam kota [Lengkap]</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_didalam_kota" value="{{$data->alamat_didalam_kota}}">{{$data->alamat_didalam_kota}}
															</td>
														</tr>
														<tr>
															<td>Kode Pos</td>
															<td>:</td>
															<td><input type="hidden" name="kode_pos_1" value="{{$data->kode_pos_1}}">{{$data->kode_pos_1}}
															</td>
														</tr>                                   
													</div>
												</table>
											</div>
											<div class="col" style="font-size: 10px;">
												<table>
													<div class="form-group row">
														<tr>
															<td>No. KTP</td>
															<td>:</td>
															<td><input type="hidden" name="no_ktp" value="{{$data->no_ktp}}">{{$data->no_ktp}}</td>
														</tr>
														<tr>
															<td>Email Pribadi</td>
															<td>:</td>
															<td><input type="hidden" name="email_pribadi" value="{{$data->email_pribadi}}"></td>
														</tr>
														<tr>
															<td>Alamat sosmed pribadi</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_sosmed" value="{{$data->alamat_sosmed}}">{{$data->alamat_sosmed}}</td>
														</tr>        
														<tr>
															<td>SIM yang dimiliki</td>
															<td>:</td>
															<td><input type="hidden" name="kategori_sim" value="{{$data->kategori_sim}}">{{$data->kategori_sim}}&nbsp;&nbsp;&nbsp;&nbsp;No<input type="hidden" name="no_sim" value="{{$data->no_sim}}">{{$data->no_sim}}</td>
														</tr> 
														<tr>
															<td>No. HP</td>
															<td>:</td>
															<td><input type="hidden" name="no_hp" value="{{$data->no_hp}}">{{$data->no_hp}}</td>
														</tr>   
														<tr>
															<td>No. Telp</td>
															<td>:</td>
															<td><input type="hidden" name="no_telepon" value="{{$data->no_telepon}}">{{$data->no_telepon}}</td>
														</tr>
														<tr>
															<td>Tempat Tinggal</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal"  value="milik sendiri" {{ $data->tempat_tinggal == 'milik sendiri' ? 'checked' : NULL }} >Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal" value="ikut orangtua" {{ $data->tempat_tinggal == 'ikut orangtua' ? 'checked' : NULL }}>Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input class="form-check-input" disabled type="radio" name="tempat_tinggal" value="kost/kontrak" {{ $data->tempat_tinggal == 'kost/kontrak' ? 'checked' : NULL }}>Orangtua
																</div>	
															</td>
														</tr>   
														<tr>
															<td>Status Perkawinan</td>
															<td>:</td>
															<td>
																<div class="form-check">
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan"  value="menikah" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}>
																		Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan" value="janda/duda" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}>
																		Janda/Duda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																
																	<input class="form-check-input" disabled type="radio" name="status_perkawinan" value="tidak menikah" {{ $data->status_perkawinan == 'tidak menikah' ? 'checked' : NULL }}>
																		Tidak Menikah
																</div>
															</td>
														</tr>   
														<tr>
															<td>Alamat diluar kota(Lengkap)</td>
															<td>:</td>
															<td><input type="hidden" name="alamat_diluar_kota" value="{{$data->alamat_diluar_kota}}">{{$data->alamat_diluar_kota}}</td>
														</tr>   
														<tr>
															<td>Kode Pos</td>
															<td>:</td>
															<td><input type="hidden" name="kode_pos_2" value="{{$data->kode_pos_2}}">{{$data->kode_pos_2}}</td>
														</tr> 
														<tr>
															<td>Photo</td>
															<td>:</td>
															<td><input type="hidden" name="pas_foto" value="{{$data->pas_foto}}">{{$data->pas_foto}} <a href="" download>download</a></td>
														</tr>                         
													</div>
												</table>
											</div>
										</div>
											


										</div>
										
										
									</div>
									
								</div>
								<hr>
								
								<br>
							@endforeach
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@endsection

    @push('scripts')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    @endpush


@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
     console.log("masuk");
            $("#example").DataTable();
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection