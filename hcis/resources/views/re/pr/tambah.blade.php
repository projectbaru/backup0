@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">


@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent
<div class="card">
	<div class="card-header" style="">
	<b style="font-size: 16px;">Tambah Process Recruitment</b>
	</div>
	<div class="card-body">
		<div class="card">
			<div class="card-header" style="background-color: rgb(47 116 181); color: white;text-align:left;">
				<b style="font-size: 14px;">Pencatatan setiap Step-step process Recruitment si pelamar</b>
			</div>
			<div class="card-body">
				<form action="{{route('simpan_pro')}}" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col">
						<input type="hidden" style="font-size:11px;" name="kode_process_recruitment" id="kode_process_recruitment" class="form-control form-control-sm" value="{{$code_}}" readonly>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">No Dokumen FPTK<a style="color:red;">*</a></label>
								<div class="col-sm-10"> 
									<select name="no_dokumen" id="no_dokumen" required style="font-size:11px;" class="form-control form-control-sm" >
										<option value="" selected>--- Belum Pilih ---</option>
										@foreach ($no_doc['looks'] as $p)
										<option value="{{$p->no_dokumen}}">{{$p->no_dokumen}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode CV Pelamar<a style="color:red;">*</a></label>
								<div class="col-sm-10">
									<select name="kode_cv_pelamar" required style="font-size:11px;" class="form-control form-control-sm" id="kode_cv_pelamar">
										<option value="" selected>--- Belum Pilih ---</option>
										@foreach ($no_cv['looks'] as $cv )
										<option value="{{$cv->kode_cv_pelamar}}" data-email="{{ $cv->email }}" data-no_hp="{{ $cv->no_hp }}" data-nama="{{ $cv->nama_lengkap }}" data-jabatan="{{ $cv->jabatan_lowongan }}">{{$cv->kode_cv_pelamar}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Kandidat</label>
								<div class="col-sm-10">
									<input type="text" style="font-size:11px;" name="nama_kandidat" id="nama_kandidat" class="form-control form-control-sm" readonly>
								</div>
							</div>
						
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jabatan yang dilamar</label>
								<div class="col-sm-10">
									<input type="text" style="font-size:11px;" name="jabatan_dilamar" id="jabatan_dilamar" class="form-control form-control-sm" readonly>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Data Pelamar</label>
								<div class="col-sm-10">
									<select name="kode_data_pelamar" id="kode_data_pelamar" style="font-size:11px;" class="form-control form-control-sm" >
										<option value="" selected>--- Belum Pilih ---</option>
										@foreach ($kode['looks'] as $kdv )
										<option value="{{$kdv->kode_data_pelamar}}">{{$kdv->kode_data_pelamar}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Posisi<a style="color:red;">*</a></label>
								<div class="col-sm-10">
									<select name="nama_posisi" id="nama_posisi" required style="font-size:11px;" class="form-control form-control-sm" >
										<option value="" selected>--- Belum Pilih ---</option>
										@foreach ($nama_posisi['looks'] as $p )
										<option value="{{$p->nama_jabatan}}">{{$p->nama_jabatan}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Status<a style="color:red;">*</a></label>
								<div class="col-sm-10">
									<select name="status" id="status" required style="font-size:11px;" class="form-control form-control-sm " >
										<option value="Recruitment belum selesai" selected>--- Recruitment belum selesai ---</option>
										<option value="Disarankan">Disarankan</option>
										<option value="Dicadangkan">Dicadangkan</option>
										<option value="Tidak disarankan">Tidak disarankan</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								
								<input type="hidden" style="font-size:11px;" name="email" id="email" class="form-control form-control-sm" readonly>
							
						</div>
						<div class="form-group row">
							
								<input type="hidden" style="font-size:11px;" name="no_hp" id="no_hp" class="form-control form-control-sm" readonly>
							
						</div>
						</div>
					</div>
					<hr>
					{{ csrf_field() }}
					<div class="form-group row" id="qualificationInputContainer">
						<table class="table table-bordered">
						<thead>
								<tr>
									<th>No urut step</th>
									<th>Nama step</th>
									<th>Tanggal target</th>
									<th>Tanggal aktual</th>
									<th>Status</th>
									<th>Keterangan</th>
								</tr>
							</thead>
							<tbody id="stepsBody">
								
							</tbody>
						</table>
					</div>
					<input type="hidden" name="jumlah_langkah"  id="rowCountInput" readonly>

					
					<div style="width: 100%;">
						<table>
						<tr>
							<!-- <td>
								<input type="text">
							</td> -->
							<td>
								<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
							</td>
							<td>
								<a href="{{ route('list_process') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
							</td>
						</tr>
						</table>
					</div>
					<br>
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
    $('#nilai_skala').inputmask();

    $('#kode_cv_pelamar').change(function() {
      let nama = $(this).find(':selected').attr('data-nama');
      let jabatan = $(this).find(':selected').attr('data-jabatan');
	  let no_hp = $(this).find(':selected').attr('data-no_hp');
	  let email = $(this).find(':selected').attr('data-email');

      $('#nama_kandidat').val(nama);
      $('#jabatan_dilamar').val(jabatan); 
	  $('#no_hp').val(no_hp); 
	  $('#email').val(email); 
    })
	});
	function getStatusTable(val){
		if(val.value){
			$('select[name=status]').val(val.value).change()
		}else{
			$('select[name=status]').val('Recruitment belum selesai').change()
		}
	}
</script>

<script>
	$(document).ready(function() {
    $('#nama_posisi').change(function() {
        var selectedPosition = $(this).val();

        $.ajax({
            url: '{{ URL::to('/get-steps-by-position') }}', // Adjust the route URL
            method: 'GET',
            data: { nama_posisi: selectedPosition },
            success: function(response) {
                $('#stepsBody').empty(); // Clear existing rows

                response.steps.forEach(function(step, index) {
                    var newRow = '<tr>' +
                                 '<td><input type="text" readonly value="' + (index + 1) + '" name="no_urut' + (index + 1) + '" class="form-control form-control-sm"></td>' +
                                 '<td><input type="text" readonly name="nama_step' + (index + 1) + '" value="' + step.nama_step + '" class="form-control form-control-sm"></td>' +
                                 '<td><input type="date" name="target_tanggal' + (index + 1) + '" class="form-control form-control-sm"></td>' +
                                 '<td><input type="date" name="tanggal_aktual' + (index + 1) + '" class="form-control form-control-sm"></td>' +
                                 '<td><select name="status_step' + (index + 1) + '"  style="font-size:11px;" class="form-control form-control-sm status-step">' +
                                 '<option value="" selected>--- Belum Pilih ---</option>' +
                                 '<option value="Disarankan">Disarankan</option>' +
                                 '<option value="Dicadangkan">Dicadangkan</option>' +
                                 '<option value="Tidak disarankan">Tidak disarankan</option>' +
                                 '</select></td>' +
                                 '<td><input type="text" name="keterangan' + (index + 1) + '" class="form-control form-control-sm"></td>' +
                                 '</tr>';
                    $('#stepsBody').append(newRow);
                });
				var rows = document.querySelectorAll('#stepsBody tr');
                var rowCount = rows.length;

                // Update the input value
                $('#rowCountInput').val(rowCount);
            }
        });
    });
	$(document).on('change', '.status-step', function() {
        var selectedOptionValue = $(this).val();
        var currentIndex = $('.status-step').index(this);

        if (currentIndex === $('.status-step').length - 1) {
            if (selectedOptionValue === '') {
                $('#status').val('Recruitment belum selesai').change();
            } else if (selectedOptionValue === 'Tidak disarankan') {
                $('#status').val('Tidak disarankan').change();
            } else {
                $('#status').val(selectedOptionValue).change();
            }
        } else if (selectedOptionValue === 'Tidak disarankan') {
            $('.status-step:gt(' + currentIndex + ')').val('Tidak disarankan').change();
        }
    });
});



</script>
@include('js.step_recruit.tambah')
@endpush


