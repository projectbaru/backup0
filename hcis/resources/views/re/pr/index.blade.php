@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>

@endpush



@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
	<div class="card-header" style="background-color: rgb(47 116 181); color: white;text-align:center;">
	    <b style="">Pencatatan setiap Step-step process Recruitment si pelamar</b>
	</div>
	<div class="card-body">
        <form action="{{ URL::to('/list_process/hapus_banyak') }}" method="POST" id="form_delete" style="">
            @csrf
            <a href="{{route('tambah_pro')}}" class="btn btn-success">Tambah Jabatan Proses Rekruitment</a>
            <br><br>
            <div class="table-responsive mb-0">
                <table id="tbl_data" class="table table-bordered dt-responsive nowrap" style="text-align:center;">
                    <thead style="color:black;font-size: 12px; ">
                        <tr>
                            <th style="" >No</th>
                            <th style="" >Kode Kandidat</th>
                            <th style="" >No Dokumen FPTK</th>
                            <th style=" " >Kode CV Pelamar</th>
                            <th style=" " >Kode Data Personal Pelamar</th>
                            <th style=" " >Nama Kandidat</th>
                            <th style=" " >Jabatan yang dilamar</th>
                            <th style=" " >Nama Posisi</th>
                            <th style=" " >No HP</th>
                            <th style=" " >Email Pribadi</th>
                            <th style=" " >Action</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 11px;">
                    @php $b=1; @endphp
                        @foreach($rc_prosess as $data)  
                        {{-- {{dd($data)}} --}}
                            <tr>
                                <td>{{$b++;}}</td>
                                <td style="padding-right:-60px;">{{$data->kode_process_recruitment}}</td>
                                <td><a href="{{URL::to('/list_process/d_fptk/'.$data->no_dokumen)}}" target="_blank">{{$data->no_dokumen}}</a></td>
                                <td><a href="{{URL::to('/list_process/d_dcp/'.$data->kode_cv_pelamar)}}" target="_blank">{{$data->kode_cv_pelamar}}</a></td>
                                <td><a href="{{URL::to('/list_process/d_dpp/'.$data->kode_data_pelamar)}}" target="_blank">{{$data->kode_data_pelamar}}</a></td>
                                <td>{{$data->nama_kandidat}}</td>
                                <td>{{$data->jabatan_dilamar}}</td>
                                <td>{{$data->nama_posisi}}</td>
                                <td>{{$data->no_hp}}</td>
                                <td>{{$data->email}}</td>
                                <td>
                                    <!-- <a href="{{ URL::to('/list_process/detail/'.$data->id_process_recruitment) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                    <a href="{{ URL::to('/list_process/detail/'.$data->id_process_recruitment) }}"  class="btn btn-primary btn-sm">Detail</a>
                                    <a href="{{ URL::to('/list_process/edit/'.$data->id_process_recruitment) }}"  class="btn btn-success btn-sm">Edit</a>
                                    <a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;"  href="{{route('hapus_pro', $data->id_process_recruitment)}}" id="btn_delete">Hapus</a>
                                    <!-- <a href="{{ URL::to('/list_process/hapus/'.$data->id_process_recruitment) }}" class="btn btn-danger btn-sm">Hapus</a> -->
                                </td>
                                <!-- <td>
                                    <input type="checkbox" name="multiDelete[]" value="{{ $data->id_process_recruitment }}" id="multiDelete" >
                                </td> -->
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <br>
            
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>

$(document).ready(function() {
        $('#tbl_data').DataTable({
            responsive: true,
        });
    });
    $(function() {
        console.log("masuk");
            $("#example").DataTable();
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
<script type="text/javascript">
    $(document).on('click', '#btn_delete', function(e) {
    e.preventDefault();
    var link = $(this);
    var linkHref = link.attr('href');
    console.log(linkHref);

    Swal.fire({
        title: '<h4>Anda yakin ingin menghapus?</h4>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",
        confirmButtonText: "<i>Ya, hapus!</i>",
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: linkHref,
                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil!",
                            text: "Data telah terhapus.",
                        }).then((result) => {
                            if (result.value) {
                                location.href = "{{ URL::to('list_process') }}";
                            }
                        });
                    }
                },
                error: function(e) {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: "Data tidak terhapus.",
                    });
                },
            });
        }
    });
});
</script>
@endsection