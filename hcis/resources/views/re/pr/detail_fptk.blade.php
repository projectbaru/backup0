
@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="text-align: center;background-color:#70ac48;">
		<b style="color:white;">Detail Formulir Permintaan Tenaga Kerja</b>
	</div>
    <div class="card-body">
		<form action="{{route('simpan_fptk')}}" method="post" enctype="multipart/form-data">
		<hr>
		{{ csrf_field() }}
			@foreach($data_fptk as $data)
			<div class="form-group" style="width:95%;font-size:11px;">
				<input type="hidden" name="tg_id" id="tg_id" value="{{ $data->id_fptk }}" />
				<div class="row">
					<div class="col">
					<table>
						<tr>
							<td>No ISO</td>
							<td>:</td>
							<td></td>
						</tr>
						<tr>
							<td>No.Dokumen</td>
							<td>:</td>
							<td>{{$data->no_dokumen}}</td>
						</tr>
						<tr>
							<td>Tanggal</td>
							<td>:</td>
							<td>{{$data->tanggal_mulai_bekerja}}</td>
						</tr>
					</table>
					</div>
					<div class="col">
						<table>
							<tr>
								<td>Revisi</td>
								<td>:</td>
								<td></td>
							</tr>
							<tr>
								<td>Halaman</td>
								<td>:</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><a href="{{ route('preview_fptk', $data->id_fptk) }}" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Print</a></td>
							</tr>
						</table>
					</div>
				</div>
				<hr>
				
				<div class="">
					<b>Pemohon Tenaga Kerja</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Nama Pemohon</td>
									<td>:</td>
									<td>{{$data->nama_pemohon}}</td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>:</td>
									<td>{{$data->jabatan_pemohon}}</td>
								</tr>
								<tr>
									<td>Departemen</td>
									<td>:</td>
									<td>{{$data->departemen_pemohon}}</td>
								</tr>
							</table>
						</div>
						
					</div>
					<hr>
					<b>Permintaan Tenaga Kerja</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Pengganti Tenaga Kerja</td>
									<td>:</td>
									<td>
										<div class="form-check">
											<input class="form-check-input" onclick="return false" type="checkbox" value="{{$data->penggantian_tenaga_kerja}}" id="flexCheckDefault" {{$data->penggantian_tenaga_kerja="Iya" ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexCheckDefault">
											</label>
										</div>	
									</td>
								</tr>
								<tr>
									<td>Penambahan Tenaga Kerja</td>
									<td>:</td>
									<td>
										<div class="form-check">
											<input class="form-check-input" onclick="return false" type="checkbox" value="{{$data->penambahan_tenaga_kerja}}" id="flexCheckDefault" {{$data->penambahan_tenaga_kerja="Iya" ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexCheckDefault">
											</label>
										</div>	
									</td>
								</tr>
								<tr>
									<td>Header Hunter</td>
									<td>:</td>
									<td>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" onclick="return false" value="{{$data->head_hunter}}" id="flexCheckDefault" {{$data->head_hunter="Iya" ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexCheckDefault">
											</label>
										</div>	
									</td>
								</tr>
							</table>
						</div>
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Percobaan/Tetap</td>
									<td>:</td>
									<td>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" onclick="return false" value="{{$data->status_kepegawaian}}" id="flexCheckDefault" {{$data->head_hunter="Iya" ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexCheckDefault">
											</label>
										</div>	
									</td>
								</tr>
								<tr>
									<td>Kontrak Selama</td>
									<td>:</td>
									<td>
										{{$data->kontrak_selama}}
									</td>
								</tr>
								<tr>
									<td>Magang Selama</td>
									<td>:</td>
									<td>
										{{$data->magang_selama}}
									</td>
								</tr>
							</table>
						</div>
					</div>
					<hr>
					<b>Kebutuhan Tenaga Kerja</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Jabatan yang Dibutuhkan</td>
									<td>:</td>
									<td>{{$data->jabatan_kepegawaian}}</td>
								</tr>
								<tr>
									<td>Tingkat</td>
									<td>:</td>
									<td>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio1" value="staff" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineRadio1" style="font-size:11px;">Staff</label>
											</div>
											<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio2" value="middle" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineRadio2" style="font-size:11px;">Middle</label>
											</div>
											<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio3" value="senior" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineRadio3" style="font-size:11px;">Senior</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Jumlah Kebutuhan</td>
									<td>:</td>
									<td>{{$data->jumlah_kebutuhan}}</td>
								</tr>
								<tr>
									<td>Tanggal Mulai Bekerja</td>
									<td>:</td>
									<td>{{$data->tanggal_mulai_bekerja}}</td>
								</tr>
								<tr>
									<td>Lokasi Kerja</td>
									<td>:</td>
									<td>{{$data->lokasi_kerja}}</td>
								</tr>
							</table>
						</div>
						<div class="col">
							<table>
								<tr>
									<td>Alasan Penambahan Tenaga Kerja</td>
									<td>:</td>
									<td>{{$data->alasan_penambahan_tenaga_kerja}}</td>
								</tr>
							</table>
						</div>
					</div>
					<hr>
					<b>Kualifikasi Tenaga Kerja</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Jenis Kelamin</td>
									<td>:</td>
									<td>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" id="inlineCheckbox1" name="jenis_kelamin" value="pria" {{ $data->jenis_kelamin == 'pria' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineCheckbox1">Pria</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" id="inlineCheckbox2" name="jenis_kelamin" value="wanita" {{ $data->jenis_kelamin == 'wanita' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineCheckbox2">Wanita</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Usia</td>
									<td>:</td>
									<td>{{$data->usia_minimal}} Tahun &nbsp;&nbsp;&nbsp;&nbsp; S/d &nbsp;&nbsp;&nbsp;&nbsp; {{$data->usia_maksimal}} Tahun</td>
								</tr>
								<tr>
									<td>Status</td>
									<td>:</td>
									<td>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" id="inlineCheckbox1" value="lajang" {{ $data->status_kandidat == 'lajang' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineCheckbox1">Lajang</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" onclick="return false" id="inlineCheckbox2" value="menikah" {{ $data->status_kandidat == 'menikah' ? 'checked' : NULL }}>
											<label class="form-check-label" for="inlineCheckbox2">Menikah</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Pendidikan</td>
									<td>:</td>
									<td>
										<div class="form-check">
											<input class="form-check-input" name="pendidikan" type="checkbox" onclick="return false" value="smu/sederajat" id="defaultCheck1" {{ $data->pendidikan == 'smu/sederajat' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck1">
												SMU/Sederajat
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" name="pendidikan" type="checkbox" onclick="return false" value="d1" id="defaultCheck2" {{ $data->pendidikan == 'd1' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck2">
												D1
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" name="pendidikan" type="checkbox" onclick="return false" value="d2" id="defaultCheck2" {{ $data->pendidikan == 'd2' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck2">
												D2
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" onclick="return false" value="d3" id="defaultCheck2" name="pendidikan" {{ $data->pendidikan == 'd3' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck2">
												D3
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" name="pendidikan" value="s1" id="defaultCheck2" {{ $data->pendidikan == 's1' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck2">
												S1
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" name="pendidikan" onclick="return false" value="s2" id="defaultCheck2" {{ $data->pendidikan == 's2' ? 'checked' : NULL }}>
											<label class="form-check-label" for="defaultCheck2">
												S2
											</label>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Pengalaman</td>
									<td>:</td>
									<td>{{$data->pengalaman}}</td>
								</tr><br><br>
								<tr>
									<td>Kemampuan Lainnya</td>
									<td>:</td>
									<td>
										{{$data->kemampuan_lainnya}}
									</td>
								</tr>
							</table>
						</div>
					</div>
					<hr>
					<b>Kopensasi Tenaga Kerja</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<tr>
									<td>Gaji</td>
									<td>:</td>
									<td>
										{{$data->gaji}}
									</td>
								</tr>
								<tr>
									<td>Tunjangan Kesehatan</td>
									<td>:</td>
									<td>{{$data->tunjangan_kesehatan}}</td>
								</tr>
								<tr>
									<td>Tunjangan Transportasi</td>
									<td>:</td>
									<td>{{$data->tunjangan_transportasi}}</td>
								</tr>
								<tr>
									<td>Tunjangan Makan</td>
									<td>:</td>
									<td>{{$data->tunjangan_makan}}</td>
								</tr>
								<tr>
									<td>Tunjangan Komunikasi</td>
									<td>:</td>
									<td>{{$data->tunjangan_komunikasi}}</td>
								</tr>
								<tr>
									<td>Hari Kerja</td>
									<td>:</td>
									<td>{{$data->hari_kerja}}</td>
								</tr>
								<tr>
									<td>Jam Kerja</td>
									<td>:</td>
									<td>{{$data->jam_kerja}}</td>
								</tr>												
							</table>
						</div>
					</div>
					<hr>
					<b>Persetujuan Permintaan Tenaga Kerja</b>
					<div class="row">
						<div class="col">
							<div class="form-group row">
							<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diajukan</label>
							<div class="col-sm-9" style="font-size:11px;">
								<input required readonly class="form-control form-control-sm" value="{{$data->nama_pemohon}}" style="font-size:11px;" type="text" name="nama_pemohon" id="flexCheckDefault">
							</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diperiksa</label>
								<div class="col-sm-9" style="font-size:11px;">
									<input required readonly class="form-control form-control-sm" value="{{$data->nama_atasan_pemohon}}" style="font-size:11px;" type="text" name="nama_atasan_pemohon" id="flexCheckDefault">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Disetujui</label>
								<div class="col-sm-9" style="font-size:11px;">
									<input required readonly class="form-control form-control-sm" value="{{$data->nama_penyetuju}}" style="font-size:11px;" type="text" name="nama_penyetuju" id="flexCheckDefault">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Diperiksa/Dianalisa</label>
								<div class="col-sm-9" style="font-size:11px;">
									<input readonly class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_penganalisa}}" type="text" name="nama_penganalisa" id="flexCheckDefault">
								</div>
							</div>
						</div>
						<div class="col">
							<div class="form-group row">
								<div class="col-sm-10" style="font-size:11px;">
									<input readonly class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_pemohon" value="Default Jabatannya" id="flexCheckDefault">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10" style="font-size:11px;">
									<input readonly class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_atasan_pemohon" value="Default Jabatannya" id="flexCheckDefault">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10" style="font-size:11px;">
									<input readonly class="form-control form-control-sm" value="GM/HD/GM NHD/GM Operasional/Corporate" style="font-size:11px;" type="text" name="nama_penyetuju" id="flexCheckDefault">
								</div>
							</div>
							<div class="form-group row">
							
								<div class="col-sm-10" style="font-size:11px;">
									<input readonly class="form-control form-control-sm" value="HR Dept. Head" style="font-size:11px;" type="text" name="nama_penganalisa" id="flexCheckDefault">
								</div>
							</div>
						</div>
					</div>
					<div style="width: 100%;">
						<table>
						<tr>
							<td>
								<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
							</td>
							<td>
								<button href="{{ route('edit_fptk',$data->id_fptk) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Ubah</a>
							</td>
							<td>
								<a href="{{ route('hapus_fptk',$data->id_fptk) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete">Hapus</a>
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			@endforeach
			<br>
		</form>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.nastable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endpush

