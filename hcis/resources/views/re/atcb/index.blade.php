@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
    button .accordion-button{
        color: white;
    }
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Applicant Target Candidate Buffer @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">List Laporan Applicant Target Candidate Buffer</b>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                <b>Achievement Rate</b>
            </div>
            <?php
            $achQ1Total = 0;
            $achQ2Total = 0;
            $achQ3Total = 0;
            $achQ4Total = 0;
            $totalDataCount = 0;
            ?>
            @foreach($atcb as $data)
                @foreach($data->divisions as $d)
                    @foreach($d->positions as $p)
                        @if(count($p->quarters) == 4)
                            <?php
                            $achQ1Total += $p->quarters[0]->achievement_quarter;
                            $achQ2Total += $p->quarters[1]->achievement_quarter;
                            $achQ3Total += $p->quarters[2]->achievement_quarter;
                            $achQ4Total += $p->quarters[3]->achievement_quarter;
                            $totalDataCount++;
                            ?>
                        @endif
                    @endforeach
                @endforeach
            @endforeach

            <?php
            $averageQ1 = $totalDataCount > 0 ? round($achQ1Total / $totalDataCount, 2) : 0;
            $averageQ2 = $totalDataCount > 0 ? round($achQ2Total / $totalDataCount, 2) : 0;
            $averageQ3 = $totalDataCount > 0 ? round($achQ3Total / $totalDataCount, 2) : 0;
            $averageQ4 = $totalDataCount > 0 ? round($achQ4Total / $totalDataCount, 2) : 0;
            ?>
            <div class="card-body container">
                <div class="row" style="text-align: center;">
                    <div class="col">
                        <button class="btn btn-primary btn-sm">Q1: {{ $averageQ1 }}%</button>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary btn-sm">Q2: {{ $averageQ2 }}%</button>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary btn-sm">Q3: {{ $averageQ3 }}%</button>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary btn-sm">Q4: {{ $averageQ4 }}%</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                <b>Note</b>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>Position</td>
                        <td>:</td>
                        <td>Data vacant position yang mempunyai tingkat ternover tinggi</td>
                    </tr>
                    <tr>
                        <td>Total Vacant 2020</td>
                        <td>:</td>
                        <td>Jumlah karyawan resign tahun 2020</td>
                    </tr>
                    <tr>
                        <td>Target Buffer</td>
                        <td>:</td>
                        <td>Target pencarian kandidat jika vacant (rasio 1:2)</td>
                    </tr>
                    <tr>
                        <td>Target 2021</td>
                        <td>:</td>
                        <td>Jumlah target kandidat yang dicari per kuartal (Target buffer dibagi 4 kuartal)</td>
                    </tr>
                    <tr>
                        <td>Deadline</td>
                        <td>:</td>
                        <td>Pengumpulan kandidat setiap bulan di awal kuartal harus tersedia</td>
                    </tr>
                    <tr>
                        <td>Jumlah Posisi</td>
                        <td>:</td>
                        <td>Jumlah posisi dengan tingkat turnover tertinggi yang ingin ditampilkan</td>
                    </tr>
                </table>
            </div>
        </div>    
            <form action="{{ route('list_at') }}">
                <div class="row">
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-sm-4 col-form-label" style="font-size: 10px;">Tahun Target</label>
                            <div class="col-sm-8">
                                <select name="tahun_target" id="tahun_target" class="form-control form-control-sm" onchange="document.location.href = '/hcis/public/list_at?tahun_target=' + this.value">
                                    <option value="" selected {{ $tahun_target == null ? "selected" : "" }}>-- Semua Tahun --</option>
                                        @php
                                            $year= date('Y');
                                            $min = $year - 60;
                                            $max = $year;
                                            for($i=$max; $i>=$min; $i--){
                                            $selected = $tahun_target == $i ? "selected" : "";
                                            echo'<option value='.$i.' '.$selected.'>'.$i.'</option>';
                                            }
                                        @endphp
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3 row">
                            <label class="col-sm-4 col-form-label" style="font-size: 10px;">Jumlah Posisi</label>
                            <div class="col-sm-8">
                                <select name="jumlah_posisi" id="nama_cabang_kantor" class="form-control form-control-sm">
                                    <option value="" selected>-- Semua Jumlah --</option>
                                    @for($i = 1; $i <= min($total_atcb, 100); $i++)
                                        <option value="{{$i}}" @if($jumlah_posisi_filter == $i) selected @endif>{{$i}}</option>
                                    @endfor
                                </select>	
                            </div>
                        </div> 
                    </div>

                    <div class="col" style="text-align: right;">
                        <!-- <button type="submit" style="border-radius:5px; font-size:11px;" value="Filter" class="btn btn-primary">Filter</button> -->
                        <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_at')}}">Tambah Data Parameter</a></td>

                        <a class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="{{route('preview_at', ['tahun_target' => $tahun_target])}}">Print</a></td>
                    </div>
                </div>
            </form>	
            <br>
            @foreach($atcb as $data)
            <div class="card accordion" id="myAccordion{{ $loop->index }}">
                <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                    <button class="accordion-button collapsed" style="margin: -12px;background-color: rgb(42 117 183); color:white;" data-bs-toggle="collapse" data-bs-target="#collapseThree{{ $loop->index }}">{{$data->nama_cabang_kantor}}</button>
                </div>
                <div id="collapseThree{{ $loop->index }}" class="accordion-collapse collapse" data-bs-parent="#myAccordion{{ $loop->index }}" style="width:100%;">
                    <div class="card-body">
                        <table id="example" class="table table-bordered dt-responsive nowrap table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead style="color:black;font-size: 14px; ">
                                <input type="hidden" id="tg_id" name="id_pencapaian_kpi_hr_recruitment"  value="" class="form-control form-control-sm" placeholder="">
                                <tr>
                                </tr>
                                <tr>
                                    <th scope="col">Division</th>
                                    <th scope="col">Position</th>
                                    <th scope="col">Total Vacant 2020 in Avg/month</th>
                                    <th scope="col">Target Buffer Candidate 2021</th>
                                    <th scope="col">Target 2021 Buffer/Quarter</th>
                                    <th scope="col">Ach Q1</th>
                                    <th scope="col">Ach Q2</th>
                                    <th scope="col">Ach Q3</th>
                                    <th scope="col">Ach Q4</th>
                                </tr>
                            </thead>
                            
                            <tbody style="font-size: 10px;color:black;">  
                                @foreach($data->divisions as $d)   
                                    <tr>
                                        <td rowspan="{{ count($d->positions) }}">{{$d->nama_divisi}}</td>    
                                        @foreach($d->positions as $p) 
                                            @if($loop->index == 0)
                                                <td>{{$p->nama_posisi}}</td>
                                                <td>{{$p->rata_rata_perbulan}}</td>
                                                <td>{{$p->target_buffer_candidate}}</td>
                                                <td>{{$p->target_buffer_quarter}}</td>
                                                @for ($i = 0; $i < 4; $i++)
                                                    <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                                @endfor
                                            @else
                                                <tr>
                                                    <td>{{$p->nama_posisi}}</td>
                                                    <td>{{$p->rata_rata_perbulan}}</td>
                                                    <td>{{$p->target_buffer_candidate}}</td>
                                                    <td>{{$p->target_buffer_quarter}}</td>
                                                    @for ($i = 0; $i < 4; $i++)
                                                        <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                                    @endfor
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach    
                            </tbody>
                            <tfoot  style="text-align:right;">
                                <tr>
                                    <td colspan="11">
                                        <a href="{{ route('edit_at',$data->id_applicant_target) }}" class="btn btn-success btn-sm">Ubah</a> 
                                        <a href="{{ url('list_at/detail',$data->id_applicant_target) }}" class="btn btn-secondary btn-sm">Detail</a> 
                                        <a href="javascript:void(0);" id="btn_delete" onclick="deleteItem('{{ $data->id_applicant_target }}')" class="btn btn-danger btn-sm">Hapus</a>
                                    </td>
                                </tr>
                            </tfoot>                                         
                        </table><br>
                    </div>
                </div>
            </div>
            @endforeach
                    
            
    </div>    
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
@endpush

@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script type="text/javascript">
function deleteItem(id) {
    Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                var actionurl = "{{ URL::to('/list_at/hapus/:id')}}"
                actionurl = actionurl.replace(':id', id);

                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        data: {
				        	"_token": "{{ csrf_token() }}",
				        },
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_at') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
}
</script>
@endsection