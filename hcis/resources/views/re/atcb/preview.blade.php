<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    </style>
</head>

<body>
    <div class="container-fluid" >
    
        <table class="table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 10px; border:1px solid #d9d9d9;color:black;">
            <b>Applicant Target (Candidate Buffer)</b>
            <br>
            <div class="col">
                <button class="btn btn-danger btn-sm no-print " style="border-radius:5px; font-size:10px;" onclick="window.print();">Print</butt>
            </div>
            <b>Data simulasi</b>
            
            <thead style="text-align:center;">
                <tr>
                    <th rowspan="2">Branch</th>
                    <th rowspan="2">Division</th>
                    <th rowspan="2">Position</th>
                    <th rowspan="2">Total Vacant 2020 in Avg/Month</th>
                    <th rowspan="2">Target Buffer Candidate 2021</th>
                    <th rowspan="2">Target Buffer/Quarter</th>
                    <th colspan="3">Q1</th>
                    <th colspan="3">Q2</th>
                    <th colspan="3">Q3</th>
                    <th colspan="3">Q4</th>
                </tr>
                <tr>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                </tr>
            </thead>
            <tbody>
            @foreach($atcb as $data)
                <tr>
                    <td rowspan="{{ $data->total_position }}">{{$data->nama_cabang_kantor}}</td>
                    @foreach($data->divisions as $d) 
                        @if($loop->index == 0)
                            <td rowspan="{{ count($d->positions) }}">{{$d->nama_divisi}}</td>
                            @foreach($d->positions as $p) 
                                @if($loop->index == 0)
                                    <td>{{$p->nama_posisi}}</td>
                                    <td>{{$p->rata_rata_perbulan}}</td>
                                    <td>{{$p->target_buffer_candidate}}</td>
                                    <td>{{$p->target_buffer_quarter}}</td>
                                    @for ($i = 0; $i < 4; $i++)
                                        <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->target_buffer : '-'}}</td>
                                        <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->actual_buffer : '-'}}</td>
                                        <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                    @endfor
                                @else
                                    <tr>
                                        <td>{{$p->nama_posisi}}</td>
                                        <td>{{$p->rata_rata_perbulan}}</td>
                                        <td>{{$p->target_buffer_candidate}}</td>
                                        <td>{{$p->target_buffer_quarter}}</td>
                                        @for ($i = 0; $i < 4; $i++)
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->target_buffer : '-'}}</td>
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->actual_buffer : '-'}}</td>
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                        @endfor
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td rowspan="{{ count($d->positions) }}">{{$d->nama_divisi}}</td>
                                @foreach($d->positions as $p) 
                                    @if($loop->index == 0)
                                        <td>{{$p->nama_posisi}}</td>
                                        <td>{{$p->rata_rata_perbulan}}</td>
                                        <td>{{$p->target_buffer_candidate}}</td>
                                        <td>{{$p->target_buffer_quarter}}</td>
                                        @for ($i = 0; $i < 4; $i++)
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->target_buffer : '-'}}</td>
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->actual_buffer : '-'}}</td>
                                            <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                        @endfor
                                    @else
                                        <tr>
                                            <td>{{$p->nama_posisi}}</td>
                                            <td>{{$p->rata_rata_perbulan}}</td>
                                            <td>{{$p->target_buffer_candidate}}</td>
                                            <td>{{$p->target_buffer_quarter}}</td>
                                            @for ($i = 0; $i < 4; $i++)
                                                <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->target_buffer : '-'}}</td>
                                                <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->actual_buffer : '-'}}</td>
                                                <td>{{count($p->quarters) == 4 ? $p->quarters[$i]->achievement_quarter : '-'}}</td>
                                            @endfor
                                        </tr>
                                    @endif
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                </tr>
                @endforeach 
            </tbody>
            <tfoot style="text-align:center;">
                <tr>
                  <td colspan="8">Achievement Rate</td>
                  <td>%</td>
                  <td colspan="2"></td>
                  <td>%</td>
                  <td colspan="2"></td>
                  <td>%</td>
                  <td colspan="2"></td>
                  <td>%</td>
                </tr>
            </tfoot>
        </table><br>
        
        <br>
        <b>Note</b>
        <table style="font-size: 10px;">
            <tr>
                <td>Position</td>
                <td>:</td>
                <td>Data vacant position yang mempunyai tingkat ternover tinggi</td>
            </tr>
            <tr>
                <td>Total Vacant 2020</td>
                <td>:</td>
                <td>Jumlah karyawan resign tahun 2020</td>
            </tr>
            <tr>
                <td>Target Buffer</td>
                <td>:</td>
                <td>Target pencarian kandidat jika posisi vacant (radio 1:2)</td>
            </tr>
            <tr>
                <td>Target 2021</td>
                <td>:</td>
                <td>Jumlah target kandidat yang dicari per kuartal(Target buffer dibagi 4 kuartal)</td>
            </tr>
            <tr>
                <td>Deadline</td>
                <td>:</td>
                <td>Pengumpulan kandidat setiap bulan di awal kuartal harus tersedia</td>
            </tr>
        </table>
    </div>
</body>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
</html>