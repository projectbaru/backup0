@extends('re.atcb.side')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<div class="pcoded-wrapper container-fluid" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Edit Laporan Applicant Target Candidate Buffer</h5>
									</div>
									<!-- <ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul> -->
								</div>
							</div>
						</div>
					</div>  
                    <form action="">
                        <b>Note</b>
                        <table>
                            <tr>
                                <td>Position</td>
                                <td>:</td>
                                <td>Data vacant position yang mempunyai tingkat turnover tinggi</td>
                            </tr>
                            <tr>
                                <td>Total Vacant 2020</td>
                                <td>:</td>
                                <td>Jumlah karyawan resign tahun 2020</td>
                            </tr>
                            <tr>
                                <td>Target Buffer</td>
                                <td>:</td>
                                <td>Target pencarian kandidat jika posisi vacant(rasio 1:2)</td>
                            </tr>
                            <tr>
                                <td>Target 2021</td>
                                <td>:</td>
                                <td>Jumlah target kandidat yang dicari per kuartal(Target buffer dibagi 4 kuartal)</td>
                            </tr>
                            <tr>
                                <td>Deadline</td>
                                <td>:</td>
                                <td>Pengumpulan kandidat setiap bulan di awal kuartal harus tersedia</td>
                            </tr>
                        </table>
                        <b>Data Wilayah & Tahun Target</b>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun Target</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="tahun_target" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kantor Cabang</label>
                                    <div class="col-sm-9">
                                        <select name="nama_cabang_kantor" required id="nama_cabang_kantor" class="form-control form-control-sm">
                                            <option value="{{$data->nama_cabang_kantor}}"></option>
                                        </select>	
                                    </div>
                                </div>
                            </div>
                        </div>
                        <b>Data Divisi</b>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Divisi</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                    <div class="col-sm-9">
                                        <select name="nama_posisi" required id="nama_posisi" class="form-control form-control-sm">
                                            <option value="{{$data->nama_posisi}}"></option>
                                        </select>	
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Vacant in Avg/month</label>
                                    <div class="col-sm-9">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Avg/month</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer Candidate</label>
                                    <div class="col-sm-9">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer/Quarter</label>
                                    <div class="col-sm-9">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tahun</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                        <input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <table>
                                    <tr>
                                        <th rowspan="2">Q1</th>
                                        <th>Target</th>
                                        <th>Actual</th>
                                        <th>Ach</th>
                                    </tr>
                                    <tr>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                    </tr>
                                    <tr>
                                        <th>Q2</th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                    </tr>
                                    <tr>
                                        <th>Q3</th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                    </tr>
                                    <tr>
                                        <th>Q4</th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                        <th><input type="text" name="nama_divisi" value="{{$data->tahun_target}}" class="form-control form-control-sm" required></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </form>

                    </div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
       
	   <script>
		 $(document).ready(function() {
			 // Select2 Multiple
			 $('.select2-multiple').select2({
				 placeholder: "Lokasi Kerja",
				 allowClear: true
			 });
 
		 });
 
	 </script>
<script>
	function informasi(that) {
    if (that.value == "Lainnya") {
		
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
		}
</script>

@endsection

