@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Applicant Hasil Interview @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">Ubah Laporan Applicant Target Candidate Buffer</b>
    </div>
    <div class="card-body">
		<div class="form-group row" >
			<form action="{{ route('update_atcb', $atcb->id_applicant_target)}}" method="POST" enctype="multipart/form-data" class="pl-3">
			<input type="hidden" name="jmldetail" id="jmldetail">
				@csrf
				@method('PUT')
				<input type="hidden" name="temp_id" id="temp_id" />
				<div class="form-group" style="width:95%;" >
				<div class="card">
            <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                <b>Note</b>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>Position</td>
                        <td>:</td>
                        <td>Data vacant position yang mempunyai tingkat ternover tinggi</td>
                    </tr>
                    <tr>
                        <td>Total Vacant 2020</td>
                        <td>:</td>
                        <td>Jumlah karyawan resign tahun 2020</td>
                    </tr>
                    <tr>
                        <td>Target Buffer</td>
                        <td>:</td>
                        <td>Target pencarian kandidat jika vacant (rasio 1:2)</td>
                    </tr>
                    <tr>
                        <td>Target 2021</td>
                        <td>:</td>
                        <td>Jumlah target kandidat yang dicari per kuartal (Target buffer dibagi 4 kuartal)</td>
                    </tr>
                    <tr>
                        <td>Deadline</td>
                        <td>:</td>
                        <td>Pengumpulan kandidat setiap bulan di awal kuartal harus tersedia</td>
                    </tr>
                    <tr>
                        <td>Jumlah Posisi</td>
                        <td>:</td>
                        <td>Jumlah posisi dengan tingkat turnover tertinggi yang ingin ditampilkan</td>
                    </tr>
                </table>
            </div>
        </div> 
				<div style="width: 100%;">
					<table>
						<tr>
							<td>
								<input type="submit" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;" value="Ubah">
							</td>
							<td>
							<a href="{{ route('list_at') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
							</td>
							<td>
								<a href="{{ route('list_at') }}" class="btn btn-secondary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
							</td>
						</tr>
					</table>
				</div>
					<div class="card">
						<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
							<b style="">Data Wilayah & Tahun Target</b>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align: left;">Tahun Target</label>
								<div class="col-sm-2">
									<input type="text" value="{{ $atcb->tahun_target}}" name="tahun_target" readonly value="" class="form-control form-control-sm" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align: left;">Kantor Cabang</label>
								<div class="col-sm-2">
									<select name="nama_cabang_kantor" required  readonly id="nama_cabang_kantor" class="form-control form-control-sm">
										<option value="{{$atcb->nama_cabang_kantor}}">{{$atcb->nama_cabang_kantor}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div id="pkhr_tambah" class="">
						@php $i = 1; @endphp
						@foreach ($divisi as $item)
						<div class="container-divisi" style="">
							<div class="card">
								<div class="card-header" style="background-color: rgb(42 117 183);color:white;">
									<div class="row">
										<div class="col">
											<b style="">Data Divisi</b>
										</div>
										<div class="col">
											<div class="" style="float:right;">
												@if ($loop->index == 0)
													<button type="button" style="font-size:11px;margin: -10px;" class="btn btn-sm btn-success" id="add_pkhr_all">
														<i class="fa-solid fa-plus"></i>
													</button>
												@else
													<button type="button" style="font-size:11px;margin: -10px;" class="btn btn-sm btn-danger removeResponsibilityInput" id="removeResponsibilityInput{{ $i }}" data-number="{{ $i }}">
														<i class="fa-solid fa-minus"></i>
													</button>
												@endif
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div id="responsibilityClone{{ $i }}">
										<div class="row container-fluid">
											<div class="col-md-12">
												<div class="row">
													<div class="col">
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Nama Divisi</label>
															<div class="col-sm-2">
																<input type="text" name="nama_divisi[]" value="{{$item->nama_divisi}}" class="form-control form-control-sm" required>
															</div>
														</div>
													</div>
												</div>
												<div class="row container-fluid" id="descriptionInputContainer{{ $i }}" >
												@foreach ($item->positions as $p)
													<div id="responsibilityClone{{ $i }}{{ $loop->index + 1 }}" class="row container-fluid container-position{{ $i }}">
														<div class="card">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-4">
																		<div class="form-group row">
																			<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
																			<div class="col-sm-9">
																				<input type="text" name="nama_posisi[{{ $i-1 }}][]" value="{{$p->nama_posisi}}" class="form-control form-control-sm" required>
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Total Vacant</label>
																			<div class="col-sm-4">
																				<input type="number" name="tahun_total_vacant[{{ $i-1 }}][]" value="{{$p->tahun_total_vacant}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
																			</div>
																			<div class="col-sm-4">
																				<input type="number" name="rata_rata_perbulan[{{ $i-1 }}][]" value="{{ $p->rata_rata_perbulan}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Avg/ month</label>
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Target Buffer Candidate</label>
																			<div class="col-sm-4">
																				<input type="number" name="tahun_buffer_candidate[{{ $i-1 }}][]" value="{{$p->tahun_buffer_candidate}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
																			</div>
																			<div class="col-sm-4">
																				<input type="number" name="target_buffer_candidate[{{ $i-1 }}][]" value="{{$p->target_buffer_candidate}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Target Buffer/Quarter</label>
																			<div class="col-sm-4">
																				<input type="number" name="tahun_target_buffer_quarter[{{ $i-1 }}][]" value="{{$p->tahun_buffer_quarter}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
																			</div>
																			<div class="col-sm-4">
																				<input type="number" name="target_buffer_quarter[{{ $i-1 }}][]" value="{{$p->target_buffer_quarter}}" class="form-control form-control-sm" required>
																				<label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-4">
																	</div>
																	<div class="col-md-4">
																		<div class="mb-2" style="float:right;">
																			@if ($loop->index == 0)
																			<button type="button" style="font-size:10px;" class="btn btn-success btn-sm add_pkhr" id="add_pkhr" data-detailnum="{{ $i }}">
																				<i class="fa-solid fa-plus"></i>
																			</button>
																			@else
																			<button type="button" style="font-size:10px;" class="btn btn-sm btn-danger removeResponsibilityInputDetail" data-header="{{ $i }}" data-number="{{ $loop->index + 1 }}">
																				<i class="fa-solid fa-minus"></i>
																			</button>
																			@endif
																		</div>
																		<br>
																		<table class="table table-bordered">
																			<tr>
																				<th></th>
																				<th>Target</th>
																				<th>Actual</th>
																				<th>Ach</th>
																			</tr>
																			@foreach($p->quarters as $q)
																				<tr>
																					<th><input type="text" readonly name="quarter[{{ $i-1 }}][]" class="form-control form-control-sm" value="{{ $q->quarter}}" required></th>
																					<th><input type="number"  name="target_buffer[{{ $i-1 }}][]" class="form-control form-control-sm" value="{{$q->target_buffer}}" required></th>
																					<th><input type="number"  name="actual_buffer[{{ $i-1 }}][]" class="form-control form-control-sm" value="{{$q->actual_buffer}}" required></th>
																					<th><input type="number"  name="achivement_quarter[{{ $i-1 }}][]" class="form-control form-control-sm" value="{{ $q->achievement_quarter}}" required></th>
																				</tr>
																			@endforeach
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@php $i++; @endphp
						@endforeach
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.atcb.edit')





<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	   <script>
		 $(document).ready(function() {
			 // Select2 Multiple
			 $('.select2-multiple').select2({
				 placeholder: "Lokasi Kerja",
				 allowClear: true
			 });

        });

        function myFunction(e) {
            document.getElementById("myText").value = e.target.value
        }
	 </script>
<script>
	function informasi(that) {
    if (that.value == "Lainnya") {

				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
		}
</script>
<script>
	$(document).ready(function(){
   $("#file_cv").change(function(){
     fileobj = document.getElementById('file_cv').files[0];
     var fname = fileobj.name;
     var ext = fname.split(".").pop().toLowerCase();
     if(ext == "pdf" || ext == "jpeg" || ext == "png" || ext == "jpg"){
        $("#info_img_file").html(fname);
     }else{
        alert("Hanya untuk file pdf, jpg, jpeg dan png saja..");
        $("#file_cv").val("");
        $("#info_img_file").html("Tidak ada file yag dimasukkan");
        return false;
     }
   });
//    $("#audio_file").change(function(){
//      fileobj = document.getElementById('audio_file').files[0];
//      var fname = fileobj.name;
//      var ext = fname.split(".").pop().toLowerCase();
//      if(ext == "mp3" || ext == "mp4" || ext == "wav"){
//         $("#info_audio_file").html(fname);
//      }else{
//         alert("Accepted file mp3, mp4 and wav only..");
//         $("#aud_file").val("");
//         $("#info_audio_file").html("No file selected");
//         return false;
//      }
//    });
   $("#btn_submit").click(function(){
     var img_file = $('#file_cv').val();

     if(img_file =="" || aud_file ==""){
         alert('Please select the file');
        return false;
     }
   });
});
</script>
<script>
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script>
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endpush
