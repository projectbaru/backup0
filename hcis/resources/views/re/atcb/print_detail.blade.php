<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>
<body>
    <br>
    <div class="container">
        <b>Applicant Target (Candidate Buffer)</b><br><br>
        <b>Data simulasi</b><br>
        <table class="table table-bordered" style="text-align:center;font-size:8px;">
            <thead>
                <tr>
                    <th rowspan="2">Branch</th>
                    <th rowspan="2">Division</th>
                    <th rowspan="2">Position</th>
                    <th rowspan="2">Total Vacant 2020 in Avg/month</th>
                    <th rowspan="2">Target Buffer Candidate 2021</th>
                    <th rowspan="2">Target 2021 Buffer/Quarter</th>
                    <th colspan="3">Q1</th>
                    <th colspan="3">Q2</th>
                    <th colspan="3">Q3</th>
                    <th colspan="3">Q4</th>
                </tr>
                <tr>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                    <th>Target Buffer</th>
                    <th>Actual Buffer</th>
                    <th>Ach</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">Achievement Rate</td>
                    <td></td>
                    <td colspan="2"></td>
                    <td></td>
                    <td colspan="2"></td>
                    <td></td>
                    <td colspan="2"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        <div>
           <b>Note</b>
           <table>
            <tbody style="font-size:10px;">
                <tr>
                    <td>Position</td>
                    <td>:</td>
                    <td>Data vacant position yang mempunyai tingkat turnover tinggi</td>
                </tr>
                <tr>
                    <td>Total Vacant 2020</td>
                    <td>:</td>
                    <td>Jumlah karyawan resign tahun 2020</td>
                </tr>
                <tr>
                    <td>Target Buffer</td>
                    <td>:</td>
                    <td>Target p karyawan resign tahun 2020</td>
                </tr>
                <tr>
                    <td>Target 2021</td>
                    <td>:</td>
                    <td>Jumlah target kandidat yang dicari per kuartal (Target buffer dibagi 4 kuartal)</td>
                </tr>
                <tr>
                    <td>Deadline</td>
                    <td>:</td>
                    <td>Pengumpulan kandidat setiap bulan di awal kuartal harus tersedia</td>
                </tr>
            </tbody>
           </table>
        </div>
    </div>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>