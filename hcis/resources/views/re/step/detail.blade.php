@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent    
		<form action="{{route('update_step')}}" method="post" enctype="multipart/form-data">
			<input type="hidden" name="id_step_process_recruitment" value="{{$data->id_step_process_recruitment}}">
			<hr>
			{{ csrf_field() }}
			<div class="form-group row">
				<label for="colFormLabelSm" style="" class="col-sm-2 col-form-label col-form-label-sm">Nama Jabatan</label>
				<div class="col-sm-8">
					<input value="{{$data->nama_jabatan}}" disabled type="text" style="" name="nama_jabatan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
				</div>
			</div>
			
			@php
				$i = 0;
			@endphp					
			<div class="form-group row" id="qualificationInputContainer">
				@foreach ($detail_step as $detail)
					<div id="qualificationClone{{ $loop->index + 1 }}" class="qualificationClone mt-3 col-sm-12 row">		
						<input type="hidden" name="id_detail_step_process_recruitment[]" value="{{$detail->id_detail_step_process_recruitment}}">
						<label for="colFormLabelSm" style="" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
						<div class="col-sm-2">
							<input type="text" disabled style="" name="nama_step[]" value="{{$detail->nama_step}}" class="form-control form-control-sm" required>
						</div>
						<label for="colFormLabelSm" style="" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
						<div class="col-sm-2">
							<input type="number" style="" name="no_urut[]" value="{{$detail->no_urut}}" class="form-control form-control-sm" required readonly>
						</div>
						<label for="colFormLabelSm" style="" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
						<div class="col-sm-2">
							<textarea type="text" disabled readonly style="" name="keterangan[]" class="form-control form-control-sm" required>{{$detail->keterangan}}</textarea>
						</div>
						<div class="col-sm-1">
							@if ($loop->index == 0)
							<button type="button" style="" disabled class="btn btn-sm btn-primary" id="addQualificationInput">
								<i class="fa-solid fa-plus"></i>
							</button>
							@else
								<button type="button" disabled style="" class="btn btn-sm btn-danger removeQualificationInput" data-id ="{{$detail->id_detail_step_process_recruitment}}" data-number="{{ $loop->index + 1 }}">
							<i class="fa-solid fa-minus"></i>
						</button>
							@endif
						</div>
					</div>
					@php
						$i++;
					@endphp
				@endforeach
			</div>
			<hr>
			<div style="width: 100%;">
				<table>
					<tr>
						<!-- <td>
							<input type="text">
						</td> -->
						<td>
							<a class="btn btn-success btn-sm " style="border-radius:5px;font-size:11px;" href="{{route('ubah_step', $data->id_step_process_recruitment)}}">Ubah</a>
						</td>
						<td>
							<a class="btn btn-danger btn-sm " style="border-radius:5px;font-size:11px;" href="{{route('hapus_step', $data->id_step_process_recruitment)}}" id="btn_delete">Hapus</a>
						</td>
						<td>
							<a href="{{route('list_step')}}" class="btn btn-danger btn-sm" style="border-radius:5px;font-size:11px;">Batal</a>
						</td>
						

					</tr>
				</table>
			</div>
			<br>
		</form>
		@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_step') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.step_recruit.edit')

@endsection
