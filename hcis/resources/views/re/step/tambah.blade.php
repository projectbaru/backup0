@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush
@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent      
<form action="{{route('simpan_step')}}" method="post" enctype="multipart/form-data">
	<div class="form-group row">
		<label for="colFormLabelSm" style="font-size:11px;text-align:left;" class="col-sm-2 col-form-label col-form-label-sm">Nama Jabatan</label>
		<div class="col-sm-10">
			<input type="text" style="font-size:11px;" required name="nama_jabatan" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
		</div>
	</div>
	<hr>
	{{ csrf_field() }}
	<div class="form-group row" id="qualificationInputContainer" style="text-align:left;">
		<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
		<div class="col-sm-2">
			<input type="text" style="font-size:11px;" name="nama_step[]" class="form-control form-control-sm" required>
		</div>
		<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
		<div class="col-sm-2">
			<input type="number" style="font-size:11px;" name="no_urut[]" class="form-control form-control-sm" required value="1" readonly>
		</div>
		<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
		<div class="col-sm-2">
			<textarea type="text" style="font-size:11px;" name="keterangan[]" class="form-control form-control-sm" required></textarea>
		</div>
		<div class="col-sm-1">
			<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
				<i class="fa-solid fa-plus"></i>
			</button>
		</div>
	</div>
	<hr>
	<div style="width: 100%;">
		<table>
		<tr>
			<!-- <td>
				<input type="text">
			</td> -->
			<td>
				<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
				<a href="{{ route('list_step') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
		</tr>
		</table>
	</div>
	<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@include('js.step_recruit.tambah')

@endpush


