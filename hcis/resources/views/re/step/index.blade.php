@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent      
<div class="row" >
    <div class="col-xl-12">
        <div class="card" style="border-radius:8px;border:none;">
            <div class="card-header" style="background-color: rgb(47 116 181);color:white;">
                <!-- <h5>Per Table</h5> -->
                <b style="color:white;">Step-step Process Rekruitment Master</b>
            </div>
            <div class="card-body ">
                <div class="card" style="border-radius:8px;border:none;">
                    <div class="card-header" >
                    <a href="{{route('tambah_step')}}"  class="btn btn-sm btn-success" style="color:white;">Tambah Jabatan Proses Rekruitment</a>
                    </div>
                    <div class="card-body  ">
                    
                        <div class="" style="overflow-x:auto">
                        <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                            <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->

                            @php $b=1; @endphp
                            @foreach ($step as $data)
                            <div style="border:1px solid #eaf0f9;">
                                <form action="{{ URL::to('/list_step/hapus_banyak') }}" method="POST" id="form_delete" style="margin:10px;">
                                @csrf
                                    <input type="hidden" name="id" value="{{$data->id_step_process_recruitment}}" class="form-control form-control-sm">
                                    <b>Nama Jabatan: {{$data->nama_jabatan}}</b>
                                    <hr>
                                    <table id="example" class="table table-sm table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        
                                        <thead style="color:black;font-size: 12px; ">
                                            <tr>
                                                <th style="">Urutan</th>
                                                <th style="" scope="col">Nama Step</th>
                                                <th style="" scope="col">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 11px;">
                                            @foreach ($data->details as $detail)
                                                <tr>
                                                    <td style="padding-right:-60px;">{{$detail->no_urut}}</td>
                                                    <td style="padding-right:-60px;">{{$detail->nama_step}}</td>
                                                    <td style="padding-right:-60px;">{{$detail->keterangan}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                                    <table>
                                        <tr>
                                            <td>
                                                <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('ubah_step', $data->id_step_process_recruitment)}}">Ubah</a>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('detail_step', $data->id_step_process_recruitment)}}">Detail</a>
                                            </td>
                                            <td>
                                                <a id="btn_delete" href="{{route('hapus_step', $data->id_step_process_recruitment)}}" class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;">Hapus</a>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <br>
                            <br>
                            

                            

                            
                            @endforeach
                            
                        <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        <br>
                    
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).on('click', '#btn_delete', function(e) {
    e.preventDefault();
    var link = $(this);
    var linkHref = link.attr('href');
    console.log(linkHref);

    Swal.fire({
        title: '<h4>Anda yakin ingin menghapus?</h4>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",
        confirmButtonText: "<i>Ya, hapus!</i>",
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: linkHref,
                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil!",
                            text: "Data telah terhapus.",
                        }).then((result) => {
                            if (result.value) {
                                location.href = "{{ URL::to('list_step') }}";
                            }
                        });
                    }
                },
                error: function(e) {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: "Data tidak terhapus.",
                    });
                },
            });
        }
    });
});
</script>
@endsection