@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb60SsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Transaksi PTK @endslot
@slot('title') Recruitment @endslot
@endcomponent 
						<div class="form-group row" >
							<form action="{{route('update_of')}}" method="POST" enctype="multipart/form-data">
								<hr>
								@foreach($of as $data)
								{{ csrf_field() }}
								<input type="hidden" name="id_ontime_fulfillment" id="temp_id" readonly value="{{$data->id_ontime_fulfillment}}" />
								<div class="form-group" style="width:95%;" >
									<b>Tambah Report Ontime Fulfillment Report</b><br><br>
									<p>Data Pribadi</p>
									<hr>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No FPTK</label>
												<div class="col-sm-9">
													<input type="text" required name="nomor_dokumen_fptk" readonly value="{{$data->nomor_dokumen_fptk}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pemohon</label>
												<div class="col-sm-9">
													<input type="text" required name="nama_pemohon" readonly value="{{$data->nama_pemohon}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan Yang Dibutuhkan</label>
												<div class="col-sm-9">
													<input type="text" required name="jabatan_pemohon" readonly value="{{$data->jabatan_pemohon}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
										</div>
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Divisi</label>
												<div class="col-sm-9">
													<input type="text" required name="departemen_pemohon" style="font-size:11px;" readonly value="{{$data->departemen_pemohon}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pemohon</label>
												<div class="col-sm-9">
													<input type="text" required name="nama_pemohon" style="font-size:11px;" readonly value="{{$data->nama_pemohon}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan Yang Dibutuhkan</label>
												<div class="col-sm-9">
													<input type="text" required name="jabatan_pemohon" readonly value="{{$data->jabatan_pemohon}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
										</div>
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Permintaan</label>
												<div class="col-sm-9">
													<input type="text" required name="jumlah_permintaan" style="font-size:11px;" readonly value="{{$data->jumlah_permintaan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Karyawan Masuk</label>
												<div class="col-sm-9">
													<input type="text" required name="karyawan_masuk" readonly value="{{$data->karyawan_masuk}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Pemenuhan</label>
												<div class="col-sm-9">
													<input type="text" required name="jumlah_pemenuhan" readonly value="{{$data->jumlah_pemenuhan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kekurangan</label>
												<div class="col-sm-9">
													<input type="text" required name="kekurangan" readonly value="{{$data->kekurangan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>	
										</div>										
									</div>
									<hr>
									<div id="pkhr_tambah">
										<div class="row">
											<div class="col-md-10">
												<b>Tanggal Report</b>
												<div class="row">
													<div class="col">
													<table  class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%;">
														<thead style="font-size: 10px; ">
															<tr>
																<th scope="col">Tgl User Input FPTK</th>
																<th scope="col">Tgl Approval PIC</th>
																<th scope="col">Tgl Approval Atasan</th>
																<th scope="col">Tgl Approval GM</th>
																<th scope="col">Tgl Masuk FPTK ke HRD</th>
																<th scope="col">Tgl Masuk FPTK ke Pimpinan</th>
															</tr>
														</thead>
														<tbody style="font-size: 12px;color:black;">
															<tr>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tgl_user_input_fptk" readonly value="{{date('d-m-Y', strtotime($data->tgl_user_input_fptk))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tgl_approval_pic_fptk" readonly value="{{date('d-m-Y', strtotime($data->tgl_approval_pic_fptk))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tgl_atasan_pic_fptk" readonly value="{{date('d-m-Y', strtotime($data->tgl_atasan_pic_fptk))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tgl_approval_gm_fptk" readonly value="{{date('d-m-Y', strtotime($data->tgl_approval_gm_fptk))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tanggal_masuk_fptk_hrd" readonly value="{{date('d-m-Y', strtotime($data->tanggal_masuk_fptk_hrd))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
																<td style="font-size: 10px;color:black;"><input type="date" required name="tanggal_masuk_fptk_pimpinan" readonly value="{{date('d-m-Y', strtotime($data->tanggal_masuk_fptk_pimpinan))}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></td>
															</tr>
														</tbody>
													</table><br>
													</div>
												</div>		
											</div>
										</div>
										<hr>
										<div class="row" style="">
											<div class="col-md-10">
												<b>Tanggal Waktu & Status</b>
												
												<div class="row">
													<div class="col">
													<table id="" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
                                                    <thead style="color:black;font-size: 10px; ">
                                                        <tr>
                                                            <th scope="col">Leadtime (Days)</th>
                                                            <th scope="col">Leadtime UN (Days)</th>
                                                            <th scope="col">Target Pemenuhan</th>
                                                            <th scope="col">Tgl Masuk Karyawan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="font-size: 12px;color:black;">
                                                   
                                                        <tr>
                                                            <td style="font-size: 10px;color:black;"><input type="number" required name="waktu_pengerjaan" readonly value="{{$data->waktu_pengerjaan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> </td>
                                                            <td style="font-size: 10px;color:black;"><input type="number" required name="lead_time_un" readonly value="{{$data->lead_time_un}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> </td>
                                                            <td style="font-size: 10px;color:black;"><input type="date" required name="tanggal_target_pemenuhan" readonly value="{{$data->tanggal_target_pemenuhan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> </td>
															<td style="font-size: 10px;color:black;"><input type="date" required name="tanggal_masuk_karyawan" readonly value="{{$data->tanggal_masuk_karyawan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> </td>
                                                        </tr>
                                                    </tbody>
                                                </table><br>
													</div>
												</div>
														
											</div>
											<div class="col-md-2" style="border:1px solid #d9d9d9;text-align:center;">
												<b> Status Ach</b>
												<textarea name="status_ach" readonly style="width:100%;">{{$data->status_ach}}</textarea>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Karyawan</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_karyawan" readonly value="{{$data->nama_karyawan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Referensi</label>
													<div class="col-sm-9">
														<input type="text" required name="referensi" readonly value="{{$data->referensi}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
											</div>
											<div class="col">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No RQ</label>
													<div class="col-sm-9">
														<input type="text" required name="no_rq"  readonly value="{{$data->no_rq}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
													<div class="col-sm-9">
														<input type="text" required name="keterangan" readonly value="{{$data->keterangan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div style="width: 100%;">
									<table>
										<tr>
											<td>
												<button type="submit" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
											</td>
											<td>
												<a href="{{ route('list_of',$data->id_ontime_fulfillment) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
											</td>
											<td>
												<a href="{{ route('list_of') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>
										</tr>
									</table>
								</div>
								<br>
								@endforeach
							</form>
						</div>
						@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_of') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.pkhr.tambah')
@endsection

