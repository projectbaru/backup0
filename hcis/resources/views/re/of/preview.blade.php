<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <style>
    </style>
</head>
<body>
    <br>
    <div class="container" style="font-size:8px;text-align:center;">
    <button type="button" class="print-button" style="margin-bottom: 20px;font-size:10px;" onclick="window.print()">Print</button>
    <table>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table table-bordered table-sm">
        <thead>
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">No. FPTK</th>
                <th rowspan="2">Pemohon</th>
                <th rowspan="2">Jabatan yang dibutuhkan</th>
                <th rowspan="2">Div</th>
                <th rowspan="2">Level</th>
                <th rowspan="2">Lokasi</th>
                <th rowspan="2">Tgl User Input FPTK</th>
                <th rowspan="2">Tgl Approval PIC</th>
                <th rowspan="2">Tgl Approval Atasan</th>
                <th rowspan="2">Tgl Approval GM</th>
                <th rowspan="2">Tgl Masuk FPTK ke HRD</th>
                <th rowspan="2">Tgl Masuk FPTK ke Pimpinan</th>
                <th rowspan="2">Jumlah Permintaan</th>
                <th rowspan="2">Karyawan Masuk</th>
                <th rowspan="2">Jumlah Pemenuhan</th>
                <th rowspan="2">Kekurangan</th>
                <th colspan="2">Status</th>
                <th rowspan="2">Leadtime (Days)</th>
                <th rowspan="2">Leadtime UN (Days)</th>
                <th rowspan="2">Target Pemenuhan (Date)</th>
                <th rowspan="2">Tgl masuk karyawan</th>
                <th rowspan="2">Waktu yang diperlukan (hari)</th>
                <th rowspan="2">Nama karyawan yang diterima</th>
                <th rowspan="2">Referensi</th>
                <th rowspan="2">No RQ</th>
                <th rowspan="2">Keterangan</th>
                <th rowspan="2">Status Ach</th>
            </tr>
            <tr>
                <th>Ganti</th>
                <th>Tambah</th>
            </tr>
        </thead>
        <tbody>
            @php $b=1; @endphp
            @foreach($of as $data)
            <tr>
                <td>{{$b++;}}</td>
                <td>{{$data->nomor_dokumen_fptk}}</td>
                <td>{{$data->nama_pemohon}}</td>
                <td>{{$data->jabatan_pemohon}}</td>
                <td>{{$data->departemen_pemohon}}</td>
                <td>{{$data->jabatan_pemohon}}</td>
                <td>{{$data->lokasi}}</td>
                <td>{{$data->tgl_user_input_fptk}}</td>
                <td>{{$data->tgl_approval_pic_fptk}}</td>
                <td>{{$data->tgl_atasan_pic_fptk}}</td>
                <td>{{$data->tgl_approval_gm_fptk}}</td>
                <td>{{$data->tanggal_masuk_fptk_hrd}}</td>
                <td>{{$data->tanggal_masuk_fptk_pimpinan}}</td>
                <td>{{$data->jumlah_permintaan}}</td>
                <td>{{$data->karyawan_masuk}}</td>
                <td>{{$data->jumlah_pemenuhan}}</td>
                <td>{{$data->kekurangan}}</td>
                <td>
                    @if($data->penggantian_tenaga_kerja === 'ganti')
                    V
                    @else
                    @endif
                </td>
                <td>
                    @if($data->penggantian_tenaga_kerja === 'tambah')
                    V
                    @else
                    @endif
                </td>
                <td>{{$data->waktu_pengerjaan}}</td>
                <td>{{$data->lead_time_un}}</td>
                <td>{{$data->tanggal_target_pemenuhan}}</td>
                <td>{{$data->tanggal_masuk_karyawan}}</td>
                <td>{{$data->waktu_pengerjaan}}</td>
                <td>{{$data->nama_karyawan}}</td>
                <td>{{$data->referensi}}</td>
                <td>{{$data->no_rq}}</td>
                <td>{{$data->keterangan}}</td>
                <td>{{$data->status_ach}}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            
                <tr>
                    <td colspan="12"></td>
                    <td>Total :</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="12"></td>
                    <td>Persentasi :</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            
       
        <tr>
            <td colspan="23">
                Faktor-faktor belum tercapai: 
                1. Pelamar yang masuk ke jobstreet cukup banyak
                2. Pelamar yang masuk ke jobstreet cukup banyak
                3. Tes yang masuk ke jobstreet cukup banyak
                4. Interview 
            </td>
            <td></td>
            <td></td>
        </tr>
        </tfoot>
      
        
    </table>
    <div style="height:400px; width:900px; margin-auto;">
        <canvas id="barChart"></canvas>
    </div>
    </div>
</body>
</html>

<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>

@section('add-scripts')
<script>
    $(function() {
        var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Permintaan", "Karyawan Masuk", "Kekurangan"],
				datasets: [{
					label: '# of Votes',
					data: [ {{$jumlah_permintaan}} , {{$karyawan_masuk}} , {{$kekurangan}} ],
					backgroundColor: [
					'rgba(0, 255, 0, 0.2)',
					'rgba(255, 255, 0, 0.2)',
					'rgba(255, 0, 0, 0.2)',
				
					],
					borderColor: [
					'rgba(0, 255, 0, 1)',
					'rgba(255, 255, 0, 1)',
					'rgba(255, 0, 0, 1)',
					
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection