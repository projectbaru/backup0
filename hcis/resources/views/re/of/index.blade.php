@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
        margin-left: 0.5em;
        display: inline-block;
        width: auto;
    }
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Ontime Fullfilment @endslot
@slot('title') Recruitment @endslot
@endcomponent 

<div class="row" >
    <div class="col-xl-12">
        <div class="card" style="border-radius:8px;border:none;">
            
            <div class="card-body table-border-style ">
                <div class="table " style="">
                    <form action="{{ route('filter_dua_dcp')}}" method="POST">
                        @csrf
                        <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                            <!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                                <p>DATA LOWONGAN KERJA</p>
                            </div> -->
                                <div class="row container" >
                                    <div class="col-md-6" style=""><form action="{{route('simpan_lo')}}" method="POST" >
                                        <div class="row m-2" style="border:2px solid #f3f3f3;width:203%;">
                                            <div class="col-md-5 m-2" style="font-size: 10px;">
                                            <form action="{{ route('view_of') }}" method="get" id="form-filter-of">
                                                <div class="form-group row">
                                                    <label for="staticEmail" class="col-sm-2 col-form-label">Tampilkan</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Tahun</label>
                                                    <div class="col-sm-10">
                                                        <select name="tahun" id="tahun" class="form-control form-control-sm">
                                                            <option value="" disabled>-- Pilih Tahun --</option>
                                                            @php
                                                                $year= date('Y');
                                                                $min = $year - 60;
                                                                $max = $year;
                                                                for($i=$max; $i>=$min; $i--){
                                                                echo'<option value='.$i.'>'.$i.'</option>';
                                                                }
                                                            @endphp
                                                        </select>	
                                                        <!-- <input type="password" class="form-control" id="inputPassword" placeholder="Password"> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Bulan</label>
                                                    <div class="col-sm-10">
                                                        <select name="bulan" id="bulan" style="" class="form-control form-control-sm">
                                                            <option value="">ALL</option>
                                                            <option value="Januari" >Januari</option>
                                                            <option value="Februari" >Februari</option>
                                                            <option value="Maret" >Maret</option>
                                                            <option value="April" >April</option>
                                                            <option value="Mei" >Mei</option>
                                                            <option value="Juni" >Juni</option>
                                                            <option value="Juli" >Juli</option>
                                                            <option value="Agustus" >Agustus</option>
                                                            <option value="September" >September</option>
                                                            <option value="Oktober" >Oktober</option>
                                                            <option value="November">November</option>
                                                            <option value="Desember">Desember</option>
                                                        </select>	
                                                        <!-- <input type="password" class="form-control" id="inputPassword" placeholder="Password"> -->
                                                    </div>
                                                </div>
                                                <div class="container" style="text-align:center;width:20%;">
                                                    <button form="form-filter-of" type="submit" class="btn btn-success btn-sm btn-block" style="font-size:13px;color:white;">
                                                        Cari data
                                                    </button>
                                                </div>
                                            </form>
                                            </div>
                                            <div class="col-3 col-sm-3">
                                                <div style="width: 500px;height: 250px">
                                                    <canvas id="myChart"></canvas>
                                                </div>
                                            </div> 
                                        </div>                                                                                                                                                                 
                                    </form>        
                                </div>                                                                      
                        </div>                                          
                    </form>
                </div>
                <div class="table-responsive-xl">
                <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                    <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->                                        
                    <div>
                        <form action="{{ URL::to('/list_of/hapus_banyak') }}" method="POST" id="form_delete" style="margin:30px;overflow:auto;" >
                            @csrf
                            <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead style="color:black;font-size: 12px; ">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th style="" rowspan="2">No. FPTK</th>
                                        <th style="" rowspan="2">Nama Pemohon</th>
                                        <th style="" rowspan="2" scope="col">Jabatan yang Dibutuhkan</th>
                                        <th style="" rowspan="2" scope="col">Div</th>
                                        <th style="" rowspan="2" scope="col">Level</th>
                                        <th style="" rowspan="2" scope="col">Lokasi</th>
                                        <th style="" rowspan="2" scope="col">Tgl User Input FPTK</th>
                                        <th style="" rowspan="2" scope="col">Tgl Masuk FPTK ke Pimpinan</th>
                                        <th style="" rowspan="2" scope="col">Jumlah Permintaan</th>
                                        <th style="" rowspan="2" scope="col">Karyawan Masuk</th>
                                        <th style="" rowspan="2" scope="col">Kekurangan</th>
                                        <th style="" scope="col" colspan="2">Status</th>
                                        <th style="" rowspan="2" scope="col">Target Pemenuhan (Date)</th>
                                        <th style="" rowspan="2" scope="col">Tgl Masuk Karyawan</th>
                                        <th style="" rowspan="2" scope="col">Nama Karyawan yang diterima</th>
                                        <th style="" rowspan="2" scope="col">Status Ach</th>
                                        <th style="" rowspan="2" scope="col"><i class="fa-solid fa-check"></i></th>
                                    </tr>
                                    <tr>
                                        <th>Ganti</th>
                                        <th>Tambah</th>
                                    </tr>
                                </thead>
                                @php $b=1; @endphp
                                    @foreach($rc_of as $data) 
                                        <tbody style="font-size: 11px;">
                                            {{-- {{dd($data)}} --}}
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    <td style="padding-right:-60px;">{{$data->nomor_dokumen_fptk}}</td>
                                                    <td>{{$data->nama_pemohon}}</td>
                                                    <td>{{$data->jabatan_pemohon}}</td>
                                                    <td>{{$data->departemen_pemohon}}</td>
                                                    <td>{{$data->jabatan_pemohon}}</td>
                                                    <td>{{$data->lokasi}}</td>
                                                    <td>{{$data->tgl_user_input_fptk}}</td>
                                                    <td>{{$data->tanggal_masuk_fptk_pimpinan}}</td>
                                                    <td>{{$data->jumlah_permintaan}}</td>
                                                    <td>{{$data->karyawan_masuk}}</td>
                                                    <td>{{$data->kekurangan}}</td>
                                                    <td>{{$data->penggantian_tenaga_kerja}}</td>
                                                    <td>{{$data->penambahan_tenaga_kerja}}</td>
                                                    <td>{{$data->tanggal_target_pemenuhan}}</td>
                                                    <td>{{$data->tanggal_masuk_karyawan}}</td>
                                                    <td>{{$data->nama_karyawan}}</td>
                                                    <td>{{$data->status_ach}}</td>
                                                    <td>
                                                        <input type="checkbox" name="multiDelete[]" value="{{ $data->id_ontime_fulfillment }}" id="multiDelete" >
                                                    </td>
                                                </tr>
                                        </tbody>
                                    @endforeach
                            </table>
                            <br>
                            <table>
                                <tr>
                                    <td>
                                        <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_of')}}">Tambah</a></td>
                                    <td>
                                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <br>
                <!-- </div> -->
                </div>
            </div>
        <br>
            <br>
                    
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
        });
    });
    $(function() {
        
        var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Permintaan", "Karyawan Masuk", "Kekurangan"],
				datasets: [{
					label: '# of Votes',
					data: [ {{$jumlah_permintaan}} , {{$karyawan_masuk}} , {{$kekurangan}} ],
					backgroundColor: [
					'rgba(0, 255, 0, 0.2)',
					'rgba(255, 255, 0, 0.2)',
					'rgba(255, 0, 0, 0.2)',
				
					],
					borderColor: [
					'rgba(0, 255, 0, 1)',
					'rgba(255, 255, 0, 1)',
					'rgba(255, 0, 0, 1)',
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection
