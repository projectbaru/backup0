@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }

    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }

    div.dataTables_wrapper div.dataTables_filter input {
        margin-left: 0.5em;
        display: inline-block;
        width: auto;
    }
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="background-color: rgb(47 116 181);text-align: center;color:white;">
        <b>Ubah Transaksi PTK</b>
	</div>
    <div class="card-body">
        <form action="{{route('simpan_tptk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
            
            {{ csrf_field() }}
            <div class="form-group" style="width:95%;">
                <input type="hidden" name="id_transaksi_ptk" id="id_transaksi_ptk" value="{{ $data->id_transaksi_ptk }}">
                <div class="row">
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Transaksi PTK</label>
                            <div class="form-check col-sm-3">
                                <input class="form-check-input" readonly type="radio" name="ischeckedTipe" value="0" id="isCheckTambhan" required>
                                <label class="form-check-label" for="isCheckTambhan">Tambahan</label>
                            </div>
                            <div class="form-check col-sm-3">
                                <input class="form-check-input" readonly name="ischeckedTipe" type="radio" value="1" id="isCheckPergantian" required>
                                <label class="form-check-label" for="isCheckPergantian">Penggantian</label>
                            </div>
                        </div>
                    </div>
                </div>
                <b>Informasi</b>
                <div class="row">
                    <div class="col" style="font-size: 10px;">
                        <!-- <div class="form-group row">
                            <label for="nomor_permintaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
                            <div class="col-sm-9">
                                <input type="text" required name="nomor_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan" value="{{$data->nomor_permintaan}}" placeholder="">
                            </div>
                        </div> -->
                        <div class="form-group row" id="ptkIfTamb">
                            <label for="nomor_permintaan_tambahan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
                            <div class="col-sm-9">
                                <input type="text" name="nomor_permintaan_tambahan" readonly value="{{$data->flag == 0 ? $data->nomor_permintaan : $random_t}}" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan_t" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row" id="ptkIfPerg">
                            <label for="nomor_permintaan_pergantian" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
                            <div class="col-sm-9">
                                <input type="text" name="nomor_permintaan_pergantian" readonly value="{{$data->flag == 1 ? $data->nomor_permintaan : $random_p}}" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan_p" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nomor_referensi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
                            <div class="col-sm-9">
                                <input type="text" name="nomor_referensi"  style="font-size:11px;" class="form-control form-control-sm" value="{{$data->nomor_referensi}}" id="nomor_referensi" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="tanggal_permintaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
                            <div class="col-sm-9">
                                <input type="date" name="tanggal_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_permintaan" value="{{ $data->tanggal_permintaan }}" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tanggal_efektif_transaksi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
                            <div class="col-sm-9">
                                <input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_efektif_transaksi" value="{{ $data->tanggal_efektif_transaksi }}" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b>Informasi PTK {{$data->sub}}</b>
                <br>
                <br>

                <div class="row" id="ptkIfTambahan">
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="nama_ptk_tujuan_ptkt" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
                            <div class="col-sm-9">
                                <select name="nama_ptk_tujuan_ptkt" id="nama_ptk_tujuan_ptkt" class="form-control form-control-sm ptkt" style="font-size:11px;">
                                    @foreach($ptk_nptk['looks'] as $ptk_tamb)
                                    <option value="{{$ptk_tamb->nama_ptk}}"  <?= $data->nama_ptk_tujuan == $ptk_tamb->nama_ptk ? 'selected' : '' ?> >{{$ptk_tamb->nama_ptk}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" required name="nama_ptk_tujuan_ptkt" style="font-size:11px;" class="form-control form-control-sm ptkt" value="{{ $data->nama_ptk_tujuan }}" id="nama_ptk_tujuan_ptkt" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_ptk" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
                            <div class="col-sm-9">
                                <input type="number" name="total_ptk" style="font-size:11px;" class="form-control form-control-sm ptkt" id="total_ptk" value="{{ $data->total_ptk_tujuan }}" placeholder="">
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;" class="form-control form-control-sm ptkt" id="deskripsi" placeholder="">{{ $data->deskripsi }}</textarea>
                            </div>
                        </div>
                    </div>

                    <!-- tidak ada  -->
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="tahun_periode" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                            <div class="col-sm-10">
                                <select name="periode_tujuan" id="periode_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                    @foreach($ptk_pp['looks'] as $p)
                                    <option value="{{$p->periode_ptk}}" <?= $data->periode_tujuan == $p->periode_ptk ? 'selected' : '' ?>>{{$p->periode_ptk}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="posisi" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                            <div class="col-sm-10">
                                <select name="nama_posisi_tujuan" id="nama_posisi_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                     @foreach($ptk_np['looks'] as $p)
										<option value="{{$p->nama_posisi}}" <?= $data->nama_posisi_tujuan == $p->nama_posisi ? 'selected' : '' ?>>{{$p->nama_posisi}}</option>
										@endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lokasi_kerja" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                            <div class="col-sm-10">
                                <select name="nama_lokasi_kerja_tujuan" id="nama_lokasi_kerja_tujuan" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nlk['looks'] as $p_nlk)
										<option value="{{$p_nlk->nama_lokasi_kerja}}"  <?= $data->nama_lokasi_kerja_tujuan == $p_nlk->nama_lokasi_kerja ? 'selected' : '' ?>>{{$p_nlk->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
                            </div>
                        </div>
                    </div>
                    <!-- tidak ada -->
                </div>

                <div class="row" id="ptkIfPergantian">
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="nama_ptk_tujuan_ptkp" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
                            <div class="col-sm-10">
                                    <select name="nama_ptk_tujuan_ptkp" id="nama_ptk_tujuan_ptkp" class="form-control form-control-sm ptkp" style="font-size:11px;">
										@foreach($ptk_nptk['looks'] as $p_ptk)
										<option value="{{$p_ptk->nama_ptk}}" <?= $data->nama_ptk_tujuan == $p_ptk->nama_ptk ? 'selected' : '' ?>>{{$p_ptk->nama_ptk}}</option>
										@endforeach
									</select>
                                <!-- <input type="text" required name="nama_ptk_tujuan_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_tujuan_ptkp" value="{{ $data->nama_ptk_tujuan }}" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_ptk_asal" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Asal</label>
                            <div class="col-sm-10">
                            <select name="nama_ptk_asal" id="nama_ptk_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
										@foreach($ptk_nptk['looks'] as $p_ptk)
										<option value="{{$p_ptk->nama_ptk}}"  <?= $data->nama_ptk_asal == $p_ptk->nama_ptk ? 'selected' : '' ?>>{{$p_ptk->nama_ptk}}</option>
										@endforeach
									</select>
                                <!-- <input type="text" name="nama_ptk_asal" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_asal" value="{{ $data->nama_ptk_asal }}" placeholder=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_ptk_tujuan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
                            <div class="col-sm-9">
                                <input type="number" name="total_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm ptkp" id="total_ptk_tujuan" value="{{ $data->total_ptk_tujuan }}" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi_ptkp" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea type="text" rows="4" cols="50" name="deskripsi_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="deskripsi_ptkp" placeholder="">{{ $data->deskripsi }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col" style="font-size: 10px;">
                        <b style="font-size:11px;">PTK Asal</b>
                        <hr>
                        <div class="form-group row">
                            <label for="periode_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                            <div class="col-sm-10">
                                <select name="periode_asal" id="periode_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
                                    @foreach($ptk_pp['looks'] as $p)
                                    <option value="{{$p->periode_ptk}}" <?= $data->periode_asal == $p->periode_ptk ? 'selected' : '' ?>>{{$p->periode_ptk}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_posisi_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                            <div class="col-sm-10">
                                <select name="nama_posisi_asal" id="nama_posisi_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
                                    @foreach($ptk_np['looks'] as $pos)
                                    <option value="{{$pos->nama_posisi}}" <?= $data->nama_posisi_asal == $pos->nama_posisi ? 'selected' : '' ?>>{{$pos->nama_posisi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_lokasi_kerja_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                            <div class="col-sm-10">
                                <select name="nama_lokasi_kerja_asal" id="nama_lokasi_kerja_asal" class="form-control form-control-sm" style="font-size:11px;">
                                    @foreach($ptk_nlk['looks'] as $p)
                                    <option value="{{$p->nama_lokasi_kerja}}" <?= $data->nama_lokasi_kerja_asal == $p->nama_lokasi_kerja ? 'selected' : '' ?>>{{$p->nama_lokasi_kerja}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col" style="font-size: 10px;">
                        <b style="font-size:11px;">PTK Tujuan</b>
                        <hr>
                        <div class="form-group row">
                            <label for="periode_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
                            <div class="col-sm-10">
                                <select name="periode_tujuan_peng" id="periode_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                    @foreach($ptk_pp['looks'] as $pPt)
                                    <option value="{{$pPt->periode_ptk}}" <?= $data->periode_tujuan == $pPt->periode_ptk ? 'selected' : '' ?>>{{$pPt->periode_ptk}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_posisi_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
                            <div class="col-sm-10">
                                <select name="nama_posisi_tujuan_peng" id="nama_posisi_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                    @foreach($ptk_np['looks'] as $j)
                                    <option value="{{$j->nama_posisi}}" <?= $data->nama_posisi_tujuan == $j->nama_posisi ? 'selected' : '' ?>>{{$j->nama_posisi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_lokasi_kerja_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
                            <div class="col-sm-10">
                                <select name="nama_lokasi_kerja_tujuan_peng" id="nama_lokasi_kerja_tujuan" class="form-control form-control-sm" style="font-size:11px;">
                                    @foreach($ptk_nlk['looks'] as $k)
                                    <option value="{{$k->nama_lokasi_kerja}}" <?= $data->nama_lokasi_kerja_tujuan == $k->nama_lokasi_kerja ? 'selected' : '' ?>>{{$k->nama_lokasi_kerja}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <b>Rekaman f Informasi</b>
                <div class="row">
                    <div class="col" style="font-size: 10px;">
                        <div class="form-group row">
                            <label for="lokasi_kerja" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_mulai_efektif" required style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif" value="{{ $data->tanggal_mulai_efektif }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tanggal_selesai_efektif" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" value="{{ $data->tanggal_selesai_efektif }}" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="keterangan" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                            <div class="col-sm-10">
                                <textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="keterangan" placeholder="">{{ $data->keterangan }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 90%;">
                <table>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                        </td>
                        <td>
                            <a href="{{ route('hapus_tptk',$data->id_transaksi_ptk) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
                        </td>
                        <td>
                            <a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script>
$('#nama_lokasi').change(function(){
	$('#kode_lokasi').val($('#nama_lokasi option:selected').data('kode_lokasi'));
})
</script>
@endpush
@section('add-scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
        $("#ptkIfPerg").hide();
        var flag = <?= json_encode($data->flag) ?>;
        if (flag == 0) {
            $('#isCheckTambhan').trigger('click');
            isTambahan();
        }
        if (flag == 1) {
            $("#isCheckPergantian").trigger('click');
            isPergantian();
        }

        $("#isCheckTambhan").change(function() {
            if (this.checked) {
                $('.ptkp').attr('required', false);
                $("#ptkIfTamb").show();
                $("#ptkIfPerg").hide();
                isTambahan();
            } else {
                $("#isCheckPergantian").attr('disabled', false);
                $("#isCheckPergantian").attr('required', false);
                $('.ptkp').attr('required', true);
            }
        });

        $("#isCheckPergantian").change(function() {
            if (this.checked) {
                $('.ptkt').attr('required', false);
                $("#ptkIfTamb").hide();
                $("#ptkIfPerg").show();
                isPergantian();
            } else {
                $("#isCheckTambhan").attr('disabled', false);
                $('.ptkt').attr('required', true);
                $("#ptkIfTamb").show();
                $("#ptkIfPerg").hide();
            }
        });

    });

    function isTambahan() {
        // $("#isCheckPergantian").attr('disabled', true);
        $("#isCheckPergantian").removeAttr('required', true);
        $('.ptkp').attr('required', false);
        $("#ptkIfTambahan").show();
        $("#ptkIfPergantian").hide();
        $("#ptkIfTamb").show();
        $("#ptkIfPerg").hide();
    }

    function isPergantian() {
        // $("#isCheckTambhan").attr('disabled', true);
        $("#isCheckPergantian").removeAttr('required', false);
        $('.ptkt').attr('required', false);
        $("#ptkIfTambahan").hide();
        $("#ptkIfPergantian").show();
        $("#ptkIfTamb").hide();
        $("#ptkIfPerg").show();
    }
</script>
@endsection