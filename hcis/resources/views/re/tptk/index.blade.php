@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>

ol {
list-style-type: decimal; 
}

li {
display: list-item; 
} 

    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;

    }
    table th{
     text-align:center;   
    }
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent

<div class="card">
  <div class="card-header">
    <div class="row">
        <h5>Transaksi PTK</h5>
    </div>
  </div>
    <div class="card-body">
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamtptk')}}" class="btn btn-primary btn-sm" style="color: white;"><span class="d-block m-t-5">Buat Tampilan Baru</span></a>
        </div>
        <br>
        <form action="{{ URL::to('/list_tptk/hapus_banyak') }}" method="POST" id="form_delete">
            @csrf
            
            <div class="table-responsive mb-0">
                <table id="example" class="table table-sm table-bordered table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead style="color:black;font-size:12px;">
                        <tr >
                            <th style="" scope="col">No</th>
                            <th style="" scope="col">Nomor Permintaan</th>
                            <th style="" scope="col">Nama Grup PTK Tujuan</th>
                            <th class="text-center" style="" scope="col">Aksi</th>
                            <th class="text-center" style="" scope="col"><i class="fa-solid fa-check"></i></th>
                        </tr>
                    </thead>
                    <tbody style="font-size:11px;">
                        @php $b=1; @endphp
                        @foreach ($tptk as $data)
                        {{-- {{dd($data)}} --}}
                        <tr>
                            <td>{{ $b++; }}</td>
                            <td>{{ $data->nomor_permintaan }}</td>
                            <td>{{ $data->nama_ptk_tujuan }}</td>
                            <td class="text-center">
                                <a href="{{ URL::to('/list_tptk/detail/'.$data->id_transaksi_ptk) }}" class="btn btn-secondary btn-sm">Detail</a>
                                <a href="{{ URL::to('/list_tptk/edit/'.$data->id_transaksi_ptk) }}" class="btn btn-success btn-sm">Edit</a>
                            </td>
                            <td class="text-center"><input type="checkbox" name="multiDelete[]" id="multiDelete" value="{{ $data->id_transaksi_ptk }}"></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_tptk_t')}}">Tambah</a></td>
                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                        <td>
                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</div>

@endsection                   

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
    $(function() {
        console.log("masuk");
            $("#example").DataTable();
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection