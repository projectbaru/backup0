@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="background-color: rgb(47 116 181);text-align: center;color:white;">
        <div class="row" style="width:100%;">
            <div class="col-md-6" style="text-align:left;">
                
            </div>
            <div class="col-md-6" style="text-align:right;">
                
            </div>

        </div>	
    
	</div>
    <div class="card-body">   
		<form action="{{route('update_tptk')}}" method="post" enctype="multipart/form-data" >{{ csrf_field() }}
			<hr>
				{{ csrf_field() }}
				@foreach($tptk_tamb as $data)
				<div class="form-group" style="width:95%;">
					<div class="">
						<div class="row">
							
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<!-- <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Logo Perusahaan</label> -->
									
								</div>
							</div>
							

						</div>
						<hr>
						<b>Informasi</b>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
									<div class="col-sm-9">
									<input type="text" required value="{{$data->nomor_permintaan}}" name="nomor_permintaan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
									<div class="col-sm-9">
									<input type="text"  value="{{$data->nomor_referensi}}" name="nomor_referensi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
									<div class="col-sm-9">
									<input type="date"  name="tanggal_permintaan" value="{{$data->tanggal_permintaan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
									<div class="col-sm-9">
									<input type="date" required  value="{{$data->tanggal_efektif_transaksi}}" name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<hr>
						<b>Informasi PTK Tambahan</b>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
									<div class="col-sm-9">
										<input type="text" required name="nama_ptk_tujuan" value="{{$data->nama_ptk_tujuan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
									<div class="col-sm-9">
										<input type="text" name="total_ptk" style="font-size:11px;" value="{{$data->total_ptk}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
									<div class="col-sm-9">
										<textarea type="text" rows="4" cols="50" name="deskripsi" value="{{$data->deskripsi}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
									</div>
								</div>
							</div>

							<!-- tidak ada  -->
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
									<div class="col-sm-10">
										<select name="tahun_periode" id="tahun_periode"  class="form-control form-control-sm" id="" style="font-size:11px;">
											<option value="">--- Pilih ---</option>
										</select>		
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
									<div class="col-sm-10">
										<select name="posisi" id="posisi" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
											<option value="">--- Pilih ---</option>
										</select>		
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
									<div class="col-sm-10">
										<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
											<option value="">--- Pilih ---</option>
											
										</select>		
									</div>
								</div>
								
								
							</div>
							<!-- tidak ada -->
							
						</div>
						<hr>
						<b>Rekaman Informasi</b>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
									<div class="col-sm-10">
									<input type="date" required value="{{$data->tanggal_efektif_transaksi}}" name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>	
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
									<div class="col-sm-10">
									<input type="date" name="tanggal_selesai_transaksi" value="{{$data->tanggal_efektif_transaksi}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
									<div class="col-sm-10">
										<textarea type="text" rows="4" cols="50" value="{{$data->keterangan}}" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				<div style="width: 90%;">
					<table>
					<tr>
						<!-- <td>
						<a href="" class="btn">Hapus</a>
					</td> -->
						<!-- <td>
						<a href="" class="btn">Hapus</a>
					</td> -->
						<td>
							<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
						</td>
						<td>
							<a href="{{route('hapus_a',$data->id_transaksi_ptk)}}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
						</td>
						<td>
							<a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
						</td>
					</tr>
					</table>
				</div>
				<br>
		</form>
	</div>
</div>
@endsection
                               
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        var pathname = window.location.pathname;
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_a') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_o') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endpush