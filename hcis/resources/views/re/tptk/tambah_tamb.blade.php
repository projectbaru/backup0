@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
	div.dataTables_wrapper div.dataTables_paginate ul.pagination {
		margin: 2px 0;
		white-space: nowrap;
		justify-content: flex-end;
	}

	div.dataTables_wrapper div.dataTables_length select {
		width: auto;
		display: inline-block;
	}

	div.dataTables_wrapper div.dataTables_filter {
		text-align: right;
	}

	div.dataTables_wrapper div.dataTables_filter input {
		margin-left: 0.5em;
		display: inline-block;
		width: auto;
	}
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
	<div class="card-header" style="background-color: rgb(47 116 181);text-align: center;color:white;">
		<b>Tambah Transaksi PTK</b>
	</div>
	<div class="card-body">
		<form action="{{route('simpan_tptk')}}" method="post" enctype="multipart/form-data">
			<hr>
			{{ csrf_field() }}
			<div class="form-group" style="width:95%;">
				<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Transaksi PTK</label>
								<div class="form-check col-sm-3">
									<input class="form-check-input" checked type="radio" name="ischeckedTipe" value="0" id="isCheckTambhan" required>
									<label class="form-check-label" for="isCheckTambhan">Tambahan</label>
								</div>
								<div class="form-check col-sm-3">
									<input class="form-check-input" name="ischeckedTipe" type="radio" value="1" id="isCheckPergantian" required>
									<label class="form-check-label" for="isCheckPergantian">Penggantian</label>
								</div>
							</div>
						</div>
					</div>
					
					<b>Informasi</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row" id="ptkIfTamb">
								<label for="nomor_permintaan_tambahan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
								<div class="col-sm-9">
									<input type="text" name="nomor_permintaan_tambahan" readonly value="{{$random}}" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan_t" placeholder="">
								</div>
							</div>
							
							<div class="form-group row" id="ptkIfPerg">
								<label for="nomor_permintaan_pergantian" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
								<div class="col-sm-9">
									<input type="text" name="nomor_permintaan_pergantian" readonly value="{{$random_p}}" style="font-size:11px;" class="form-control form-control-sm" id="nomor_permintaan_p" placeholder="">
								</div>
							</div>

							<div class="form-group row">
								<label for="nomor_referensi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
								<div class="col-sm-9">
									<input type="text" name="nomor_referensi" style="font-size:11px;" class="form-control form-control-sm" id="nomor_referensi" placeholder="">
								</div>
							</div>
						</div>
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="tanggal_permintaan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
								<div class="col-sm-9">
									<input type="date" name="tanggal_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_permintaan" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="tanggal_efektif_transaksi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
								<div class="col-sm-9">
									<input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;" class="form-control form-control-sm" id="tanggal_efektif_transaksi" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<hr>
					<b>Informasi PTK Tambahan</b>
					<br>
					<br>
					<div class="row" id="ptkIfTambahan">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="nama_ptk_tujuan_ptkt" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
								<div class="col-sm-9">
									<select name="nama_ptk_tujuan_ptkt" id="nama_ptk_tujuan_ptkt" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nptk['looks'] as $ptk_tamb)
										<option value="{{$ptk_tamb->nama_ptk}}" >{{$ptk_tamb->nama_ptk}}</option>
										@endforeach
									</select>

									<!-- <input type="text" required name="nama_ptk_tujuan_ptkt" style="font-size:11px;" class="form-control form-control-sm ptkt" id="nama_ptk_tujuan_ptkt" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="total_ptk" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
								<div class="col-sm-9">
									<input type="number" name="total_ptk" style="font-size:11px;" class="form-control form-control-sm ptkt" id="total_ptk" placeholder="">
								</div>
								<!-- <input type="text" readonly name="ptk_akhir" style="font-size:11px;" value="" class="form-control form-control-sm" id="ptk_akhir" placeholder=""> -->
								
							</div>
							<div class="form-group row">
								<label for="deskripsi" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
								<div class="col-sm-9">
									<textarea type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;" class="form-control form-control-sm ptkt" id="deskripsi" placeholder=""></textarea>
								</div>
							</div>
						</div>

						<!-- tidak ada  -->
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="periode_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
								<div class="col-sm-10">
									<select name="periode_tujuan" id="periode_tujuan" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_pp['looks'] as $pp)
										<option value="{{$pp->periode_ptk}}" >{{$pp->periode_ptk}}</option>
										@endforeach
									</select>

								</div>
							</div>
							<div class="form-group row">
								<label for="nama_posisi_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
								<div class="col-sm-10">
									<select name="nama_posisi_tujuan" id="nama_posisi_tujuan" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_np['looks'] as $np)
										<option value="{{$np->nama_posisi}}" >{{$np->nama_posisi}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="lokasi_kerja" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
								<div class="col-sm-10">
									<select name="nama_lokasi_kerja_tujuan" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nlk['looks'] as $nlk)
										<option value="{{$nlk->nama_lokasi_kerja}}" >{{$nlk->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<!-- tidak ada -->
					</div>

					<div class="row" id="ptkIfPergantian">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="nama_ptk_tujuan_ptkp" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
								<div class="col-sm-10">
									<select name="nama_ptk_tujuan_ptkp" id="nama_ptk_tujuan_ptkp" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nptk['looks'] as $ptkTamb)
										<option value="{{$ptkTamb->nama_ptk}}">{{$ptkTamb->nama_ptk}}</option>
										@endforeach
									</select>
									<!-- <input type="text" required name="nama_ptk_tujuan_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_tujuan_ptkp" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="nama_ptk_asal" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Asal</label>
								<div class="col-sm-10">
									<select name="nama_ptk_asal" id="nama_ptk_asal" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nptk['looks'] as $ptkTamb)
										<option value="{{$ptkTamb->nama_ptk}}">{{$ptkTamb->nama_ptk}}</option>
										@endforeach
									</select>
									<!-- <input type="text" name="nama_ptk_asal" style="font-size:11px;" class="form-control form-control-sm ptkp" id="nama_ptk_asal" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="total_ptk_tujuan" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
								<div class="col-sm-9">
									<input type="number" name="total_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm ptkp" id="total_ptk_tujuan" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="deskripsi_ptkp" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
								<div class="col-sm-9">
									<textarea type="text" rows="4" cols="50" name="deskripsi_ptkp" style="font-size:11px;" class="form-control form-control-sm ptkp" id="deskripsi_ptkp" placeholder=""></textarea>
								</div>
							</div>
						</div>

						<!-- tidak ada  -->
						<div class="col" style="font-size: 10px;">
							<b style="font-size:11px;">PTK Asal</b>
							<hr>
							<div class="form-group row">
								<label for="periode_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
								<div class="col-sm-10">
									<select name="periode_asal" id="periode_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
										@foreach($ptk_pp['looks'] as $pp)
										<option value="{{$pp->periode_ptk}}">{{$pp->periode_ptk}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="nama_posisi_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
								<div class="col-sm-10">
									<select name="nama_posisi_asal" id="nama_posisi_asal" class="form-control form-control-sm ptkp" style="font-size:11px;">
										@foreach($ptk_np['looks'] as $po)
										<option value="{{$po->nama_posisi}}">{{$po->nama_posisi}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="nama_lokasi_kerja_asal" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
								<div class="col-sm-10">
									<select name="nama_lokasi_kerja_asal" id="nama_lokasi_kerja_asal" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nlk['looks'] as $p)
										<option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="col" style="font-size: 10px;">
							<b style="font-size:11px;">PTK Tujuan</b>
							<hr>
							<div class="form-group row">
								<label for="periode_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
								<div class="col-sm-10">
									<select name="periode_tujuan_peng" id="periode_tujuan_peng" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_pp['looks'] as $p)
										<option value="{{$p->periode_ptk}}">{{$p->periode_ptk}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="nama_posisi_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
								<div class="col-sm-10">
									<select name="nama_posisi_tujuan_peng" id="nama_posisi_tujuan_peng" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_np['looks'] as $p)
										<option value="{{$p->nama_posisi}}">{{$p->nama_posisi}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="nama_lokasi_kerja_tujuan" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
								<div class="col-sm-10">
									<select name="nama_lokasi_kerja_tujuan_peng" id="nama_lokasi_kerja_tujuan_peng" class="form-control form-control-sm" style="font-size:11px;">
										@foreach($ptk_nlk['looks'] as $p_nlk)
										<option value="{{$p_nlk->nama_lokasi_kerja}}">{{$p_nlk->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<!-- tidak ada -->
					</div>

					<hr>
					<b>Rekaman Informasi</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="lokasi_kerja" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
								<div class="col-sm-10">
									<input type="date" name="tanggal_mulai_efektif" required style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif" />
								</div>
							</div>
							<div class="form-group row">
								<label for="tanggal_selesai_efektif" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
								<div class="col-sm-10">
									<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />
								</div>
							</div>
							<div class="form-group row">
								<label for="keterangan" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
								<div class="col-sm-10">
									<textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="keterangan" placeholder=""></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="width: 90%;">
				<table>
					<tr>
						<td>
							<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
						</td>
						<td>
							<a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
						</td>
					</tr>
				</table>
			</div>
			<br>
		</form>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
    function updateCode() {
        var ischeckedValue = document.querySelector('input[name="ischecked"]:checked').value;
        var codeElement = document.getElementById('nomor_permintaan_t');

        var tipeTransaksi = '';
        if (ischeckedValue === '0') {
            tipeTransaksi = 'TAMBAH';
        } else if (ischeckedValue === '1') {
            tipeTransaksi = 'PENGGANTIAN';
        }

        var next_ = parseInt(codeElement.dataset.maxId) + 1 || 1;
        var code = String(next_).padStart(4, '0') + '/' + tipeTransaksi + '/PTK/HRD/' + new Date().getFullYear();

        codeElement.textContent = code;
    }
</script>

<script>
$('#nama_posisi_tujuan').change(function(){
	$('#ptk_akhir').val($('#nama_posisi_tujuan option:selected').data('ptk_akhir'));
})
</script>
<script>
$('#periode_tujuan').change(function(){
	$('#ptk_akhir').val($('#periode_tujuan option:selected').data('ptk_akhir'));
})
</script>
<script>
$('#nama_ptk_tujuan_ptkt').change(function(){
	$('#ptk_akhir').val($('#nama_ptk_tujuan_ptkt option:selected').data('ptk_akhir'));
})
</script>
<script>
$('#nama_lokasi_kerja_tujuan').change(function(){
	$('#ptk_akhir').val($('#nama_lokasi_kerja_tujuan option:selected').data('ptk_akhir'));
})
</script>
<script type="text/javascript">
	
	$(document).ready(function() {
		$("#ptkIfPergantian").hide();
		$("#ptkIfPerg").hide();
		$('.ptkp').attr('required', false);

		$("#isCheckTambhan").change(function() {
			if (this.checked) {
				$('.ptkp').attr('required', false);
				$("#ptkIfTambahan").show();
				$("#ptkIfPergantian").hide();
			} else {
				$("#isCheckPergantian").attr('disabled', false);
				$("#isCheckPergantian").attr('required', false);
				$('.ptkp').attr('required', true);
			}
		});		

		$("#isCheckPergantian").change(function() {
			if (this.checked) {
				$('.ptkt').attr('required', false);
				$("#ptkIfTambahan").hide();
				$("#ptkIfPergantian").show();
			} else {
				$("#isCheckTambhan").attr('disabled', false);
				$('.ptkt').attr('required', true);
				$("#ptkIfTambahan").show();
				$("#ptkIfPergantian").hide();
			}
		});

		$("#isCheckTambhan").change(function() {
			if (this.checked) {

				$('.ptkp').attr('required', false);
				$("#ptkIfTamb").show();
				$("#ptkIfPerg").hide();
			} else {
				$("#isCheckPergantian").attr('disabled', false);
				$("#isCheckPergantian").attr('required', false);
				$('.ptkp').attr('required', true);
			}
		});

		$("#isCheckPergantian").change(function() {
			if (this.checked) {

				$('.ptkt').attr('required', false);

				$("#ptkIfTamb").hide();
				$("#ptkIfPerg").show();
			} else {
				$("#isCheckTambhan").attr('disabled', false);
				$('.ptkt').attr('required', true);

				$("#ptkIfTamb").show();
				$("#ptkIfPerg").hide();
			}
		});
	});

	function setDumy() {
		$("#nomor_permintaan").val("014fsfs");
		$("#tanggal_mulai_efektif").val("2022-10-24");
		$("#tanggal_selesai_efektif").val("2022-10-25");
		$("#tanggal_permintaan").val("2022-10-22");
		$("#tanggal_efektif_transaksi").val("2022-10-23");
		$("#nama_ptk_tujuan_ptkt").val("test");
		$("#total_ptk").val(10);
		$("#deskripsi").val("test juga");
	};
</script>


<script>
  $(document).ready(function() {
    $('#nama_grup_lokasi_kerja').change(function() {
        var selectedValue = $(this).val();

        $('#kode_grup_lokasi_kerja').val($(this).find(':selected').data('kode'));

        var url = `{{ URL::to('/list_ptk/lokasi-kerja/:nama') }}`;
        url = url.replace(':nama', selectedValue);

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                _token: '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);

                var lokasiKerjaSelect = $('#lokasi_kerja');
                lokasiKerjaSelect.empty()

                lokasiKerjaSelect.append($('<option selected disabled></option>').val("").text("--- Pilih ---"));
                $.each(data, function(index, option) {
                    lokasiKerjaSelect.append($(`<option data-kode='${option.kode_lokasi_kerja}'></option>`).val(option.lokasi_kerja).text(option.lokasi_kerja));
                });
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });

    $('#lokasi_kerja').change(function() {
        var selectedValue = $(this).val();

        $('#kode_lokasi_kerja').val($(this).find(':selected').data('kode'));

        var url = `{{ URL::to('/list_ptk/lokasi-kerja-detail/:nama') }}`;
        url = url.replace(':nama', selectedValue);

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                _token: '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                var groupLokasiKerjaSelect = $('#nama_grup_lokasi_kerja');
                groupLokasiKerjaSelect.val(data.nama_grup_lokasi_kerja)
                $('#kode_grup_lokasi_kerja').val(data.kode_grup_lokasi_kerja)
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });
});
@endpush