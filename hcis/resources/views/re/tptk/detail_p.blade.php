@extends('re.tptk.side')
@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="page-header-title">
                                        <!-- <h5 class="m-b-10">Detail Transaksi PTK Tambahan</h5> -->
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                        <!-- <li class="breadcrumb-item"><a href="#!">transaksi PTK tambahan</a></li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('simpan_pd')}}" method="post">
                        {{ csrf_field() }}
                        @foreach($tptk_peng as $data)

                            <hr>
                            <input type="hidden" name="tg_id" id="tg_id" value="" />
                            <table>
                                <tr>
                                    <!-- <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="">Print</a></td> -->
                                    <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="">Ubah</a></td>
                                    <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="" id="btn_delete">Hapus</a></td>
                                    <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_tptk')}}">Batal</a></td>
                                </tr>
                            </table><br>
                            <b style="color:black;">Informasi</b><br><br>
                            <div class="row">
                                <div class="col">
                                    <table style="font-size:12px;">
                                       <tr>
                                            <td>Nomor Permintaan</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nomor_permintaan" value="{{$data->nomor_permintaan}}">{{$data->nomor_permintaan}}</td>
                                       </tr>
                                       <tr>
                                            <td>Nomor Referensi</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nomor_referensi" value="{{$data->nomor_referensi}}">{{$data->nomor_referensi}}</td>
                                       </tr>
                                       <tr>
                                            <td>Tanggal Permintaan</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="tanggal_permintaan" value="{{$data->tanggal_permintaan}}">{{$data->tanggal_permintaan}}</td>
                                       </tr>
                                    </table>
                                </div>
                                <div class="col" style="font-size:12px;">
                                    <table>
                                        <tr>
                                            <td>Tanggal Efektif</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="tanggal_efektif" value="{{$data->tanggal_efektif}}">{{$data->tanggal_efektif}}</td>
                                       </tr>
                                        
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <b style="color:black;">Informasi PTK Tambahan</b>
                            <div class="row">
                                <div class="col">
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Nama PTK Tujuan</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_ptk_tujuan" value="{{$data->nama_ptk_tujuan}}">{{$data->nama_ptk_tujuan}}</td>
                                       </tr>
                                       
                                       <tr>
                                            <td>Total PTK</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="total_ptk_tujuan" value="{{$data->total_ptk_tujuan}}">{{$data->total_ptk_tujuan}}</td>
                                       </tr>
                                       <tr>
                                            <td>Deskripsi PTK</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_ptk_asal" value="{{$data->nama_ptk_asal}}">{{$data->nama_ptk_asal}}</td>
                                       </tr>
                                       

                                    </table>
                                </div>
                                <div class="col">
                                    <b>PTK Asal</b>
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Tahun Periode</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="periode_asal" value="{{$data->periode_asal}}">{{$data->periode_asal}}</td>
                                       </tr> 
                                       <tr>
                                            <td>Posisi</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_posisi_asal" value="{{$data->nama_posisi_asal}}">{{$data->nama_posisi_asal}}</td>
                                       </tr>
                                       <tr>
                                            <td>Lokasi Kerja</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_lokasi_kerja_asal" value="{{$data->nama_lokasi_kerja_asal}}">{{$data->nama_lokasi_kerja_asal}}</td>
                                       </tr>
                                    </table>
                                </div>
                                <div class="col">
                                    <b>PTK Tujuan</b>
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Tahun Periode</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="periode_tujuan" value="{{$data->periode_tujuan}}">{{$data->periode_tujuan}}</td>
                                       </tr> 
                                       <tr>
                                            <td>Posisi</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_posisi_tujuan" value="{{$data->nama_posisi_tujuan}}">{{$data->nama_posisi_tujuan}}</td>
                                       </tr>
                                       <tr>
                                            <td>Lokasi Kerja</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="nama_lokasi_kerja_tujuan" value="{{$data->nama_lokasi_kerja_tujuan}}">{{$data->nama_lokasi_kerja_tujuan}}</td>
                                       </tr>
                                    </table>
                                </div>
                            
                            </div>
                            <hr>
                            <b style="color:black;">Rekaman Informasi</b>
                            <div class="row">
                                <div class="col">
                                    <table style="font-size:12px;">
                                        <tr>
                                            <td>Tanggal Mulai Efektif</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                       </tr> 
                                       <tr>
                                            <td>Tanggal Selesai Efektif</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                       </tr>
                                       <tr>
                                            <td>Keterangan</td>
                                            <td>:</td>
                                            <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                       </tr> 
                                    </table><br>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('add-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_p') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection