@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="background-color: rgb(47 116 181);text-align: center;color:white;">
        <div class="row" style="width:100%;">
            <div class="col-md-6" style="text-align:left;">
                <b>Transaksi PTK</b>
            </div>
            <div class="col-md-6" style="text-align:right;">
                <a href="{{route('ubah_tamtptk')}}" class="btn btn-sm btn-primary">Buat Tampilan Baru</a>
            </div>

        </div>	
    
	</div>
    <div class="card-body">
        <div class="" style="overflow-x:auto">
                <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                    <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
            <form action="{{ URL::to('/list_tptk/hapus_banyak') }}" method="POST"  id="form_delete">
                @csrf
                <table id="example" class="table table-sm table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead style="color:black;">
                        <th style="">No</th>
                            @for($i = 0; $i < count($th); $i++)
                                <!-- @if($th[$i] == 'id_perusahaan')
                                    <th></th>
                                @endif -->
                                @if($th[$i] == 'nomor_permintaan')
                                    <th style="">Nomor Permintaan</th>
                                @endif
                                @if($th[$i] == 'tipe_transaksi')
                                    <th style="">Tipe Transaksi</th>
                                @endif
                                @if($th[$i] == 'nomor_referensi')
                                    <th style="">Nomor Referensi</th>
                                @endif
                                @if($th[$i] == 'tanggal_permintaan')
                                    <th style="">Tanggal Permintaan</th>
                                @endif
                                @if($th[$i] == 'tanggal_efektif_transaksi')
                                    <th style="">Tanggal Efektif Transaksi</th>
                                @endif
                                @if($th[$i] == 'periode_tujuan')
                                    <th style="">Periode Tujuan</th>
                                @endif
                                @if($th[$i] == 'periode_asal')
                                    <th style="">Periode Asal</th>
                                @endif
                                @if($th[$i] == 'kode_ptk_asal')
                                    <th style="">Kode PTK Asal</th>
                                @endif
                                @if($th[$i] == 'nama_ptk_asal')
                                    <th style="">Nama PTK Asal</th>
                                @endif
                                @if($th[$i] == 'total_ptk_asal')
                                    <th style="">Total PTK Asal</th>
                                @endif
                                @if($th[$i] == 'kode_ptk_tujuan')
                                    <th style="">Kode PTK Tujuan</th>
                                @endif
                                @if($th[$i] == 'nama_ptk_tujuan')
                                    <th style="">Nama PTK Tujuan</th>
                                @endif
                                @if($th[$i] == 'total_ptk_tujuan')
                                    <th style="">Total PTK Tujuan</th>
                                @endif
                                @if($th[$i] == 'nama_posisi_tujuan')
                                    <th style="">Nama Posisi Tujuan</th>
                                @endif
                                @if($th[$i] == 'nama_lokasi_kerja_tujuan')
                                    <th style="">Nama Lokasi Kerja Tujuan</th>
                                @endif
                                @if($th[$i] == 'nama_posisi_asal')
                                    <th style="">Nama Posisi Asal</th>
                                @endif
                                @if($th[$i] == 'nama_lokasi_kerja_asal')
                                    <th style="">Nama Lokasi Kerja Asal</th>
                                @endif
                                @if($th[$i] == 'deskripsi')
                                    <th style="">Deskripsi</th>
                                @endif
                                @if($th[$i] == 'keterangan')
                                    <th style="">Keterangan</th>
                                @endif
                                @if($th[$i] == 'status_rekaman')
                                    <th style="">Status Rekaman</th>
                                @endif
                                @if($th[$i] == 'waktu_user_input')
                                    <th style="">Waktu User Input</th>
                                @endif
                                @if($th[$i] == 'tanggal_mulai_efektif')
                                    <th style="">Tanggal Mulai Efektif</th>
                                @endif
                                @if($th[$i] == 'tanggal_selesai_efektif')
                                    <th style="">Tanggal Selesai Efektif</th>
                                @endif
                                @if($i == count($th) - 1)
                                    <th style="">Aksi</th>
                                    <th style="">V</th>
                                @endif
                            @endfor
                        </thead>
                        <tbody style="font-size:11px;">
                            @php $b=1 @endphp 
                            @foreach($query as $row)
                                <tr>
                                    <td style="">{{$b++}}</td>
                                    @for($i = 0; $i < count($th); $i++)
                                        <!-- @if($th[$i] == 'id_perusahaan')
                                            <td>{{ $row->id_perusahaan ?? 'NO DATA' }}</td>
                                        @endif -->

                                        @if($th[$i] == 'nomor_permintaan')
                                            <td>{{ $row->nomor_permintaan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'tipe_transaksi')
                                            <td>{{ $row->tipe_transaksi ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nomor_referensi')
                                            <td>{{ $row->nomor_referensi ?? 'NO DATA' }}</td>
                                        @endif
                                        
                                        @if($th[$i] == 'tanggal_permintaan')
                                        <td>{{ $row->tanggal_permintaan ? date('d-m-Y', strtotime($row->tanggal_permintaan)) : 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'tanggal_efektif_transaksi')
                                        <td>{{ $row->tanggal_efektif_transaksi ? date('d-m-Y', strtotime($row->tanggal_efektif_transaksi)) : 'NO DATA' }}</td>
                                        @endif
                                        
                                        @if($th[$i] == 'periode_tujuan')
                                            <td>{{ $row->periode_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'periode_asal')
                                            <td>{{ $row->periode_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'kode_ptk_asal')
                                            <td>{{ $row->kode_ptk_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_ptk_asal')
                                            <td>{{ $row->nama_ptk_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'total_ptk_asal')
                                            <td>{{ $row->total_ptk_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'kode_ptk_tujuan')
                                            <td>{{ $row->kode_ptk_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_ptk_tujuan')
                                            <td>{{ $row->nama_ptk_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'total_ptk_tujuan')
                                            <td>{{ $row->total_ptk_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_posisi_tujuan')
                                            <td>{{ $row->nama_posisi_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_lokasi_kerja_tujuan')
                                            <td>{{ $row->nama_lokasi_kerja_tujuan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_posisi_asal')
                                            <td>{{ $row->nama_posisi_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'nama_lokasi_kerja_asal')
                                            <td>{{ $row->nama_lokasi_kerja_asal ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'deskripsi')
                                            <td>{{ $row->deskripsi ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'keterangan')
                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'status_rekaman')
                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'waktu_user_input')
                                            <td>{{ $row->waktu_user_input ? date('d-m-Y', strtotime($row->waktu_user_input)) : 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                        @endif
                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                        @endif
                                        @if($i == count($th) - 1)
                                            <td> 
                                                <a href="{{ URL::to('/list_tptk/detail/'.$row->id_transaksi_ptk) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                <a href="{{ URL::to('/list_tptk/edit/'.$row->id_transaksi_ptk) }}" class="">Edit</a>
                                            </td>
                                            <td>
                                            <input type="checkbox" name="multiDelete[]" value="{{ $row->id_transaksi_ptk }}"  id="multiDelete">                                  
                                            </td>
                                        @endif
                                    @endfor
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <table>
                        <tr>
                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_tptk_t')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                            <td>
                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                            </td>
                            <!-- <td>
                                <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Print</button>
                            </td> -->
                        </tr>
                    </table>
            </form>

        </div>
    </div>
</div>
@endsection
                               
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        var pathname = window.location.pathname;
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_a') {
            $("#hasOpen").trigger('click');
        }
        if (pathname != '/list_tptk') {
            $("#hasOpen").trigger('click');
        }
    });
</script>

<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endpush