@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<style>
div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="background-color: rgb(47 116 181);text-align: center;color:white;">
		<b>Tambah Transaksi PTK</b>
	</div>
    <div class="card-body">
		<form action="{{route('simpan_tptk')}}" method="post" enctype="multipart/form-data">
			<hr>
			{{ csrf_field() }}
			<div class="form-group" style="width:95%;">
				<div class="">
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Transaksi PTK</label>
								<div class="form-check col-sm-3">
									<input class="form-check-input" type="checkbox"  value="tambahan" id="defaultCheck1">
									<label class="form-check-label" for="defaultCheck1">
										Tambahan
									</label>
								</div>
								<div class="form-check col-sm-3">
									<input class="form-check-input" type="checkbox"  value="penggantian" id="defaultCheck1">
									<label class="form-check-label" for="defaultCheck1">
										Penggantian
									</label>
								</div>
							</div>
						</div>

					</div>
					<hr>
					<b>Informasi</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Permintaan</label>
								<div class="col-sm-9">
									<input type="text" required name="nomor_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Referensi</label>
								<div class="col-sm-9">
									<input type="text" name="nomor_referensi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Permintaan</label>
								<div class="col-sm-9">
									<input type="date" name="tanggal_permintaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
						</div>
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Efektif</label>
								<div class="col-sm-9">
									<input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<hr>
					<b>Informasi PTK Tambahan</b>
					<br>
					<br>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Tujuan</label>
								<div class="col-sm-10">
									<input type="text" required name="nama_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama PTK Asal</label>
								<div class="col-sm-10">
									<input type="text" name="nama_ptk_asal" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total PTK</label>
								<div class="col-sm-9">
									<input type="text" name="total_ptk_tujuan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi</label>
								<div class="col-sm-9">
									<textarea type="text" rows="4" cols="50" name="deskripsi" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
								</div>
							</div>
						</div>

						<!-- tidak ada  -->
						<div class="col" style="font-size: 10px;">
							<b style="font-size:11px;">PTK Asal</b>
							<hr>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
								<div class="col-sm-10">
									<select name="periode_asal" required id="periode_asal" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
										@foreach($ptk as $p)
											<option value="{{$p->periode_ptk}}">{{$p->periode_ptk}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
								<div class="col-sm-10">
									<select name="nama_posisi_asal" required id="nama_posisi_asal" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
										@foreach($ptk as $p)
											<option value="{{$p->nama_posisi}}">{{$p->nama_posisi}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
								<div class="col-sm-10">
									<select name="nama_lokasi_kerja_asal" id="nama_lokasi_kerja_asal" required class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
									@foreach($ptk as $p)
											<option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col" style="font-size: 10px;">
							<b style="font-size:11px;">PTK Tujuan</b>
							<hr>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tahun Periode</label>
								<div class="col-sm-10">
									<select name="periode_tujuan" id="periode_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
										@foreach($ptk as $p)
											<option value="{{$p->periode_ptk}}">{{$p->periode_ptk}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Posisi</label>
								<div class="col-sm-10">
									<select name="nama_posisi_tujuan" id="nama_posisi_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
										@foreach($ptk as $p)
											<option value="{{$p->nama_posisi}}">{{$p->nama_posisi}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Lokasi Kerja</label>
								<div class="col-sm-10">
									<select name="nama_lokasi_kerja_tujuan" id="nama_lokasi_kerja_tujuan" class="form-control form-control-sm" id="exampleFormControlSelect1" style="font-size:11px;">
										@foreach($ptk as $p)
											<option value="{{$p->nama_lokasi_kerja}}">{{$p->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<!-- tidak ada -->
					</div>
					<hr>
					<b>Rekaman Informasi</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
								<div class="col-sm-10">
									<input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm startDates" id="tanggal_mulai_efektif" placeholder="Tanggal mulai efektif">

									<!-- <input type="date" required name="tanggal_efektif_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
								<div class="col-sm-10">
									<input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm endDates" id="tanggal_selesai_efektif" placeholder="Tanggal selesai efektif" />

									<!-- <input type="date" name="tanggal_selesai_transaksi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
								<div class="col-sm-10">
									<textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div style="width: 100%;">
				<table>
					<tr>
						<!-- <td>
							<input type="text">
						</td> -->
						<td>
							<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
						</td>
						<td>
							<a href="{{ route('list_tptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
						</td>
					</tr>
				</table>
			</div>
			<br>
		</form>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

@endpush

@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
      console.log("masuk");
            $("#example").DataTable();
    // $(function() {
        //hang on event of form with id=myform
        $('#form-filter-kolom').on('submit', function(e) {
            e.preventDefault()

            var actionurl = "{{ route('filter_dcp') }}"
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $(this).serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah difilter.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        })

    // });
</script>
@endsection