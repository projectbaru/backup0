@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Perencanaan Tenaga Kerja @endslot
@slot('title') Workstrukture @endslot
@endcomponent
@foreach($rc_fptk as $data)
    <div class="">
        <table id="example" class="">
            <tr>
                <td>No ISO</td>
                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>No Dokumen</td>
                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                <td>{{$data->no_dokumen}}</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                <td>{{date('d-m-Y', strtotime($data->tanggal_upload_dokumen_terakhir))}}</td>
            </tr>
        </table>
    </div>
    <hr>
    <div class="row">
        <hr>
        <div class="col">
            <table>
                <tbody>
                <tr>
                    <td>Nama Pemohon</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$data->nama_pemohon}}</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$data->jabatan_pemohon}}</td>
                </tr>
                <tr>
                    <td>Departemen</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$data->departemen_pemohon}}</td>
                </tr>
                <tr>
                    <td>Dokumen FPTK Terakhir</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td><a href="" download>Download</a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col">
        <table>
            <tbody>
                <tr>
                    <td>Jumlah Kebutuhan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$data->jumlah_kebutuhan}}</td>
                </tr>
                <tr>
                    <td>Jabatan yang Dibutuhkan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$data->jabatan_kepegawaian}}</td>
                </tr>
                <tr>
                    <td>Tingkat</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="staff" id="flexCheckDefault" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }} >
                            <label class="form-check-label" for="flexCheckDefault">
                                Staff
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="middle" id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Middle
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="senior" id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Senior
                            </label>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
@endforeach
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection