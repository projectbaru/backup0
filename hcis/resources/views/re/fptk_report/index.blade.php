@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

@endpush


@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') FPTK Report @endslot
@slot('title') Recruitment @endslot
@endcomponent
<table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
  <thead style="color:black;font-size: 14px; ">
    <tr>
      <th style="">No</th>
      <th style="" scope="col">Nama Pemohon</th>
      <th style="" scope="col">No Dokumen</th>
      <th style="" scope="col">Jabatan Yang Dibutuhkan</th>
      <th style="" scope="col">Tingkat</th>
      <th style="" scope="col">Jumlah Kebutuhan</th>
      <th style="" scope="col">Lampiran</th>
      <th style="" scope="col">Tanggal Upload Dokumen Terakhir</th>
      <th style="" scope="col">Action</th>
    </tr>
  </thead>
  <tbody style="font-size: 12px;color:black;"> 
  @php $b=1; @endphp @foreach($rc_fptk as $data) {{-- {{dd($data)}} --}} 
    <tr>
      <td>{{$b++;}}</td>
      <td style="font-size: 12px;color:black;">{{$data->nama_pemohon}}</td>
      <td style="font-size: 12px;color:black;">{{$data->no_dokumen}}</td>
      <td style="font-size: 12px;color:black;">{{$data->jabatan_kepegawaian}}</td>
      <td style="font-size: 12px;color:black;">{{$data->tingkat_kepegawaian}}</td>
      <td style="font-size: 12px;color:black;">{{$data->jumlah_kebutuhan}}</td>
      <td style="font-size: 12px;color:black;">{{$data->lampiran_file}}</td>
      <td style="font-size: 12px;color:black;">{{$data->tanggal_upload_dokumen_terakhir}}</td>
      <td>
        <a href="{{ URL::to('/list_fptk_r/detail/'.$data->id_fptk) }}" class="btn btn-secondary btn-sm">Preview</a>
      </td>
    </tr> 
  @endforeach 
  </tbody>
</table>
                                             
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
      @section('add-scripts') <script>
        $('#manual-ajax').click(function(event) {
          event.preventDefault();
          this.blur();
          /
          $.get(this.href, function(html) {
            $(html).appendTo('body').modal();
          });
        });
      </script>
      <script>
        $('#nama_grup_lokasi_kerja').change(function() {
          $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
        })
      </script>
      <script>
        $('#nama_lokasi_kerja').change(function() {
          $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
        })
      </script>
      <script>
        $(function() {
          //hang on event of form with id=myform
          $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
              title: ' < h4 > Anda yakin ingin menghapus ? < /h4>',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              cancelButtonText: "Batal",
              confirmButtonText: " < i > Ya,
              hapus! < /i>",
              // buttonsStyling: false
            }).then((result) => {
              if (result.isConfirmed) {
                $.ajax({
                  url: actionurl,
                  type: 'post',
                  dataType: 'JSON',
                  cache: false,
                  data: $("#form_delete").serialize(),
                  success: function(res) {
                    if (res.status) {
                      Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Data berhasil dihapus.",
                      });
                    }
                  },
                  error: function(e) {
                    console.log(e);
                    Swal.fire({
                      icon: "error",
                      title: "Gagal!",
                      text: "Terjadi kesalahan saat menghapus data.",
                    });
                  },
                  complete: function(jqXhr, msg) {
                    setTimeout(() => {
                      location.reload();
                    }, 800);
                    // console.log(jqXhr, msg);
                  }
                });
              }
            })
          });
        });
      </script> 
@endsection