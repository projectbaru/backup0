@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

@endpush


@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('li_3') Formulir Permintaan Tenaga Kerja Report @endslot
@slot('title') Recruitmen @endslot
@endcomponent
    <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
        @csrf        
            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9; ">
                <thead style="color:black;font-size:12px;">
                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                    @for($i = 0; $i < count($th); $i++)
                        @if($th[$i] == 'no_dokumen')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No Dokumen</th>
                        @endif
                        @if($th[$i] == 'nik_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">NIK Pemohon</th>
                        @endif
                        @if($th[$i] == 'nama_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Pemohon</th>
                        @endif
                        @if($th[$i] == 'jabatan_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Pemohon</th>
                        @endif
                        @if($th[$i] == 'departemen_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Departemen Pemohon</th>
                        @endif
                        @if($th[$i] == 'penggantian_tenaga_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Penggantian Tenaga Kerja</th>
                        @endif
                        @if($th[$i] == 'penambahan_tenaga_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Penambahan Tenaga Kerja</th>
                        @endif
                        @if($th[$i] == 'head_hunter')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Head Hunter</th>
                        @endif
                        @if($th[$i] == 'status_kepegawaian')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Kepegawaian</th>
                        @endif
                        @if($th[$i] == 'kontrak_selama')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kontrak Selama</th>
                        @endif
                        @if($th[$i] == 'magang_selama')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Magang Selama</th>
                        @endif
                        @if($th[$i] == 'jabatan_kepegawaian')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Kepegawaian</th>
                        @endif
                        @if($th[$i] == 'tingkat_kepegawaian')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Kepegawaian</th>
                        @endif
                        @if($th[$i] == 'tanggal_mulai_bekerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Mulai Bekerja</th>
                        @endif
                        @if($th[$i] == 'jumlah_kebutuhan')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jumlah Kebutuhan</th>
                        @endif
                        @if($th[$i] == 'lokasi_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Kerja</th>
                        @endif
                        @if($th[$i] == 'alasan_penambahan_tenaga_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Alasan Penambahan Tenaga Kerja</th>
                        @endif
                        @if($th[$i] == 'gaji')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Gaji</th>
                        @endif
                        @if($th[$i] == 'tunjangan_kesehatan')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tunjangan Kesehatan</th>
                        @endif 
                        @if($th[$i] == 'tunjangan_transportasi')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tunjangan Transportasi</th>
                        @endif
                        @if($th[$i] == 'tunjangan_komunikasi')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tunjangan Komunikasi</th>
                        @endif
                        @if($th[$i] == 'hari_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Hari Kerja</th>
                        @endif
                        @if($th[$i] == 'jam_kerja')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jam Kerja</th>
                        @endif
                        @if($th[$i] == 'jenis_kelamin')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Kelamin</th>
                        @endif
                        @if($th[$i] == 'usia_minimal')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Usia Minimal</th>
                        @endif
                        @if($th[$i] == 'usia_maksimal')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Usia Maksimal</th>
                        @endif
                        @if($th[$i] == 'pendidikan')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pendidikan</th>
                        @endif
                        @if($th[$i] == 'pengalaman')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman</th>
                        @endif
                        @if($th[$i] == 'kemampuan_layanan')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman</th>
                        @endif
                        @if($th[$i] == 'lampiran_file')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman</th>
                        @endif
                        @if($th[$i] == 'jabatan_penyetuju')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Penyetuju</th>
                        @endif
                        @if($th[$i] == 'nama_penyetuju')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Penyetuju</th>
                        @endif
                        @if($th[$i] == 'ttd_penyetuju')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">TTD Penyetuju</th>
                        @endif
                        @if($th[$i] == 'nama_penganalisa')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Penganalisa</th>
                        @endif
                        @if($th[$i] == 'hasil_penyetujuan_hr')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Hasil Penyetujuan HR</th>
                        @endif
                        @if($th[$i] == 'ttd_hr')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">TTD HR</th>
                        @endif
                        @if($th[$i] == 'analisa_hrd')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Analisa HRD</th>
                        @endif
                        @if($th[$i] == 'ttd_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">TTD Pemohon</th>
                        @endif
                        @if($th[$i] == 'nama_atasan_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Atasan Pemohon</th>
                        @endif
                        @if($th[$i] == 'ttd_atasan_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Atasan Pemohon</th>
                        @endif
                        @if($th[$i] == 'jabatan_atasan_pemohon')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Atasan Pemohon</th>
                        @endif
                        @if($th[$i] == 'status_rekaman')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Rekaman</th>
                        @endif
                        @if($th[$i] == 'nik_input_user')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">NIK Input User</th>
                        @endif
                        @if($th[$i] == 'nama_user_input')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama User Input</th>
                        @endif
                        @if($th[$i] == 'dokumen_terakhir')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Dokumen Terakhir</th>
                        @endif
                        @if($th[$i] == 'tanggal_upload_dokumen_terakhir')
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Upload Dokumen Terakhir</th>
                        @endif
                        @if($i == count($th) - 1)
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                        @endif
                    @endfor
                </thead>
                <tbody style="font-size:11px;">
                    @php $b = 1; @endphp  @foreach($query as $row)
                        <tr>
                            <td>{{$b++}}</td>
                            @for($i = 0; $i < count($th); $i++)
                                @if($th[$i] == 'no_dokumen')
                                    <td>{{ $row->no_dokumen ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nik_pemohon')
                                    <td>{{ $row->nik_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_pemohon')
                                    <td>{{ $row->nama_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jabatan_pemohon')
                                    <td>{{ $row->jabatan_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'departemen_pemohon')
                                    <td>{{ $row->departemen_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'penggantian_tenaga_kerja')
                                    <td>{{ $row->penggantian_tenaga_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'head_hunter')
                                    <td>{{ $row->head_hunter ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'status_kepegawaian')
                                    <td>{{ $row->status_kepegawaian ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'kontrak_selama')
                                    <td>{{ $row->kontrak_selama ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'magang_selama')
                                    <td>{{ $row->magang_selama ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jabatan_kepegawaian')
                                    <td>{{ $row->jabatan_kepegawaian ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tingkat_kepegawaian')
                                    <td>{{ $row->tingkat_kepegawaian ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggal_mulai_bekerja')
                                    <td>{{ $row->tanggal_mulai_bekerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jumlah_kebutuhan')
                                    <td>{{ $row->jumlah_kebutuhan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'lokasi_kerja')
                                    <td>{{ $row->lokasi_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'alasan_penambahan_tenaga_kerja')
                                    <td>{{ $row->alasan_penambahan_tenaga_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'gaji')
                                    <td>{{ $row->gaji ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tunjangan_kesehatan')
                                    <td>{{ $row->tunjangan_kesehatan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tunjangan_transportasi')
                                    <td>{{ $row->tunjangan_transportasi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tunjangan_komunikasi')
                                    <td>{{ $row->tunjangan_komunikasi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'hari_kerja')
                                    <td>{{ $row->hari_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jam_kerja')
                                    <td>{{ $row->hari_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jenis_kelamin')
                                    <td>{{ $row->jenis_kelamin ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'usia_minimal')
                                    <td>{{ $row->usia_minimal ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'usia_maksimal')
                                    <td>{{ $row->usia_maksimal ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'pendidikan')
                                    <td>{{ $row->pendidikan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'pengalaman')
                                    <td>{{ $row->pengalaman ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'kemampuan_layanan')
                                    <td>{{ $row->kemampuan_layanan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'lampiran_file')
                                    <td>{{ $row->lampiran_file ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jabatan_penyetuju')
                                    <td>{{ $row->jabatan_penyetuju ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_penyetuju')
                                    <td>{{ $row->nama_penyetuju ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'ttd_penyetuju')
                                    <td>{{ $row->ttd_penyetuju ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_penganalisa')
                                    <td>{{ $row->nama_penganalisa ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'hasil_penyetujuan_hr')
                                    <td>{{ $row->hasil_penyetujuan_hr ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'ttd_hr')
                                    <td>{{ $row->ttd_hr ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'analisa_hrd')
                                    <td>{{ $row->analisa_hrd ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'ttd_pemohon')
                                    <td>{{ $row->ttd_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_atasan_pemohon')
                                    <td>{{ $row->nama_atasan_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'ttd_atasan_pemohon')
                                    <td>{{ $row->ttd_atasan_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'jabatan_atasan_pemohon')
                                    <td>{{ $row->jabatan_atasan_pemohon ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'status_rekaman')
                                    <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nik_input_user')
                                    <td>{{ $row->nik_input_user ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_user_input')
                                    <td>{{ $row->nama_user_input ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'dokumen_terakhir')
                                    <td>{{ $row->dokumen_terakhir ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggal_upload_dokumen_terakhir')
                                    <td>{{ $row->tanggal_upload_dokumen_terakhir ?? 'NO DATA' }}</td>
                                @endif
                                @if($i == count($th) - 1)
                                    <td>
                                        <a href="{{ URL::to('/list_fptk_r/detail/'.$row->id_fptk) }}" class="">Kembali</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_fptk }}">
                                    </td>
                                @endif
                            @endfor
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <table>
                <tr>
                    <td><a class="btn btn-success btn-sm" href="{{route('tambah_fptk')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                    <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                    <td>
                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                    </td>
                </tr>
            </table>
            
    </form>                                
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection