@extends('re.cv.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<!-- <h5 class="m-b-10">Tambah Formulir Permintaan Tenaga Kerja</h5> -->
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<!-- <li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li> -->
									</ul>
								</div>
							</div>
						</div>
					</div> 
					<div> 
						<form action="{{route('update_fptk')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($data_fptk as $data)
							<div class="form-group" style="width:95%;font-size:11px;">
								<input type="hidden" name="id_fptk" id="tg_id" value="{{ $data->id_fptk }}" />
								<div class="row">
									<div class="col">
									<table>
										<tr>
											<td>No ISO</td>
											<td>:</td>
											<td></td>
										</tr>
										<tr>
											<td>No.Dokumen</td>
											<td>:</td>
											<td><input type="hidden" name="logo_perusahaan" style="font-size:11px;" id="" placeholder="">{{$data->no_dokumen}}</td>
										</tr>
										<tr>
											<td>Tanggal</td>
											<td>:</td>
											<td>{{$data->tanggal_mulai_bekerja}}</td>
										</tr>
									</table>
									</div>
									<hr>

								</div>
								<div class="">
									<b>Pemohon Tenaga Kerja</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<table>
												<tr>
													<td>Nama Pemohon</td>
													<td>:</td>
													<td>{{$data->nama_pemohon}}</td>
												</tr>
												<tr>
													<td>Jabatan</td>
													<td>:</td>
													<td>{{$data->jabatan_pemohon}}</td>
												</tr>
												<tr>
													<td>Departemen</td>
													<td>:</td>
													<td>{{$data->departemen_pemohon}}</td>
												</tr>
												<tr>
													<td>Upload Dokumen FPTK Terakhir</td>
													<td>:</td>
													<td><input type="file" name="lampiran_file" style="font-size:11px;" id="" placeholder=""></td>
												</tr>
											</table>
										</div>
										<div class="col" style="font-size: 10px;">
											<table>
												<tr>
													<td>Jumlah Kebutuhan</td>
													<td>:</td>
													<td>{{$data->jumlah_kebutuhan}}</td>
												</tr>
												<tr>
													<td>Jabatan yang Dibutuhkan</td>
													<td>:</td>
													<td>{{$data->jabatan_kepegawaian}}</td>
												</tr>
												<tr>
													<td>Tingkat</td>
													<td>:</td>
													<td>
														<div class="form-group row">
															
															<div class="col-sm-9">
																<div class="form-check">
																	<input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="staff" id="exampleRadios1" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }}>
																	<label class="form-check-label" for="exampleRadios1">
																		staff
																	</label>
																</div><br>
																<div class="form-check">
																	<input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="middle" id="exampleRadios2" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
																	<label class="form-check-label" style="font-size:11px;" for="exampleRadios2">
																		Middle
																	</label>
																</div>
																<div class="form-check">
																	<input class="form-check-input" type="checkbox" name="tingkat_kepegawaian" value="tidak hadir" id="exampleRadios2" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
																	<label class="form-check-label" style="font-size:11px;" for="exampleRadios2">
																		Senior
																	</label>
																</div>
																
															</div>
														</div>	
													{{$data->tingkat_kepegawaian}}</td>
												</tr>
											</table>
										</div>
									</div>
									<hr>
									
									<div style="width: 100%;">
										<table>
										<tr>
											<td>
												<button class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;" type="submit">Simpan</a>
											</td>
											<td>
												<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
											</td>	
										</tr>
										</table>
									</div>
								@endforeach
							<br>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endsection

