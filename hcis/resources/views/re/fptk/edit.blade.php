@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Formulir Permintaan Tenaga Kerja @endslot
@slot('title') Recruitment @endslot
@endcomponent
<form action="{{route('update_fptk')}}" method="post" enctype="multipart/form-data">
<hr>
{{ csrf_field() }}
@foreach($data_rf as $data)
<div class="form-group" style="width:95%;">
	No ISO: 
	<div class="">
		<b>Pemohon Tenaga Kerja</b>
		<div class="row">
			<div class="col" style="font-size: 10px;">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pemohon</label>
					<div class="col-sm-10">
						<input type="hidden" required name="kode_pemohon" value="{{$data->kode_pemohon}}"  style="font-size:11px;" class="form-control form-control-sm" id="kode_pemohon" placeholder=""> 													
						<input type="hidden" required name="nik_pemohon" value="{{$data->nik_pemohon}}" style="font-size:11px;" class="form-control form-control-sm" id="nik" placeholder=""> 													
						<select name="nama_pemohon" id="nama_pemohon" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_atasan="{{$dk->Kode_Karyawan}}" data-kode_jab_atas_pem="{{$dk->Tipe_User}}" data-nik_atasan="{{$dk->nik}}" {{ $dk->Nama_Karyawan == $data->nama_pemohon ? 'selected' : NULL }}>{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select><a style="color:red;">*wajib isi</a>		
						<!-- <input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
					<div class="col-sm-10">
						<input type="text" required name="jabatan_kepegawaian" value="{{$data->jabatan_kepegawaian}}" style="font-size:8px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 													
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Departemen</label>
					<div class="col-sm-10">
						<input type="text" required name="departemen_pemohon" value="{{$data->departemen_pemohon}}" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 													
					</div>
				</div>
			</div>
		</div>
		<hr>
		<b>Permintaan Tenaga Kerja</b>
		<div class="row">
			<div class="col">
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;" for="flexCheckDefault">
						Pengganti Tenaga Kerja
					</label>
					<input class="col-sm-3 form-check-input" style="font-size:11px;" type="checkbox" name="penggantian_tenaga_kerja" value="ya" id="flexCheckDefault" {{ $data->penggantian_tenaga_kerja == 'ya' ? 'checked' : NULL }} >
				</div>
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;" for="flexCheckDefault">
						Penambahan Tenaga Kerja
					</label>
					<input class="col-sm-3 form-check-input" style="font-size:11px;" type="checkbox" name="penambahan_tenaga_kerja" value="ya" id="flexCheckDefault" {{ $data->penambahan_tenaga_kerja == 'ya' ? 'checked' : NULL }}>
				</div>
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;" for="flexCheckDefault">
						Header Hunter
					</label>
					<input class="col-sm-3 form-check-input" style="font-size:11px;" type="checkbox" name="head_hunter" value="ya" id="flexCheckDefault" {{ $data->head_hunter == 'ya' ? 'checked' : NULL }}>
				</div>
			</div>
			<div class="col">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Status Kepegawaian</label>
					<div class="">
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" id="exampleRadios1" name="status_kepegawaian" value="percobaan/tetap" {{ $data->status_kepegawaian == 'percobaan/tetap' ? 'checked' : NULL }}>
							<label class="form-check-label" for="exampleRadios1">
								Percobaan/Tetap
							</label>
						</div>
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" name="status_kepegawaian" id="exampleRadios3" value="kontrak selama" {{ $data->status_kepegawaian == 'kontrak selama' ? 'checked' : NULL }}>
							<label class="form-check-label" for="exampleRadios3">
								Kontrak Selama
							</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input  type="text" class="form-control form-control-sm" name="masa_kontrak" id="exampleRadios2" value="">
						</div>
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" name="status_kepegawaian" id="exampleRadios3" value="magang selama" {{ $data->status_kepegawaian == 'magang selama' ? 'checked' : NULL }}>
							<label class="form-check-label" for="exampleRadios3">
								Magang Selama
							</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input  type="text" class="form-control form-control-sm" name="magang_selama" value="">
						</div>
					</div>
				</div>
			</div>
		<div>
		<hr>
	</div>	
</div>
<hr>
<b>Kebutuhan Tenaga Kerja</b>
<br><br>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan Yang Dibutuhkan</label>
			<div class="col-sm-10">
				<select name="jabatan_kepegawaian" required id="jabatan_kepegawaian" class="form-control form-control-sm" style="font-size:11px;">
					@foreach($data_jbk as $d)
					<option value="{{$d->nama_jabatan}}">{{$d->nama_jabatan}}</option>
					@endforeach
				</select><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
			<div class="col-sm-10">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" style="font-size:11px;" value="staff" name="tingkat_kepegawaian" id="inlineRadio1" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }}>
					<label class="form-check-label" for="inlineRadio1" style="font-size:11px;">Staff</label>
					</div>
					<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio2" value="middle" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
					<label class="form-check-label" for="inlineRadio2" style="font-size:11px;">Middle</label>
					</div>
					<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio3" value="senior" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
					<label class="form-check-label" for="inlineRadio3" style="font-size:11px;">Senior</label>
				</div>
				<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Kebutuhan</label>
			<div class="col-sm-10">
				<input type="text" required name="jumlah_kebutuhan" value="{{$data->jumlah_kebutuhan}}" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" {{ $data->penggantian_tenaga_kerja == 'terisi(pjt)' ? 'checked' : NULL }}><a style="color:red;">*wajib isi</a><small style="font-size:11px;">Orang</small> 													
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Bekerja</label>
			<div class="col-sm-10">
				<input type="date" required name="tanggal_mulai_bekerja" value="{{$data->tanggal_mulai_bekerja}}" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" ><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
			<div class="col-sm-10">
				<select name="lokasi_kerja" required id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
					@foreach($data_lk as $lk)
					<option value="{{$lk->nama_lokasi_kerja}}">{{$lk->nama_lokasi_kerja}}</option>
					@endforeach
				</select><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		
	</div>
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alasan Penambahan Tenaga Kerja</label>
			<div class="col-sm-10">
				<textarea type="text" required rows="4" cols="50" name="alasan_penambahan_tenaga_kerja" value="{{$data->alasan_penambahan_tenaga_kerja}}" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$data->alasan_penambahan_tenaga_kerja}}</textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
	</div>
</div>
<hr>
<b>Kualifikasi Tenaga Kerja</b>		
<br><br>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
			<div class="col-sm-3" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox"  name="jenis_kelamin" value="pria" id="flexCheckDefault" {{ $data->jenis_kelamin == 'pria' ? 'checked' : NULL }}>Pria
			</div>
			<div class="col-sm-3" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="jenis_kelamin" value="wanita" id="flexCheckDefault" {{ $data->jenis_kelamin == 'wanita' ? 'checked' : NULL }}>Wanita
			</div>
		</div>
		<div class="form-group row">
			<table>
				<tr style="">
					<td>
						<div class="form-group row">
							&nbsp;&nbsp;&nbsp;&nbsp;<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Usia</label>
							<div class="col-sm-5">
								<input type="number" required name="usia_minimal" value="{{$data->usia_minimal}}" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
							</div>
						</div>												
					</td>
					<td>
						<div class="form-group row">
							Tahun S/d
						</div>
					</td>
					<td>
						<div class="form-group row">
							<div class="col-sm-5">
								<input type="number" required name="usia_maksimal" style="font-size:8px;" value="{{$data->usia_maksimal}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
							</div>
							<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Usia</label>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status</label>
			<div class="col-sm-3" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="status_kandidat" value="lajang" id="flexCheckDefault" {{ $data->status_kandidat == 'lajang' ? 'checked' : NULL }}>&nbsp;&nbsp;Lajang
			</div>
			<div class="col-sm-3" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="status_kandidat" value="menikah" id="flexCheckDefault" {{ $data->status_kandidat == 'menikah' ? 'checked' : NULL }}>&nbsp;&nbsp;Menikah
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
			<div class="col-sm-3" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 'SMU/Sederajat' ? 'checked' : NULL }}>&nbsp;&nbsp;SMU/Sederajat
			</div><br>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 'd1' ? 'checked' : NULL }}>&nbsp;&nbsp;D1
			</div><br>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 'd2' ? 'checked' : NULL }}>&nbsp;&nbsp;D2
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 'd3' ? 'checked' : NULL }}>&nbsp;&nbsp;D3
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 's1' ? 'checked' : NULL }}>&nbsp;&nbsp;S1
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="{{$data->pendidikan}}" id="flexCheckDefault" {{ $data->pendidikan == 's2' ? 'checked' : NULL }}>&nbsp;&nbsp;S2
			</div>
		</div>
	</div>
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman</label>
			<div class="col-sm-10">
				<textarea type="text" value="Minimal memiliki pengalaman kerja ...." required rows="4" cols="50" name="pengalaman" style="font-size:8px;" value="{{$data->pengalaman}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$data->pengalaman}}</textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kemampuan Lainnya</label>
			<div class="col-sm-10">
				<textarea type="text" value="Mahir dalam ...." required rows="4" cols="50" name="kemampuan_lainnya" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{$data->kemampuan_lainnya}}</textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
	</div>
</div>
<hr>
<b>Kopensasi Tenaga Kerja</b>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Gaji</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="number" name="gaji" value="{{$data->gaji}}" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tunjangan Kesehatan</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" value="{{$data->tunjangan_kesehatan}}" name="tunjangan_kesehatan" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tunjangan Transport</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" value="{{$data->tunjangan_transportasi}}" name="tunjangan_transportasi" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tunjangan Makan</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" value="{{$data->tunjangan_makan}}" name="tunjangan_makan" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tunjangan Komunikasi</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" value="{{$data->tunjangan_komunikasi}}" name="tunjangan_komunikasi" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Hari Kerja</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="hari_kerja" value="{{$data->hari_kerja}}" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jam Kerja</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" value="{{$data->tunjangan_transportasi}}" name="tunjangan_transportasi" id="flexCheckDefault">
			</div>
		</div>
		<hr>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Lampiran File</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="file" name="lampiran_file" value="{{$data->lampiran_file}}" id="flexCheckDefault">
				{{$data->lampiran_file}}
			</div>
		</div>
		<hr>
		<b>Persetujuan Permintaan Tenaga Kerja</b>
		<div class="row">
			<div class="col">
				<div class="form-group row">
				<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diajukan</label>
				<div class="col-sm-10" style="font-size:11px;">
					<!-- <input type="hidden" required name="kode_pemoh" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_pemoh" placeholder=""> 													
					<input type="hidden" required name="nik_pemoh" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik_pemoh" placeholder=""> 													 -->
					<select name="nama_pemohon" id="nama_pemoh" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
						<option value="" selected disabled>--- Pilih ---</option>
						@foreach($data_k['looks'] as $dk)
						<option value="{{$dk->Nama_Karyawan}}" data-kode_pemoh="{{$dk->Kode_Karyawan}}" data-nik_pemoh="{{$dk->nik}}" data-nama_pemohon="{{$dk->Nama_Karyawan}}" data-kode_jab_pemoh="{{$dk->Tipe_User}}" {{ $dk->Nama_Karyawan == $data->nama_pemohon ? 'selected' : NULL }}>{{$dk->Nama_Karyawan}}</option>
						@endforeach
					</select><a style="color:red;">*wajib isi</a>								
				</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diperiksa</label>
					<div class="col-sm-10" style="font-size:11px;">
						<!-- <input required class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_atasan_pemohon" id="flexCheckDefault"> -->
						<input type="hidden" required name="kode_atasan_pemohon" value="{{$data->kode_atasan_pemohon}}"  style="font-size:11px;" class="form-control form-control-sm" id="kode_atasan_pemohon" placeholder=""> 													
						<input type="hidden" required name="nik_atasan" value="{{$data->nik_atasan}}" style="font-size:11px;" class="form-control form-control-sm" id="nik_atasan" placeholder=""> 													
						<select name="nama_atasan_pemohon" id="nama_atasan_pemohon" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_atasan="{{$dk->Kode_Karyawan}}" data-kode_jab_atas_pem="{{$dk->Tipe_User}}" data-nik_atasan="{{$dk->nik}}" {{ $dk->Nama_Karyawan == $data->nama_atasan_pemohon ? 'selected' : NULL }}>{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select><a style="color:red;">*wajib isi</a>		
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Disetujui</label>
					<div class="col-sm-10" style="font-size:11px;">
						<input type="hidden" required name="kode_penyetuju" value="{{$data->kode_penyetuju}}"  style="font-size:11px;" class="form-control form-control-sm" id="kode_penyetuju" placeholder=""> 													
						<input type="hidden" required name="nik_penyetuju" value="{{$data->nik_penyetuju}}" style="font-size:11px;" class="form-control form-control-sm" id="nik_penyetuju" placeholder=""> 													
						<select name="nama_penyetuju" id="nama_penyetuju" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_penyetuju="{{$dk->Kode_Karyawan}}" data-nik_penyetuju="{{$dk->nik}}" {{ $dk->Nama_Karyawan == $data->nama_penyetuju ? 'selected' : NULL }}>{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select><a style="color:red;">*wajib isi</a>		
						<!-- <input required class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_penyetuju" id="flexCheckDefault"> -->
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diperiksa/Dianalisa</label>
					<div class="col-sm-10" style="font-size:11px;">
						<input type="hidden" required name="kode_penganalisa" value="{{$data->kode_penganalisa}}"  style="font-size:11px;" class="form-control form-control-sm" id="kode_penganalisa" placeholder=""> 													
						<input type="hidden" required name="nik_penganalisa" value="{{$data->nik_penganalisa}}" style="font-size:11px;" class="form-control form-control-sm" id="nik_penganalisa" placeholder=""> 													
						<select name="nama_penganalisa" id="nama_penganalisa" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							   <option value="{{$dk->Nama_Karyawan}}" data-nik_penganalisa="{{$dk->nik}}" data-kode_penganalisa="{{$dk->Kode_Karyawan}}" {{ $dk->Nama_Karyawan == $data->nama_penganalisa ? 'selected' : NULL }}>{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select>
						<!-- <input class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_penganalisa" id="flexCheckDefault"> -->
					</div>
				</div>
			</div>
			<div class="col">
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;"> 
						<input readonly class="form-control form-control-sm" value="{{$data->jabatan_kepegawaian}}" style="font-size:11px;" type="text" name="jabatan_pemohon" id="jabatan_pemohon">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" value="{{$data->jabatan_atasan_pemohon}}" style="font-size:11px;" type="text" name="jabatan_atasan_pemohon" id="jabatan_atasan_pemohon">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" name="jabatan_penyetuju" value="GM/HD/GM NHD/GM Operasional/Corporate" style="font-size:11px;" type="text" id="flexCheckDefault">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" name="analisa_hrd" value="HR Dept. Head" style="font-size:11px;" type="text" id="flexCheckDefault">
					</div>
				</div>
			</div>
		</div>
</div>
<div style="width: 100%;">
	<table>
	<tr>
		<!-- <td>
			<input type="text">
		</td> -->
		<td>
			<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
		</td>
		<td>
			<button type="submit" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
		</td>
		<td>
			<a href="{{ route('hapus_fptk', $data->id_fptk) }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;" id="btn_delete">Hapus</a>
		</td>
	</tr>
	</table>
</div>
@endforeach
<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.nastable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

<script>
$('#nama_pemohon').change(function(){
	$('#kode_pemohon').val($('#nama_pemohon option:selected').data('kode_p'));
	$('#nik').val($('#nama_pemohon option:selected').data('nik'));
	$('#jabatan_kepegawaian').val($('#nama_pemohon option:selected').data('kode_jab_pem'));	
	
}) 
</script>

<!-- ttd -->
<script>
$('#nama_pemohon').change(function(){
	$('#jabatan_pemohon').val($('#nama_pemohon option:selected').data('kode_jab_pem'));
})
</script>

<script>
$('#nama_pemoh').change(function(){
	$('#jabatan_pemohon').val($('#nama_pemoh option:selected').data('kode_jab_pemoh'));
})
</script>

<script>
	$('#nama_pemohon').change(function() {
        $('#nama_pemoh').val($('#nama_pemohon option:selected').data('nama_pemoh'));
    })
    $('#nama_pemoh').change(function() {
        $('#nama_pemohon').val($('#nama_pemoh option:selected').data('nama_pemohon'));
    })
</script>

<script>
$('#nama_pemoh').change(function(){
	$('#kode_pemoh').val($('#nama_pemoh option:selected').data('kode_pemoh'));
	$('#nik_pemoh').val($('#nama_pemoh option:selected').data('nik_pemoh'));
})
</script>

<script>
$('#nama_atasan_pemohon').change(function(){
	$('#jabatan_atasan_pemohon').val($('#nama_atasan_pemohon option:selected').data('kode_jab_atas_pem'));
	$('#kode_atasan_pemohon').val($('#nama_atasan_pemohon option:selected').data('kode_atasan'));
	$('#nik_atasan').val($('#nama_atasan_pemohon option:selected').data('nik_atasan'));
})
</script>
<script>
$('#nama_penyetuju').change(function(){
	$('#kode_penyetuju').val($('#nama_penyetuju option:selected').data('kode_penyetuju'));
	$('#nik_penyetuju').val($('#nama_penyetuju option:selected').data('nik_penyetuju'));
})
</script>
<script>
$('#nama_penganalisa').change(function(){
	$('#kode_penganalisa').val($('#nama_penganalisa option:selected').data('kode_penganalisa'));
	$('#nik_penganalisa').val($('#nama_penganalisa option:selected').data('nik_penganalisa'));
})
</script>

@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection
