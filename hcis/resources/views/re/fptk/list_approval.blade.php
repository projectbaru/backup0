@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Workstrukture @endslot
@slot('li_3') Alamat Perusahaan @endslot
@slot('title') Workstrukture @endslot
@endcomponent
<form action="{{ URL::to('/list_fptk/hapus_banyak') }}" method="POST" id="form_delete">
    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;color:black;">
        <thead style="color:black;font-size: 14px;">
            <tr>
                <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">No</th>
                <th style="width:3px;color:black;font-size: 14px;background-color:#a5a5a5; padding:-70px;">Nama Pemohon</th>
                <th style="color:black;font-size: 14px;background-color:#a5a5a5; padding-right:-60px;" scope="col">No Dokumen</th>
                <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Jabatan Yang Dibutuhkan</th>
                <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Tingkat</th>
                <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Jumlah Kebutuhan</th>
                <th style="color:black;font-size: 14px; background-color:#a5a5a5;" scope="col">Lampiran</th>
                <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Status</th>
                <th style="color:black;font-size: 14px;background-color:#a5a5a5;" scope="col">Action</th>
            </tr>
        </thead>
        <tbody style="font-size: 10px;color:black;">
        @php $b=1; @endphp
            @foreach($rc_fptk as $data)
            {{-- {{dd($data)}} --}}
                <tr>
                    <td>{{$b++;}}</td>
                    <td style="font-size: 12px;color:black;">{{$data->nama_pemohon}}</td>
                    <td style="font-size: 12px;color:black;">{{$data->no_dokumen}}</td>
                    <td style="font-size: 12px;color:black;">{{$data->jabatan_pemohon}}</td>
                    <td style="font-size: 12px;color:black;">{{$data->tingkat_kepegawaian}}</td>
                    <td style="font-size: 12px;color:black;">{{$data->jumlah_kebutuhan}}</td>
                    <td style="font-size: 12px;color:black;"><a href="{{url('/data_file/'.$data->lampiran_file)}}" download>Lampiran</a></td>
                    <td style="font-size: 12px;color:black;">{{$data->status}}</td>                                                          
                    <td>
                        <a href="{{ URL::to('/list_fptk/preview/'.$data->id_fptk) }}" style="font-size:9px;" class="btn btn-primary btn-sm">Preview</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <table>
        <tr>
            <td>
                <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_fptk')}}">Tambah</a></td>
            <td>
                <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                <button class="btn btn-danger btn-sm"  style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
            </td>
        </tr>
    </table>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.nastable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
@endpush

@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection