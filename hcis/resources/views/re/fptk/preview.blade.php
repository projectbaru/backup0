
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container-fluid">
        <table class="table table-responsive-xl table-bordered mt-2" style="font-size:10px;color:black;">
        @foreach($data as $data)
            <tbody>
                <tr>
                    <td rowspan="2">logo</td>
                    <td colspan="4" style="text-align:center;font-size:20px;"><b>FORMULIR PERMINTAAN TENAGA KERJA (FPTK)</b></td>
                </tr>
                <tr>
                    <td>No.doc</td>
                    <td>Revisi: </td>
                    <td>Tanggal: </td>
                    <td>Hal 1 dari 1</td>
                </tr>
                <tr>
                    <td colspan="5"><b>KOLOM PEMOHON TENAGA KERJA(Diisi Oleh Pemohon)</b></td>
                </tr>
                <tr>
                    <td colspan="2">Nama Pemohon</td>
                    <td colspan="4">{{$data->nama_pemohon}}</td>
                </tr>
                <tr>
                    <td colspan="2">Jabatan</td>
                    <td colspan="4">{{$data->jabatan_pemohon}}</td>
                </tr>
                <tr>
                    <td colspan="2">Departemen</td>
                    <td colspan="4">{{$data->departemen_pemohon}}</td>
                </tr>
                <tr>
                    <td colspan="5"><b>KOLOM PERMINTAAN TENAGA KERJA(Diisi Oleh Pemohon)<b></td>
                </tr>
                <tr>
                    <td>Penggantian Tenaga Kerja</td>
                    <td><input class="form-check-input" style="font-size:8px;" readonly type="checkbox" onclick="return false" name="penggantian_tenaga_kerja" value=""></td>
                    <td colspan="4" rowspan="3">
                        Status Kepegawaian : 
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" readonly value="percobaan/tetap" onclick="return false" id="flexCheckDefault" {{ $data->penggantian_tenaga_kerja == 'percobaan/tetap' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Percobaan/Tetap
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" readonly value="kontrak selama" onclick="return false" id="flexCheckChecked" {{ $data->penggantian_tenaga_kerja == 'kontrak selama' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Kontrak Selama: {{$data->penggantian_tenaga_kerja}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" readonly value="magang selama" onclick="return false" id="flexCheckChecked" {{ $data->penggantian_tenaga_kerja == 'magang selama' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Magang Selama: {{$data->penggantian_tenaga_kerja}}
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Penambahan Tenaga Kerja</td>
                    <td colspan="4" style="text-align:left;"><input class="form-check-input" onclick="return false" style="font-size:11px;" type="checkbox" name="penambahan_tenaga_kerja" value="{{$data->penambahan_tenaga_kerja}}"></td>
                </tr>
                <tr>
                    <td>Head Hunter</td>
                    <td colspan="4"><input class="form-check-input" onclick="return false" style="font-size:11px;" type="checkbox" name="head_hunter" value="{{$data->head_hunter}}"></td>
                </tr>
                <tr>
                    <td colspan="5"><b>KOLOM KEBUTUHAN TENAGA KERJA(Diisi Oleh Pemohon)</b></td>
                </tr>
                <tr>
                    <td colspan="2">Jabatan yang dibutuhkan</td>
                    <td colspan="4">{{$data->jabatan_kepegawaian}}</td>
                </tr>
                <tr>
                    <td colspan="2">Level</td>
                    <td colspan="4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="staff" onclick="return false" readonly id="flexCheckDefault" {{ $data->tingkat_kepegawaian == 'staff' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Staff
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="middle" onclick="return false" readonly id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'middle' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Middle
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" onclick="return false" readonly id="flexCheckChecked" {{ $data->tingkat_kepegawaian == 'senior' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckChecked">
                                Senior
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Jumlah Kebutuhan</td>
                    <td colspan="4">{{$data->jumlah_kebutuhan}} Orang</td>
                </tr>
                <tr>
                    <td colspan="2">Tanggal Mulai Bekerja</td>
                    <td colspan="4">{{date('d-m-Y', strtotime($data->tanggal_mulai_bekerja))}}</td>
                </tr>
                <tr>
                    <td colspan="2">Lokasi Kerja</td>
                    <td colspan="4">{{$data->lokasi_kerja}}</td>
                </tr>
                <tr>
                    <td colspan="5"><b>KOLOM KOMPENSASI TENAGA KERJA(Diisi Oleh HRD)</b></td>
                </tr>
                <tr>
                    <td colspan="2">
                        Gaji &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" onclick="return false" value="option1" {{ $data->gaji == 'middle' ? 'checked' : NULL }}>
                        <label class="form-check-label" for="inlineCheckbox1">Gross</label>
                        </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" onclick="return false" value="option2">
                        <label class="form-check-label" for="inlineCheckbox2">Nett</label>
                        </div>
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            Rp.{{$data->gaji}}/bulan
                            <!-- <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Nett
                            </label> -->
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Tunjangan Kesehatan 
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->tunjangan_kesehatan}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Tunjangan Transport
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->tunjangan_transportasi}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Tunjangan Makan
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->tunjangan_makan}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Tunjangan Komunikasi
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->tunjangan_komunikasi}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Hari Kerja
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->hari_kerja}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Jam Kerja  
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{$data->jam_kerja}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><b>KOLOM JABATAN TENAGA KERJA (Diisi oleh Pemohon)</b></td>
                </tr>
                <tr>
                    <td colspan="5">Alasan Penambahan Teanga Kerja:</td>
                </tr>
                <tr>
                    <td colspan="5">
                    <b>
                    KOLOM KUALIFIKASI TENAGA KERJA (Diisi oleh Pemohon)
                    </b>       
                </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Jenis Kelamin
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="jenis_kelamin" value="pria" id="flexCheckDefault" onclick="return false" {{ $data->jenis_kelamin == 'pria' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Pria
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="jenis_kelamin" type="checkbox" value="wanita" id="flexCheckDefault" onclick="return false" {{ $data->jenis_kelamin == 'wanita' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Wanita
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Usia
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                        {{$data->usia_minimal}} s/d {{$data->usia_maksimal}}  &nbsp;&nbsp;tahun
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Status                    
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            <input class="form-check-input" name="status_kandidat" type="checkbox" value="{{$data->status_kandidat}}" onclick="return false" id="flexCheckDefault" {{ $data->status_kandidat == 'lajang' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Single
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="status_kandidat" type="checkbox" value="{{$data->status_kandidat}}" onclick="return false" id="flexCheckDefault" {{ $data->status_kandidat == 'menikah' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                Menikah
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Pendidikan
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 'SMU Sederajat' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                SMU Sederajat
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 'd1' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                D1
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 'd2' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                D2
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 'd3' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                D3
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 's1' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                S1
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" name="pendidikan" type="checkbox" value="{{$data->pendidikan}}" onclick="return false" id="flexCheckDefault" {{ $data->pendidikan == 's2' ? 'checked' : NULL }}>
                            <label class="form-check-label" for="flexCheckDefault">
                                S2
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <b>
                            KOLOM PERSETUJUAN
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Tanggal
                    </td>
                    <td colspan="4">
                        <div class="form-check">
                            {{date('d-m-Y', strtotime($data->tanggal_mulai_bekerja))}}
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        Diajukan   
                    </td>
                    <td colspan="2" style="text-align:center;">
                        Diperiksa
                    </td>
                    <td colspan="2" style="text-align:center;">
                        Disetujui
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        Pemohon
                    </td>
                    <td colspan="2" style="text-align:center;">
                        Atasan Pemohon
                    </td>
                    <td colspan="2" style="text-align:center;">
                        GM HD/GM NHD/GM Operasional/Corporate Service
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form-check">
                            <textarea name="" id="" cols="30" rows="5" style="border:none;"></textarea>  <br>
                            <p style="text-align:center;"> ({{$data->nama_pemohon}})  </p>                    
                        </div>    
                    </td>
                    <td colspan="2">
                        <div class="form-check">
                            <textarea name="" id="" cols="30" rows="5" style="border:none;"></textarea>  <br>
                        <p style="text-align:center;"> ({{$data->nama_atasan_pemohon}}) </p>                    
                        </div>    
                    </td>
                    <td colspan="2">
                        <div class="form-check">
                            <textarea name="" id="" cols="30" rows="5" style="border:none;"></textarea>
                            <p style="text-align:center;">({{$data->nama_atasan_pemohon}}) </p>                        
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Nama & Tanda Tangan Jelas
                    </td>
                    <td colspan="2">
                        Nama & Tanda Tangan Jelas
                    </td>
                    <td colspan="2">
                        Nama & Tanda Tangan Jelas
                    </td>
                </tr>
                <tr rowspan="">
                    <td colspan="2">
                        <div class="form-check">
                            Analisa HRD <br>
                            <textarea name="analisa_hrd" id="analisa_hrd" cols="30" rows="5" value="{{$data->analisa_hrd}}" style="border:none;">{{$data->analisa_hrd}}</textarea>                        
                        </div>    
                    </td>
                    <td colspan="2">
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Hasil Permohonan:
                            </label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="setuju" onclick="return false" id="flexCheckDefault" {{ $data->hasil_penyetujuan_hr == 'setuju' ? 'checked' : NULL }}>
                                <label class="form-check-label" for="flexCheckDefault">
                                    Disetujui
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="tolak" onclick="return false" id="flexCheckDefault" {{ $data->hasil_penyetujuan_hr == 'ditolak' ? 'checked' : NULL }}>
                                <label class="form-check-label" for="flexCheckDefault">
                                    Ditolak
                                </label>
                            </div>
                        </div>    
                    </td>
                    <td colspan="2">
                        <div class="form-check">
                            Diperiksa/ Dianalisa,                    
                        </div>    
                        <div class="form-check">
                            HR Dept Head
                        </div>    
                        <div class="form-check">
                            <textarea name="" id="" style="border:none;" cols="30" rows="5"></textarea>
                        </div>    
                        <div class="form-check" style="text-align:center;">
                            Yudhi Chandra
                        </div>
                        <div class="form-check" style="text-align:center;">
                            Nama & Tanda Tangan Jelas
                        </div>    
                    </td>
                </tr>
                <tr rowspan="">
                    <td colspan="2">
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Catatan:
                            </label><br>
                            <textarea name="" id="" cols="30" rows="5" style="border:none;"></textarea>                        
                        </div>    
                    </td>
                    <td colspan="2">
                        <div class="form-check">
                            <label class="form-check-label" for="flexCheckDefault">
                                Hasil Permohonan:
                            </label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="disetujui" onclick="return false" id="flexCheckDefault" {{ $data->hasil_penyetujuan_hr == 'setuju' ? 'checked' : NULL }}>
                                <label class="form-check-label" for="flexCheckDefault">
                                    Disetujui
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="ditolak" onclick="return false" id="flexCheckDefault" {{ $data->hasil_penyetujuan_hr == 'ditolak' ? 'checked' : NULL }}>
                                <label class="form-check-label" for="flexCheckDefault">
                                    Ditolak
                                </label>
                            </div>        
                        </div>    
                    </td>
                    <td colspan="2" style="text-align:center;">
                        <div class="form-check">
                            Diketahui,                    
                        </div>    
                        <div class="form-check">
                            <textarea name="" id="" style="border:none;" cols="30" rows="5"></textarea>
                        </div>    
                        <div class="form-check">
                            <b>PIMPINAN</b>
                        </div>    
                    </td>
                </tr>
            </tbody>
        @endforeach
        </table>
    </div>
</body>
</html>