@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
	.form-group{
		color:black;
	}
	
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitmen @endslot
@slot('title') Rekruitmen @endslot
@endcomponent
<form action="{{route('simpan_fptk')}}" method="POST" enctype="multipart/form-data">
<hr>
{{ csrf_field() }}
<div class="form-group">
	No ISO: 
	<div class="">
		<b>Pemohon Tenaga Kerja</b><br><br>
		<div class="row">
			<div class="col" style="font-size: 10px;">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Nama Pemohon</label>
					<div class="col-sm-10">
						<!-- <select name="nama_pemohon" id="nama_pemohon" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							<option value="">nama 1</option>
							<option value="">nama 2</option>
						</select>		 -->
						<input type="hidden" required name="kode_pemohon" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_pemohon" placeholder=""> 													
						<input type="hidden" required name="nik_pemohon" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik" placeholder=""> 													
						<select name="nama_pemohon" id="nama_pemohon" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_p="{{$dk->Kode_Karyawan}}" data-nama_pemoh="{{$dk->Nama_Karyawan}}" data-jabatan="{{$dk->Nama_Karyawan}}" data-kode_jab_pem="{{$dk->Tipe_User}}" data-nik="{{$dk->nik}}">{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select><a style="color:red;">*wajib isi</a>
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jabatan</label>
					<div class="col-sm-10">
						<input type="text" required name="jabatan_kepegawaian" style="font-size:11px;" class="form-control form-control-sm" id="jabatan_kepegawaian" placeholder=""><a style="color:red;">*wajib isi</a> 													
					</div>
				</div>
				<input type="hidden" name="kode_karyawan" style="font-size:11px;" class="form-control form-control-sm" id="kode_karyawan" placeholder=""> 												
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;"></label>
					<div class="col-sm-10">
						<input type="hidden" required name="status" value="" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Departemen</label>
					<div class="col-sm-10">
						<input type="text" required name="departemen_pemohon" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a> 													
					</div>
				</div>
			</div>
		</div>
		<hr>
		<b>Permintaan Tenaga Kerja</b>
		<div class="row">
			<div class="col">
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;color:black;" for="flexCheckDefault">
						Pengganti Tenaga Kerja
					</label>
					<input class="col-sm-2 form-check-input" style="font-size:11px;" type="checkbox" name="penggantian_tenaga_kerja" value="Ya" id="flexCheckDefault">
				</div>
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;color:black;" for="flexCheckDefault">
						Penambahan Tenaga Kerja
					</label>
					<input class="col-sm-2 form-check-input" style="font-size:11px;" type="checkbox" name="penambahan_tenaga_kerja" value="Ya" id="flexCheckDefault">
				</div>
				<div class="form-group ">
					<label class="col-sm-4 form-check-label" style="font-size:11px;color:black;" for="flexCheckDefault">
						Header Hunter
					</label>
					<input class="col-sm-2 form-check-input" style="font-size:11px;" type="checkbox" name="head_hunter" value="Ya" id="flexCheckDefault">
				</div>
			</div>
			<div class="col">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" >Status Kepegawaian</label>
					<div class="">
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" id="exampleRadios1" name="status_kepegawaian" value="percobaan/tetap" checked>
							<label class="form-check-label" for="exampleRadios1">
								Percobaan/Tetap
							</label>
						</div>
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" name="status_kepegawaian" id="exampleRadios3" value="kontrak selama">
							<label class="form-check-label" for="exampleRadios3">
								Kontrak Selama
							</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input  type="text" class="form-control form-control-sm" name="masa_kontrak" id="exampleRadios2" value="">
						</div>
						<div class="form-check" style="font-size:11px;">
							<input class="form-check-input" type="radio" name="status_kepegawaian" id="exampleRadios3" value="magang selama">
							<label class="form-check-label" for="exampleRadios3">
								Magang Selama
							</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input  type="text" class="form-control form-control-sm" name="magang_selama" id="exampleRadios2" value="">
						</div>
					</div>
				</div>
			</div>
		<div>
		<hr>
		
		
	</div>	
	
</div>
<hr>
<b>Kebutuhan Tenaga Kerja</b>
<br><br>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jabatan Yang Dibutuhkan</label>
			<div class="col-sm-10">
				<select name="jabatan_kepegawaian" id="jabatan_kepegawaian" class="form-control form-control-sm" style="font-size:11px;">
				<option value="" selected disabled>--- Pilih ---</option>
				@foreach($data_jb['looks'] as $data)
					<option value="{{$data->nama_jabatan}}">{{$data->nama_jabatan}}</option>
				@endforeach
				</select><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tingkat</label>
			<div class="col-sm-10">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio1" value="staff">
					<label class="form-check-label" for="inlineRadio1">Staff</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio2" value="middle">
					<label class="form-check-label" for="inlineRadio2">Middle</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="tingkat_kepegawaian" id="inlineRadio3" value="senior">
					<label class="form-check-label" for="inlineRadio3">Senior</label>
				</div>
				<!-- <div class="form-check form-check-inline">
					
					<input class="form-check-input" type="checkbox" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio1" value="staff">
					<label class="form-check-label" for="inlineRadio1" style="font-size:11px;">Staff</label>
					</div>
					<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio2" value="middle">
					<label class="form-check-label" for="inlineRadio2" style="font-size:11px;">Middle</label>
					</div>
					<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" style="font-size:11px;" name="tingkat_kepegawaian" id="inlineRadio3" value="senior">
					<label class="form-check-label" for="inlineRadio3" style="font-size:11px;">Senior</label>
				</div> -->
				<!-- <input type="text" required name="tingkat_kepegawaian" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													 -->
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jumlah Kebutuhan<a style="color:red;">*</a></label>
			<div class="col-sm-8">
				<input type="text" required name="jumlah_kebutuhan" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
			</div>
			<div class="col-sm-2">
				<small>Orang</small>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tanggal Mulai Bekerja</label>
			<div class="col-sm-10">
				<input type="date" required name="tanggal_mulai_bekerja" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Lokasi Kerja</label>
			<div class="col-sm-10">
				<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
					<option value="">-</option>
					
				</select><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		
	</div>
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Alasan Penambahan Tenaga Kerja</label>
			<div class="col-sm-10">
				<textarea type="text" required rows="4" cols="50" name="alasan_penambahan_tenaga_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
	</div>
</div>
<hr>
<b>Kualifikasi Tenaga Kerja</b>		
<br><br>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jenis Kelamin</label>
			<div class="col-sm-2" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="jenis_kelamin" value="pria" id="flexCheckDefault">&nbsp;&nbsp;Pria
			</div>
			<div class="col-sm-2" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="jenis_kelamin" value="wanita" id="flexCheckDefault">&nbsp;&nbsp;Wanita
			</div>
		</div>
		<div class="form-group row">
			<table>
				<tr>
					<td>
						<div class="form-group row">
							&nbsp;&nbsp;&nbsp;&nbsp;<label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Usia<a style="color:red;">*</a></label>
							<div class="col-sm-2">
								<input type="number" required name="usia_minimal" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								 													
							</div>
							<div class="col-sm-1">
								<p class="" style="font-size:10px;">
									Tahun S/d
								</p>
							</div>
							
							<div class="col-sm-5">
								<input type="number" required name="usia_maksimal" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 													
							</div>
							<div class="col-sm-2">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tahun</label>
							</div>
							
							
						</div>												
					</td>
					
					<td>
						<div class="form-group row">
							
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Status</label>
			<div class="col-sm-2" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="status_kandidat" value="lajang" id="flexCheckDefault">Lajang
			</div>
			<div class="col-sm-2" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="status_kandidat" value="menikah" id="flexCheckDefault">Menikah
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Pendidikan</label>
			<div class="col-sm-2" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="SMU/Sederajat" id="flexCheckDefault">&nbsp;&nbsp;SMU/Sederajat
			</div><br>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="d1" id="flexCheckDefault">&nbsp;&nbsp;D1
			</div><br>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="d2" id="flexCheckDefault">&nbsp;&nbsp;D2
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="d3" id="flexCheckDefault">&nbsp;&nbsp;D3
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="s1" id="flexCheckDefault">&nbsp;&nbsp;S1
			</div>
			<div class="col-sm-1" style="font-size:11px;">
				<input class="form-check-input" style="font-size:11px;" type="checkbox" name="pendidikan" value="s2" id="flexCheckDefault">&nbsp;&nbsp;S2
			</div>
		</div>
	</div>
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Pengalaman</label>
			<div class="col-sm-10">
				<textarea type="text" required rows="4" cols="50" name="pengalaman" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Kemampuan Lainnya</label>
			<div class="col-sm-10">
				<textarea type="text" required rows="4" cols="50" name="kemampuan_lainnya" style="font-size:8px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea><a style="color:red;">*wajib isi</a>
			</div>
		</div>
	</div>
</div>
<hr>
<b>Kopensasi Tenaga Kerja</b>
<div class="row">
	<div class="col">
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Gaji</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="number" name="gaji" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tunjangan Kesehatan</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="tunjangan_kesehatan" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tunjangan Transport</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="tunjangan_transportasi" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tunjangan Makan</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="tunjangan_makan" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Tunjangan Komunikasi</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="tunjangan_komunikasi" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Hari Kerja</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="hari_kerja" id="flexCheckDefault">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jam Kerja</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="text" name="jam_kerja" id="flexCheckDefault">
			</div>
		</div>
		<hr>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Lampiran File</label>
			<div class="col-sm-10" style="font-size:11px;">
				<input class="form-control form-control-sm" style="font-size:11px;" type="file" name="lampiran_file" id="flexCheckDefault">
			</div>
		</div>
		<hr>
		<b>Persetujuan Permintaan Tenaga Kerja</b>
		<br><br>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diajukan<a style="color:red;">*</a></label>
					<div class="col-sm-8" style="font-size:11px;">
						<!-- <input type="hidden" required name="kode_pemohon" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_pemoh" placeholder=""> 													 -->
						<!-- <input type="hidden" required name="nik_pemoh" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik_pemoh" placeholder=""> 													 -->
						<select name="nama_pemohon" id="nama_pemoh" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_pemoh="{{$dk->Kode_Karyawan}}" data-nik_pemoh="{{$dk->nik}}" data-nama_pemohon="{{$dk->Nama_Karyawan}}" data-kode_jab_pemoh="{{$dk->Tipe_User}}" data-kode_karyawan="{{$dk->Kode_Karyawan}}">{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select>								
						
					</div>
				</div>
				
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diperiksa<a style="color:red;">*</a></label>
					<div class="col-sm-8" style="font-size:11px;">
						<!-- <input required class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_atasan_pemohon" id="flexCheckDefault"> -->
						<input type="hidden" required name="kode_atasan_pemohon" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_atasan_pemohon" placeholder=""> 													
						<input type="hidden" required name="nik_atasan" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik_atasan" placeholder=""> 													
						<select name="nama_atasan_pemohon" id="nama_atasan_pemohon" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_atasan="{{$dk->Kode_Karyawan}}" data-kode_jab_atas_pem="{{$dk->Tipe_User}}" data-nik_atasan="{{$dk->nik}}">{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Disetujui<a style="color:red;">*</a></label>
					<div class="col-sm-8" style="font-size:11px;">
						<input type="hidden" required name="kode_penyetuju" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_penyetuju" placeholder=""> 													
						<input type="hidden" required name="nik_penyetuju" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik_penyetuju" placeholder=""> 													
						<select name="nama_penyetuju" id="nama_penyetuju" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-kode_penyetuju="{{$dk->Kode_Karyawan}}" data-nik_penyetuju="{{$dk->nik}}">{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select>	
						<!-- <input required class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_penyetuju" id="flexCheckDefault"> -->
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Diperiksa/Dianalisa<a style="color:red;">*</a></label>
					<div class="col-sm-8" style="font-size:11px;">
						<input type="hidden" required name="kode_penganalisa" value=""  style="font-size:11px;" class="form-control form-control-sm" id="kode_penganalisa" placeholder=""> 													
						<input type="hidden" required name="nik_penganalisa" value="" style="font-size:11px;" class="form-control form-control-sm" id="nik_penganalisa" placeholder=""> 													
						<select name="nama_penganalisa" id="nama_penganalisa" style='font-size:11px;' required class="form-control form-control-sm" data-number="00">
							<option value="" selected disabled>--- Pilih ---</option>
							@foreach($data_k['looks'] as $dk)
							<option value="{{$dk->Nama_Karyawan}}" data-nik_penganalisa="{{$dk->nik}}" data-kode_penganalisa="{{$dk->Kode_Karyawan}}">{{$dk->Nama_Karyawan}}</option>
							@endforeach
						</select>
						<!-- <input class="form-control form-control-sm" style="font-size:11px;" type="text" name="nama_penganalisa" id="flexCheckDefault"> -->
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;"> 
						<input readonly class="form-control form-control-sm" value="" style="font-size:11px;" type="text" name="jabatan_pemohon" id="jabatan_pemohon">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" style="font-size:11px;" type="text" name="jabatan_atasan_pemohon" id="jabatan_atasan_pemohon">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" name="jabatan_penyetuju" value="GM/HD/GM NHD/GM Operasional/Corporate" style="font-size:11px;" type="text" id="flexCheckDefault">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10" style="font-size:11px;">
						<input readonly class="form-control form-control-sm" name="analisa_hrd" value="HR Dept. Head" style="font-size:11px;" type="text" id="flexCheckDefault">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="width: 100%;">
	<table>
	<tr>
		<!-- <td>
			<input type="text">
		</td> -->
		<td>
			<button href="{{ route('simpan_fptk') }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
		</td>
		<td>
			<a href="{{ route('list_fptk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
		</td>
	</tr>
	</table>
</div>
<br>
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.nastable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script>
$('#nama_pemohon').change(function(){
	$('#kode_pemohon').val($('#nama_pemohon option:selected').data('kode_p'));
	$('#nik').val($('#nama_pemohon option:selected').data('nik'));
}) 
</script>
<script>
$('#nama_pemohon').change(function(){
	$('#jabatan_kepegawaian').val($('#nama_pemohon option:selected').data('kode_jab_pem'));	
	$('#kode_karyawan').val($('#nama_pemohon option:selected').data('kode_karyawan'));	
}) 
</script>



<!-- ttd -->
<script>
$('#nama_pemohon').change(function(){
	$('#jabatan_pemohon').val($('#nama_pemohon option:selected').data('kode_jab_pem'));
})
</script>

<script>
$('#nama_pemoh').change(function(){
	$('#jabatan_pemohon').val($('#nama_pemoh option:selected').data('kode_jab_pemoh'));
})
</script>

<script>
	$('#nama_pemohon').change(function() {
        $('#nama_pemoh').val($('#nama_pemohon option:selected').data('nama_pemoh'));
    })
    $('#nama_pemoh').change(function() {
        $('#nama_pemohon').val($('#nama_pemoh option:selected').data('nama_pemohon'));
    })
</script>

<script>
$('#nama_pemoh').change(function(){
	$('#kode_pemoh').val($('#nama_pemoh option:selected').data('kode_pemoh'));
	$('#nik_pemoh').val($('#nama_pemoh option:selected').data('nik_pemoh'));
})
</script>

<script>
$('#nama_atasan_pemohon').change(function(){
	$('#jabatan_atasan_pemohon').val($('#nama_atasan_pemohon option:selected').data('kode_jab_atas_pem'));
	$('#kode_atasan_pemohon').val($('#nama_atasan_pemohon option:selected').data('kode_atasan'));
	$('#nik_atasan').val($('#nama_atasan_pemohon option:selected').data('nik_atasan'));
})
</script>
<script>
$('#nama_penyetuju').change(function(){
	$('#kode_penyetuju').val($('#nama_penyetuju option:selected').data('kode_penyetuju'));
	$('#nik_penyetuju').val($('#nama_penyetuju option:selected').data('nik_penyetuju'));
})
</script>
<script>
$('#nama_penganalisa').change(function(){
	$('#kode_penganalisa').val($('#nama_penganalisa option:selected').data('kode_penganalisa'));
	$('#nik_penganalisa').val($('#nama_penganalisa option:selected').data('nik_penganalisa'));
})
</script>


<script>
	function informasi(that) {
    if (that.value == "Lainnya") {
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
		}
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

@endpush

