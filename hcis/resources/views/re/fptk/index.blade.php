@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <h5>Formulir Permintaan Tenaga Kerja</h5>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('tambah_fptk')}}" class="btn btn-primary btn-sm" style="color: white;"><span class="d-block m-t-5">Tambah</span></a>
                    </div>
                </div>
                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
            </div>
            <form action="{{route('duplicate_fptk')}}" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="table-responsive mb-0">
                        <table id="tbl_data" class="table table-bordered dt-responsive nowrap" style="text-align:center;">
                            <thead style="color:black;">
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Dokumen</th>
                                    <th>Status</th>
                                    <th>Nama Pemohon</th>
                                    <th>Tanggal Dibuat</th>
                                    <th>Jumlah Kebutuhan</th>
                                    <th>Level</th>
                                    <th>Jabatan Yang Dibutuhkan</th>
                                    <th>Lampiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody style="color:black;">
                                @php $b=1; @endphp
                                @foreach($rc_fptk as $data)
                                <tr>
                                    <td>{{$b++;}}</td>
                                    <td style="color:black;">
                                        <input type="hidden" value="{{$data->no_dokumen}}" name="no_dokumen">
                                        {{$data->no_dokumen}}
                                    </td>
                                    <td style="color:black; text-align:center;">
                                        @if(strtolower($data->status) === 'waiting approval')
                                        <a href="{{route('detail_ap', $data->id_fptk)}}">
                                            <input type="hidden" value="{{$data->status}}" name="status">
                                            <p style="color:blue;">{{$data->status}}</p>
                                        </a>
                                        @elseif(strtolower($data->status) === 'rejected')
                                        <a href="{{route('detail_ap',$data->id_fptk)}}">
                                            <input type="hidden" value="{{$data->status}}" name="status">
                                            <p style="color:red;">{{$data->status}}</p>
                                        </a>
                                        @elseif(strtolower($data->status) === 'release')
                                        <a href="{{route('detail_ap',$data->id_fptk)}}">
                                            <input type="hidden" value="{{$data->status}}" name="status">
                                            <p style="color:green;">{{$data->status}}</p>
                                        </a>
                                        @elseif(strtolower($data->status) === 'new')
                                        <a href="{{route('detail_ap',$data->id_fptk)}}">
                                            <input type="hidden" value="{{$data->status}}" name="status">
                                            <p style="color:orange;">{{$data->status}}</p>
                                        </a>
                                        @endif
                                    </td>
                                    <td style="font-size: 12px;color:black;"><input type="hidden" value="{{$data->nama_pemohon}}" name="nama_pemohon"> {{$data->nama_pemohon}}</td>
                                    <td style="font-size: 12px;color:black;"><input type="hidden" value="{{$data->tanggal_upload_dokumen_terakhir}}" name="tanggal_upload_dokumen_terakhir">{{$data->tanggal_upload_dokumen_terakhir}}</td>
                                    <td style="font-size: 12px;color:black;"><input type="hidden" value="{{$data->jumlah_kebutuhan}}" name="jumlah_kebutuhan">{{$data->jumlah_kebutuhan}}</td>
                                    <td style="font-size: 12px;color:black;"><input type="hidden" value="{{$data->tingkat_kepegawaian}}" name="tingkat_kepegawaian">{{$data->tingkat_kepegawaian}}</td>
                                    <td style="font-size: 12px;color:black;"><input type="hidden" value="{{$data->jabatan_kepegawaian}}" name="jabatan_kepegawaian">{{$data->jabatan_kepegawaian}}</td>
                                    <td style="font-size: 12px;color:black;"><a href="{{url('/data_file/'.$data->lampiran_file)}}" download>Lampiran</a></td>
                                    @if(strtolower($data->status) === 'new')
                                    <td>
                                        <a href="{{ route('preview_fptk',$data->id_fptk) }}" style="font-size:9px;" class="btn btn-primary btn-sm">Preview</a>
                                        <a href="{{ route('edit_fptk',$data->id_fptk) }}" style="font-size:9px;" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="javascript:void(0)" onclick="deleteItem('{{ $data->id_fptk }}')" style="font-size:9px;" class="btn btn-danger btn-sm">Hapus</a>
                                        <a style="font-size:9px;" href="{{ route('send_approval',$data->id_fptk) }}" class="btn btn-success btn-sm">Send Approval</a>
                                    </td>
                                    @elseif(strtolower($data->status) === 'rejected')
                                    <td>
                                        <button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Duplikat</button>
                                        <!-- <a type="submit" style="font-size:9px;" class="btn btn-secondary btn-sm"></a> -->
                                    </td>
                                    @elseif(strtolower($data->status) === 'release')
                                    <td>
                                        <a href="{{ route('preview_fptk',$data->id_fptk) }}" style="font-size:9px;" class="btn btn-warning btn-sm">Print</a>
                                        <a href="{{ route('upload_doc_fptk',$data->id_fptk) }}" style="font-size:9px;" class="btn btn-success btn-sm">Upload Dokumen Terakhir</a>
                                    </td>
                                    @elseif(strtolower($data->status) === 'waiting approval')
                                    <td>
                                        <a href="{{route('preview_fptk', $data->id_fptk)}}" style="font-size:9px;" class="btn btn-warning btn-sm">Preview</a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<!-- <script src="{{ URL::asset('assets/js/pages/jquery.responsive-table.init.js') }}"></script> -->
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
    $(document).ready(function() {
        $('#tbl_data').DataTable({
            responsive: true,
        });
    });

    function deleteItem(id) {
        Swal.fire({
            title: '<h4>Anda yakin ingin menghapus?</h4>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: "Batal",
            confirmButtonText: "<i>Ya, hapus!</i>",
            // buttonsStyling: false
        }).then((result) => {
            var actionurl = "{{ URL::to('/list_fptk/hapus/:id')}}"
            actionurl = actionurl.replace(':id', id);
            if (result.isConfirmed) {
                $.ajax({
                    url: actionurl,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(res) {
                        if (res.status) {
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil!",
                                text: "Data berhasil dihapus.",
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        Swal.fire({
                            icon: "error",
                            title: "Gagal!",
                            text: "Terjadi kesalahan saat menghapus data.",
                        });
                    },
                    complete: function(jqXhr, msg) {
                        setTimeout(() => {
                            location.reload();
                        }, 800);
                        // console.log(jqXhr, msg);
                    }
                });
            }
        })
    }
</script>

@endsection