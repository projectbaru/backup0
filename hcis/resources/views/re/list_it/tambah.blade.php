<div class="card">
    <div class="card-header" style="background-color: rgb(47 116 181);">
		<h5 style="color:white;">Tambah Data CV Pelamar</h5>
	</div>
    <div class="card-body">
		<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
			<hr>
			{{ csrf_field() }}
			<div class="form-group" style="width:95%;">
			<div class="">
			<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
			<p>Tambah Data CV Pelamar</p>
			</div> -->
			<b>Melamar Lowongan</b>
			<div class="row">
			<div class="col" style="font-size: 10px;">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
					<!-- <div class="col-sm-9">
					<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					</div> -->
					<div class="col-sm-9">
						<input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
					<div class="col-sm-9">
						<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
						<select name="tingkat_pekerjaan" id="tingkat_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
							<option value="" selected disabled>--Tingkat--</option>
							<option value="staff" >Staff</option>
							<option value="middle" >Middle</option>
							<option value="senior">Senior</option>
						</select>	
					</div>

				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
					<div class="col-sm-9">
						<select name="lokasi_kerja[]"  id="select2-multiple" multiple="multiple" class="select2-multiple form-control form-control-sm" style="font-size:11px; ">
							@foreach ($datalk['looks'] as $np )
							<option value="{{$np->nama_lokasi_kerja}}">{{$np->nama_lokasi_kerja}}</option>
							@endforeach
						</select>	
					
					</div>
				
				</div>
			</div>
			</div>
			
			<hr>
			<b>Personal Information</b>
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
						<div class="col-sm-9">
						<input type="file" id="file_cv" download name="resume_cv" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
						</div>
					</div>
				
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
						<div class="col-sm-9">
						<input type="nama_lengkap" required name="nama_lengkap" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
						<div class="col-sm-9">
						<input type="email" name="email" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
				
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
						<div class="col-sm-9">
						<input type="number" required name="no_hp" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
						<div class="col-sm-9">
							<input type="text" required name="lokasi_pelamar" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
						<div class="col-sm-9">
							<div class="form-check">
								<input class="form-check-input" type="radio" name="pengalaman_kerja" onchange="informasi(this);" value="Berpengalaman" >
								<label class="form-check-label" for="flexRadioDefault1">
									Memiliki Pengalaman
								</label>
							</div>&nbsp;&nbsp;&nbsp;
							<div class="form-check">
								<input class="form-check-input" type="radio" name="pengalaman_kerja" onchange="informasi(this);" value="Lulusan Baru">
								<label class="form-check-label" for="flexRadioDefault1">
									Lulusan Baru
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
						<div class="col-sm-4">
							<input type="text" required id="tahun" name="tahun_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="" >Tahun
						</div>
						<div class="col-sm-5">
							<input type="text" required id="bulan" name="bulan_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="" >Bulan
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
						<div class="col-sm-9">
							<input type="date" required name="tanggal_lahir" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
						
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
						<div class="col-sm-9">
							<input type="date" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
						</div>
					</div>

					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
						<!-- <div class="col-sm-4">
							<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
						</div>
						<div class="col-sm-5">
							<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
						</div> -->
						<div class="col-sm-9">
							<div class="form-check">
								<input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Laki-laki
								</label>
							</div>&nbsp;&nbsp;&nbsp;
							<div class="form-check">
								<input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="flexRadioDefault1">
									Perempuan
								</label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<hr>
			<b>Pendidikan</b>
			<br>

			<div class="row">
			<div class="col" style="font-size: 10px;">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-labelgit -sm" style="font-size:11px;">Pendidikan Tertinggi</label>
					<div class="col-sm-9">
						<select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="git form-control form-control-sm" style="font-size:11px;">
							<!-- <option value="" selected disabled>--Tingkat--</option> -->
							<option value="SMK/Sederajat">SMK/Sederajat</option>
							<option value="D1">D1</option>
							<option value="D2">D2</option>
							<option value="D3">D3</option>
							<option value="S1">S1</option>
							<option value="S2">S2</option>
							<option value="S3">S3</option>

						</select>	
						<!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
					<div class="col-sm-9">
						<input type="text" name="nama_sekolah" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
					<div class="col-sm-9">
						<input type="text" required name="program_studi_jurusan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
					<div class="col-sm-9">
						<input type="date" required name="tanggal_wisuda" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
					<div class="col-sm-4">
						<input type="input" required name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
					</div>Skala*
					<div class="col-sm-4">
					<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
						<input type="text" required name="nilai_skala" style="font-size:11px;" id="nilai_skala" class="form-control form-control-sm" placeholder="">
					</div>
				</div>
			</div>

			</div>
			<hr>
			<b style="font-size:11px;">Informasi Tambahan</b>
			<hr>
			<div class="row">
			<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
			<div class="col-sm-4">
				<select name="informasi_lowongan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
					<option value="Web SRU">Web SRU</option>
					<option value="Job Street" >Job Street</option>
					<option value="Karyawan SRU" >Karyawan SRU</option>
					<option value="Lainnya" >Lainnya</option>
				</select>	
			</div>
			<div class="col-sm-4">
				<input type="text" required name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
			</div>
			</div><br>
			<div class="row">
			<div class="col-sm-12 form-floating">
				<textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px"></textarea>
			</div>
			</div>
			</div>
			</div>
			<hr>
			<div style="width: 100%;">
			<table>
			<tr>
			<!-- <td>
			<input type="text">
			</td> -->
			<td>
			<button type="submit" id="btn_submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
			</td>
			<td>
			<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
			</td>
			</tr>
			</table>
			</div>
			<br>
		</form>
	</div>
</div>