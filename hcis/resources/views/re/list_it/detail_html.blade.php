@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endpush
@section('content')
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Konten modal -->
            <div class="modal-header">
                <h5 class="modal-title">Lamaran Kerja</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card">
    
                    <div class="card-body">
                        <form action="{{route('simpan_it')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @foreach($job as $job)
                            <div class="form-group" style="width:95%;">
                            <div class="">
                            <!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                            <p>Tambah Data CV Pelamar</p>
                            </div> -->
                            
                            <b>Personal Information</b>
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
                                        <div class="col-sm-9">
                                        <input type="file" id="file_cv" download name="resume_cv" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
                                        </div>
                                    </div>
                                
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
                                        <div class="col-sm-9">
                                        <input type="nama_lengkap" required name="nama_lengkap" style="font-size:11px;" value="{{$job->jabatan_lowongan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                                        <div class="col-sm-9">
                                        <input type="email" name="email" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
                                        <div class="col-sm-9">
                                        <input type="number" required name="no_hp" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
                                        <div class="col-sm-9">
                                            <input type="text" required name="lokasi_pelamar" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
                                        <div class="col-sm-9">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="pengalaman_kerja" onchange="informasi(this);" value="Berpengalaman" >
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Memiliki Pengalaman
                                                </label>
                                            </div>&nbsp;&nbsp;&nbsp;
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="pengalaman_kerja" onchange="informasi(this);" value="Lulusan Baru">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Lulusan Baru
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
                                        <div class="col-sm-4">
                                            <input type="text" required id="tahun" name="tahun_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="" >Tahun
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" required id="bulan" name="bulan_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="" >Bulan
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input type="date" required name="tanggal_lahir" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                        
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <input type="date" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
                                        <!-- <div class="col-sm-4">
                                            <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
                                        </div> -->
                                        <div class="col-sm-9">
                                            <div class="form-check">
                                                <input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Laki-laki
                                                </label>
                                            </div>&nbsp;&nbsp;&nbsp;
                                            <div class="form-check">
                                                <input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Perempuan
                                                </label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <b>Pendidikan</b>
                            <br>

                            <div class="row">
                            <div class="col" style="font-size: 10px;">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-labelgit -sm" style="font-size:11px;">Pendidikan Tertinggi</label>
                                    <div class="col-sm-9">
                                        <select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="git form-control form-control-sm" style="font-size:11px;">
                                            <!-- <option value="" selected disabled>--Tingkat--</option> -->
                                            <option value="SMK/Sederajat">SMK/Sederajat</option>
                                            <option value="D1">D1</option>
                                            <option value="D2">D2</option>
                                            <option value="D3">D3</option>
                                            <option value="S1">S1</option>
                                            <option value="S2">S2</option>
                                            <option value="S3">S3</option>

                                        </select>	
                                        <!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="nama_sekolah" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
                                    <div class="col-sm-9">
                                        <input type="text" required name="program_studi_jurusan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
                                    <div class="col-sm-9">
                                        <input type="date" required name="tanggal_wisuda" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
                                    <div class="col-sm-4">
                                        <input type="input" required name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                    </div>Skala*
                                    <div class="col-sm-4">
                                    <!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
                                        <input type="text" required name="nilai_skala" style="font-size:11px;" id="nilai_skala" class="form-control form-control-sm" placeholder="">
                                    </div>
                                </div>
                            </div>

                            </div>
                            <hr>
                            <b style="font-size:11px;">Informasi Tambahan</b>
                            <hr>
                            <div class="row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
                            <div class="col-sm-4">
                                <select name="informasi_lowongan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
                                    <option value="Web SRU">Web SRU</option>
                                    <option value="Job Street" >Job Street</option>
                                    <option value="Karyawan SRU" >Karyawan SRU</option>
                                    <option value="Lainnya" >Lainnya</option>
                                </select>	
                            </div>
                            <div class="col-sm-4">
                                <input type="text" required name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
                            </div>
                            </div><br>
                            <div class="row">
                            <div class="col-sm-12 form-floating">
                                <textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px"></textarea>
                            </div>
                            </div>
                            </div>
                            </div>
                            <hr>
                            <div style="width: 100%;">
                            <table>
                            <tr>
                            <!-- <td>
                            <input type="text">
                            </td> -->
                            <td>
                            <button type="submit" id="btn_submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                            </td>
                            <td>
                            <a href="{{ route('list_it') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                            </td>
                            </tr>
                            </table>
                            </div>
                            <br>
                        @endforeach
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('add-scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

<script>
$(document).ready(function() {
	// Select2 Multiple
  $('.select2-multiple').select2({
	  placeholder: "Lokasi Kerja",
		allowClear: true
	});

}); 
</script>


<script>
	function informasi(that) {
		var tahun_h = document.getElementById("tahun_hid").value;
		var bulan_h = document.getElementById("bulan_hid").value;
		if (that.value == "Berpengalaman") {
				document.getElementById("tahun").value =tahun_h;
				document.getElementById("bulan").value =bulan_h;
				document.getElementById("tahun").readOnly = false;
				document.getElementById("bulan").readOnly = false;
			} else {
				document.getElementById("tahun").value ='';
				document.getElementById("bulan").value ='';
				document.getElementById("tahun").readOnly = true;
				document.getElementById("bulan").readOnly = true;
			}
		}
</script>




<script>
	function informasi(that) {
    if ((that.value == "Lainnya") || (that.value == "Karyawan SRU")){
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
	}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection