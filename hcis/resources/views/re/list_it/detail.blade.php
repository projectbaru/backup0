@extends('layouts.master-without_nav')

@section('content')
<style>
    .list-group-item.selected {
        background-color: black;
        color: white;
    }
</style>

<body class="" >
    <div class="container-fluid" style="width:90%;">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
            <a class="navbar-brand" href="#" style="color:white;">PT SRU</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav" style="color:white;">
                <li class="nav-item" style="color:white;">
                <a class="nav-link active" aria-current="page" href="#" style="color:white;">Home</a>
                </li>
                <li class="nav-item" style="color:white;">
                <a class="nav-link" href="#" style="color:white;">Product</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#" style="color:white;">Service</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#" style="color:white;">Corporate</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Career</a>
                </li>
                
            </ul>
            </div>
        </div>
        </nav>
        <div class="">
            <div class="">
                <div class="banner">
                    <img style="height: 400px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_188f1c8b5d3%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_188f1c8b5d3%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.91796875%22%20y%3D%22218.45%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="img-fluid w-100" alt="Banner">
                </div>
                </br>
@foreach($job as $data)
<div class="">
    <div style="border-radius:2px;">
        <div class="row">
		<div class="row ">
            <div class="row ">
                <div class="col-4">
                    <div class="">
					<div class="card-body">
                            <h2 class="card-text">{{$data->jabatan_lowongan}}</h2>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Lokasi</h5>
                            <p class="card-text">{{$data->lokasi_kerja}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Kategori Pekerjaan</h5>
                            <p class="card-text">{{$data->kategori_pekerjaan}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Minimal Pendidikan</h5>
                            <p class="card-text">{{$data->minimal_pendidikan}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Pengalaman Kerja</h5>
                            <p class="card-text">{{$data->pengalaman_kerja}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Tipe Kepegawaian</h5>
                            <p class="card-text">{{$data->status_kepegawaian}}</p>
                        </div>
                    </div><br>
                    <div class="">
                        <div class="card-body">
                            <h5 class="card-title">Tingkat</h5>
                            <p class="card-text">{{$data->tingkat_pekerjaan}}</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-8">
				<div class="row m-5">
                    <div>
                        <h3>Tentang Peran Pekerjaan</h3>
                    </div>
                    <div>
                    {!!($data->deskripsi_pekerjaan) !!}
                    </div>
                    <div style="background-color:#e7e7e7;">
                        <div class="m-3"><br>
                            <div>
                                <h6>Deskripsi Pekerjaan</h6>
                               
                                {!!($data->syarat_pengalaman)!!}
                                </p>
                            </div>
                            
                            <div>
                                <h6>Pengalaman</h6>
                                <p>
                                {!!($data->syarat_pengalaman)!!}
                                </p>
                            </div>
                            <div>
                                <h6>Kemampuan</h6>
                                <p>
                                {!!($data->syarat_kemampuan)!!}
                                </p>
                            </div>
                            <div>
                                <h6>Lainnya</h6>
                                <p>
                                {!!($data->syarat_lainnya)!!}
                                </p>
                            </div>
                        
                            <br>
                            <br>
							
                        </div>
                   
                    </div>
					
					<div class="row m-3">
					<div class="col-3">
                    <button class="btn btn-primary" onclick="getCreateData('{{ $data->id_lowongan_kerja }}')">
                        Lamar Sekarang
                    </button>
					</div>
					</div>
                </div>
				</div>
            </div>
        </div>
    </div>
</div>
 @endforeach
 </div>
        </div>
    
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
            &nbsp;&nbsp;&nbsp;<a class="navbar-brand" href="#" style="color:white;">PT SRU</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            
            </div>
        </div>
        </nav>
            
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
   
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav" style="color:white;">
                    <li class="nav-item" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">Home</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item" style="color:white;">
                    <a class="nav-link" href="#" style="color:white;">Product</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Service</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Corporate</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color:white;">Career</a>
                    </li>
                    
                </ul>
            </div>
        </div>
        </nav>
        
    </div>    

 <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Konten modal -->
                <div class="modal-header">
                    <h5 class="modal-title">Lamaran Kerja</h5>
                    <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="card">
    
    		<div class="card-body" id="formCreate">
		
				</div>
</div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('add-scripts')
<script>
	function informasi_pengalaman(that) {
    if (that.value == "Berpengalaman")  {
				document.getElementById("tahun").readOnly = false;;
				document.getElementById("bulan").readOnly = false;;
			} else {
				document.getElementById("tahun").readOnly = true;;
				document.getElementById("bulan").readOnly = true;;
			}
		}
</script>
<script>
function getCreateData(id) {
  let url = "{{ URL::to('/lamar_pekerjaan/create_html/:id') }}";
  url = url.replace(":id", id);
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function(data){
        console.log(data);
        $('#formCreate').html(data.data);
        $('#myModal').modal('show');
    }
 });
}
</script>
@endsection