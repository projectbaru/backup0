@extends('layouts.master-without_nav')

@section('content')
<style>
    .list-group-item.selected {
        background-color: black;
        color: white;
    }
</style>
<body class="" >
    <div class="container-fluid" style="width:90%;">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
            <a class="navbar-brand" href="#" style="color:white;">PT SRU</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav" style="color:white;">
                <li class="nav-item" style="color:white;">
                <a class="nav-link active" aria-current="page" href="#" style="color:white;">Home</a>
                </li>
                <li class="nav-item" style="color:white;">
                <a class="nav-link" href="#" style="color:white;">Product</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#" style="color:white;">Service</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="#" style="color:white;">Corporate</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Career</a>
                </li>
                
            </ul>
            </div>
        </div>
        </nav>
        <div class="">
            <div class="">
                <div class="banner">
                    <img style="height: 400px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_188f1c8b5d3%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_188f1c8b5d3%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.91796875%22%20y%3D%22218.45%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="img-fluid w-100" alt="Banner">
                </div>
                </br>
                <form action="{{ route('list_it') }}" method="GET" class="d-flex">
                    <input type="text" name="keyword" class="form-control" placeholder="Masukkan kata kunci">
                    <button type="submit" class="btn btn-primary ml-2">Cari</button>
                </form>
                </br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="sidebar">
                            <h4>Kategori Pekerjaan</h4>
                            <ul class="list-group">
                                @foreach($cat as $c)
                                    <a href="{{ route('list_it', ['kategori' => $c, 'keyword' => $request->key]) }}"><li class="list-group-item {{ $request->kategori == $c ? 'selected' : '' }}">{{ $c }}</li></a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            @foreach($job as $j)
                            <div class="card-body">
                                <a href="{{ URL::to('/lamar_pekerjaan/'.$j->id_lowongan_kerja) }}"><h2 class="text-primary">{{$j->jabatan_lowongan}}</h2></a>
                                <p>{{$j->lokasi_kerja}} | {{$j->kategori_pekerjaan}} | {{$j->pengalaman_kerja}}</p>
                                <p>{{ $j->deskripsi_pekerjaan }}</p>
                            </div>
                            <hr>
                            @endforeach
                        </div>
                        <br>

                        <nav aria-label="Page navigation">
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><a class="page-link" href="{{ $job->previousPageUrl() }}">Previous</a></li>
                                
                                @foreach($job->links()->elements as $element)
                                    @if (is_array($element))
                                        @foreach ($element as $page => $url)
                                            <li class="page-item{{ $job->currentPage() == $page ? ' active' : '' }}">
                                                <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="page-item disabled">
                                            <span class="page-link">{{ $element }}</span>
                                        </li>
                                    @endif
                                @endforeach

                                <li class="page-item"><a class="page-link" href="{{ $job->nextPageUrl() }}">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
            &nbsp;&nbsp;&nbsp;<a class="navbar-brand" href="#" style="color:white;">PT SRU</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            
            </div>
        </div>
        </nav>
            
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: rgb(4 132 249);color:white;">
        <div class="container-fluid" style="color:white;">
   
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav" style="color:white;">
                    <li class="nav-item" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">Home</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item" style="color:white;">
                    <a class="nav-link" href="#" style="color:white;">Product</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Service</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#" style="color:white;">Corporate</a>
                    </li>
                    <li class="" style="color:white;">
                    <a class="nav-link active" aria-current="page" href="#" style="color:white;">|</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color:white;">Career</a>
                    </li>
                    
                </ul>
            </div>
        </div>
        </nav>
        
    </div>    
</body>


@endsection