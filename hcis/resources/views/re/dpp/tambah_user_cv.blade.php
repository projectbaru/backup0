@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Data Personal Pelamar @endslot
@slot('title') Recruitment @endslot
@endcomponent
	<form action="{{route('simpan_dpp')}}" method="post" enctype="multipart/form-data">
		<hr>
		{{ csrf_field() }}
			<input type="hidden" name="temp_id" id="temp_id" />

			<div class="form-group" style="width:95%;">
				<div class="tab">
					<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
						<p>Tambah Data CV Pelamar</p>
					</div> -->
					<b>Formulir Data Pelamar</b><br><br>
					<p>Data Pribadi</p>
					<hr>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Pelamar</label>
								<div class="col-sm-9">
									<select name="kode_cv_pelamar" id="kode_cv_pelamar" class="form-control form-control-sm">
										<option value="" selected disabled>--- Pilih ---</option>
										@foreach($dcp as $data){{$data->kode_cv_pelamar}}		
										<option value="{{$data->kode_cv_pelamar}}">{{$data->kode_cv_pelamar}}</option>
										@endforeach
									</select>	
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Posisi yang dilamar</label>
								<div class="col-sm-9">
									<input type="text" required name="posisi_lamaran" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama lengkap (KTP)</label>
								<div class="col-sm-9">
									<input type="text" required name="nama_lengkap" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama panggilan</label>
								<div class="col-sm-9">
									<input type="text" required name="nama_panggilan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat & tgl lahir</label>
								<div class="col-sm-6">
									<input type="text" required name="tempat_lahir" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
								<div class="col-sm-3">
									<input type="date" required name="tanggal_lahir" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Agama</label>
								<div class="col-sm-9">
									<select name="agama" style="font-size:10px;" id="agama" class="form-control form-control-sm">
										<option value="islam" data-kode ="" >Islam</option>
										<option value="kristen" data-kode ="" >Kristen</option>
										<option value="budha" data-kode ="" >Budha</option>
										<option value="hindu" data-kode ="" >Hindu</option>								
									</select>	
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kebangsaan</label>
								<div class="col-sm-9">
									<input type="text" required name="kebangsaan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis kelamin</label>
								<div class="col-sm-9">
									<!-- <div class="form-check">
										<input class="form-check-input" type="radio" name="jenis_kelamin"  value="laki-laki">
											Laki-laki&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan">
											Perempuan
									</div> -->
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="laki-laki">
										<label class="form-check-label" for="inlineRadio1">Laki-laki</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="perempuan">
										<label class="form-check-label" for="inlineRadio2">Perempuan</label>
									</div>
									<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">BB & TB</label>
								<div class="col-sm-9">
										<div class="row">
											<div class="col-sm-4">
												<input type="number"  required name="berat_badan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >Kg
											</div>
											<div class="col-sm-5">
												<input type="number"  required name="tinggi_badan" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >Cm
											</div>
										</div>
									<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kendaraan</label>
								<div class="col-sm-9">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="status_kendaraan"  value="milik sendiri">
										Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" name="status_kendaraan" value="kantor" >
										Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" name="status_kendaraan" value="orangtua" >
										Orangtua&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" name="status_kendaraan" value="lainnya" >
										Lainnya
									</div>
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis kendaraan/merk</label>
								<div class="col-sm-9">
									<input type="text" name="jenis_kendaraan" class="form-control form-control-sm">
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat didalam kota(Lengkap)</label>
								<div class="col-sm-9">
									<textarea type="text" name="alamat_didalam_kota" class="form-control form-control-sm" rows="4" cols="50"></textarea>
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
								<div class="col-sm-9">
									<input type="number" name="kode_pos_1" class="form-control form-control-sm">
								</div>
							</div>
						</div>
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. KTP</label>
								<div class="col-sm-9">
									<input type="number" name="no_ktp" class="form-control form-control-sm">
								</div>
							</div><br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email Pribadi</label>
								<div class="col-sm-9">
									<input type="email" name="email_pribadi" class="form-control form-control-sm">
								</div>
							</div><br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat sosmed pribadi</label>
								<div class="col-sm-9">
									<input type="text" name="alamat_sosmed" class="form-control form-control-sm">
								</div>
							</div><br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">SIM yang dimiliki</label>
								<div class="col-sm-9">
									<div class="row">
										<div class="col-sm-4">
											<select name="kategori_sim" id="kategori_sim" class="form-control form-control-sm">
												<option value="" selected disabled>--- Pilih ---</option>
												<option value="" data-kode ="" >A</option>
												<option value="" data-kode ="" >B</option>
												<option value="" data-kode ="" >B1</option>
												<option value="" data-kode ="" >B2</option>
												<option value="" data-kode ="" >C</option>
												<option value="" data-kode ="" >D</option>
											</select>		
										</div>
										<!-- <div class="col-sm-4">
											<input type="number"  required name="no_sim" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >No
										</div> -->
									</div>
									<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. HP</label>
								<div class="col-sm-9">
									<input type="number" name="no_hp" class="form-control form-control-sm">
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. Telp</label>
								<div class="col-sm-9">
									<input type="number" name="no_telepon" class="form-control form-control-sm">
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat Tinggal</label>
								<div class="col-sm-9">
									<!-- <div class="form-check">
										<input class="form-check-input"  type="radio" name="tempat_tinggal"  value="milik sendiri" >Milik Sendiri
										<input class="form-check-input"  type="radio" name="tempat_tinggal" value="ikut orangtua" >Kost/Kontrak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input"  type="radio" name="tempat_tinggal" value="kost/kontrak" >Ikut Orangtua
									</div> -->
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="tempat_tinggal" id="inlineRadio1" value="Milik Sendiri">
										<label class="form-check-label" for="inlineRadio1">Milik Sendiri</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="tempat_tinggal" id="inlineRadio2" value="Kost/Kontrak">
										<label class="form-check-label" for="inlineRadio2">Kost/Kontrak</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="tempat_tinggal" id="inlineRadio3" value="Ikut Orang Tua">
										<label class="form-check-label" for="inlineRadio3">Ikut Orang Tua</label>
									</div>
								</div>
							</div>
							<br>	
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status</label>
								<div class="col-sm-9">
									<!-- <div class="form-check">
										<input class="form-check-input"  type="radio" name="tempat_tinggal"  value="milik sendiri" >Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input"  type="radio" name="tempat_tinggal" value="ikut orangtua" >Janda/Duda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input"  type="radio" name="tempat_tinggal" value="kost/kontrak" >Tidak Menikah
									</div> -->
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="status_perkawinan" id="inlineRadio1" value="Menikah">
										<label class="form-check-label" for="inlineRadio1">Menikah</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="status_perkawinan" id="inlineRadio2" value="Janda/Duda">
										<label class="form-check-label" for="inlineRadio2">Janda/Duda</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="status_perkawinan" id="inlineRadio3" value="Tidak Menikah">
										<label class="form-check-label" for="inlineRadio3">Tidak Menikah</label>
									</div>
								</div>
							</div>	
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat diluar kota(Lengkap)</label>
								<div class="col-sm-9">
									<textarea class="form-control form-control-sm"  type="text" name="alamat_diluar_kota" rows="4" cols="50"  value=""></textarea>
								</div>
							</div>	
							<br>		
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
								<div class="col-sm-9">
									<input class="form-control form-control-sm"  type="number" name="kode_pos_2" value="">
								</div>
							</div>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Photo</label>
								<div class="col-sm-9">
									<input class="form-control form-control-sm"  type="file" name="pas_foto" value="">
								</div>
							</div>	
							<br>
						</div>
					</div>
					<br>
					<div class="form-group row">
						<b>Susunan Keluarga (termasuk pelamar)</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 8px;">
								<table class="table" style="font-size: 8px;">
									<thead>
										<tr style="border:1px solid #dee2e6;">
											<th scope="col" ></th>
											<th scope="col"></th>
											<th scope="col"></th>
											<th scope="col"></th>
											<th scope="col"></th>
											<th scope="col" colspan="2" style="border:1px solid #dee2e6;text-align:center;">Pekerjaan Terakhir</th>
											<th scope="col"></th>
										</tr>
										<tr style="">
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Hubungan</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Nama</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Laki-laki/Perempuan</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Usia(Tahun Lahir)</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Pendidikan Terakhir</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Jabatan</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Perusahaan</th>
											<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Keterangan</th>
										</tr>
									</thead>
									<tbody class="table" style="">
										<tr>
											<td><input  type="text" name="hubungan_keluarga" value="Ayah" class="form-control form-control-sm"></td>
											<td><input type="text" required name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td><input type="text" readonly name="jenis_kelamin" value="L" class="form-control form-control-xl"></td>
											<td><input type="number" required value="usia_tahun_lahir" name="usia_tahun_lahir" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1">D1</option>
														<option value="D2">D2</option>
														<option value="D3">D3</option>
														<option value="S1">S1</option>
														<option value="S2">S2</option>
														<option value="S3">S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga_1" value="Ibu" class="form-control form-control-sm"></td>
											<td><input type="text" required name="nama_keluarga_1" value="" class="form-control form-control-sm"></td>
											<td><input type="text" readonly name="jenis_kelamin_1" value="P" class="form-control form-control-sm"></td>
											<td><input type="text" required name="usia_tahun_lahir_1" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir_1" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan_1" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan_1" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga_1" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" required name="hubungan_keluarga" value="Saudara 1" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s1" id="" value="L">
														<label class="form-check-label" for="">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s1" id="" value="P">
														<label class="form-check-label" for="">P</label>
													</div>
												
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" required value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>

										</tr>
										<tr>
											<td><input type="text" required name="hubungan_keluarga" value="Saudara 2" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s2" id="" value="L">
														<label class="form-check-label" for="">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s2" id="" value="P">
														<label class="form-check-label" for="">P</label>
													</div>
												
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" required value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 3" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s3" id="inlineRadio3" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s3" id="inlineRadio3" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
													
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 4" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<fieldset id="group2">
														<div class="form-check form-check-inline">
															<input class="form-check-input" type="radio" name="jenis_kelamin_s4" id="inlineRadio1" value="option1">
															<label class="form-check-label" for="inlineRadio1">L</label>
														</div>
														<div class="form-check form-check-inline">
															<input class="form-check-input" type="radio" name="jenis_kelamin_s4" id="inlineRadio2" value="option2">
															<label class="form-check-label" for="inlineRadio2">P</label>
														</div>
													</fieldset>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 5" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s5" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s5" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 6" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s6" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s6" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
										<td><input type="text" name="hubungan_keluarga" value="Saudara 7" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s7" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s7" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 8" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s8" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s8" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Saudara 9" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s9" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_s9" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr><br></tr>
										<tr><br></tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Isteri/Suami*" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_is" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_is" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Anak 1" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a1" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a1" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Anak 2" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a2" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a2" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Anak 3" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a3" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a3" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Anak 4" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a4" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a4" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										<tr>
											<td><input type="text" name="hubungan_keluarga" value="Anak 5" class="form-control form-control-sm"></td>
											<td><input type="text" name="nama_keluarga" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="form-group container">
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a5" id="inlineRadio1" value="option1">
														<label class="form-check-label" for="inlineRadio1">L</label>
													</div>
													<div class="form-check form-check-inline">
														<input class="form-check-input" type="radio" name="jenis_kelamin_a5" id="inlineRadio2" value="option2">
														<label class="form-check-label" for="inlineRadio2">P</label>
													</div>
												</div>
											</td>
											<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
											<td>
												<div class="col-sm-10">
													<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
														<option value="SD" >SD</option>
														<option value="SMP/Sederajat" >SMP/Sederajat</option>
														<option value="SMK/Sederajat" >SMK/Sederajat</option>
														<option value="D1" >D1</option>
														<option value="D2" >D2</option>
														<option value="D3" >D3</option>
														<option value="S1" >S1</option>
														<option value="S2" >S2</option>
														<option value="S3" >S3</option>
													</select>	
												</div>
											</td>
											<td><input type="text" name="jabatan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="perusahaan" value="" class="form-control form-control-sm"></td>
											<td><input type="text" name="keterangan_susunan_keluarga" value="" class="form-control form-control-sm"></td>
										</tr>
										
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<b>Latar Belakang Pendidikan Formal / Informal</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
							<div class="form-group row" id="qualificationInputContainer">
								<table class="table table-bordered">
									<thead>
										<tr style="border:1px solid #dee2e6;">
											<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tingkat</th>
											<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Nama Sekolah</th>
											<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tempat/Kota</th>
											<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jurusan</th>
											<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
											<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Lulus/Tidak</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<!-- <input type="text" class="form-control form-control-sm" readonly value="SD"> -->
												<select name="tingkat_pendidikan[]" required id="tingkat_pendidikan" class="form-control form-control-sm" style="font-size:11px;">
													<option value="SD">SD</option>
													<option value="SMP" >SMP</option>
													<option value="D1" >D1</option>
													<option value="D2" >D2</option>
													<option value="D3" >D3</option>
													<option value="S1" >S1</option>
													<option value="S2" >S2</option>
													<option value="S3" >S3</option>
												</select>	
											</td>
											<td><input type="text" required class="form-control form-control-sm" name="nama_sekolah[]"></td>
											<td><input type="text" required class="form-control form-control-sm" name="jurusan[]"></td>
											<td><input type="number" required class="form-control form-control-sm" name="tahun_masuk[]"></td>
											<td><input type="number" required class="form-control form-control-sm" name="tahun_keluar[]"></td>
											<td>
												<input type="text" required class="form-control form-control-sm" name="status_kelulusan[]">
											</td>
											<td>
												<div class="col-sm-1">
													<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
														<i class="fa-solid fa-plus"></i>
													</button>
												</div>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>
						<br>
						<b>Kursus/Pelatihan yang pernah diikuti</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row" id="kursus">
									<table class="table table-bordered">
										<thead>
											<tr style="border:1px solid #dee2e6;">
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bidang/Jenis</th>
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Penyelenggara</th>
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tempat/Kota</th>
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Lama Kursus</th>
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tahun</th>
												<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Dibiayai Oleh</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th><input type="text" required class="form-control form-control-sm" name="bidang_pelatihan[]"></th>
												<td><input type="text" required class="form-control form-control-sm" name="penyelenggara[]"></td>
												<td><input type="text" required class="form-control form-control-sm" name="tempat_kota[]"></td>
												<td><input type="number" required class="form-control form-control-sm" name="lama_pelatihan[]"></td>
												<td><input type="number" class="form-control form-control-sm" required name="tahun_pelatihan[]"></td>
												<td><input type="text" class="form-control form-control-sm" required name="dibiayai_oleh[]"></td>
												<td>
													<div class="col-sm-1">
														<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addKursusInput">
															<i class="fa-solid fa-plus"></i>
														</button>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<br>
						<b>Pengetahuan Bahasa Asing</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
							<div class="form-group row" id="bahasa">
							<table class="table table-bordered">
								<thead>
									<tr style="border:1px solid #dee2e6;">
										<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bahasa</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Mendengar</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Membaca</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Berbicara</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Menulis</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">
											<input type="text" required class="form-control form-control-sm" name="bahasa[]">
										</td>
										<td>
											<div class="col-sm-10">
												<select name="mendengar[]" required id="mendengar" class="form-control form-control-sm">
													<option value="kurang"  >Kurang</option>
													<option value="baik" data-kode ="" >Baik</option>
													<option value="sangat baik" data-kode ="" >Sangat Baik</option>
												</select>	
											</div>
										</td>
										<td>
											<div class="col-sm-10">
												<select name="membaca[]" required id="membaca" class="form-control form-control-sm">
													<option value="kurang">Kurang</option>
													<option value="baik">Baik</option>
													<option value="sangat baik">Sangat Baik</option>
												</select>	
											</div>
										</td>
										<td>
											<div class="col-sm-10">
												<select name="berbicara[]" required id="berbicara" class="form-control form-control-sm">
													<option value="kurang" >Kurang</option>
													<option value="baik" >Baik</option>
													<option value="sangat baik" >Sangat Baik</option>
												</select>	
											</div>
										</td>
										<td>
											<div class="col-sm-10">
												<select name="menulis[]" required id="menulis" class="form-control form-control-sm">
													<option value="kurang" >Kurang</option>
													<option value="baik" >Baik</option>
													<option value="sangat baik">Sangat Baik</option>
												</select>	
											</div>
										</td>
										<td>
											<div class="col-sm-1">
												<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addBahasaInput">
													<i class="fa-solid fa-plus"></i>
												</button>
											</div>
										</td>
									</tr>														
								</tbody>
							</table>
							</div>

							</div>
						</div>
						<br>
						<b>Kegiatan Sosial</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
							<div class="form-group row" id="Sosial">
							<table class="table table-bordered">
								<thead>
									<tr style="border:1px solid #dee2e6;">
										<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Organisasi</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jenis Kegiatan</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jabatan</th>
										<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th><input type="text" class="form-control form-control-sm" id="nama_organisasi" name="nama_organisasi[]"></th>
										<td><input type="text" class="form-control form-control-sm" id="jenis_kegiatan" name="jenis_kegiatan[]"></td>
										<td><input type="text" class="form-control form-control-sm" id="jabatan" name="jabatan[]"></td>
										<td><input type="text" class="form-control form-control-sm" id="tahun" name="tahun[]"></td>
										<td>
											<div class="col-sm-1">
												<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addSosialInput">
													<i class="fa-solid fa-plus"></i>
												</button>
											</div>
										</td>
									</tr>
									
								</tbody>
							</table>
							</div>

							</div>
						</div>
						<br>
						<b>Hobby dan Kegiatan Waktu Luang</b>
						<hr>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<textarea name="hoby_dan_kegiatan" required class="form-control" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Hoby Saya ..."></textarea>
								</div>
							</div>
						</div>
						<br>
						<b>Kegiatan Membaca</b>
						<hr>
						<div class="row">
							<div class="col">
								<p>Dibanding seluruh aktifitas waktu yang anda luangkan</p>
								<select name="kegiatan_membaca[]" required id="kegiatan_membaca" class="form-control form-control-sm">
									<option value="sedikit">Sedikit</option>
									<option value="sedang">Sedang</option>
									<option value="banyak">Banyak</option>
								</select>	
								<br>
								<b>Media yang dibaca</b>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Surat Kabar</label>
									<div class="col-sm-9">
										<input type="text" required name="surat_kabar[]" class="form-control form-control-sm">
									</div>
								</div>
								<br>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Majalah</label>
									<div class="col-sm-9">
										<input type="text" required name="majalah[]" class="form-control form-control-sm">
									</div>
								</div>
							</div>
							<div class="col">
								<p>Pokok-pokok yang dibaca</p>
								<textarea name="pokok_yang_dibaca[]" required class="form-control" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Pokok-pokok yang dibaca ..."></textarea>
							</div>
						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<div class="tab">
							<br>
							<div class="form-group row" id="riwayatPekerjaan">
								<b>Riwayat Pekerjaan (mulai dari pekerjaan sekarang s/d terakhir)</b>
								<hr>
								<p>*isi tanda "-" jika belum memiliki pengalaman kerja atau magang</p>
								<div class="row">
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
											<div class="col-sm-9">
												<input type="text" name="nama_perusahaan[]" required class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat</label>
											<div class="col-sm-9">
												<input type="text" name="alamat[]" required class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama bekerja</label>
											<div class="col-sm-9">
												<input type="text" required name="lama_bekerja[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama atasan langsung</label>
											<div class="col-sm-9">
												<input type="text" required name="nama_atasan[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Gaji terakhir</label>
											<div class="col-sm-9">
												<input type="text" required name="gaji_terakhir[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis usaha</label>
											<div class="col-sm-9">
												<input type="text" required name="jenis_usaha[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alasan berhenti</label>
											<div class="col-sm-9">
												<input type="text" required name="alasan_berhenti[]" class="form-control form-control-sm">
											</div>
										</div>
										
									</div>
									<div class="col" style="font-size: 10px;">
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan awal</label>
											<div class="col-sm-9">
												<input type="text" required name="jabatan_awal[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan akhir</label>
											<div class="col-sm-9">
												<input type="text" required name="jabatan_akhir[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Telp perusahaan</label>
											<div class="col-sm-9">
												<input type="text" required name="telp_perusahaan[]" class="form-control form-control-sm">
											</div>
										</div><br>
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Direktur</label>
											<div class="col-sm-9">
												<input type="text" required name="nama_direktur[]" class="form-control form-control-sm">
											</div>
										</div>
									</div>
								</div>
								<br>
								<div>
									<div class="col-sm-1">
										<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addRiwayat">
											<i class="fa-solid fa-plus"></i>
										</button>
									</div>
								</div>
							</div>

							
							<b>Referensi (Kepada siapa kami dapat menanyakan diri anda lebih lengkap)</b>
							<hr>
							<div class="form-group row" id="referensi">
								<div class="col-md-12" style="font-size: 10px;">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>NAMA</th>
												<th>ALAMAT/TELEPON</th>
												<th>PEKERJAAN</th>
												<th>HUBUNGAN</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="text" required class="form-control form-control-sm" name="nama_lengkap"></td>
												<td><input type="text" required class="form-control form-control-sm" name="alamat_telepon"></td>
												<td><input type="text" required class="form-control form-control-sm" name="pekerjaan"></td>
												<td><input type="text" required class="form-control form-control-sm" name="hubungan"></td>
											</tr>
										</tbody>
										
									</table>
								</div>
								<div class="">
									<div class="col-sm-1">
										<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addReferensi">
											<i class="fa-solid fa-plus"></i>
										</button>
									</div>
								</div>
							</div>
							<br>
							<b>Orang yang dapat dihubungi dalam keadaan mendesak</b>
							<hr>
							<div class="row form-group" id="hubungi">
								<div class="col" style="font-size: 10px;">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>NAMA</th>
												<th>ALAMAT/TELEPON</th>
												<th>PEKERJAAN</th>
												<th>HUBUNGAN</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="text" required class="form-control form-control-sm" name="nama_lengkap"></td>
												<td><input type="text" required class="form-control form-control-sm" name="alamat_telpon"></td>
												<td><input type="text" required class="form-control form-control-sm" name="pekerjaan"></td>
												<td><input type="text" required class="form-control form-control-sm" name="hubungan"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="">
									<div class="col-sm-1">
										<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addhubungi">
											<i class="fa-solid fa-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="tab">
							<br>
							<b>Uraikan tugas & tanggung jawab Anda pada jabatan yang terakhir</b>
							<hr>
							<div class="row">
								<div class="col" style="font-size: 10px;">
									<textarea name="tugas_jabatan_terakhir" class="form-control" required id="" cols="30" rows="7" placeholder="Tugas saya adalah.........."></textarea>
									<br>*isi Fresh graduate jika belum miliki pengalaman
								</div>
							</div>
							<br>
							<b>Uraikan hal-hal positif yang menjadi kekuatan anda & hal negatif dalam diri anda yang ingin anda kembangkan</b>
							<hr>
							<div class="row">
								<div class="col" style="font-size: 10px;">
									<table class="" style="border:none;">
										<thead style="border:1px solid black;">
											<tr>
												<th></th>
												<th>Hal-hal Positif</th>
												<th></th>
												<th></th>
												<th>Hal-hal Negatif</th>
											</tr>
										</thead>
										<tbody style="border:1px solid black;">
											<tr>
												<td>1</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
												<td></td>
												<td>1</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
											</tr>
											<tr>
												<td>2</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
												<td></td>
												<td>2</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
											</tr>
											<tr>
												<td>3</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
												<td></td>
												<td>3</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
											</tr>
											<tr>
												<td>4</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
												<td></td>
												<td>4</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
											</tr>
											<tr>
												<td>5</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
												<td></td>
												<td>5</td>
												<td><input type="text" required class="form-control form-control-sm"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<b>Gambarkan secara jelas denah tempat tinggal anda dengan patokan jalan utama</b><br>(digambar pada saat anda diundang interview)
							<hr>
							<i style="font-size:10px;">Masukkan berupa gambar</i><br>
							<input type="file" href=""  class="btn btn-primary btn-sm" value="Gambar Denah Tempat Tinggal"></input>
							<br><br>
							<div style="width: 100%;text-align:center;">
								<table>
								<tr>
									<td>
										<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal Simpan Data</a>
									</td>
								</tr>
								</table>
							</div>
						</div>
						<div class="tab">
							<div class="row">
								<div class="col" style="font-size: 14px;">
									<table class="table table-bordered">
										<thead style="border:1px solid black;">
											<tr>
												<th rowspan="2" style="text-align:center;">No</th>
												<th rowspan="2" style="text-align:center;">PERNYATAAN</th>
												<th colspan="3" style="text-align:center;">JAWABAN</th>
												
											</tr>
											<tr>
												<th rowspan="2" style="text-align:center;">Ya</th>
												<th rowspan="2" style="text-align:center;">Tidak</th>
												<th colspan="3" style="text-align:center;">Penjelasan</th>
											</tr>
										</thead>
										<tbody style="border:1px solid black;">
											<tr>
												<td>1</td>
												<td>Apakah anda pernah melamar di group /perusahaan ini sebelumnya? Kapan dan sebagai apa?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" onclick="return false" type="radio" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" onclick="return false" type="radio" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>2</td>
												<td>Selain disini, di perusahaan mana lagi anda melamar waktu ini? sebagai apa?</td>
												<div class="form-check">
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK">
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>3</td>
												<td>Apakah anda terikat kontrak dengan perusahaan tempat kerja saat ini</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK">
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>4</td>
												<td>Apakah anda mempunyai pekerjaan sampingan / part timer? Dimana dan sebagai apa?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK">
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>5</td>
												<td>Apakah anda berkeberatan bila kami meminta referensi pada perusahaan tempat anda pernah bekerja?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>6</td>
												<td>Apakah anda mempunyai teman / saudara yang bekerja di group/perusahaan ini? Sebutkan nama & jabatannya.</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>7</td>
												<td>Apakah anda pernah menderita penyakit menular/sakit keras/kronis/kecelakaan berat/operasi? Kapan dan bagaimana?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>8</td>
												<td>Apakah anda pernah menjalani pemeriksaan psikologis/psikotest? Dimana dan untuk tujuan apa?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>9</td>
												<td>Apakah anda pernah berurusan dengan polisi karena tindakan suatu kejahatan?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>10</td>
												<td>Bilamana diterima, bersedikah bertugas ke luar kota?</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK" >
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>11</td>
												<td>Bersedikah anda untuk melaksanakan ikatan kerja untuk kurun waktu tertentu (kontrak)</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="YA" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="TIDAK">
													</td>
												</div>
												<td>
													<input type="text" class="form-control" name="penjelasan" value="">																
												</td>
											</tr>
											<tr>
												<td>12</td>
												<td>Macam pekerjaan/ jabatan apakah yang sesuai dengan cita-cita anda?</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
												
											</tr>
											<tr>
												<td>13</td>
												<td>Jenis pekerjaan bagaimana yang tidak anda sukai?</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
											</tr>
											<tr>
												<td>14</td>
												<td>Berapa besarkah penghasilan anda sebulan dan fasilitas apa saja yang anda peroleh saat ini?</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
											</tr>
											<tr>
												<td>15</td>
												<td>Bila diterima, berapa besar gaji dan fasilitas apa saja yang anda peroleh saat ini?</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
											</tr>
											<tr>
												<td>16</td>
												<td>Bila diterima, kapankah anda dapat mulai bekerja?</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
											</tr>
											<tr>
												<td>17</td>
												<td>Apakah anda mempunyai rencana menikah dalam waktu dekat? kapan? (bagi yang belum menikah)</td>
												<td style="text-align:center;" colspan="3">
													<input type="text" required class="form-control" name="jawaban">
												</td>
											</tr>
										</tbody>
									</table>
									<div class="row container">
										<div class="col" style="border:1px solid black;">
											Dengan ini, saya menyatakan bahwa keterangan yang saya berikan diatas benar isinya. Dengan mendatangani formulir ini 
											saya menyatakan bersedia untuk diberhentikan apabila yang saya tuliskan dalam formulir lamaran ini tidak benar.
										</div>
										<div class="col">
											<table>
												<tr>
													<td><label for="">Tempat Tanda Tangan</label></td>
													<td><input type="text" required name="tempat_ttd">&nbsp;&nbsp;&nbsp;<input type="text" name="tanggal_lamar"></td>
												</tr>
												<tr>
													<td><label for="">Nama Lengkap</label></td>
													<td><input type="text" name="nama_lengkap"></td>
												</tr>
											</table>
											
											
											
											
										</div>
									</div>
									
									<br>
								</div>
							</div>
							
							<br>
							<br>
						</div>
						<div class="tab">
							<b>MARSTON MODEL INDONESIA (MMI)</b>
							<br>
							<br>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
								<div class="col-sm-9">
									<input type="text" required name="nama_lengkap" class="form-control form-control-sm">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th style="text-align:center;">P</th>
												<th style="text-align:center;">K</th>
												<th style="text-align:center;">Contoh</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>
												<td>Mudah bergaul, ramah</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban" onclick="return false"  value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban" onclick="return false" value="kurang">
													</td>
												</div>
												<td>Penuh kepercayaan, percaya kpd orang lain</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>
												
												<td>Petualang, pengambil resiko</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>
												
												<td>Toleran, penuh hormat</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col">
									PETUNJUK: Bayangkan anda berada dalam salah satu setting lingkungan(kerja, keluarga, sekolah atau lainnya) 
									sesuai dengan tujuan pemeriksaan. Tugas anda membaca 4 kalimat yang terdapat di dalam
									masing-masing kotak. Klik pilihan pada kolom "P" kalimat yang PALING menggambarkan diri anda dalam setting yang sudah ditentukan tersebut.
									Kemudian klik pada kolom "K" disamping kalimat yang KURANG menggambarkan diri anda dalam setting tersebut, untuk masing-masing kotak,
									pilih satu respon PALING atau KURANG.
								</div>
							</div>
							<div class="row">
								<div class="col">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th style="text-align:center;">P</th>
												<th style="text-align:center;">K</th>
												<th style="text-align:center;">Contoh</th>
												<th style="text-align:center;">P</th>
												<th style="text-align:center;">K</th>
												<th style="text-align:center;">Contoh</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<div class="form-check" >
													
													<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="1">
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="kurang">
													</td>
												</div>
												<td>Mudah bergaul, ramah</td>

												<div class="form-check" >
													
														<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="2">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="kurang">
													</td>

												</div>
												
												<td>Yang penting adalah hasil</td>
											</tr>
											<tr>
												<div class="form-check" >
													
														<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="3">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang" >
													</td>
												</div>
												
												<td>Penuh kepercayaan, percaya kpd orang lain</td>
												<div class="form-check" >
													
														<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="4">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>
												
												<td>Melakukan dengan benar, ketepatan dihitung</td>
											</tr>
											<tr>
												<div class="form-check" >
													
														<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="5">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban"  value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban"  value="kurang">
													</td>
												</div>
												
												<td>Petualangan, percaya kpd orang lain</td>
									
												<div class="form-check" >
													
														<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="6">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio"  name="jawaban" value="kurang" >
													</td>
												</div>
												
												<td>Buat menjadi menyenangkan</td>
											</tr>
											<tr>
												<div class="form-check" >
													
													<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="7">
													
													<td style="text-align:center;">
														<input class="form-check-input" type="radio"  name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio"  name="jawaban" value="kurang">
													</td>
												</div>
												
												<td>Toleran, penuh hormat</td>
												<div class="form-check" >
													<input class="form-check-input" type="hidden" name="id_pertanyaan_marston_model" value="6">
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
													</td>
												</div>
												
												<td>Mari melakukan bersama</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
													<b>
														P
													</b>
													</td>
													<td style="text-align:center;">
													<b>
														K
													</b>
													</td>
												</div>
												
												<td></td>
												<div class="form-check" >
													<td style="text-align:center;">
													<b>
													P
													</b>
													</td>
													<td style="text-align:center;">
													<b>
													K
													</b>
													</td>
												</div>
												
												<td></td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>

												</div>
												
												<td>Lembut, pendiam</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang" >
													</td>
												</div>	
												<td>Akan melakukan tanpa kontrol</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>

												</div>
												
												<td>Optimis, pengkhayal</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban"  value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>	
												<td>Akan membeli berdasarkan hasrat</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" onclick="return false" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" onclick="return false" value="kurang" >
													</td>

												</div>
												
												<td>Pusat perhatian, mudah bersosialisasi</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>	
												<td>Akan menunggu, tidak ada tekanan</td>
											</tr>
											<tr>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling" >
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>
												<td>Pembuat perdamaian, membawa ketenangan</td>
												<div class="form-check" >
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="paling">
													</td>
													<td style="text-align:center;">
														<input class="form-check-input" type="radio" onclick="return false" name="jawaban" value="kurang">
													</td>
												</div>	
												<td>Akan membelanjakan apa yang saya inginkan</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
			<hr>
			<div style="width: 100%;">
			<table>
				<tr>
					<td>
						<button type="submit" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
					</td>
					
					<td>
						<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
					</td>
				</tr>
				</table>
			</div>
			<br>
	</form>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>

@include('js.personal.tambah')
@endsection
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>


