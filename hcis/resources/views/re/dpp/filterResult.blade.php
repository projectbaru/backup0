@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitmen @endslot

@slot('li_2') Data CV Pelamar @endslot
@endcomponent
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Data Personal Pelamar</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tamalamat')}}" class="btn btn-primary btn-sm" ><span class="d-block m-t-5" > <code style="color:white;">Buat Tampilan Baru</code></span></a>
        </div>
    </div>
                
    <!-- <h5>Per Table</h5> -->
    
</div><br>
<form action="{{ URL::to('/list_dpp/hapus_banyak') }}" method="POST" id="form_delete">
    @csrf
    
    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">            <thead style="color:black;font-size:12px;">
                <th style="">No</th>

                @for($i = 0; $i < count($th); $i++)
                    @if($th[$i] == 'kode_cv_pelamar')
                        <th style="">Kode CV Pelamar</th>
                    @endif
                    @if($th[$i] == 'kode_data_pelamar')
                        <th style="">Kode Data Pelamar</th>
                    @endif
                    @if($th[$i] == 'posisi_lamaran')
                        <th style="">Posisi yang Dilamaran</th>
                    @endif
                    @if($th[$i] == 'nama_lengkap')
                        <th style="">Nama Lengkap</th>
                    @endif
                    @if($th[$i] == 'nama_panggilan')
                        <th style="">Nama Panggilan</th>
                    @endif
                    @if($th[$i] == 'tempat_lahir')
                        <th style="">Tempat Lahir</th>
                    @endif
                    @if($th[$i] == 'tanggal_lahir')
                        <th style="">Tanggal Lahir</th>
                    @endif
                    @if($th[$i] == 'agama')
                        <th style="">Agama</th>
                    @endif
                    @if($th[$i] == 'kebangsaan')
                        <th style="">Kebangsaan</th>
                    @endif
                    @if($th[$i] == 'jenis_kelamin')
                        <th style="">Jenis Kelamin</th>
                    @endif
                    @if($th[$i] == 'berat_badan')
                        <th style="">Berat Badan</th>
                    @endif
                    @if($th[$i] == 'tinggi_badan')
                        <th style="">Tinggi Badan</th>
                    @endif
                    @if($th[$i] == 'no_ktp')
                        <th style="">No KTP</th>
                    @endif
                    
                    @if($th[$i] == 'email_pribadi')
                        <th style="">Email Pribadi</th>
                    @endif
                    @if($th[$i] == 'alamat_sosmed')
                        <th style="">Alamat Sosmed</th>
                    @endif
                    @if($th[$i] == 'kategori_sim')
                        <th style="">Kategori SIM</th>
                    @endif
                    @if($th[$i] == 'no_sim')
                        <th style="">No SIM</th>
                    @endif
                    @if($th[$i] == 'no_hp')
                        <th style="">No HP</th>
                    @endif
                    @if($th[$i] == 'no_telepon')
                        <th style="">No Telepon</th>
                    @endif 
                    @if($th[$i] == 'status_kendaraan')
                        <th style="">Status Kendaraan</th>
                    @endif
                    @if($th[$i] == 'jenis_merk_kendaraan')
                        <th style="">Jenis Merk Kendaraan</th>
                    @endif
                    @if($th[$i] == 'tempat_tinggal')
                        <th style="">Tempat Tinggal</th>
                    @endif
                    
                    @if($th[$i] == 'status_perkawinan')
                        <th style="">Status Perkawinan</th>
                    @endif
                    @if($th[$i] == 'alamat_didalam_kota')
                        <th style="">Alamat Didalam Kota</th>
                    @endif
                    @if($th[$i] == 'kode_pos_1')
                        <th style="">Kode Pos 1</th>
                    @endif
                    @if($th[$i] == 'alamat_diluar_kota')
                        <th style="">Alamat Diluar Kota</th>
                    @endif
                    @if($th[$i] == 'kode_pos_2')
                        <th style="">Kode Pos 2</th>
                    @endif
                    @if($th[$i] == 'pas_foto')
                        <th style="">Pas Foto</th>
                    @endif
                    @if($th[$i] == 'hoby_dan_kegiatan')
                        <th style="">Hoby dan Kegiatan</th>
                    @endif
                    @if($th[$i] == 'kegiatan_membaca')
                        <th style="">Kegiatan Membaca</th>
                    @endif
                    @if($th[$i] == 'surat_kabar')
                        <th style="">Surat Kabar</th>
                    @endif
                    @if($th[$i] == 'majalah')
                        <th style="">Majalah</th>
                    @endif
                    @if($th[$i] == 'pokok_yang_dibaca')
                        <th style="">Pokok yang Dibaca</th>
                    @endif
                    @if($th[$i] == 'tugas_jabatan_terakhir')
                        <th style="">Tugas Jabatan Terakhir</th>
                    @endif
                    @if($th[$i] == 'tanggal_lamar')
                        <th style="">Tanggal Lamar</th>
                    @endif
                    @if($th[$i] == 'gambar_denah')
                        <th style="">Gambar Denah</th>
                    @endif
                    @if($th[$i] == 'tempat_ttd')
                        <th style="">Tempat TTD</th>
                    @endif
                    @if($th[$i] == 'tanda_tangan')
                        <th style="">Tanda Tangan</th>
                    @endif
                    @if($th[$i] == 'status_rekaman')
                        <th style="">Status Rekaman</th>
                    @endif
                    @if($th[$i] == 'waktu_user_input')
                        <th style="">Waktu User Input</th>
                    @endif
                    
                    @if($i == count($th) - 1)
                        <th style="">Aksi</th>
                        <th style="">V</th>

                    @endif
                @endfor
            </thead>
            <tbody style="font-size:11px;">
                @php $b = 1; @endphp  @foreach($query as $row)
                    <tr>
                        <td>{{$b++}}</td>
                        @for($i = 0; $i < count($th); $i++)
                            @if($th[$i] == 'kode_cv_pelamar')
                                <td>{{ $row->kode_cv_pelamar ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kode_data_pelamar')
                                <td>{{ $row->kode_data_pelamar ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'posisi_lamaran')
                                <td>{{ $row->posisi_lamaran ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'nama_lengkap')
                                <td>{{ $row->nama_lengkap ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'nama_panggilan')
                                <td>{{ $row->nama_panggilan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tempat_lahir')
                                <td>{{ $row->tempat_lahir ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_lahir')
                                <td>{{ $row->tanggal_lahir ?? 'NO DATA', $row->tanggal_lahir ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'agama')
                                <td>{{ $row->agama ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kebangsaan')
                                <td>{{ $row->kebangsaan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'jenis_kelamin')
                                <td>{{ $row->jenis_kelamin ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'berat_badan')
                                <td>{{ $row->berat_badan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tinggi_badan')
                                <td>{{ $row->tinggi_badan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'no_ktp')
                                <td>{{ $row->no_ktp ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'email_pribadi')
                                <td>{{ $row->email_pribadi ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'alamat_sosmed')
                                <td>{{ $row->alamat_sosmed ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kategori_sim')
                                <td>{{ $row->kategori_sim ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'no_sim')
                                <td>{{ $row->no_sim ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'no_hp')
                                <td>{{ $row->no_hp ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'no_telepon')
                                <td>{{ $row->no_telepon ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'status_kendaraan')
                                <td>{{ $row->status_kendaraan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'jenis_merk_kendaraan')
                                <td>{{ $row->jenis_merk_kendaraan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tempat_tinggal')
                                <td>{{ $row->tempat_tinggal ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'status_perkawinan')
                                <td>{{ $row->status_perkawinan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'alamat_didalam_kota')
                                <td>{{ $row->alamat_didalam_kota ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kode_pos_1')
                                <td>{{ $row->kode_pos_1 ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'alamat_diluar_kota')
                                <td>{{ $row->alamat_diluar_kota ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kode_pos_2')
                                <td>{{ $row->kode_pos_2 ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'pas_foto')
                                <td>{{ $row->pas_foto ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'hoby_dan_kegiatan')
                                <td>{{ $row->hoby_dan_kegiatan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'kegiatan_membaca')
                                <td>{{ $row->kegiatan_membaca ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'surat_kabar')
                                <td>{{ $row->surat_kabar ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'majalah')
                                <td>{{ $row->majalah ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'pokok_yang_dibaca')
                                <td>{{ $row->pokok_yang_dibaca ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tugas_jabatan_terakhir')
                                <td>{{ $row->tugas_jabatan_terakhir ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanggal_lamar')
                                <td>{{ $row->tanggal_lamar ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'gambar_denah')
                                <td>{{ $row->gambar_denah ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tempat_ttd')
                                <td>{{ $row->tempat_ttd ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'tanda_tangan')
                                <td>{{ $row->tanda_tangan ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'status_rekaman')
                                <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                            @endif
                            @if($th[$i] == 'waktu_user_input')
                                <td>{{ $row->waktu_user_input ?? 'NO DATA' }}</td>
                            @endif
                            
                            @if($i == count($th) - 1)
                                <td>
                                    <a href="{{ URL::to('/list_dpp/detail/'.$row->id_data_pelamar) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a href="{{ URL::to('/list_dpp/edit/'.$row->id_data_pelamar) }}" class="">Edit</a>
                                </td>
                                <td>
                                    <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_data_pelamar }}">
                                </td>
                            @endif
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
        <table>
            <tr>
                <td><a class="btn btn-success btn-sm" href="{{route('tambah_dpp')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                <td>
                    <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                </td>
            </tr>
        </table>
        
</form>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush

@section('add-scripts')
<script>
       console.log("masuk");
            $("#example").DataTable();
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection