@extends('re.personal.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Detail Data Personal Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data Personal Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dpp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							@foreach($dpp as $data)
								<input type="hidden" name="temp_id" id="temp_id" />

								<div class="form-group" style="width:95%;">
									<div class="tab">
										<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
											<p>Tambah Data CV Pelamar</p>
										</div> -->
										<b>Formulir Data Pelamar</b><br><br>
										<p>Data Pribadi</p>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Pelamar</label>
													<div class="col-sm-9">
														<select name="kode_cv_pelamar" id="kode_cv_pelamar" class="form-control form-control-sm">
													
														</select>	
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Posisi yang dilamar</label>
													<div class="col-sm-9">
														<input type="text" required name="posisi_lamaran" style="font-size:11px;" value="{{$data->posisi_lamaran}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama lengkap (KTP)</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_lengkap" style="font-size:11px;" value="{{$data->nama_lengkap}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama panggilan</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_panggilan" style="font-size:11px;" value="{{$data->nama_panggilan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat & tgl lahir</label>
													<div class="col-sm-6">
														<input type="text" required name="tempat_lahir" style="font-size:11px;" value="{{$data->tempat_lahir}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
													<div class="col-sm-3">
														<input type="date" required name="tanggal_lahir" style="font-size:11px;" value="{{$data->tanggal_lahir}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Agama</label>
													<div class="col-sm-9">
														<select name="agama" id="agama" class="form-control form-control-sm">
															<option value="islam" data-kode ="" {{ $data->agama == 'islam' ? 'selected' : NULL }}>Islam</option>
															<option value="kristen" data-kode ="" {{ $data->agama == 'kristen' ? 'selected' : NULL }}>Kristen</option>
															<option value="budha" data-kode ="" {{ $data->agama == 'budha' ? 'selected' : NULL }}>Budha</option>
															<option value="hindu" data-kode ="" {{ $data->agama == 'hindu' ? 'selected' : NULL }}>Hindu</option>
															<option value="konghucu" data-kode ="" {{ $data->agama == 'konghucu' ? 'selected' : NULL }}>Kong Hu Cu</option>
														</select>	
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kebangsaan</label>
													<div class="col-sm-9">
														<input type="text" required name="kebangsaan" value="{{$data->kebangsaan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis kelamin</label>
													<div class="col-sm-9">
														<div class="form-check">
																<input class="form-check-input" type="radio" name="jenis_kelamin" value="laki-laki" {{ $data->jenis_kelamin == 'islam' ? 'checked' : NULL }}>
																Laki-laki&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
																Perempuan
														</div>
														<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">BB & TB</label>
													<div class="col-sm-9">
															<div class="row">
																<div class="col-sm-4">
																	<input type="text"  required name="berat_badan" value="{{$data->berat_badan}}" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >Kg
																</div>
																<div class="col-sm-5">
																	<input type="text"  required name="tinggi_badan" style="font-size:11px;" value="{{$data->tinggi_badan}}" class="form-control form-control-sm" placeholder="" >Cm
																</div>
															</div>
														<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kendaraan</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input" type="radio" name="status_kendaraan"  value="milik sendiri" {{ $data->status_kendaraan == 'milik sendiri' ? 'checked' : NULL }}>
															Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input" type="radio" name="status_kendaraan" value="kantor" {{ $data->status_kendaraan == 'kantor' ? 'checked' : NULL }}>
															Kantor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input" type="radio" name="status_kendaraan" value="orangtua" {{ $data->status_kendaraan == 'orangtua' ? 'checked' : NULL }}>
															Orangtua&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input" type="radio" name="status_kendaraan" value="lainnya" {{ $data->status_kendaraan == 'lainnya' ? 'checked' : NULL }}>
															Lainnya
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis kendaraan/merk</label>
													<div class="col-sm-9">
														<input type="text" name="jenis_kendaraan" value="{{$data->jenis_merk_kendaraan}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat didalam kota(Lengkap)</label>
													<div class="col-sm-9">
														<textarea type="text" name="alamat_didalam_kota" value="{{$data->alamat_didalam_kota}}" class="form-control form-control-sm" rows="4" cols="50"></textarea>
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
													<div class="col-sm-9">
														<input type="text" name="kode_pos_1" value="{{$data->kode_pos_1}}" class="form-control form-control-sm">
													</div>
												</div>
											</div>
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. KTP</label>
													<div class="col-sm-9">
														<input type="number" name="no_ktp" value="{{$data->no_ktp}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email Pribadi</label>
													<div class="col-sm-9">
														<input type="email" name="email_pribadi" value="{{$data->email_pribadi}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat sosmed pribadi</label>
													<div class="col-sm-9">
														<input type="text" name="alamat_sosmed" value="{{$data->alamat_sosmed}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">SIM yang dimiliki</label>
													<div class="col-sm-9">
														<div class="row">
															<div class="col-sm-4">
																<select name="kategori_sim" id="kategori_sim" class="form-control form-control-sm">
																	<option value="none" data-kode ="" {{ $data->kategori_sim == 'none' ? 'selected' : NULL }}>None</option>
																	<option value="sim a" data-kode ="" {{ $data->kategori_sim == 'sim a' ? 'selected' : NULL }}>SIM A</option>
																	<option value="sim b" data-kode ="" {{ $data->kategori_sim == 'sim b' ? 'selected' : NULL }}>SIM B</option>
																	<option value="sim b1" data-kode ="" {{ $data->kategori_sim == 'sim b1' ? 'selected' : NULL }}>SIM B1</option>
																	<option value="sim b2" data-kode ="" {{ $data->kategori_sim == 'sim b2' ? 'selected' : NULL }}>SIM B2</option>
																	<option value="sim c" data-kode =""  {{ $data->kategori_sim == 'sim c' ? 'selected' : NULL }}>SIM C</option>
																	<option value="sim d" data-kode ="" {{ $data->kategori_sim == 'sim d' ? 'selected' : NULL }}>SIM D</option>
																</select>		
															</div>
															<div class="col-sm-4">
																<input type="text"  required name="no_sim" value="{{$data->no_sim}}" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >No
															</div>
														</div>
														<!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. HP</label>
													<div class="col-sm-9">
														<input type="text" name="no_hp" class="form-control form-control-sm" value="{{$data->no_hp}}">
													</div>
												</div>
												
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. Telp</label>
													<div class="col-sm-9">
														<input type="text" name="no_telepon" value="{{$data->no_telepon}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat tinggal</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input"  type="radio" name="tempat_tinggal"  value="milik sendiri" {{ $data->tempat_tinggal == 'milik sendiri' ? 'checked' : NULL }}>Milik Sendiri&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="tempat_tinggal" value="kost/kontrak" {{ $data->tempat_tinggal == 'kost/kontrak' ? 'checked' : NULL }}>Kost/Kontrak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="tempat_tinggal" value="orangtua" {{ $data->tempat_tinggal == 'orangtua' ? 'checked' : NULL }}>Ikut Orangtua
														</div>
													</div>
												</div>	
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Perkawinan</label>
													<div class="col-sm-9">
														<div class="form-check">
															<input class="form-check-input"  type="radio" name="status_perkawinan"  value="menikah" {{ $data->status_perkawinan == 'menikah' ? 'checked' : NULL }}>Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="status_perkawinan" value="janda/duda" {{ $data->status_perkawinan == 'janda/duda' ? 'checked' : NULL }}>Janda/Duda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input class="form-check-input"  type="radio" name="status_perkawinan" value="tidak menikah" {{ $data->status_perkawinan == 'tidak menikah' ? 'checked' : NULL }}>Tidak Menikah
														</div>
													</div>
												</div>	
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat diluar kota(Lengkap)</label>
													<div class="col-sm-9">
														<textarea class="form-control form-control-sm"  type="text" name="alamat_diluar_kota" rows="4" cols="50"  value="{{$data->alamat_diluar_kota}}"></textarea>
													</div>
												</div>	
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
													<div class="col-sm-9">
														<input class="form-control form-control-sm"  type="text" name="kode_pos_2" value="{{$data->kode_pos_2}}">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Photo</label>
													<div class="col-sm-9">
														<input class="form-control form-control-sm"  type="file" name="pas_foto" value="{{$data->pas_foto}}">
													</div>
												</div>	
											</div>
										</div>
										<br>
										<b>Susunan Keluarga (termasuk pelamar)</b>
										<hr>
										@foreach($skp as $data_kel)
										<div class="row">
											<div class="col" style="font-size: 8px;">
												<table class="table" style="font-size: 8px;">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" ></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col" colspan="2" style="border:1px solid #dee2e6;">Pekerjaan Terakhir</th>
															<th scope="col"></th>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Hubungan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Nama</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Laki-laki/Perempuan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Usia(Tahun Lahir)</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Pendidikan Terakhir</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Jabatan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Perusahaan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Keterangan</th>
														</tr>
													</thead>
													<tbody class="table table-bordered" style="">
														<tr>
															<td><input type="text"  name="hubungan_keluarga" value="Ayah" class="form-control form-control-sm"></td>
															<td><input type="text" required name="nama_keluarga" value="{{$data_kel->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" readonly name="jenis_kelamin" value="L" class="form-control form-control-sm"></td>
															<td><input type="text" required value="usia_tahun_lahir" name="usia_tahun_lahir" value="{{$data_kel->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data_kel->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data_kel->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data_kel->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data_kel->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data_kel->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Ibu" class="form-control form-control-sm"></td>
															<td><input type="text" required name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" readonly name="jenis_kelamin" value="P" class="form-control form-control-sm"></td>
															<td><input type="text" required name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" required name="hubungan_keluarga" value="Saudara 1" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }} >
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" required value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }} >SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm" ></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>

														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 2" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<div class="form-group container">	
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio"  name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																	<label class="form-check-label" for="inlineRadio1">L</label>
																</div>
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																	<label class="form-check-label" for="inlineRadio2">P</label>
																</div>
															
															</div>
															<td><input type="text" name="usia_tahun_lahir" value="{{ $data->usia_tahun_lahir }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																			<label class="form-check-label" for="inlineRadio1">L</label>
																		</div>
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																			<label class="form-check-label" for="inlineRadio2">P</label>
																		</div>
																
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{ $data->usia_tahun_lahir }}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 3" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="P" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																	
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{ $data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 4" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<fieldset id="group2">
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }} >
																			<label class="form-check-label" for="inlineRadio1">L</label>
																		</div>
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																			<label class="form-check-label" for="inlineRadio2">P</label>
																		</div>
																	</fieldset>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 5" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 6" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
														<td><input type="text" name="hubungan_keluarga" value="Saudara 7" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 8" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 9" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr><br></tr>
														<tr><br></tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Isteri/Suami*" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->pendidikan_terakhir == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->pendidikan_terakhir == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 1" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 2" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->pendidikan_terakhir == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->pendidikan_terakhir == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 3" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }} >SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 4" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 5" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}} class="form-control form-control-sm"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										@endforeach
									</div>
									<div class="tab">
										<br>
										<b>Latar Belakang Pendidikan Formal / Informal</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 8px;">
												<table class="table" style="font-size: 8px;">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" ></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col"></th>
															<th scope="col" colspan="2" style="border:1px solid #dee2e6;">Pekerjaan Terakhir</th>
															<th scope="col"></th>
														</tr>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Hubungan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Nama</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Laki-laki/Perempuan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Usia(Tahun Lahir)</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Pendidikan Terakhir</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Jabatan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Perusahaan</th>
															<th scope="col" style="border:1px solid #dee2e6;font-size: 8px;">Keterangan</th>
														</tr>
													</thead>
													<tbody class="table table-bordered" style="">
														<tr>
															<td><input type="text"  name="hubungan_keluarga" value="Ayah" class="form-control form-control-sm"></td>
															<td><input type="text" required name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" readonly name="jenis_kelamin" value="L" class="form-control form-control-sm"></td>
															<td><input type="text" required value="{{$data->usia_tahun_lahir}}" name="usia_tahun_lahir" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Ibu" class="form-control form-control-sm"></td>
															<td><input type="text" required name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td><input type="text" readonly name="jenis_kelamin" value="P" class="form-control form-control-sm"></td>
															<td><input type="text" required name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" required name="hubungan_keluarga" value="Saudara 1" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" required value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>

														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 2" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<div class="form-group container">	
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																	<label class="form-check-label" for="inlineRadio1">L</label>
																</div>
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																	<label class="form-check-label" for="inlineRadio2">P</label>
																</div>
															
															</div>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">	
																	<div class="col-sm-10">
																		<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																			<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																			<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																			<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																			<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																			<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																			<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																			<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																			<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																			<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																		</select>	
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 3" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="L" {{ $data->pendidikan_terakhir == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="P" {{ $data->pendidikan_terakhir == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																	
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 4" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<fieldset id="group2">
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																			<label class="form-check-label" for="inlineRadio1">L</label>
																		</div>
																		<div class="form-check form-check-inline">
																			<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																			<label class="form-check-label" for="inlineRadio2">P</label>
																		</div>
																	</fieldset>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }} >D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }} >D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 5" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 6" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
														<td><input type="text" name="hubungan_keluarga" value="Saudara 7" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'p' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 8" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'PASSWORD_BCRYPT' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Saudara 9" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }} >SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr><br></tr>
														<tr><br></tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Isteri/Suami*" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 1" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'checked' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 2" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 3" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{ $data->jabatan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{ $data->perusahaan }}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{ $data->keterangan_susunan_keluarga }}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 4" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{ $data->nama_keluarga }}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td><input type="text" name="hubungan_keluarga" value="Anak 5" class="form-control form-control-sm"></td>
															<td><input type="text" name="nama_keluarga" value="{{$data->nama_keluarga}}" class="form-control form-control-sm"></td>
															<td>
																<div class="form-group container">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td><input type="text" name="usia_tahun_lahir" value="{{$data->usia_tahun_lahir}}" class="form-control form-control-sm"></td>
															<td>
																<div class="col-sm-10">
																	<select required name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control form-control-sm">
																		<option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : NULL }}>SD</option>
																		<option value="SMP/Sederajat" {{ $data->pendidikan_terakhir == 'SMP/Sederajat' ? 'selected' : NULL }}>SMP/Sederajat</option>
																		<option value="SMK/Sederajat" {{ $data->pendidikan_terakhir == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
																		<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																		<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																		<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																		<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																		<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																		<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																	</select>	
																</div>
															</td>
															<td><input type="text" name="jabatan" value="{{$data->jabatan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="perusahaan" value="{{$data->perusahaan}}" class="form-control form-control-sm"></td>
															<td><input type="text" name="keterangan_susunan_keluarga" value="{{$data->keterangan_susunan_keluarga}}" class="form-control form-control-sm"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>

										<div style="width: 100%;text-align:center;">
											<table>
											<tr>
												<td>
													<a href="" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Buat Token Link</a>
												</td>
												<td>
													<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
												</td>
											</tr>
											</table>
										</div>
										<!-- Latar belakang -->
										<br>
										<b>Latar Belakang Pendidikan Formal / Informal</b>

										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="qualificationInputContainer">
												<table class="table">
													<thead>
														<tr style="border:1px solid #dee2e6;">
															<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tingkat</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Nama Sekolah</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tempat/Kota</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jurusan</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
															<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Lulus/Tidak</th>
														</tr>
													</thead>
													<tbody class="table table-bordered">
														<tr>
															<td>
																<!-- <input type="text" class="form-control form-control-sm" readonly value="SD"> -->
																<select name="tingkat_pendidikan" required id="tingkat_pendidikan" class="form-control form-control-sm" style="font-size:11px;">
																	<option value="sd" {{ $data->pendidikan_terakhir == 'sd' ? 'selected' : NULL }}>SD</option>
																	<option value="smp" {{ $data->pendidikan_terakhir == 'smp' ? 'selected' : NULL }}>SMP</option>
																	<option value="D1" {{ $data->pendidikan_terakhir == 'D1' ? 'selected' : NULL }}>D1</option>
																	<option value="D2" {{ $data->pendidikan_terakhir == 'D2' ? 'selected' : NULL }}>D2</option>
																	<option value="D3" {{ $data->pendidikan_terakhir == 'D3' ? 'selected' : NULL }}>D3</option>
																	<option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : NULL }}>S1</option>
																	<option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : NULL }}>S2</option>
																	<option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : NULL }}>S3</option>
																</select>	
															</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td>
																<div class="container form-group row">
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio1">L</label>
																	</div>
																	<div class="form-check form-check-inline">
																		<input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : NULL }}>
																		<label class="form-check-label" for="inlineRadio2">P</label>
																	</div>
																</div>
															</td>
															<td>
																<div class="col-sm-1">
																	<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addQualificationInput">
																		<i class="fa-solid fa-plus"></i>
																	</button>
																</div>
															</td>
														</tr>
														
													</tbody>
												</table>
											</div>

											</div>
										</div>
										<br>
										<b>Kursus/Pelatihan yang pernah diikuti</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="kursus">
											<table class="table table-bordered">
												<thead>
													
													<tr style="border:1px solid #dee2e6;">
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bidang/Jenis</th>
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Penyelenggara</th>
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tempat/Kota</th>
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Lama Kursus</th>
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Tahun</th>
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Dibiayai Oleh</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th><input type="text" required class="form-control form-control-sm" value="bidang_pelatihan" name="{{$data->bidang_pelatihan}}"></th>
														<td><input type="text" required class="form-control form-control-sm" value="penyelenggara" name="{{$data->penyelenggara}}"></td>
														<td><input type="text" required class="form-control form-control-sm" value="tempat_kota" name="{{$data->tempat_kota}}"></td>
														<td><input type="text" required class="form-control form-control-sm" value="lama_pelatihan" name="{{$data->lama_pelatihan}}"></td>
														<td><input type="text" class="form-control form-control-sm" required value="tahun_pelatihan" name="{{$data->tahun_pelatihan}}"></td>
														<td><input type="text" class="form-control form-control-sm" required value="dibiayai_oleh" name="{{$data->tahun_pelatihan}}"></td>
														<td>
															<div class="col-sm-1">
																<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addKursusInput">
																	<i class="fa-solid fa-plus"></i>
																</button>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
											</div>

											</div>
										</div>
										<br>
										<b>Pengetahuan Bahasa Asing</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="bahasa">
											<table class="table table-bordered">
												<thead>
													
													<tr style="border:1px solid #dee2e6;">
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Bahasa</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Mendengar</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Membaca</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Berbicara</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Menulis</th>
													</tr>
													
												</thead>
												<tbody>
													<tr>
														<td scope="row">
															<input type="text" required class="form-control form-control-sm" name="bahasa">
														</td>
														<td>
															<div class="col-sm-10">
																<select name="mendengar" required id="mendengar" class="form-control form-control-sm">
																	<option value="kurang"  {{ $data->mendengar == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" data-kode ="" {{ $data->mendengar == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" data-kode ="" {{ $data->mendengar == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="membaca" required id="membaca" class="form-control form-control-sm">
																	<option value="kurang" {{ $data->membaca == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" {{ $data->membaca == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $data->membaca == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="berbicara" required id="berbicara" class="form-control form-control-sm">
																	<option value="kurang" {{ $data->berbicara == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik" {{ $data->berbicara == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $data->berbicara == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-10">
																<select name="menulis" required id="menulis" class="form-control form-control-sm">
																	<option value="kurang" {{ $data->menulis == 'kurang' ? 'selected' : NULL }}>Kurang</option>
																	<option value="baik"  {{ $data->menulis == 'baik' ? 'selected' : NULL }}>Baik</option>
																	<option value="sangat baik" {{ $data->menulis == 'sangat baik' ? 'selected' : NULL }}>Sangat Baik</option>
																</select>	
															</div>
														</td>
														<td>
															<div class="col-sm-1">
																<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addBahasaInput">
																	<i class="fa-solid fa-plus"></i>
																</button>
															</div>
														</td>
													</tr>														
												</tbody>
											</table>
											</div>

											</div>
										</div>
										<br>
										<b>Kegiatan Sosial</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row" id="sosial">
											<table class="table table-bordered">
												<thead>
													<tr style="border:1px solid #dee2e6;">
														<th scope="col" style="border:1px solid #dee2e6;" rowspan="2">Organisasi</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jenis Kegiatan</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Jabatan</th>
														<th scope="col" style="border:1px solid #dee2e6;"rowspan="2">Tahun</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td scope="row"><input type="text" class="form-control form-control-sm" value="{{ $data->nama_organisasi }}"></td>
														<td><input type="text" class="form-control form-control-sm" value="{{ $data->jenis_kegiatan }}"></td>
														<td><input type="text" class="form-control form-control-sm" value="{{ $data->jabatan }}"></td>
														<td><input type="text" class="form-control form-control-sm" value="{{ $data->tahun }}"></td>
														<td>
															<div class="col-sm-1">
																<button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addSosialInput">
																	<i class="fa-solid fa-plus"></i>
																</button>
															</div>
														</td>
													</tr>
													
												</tbody>
											</table>
											</div>

											</div>
										</div>
										<br>
										<b>Hobby dan Kegiatan Waktu Luang</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<textarea name="hoby_dan_kegiatan" required value="{{$data->hoby_dan_kegiatan}}" class="form-control" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Hoby Saya ...">{{$data->hoby_dan_kegiatan}}</textarea>
											</div>
											</div>
										</div>
										<br>
										<b>Kegiatan Membaca</b>
										<hr>
										<div class="row">
											<div class="col">
												<p>Dibanding seluruh aktifitas waktu yang anda luangkan</p>
												<select name="kegiatan_membaca" required id="kegiatan_membaca" class="form-control form-control-sm">
													<option value="sedikit" {{ $data->kegiatan_membaca == 'sedikit' ? 'selected' : NULL }}>Sedikit</option>
													<option value="sedang" {{ $data->kegiatan_membaca == 'sedang' ? 'selected' : NULL }}>Sedang</option>
													<option value="banyak" {{ $data->kegiatan_membaca == 'banyak' ? 'selected' : NULL }}>Banyak</option>
												</select>	
												<br>
												<b>Media yang dibaca</b>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Surat Kabar</label>
													<div class="col-sm-9">
														<input type="text" required name="surat_kabar" value="{{$data->surat_kabar}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Majalah</label>
													<div class="col-sm-9">
														<input type="text" required name="majalah" value="{{$data->majalah}}" class="form-control form-control-sm">
													</div>
												</div>
											</div>
											<div class="col">
												<p>Pokok-pokok yang dibaca</p>
												<textarea name="pokok_yang_dibaca" required value="{{$data->pokok_yang_dibaca}}" class="form-control" id="exampleFormControlTextarea1" id="" cols="30" rows="5" placeholder="Pokok-pokok yang dibaca ...">{{$data->pokok_yang_dibaca}}</textarea>
											</div>
										</div>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<br>
									
										<div style="width: 100%;text-align:center;">
											<table>
											<tr>
												<td>
													<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
												</td>
											</tr>
											</table>
										</div>
									</div>
									<div class="tab">
										<br>
										<b>Riwayat Pekerjaan (mulai dari pekerjaan sekarang s/d terakhir)</b>
										<hr>
										<p>*isi tanda "-" jika belum memiliki pengalaman kerja atau magang</p>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
													<div class="col-sm-9">
														<input type="text" name="nama_perusahaan" value="{{$data->nama_perusahaan}}" required class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alamat</label>
													<div class="col-sm-9">
														<input type="text" name="alamat" value="{{$data->alamat}}" required class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama bekerja</label>
													<div class="col-sm-9">
														<input type="text" required name="lama_bekerja" value="{{$data->lama_bekerja}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama atasan langsung</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_atasan" value="{{$data->nama_atasan}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Gaji terakhir</label>
													<div class="col-sm-9">
														<input type="text" required name="gaji_terakhir" value="{{$data->gaji_terakhir}}"  class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis usaha</label>
													<div class="col-sm-9">
														<input type="text" required name="jenis_usaha" value="{{$data->jenis_usaha}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Alasan berhenti</label>
													<div class="col-sm-9">
														<input type="text" required name="alasan_berhenti" value="{{$data->alasan_berhenti}}" class="form-control form-control-sm">
													</div>
												</div>
											</div>
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan awal</label>
													<div class="col-sm-9">
														<input type="text" required name="jabatan_awal" class="form-control form-control-sm" value="{{$data->jabatan_awal}}">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan akhir</label>
													<div class="col-sm-9">
														<input type="text" required name="jabatan_akhir" class="form-control form-control-sm" value="{{$data->jabatan_akhir}}">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Telp perusahaan</label>
													<div class="col-sm-9">
														<input type="text" required name="telp_perusahaan" value="{{$data->telp_perusahaan}}" class="form-control form-control-sm">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Direktur</label>
													<div class="col-sm-9">
														<input type="text" required name="nama_direktur" value="{{$data->nama_direktur}}" class="form-control form-control-sm">
													</div>
												</div>
											</div>
										</div>
										<div >
										<a href="" class="btn btn-primary">Tambah Riwayat Pekerjaan</a>
										</div>

										<br>
										<b>Referensi (Kepada siapa kami dapat menanyakan diri anda lebih lengkap)</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>NAMA</th>
															<th>ALAMAT/TELEPON</th>
															<th>PEKERJAAN</th>
															<th>HUBUNGAN</th>
														</tr>
													</thead>
													<tbody>
														
														<tr>
															<td><input type="text" required class="form-control form-control-sm" name="nama_lengkap"></td>
															<td><input type="text" required class="form-control form-control-sm" name="alamat_telepon"></td>
															<td><input type="text" required class="form-control form-control-sm" name="pekerjaan"></td>
															<td><input type="text" required class="form-control form-control-sm" name="hubungan"></td>
														</tr>
													</tbody>
													
												</table>
											</div>
										</div>
										<br>
										<b>Orang yang dapat dihubungi dalam keadaan mendesak</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>NAMA</th>
															<th>ALAMAT/TELEPON</th>
															<th>PEKERJAAN</th>
															<th>HUBUNGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><input type="text" required class="form-control form-control-sm" name="nama_lengkap" value="{{$data->nama_lengkap}}"></td>
															<td><input type="text" required class="form-control form-control-sm" name="alamat_telpon" value="{{$data->alamat_telpon}}"></td>
															<td><input type="text" required class="form-control form-control-sm" name="pekerjaan" value="{{$data->pekerjaan}}"></td>
															<td><input type="text" required class="form-control form-control-sm" name="hubungan" value="{{$data->hubungan}}"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="tab">
										<br>
										<b>Uraikan tugas & tanggung jawab Anda pada jabatan yang terakhir</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<textarea name="tugas_jabatan_terakhir" class="form-control" required id="" cols="30" rows="7" placeholder="Tugas saya adalah.........." value="{{$data->tugas_jabatan_terakhir}}">{{$data->tugas_jabatan_terakhir}}</textarea>
												<br>*isi Fresh graduate jika belum miliki pengalaman
											</div>
										</div>
										<br>
										<b>Uraikan hal-hal positif yang menjadi kekuatan anda & hal negatif dalam diri anda yang ingin anda kembangkan</b>
										<hr>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<table class="" style="border:none;">
													<thead style="border:1px solid black;">
														<tr>
															<th></th>
															<th>Hal-hal Positif</th>
															<th></th>
															<th></th>
															<th>Hal-hal Negatif</th>
														</tr>
													</thead>
													<tbody style="border:1px solid black;">
														<tr>
															<td>1</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td></td>
															<td>1</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td>2</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td></td>
															<td>2</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td>3</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td></td>
															<td>3</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td>4</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td></td>
															<td>4</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
														</tr>
														<tr>
															<td>5</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
															<td></td>
															<td>5</td>
															<td><input type="text" required class="form-control form-control-sm"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<br>
										<b>Gambarkan secara jelas denah tempat tinggal anda dengan patokan jalan utama</b><br>(digambar pada saat anda diundang interview)
										<hr>
										<i style="font-size:10px;">Masukkan berupa gambar</i><br>
										<input type="file" href=""  class="btn btn-primary btn-sm" value="Gambar Denah Tempat Tinggal">
											<img src="{{url('/data_file/'.$data -> )}}"  width="100" height="100" alt="">
										</input>
										<br><br>
										<div style="width: 100%;text-align:center;">
											<table>
											<tr>
												<td>
													<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal Simpan Data</a>
												</td>
											</tr>
											</table>
										</div>
									</div>
									<div class="tab">
										<div class="row">
											<div class="col" style="font-size: 14px;">
												<table class="table table-bordered">
													<thead style="border:1px solid black;">
														<tr>
															<th rowspan="2" style="text-align:center;">No</th>
															<th rowspan="2" style="text-align:center;">PERNYATAAN</th>
															<th colspan="3" style="text-align:center;">JAWABAN</th>
															
														</tr>
														<tr>
															<th rowspan="2" style="text-align:center;">Ya</th>
															<th rowspan="2" style="text-align:center;">Tidak</th>
															<th colspan="3" style="text-align:center;">Penjelasan</th>
														</tr>
													</thead>
													<tbody style="border:1px solid black;">
														<tr>
															<td>1</td>
															<td>Apakah anda pernah melamar di group /perusahaan ini sebelumnya? Kapan dan sebagai apa?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="1">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" {{ $data->pendidikan_terakhir == 'YA' ? 'selected' : NULL }}>
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" {{ $data->pendidikan_terakhir == 'TIDAK' ? 'selected' : NULL }}>
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Selain disini, di perusahaan mana lagi anda melamar waktu ini? sebagai apa?</td>
															<div class="form-check">
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="2">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" {{ $data->pendidikan_terakhir == 'YA' ? 'selected' : NULL }}>
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" {{ $data->pendidikan_terakhir == 'YA' ? 'selected' : NULL }}>
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Apakah anda terikat kontrak dengan perusahaan tempat kerja saat ini</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="3">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK">
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Apakah anda mempunyai pekerjaan sampingan / part timer? Dimana dan sebagai apa?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="4">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK">
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Apakah anda berkeberatan bila kami meminta referensi pada perusahaan tempat anda pernah bekerja?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="5">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Apakah anda mempunyai teman / saudara yang bekerja di group/perusahaan ini? Sebutkan nama & jabatannya.</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="6">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Apakah anda pernah menderita penyakit menular/sakit keras/kronis/kecelakaan berat/operasi? Kapan dan bagaimana?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="7">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>8</td>
															<td>Apakah anda pernah menjalani pemeriksaan psikologis/psikotest? Dimana dan untuk tujuan apa?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="8">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>9</td>
															<td>Apakah anda pernah berurusan dengan polisi karena tindakan suatu kejahatan?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="9">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>10</td>
															<td>Bilamana diterima, bersedikah bertugas ke luar kota?</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="10">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK" >
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>11</td>
															<td>Bersedikah anda untuk melaksanakan ikatan kerja untuk kurun waktu tertentu (kontrak)</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="11">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="YA" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="TIDAK">
																</td>
															</div>
															<td>
																<input type="text" class="form-control" name="penjelasan" value="">																
															</td>
														</tr>
														<tr>
															<td>12</td>
															<td>Macam pekerjaan/ jabatan apakah yang sesuai dengan cita-cita anda?</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="12">
															</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
															
														</tr>
														<tr>
															<td>13</td>
															<td>Jenis pekerjaan bagaimana yang tidak anda sukai?</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="13">
															</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
														</tr>
														<tr>
															<td>14</td>
															<td>Berapa besarkah penghasilan anda sebulan dan fasilitas apa saja yang anda peroleh saat ini?</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="14">
															</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
														</tr>
														<tr>
															<td>15</td>
															<td>Bila diterima, berapa besar gaji dan fasilitas apa saja yang anda peroleh saat ini?</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="15">
															</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
														</tr>
														<tr>
															<td>16</td>
															<td>Bila diterima, kapankah anda dapat mulai bekerja?</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="16">
															</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
														</tr>
														<tr>
															<td>17</td>
															<td style="text-align:center;">
																<input class="form-control form-control-sm" type="hidden" name="id_pertanyaan_pilihan_esai"  value="17">
															</td>
															<td>Apakah anda mempunyai rencana menikah dalam waktu dekat? kapan? (bagi yang belum menikah)</td>
															<td style="text-align:center;" colspan="3">
																<input type="text" required class="form-control" name="jawaban">
															</td>
														</tr>
													</tbody>
												</table>
												<div class="row">
													<div class="col" style="border:1px solid black;">
														Dengan ini, saya menyatakan bahwa keterangan yang saya berikan diatas benar isinya. Dengan mendatangani formulir ini 
														saya menyatakan bersedia untuk diberhentikan apabila yang saya tuliskan dalam formulir lamaran ini tidak benar.
													</div>
													<div class="col">
														<label for="">Tempat Tanda Tangan</label>
															<input type="text" required name="tempat_ttd">&nbsp;&nbsp;&nbsp;<input type="text" name="tanggal_lamar">
														<label for="">Nama Lengkap</label>
															<input type="text" name="nama_lengkap">
														
													</div>
												</div>
												
												<br>
											</div>
										</div>
										
										<br>
										<br>
									</div>
									<div class="tab">
										<b>MARSTON MODEL INDONESIA (MMI)</b>

										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
											<div class="col-sm-9">
												<input type="text" required name="nama_lengkap" class="form-control form-control-sm">
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center;">P</th>
															<th style="text-align:center;">K</th>
															<th style="text-align:center;">Contoh</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" checked>
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" >
																</td>
															</div>
															<td>Mudah bergaul, ramah</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"   >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  checked>
																</td>
															</div>
															<td>Penuh kepercayaan, percaya kpd orang lain</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" checked>
																</td>
															</div>
															
															<td>Petualang, pengambil resiko</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" checked >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" >
																</td>
															</div>
															
															<td>Toleran, penuh hormat</td>
														</tr>
													</tbody>
												</table>

											</div>
											<div class="col">

												PETUNJUK: Bayangkan anda berada dalam salah satu setting lingkungan(kerja, keluarga, sekolah atau lainnya) 
												sesuai dengan tujuan pemeriksaan. Tugas anda membaca 4 kalimat yang terdapat di dalam
												masing-masing kotak. Klik pilihan pada kolom "P" kalimat yang PALING menggambarkan diri anda dalam setting yang sudah ditentukan tersebut.
												Kemudian klik pada kolom "K" disamping kalimat yang KURANG menggambarkan diri anda dalam setting tersebut, untuk masing-masing kotak,
												pilih satu respon PALING atau KURANG.
											</div>
										</div>
										<div class="row">
											<div class="col">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center;">P</th>
															<th style="text-align:center;">K</th>
															<th style="text-align:center;">Contoh</th>
															<th style="text-align:center;">P</th>
															<th style="text-align:center;">K</th>
															<th style="text-align:center;">Contoh</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="kurang">
																</td>
															</div>
															<td>Mudah bergaul, ramah</td>

															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>

															</div>
															
															<td>Yang penting adalah hasil</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="kurang">
																</td>
															</div>
															
															<td>Penuh kepercayaan, percaya kpd orang lain</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>
															
															<td>Melakukan dengan benar, ketepatan dihitung</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling"  >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>
															</div>
															
															<td>Petualangan, percaya kpd orang lain</td>
												
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>
															
															<td>Buat menjadi menyenangkan</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling"  >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>
															
															<td>Toleran, penuh hormat</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>
															</div>
															
															<td>Mari melakukan bersama</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																<b>
																	P
																</b>
																</td>
																<td style="text-align:center;">
																<b>
																	K
																</b>
																</td>
															</div>
															
															<td></td>
															<div class="form-check" >
																<td style="text-align:center;">
																<b>
																P
																</b>
																</td>
																<td style="text-align:center;">
																<b>
																K
																</b>
																</td>
															</div>
															
															<td></td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>

															</div>
															
															<td>Lembut, pendiam</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>
															</div>	
															<td>Akan melakukan tanpa kontrol</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>

															</div>
															
															<td>Optimis, pengkhayal</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>	
															<td>Akan membeli berdasarkan hasrat</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban"  value="paling">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>

															</div>
															
															<td>Pusat perhatian, mudah bersosialisasi</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>	
															<td>Akan menunggu, tidak ada tekanan</td>
														</tr>
														<tr>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling" >
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang" >
																</td>
															</div>
															<td>Pembuat perdamaian, membawa ketenangan</td>
															<div class="form-check" >
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="paling">
																</td>
																<td style="text-align:center;">
																	<input class="form-check-input" type="radio" name="jawaban" value="kurang">
																</td>
															</div>	
															<td>Akan membelanjakan apa yang saya inginkan</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div style="width: 100%;">
								<table>
									<tr>
										<td>
											<a href="{{ route('simpan_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</a>
										</td>
										<td>
											<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
										</td>
										<td>
											<a href="{{ route('list_dpp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
										</td>
									</tr>
									</table>
								</div>
								<br>
							@endforeach
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

@endsection

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@include('js.personal.tambah')

@endsection
