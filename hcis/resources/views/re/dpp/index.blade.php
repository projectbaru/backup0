@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('li_3') Data Personal Pelamar @endslot
@slot('title') Recruitment @endslot
@endcomponent
<br>
<div class="card-header">
    <div class="row">
        <div class="col">
        <h4 class="card-title">Data Personal Pelamar</h4>
        </div>
        <div class="col" style="text-align: right;">
            <a href="{{route('ubah_tampersonal')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
        </div>
    </div>
    <!-- <h5>Per Table</h5> -->
</div><br>

<form action="{{ URL::to('/list_dpp/hapus_banyak') }}" method="POST" id="form_delete">
<table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead style="color:black;font-size: 14px; ">
        <tr>
            <th style="">No</th>
            <th style=" " scope="col">Kode Personal Pelamar</th>
            <th style="" scope="col">Posisi Yang Dilamaran</th>
            <th style="" scope="col">Nama Lengkap (KTP)</th>
            <th style="" scope="col">No. KTP</th>
            <th style="" scope="col">No. HP</th>
            <th style="" scope="col">Email Pribadi</th>
            <th style="" scope="col">Status</th>
            <th style="" scope="col">Action</th>
            <th style="" scope="col">V</th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;color:black;">
    @php $b=1; @endphp
        @foreach($rc_cv as $data)
        {{-- {{dd($data)}} --}}
            <tr>
                <td>{{$b++;}}</td>
                <td style="font-size: 12px;color:black;">{{$data->kode_cv_pelamar}}</td>
                <td style="font-size: 12px;color:black;">{{$data->posisi_lamaran}}</td>
                <td style="font-size: 12px;color:black;">{{$data->nama_lengkap}}</td>
                <td style="font-size: 12px;color:black;">{{$data->no_ktp}}</td>
                <td style="font-size: 12px;color:black;">{{$data->no_hp}}</td>
                <td style="font-size: 12px;color:black;">{{$data->email_pribadi}}</td>
                <td style="font-size: 12px;color:black;">{{$data->status_perkawinan}}</td>
                <td>
                    <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                    <a href="{{ URL::to('/list_dpp/detail/'.$data->id_data_pelamar) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="{{ URL::to('/list_dpp/edit/'.$data->id_data_pelamar) }}" class="">Edit</a>
                </td>
                <td>
                    <input type="checkbox" name="multiDelete[]" value="{{ $data->id_data_pelamar }}" id="multiDelete" >
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<table>
    <tr>
        <td>
            <a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_dpp')}}">Tambah</a></td>
        <td>
            <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
            <button class="btn btn-danger btn-sm"  style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
        </td>
    </tr>
</table>

<!DOCTYPE html>
<html>
<head>
    <title>Multiple Form in Table Layout</title>
</head>
<body>
    <h2>Add and Remove Multiple Entries</h2>
    <form id="multipleForm" action="process_form.php" method="post">
        <table id="formTable">
            <!-- Initial form fields in a table row -->
            <tr class="entry">
                <td><label for="name">Name:</label></td>
                <td><input type="text" name="name[]" required></td>

                <td><label for="email">Email:</label></td>
                <td><input type="email" name="email[]" required></td>

                <td><label for="message">Message:</label></td>
                <td><textarea name="message[]" rows="4" required></textarea></td>

                <td><button type="button" onclick="removeForm(this)">Remove</button></td>
            </tr>
        </table>

        <button type="button" onclick="addForm()">Add Another Entry</button>
        <button type="submit">Submit</button>
    </form>

    <script>
        function addForm() {
            const formTable = document.getElementById("formTable");
            const newRow = document.createElement("tr");
            newRow.classList.add("entry");

            newRow.innerHTML = `
                <td><label for="name">Name:</label></td>
                <td><input type="text" name="name[]" required></td>

                <td><label for="email">Email:</label></td>
                <td><input type="email" name="email[]" required></td>

                <td><label for="message">Message:</label></td>
                <td><textarea name="message[]" rows="4" required></textarea></td>

                <td><button type="button" onclick="removeForm(this)">Remove</button></td>
            `;

            formTable.appendChild(newRow);
        }

        function removeForm(button) {
            const formTable = document.getElementById("formTable");
            formTable.removeChild(button.parentElement.parentElement);
        }
    </script>
</body>
</html>


</form>

    
@endsection

    @push('scripts')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    @endpush


@section('add-scripts')

<script>
    $('#manual-ajax').click(function(event) {
  event.preventDefault();
  this.blur(); /
  $.get(this.href, function(html) {
    $(html).appendTo('body').modal();
  });
});
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>


<script>
     console.log("masuk");
            $("#example").DataTable();
    $(function() {
        
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection