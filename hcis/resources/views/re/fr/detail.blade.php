@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

@endpush


@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitment @endslot
@slot('li_3') Fullfillment Rate @endslot
@slot('title') Rekruitment @endslot
@endcomponent
    <div class="row">
        <div class="col" style="font-size: 10px;">
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                <div class="col-sm-5" style="font-size:11px;">
                    <input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-5" style="font-size:11px;">
                    <a href="{{route('list_fr')}}"  name="jabatan_lowongan" style="font-size:11px;" class="btn btn-danger" id="colFormLabelSm" placeholder=""> kembali</a>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-5" style="font-size:11px;">
                    <input type="text" readonly name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
                </div>
                <div class="col-sm-5" style="font-size:11px;">
                    <input type="text" readonly name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
                </div>
            </div>														
            
        </div>
        <div class="col" style="text-align:right;">
            <a href="#"  name="jabatan_lowongan" style="font-size:11px;" class="btn btn-danger" id="colFormLabelSm" placeholder="">Print</a>
        </div>
    </div>
    
    <div class="mb-6">

        <div class="table-responsive">
            <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9;">
                <thead style="color:black;font-size: 12px; ">
                    <tr>
                        <th style="width:3px;color:black;font-size: 12px;background-color:#a5a5a5; padding:-70px;">No</th> 
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Departemen</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Posisi</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Branch Office</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Work Location</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Level</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">MP Awal Disetujui</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">MP Perubahan (+)</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">MP Perubahan (-)</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">MP Akhir</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Emp. Existing</th>
                        <th style="color:black;font-size: 12px;background-color:#a5a5a5;" scope="col">Vacant</th>
                    </tr>
                </thead>
                <tbody style="font-size: 11px;">
                    @php $b=1; @endphp
                    @foreach($fr_d as $data)
                    <tr>   
                        <td>{{$b++;}}</td>                                     
                        <td>{{$data->nama_departemen}}</td>
                        <td>{{$data->nama_posisi}}</td>
                        <td>{{$data->cabang_kantor}}</td>
                        <td>{{$data->lokasi_kerja}}</td>
                        <td>{{$data->level}}</td>
                        <td>{{$data->mp_awal_disetujui}}</td>
                        <td>{{$data->mp_perubahan_tambahan}}</td>
                        <td>{{$data->mp_perubahan_pengurangan}}</td>
                        <td>{{$data->mp_akhir}}</td>
                        <td>{{$data->mp_saat_ini}}</td>
                        <td>{{$data->vacant}}</td>
                        
                    </tr>
                    @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
    @endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush
@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
    // $(function() {
        //hang on event of form with id=myform
        $('#form-filter-kolom').on('submit', function(e) {
            e.preventDefault()

            var actionurl = "{{ route('filter_dcp') }}"
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $(this).serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah difilter.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        })

    // });
</script>
@endsection