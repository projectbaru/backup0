<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    <style>

    </style>
</head>
<body>
    <br>
    <div class="container" style="font-size:8px;text-align:center;">
    <button type="button" class="print-button" style="margin-bottom: 20px;font-size:10px;" onclick="window.print()">Print</button>
    <table>
        <thead>
        <tr>
            <th>No</th>
            <th>Dept</th>
            <th>Posisi</th>
            <th>Branch Office</th>
            <th>Work Location</th>
            <th>Level</th>
            <th>MP Awal Disetujui</th>
            <th>MP Perubahan (+)</th>
            <th>MP Perubahan (-)</th>
            <th>MP Akhir</th>
            <th>Emp.Existing</th>
            <th>Total MP</th>
            <th>Vacant</th>
            <th>Total Vacant</th>
        </tr>
        </thead>
        <tbody>
        @php 
            $b=1;
        @endphp    
        @foreach($fr as $data)
        <tr>
            <td>{{$b++;}}</td>
            <td>{{$data->nama_departemen}}</td>
            <td>{{$data->nama_posisi}}</td>
            <td>{{$data->cabang_kantor}}</td>
            <td>{{$data->lokasi_kerja}}</td>
            <td>{{$data->level}}</td>
            <td>{{$data->mp_awal_disetujui}}</td>
            <td>{{$data->mp_perubahan_tambah}}</td>
            <td>{{$data->mp_perubahan_pengurangan}}</td>
            <td>{{$data->mp_akhir}}</td>
            <td>{{$data->mp_saat_ini}}</td>
            <td>{{$data->total_mp_akhir}}</td>
            <td>{{$data->vacant}}</td>
            <td>{{$data->total_vacant}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    
    </div>
</body>
</html>