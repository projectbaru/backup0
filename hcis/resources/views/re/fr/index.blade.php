@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

@endpush


@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitment @endslot
@slot('li_3') Fullfillment Rate @endslot
@slot('title') Rekruitment @endslot
@endcomponent
<div class="row ">
    <div class="col-xl-12">
        <div class="card" style="border-radius:8px;border:none;">
            <div class="card-body  table-border-style">
                <div class="row" style="">
                    <div class="col">
                        <div class="form-group row">
                            <div class="col-sm-9">
                                <input type="button" value="Total MP Akhir : {{$total_fr}} " required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="TOtal MP Akhir"> 
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <div class="col-sm-9">
                                <input type="button" value="Total Vacant :  {{$total_va}}  " required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Total Vacant"> 
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            
                            <div class="col-sm-9">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <div class="col-sm-9">
                                <a type="button" value="Achievement : " required name="jabatan_lowongan" style="font-size:11px;" class="btn btn-primary btn-sm" id="colFormLabelSm" placeholder="Achievement">Print</a> 
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="mb-6">
                    <div class="table-responsive">
                    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead style="color:black;font-size: 12px; ">
                                <tr>
                                    <th style="">No</th> 
                                    <th style="" scope="col">Dept</th>
                                    <th style="" scope="col">MP Akhir</th>
                                    <th style="" scope="col">Vacant</th>
                                    <th style="" scope="col">Fullfillment</th>
                                    <th style="" scope="col">Action</th>
                                    <th style="" scope="col">V</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 11px;">
                            @php $b=1; @endphp
                            @foreach($fr as $data)
                            
                                <tr>    
                                
                                
                                    <td>{{ $b++; }}</td>
                                    <td>{{ $data->nama_departemen }}</td>
                                    <td>{{ $data->total_mp_akhir }}</td>
                                    <td>{{ $data->total_vacant }}</td>
                                    <td>{{ $data->fulfillment }}</td>
                                <td> <a href="{{route('detail_fr', $data->kode_departemen)}}" class="">Detail</a></td>
                                </tr>
                                
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
@endpush


@section('add-scripts')
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
    // $(function() {
        //hang on event of form with id=myform
        $('#form-filter-kolom').on('submit', function(e) {
            e.preventDefault()

            var actionurl = "{{ route('filter_dcp') }}"
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $(this).serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah difilter.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        })

    // });
</script>
@endsection