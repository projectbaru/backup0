@extends('re.cv.side')

@section('content')
<div class="pcoded-wrapper" >
            <div class="pcoded-content" >
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12" style="margin-top:10px;"> 
                                            <div class="page-header-title">
                                                <h5 class="m-b-10">Data CV Pelamar</h5>
                                            </div>
                                            <ul class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                                <li class="breadcrumb-item"><a href="#!">work strukture</a></li>
                                                <li class="breadcrumb-item"><a href="#!">data cv pelamar</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xl-12">
                                    <div class="card" style="border-radius:8px;border:none;">
                                        <div class="card-header">
                                            <!-- <h5>Per Table</h5> -->
                                            <a href="{{route('ubah_tamalamat')}}" style=""><span class="d-block m-t-5"> <code>Buat Tampilan Baru</code></span></a>
                                        </div>
                                        <div class="card-body table-border-style">
                                            <div class="table-responsive-xl"  style="overflow-x:auto">
                                            <!-- <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" > -->
                                                <!-- <h5 style="color:black;">Alamat Perusahaan</h5><hr> -->
                                            <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
                                                @csrf
                                                
                                                    <table id="example" class="table-responsive-xl table-striped table-bordered table-xl mt-2" style="width:100%; font-size: 12px; border:1px solid #d9d9d9; ">
                                                        <thead style="color:black;font-size:12px;">
                                                            <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No</th>

                                                            @for($i = 0; $i < count($th); $i++)
                                                                @if($th[$i] == 'kode_cv_pelamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Kode CV Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'jabatan_lowongan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jabatan Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'tingkat_pekerjaan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tingkat Pekerjaan</th>
                                                                @endif
                                                                @if($th[$i] == 'lokasi_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'resume_cv')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Resume CV</th>
                                                                @endif
                                                                @if($th[$i] == 'nama_lengkap')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Lengkap</th>
                                                                @endif
                                                                @if($th[$i] == 'email')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Email</th>
                                                                @endif
                                                                @if($th[$i] == 'no_hp')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">No Hp</th>
                                                                @endif
                                                                @if($th[$i] == 'lokasi_pelamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lokasi Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'pengalaman_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pengalaman Kerja</th>
                                                                @endif
                                                                @if($th[$i] == 'lama_pengalaman_kerja')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Lama Pengalaman Bekerja</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_lahir')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Lahir</th>
                                                                @endif
                                                                @if($th[$i] == 'jenis_kelamin')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Jenis Kelamin</th>
                                                                @endif
                                                                
                                                                @if($th[$i] == 'pendidikan_tertinggi')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pendidikan Tertinggi</th>
                                                                @endif
                                                                @if($th[$i] == 'nama_sekolah')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nama Sekolah</th>
                                                                @endif
                                                                @if($th[$i] == 'program_studi_jurusan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Program Studi Jurusan</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_wisuda')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Wisuda</th>
                                                                @endif
                                                                @if($th[$i] == 'nilai_rata_rata')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nilai Rata-rata</th>
                                                                @endif
                                                                @if($th[$i] == 'nilai_skala')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Nilai Skala</th>
                                                                @endif 
                                                                @if($th[$i] == 'informasi_lowongan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Informasi Lowongan</th>
                                                                @endif
                                                                @if($th[$i] == 'keterangan_informasi_pilihan')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Keterangan Informasi Pilihan</th>
                                                                @endif
                                                                @if($th[$i] == 'pesan_pelamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Pesan Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'tanggal_melamar')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Tanggal Pelamar</th>
                                                                @endif
                                                                @if($th[$i] == 'status_lamaran')
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Status Lamaran</th>
                                                                @endif
                                                                
                                                                @if($i == count($th) - 1)
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">Aksi</th>
                                                                    <th style="color:black;font-size: 12px;background-color:#a5a5a5;">V</th>

                                                                @endif
                                                            @endfor
                                                        </thead>
                                                        <tbody style="font-size:11px;">
                                                            @php $b = 1; @endphp  @foreach($query as $row)
                                                                <tr>
                                                                    <td>{{$b++}}</td>
                                                                    @for($i = 0; $i < count($th); $i++)
                                                                        @if($th[$i] == 'kode_cv_pelamar')
                                                                            <td>{{ $row->kode_cv_pelamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jabatan_lowongan')
                                                                            <td>{{ $row->jabatan_lowongan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tingkat_pekerjaan')
                                                                            <td>{{ $row->tingkat_pekerjaan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'lokasi_kerja')
                                                                            <td>{{ $row->lokasi_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'resume_cv')
                                                                            <td>{{ $row->resume_cv ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nama_lengkap')
                                                                            <td>{{ $row->nama_lengkap ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'email')
                                                                            <td>{{ $row->email ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'no_hp')
                                                                            <td>{{ $row->no_hp ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'lokasi_pelamar')
                                                                            <td>{{ $row->lokasi_pelamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pengalaman_kerja')
                                                                            <td>{{ $row->pengalaman_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'lama_pengalaman_kerja')
                                                                            <td>{{ $row->tahun_pengalaman_kerja ?? 'NO DATA', $row->bulan_pengalaman_kerja ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_lahir')
                                                                            <td>{{ $row->tanggal_lahir ? date('d-m-Y', strtotime($row->tanggal_lahir)) : 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'jenis_kelamin')
                                                                            <td>{{ $row->jenis_kelamin ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pendidikan_tertinggi')
                                                                            <td>{{ $row->pendidikan_tertinggi ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nama_sekolah')
                                                                            <td>{{ $row->nama_sekolah ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'program_studi_jurusan')
                                                                            <td>{{ $row->program_studi_jurusan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_wisuda')
                                                                            <td>{{ $row->tanggal_wisuda ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nilai_rata_rata')
                                                                            <td>{{ $row->nilai_rata_rata ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'nilai_skala')
                                                                            <td>{{ $row->nilai_skala ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'informasi_lowongan')
                                                                            <td>{{ $row->informasi_lowongan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'keterangan_informasi_pilihan')
                                                                            <td>{{ $row->keterangan_informasi_pilihan ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'pesan_pelamar')
                                                                            <td>{{ $row->pesan_pelamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'tanggal_melamar')
                                                                            <td>{{ $row->tanggal_melamar ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($th[$i] == 'status_lamaran')
                                                                            <td>{{ $row->status_lamaran ?? 'NO DATA' }}</td>
                                                                        @endif
                                                                        @if($i == count($th) - 1)
                                                                            <td>
                                                                                <a href="{{ URL::to('/list_dcp/detail/'.$row->id_cv_pelamar) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                                <a href="{{ URL::to('/list_dcp/edit/'.$row->id_cv_pelamar) }}" class="">Edit</a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" name="multiDelete[]" id="multiDelete"  value="{{ $row->id_cv_pelamar }}">
                                                                            </td>
                                                                        @endif
                                                                    @endfor
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <table>
                                                        <tr>
                                                            <td><a class="btn btn-success btn-sm" href="{{route('tambah_dcp')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                                            <td>
                                                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                            </form>
                                
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('add-scripts')
<script>
    $(function() {
        //hang on event of form with id=myform
        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Apakah anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endsection