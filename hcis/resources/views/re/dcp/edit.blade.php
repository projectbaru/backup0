
@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('li_3') Perencanaan Tenaga Kerja (PTK) @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header">
		<h5>Ubah Data CV Pelamar</h5>
	</div>
    <div class="card-body">
		<form action="{{route('update-dcp')}}" method="post" enctype="multipart/form-data">
			
			{{ csrf_field() }}
			@foreach($rc_cv as $data)
			<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_cv_pelamar }}" />

			<div class="form-group" style="width:95%;">
				<div class="">
					<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
						<p>Tambah Data CV Pelamar</p>
					</div> -->
						
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan yang dilamar</label>
									<!-- <div class="col-sm-9">
									<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div> -->
									<div class="col-sm-9">
										<input type="text" required name="jabatan_lowongan" value="{{$data->jabatan_lowongan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Melamar</label>
									<div class="col-sm-9">
										<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
										<select name="tingkat_pekerjaan" id="tingkat_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
											<option value="" selected disabled>--Tingkat--</option>
											<option value="staff" {{ $data->tingkat_pekerjaan == 'staff' ? 'selected' : NULL }}>Staff</option>
											<option value="middle" {{ $data->tingkat_pekerjaan == 'middle' ? 'selected' : NULL }}>Middle</option>
											<option value="senior" {{ $data->tingkat_pekerjaan == 'senior' ? 'selected' : NULL }}>Senior</option>
										</select>	
									</div>
							
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
									<div class="col-sm-9">
									<select name="lokasi_kerja[]"  id="select2-multiple" multiple="multiple" class="select2-multiple form-control form-control-sm" style="font-size:11px; ">
										@foreach ($datalk['looks'] as $np )
										<option value="{{$np->nama_lokasi_kerja}}" @if(old('np') == $np->nama_lokasi_kerja || $np->nama_lokasi_kerja == $data->lokasi_kerja) selected @endif>{{$np->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
									
									</div>
								
								</div>
							</div>
						</div>
						<hr>
						<b>Personal Information</b>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
									<div class="col-sm-9">
										<a style="font-size:11px;" class="btn btn-outline-success btn-sm" href="{{asset('data_file/'.$data->resume_cv)}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="download resume" download>Download</a>
									</div>
								</div>
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
									<div class="col-sm-9">
									<input type="nama_lengkap" required name="nama_lengkap" value="{{$data->nama_lengkap}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
									<div class="col-sm-9">
									<input type="email" name="email" style="font-size:11px;" value="{{$data->email}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
									<div class="col-sm-9">
									<input type="number" required name="no_hp" style="font-size:11px;" value="{{$data->no_hp}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
									<div class="col-sm-9">
									<input type="text" required name="lokasi_pelamar" style="font-size:11px;" value="{{$data->lokasi_pelamar}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
									<div class="col-sm-9">
										<div class="form-check">
											<input class="form-check-input" onchange="informasi(this);" type="radio" name="pengalaman_kerja"  value="Berpengalaman" {{ $data->pengalaman_kerja == 'Berpengalaman' ? 'checked' : NULL }} >
											<label class="form-check-label" for="flexRadioDefault1">
												Memiliki Pengalaman
											</label>
										</div>&nbsp;&nbsp;&nbsp;
										<div class="form-check">
											<input class="form-check-input" onchange="informasi(this);" type="radio" name="pengalaman_kerja" value="Lulusan Baru" {{ $data->pengalaman_kerja == 'Lulusan Baru' ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexRadioDefault1">
												Lulusan baru
											</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
									<div class="col-sm-4">
										<input type="hidden" required id="tahun_hid" name="tahun_pengalaman_kerja" style="font-size:11px;" value="{{$data->tahun_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">
										<input type="text" required id="tahun" name="tahun_pengalaman_kerja" style="font-size:11px;" value="{{$data->tahun_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">Tahun
									</div>
									<div class="col-sm-5">
										<input type="hidden" required id="bulan_hid" name="bulan_pengalaman_kerja" style="font-size:11px;" value="{{$data->bulan_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">
										<input type="text" required id="bulan" name="bulan_pengalaman_kerja" style="font-size:11px;" value="{{$data->bulan_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="" >Bulan
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
									<div class="col-sm-9">
									<input type="date" required name="tanggal_lahir" style="font-size:11px;" value="{{$data->tanggal_lahir}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
									
								</div>
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
									<!-- <div class="col-sm-4">
										<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
									</div>
									<div class="col-sm-5">
										<input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
									</div> -->
									<div class="col-sm-9">
										<div class="form-check">
											<input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1" value="{{$data->jenis_kelamin}}" {{ $data->jenis_kelamin == 'Laki-laki' ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexRadioDefault1">
												Laki-laki
											</label>
										</div>&nbsp;&nbsp;&nbsp;
										<div class="form-check">
											<input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1" value="{{$data->jenis_kelamin}}" {{ $data->jenis_kelamin == 'perempuan' ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexRadioDefault1">
												Perempuan
											</label>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					<hr>
					<b>Pendidikan</b>
					<br>
					
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
								<div class="col-sm-9">
									<select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="form-control form-control-sm" style="font-size:11px;">
										<!-- <option value="" selected disabled>--Tingkat--</option> -->
										<option value="SMK/Sederajat" {{ $data->pendidikan_tertinggi == 'SMK/Sederajat' ? 'selected' : NULL }}>SMK/Sederajat</option>
										<option value="D1" {{ $data->pendidikan_tertinggi == 'D1' ? 'selected' : NULL }}>D1</option>
										<option value="D2" {{ $data->pendidikan_tertinggi == 'D2' ? 'selected' : NULL }}>D2</option>
										<option value="D3" {{ $data->pendidikan_tertinggi == 'D3' ? 'selected' : NULL }}>D3</option>
										<option value="S1" {{ $data->pendidikan_tertinggi == 'S1' ? 'selected' : NULL }}>S1</option>
										<option value="S2" {{ $data->pendidikan_tertinggi == 'S2' ? 'selected' : NULL }}>S2</option>
										<option value="S3" {{ $data->pendidikan_tertinggi == 'S3' ? 'selected' : NULL }}>S3</option>

									</select>	
									<!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
								<div class="col-sm-9">
									<input type="text" name="nama_sekolah" style="font-size:11px;" value="{{$data->nama_sekolah}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
								<div class="col-sm-9">
									<input type="text" name="program_studi_jurusan" value="{{$data->program_studi_jurusan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
								<div class="col-sm-9">
									<input type="date" name="tanggal_wisuda" style="font-size:11px;" value="{{$data->tanggal_wisuda}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
								<div class="col-sm-4">
									<input type="input" name="nilai_rata_rata" style="font-size:11px;" value="{{$data->nilai_rata_rata}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>Skala*
								<div class="col-sm-4">
								<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
									<input type="text" name="nilai_skala" style="font-size:11px;" value="{{$data->nilai_skala}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<hr>
					<b style="font-size:11px;">Informasi Tambahan</b>
					<hr>
					<div class="row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
						<div class="col-sm-4">
							<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
								<option value="Web SRU" {{ $data->keterangan_informasi_pilihan == 'Web SRU' ? 'selected' : NULL }}>Web SRU</option>
								<option value="Job Street" {{ $data->keterangan_informasi_pilihan == 'Job Street' ? 'selected' : NULL }}>Job Street</option>
								<option value="Karyawan SRU" {{ $data->keterangan_informasi_pilihan == 'Karyawan SRU' ? 'selected' : NULL }}>Karyawan SRU</option>
								<option value="Lainnya" {{ $data->keterangan_informasi_pilihan == 'Lainnya' ? 'selected' : NULL }}>Lainnya</option>
							</select>	
						</div>
						<div class="col-sm-4">
							<input type="text" name="keterangan_informasi_pilihan" value="{{$data->keterangan_informasi_pilihan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
						</div>
					</div><br>
					<div class="row">
						<div class="col-sm-12 form-floating">
							<textarea class="form-control" name="pesan_pelamar" value="{{$data->pesan_pelamar}}" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px">{{$data->pesan_pelamar}}</textarea>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div style="width: 100%;">
				<table>
					<tr>
						<!-- <td>
							<input type="text">
						</td> -->
						<td>
							<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
						</td>
						<td>
							<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
						</td>
						<td>
							<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_dcp', $data->id_cv_pelamar)}}" id="btn_delete">Hapus</a>
						</td>
					</tr>
				</table>
			</div>
			<br>
			@endforeach
		</form>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
	// Select2 Multiple
  $('.select2-multiple').select2({
	  placeholder: "Lokasi Kerja",
		allowClear: true
	});

}); 
</script>

<script>
	function informasi(that) {
		var tahun_h = document.getElementById("tahun_hid").value;
		var bulan_h = document.getElementById("bulan_hid").value;
		if (that.value == "Berpengalaman") {
				document.getElementById("tahun").value =tahun_h;
				document.getElementById("bulan").value =bulan_h;
				document.getElementById("tahun").readOnly = false;
				document.getElementById("bulan").readOnly = false;
			} else {
				document.getElementById("tahun").value ='';
				document.getElementById("bulan").value ='';
				document.getElementById("tahun").readOnly = true;
				document.getElementById("bulan").readOnly = true;
			}
		}
</script>

@endpush

@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection