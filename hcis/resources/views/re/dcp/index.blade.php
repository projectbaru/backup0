@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<style>
    div.dataTables_wrapper div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
        justify-content: flex-end;
    }
    div.dataTables_wrapper div.dataTables_length select {
        width: auto;
        display: inline-block;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right;
    }
    div.dataTables_wrapper div.dataTables_filter input {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
}
</style>

@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="text-align: center;color: white;background-color:#0085fc;">
		<b>Data CV Pelamar</b>
	</div>
    <div class="card-body">
        <div class="table-responsive-xl " style="">
            <div class="" style=" border-radius:5px; border:2px solid #dfdfdf;">
                
                <div class="row">
                    <div class="col-md-4" style="">
                       
                        <div class="m-1 p-2" style="border:2px solid #f3f3f3;width:100%;">
                        <div class="container" style="text-align:center;">
                        <b>Filter Berdasarkan FPTK</b><br><br>
                        </div>
                            <form id="form-filter">
                                @csrf
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm" style="font-size:11px; text-align:left;">No Dokumen FPTK</label>
                                    <div class="col-sm-6">
                                        <select name="no_dokumen_fptk" id="no_dokumen_fptk" class="form-control form-control-sm" style="font-size:11px;">
                                            <option value="" selected disabled>--Pilih--</option>
                                            
                                            @foreach($fptk as $f)	
						                	    <option {{  $request->no_dokumen_fptk == $f->no_dokumen ? 'selected' : '' }} data-jabatan="{{ $f->jabatan_kepegawaian }}" data-pendidikan="{{ $f->pendidikan }}" data-lokasi="{{ $f->lokasi_kerja }}" value="{{ $f->no_dokumen }}">{{ $f->no_dokumen }}</option>
						                	@endforeach
						                </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Jabatan yang dibutuhkan</label>
                                    <div class="col-sm-6">
                
                                        <input type="text" id="jabatan_lowongan"   name="jabatan_lowongan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="" value="{{ $request->jabatan_lowongan ?? '' }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm" style="font-size:11px;text-align:left;">Minimal Pendidikan</label>
                                    <div class="col-sm-6">
                                        
                                        <input type="text" id="minimal_pendidikan"   name="minimal_pendidikan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="" value="{{ $request->minimal_pendidikan ?? '' }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm" style="text-align:left;font-size:11px;">Lokasi Kerja</label>
                                    <div class="col-sm-6">
                                        <input type="text" id="lokasi_kerja"   name="lokasi_kerja" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""  value="{{ $request->lokasi_kerja ?? '' }}" readonly>
                                    </div>
                                </div>
                            </form>
                            <div class="container" style="text-align:center;width:100%;">
                                <button
                                    type="submit"
                                    class="btn btn-sm btn-block btn-success"
                                    form="form-filter"
                                >
                                    Filter
                                </button>
                                <a
                                    type="submit"
                                    class="btn btn-sm btn-block btn-danger"
                                    href="{{route('list_dcp')}}"
                                >
                                    Clear Filter
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="row m-1" style="border:2px solid #f3f3f3;">
                            
                            <form id="form-filter-kolom" class="form-inline">
                                @csrf
                                @if(count($labels) > 0)
                                        <div class="row" style="font-size: 10px;">
                                        @foreach($labels as $idx => $label)
                                            <div class="col-md-2 m-2">
                                                <div class="form-check form-check-inline">
                                                    <input type="checkbox" class="form-check-input" name="cv[]" value="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;" readonly id="{{strtolower(str_replace(" ", "_", $label))}}" placeholder=""
                                                    @if (!empty($data->filter))
                                                        @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter)) {{ 'checked' }} @endif
                                                    @endif
                                                    >
                                                    <label class="form-check-label" for="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;">{{ $label }}</label>
                                                </div>
                                            </div>
                                            <!-- <div class=" row">
                                                <div class="">
                                                    <input type="checkbox" style="font-size:3px;" name="cv[]" value="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="{{strtolower(str_replace(" ", "_", $label))}}" placeholder=""
                                                        @if (!empty($data->filter))
                                                            @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter)) {{ 'checked' }} @endif
                                                        @endif
                                                    >
                                                </div>&nbsp;
                                                <label for="{{strtolower(str_replace(" ", "_", $label))}}" class=" " style="font-size:11px;">{{$label}}</label>
                                            </div>
                                            @if($idx%5 == 0 && $idx != 0)
                                                </div>
                                                <div class="col-md-2 m-2">
                                            @else
                                            @endif -->

                                        @endforeach
                                        </div>
                                    
                                @endif
                            </form>

                            &nbsp;
                            <div class="col row mt-2 mb-2">
                                <div class="col">
                                <div class="">
                                    <button
                                        type="submit"
                                        form="form-filter-kolom"
                                        class="btn btn-sm btn-primary"
                                    >
                                        Tampil Data
                                    </button>
                                </div>
                                </div>
                                <div class="col" style="text-align:right;color:white;">
                                <div class="">
                                    <a
                                        
                                        class="btn btn-sm btn-primary" 
                                    >
                                        Total Pelamar&nbsp;&nbsp;
                                        <button class="" style="background-color:white;color:black;border-radius:8px;">{{$pelamar_jmlh}}</button>
                                    </a>
                                </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="mb-6">
        <form action="{{ URL::to('/list_dcp/hapus_banyak') }}" method="POST" id="form_delete">
            @csrf
            <div class="table-responsive" >
            <table id="example" class="table table-striped table-hover  table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead style="color:black;font-size: 12px; ">
                        <tr  class="table-light">
                            <th style="width:3px; padding:-70px;">No</th>
                            @if (!empty($data->filter))
                                @if(count($labels) > 0)
                                    @foreach($labels as $idx => $label)
                                        @if (!empty($data->filter))
                                            @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                                <th style=" " scope="col">{{$label}}</th>
                                            @endif

                                            @else
                                            <th style=" " scope="col">{{$label}}</th>
                                        @endif
                                    @endforeach
                                @endif

                                @else

                                <th style="" scope="col">Jabatan dilamaran</th>
                                <th style=" " scope="col">Nama Lengkap</th>
                                <th style=" " scope="col">Pengalaman Kerja</th>
                                <th style="" scope="col">Pendidikan Tertinggi</th>
                                <th style="" scope="col">Status</th>
                                <th style="" scope="col">Kode CV Pelamar</th>

                            @endif
                            
                                
                            <th style="" scope="col">Aksi</th>
                            <th style="" scope="col">V</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 11px;">
                    @php $b=1; @endphp
                        
                        @if (!empty($data->filter))
                            
                            @foreach ($pelamar as $pelamars)
                                <tr>
                                    <td>{{ $b++ }}</td>
                                    @foreach ($data->filter as $filters)
                                        
                                        @if ($filters == "jabatan_dilamar")
                                            <td>{{ $pelamars->jabatan_lowongan }}</td>
                                        @elseif ($filters == "nama_lengkap")
                                            <td>{{ $pelamars->nama_lengkap }}</td>
                                        @elseif ($filters == "pengalaman_kerja")
                                            <td>{{ $pelamars->pengalaman_kerja }}</td>
                                        @elseif ($filters == "pendidikan_tertinggi")
                                            <td>{{ $pelamars->pendidikan_tertinggi }}</td>
                                        @elseif ($filters == "status")
                                            <td>{{ $pelamars->status_lamaran }}</td>
                                        @elseif ($filters == "kode_cv_pelamar")
                                            <td>{{ $pelamars->kode_cv_pelamar }}</td>
                                        @elseif ($filters == "no._hp")
                                            <td>{{ $pelamars->no_hp }}</td>
                                        @elseif ($filters == "lokasi_saat_ini")
                                            <td>{{ $pelamars->lokasi_pelamar }}</td>
                                        @elseif ($filters == "lokasi_kerja")
                                            <td>
                                                {{implode(", ",json_decode($pelamars->lokasi_kerja))}}
                                            </td>
                                        @elseif ($filters == "tanggal_lahir")
                                            <td>{{ $pelamars->tanggal_lahir }}</td>
                                        @elseif ($filters == "nama_sekolah")
                                            <td>{{ $pelamars->nama_sekolah }}</td>
                                        @elseif ($filters == "program_studi/jurusan")
                                            <td>{{ $pelamars->program_studi_jurusan }}</td>
                                        @elseif ($filters == "tanggal_wisuda")
                                            <td>{{ $pelamars->tanggal_wisuda }}</td>
                                        @elseif ($filters == "nilai_rata-rata")
                                            <td>{{ $pelamars->nilai_rata_rata }}</td>
                                        @elseif ($filters == "skala")
                                            <td>{{ $pelamars->nilai_skala }}</td>
                                        @elseif ($filters == "informasi_lowongan")
                                            <td>{{ $pelamars->informasi_lowongan }}</td>
                                        @elseif ($filters == "nama_karyawan/lainnya")
                                            <td>-</td>
                                        @elseif ($filters == "pesan_pelamar")
                                            <td>{{ $pelamars->pesan_pelamar }}</td>
                                        @elseif ($filters == "resume/cv")
                                            <td>{{ $pelamars->resume_cv }}</td>
                                        @elseif ($filters == "tanggal_melamar")
                                            <td>{{ $pelamars->tanggal_melamar }}</td>
                                        @elseif ($filters == "email")
                                            <td>{{ $pelamars->email }}</td>
                                        @endif
                                        
                                    @endforeach
                                    <td>
                                    
                                        <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                        <button type="button" class="btn btn-secondary btn-sm" onclick="getDetailData('{{$pelamars->id_cv_pelamar}}')">
                                            Detail
                                        </button>
                                        <!-- <a href="{{ URL::to('/list_dcp/detail/'.$pelamars->id_cv_pelamar) }}" class="btn btn-secondary btn-sm">Detail</a> -->
                                        <button type="button" class="btn btn-primary btn-sm" onclick="getEditData('{{$pelamars->id_cv_pelamar}}')">
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                    <input type="checkbox" name="multiDelete[]" value="{{ $pelamars->id_cv_pelamar }}" id="multiDelete" >
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            @foreach ($pelamar as $pelamars)
                                <tr>
                                    <td>{{ $b++ }}</td>
                                    <td>{{ $pelamars->jabatan_lowongan }}</td>
                                    <td>{{ $pelamars->nama_lengkap }}</td>
                                    <td>{{ $pelamars->pengalaman_kerja }}</td>
                                    <td>{{ $pelamars->pendidikan_tertinggi }}</td>
                                    <td>{{ $pelamars->status_lamaran }}</td>
                                    <td>{{ $pelamars->kode_cv_pelamar }}</td>
                                    <td>
                                        <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                        <button type="button" class="btn btn-secondary btn-sm" onclick="getDetailData('{{$pelamars->id_cv_pelamar}}')">
                                            Detail
                                        </button>
                                        <!-- <a href="{{ URL::to('/list_dcp/edit/'.$pelamars->id_cv_pelamar) }}" class="" >Edit</a> -->
                                        <button type="button" class="btn btn-primary btn-sm" onclick="getEditData('{{$pelamars->id_cv_pelamar}}')">
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="multiDelete[]" value="{{ $pelamars->id_cv_pelamar }}" id="multiDelete" >
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                        
                            {{-- <tr> --}}
                                {{-- <td style=""></td>
                                @if(isset($data) && isset($labels) && count($labels) > 0)
                                    @foreach($labels as $idx => $label)
                                        @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                            <td style="">{{ $label }}</td>
                                        @endif
                                    @endforeach
                                @else
                                    <td style="">s</td>
                                    <td style="">d</td>
                                    <td style="">d</td>
                                    <td style="">d</td>
                                    <td style="">d</td>
                                    <td style=""></td>
                                @endif
                        
                                <td>
                                    <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                    <a href="{{ URL::to('/list_dcp/detail/') }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a href="{{ URL::to('/list_dcp/edit/') }}" class="">Edit</a>
                                </td>
                                <td>
                                    <input type="checkbox" name="multiDelete[]" value="" id="multiDelete" >
                                </td> --}}
                            {{-- </tr> --}}
                    </tbody>
                </table>
            </div>

            <table>
                <tr>
                    <td>
                        <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop" style="font-size:11px;border-radius:5px;">Tambah</button>
                    </td>
                
                    <td>
                        <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                        <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                    </td>
                </tr>
            </table>
        </div>
        </form>
    </div>
</div>


<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
  Launch static backdrop modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#0085fc;">
        <b class="modal-title" id="staticBackdropLabel">Tambah Data CV Pelamar</b>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="{{route('simpan_dcp')}}" method="post" id="myForm" enctype="multipart/form-data">
			
			{{ csrf_field() }}
			<div class="form-group" style="width:95%;">
			<div class="">
			<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
			<p>Tambah Data CV Pelamar</p>
			</div> -->
			<b>Melamar Lowongan</b>
			<div class="row">
			<div class="col" style="font-size: 10px;">
                <div class="form-group row">
                    <input type="hidden" required name="kode_cv_pelamar" style="font-size:11px;" value="{{$randomId}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                </div>
                <div class="form-group row">
                    <input type="hidden" name="status_lamaran" style="font-size:11px;" value="Belum Diperiksa"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                </div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
					<!-- <div class="col-sm-9">
					<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					</div> -->
                    <div class="col-sm-9">
                    <select name="jabatan_lowongan" id="jabatan_kepegawaian" class="git form-control form-control-sm" style="font-size:11px;" onchange="onChangeJabatanKepegawaian()">
							<!-- <option value="" selected disabled>--Tingkat--</option> -->
                            <option value="" selected disabled>-- Pilih Jabatan --</option>
							@foreach ($dlk as $np )
                            <option data-tingkat="{{ $np->tingkat_pekerjaan}}" data-lokasi="{{ $np->lokasi_kerja }}" value="{{$np ->jabatan_lowongan}}">{{$np ->jabatan_lowongan}}</option>
                            @endforeach

						</select>	
                    </div>
					<!-- <div class="col-sm-9">
						<input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
					</div> -->
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
					<div class="col-sm-9">
						<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
						<select name="tingkat_pekerjaan" id="tingkat_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
							<option value="" selected disabled>--Tingkat--</option>
                            @foreach ($list_tingkat as $lt )
							    <option value="{{ $lt }}">{{ $lt }}</option>
							@endforeach
						</select>
					</div>

				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
					<div class="col-sm-9">
						<select name="lokasi_kerja[]"  id="lokasi_kerja_form" multiple="multiple" class="select2-multiple form-control form-control-sm" style="font-size:11px; width:100%; ">
							@foreach ($list_lokasi as $ll )
							    <option value="{{ $ll }}">{{ $ll }}</option>
							@endforeach
						</select>
					</div>
				
				</div>
			</div>
			</div>
			<hr>
			<b>Personal Information</b>
			<div class="row">
				<div class="col" style="font-size: 10px;">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
						<div class="col-sm-9">
						<input type="file" id="file_cv" download name="resume_cv" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
						</div>
					</div>
				
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
						<div class="col-sm-9">
						<input type="text" required name="nama_lengkap" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
						<div class="col-sm-9">
						<input type="email" name="email" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Alamat email">
						</div>
					</div>
				
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No.HP</label>
						<div class="col-sm-9">
						<input type="number" required name="no_hp" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="No Handphone">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
						<div class="col-sm-9">
							<input type="text" required name="lokasi_pelamar" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Kota/Kabupaten">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
						<div class="col-sm-9">
							<div class="form-check">
								<input class="form-check-input" type="radio" id="pengalaman_" name="pengalaman_kerja" onchange="informasi_pengalaman(this);" value="Berpengalaman" placeholder="">
								<label class="form-check-label" for="pengalaman_">
									Memiliki Pengalaman
								</label>
							</div>&nbsp;&nbsp;&nbsp;
							<div class="form-check">
								<input class="form-check-input" type="radio" id="lulusan_b"  name="pengalaman_kerja" onchange="informasi_pengalaman(this);" value="Lulusan Baru" placeholder="">
								<label class="form-check-label" for="lulusan_b">
									Lulusan Baru
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
						<div class="col-sm-4">
							<input type="number" required id="tahun" name="tahun_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="Angka" >Tahun
						</div>
						<div class="col-sm-5">
							<input type="number" required id="bulan" name="bulan_pengalaman_kerja" style="font-size:11px;" readonly class="form-control form-control-sm" placeholder="Angka" >Bulan
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
						<div class="col-sm-9">
							<input type="date" required name="tanggal_lahir" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="DD/MM/YYYY">
						</div>
						
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
						<div class="col-sm-9">
							<div class="form-check">
								<input class="form-check-input" name="jenis_kelamin" type="radio" id="laki_" value="Laki-laki" id="flexRadioDefault1">
								<label class="form-check-label" for="laki_">
									Laki-laki
								</label>
							</div>&nbsp;&nbsp;&nbsp;
							<div class="form-check">
								<input class="form-check-input" name="jenis_kelamin" type="radio" id="perem_" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1">
								<label class="form-check-label" for="perem_">
									Perempuan
								</label>
							</div>
							<!-- <input type="date" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
						</div>
					</div>
				</div>
			</div>
			<hr>
			<b>Pendidikan</b>
			<br>

			<div class="row">
			<div class="col" style="font-size: 10px;">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-labelgit -sm" style="font-size:11px;">Pendidikan Tertinggi</label>
					<div class="col-sm-9">
						<select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="git form-control form-control-sm" style="font-size:11px;">
							<option value="" selected disabled>--Silahkan Pilih--</option>
							<option value="SMK/Sederajat">SMK/Sederajat</option>
							<option value="D1">D1</option>
							<option value="D2">D2</option>
							<option value="D3">D3</option>
							<option value="S1">S1</option>
							<option value="S2">S2</option>
							<option value="S3">S3</option>

						</select>	
						<!-- <input type="text" required name="pendidikan_tertinggi" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
					<div class="col-sm-9">
						<input type="text" name="nama_sekolah" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Sekolah/Institusi">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
					<div class="col-sm-9">
						<input type="text" required name="program_studi_jurusan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Program Studi/Jurusan">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/ Tanggal Perkiraan Wisuda</label>
					<div class="col-sm-9">
						<input type="date" required name="tanggal_wisuda" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="DD/MM/YY">
					</div>
				</div>
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
					<div class="col-sm-4">
						<input type="input" required name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" class="form-control form-control-sm" placeholder="Angka">
					</div>Skala*
					<div class="col-sm-4">
					<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
						<input type="text" required name="nilai_skala" style="font-size:11px;" id="nilai_skala" class="form-control form-control-sm" placeholder="Mis. 0-4">
					</div>
				</div>
			</div>

			</div>
			<hr>
			<b style="font-size:11px;">Informasi Tambahan</b>
			<hr>
			<div class="row">
                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
                <div class="col-sm-4">
                    <select onchange="informasi_loker(this);" name="informasi_lowongan" class="form-control form-control-sm" style="font-size:11px;">
                        <option value="Web SRU">Web SRU</option>
                        <option value="Job Street" >Job Street</option>
                        <option value="Karyawan SRU" >Karyawan SRU</option>
                        <option value="Lainnya" >Lainnya</option>
                    </select>	
                </div>
                <div class="col-sm-4">
                    <input type="text" id="ifYes" readonly required name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm"  placeholder="Nama Karyawan SRU/lainnya.."> 
                </div>
            </div><br>
			<div class="row">
			<div class="col-sm-12 form-floating">
				<textarea class="form-control form-control-sm" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px"></textarea>
			</div>
			</div>
			</div>
			</div>
			
			<div class="modal-footer">
                <button type="submit" id="btn_submit" class="btn btn-primary btn-sm">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Batal</button>
            </div>
		</form>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="formEdit">
        ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="formDetail">
        ...
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
		 $(document).ready(function() {
			 // Select2 Multiple
			 $('.select2-multiple').select2({
				 placeholder: "Lokasi Kerja",
				 allowClear: true,
                 dropdownParent: $("#staticBackdrop")
			 });
		 });
 
	 </script>


<script>
	$(document).ready(function(){
   $("#file_cv").change(function(){
     fileobj = document.getElementById('file_cv').files[0];
     var fname = fileobj.name;
     var ext = fname.split(".").pop().toLowerCase();
     if(ext == "pdf" || ext == "jpeg" || ext == "png" || ext == "jpg"){
        $("#info_img_file").html(fname);
     }else{
        alert("Hanya untuk file pdf, jpg, jpeg dan png saja..");
        $("#file_cv").val("");
        $("#info_img_file").html("Tidak ada file yag dimasukkan");
        return false;
     }
   });
//    $("#audio_file").change(function(){
//      fileobj = document.getElementById('audio_file').files[0];
//      var fname = fileobj.name;
//      var ext = fname.split(".").pop().toLowerCase();
//      if(ext == "mp3" || ext == "mp4" || ext == "wav"){
//         $("#info_audio_file").html(fname);
//      }else{
//         alert("Accepted file mp3, mp4 and wav only..");
//         $("#aud_file").val("");
//         $("#info_audio_file").html("No file selected");
//         return false;
//      }
//    });
   $("#btn_submit").click(function(){
     var img_file = $('#file_cv').val();
     
     if(img_file =="" || aud_file ==""){
         alert('Please select the file');
        return false;
     }
   });
});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

@endpush

@section('add-scripts')


<script>
	function informasi_loker(that) {
    if ((that.value == "Lainnya") || (that.value == "Karyawan SRU")){
		
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
	}
</script>

<script>
	function informasi_pengalaman(that) {
    if (that.value == "Berpengalaman")  {
				document.getElementById("tahun").readOnly = false;;
				document.getElementById("bulan").readOnly = false;;
			} else {
				document.getElementById("tahun").readOnly = true;;
				document.getElementById("bulan").readOnly = true;;
			}
		}
</script>
<script>
    $('#nama_grup_lokasi_kerja').change(function(){
        $('#kode_grup_lokasi_kerja').val($('#nama_grup_lokasi_kerja option:selected').data('kode_grup_lokasi_kerja'));
    })
</script>

<script>
    $('#nama_lokasi_kerja').change(function(){
        $('#kode_lokasi_kerja').val($('#nama_lokasi_kerja option:selected').data('kode_lokasi_kerja'));
    })
</script>
<script>
      console.log("masuk");
            $("#example").DataTable();
    // $(function() {
        //hang on event of form with id=myform
        $('#form-filter-kolom').on('submit', function(e) {
            e.preventDefault()

            var actionurl = "{{ route('filter_dcp') }}"
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $(this).serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah difilter.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        })

    $('#no_dokumen_fptk').change(function() {
        var selectedValue = $(this).val();

        $('#jabatan_lowongan').val($(this).find(':selected').data('jabatan'));
        $('#minimal_pendidikan').val($(this).find(':selected').data('pendidikan'));
        $('#lokasi_kerja').val($(this).find(':selected').data('lokasi'));
    });

        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });

    // });
</script>
<script>
function getEditData(id) {
  let url = "{{ URL::to('/list_dcp/edit_data/:id') }}";
  url = url.replace(":id", id);
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function(data){
        console.log(data);
        $('#formEdit').html(data.data);
        $('#detailModal').modal('hide');
        $('#editModal').modal('show');
        $('#lokasi_kerja_edit').select2({
	        placeholder: "Lokasi Kerja",
		    allowClear: true,
		    dropdownParent: $("#editModal")
	    });
    }
 });
}

function informasi(that) {
    var tahun_h = $("#tahun_hid");
    var bulan_h = $("#bulan_hid");
    var edit_tahun = $("#edit_tahun");
    var edit_bulan = $("#edit_bulan");
    if (that.value == "Berpengalaman") {
      edit_tahun.val(tahun_h.val());
      edit_bulan.val(bulan_h.val());
      edit_tahun.prop("readOnly", false);
      edit_bulan.prop("readOnly", false);
	} else {
      edit_tahun.val("");
      edit_bulan.val("");
      edit_tahun.prop("readOnly", true);
      edit_bulan.prop("readOnly", true);
	}
}

function onChangeJabatanKepegawaian() {
    var selectedOption = $('#jabatan_kepegawaian').find('option:selected');
    var tingkatValue = selectedOption.data('tingkat');
    var lokasiValue = selectedOption.data('lokasi');
            
    $('#tingkat_pekerjaan').val(tingkatValue);
    $('#lokasi_kerja_form').val(lokasiValue).trigger('change');
}

function onChangeJabatanKepegawaianEdit() {
    var selectedOption = $('#jabatan_kepegawaian_edit').find('option:selected');
    var tingkatValue = selectedOption.data('tingkat');
    var lokasiValue = selectedOption.data('lokasi');
            
    $('#tingkat_pekerjaan_edit').val(tingkatValue);
    $('#lokasi_kerja_edit').val(lokasiValue).trigger('change');
}
</script>


<script>
   function informasi_sumber_edit(that) {
    var keterangan = $("#keterangan_informasi_pilihan_value");
    var ifYes_edit = $("#ifYes_edit");
    if ((that.value == "Lainnya") || (that.value == "Karyawan SRU")) {
      ifYes_edit.val(keterangan.val());
      ifYes_edit.prop("readOnly", false);
	} else {
      ifYes_edit.val("");
      ifYes_edit.prop("readOnly", true);
	}
}
</script>
<script>
function getDetailData(id) {
  let url = "{{ URL::to('/list_dcp/detail_data/:id') }}";
  url = url.replace(":id", id);
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    success: function(data){
        console.log(data);
        $('#formDetail').html(data.data);
        $('#detailModal').modal('show');
        $('#editModal').modal('hide');
        $('#lokasi_kerja_edit').select2({
	        placeholder: "Lokasi Kerja",
		    allowClear: true,
		    dropdownParent: $("#detailModal")
	    });
    }
 });
}


function deleteItem(linkHref) {
    Swal.fire({
        title: '<h4>Anda yakin ingin menghapus?</h4>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",     
        confirmButtonText: "<i>Ya, hapus!</i>",
        // buttonsStyling: false
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: linkHref,
                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil!",
                            text: "Data telah terhapus.",
                        }).then((result) => {
                            if (result.value) {
                                location.href = "{{ URL::to('list_dcp') }}";
                            }
                        });
                    }
                },
                error: function(e) {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: "Data tidak terhapus.",
                    });
                },
            });
        }
    })
}
</script>

@endsection