@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Rekruitment @endslot

@endcomponent 

<div class="row">
    <div class="col-xl-12">
        <div class="card" style="border-radius:8px;border:2px solid #eaebee;">
            <div class="card-header" style="text-align: center;color: white;background-color:#0085fc;">
                <b>Data Lowongan Kerja</b>
            </div>
            <div class="card-body  table-border-style" >
                <div class="table-responsive-xl " style="">
                    <div class=" p-2 mb-1" style=" border-radius:5px; border:2px solid #dfdfdf;">
                        <div class="container" style="color:white;text-align:center;">
                            <p>Data</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="font-size: 10px;">
                                <div class="m-2 p-2" style="width:95%;">
                                    <div class="row">
                                        <div class="col-md-10">
                                        <div class="m-1 p-2" style="width:100%;">
                                        <div class="container" style="text-align:center;">
                                        
                                        </div>
                                        <form action="{{ route('list_loker') }}" method="get" id="form-filter-ptk">
                                            @csrf
                                            <div class="row">
                                                <div class="col" style="font-size: 10px;">
                                                    <div class="form-group row">
                                                        <div class="col-sm-8">                                                            
                                                            <input type="text" rows="4" cols="50" name="cari" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Cari Data">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button form="form-filter-ptk" type="submit" class="btn btn-success btn-sm btn-block" style="font-size:11px;color:white;">
                                                                Filter
                                                            </button>
                                                            <a href="{{route('list_loker')}}" class="btn btn-danger btn-sm btn-block" style="font-size:11px;color:white;">Clear</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="container" style="text-align:center;width:20%;">
                                            
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="container" style="text-align:center;width:80%;">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row m-2" style="border:2px solid #f3f3f3;width:95%;">
                                    
                                    <form id="form-filter-kolom" class="form-inline">
                                        @csrf
                                        @if(count($labels) > 0)
                                                <div class="row" style="font-size: 10px;">
                                                @foreach($labels as $idx => $label)
                                                    <div class="col-md-2 m-2">
                                                        <div class="form-check form-check-inline">
                                                            <input type="checkbox" class="form-check-input" name="cv[]" value="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;" readonly id="{{strtolower(str_replace(" ", "_", $label))}}" placeholder=""
                                                            @if (!empty($data->filter))
                                                                @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter)) {{ 'checked' }} @endif
                                                            @endif
                                                            >
                                                            <label class="form-check-label" for="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;">{{ $label }}</label>
                                                        </div>
                                                    </div>
                                                    <!-- <div class=" row">
                                                        <div class="">
                                                            <input type="checkbox" style="font-size:3px;" name="cv[]" value="{{strtolower(str_replace(" ", "_", $label))}}" style="font-size:11px;" readonly class="form-control form-control-sm" id="{{strtolower(str_replace(" ", "_", $label))}}" placeholder=""
                                                                @if (!empty($data->filter))
                                                                    @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter)) {{ 'checked' }} @endif
                                                                @endif
                                                            >
                                                        </div>&nbsp;
                                                        <label for="{{strtolower(str_replace(" ", "_", $label))}}" class=" " style="font-size:11px;">{{$label}}</label>
                                                    </div>
                                                    @if($idx%5 == 0 && $idx != 0)
                                                        </div>
                                                        <div class="col-md-2 m-2">
                                                    @else
                                                    @endif -->

                                                @endforeach
                                                </div>
                                            
                                        @endif
                                    </form>

                                    &nbsp;
                                    <div class="col row mt-2">
                                        <div class="">
                                            <button
                                                type="submit"
                                                form="form-filter-kolom"
                                                class="btn btn-sm btn-primary"
                                            >
                                                Tampil Data
                                            </button>
                                        </div>&nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-6">
                    <div class="table-responsive">
                        <form action="{{ URL::to('/list_loker/hapus_banyak') }}" method="POST" id="form_delete">@csrf
                            <table id="example" class="table table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead style="color:black;font-size: 12px; ">
                                    <tr >
                                        <th style="width:3px; padding:-70px;">No</th>
                                        @if (!empty($data->filter))
                                            @if(count($labels) > 0)
                                                @foreach($labels as $idx => $label)
                                                    @if (!empty($data->filter))
                                                        @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                                            <th style=" " scope="col">{{$label}}</th>
                                                        @endif

                                                        @else
                                                        <th style=" " scope="col">{{$label}}</th>
                                                    @endif
                                                @endforeach
                                            @endif

                                            @else

                                            <th style="" scope="col">Jabatan</th>
                                            <th style=" " scope="col">Kategori Pekerjaan</th>
                                            <th style=" " scope="col">Lokasi</th>
                                            <th style="" scope="col">Pengalaman Kerja</th>
                                            <th style="" scope="col">Tingkat</th>
                                            

                                        @endif
                                        
                                            
                                        <th style="" scope="col">Aksi</th>
                                        <th style="" scope="col"><i class="fa-solid fa-check"></i></th>
                                    </tr>
                                </thead>
                                <tbody style="font-size: 11px;">
                                @php $b=1; @endphp
                                    
                                    @if (!empty($data->filter))
                                        
                                        @foreach ($pelamar as $pelamars)
                                            <tr>
                                                <td>{{ $b++ }}</td>
                                                @foreach ($data->filter as $filters)
                                                    
                                                    @if ($filters == "jabatan")
                                                        <td>{{ $pelamars->jabatan_lowongan }}</td>
                                                    @elseif ($filters == "kategori_pekerjaan")
                                                        <td>{{ $pelamars->kategori_pekerjaan }}</td>
                                                    @elseif ($filters == "lokasi")
                                                        <td>{{ implode(', ',is_array(json_decode($pelamars->lokasi_kerja)) ? json_decode($pelamars->lokasi_kerja) : ['']) }}</td>
                                                    @elseif ($filters == "status_kepegawaian")
                                                        <td>{{ $pelamars->status_kepegawaian }}</td>
                                                    @elseif ($filters == "pengalaman_kerja")
                                                        <td>{{ $pelamars->pengalaman_kerja }}</td>
                                                    @elseif ($filters == "tingkat")
                                                        <td>{{ $pelamars->tingkat_pekerjaan }}</td>
                                                    @elseif ($filters == "lokasi_saat_ini")
                                                        <td>{{ $pelamars->lokasi_pelamar }}</td>
                                                    @elseif ($filters == "minimal_pendidikan")
                                                        <td>{{ $pelamars->minimal_pendidikan }}</td>
                                                    @elseif ($filters == "peran_kerja")
                                                        <td>{{ $pelamars->peran_kerja }}</td>
                                                    @elseif ($filters == "deskripsi_pekerjaan")
                                                        <td>{{ $pelamars->deskripsi_pekerjaan }}</td>
                                                    @elseif ($filters == "syarat_pengalaman")
                                                        <td>{{ $pelamars->syarat_pengalaman }}</td>
                                                    @elseif ($filters == "syarat_kemampuan")
                                                        <td>{{ $pelamars->syarat_kemampuan }}</td>
                                                    @elseif ($filters == "syarat_lainnya")
                                                        <td>{{ $pelamars->syarat_lainnya }}</td>
                                                    @elseif ($filters == "tanggal_upload")
                                                        <td>{{ $pelamars->tanggal_upload }}</td>
                                                    @elseif ($filters == "status_lowongan")
                                                        <td>{{ $pelamars->status_lowongan }}</td>
                                                    @elseif ($filters == "kode_kategori_lowongan_kerja")
                                                        <td>{{ $pelamars->kode_kategori_lowongan_kerja }}</td>
                                                    @endif
                                                    
                                                @endforeach
                                                <td>
                                                    <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                    <!-- <a href="{{ URL::to('/list_loker/detail/'.$pelamars->id_lowongan_kerja) }}" class="btn btn-secondary btn-sm">Detail</a> -->
                                                    <button type="button" class="btn btn-secondary btn-sm" onclick="getDetailData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Detail
                                                    </button>
                                                    <!-- <a href="{{ URL::to('/list_loker/edit/'.$pelamars->id_lowongan_kerja) }}" class="btn btn-success btn-sm">Edit</a> -->
                                                    <button type="button" class="btn btn-primary btn-sm" onclick="getEditData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Edit
                                                    </button>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="multiDelete[]" value="{{ $pelamars->id_lowongan_kerja }}" id="multiDelete" >
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        @foreach ($pelamar as $pelamars)
                                            <tr>
                                                <td>{{ $b++ }}</td>
                                                <td>{{ $pelamars->jabatan_lowongan }}</td>
                                                <td>{{ $pelamars->kategori_pekerjaan }}</td>
                                                <td>{{ implode(', ',is_array(json_decode($pelamars->lokasi_kerja)) ? json_decode($pelamars->lokasi_kerja) : ['']) }}</td>
                                                <td>{{ $pelamars->pengalaman_kerja }}</td>
                                                <td>{{ $pelamars->tingkat_pekerjaan }}</td>
                                                <td>
                                                    <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                    <!-- <a href="{{ URL::to('/list_loker/detail/'.$pelamars->id_lowongan_kerja) }}" class="btn btn-secondary btn-sm">Detail</a> -->
                                                    <button type="button" class="btn btn-secondary btn-sm" onclick="getDetailData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Detail
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-sm" onclick="getEditData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Edit
                                                    </button>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="multiDelete[]" value="{{ $pelamars->id_lowongan_kerja }}" id="multiDelete" >
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                        {{-- <tr> --}}
                                            {{-- <td style=""></td>
                                            @if(isset($data) && isset($labels) && count($labels) > 0)
                                                @foreach($labels as $idx => $label)
                                                    @if(in_array(strtolower(str_replace(" ", "_", $label)), $data->filter))
                                                        <td style="">{{ $label }}</td>
                                                    @endif
                                                @endforeach
                                            @else
                                                <td style="">s</td>
                                                <td style="">d</td>
                                                <td style="">d</td>
                                                <td style="">d</td>
                                                <td style="">d</td>
                                                <td style=""></td>
                                            @endif
                                    
                                            <td>
                                                <!-- <input type="checkbox" name="multiDelete[]" value="">&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                <!-- <a href="{{ URL::to('/list_lo/detail/') }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->

                                                <button type="button" class="btn btn-secondary btn-sm" onclick="getDetailData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Detail
                                                    </button>
                                                <button type="button" class="btn btn-primary btn-sm" onclick="getEditData('{{$pelamars->id_lowongan_kerja}}')">
                                                        Edit
                                                </button>
                                            </td>
                                            <td>
                                            <input type="checkbox" name="multiDelete[]" value="" id="multiDelete" >
                                            </td> --}}
                                        {{-- </tr> --}}
                                </tbody>
                            </table>
                        
                    </div>

                    <table>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop" style="font-size:11px;border-radius:5px;">Tambah</button>
                            <td>
                                <!-- <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button> -->
                                <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#0085fc;">
        <b class="modal-title" id="staticBackdropLabel">Tambah Data Lowongan Kerja</b>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            <form action="{{route('simpan_lo')}}" method="post" enctype="multipart/form-data">
                
                {{ csrf_field() }}
                <div class="form-group" style="width:95%;">
                    <div class="">
                        <!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
                            <p>Tambah Data CV Pelamar</p>
                        </div> -->
                        
                        <div class="row">
                            <div class="col" style="font-size: 10px;">
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor FPTK</label>
                                    <!-- <div class="col-sm-9">
                                    <input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                    </div> -->
                                    <div class="col-sm-9">
                                        <select name="nomer_dokumen_fptk[]"  id="nomer_dokumen_fptk" multiple="multiple" class="select2-multiple-fptk form-control form-control-sm" style="font-size:11px; width:100%; " onchange="onChangeFptk()">
                                            @foreach ($ptk_detail as $ll )
                                                <option 
                                                    data-jabatan="{{ $ll->jabatan_kepegawaian}}"
                                                    data-kategori=""
                                                    data-tingkat="{{ $ll->tingkat_kepegawaian}}"
                                                    data-lokasi_kerja="{{ $ll->lokasi_kerja}}"
                                                    data-pendidikan="{{ $ll->pendidikan}}"
                                                    data-status="{{ $ll->status_kepegawaian}}"
                                                    data-pengalaman="{{ $ll->pengalaman}}"
                                                    data-jumlah="{{ $ll->jumlah_kebutuhan}}"
                                                    value="{{ $ll->no_dokumen }}"
                                                >{{ $ll->no_dokumen }}</option>
                                            @endforeach
                                        </select>
                                        <!-- <input type="text"  name="nomer_dokumen_fptk" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kategori Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
                                        <select name="kategori_pekerjaan" required id="kategori_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
                                            <option value="" selected disabled>--Tingkat--</option>
                                            <option value="it" >IT</option>
                                        </select>	
                                    </div>
                            
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
                                    <div class="col-sm-9">
                                        <input type="text" required id="jabatan_lowongan" name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" name="lokasi_add" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Ketik disini untuk menambahkan lokasi" onblur="addLokasiKerja(this);" onkeyup="if(event.key==='Enter') addLokasiKerja(this);"> -->
                                        <select name="lokasi_kerja[]"  id="lokasi_kerja_form" multiple="multiple" class="select22-multiple form-control form-control-sm" style="font-size:11px; width:100%; ">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">tingkat</label>
                                    <div class="col-sm-9">
                                        <select name="tingkat_kerjaan" id="tingkat_kerjaan" required class="form-control form-control-sm" style="font-size:11px;">
                                            <option value="staff" >Staff</option>
                                            <option value="middle" >Middle</option>
                                            <option value="senior">Senior</option>
                                        </select>	
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
                                    <div class="col-sm-9">
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="pengalaman_kerja" id="pengalaman_kerja_lu"  value="Lulusan Baru" >
                                            <label class="form-check-label" for="pengalaman_kerja_lu">
                                                Berpengalaman/Lulusan Baru
                                            </label>
                                        </div>&nbsp;&nbsp;&nbsp;
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="pengalaman_kerja" id="pengalaman_kerja_me" value="Memiliki Pengalaman">
                                            <label class="form-check-label" for="pengalaman_kerja_me">
                                                Berpengalaman
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kepegawaian</label>
                                        
                                    <div class="col-sm-9">
                                        <select name="status_kepegawaian" required id="status_kepegawaian" class="form-control form-control-sm" style="font-size:11px;">
                                            <option value="Kontrak">Kontrak</option>
                                            <option value="Tetap">Tetap</option>
                                        </select>		
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Minimal Pendidikan</label>
                                    <div class="col-sm-9">
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="minimal_pendidikan" id="minimal_pendidikan_smk" value="SMK/Sederajat" >
                                            <label class="form-check-label" for="minimal_pendidikan_smk">
                                                SMK/SMA
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="minimal_pendidikan" id="minimal_pendidikan_d3" value="D3">
                                            <label class="form-check-label" for="minimal_pendidikan_d3">
                                                D3
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="minimal_pendidikan" id="minimal_pendidikan_s1" value="S1">
                                            <label class="form-check-label" for="minimal_pendidikan_s1">
                                                S1
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="minimal_pendidikan" id="minimal_pendidikan_s2" value="S2">
                                            <label class="form-check-label" for="minimal_pendidikan_s2">
                                                S2
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" required type="radio" name="minimal_pendidikan" id="minimal_pendidikan_s3" value="S3">
                                            <label class="form-check-label" for="minimal_pendidikan_s3">
                                                S3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tentang Peran Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <textarea name="peran_kerja"  id="summary-ckeditor" cols="30" rows="10" class="form-control form-control-sm" placeholder="Tentang Peran Pekerjaan"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan</label>
                                    <div class="col-sm-9">
                                        <textarea name="deskripsi_pekerjaan" required id="summary-ckeditor-2" cols="30" rows="10" class="form-control form-control-sm" placeholder="Deskripsi Pekerjaan"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Pengalaman</label>
                                    <div class="col-sm-9">
                                        <textarea name="syarat_pengalaman" required id="summary-ckeditor-3" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Pengalaman"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Kemampuan</label>
                                    <div class="col-sm-9">
                                        <textarea name="syarat_kemampuan" required id="summary-ckeditor-4" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Kemampuan"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Lainnya</label>
                                    <div class="col-sm-9">
                                        <textarea name="syarat_lainnya" id="summary-ckeditor-5" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Lainnya"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <hr>
                            
                            <div class="row">
                                <div class="col" style="font-size: 10px;">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                                        <div class="col-sm-9">
                                            <input type="number" required name="jumlah_karyawan" style="font-size:11px;" value=""  class="form-control form-control-sm" id="jumlah_karyawan" placeholder=""/>
                                        </div>
                                    </div>
                                
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Upload</label>
                                        <div class="col-sm-9">
                                        <input type="date" required name="tanggal_upload" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Lowongan</label>
                                        <div class="col-sm-9">
                                            <div class="col-sm-9">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" required name="status_lowongan" id="status_lowongbuka" value="buka">
                                                    <label class="form-check-label" for="status_lowongbuka">
                                                        Buka
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" required name="status_lowongan" id="status_lowongsele" value="selesai">
                                                    <label class="form-check-label" for="status_lowongsele">
                                                        Selesai
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div style="width: 100%;">
                    <table>
                    <tr>
                        <!-- <td>
                            <input type="text">
                        </td> -->
                        <td>
                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                        </td>
                        <td>
                            <a href="{{ route('list_loker') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                        </td>
                    </tr>
                    </table>
                </div>
                <br>
            </form>
      </div>
      
    </div>
  </div>
  
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="formEdit">
        ..
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="formDetail">
        ...
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="{{ URL::asset('assets/js/select2.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush

@section('add-scripts')

<script>
    $(document).ready(function() {
        // Select2 Multiple
        $('.select2-multiple-fptk').select2({
            placeholder: "No.Dokumen FPTK",
            allowClear: true,
            dropdownParent: $("#staticBackdrop")
        });
        $('.select22-multiple').select2({
            placeholder: "Lokasi Kerja",
            allowClear: true,
            dropdownParent: $("#staticBackdrop")
        });
    });
</script>


<script>
     function onChangeFptkEdit() {
        var checkSelection = 0;
        $('#formLokerEdit #nomer_dokumen_fptk option').each(function() {
            if (this.selected) checkSelection++;
        });

        var selectedOption = $('#formLokerEdit #nomer_dokumen_fptk').find('option:selected');
        

        $('#formLokerEdit #jabatan_lowongan').children().remove();
        $('#formLokerEdit #nomer_dokumen_fptk option:selected').each(function(){
            var jabatan = $(this).data('jabatan');
            $('#formLokerEdit #jabatan_lowongan')
            .append($("<option></option>")
                .attr("value", jabatan)
                .text(jabatan));
        });

        $('#formLokerEdit #nomer_dokumen_fptk option:selected').each(function(){
            var jabatan = $(this).data('jabatan');
            $('#formLokerEdit #jabatan_lowongan')
            .append($("<option></option>")
                .attr("value", jabatan)
                .text(jabatan));
        });




        if(checkSelection == 1){
            $('#formLokerEdit #jabatan_lowongan').val(selectedOption.data('jabatan')).trigger('change');
            $('#formLokerEdit #lokasi_kerja_form').children().remove();
            $('#formLokerEdit #lokasi_kerja_form')
                .append($("<option selected></option>")
                    .attr("value", selectedOption.data('lokasi_kerja'))
                    .text(selectedOption.data('lokasi_kerja')));

            $('#formLokerEdit #tingkat_kerjaan').val(selectedOption.data('tingkat')).trigger('change');
            $('#formLokerEdit #jumlah_karyawan').val(selectedOption.data('jumlah')).trigger('change');
            
            
            
            $('#formLokerEdit #kategori_pekerjaan').val(selectedOption.data('kategori')).trigger('change');
            
            if(selectedOption.data('status') == 'kontrak selama')
                $('#formLokerEdit #status_kepegawaian').val('Kontrak').trigger('change');
            else
                $('#formLokerEdit #status_kepegawaian').val('Tetap').trigger('change');

            $('#formLokerEdit input:radio[value="SMK/Sederajat"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="D1"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="D2"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="D3"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="S1"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="S2"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="S3"]').removeAttr('checked');

            if(selectedOption.data('pendidikan') == 'SMU/Sederajat')
                $('#formLokerEdit input:radio[value="SMK/Sederajat"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D1')
                $('#formLokerEdit input:radio[value="D1"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D2')
                $('#formLokerEdit input:radio[value="D2"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D3')
                $('#formLokerEdit input:radio[value="D3"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S1')
                $('#formLokerEdit input:radio[value="S1"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S2')
                $('#formLokerEdit input:radio[value="S2"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S3')
                $('#formLokerEdit input:radio[value="S3"]').prop('checked', true);

            $('#formLokerEdit input:radio[value="Memiliki Pengalaman"]').removeAttr('checked');
            $('#formLokerEdit input:radio[value="Lulusan Baru"]').removeAttr('checked');

            if(selectedOption.data('pengalaman') != '')
                $('#formLokerEdit input:radio[value="Memiliki Pengalaman"]').prop('checked', true);
            else
                $('#formLokerEdit input:radio[value="Lulusan Baru"]').prop('checked', true);
        }else if (checkSelection > 1) {
            var totalJumlahKaryawan = 0;
            
            $('#nomer_dokumen_fptk option:selected').each(function() {
                totalJumlahKaryawan += parseInt($(this).data('jumlah') || 0);
            });

            $('#jumlah_karyawan').val(totalJumlahKaryawan).trigger('change');
        }
    }

    function onChangeFptk() {
        var checkSelection = 0;
        $('#nomer_dokumen_fptk option').each(function() {
            if (this.selected) checkSelection++;
        });
        console.log(checkSelection)
        var selectedOption = $('#nomer_dokumen_fptk').find('option:selected');
        if(checkSelection == 1){
            $('#jabatan_lowongan').val(selectedOption.data('jabatan'));
            $('#formLokerEdit #syarat_pengalaman').val(selectedOption.data('syarat_pe'));
            $('#lokasi_kerja_form').children().remove();
            $('#lokasi_kerja_form')
                .append($("<option selected></option>")
                    .attr("value", selectedOption.data('lokasi_kerja'))
                    .text(selectedOption.data('lokasi_kerja')));

            $('#tingkat_kerjaan').val(selectedOption.data('tingkat')).trigger('change');
            $('#jumlah_karyawan').val(selectedOption.data('jumlah')).trigger('change');
            
            $('#kategori_pekerjaan').val(selectedOption.data('kategori')).trigger('change');
            
            if(selectedOption.data('status') == 'kontrak selama')
                $('#status_kepegawaian').val('Kontrak').trigger('change');
            else
                $('#status_kepegawaian').val('Tetap').trigger('change');

            $('input:radio[value="SMK/Sederajat"]').removeAttr('checked');
            $('input:radio[value="D1"]').removeAttr('checked');
            $('input:radio[value="D2"]').removeAttr('checked');
            $('input:radio[value="D3"]').removeAttr('checked');
            $('input:radio[value="S1"]').removeAttr('checked');
            $('input:radio[value="S2"]').removeAttr('checked');
            $('input:radio[value="S3"]').removeAttr('checked');

            if(selectedOption.data('pendidikan') == 'SMU/Sederajat')
                $('input:radio[value="SMK/Sederajat"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D1')
                $('input:radio[value="D1"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D2')
                $('input:radio[value="D2"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'D3')
                $('input:radio[value="D3"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S1')
                $('input:radio[value="S1"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S2')
                $('input:radio[value="S2"]').prop('checked', true);
            else if(selectedOption.data('pendidikan') == 'S3')
                $('input:radio[value="S3"]').prop('checked', true);

            $('input:radio[value="Memiliki Pengalaman"]').removeAttr('checked');
            $('input:radio[value="Lulusan Baru"]').removeAttr('checked');

            if(selectedOption.data('pengalaman') != '')
                $('input:radio[value="Memiliki Pengalaman"]').prop('checked', true);
            else
                $('input:radio[value="Lulusan Baru"]').prop('checked', true);
        } else if (checkSelection > 1) {
            var totalJumlahKaryawan = 0;
            
            $('#nomer_dokumen_fptk option:selected').each(function() {
                totalJumlahKaryawan += parseInt($(this).data('jumlah') || 0);
            });

            $('#jumlah_karyawan').val(totalJumlahKaryawan).trigger('change');
        }
    }

    function addLokasiKerja(lokasi) {
        if(lokasi.value != '')
            $('#lokasi_kerja_form')
                .append($("<option selected></option>")
                    .attr("value", lokasi.value)
                    .text(lokasi.value));     
        lokasi.value = '';   
    }


    function getEditData(id) {
        let url = "{{ URL::to('/list_loker/edit_data/:id') }}";
        url = url.replace(":id", id);
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function(data){
                console.log(data.data);
                $('#formEdit').html(data.data);
                $('#editModal').modal('show');
                $('.multiple-lokasi-edit').select2({
                    placeholder: "Lokasi Kerja",
                    allowClear: true,
                    dropdownParent: $("#editModal")
                });
                $('.multiple-fptk-edit').select2({
                    placeholder: "Nomer Ddokumen FPTK",
                    allowClear: true,
                    dropdownParent: $("#editModal")
                });

                $('#formLokerEdit #nomer_dokumen_fptk option:selected').each(function(){
                    var jabatan = $(this).data('jabatan');
                    $('#formLokerEdit #jabatan_lowongan')
                    .append($("<option></option>")
                        .attr("value", jabatan)
                        .text(jabatan));
                });
                $('#formLokerEdit #jabatan_lowongan').val($('#jabatan_selected').val()).trigger('change');
            }
        });
    }


    
</script>
<script>

function getDetailData(id) {
    let url = "{{ URL::to('/list_loker/detail_data/:id') }}";
        url = url.replace(":id", id);
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function(data){
                console.log(data.data);
                $('#formEdit').html(data.data);
                $('#editModal').modal('show');
                $('.multiple-lokasi-edit').select2({
                    placeholder: "Lokasi Kerja",
                    allowClear: true,
                    dropdownParent: $("#editModal")
                });
                $('.multiple-fptk-edit').select2({
                    placeholder: "Nomer Ddokumen FPTK",
                    allowClear: true,
                    dropdownParent: $("#editModal")
                });

                $('#formLokerEdit #nomer_dokumen_fptk option:selected').each(function(){
                    var jabatan = $(this).data('jabatan');
                    $('#formLokerEdit #jabatan_lowongan')
                    .append($("<option></option>")
                        .attr("value", jabatan)
                        .text(jabatan));
                });
                $('#formLokerEdit #jabatan_lowongan').val($('#jabatan_selected').val()).trigger('change');
            }
        });
}
</script>
<script>
    function deleteItem(linkHref) {
    Swal.fire({
        title: '<h4>Anda yakin ingin menghapus?</h4>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",     
        confirmButtonText: "<i>Ya, hapus!</i>",
        // buttonsStyling: false
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: linkHref,
                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil!",
                            text: "Data telah terhapus.",
                        }).then((result) => {
                            if (result.value) {
                                location.href = "{{ URL::to('list_loker') }}";
                            }
                        });
                    }
                },
                error: function(e) {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: "Data tidak terhapus.",
                    });
                },
            });
        }
    })
}
</script>

<script>
    $('#form-filter-kolom').on('submit', function(e) {
            e.preventDefault()

            var actionurl = "{{ route('filter_lo') }}"
            console.log(actionurl)
            Swal.fire({
                title: '<h4>Anda mengganti filter table?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, filter!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $(this).serialize(),
                        success: function(res) {
                            if (res.success) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah difilter.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        })
</script>
<script>
    $(function() {
        
        //hang on event of form with id=myform
        console.log("masuk");
        $("#example").DataTable();

        $("#form_delete").submit(function(e) {
            e.preventDefault();
            var actionurl = e.currentTarget.action;
            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      
                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: actionurl,
                        type: 'post',
                        dataType: 'JSON',
                        cache: false,
                        data: $("#form_delete").serialize(),
                        success: function(res) {
                            if (res.status) {                                
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data berhasil dihapus.",
                                });
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Terjadi kesalahan saat menghapus data.",
                            });
                        },
                        complete: function(jqXhr, msg) {
                            setTimeout(() => {
                                location.reload();
                            }, 800);
                            // console.log(jqXhr, msg);
                        }
                    });
                }
            })
        });
    });
</script>

@endsection
