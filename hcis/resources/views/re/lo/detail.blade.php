
@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header" style="text-align: center;">
		<div class="row">
			<div class="col">Detail Data CV Pelamar</div>
			<div class="col">
				Kode CV Pelamar:
			</div>
		</div>
	</div>
    <div class="card-body">
		<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
			
			{{ csrf_field() }}
			@foreach($rc_cv as $data)
			<input type="hidden" name="temp_id" id="temp_id" value="{{ $data->id_cv_pelamar }}" />

			<div class="form-group" style="width:95%;">
				<div class="">
					<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
						<p>Tambah Data CV Pelamar</p>
					</div> -->
					<b>Melamar Lowongan</b>
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<table>
								<div class="form-group row">
									<tr>
										<td>{{$data->jabatan_lowongan}}</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Tanggal Melamar</td>
										<td>:</td>
										<td>{{$data->tanggal_melamar}}</td>
									</tr>
									<tr>
										<td>Tingkat</td>
										<td>:</td>
										<td>{{$data->tingkat_pekerjaan}}</td>
									</tr>
									<tr>
										<td>Lokasi Kerja</td>
										<td>:</td>
										<td>{{$data->lokasi_kerja}}</td>
									</tr>                                               
								</div>
							</table>
						</div>
					</div>
						<hr>
						<b>Personal Information</b>
						<br><br>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
									<div class="col-sm-9">
										<a href="{{asset('data_file/'.$data->resume_cv)}}" class="btn btn-outline-success btn-sm" download> Download</a>
									</div>
								</div>
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
									<div class="col-sm-9">
									<input type="nama_lengkap" readonly required name="nama_lengkap" value="{{$data->nama_lengkap}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
									<div class="col-sm-9">
									<input type="tanggal_lahir" readonly required name="tanggal_lahir" value="{{$data->tanggal_lahir}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
									<div class="col-sm-9">
									<input type="lokasi_pelamar" readonly required name="lokasi_pelamar" value="{{$data->lokasi_pelamar}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
									<div class="col-sm-9">
										<div class="form-check">
											<input class="form-check-input" type="radio" name="pengalaman_kerja" disabled value="Berpengalaman" readonly {{ $data->pengalaman_kerja == 'Berpengalaman' ? 'checked' : NULL }} >
											<label class="form-check-label" for="flexRadioDefault1">
												Memiliki Pengalaman
											</label>
										</div>&nbsp;&nbsp;&nbsp;
										<div class="form-check">
											<input class="form-check-input" type="radio" name="pengalaman_kerja" disabled value="Lulusan Baru" readonly {{ $data->pengalaman_kerja == 'Lulusan Baru' ? 'checked' : NULL }}>
											<label class="form-check-label" for="flexRadioDefault1">
												Lulusan baru
											</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
									<div class="col-sm-4">
									<input type="text" readonly required name="tahun_pengalaman_kerja" style="font-size:11px;" value="{{$data->tahun_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="">Tahun
									</div>
									<div class="col-sm-5">
									<input type="text" readonly required name="bulan_pengalaman_kerja" style="font-size:11px;" value="{{$data->bulan_pengalaman_kerja}}"  class="form-control form-control-sm" placeholder="" >Bulan
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
									<div class="col-sm-9">
									<input type="email" readonly required name="email" value="{{$data->email}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<b>Pendidikan</b>
						<div class="row">
							<div class="col">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
									<div class="col-sm-9">
										<input type="text" readonly required name="text" value="{{$data->pendidikan_tertinggi}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
									<div class="col-sm-9">
										<input type="text" readonly required name="text" value="{{$data->nama_sekolah}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
									<div class="col-sm-9">
										<input type="text" readonly required name="program_studi_jurusan" value="{{$data->program_studi_jurusan}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/Tanggal Perkiraan Wisuda</label>
									<div class="col-sm-9">
										<input type="text" readonly required name="tanggal_wisuda" value="{{$data->tanggal_wisuda}}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
									<div class="col-sm-4">
										<input type="text" value="{{$data->nilai_rata_rata}}" readonly name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" data-inputmask="'mask': '9999 9999 9999 9999'" class="form-control form-control-sm" placeholder="">
									</div>Skala*
									<div class="col-sm-4">
									<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
										<input type="text" value="{{$data->nilai_skala}}" readonly name="nilai_skala" style="font-size:11px;" id="nilai_skala" data-inputmask="'mask': '9999 9999 9999 9999'" class="form-control form-control-sm" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<b>Informasi Tambahan</b>
						<div class="row">
							<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
							<div class="col-sm-4">
								<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" disabled style="font-size:11px;">
									<option value="Web SRU" {{$data->keterangan_informasi_pilihan='Web SRU' ? 'selected' : NULL }} >Web SRU</option>
									<option value="Job Street" {{$data->keterangan_informasi_pilihan='Job Street' ? 'selected' : NULL }}>Job Street</option>
									<option value="Karyawan SRU" {{$data->keterangan_informasi_pilihan='Karyawan SRU' ? 'selected' : NULL }}>Karyawan SRU</option>
									<option value="Lainnya" {{$data->keterangan_informasi_pilihan='Lainnya' ? 'selected' : NULL }}>Lainnya</option>
								</select>	
							</div>
							<div class="col-sm-4">
								<input type="text" readonly required value="{{$data->keterangan_informasi_pilihan}}" name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
							</div>
						</div><br>
						<div class="row">
							<div class="col-sm-12 form-floating">
								<textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" value="{{$data->pesan_pelamar}}" style="height: 100px">{{$data->pesan_pelamar}}</textarea>
							</div>
						</div>
				</div>
				<br>
				<b>Penilaian HRD</b>
				<div class="row">
					<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Lamaran</label>
					<div class="col-sm-4">
						<select name="keterangan_informasi_pilihan" id="tingkat" class="form-control form-control-sm" disabled style="font-size:11px;">
							<option value="Belum Diperiksa" {{$data->status_lamaran='Web SRU' ? 'selected' : NULL }} >Web SRU</option>
							<option value="Sedang Dipertimbangkan" {{$data->status_lamaran='Job Street' ? 'selected' : NULL }}>Sedang Dipertimbangkan</option>
							<option value="Lulus" {{$data->status_lamaran='Lulus' ? 'selected' : NULL }}>Lulus</option>
						</select>	
					</div>
					
				</div><br>
			</div>
			<hr>
			<div style="width: 100%;">
				<table>
				<tr>
					<td>
						<a href="{{ URL::to('/list_dcp/edit/'.$data->id_cv_pelamar) }}" class="btn btn-success btn-sm" style="border:none;border-radius:5px;font-size:11px;">Edit</a>
					</td>
					<td>
						<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
					</td>
					<td>
						<a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;border:none;" href="{{route('hapus_dcp', $data->id_cv_pelamar)}}"  id="btn_delete">Hapus</a>
					</td>
				</tr>
				</table>
			</div>
			<br>
			@endforeach
		</form>
	</div>
</div>
@endsection
@section('add-scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>

<script>
$(document).ready(function() {
	// Select2 Multiple
  $('.select2-multiple').select2({
	  placeholder: "Lokasi Kerja",
		allowClear: true
	});

}); 
</script>


<script>
	function informasi(that) {
		var tahun_h = document.getElementById("tahun_hid").value;
		var bulan_h = document.getElementById("bulan_hid").value;
		if (that.value == "Berpengalaman") {
				document.getElementById("tahun").value =tahun_h;
				document.getElementById("bulan").value =bulan_h;
				document.getElementById("tahun").readOnly = false;
				document.getElementById("bulan").readOnly = false;
			} else {
				document.getElementById("tahun").value ='';
				document.getElementById("bulan").value ='';
				document.getElementById("tahun").readOnly = true;
				document.getElementById("bulan").readOnly = true;
			}
		}
</script>




<script>
	function informasi(that) {
    if ((that.value == "Lainnya") || (that.value == "Karyawan SRU")){
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
	}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",      

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dcp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>
@endsection