
@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitmen @endslot
@slot('title') Recruitmen @endslot
@endcomponent

<div class="card">
    <div class="card-header">
		<h4>Detail Data CV Pelamar</h4>
	</div>
    <div class="card-body">
		<form action="{{route('update_lo')}}" method="post" enctype="multipart/form-data">
		
			{{ csrf_field() }}
			@foreach($list_loker as $data)
			<div class="form-group" style="width:95%;">
				<div class="">
					<input type="hidden" id="tg_id" name="id_lowongan_kerja"  value="{{$data->id_lowongan_kerja}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
					<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
						<p>Tambah Data CV Pelamar</p>
					</div> -->
					<div class="row">
						<div class="col" style="font-size: 10px;">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor FPTK</label>
								<!-- <div class="col-sm-9">
								<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div> -->
								<div class="col-sm-9">
									<input type="text"  name="nomer_dokumen_fptk"  value="{{$data->nomer_dokumen_fptk}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
									
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kategori Pekerjaan</label>
								<div class="col-sm-9">
									<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
									<select name="kategori_pekerjaan" required id="kategori_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
										<option value="" selected >--Tingkat--</option>
										<option value="it" {{ $data->kategori_pekerjaan == 'it' ? 'selected' : NULL }}>Staff</option>
										<option value="manager" {{ $data->kategori_pekerjaan == 'manager' ? 'selected' : NULL }}>Middle</option>
										<option value="senior" {{ $data->kategori_pekerjaan == 'senior' ? 'selected' : NULL }}>Senior</option>
									</select>	
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
								<div class="col-sm-9">
									<input type="text" required  name="jabatan_lowongan" value="{{$data->jabatan_lowongan}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
								<div class="col-sm-9">
									<input type="text" required  name="lokasi_kerja" value="{{$data->lokasi_kerja}}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat</label>
								<div class="col-sm-9">
									<select name="tingkat_kerjaan" required id="tingkat_kerjaan" class="form-control form-control-sm" style="font-size:11px;">
										<option value="staff" {{ $data->tingkat_pekerjaan== 'staff' ? 'selected' : NULL }}>Staff</option>
										<option value="middle" {{ $data->tingkat_pekerjaan== 'middle' ? 'selected' : NULL }}>Middle</option>
										<option value="senior" {{ $data->tingkat_pekerjaan== 'senior' ? 'selected' : NULL }}>Senior</option>
									</select>	
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
								<div class="col-sm-9">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="pengalaman_kerja" required  value="Lulusan Baru" {{ $data->pengalaman_kerja == 'Lulusan Baru' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											Berpengalaman/Lulusan Baru
										</label>
									</div>&nbsp;&nbsp;&nbsp;
									<div class="form-check">
										<input class="form-check-input" type="radio" name="pengalaman_kerja" required value="Memiliki Pengalaman" {{ $data->pengalaman_kerja == 'Memiliki Pengalaman' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											Berpengalaman
										</label>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kepegawaian</label>
								<div class="col-sm-9">
									<select name="status_kepegawaian" required id="status_kepegawaian"  class="form-control form-control-sm" style="font-size:11px;">
										<option value="Kontrak" {{ $data->status_kepegawaian == 'Kontrak' ? 'selected' : NULL }}>Kontrak</option>
										<option value="Tetap" {{ $data->status_kepegawaian == 'Tetap' ? 'selected' : NULL }}>Tetap</option>
									</select>		
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Minimal Pendidikan</label>
								<div class="col-sm-9">
									<div class="form-check">
										<input class="form-check-input"  type="radio" required name="minimal_pendidikan"  value="SMK/Sederajat" {{ $data->minimal_pendidikan == 'SMK/Sederajat' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											SMK/SMA
										</label>
									</div>&nbsp;&nbsp;&nbsp;
									<div class="form-check">
										<input class="form-check-input"  type="radio" name="minimal_pendidikan" value="D3" {{ $data->minimal_pendidikan == 'D3' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											D3
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input"  type="radio" name="minimal_pendidikan" value="S1" {{ $data->minimal_pendidikan == 'S1' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											S1
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input"  type="radio" name="minimal_pendidikan" value="S2" {{ $data->minimal_pendidikan == 'S2' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											S2
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input"  type="radio" name="minimal_pendidikan" value="S3" {{ $data->minimal_pendidikan == 'S3' ? 'checked' : NULL }}>
										<label class="form-check-label" for="flexRadioDefault1">
											S3
										</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tentang Peran Pekerjaan</label>
								<div class="col-sm-9">
									<textarea name="peran_kerja"  id="summary-ckeditor-2" cols="30" rows="10" class="form-control form-control-sm" value="{{$data->peran_kerja}}" placeholder="">{{$data->peran_kerja}}</textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan</label>
								<div class="col-sm-9">
									<textarea name="deskripsi_pekerjaan" required id="summary-ckeditor-2" cols="30" rows="10" class="form-control form-control-sm" placeholder="Deskripsi Pekerjaan" value="{{$data->deskripsi_pekerjaan}}">{{$data->deskripsi_pekerjaan}}</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Pengalaman</label>
								<div class="col-sm-9">
									<textarea name="syarat_pengalaman" required id="summary-ckeditor-3" cols="30" rows="10" class="form-control form-control-sm" value="{{$data->syarat_pengalaman}}" placeholder="Syarat Pengalaman">{{$data->syarat_pengalaman}}</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Kemampuan</label>
								<div class="col-sm-9">
									<textarea name="syarat_kemampuan" required  id="summary-ckeditor-4" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Kemampuan" value="{{$data->syarat_kemampuan}}">{{$data->syarat_kemampuan}}</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Lainnya</label>
								<div class="col-sm-9">
									<textarea name="syarat_lainnya"  id="summary-ckeditor-5" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Lainnya">{{$data->syarat_lainnya}}</textarea>
								</div>
							</div>
						</div>
					</div>
						<hr>
						
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
									<div class="col-sm-9">
										<input type="number" required name="jumlah_karyawan" style="font-size:11px;" value="{{$data->jumlah_karyawan}}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
									</div>
								</div>
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Upload</label>
									<div class="col-sm-9">
									<input type="date" required name="tanggal_upload" style="font-size:11px;" value="{{$data->tanggal_upload}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Lowongan</label>
									<div class="col-sm-9">
										<div class="col-sm-9">
											<div class="form-check">
												<input class="form-check-input" required type="radio" name="status_lowongan" value="buka" {{ $data->status_lowongan == 'buka' ? 'checked' : NULL }}>
												<label class="form-check-label" for="flexRadioDefault1">
													Buka
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" required type="radio" name="status_lowongan" value="selesai" {{ $data->status_lowongan == 'selesai' ? 'checked' : NULL }}>
												<label class="form-check-label" for="flexRadioDefault1">
													Selesai
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div style="width: 100%;">
				<table>
				<tr>
					<!-- <td>
						<input type="text">
					</td> -->
					<td>
						<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
					</td>
					<td>
						<a href="{{ route('hapus_lo',$data->id_lowongan_kerja) }}" id="btn_delete" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
					</td>
					<td>
						<a href="{{ route('list_loker') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
					</td>
				</tr>
				</table>
			</div>
			@endforeach
			<br>
		</form>
	</div>
</div>
@endsection
@section('add-scripts')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
	CKEDITOR.replace( 'summary-ckeditor');
	CKEDITOR.replace( 'summary-ckeditor-2');
	CKEDITOR.replace( 'summary-ckeditor-3');
	CKEDITOR.replace( 'summary-ckeditor-4');
	CKEDITOR.replace( 'summary-ckeditor-5');

</script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endsection

@section('add-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn_delete").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var linkHref = link.attr('href');
                console.log(linkHref);

                Swal.fire({
                    title: '<h4>Anda yakin ingin menghapus?</h4>',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: "Batal",      

                    confirmButtonText: "<i>Ya, hapus!</i>",
                    // buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: linkHref,
                            type: 'GET',
                            dataType: 'JSON',
                            cache: false,
                            success: function(res) {
                                if (res.status) {
                                    Swal.fire({
                                        icon: "success",
                                        title: "Berhasil!",
                                        text: "Data telah terhapus.",
                                    }).then((result) => {
                                        if (result.value) {
                                            location.href = "{{ URL::to('list_loker') }}";
                                        }
                                    });
                                }
                            },
                            error: function(e) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Gagal!",
                                    text: "Data tidak terhapus.",
                                });
                            },
                        });
                    }
                })
            });
        });
    </script>
@endsection

