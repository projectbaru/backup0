@extends('re.transaksi_ptk.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Tambah Data CV Pelamar</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Data CV Pelamar</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							<div class="form-group" style="width:95%;">
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
									<b>Personal Information</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Resume/CV</label>
												<div class="col-sm-9">
												<input type="file" id="file_cv" download name="resume_cv" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lengkap</label>
												<div class="col-sm-9">
													<input type="text" required name="nama_lengkap" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
													
												</div>
										
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
												<div class="col-sm-9">
                                                    <input type="email" required name="email" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
                                            <div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">No. HP</label>
												<div class="col-sm-9">
                                                    <input type="number" required name="no_hp" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
                                            <div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
												<div class="col-sm-9">
                                                    <input type="text" required name="lokasi_pelamar" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
                                            <!-- <div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Saat Ini</label>
												<div class="col-sm-9">
                                                    <input type="text" required name="pengalaman_kerja" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div> -->
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="pengalaman_kerja"  value="Berpengalaman" >
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            Memiliki Pengalaman
                                                        </label>
                                                    </div>&nbsp;&nbsp;&nbsp;
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="pengalaman_kerja" value="Lulusan Baru" >
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            Lulusan baru
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lama Pengalaman Kerja</label>
                                                <div class="col-sm-4">
                                                    <input type="text" required name="tahun_pengalaman_kerja" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >Tahun
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="text" required name="bulan_pengalaman_kerja" style="font-size:11px;"  class="form-control form-control-sm" placeholder="" >Bulan
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Lahir</label>
                                                <div class="col-sm-9">
                                                    <input type="date" required name="tanggal_lahir" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jenis Kelamin</label>
                                                <!-- <div class="col-sm-4">
                                                    <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Laki-laki
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="radio" required name="jenis_kelamin" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">Perempuan
                                                </div> -->
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="jenis_kelamin" type="radio" value="Laki-laki" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            Laki-laki
                                                        </label>
                                                    </div>&nbsp;&nbsp;&nbsp;
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="jenis_kelamin" type="radio" value="perempuan" name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            Perempuan
                                                        </label>
                                                    </div>
                                                </div>
                                                
												</div>
										</div>
									</div>
										<hr>
										<b>Pendidikan</b>
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan Tertinggi</label>
													<div class="col-sm-9">
														<select name="pendidikan_tertinggi" id="pendidikan_tertinggi" class="git form-control form-control-sm" style="font-size:11px;">
															<option value="" selected disabled>--Silahkan Pilih--</option>
															<option value="SMK/Sederajat">SMK/Sederajat</option>
															<option value="D1">D1</option>
															<option value="D2">D2</option>
															<option value="D3">D3</option>
															<option value="S1">S1</option>
															<option value="S2">S2</option>
															<option value="S3">S3</option>
														</select>	
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Sekolah</label>
													<div class="col-sm-9">
													<input type="text" required name="nama_sekolah" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Program Studi/Jurusan</label>
													<div class="col-sm-9">
													<input type="text" name="program_studi_jurusan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
                                                <div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Wisuda/Tanggal Perkiraan Wisuda</label>
													<div class="col-sm-9">
													    <input type="date" name="tanggal_wisuda" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label>
													<div class="col-sm-4">
														<input type="input" required name="nilai_rata_rata" id="nilai_rata" style="font-size:11px;" class="form-control form-control-sm" placeholder="Angka">
													</div>Skala*
													<div class="col-sm-4">
													<!-- <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nilai Rata-Rata</label> -->
														<input type="text" required name="nilai_skala" style="font-size:11px;" id="nilai_skala" class="form-control form-control-sm" placeholder="Mis. 0-4">
													</div>
												</div>
											</div>
										</div>
									<hr>
									<b>Informasi Tambahan</b>
									<br>								
									<hr>
							
									<div class="row">
										<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Darimana Anda tahu tentang lowongan ini ?</label>
										<div class="col-sm-4">
											<select onchange="informasi(this);" name="informasi_lowongan" id="tingkat" class="form-control form-control-sm" style="font-size:11px;">
												<option value="Web SRU">Web SRU</option>
												<option value="Job Street" >Job Street</option>
												<option value="Karyawan SRU" >Karyawan SRU</option>
												<option value="Lainnya" >Lainnya</option>
											</select>	
										</div>
										<div class="col-sm-4">
											<input type="text" id="ifYes" readonly required name="keterangan_informasi_pilihan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Nama Karyawan SRU/lainnya.."> 
										</div>
									</div><br>
									<div class="row">
										<div class="col-sm-12 form-floating">
											<textarea class="form-control" name="pesan_pelamar" placeholder="Tambahkan Kalimat Perkenalan Diri atau apa pun yang ingin Anda sampaikan" id="floatingTextarea2" style="height: 100px"></textarea>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
										<input type="text">
									</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>

@endsection
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script>
	function informasi(that) {
    if (that.value == "Lainnya") {
		
				document.getElementById("ifYes").readOnly = false;;
			} else {
				document.getElementById("ifYes").readOnly = true;;
			}
		}
</script>

<script>
	$(document).ready(function(){
   $("#file_cv").change(function(){
     fileobj = document.getElementById('file_cv').files[0];
     var fname = fileobj.name;
     var ext = fname.split(".").pop().toLowerCase();
     if(ext == "pdf" || ext == "jpeg" || ext == "png" || ext == "jpg"){
        $("#info_img_file").html(fname);
     }else{
		alert("Hanya untuk file pdf, jpg, jpeg dan png saja..");
        $("#file_cv").val("");
        $("#info_img_file").html("Tidak ada file yag dimasukkan");
        return false;
     }
   });
//    $("#audio_file").change(function(){
//      fileobj = document.getElementById('audio_file').files[0];
//      var fname = fileobj.name;
//      var ext = fname.split(".").pop().toLowerCase();
//      if(ext == "mp3" || ext == "mp4" || ext == "wav"){
//         $("#info_audio_file").html(fname);
//      }else{
//         alert("Accepted file mp3, mp4 and wav only..");
//         $("#aud_file").val("");
//         $("#info_audio_file").html("No file selected");
//         return false;
//      }
//    });
   $("#btn_submit").click(function(){
     var img_file = $('#file_cv').val();
     
     if(img_file =="" || aud_file ==""){
         alert('Please select the file');
        return false;
     }
   });
});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>