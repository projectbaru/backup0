<script>

    // latar belakang pendidikan
    $(function() {
        var wrapper = $('#qualificationInputContainer');
        var z = $('.qualificationClone').length;

        $('#addQualificationInput').click(function(e) {
            e.preventDefault();

            // if (z < 1) {
            //     z++;
            // }

            z++;

            $(wrapper).append(`
                <div id="qualificationClone${z}" class="qualificationClone mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm"> Step</label>
                    <div class="col-sm-2">
                        <input type="text" style="font-size:11px;" name="nama_step[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
                    <div class="col-sm-2">
                    <input type="number" style="font-size:11px;" name="no_urut[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
                    <div class="col-sm-2">
                    <textarea type="text" style="font-size:11px;" name="keterangan[]" class="form-control form-control-sm" required></textarea>
                    </div>
                    
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeQualificationInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#qualificationClone${number}`).remove();

                z--;
            });
        });
    });
    // !latar belakang pendidikan
    // latar belakang pendidikan
    $(function() {
        var wrapper = $('#kursus');
        var z = $('.qualificationClone').length;

        $('#addKursusInput').click(function(e) {
            e.preventDefault();

            // if (z < 1) {
            //     z++;
            // }

            z++;

            $(wrapper).append(`
                <div id="qualificationClone${z}" class="qualificationClone mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
                    <div class="col-sm-2">
                        <input type="text" style="font-size:11px;" name="nama_step[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
                    <div class="col-sm-2">
                    <input type="number" style="font-size:11px;" name="no_urut[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
                    <div class="col-sm-2">
                    <textarea type="text" style="font-size:11px;" name="keterangan[]" class="form-control form-control-sm" required></textarea>
                    </div>
                    
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeQualificationInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#qualificationClone${number}`).remove();

                z--;
            });
        });
    });
    // !latar belakang pendidikan
 
</script>