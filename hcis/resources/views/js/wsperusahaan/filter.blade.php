<script>
    var isNull = false;
    $(function() {
        // var fields = <?= json_encode($fields) ?>;
        // console.log(fields);

        // SECTION reset
        $('.reset').click(function(e) {
            e.preventDefault();

            $('.clone').remove();
            $('#displayedColumn').find('option').remove().end();
            $('#displayedColumnInput').val('');
        });

        $('.queryField').change(function(e) {
            e.preventDefault();

            var number = $(this).data('number');
            var value = $(this).val();

            if (value == 'tanggal_mulai_perusahaan' || value == 'tanggal_selesai_perusahaan' || value == 'tanggal_mulai_efektif' || value == 'tanggal_selesai_efektif') {
                $(`#queryValueContainer${number}`).html('');
                $(`#queryValueContainer${number}`).append(`
                    <input type="date" name="queryValue[]" class="form-control" id="queryValue${number}">
                `);
            } else {
                $(`#queryValueContainer${number}`).html('');
                $(`#queryValueContainer${number}`).append(`
                    <input type="text" name="queryValue[]" class="form-control" id="queryValue${number}">
                `);
            }
        });
        // !SECTION ubah input jadi date atau text

        // SECTION tambah input kueri
        var wrapper = $('#queryInputContainer');
        var x = 1;

        $('.addQueryInput').click(function(e) {
            e.preventDefault();

            x++;

            $(wrapper).append(`
                <tr class="clone" id="clone${x}">
                    <td>
                        <select style="font-size:11px;height: calc(2.28rem + 2px);" name="queryField[]" id="queryField${x}" class="queryField form-control form-control-sm" data-number="${x}" required>
                            <option value="" selected null>-- kosong --</option>
                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++)
                                <option value="{{ $fields[$i]['value'] }}">{{ $fields[$i]['text'] }}</option>
                            @endfor
                        </select>
                    </td>
                    <td>
                        <select style="font-size:11px;height: calc(2.28rem + 2px);" name="queryOperator[]" id="queryOperator${x}" class="queryOperator form-control form-control-sm" data-number="${x}" required>
                            <option value="" selected null>-- kosong --</option>
                            @for($i = 0; $i < count($operators); $i++)
                                <option value="{{ $operators[$i] }}">{{ $operators[$i] }}</option>
                            @endfor
                        </select>
                    </td>
                    <td id="queryValueContainer${x}">
                        <input type="text" name="queryValue[]" style="font-size:11px;height: calc(2.28rem + 2px);" class="form-control form-control-sm" style="height: calc(1.55rem + 2px);" id="queryValue${x}" required>
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-danger removeQueryInput" style="border-radius:5px;border:1px solid white;font-size:11px;" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </td>
                </tr>
            `);

            $('.removeQueryInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#clone${number}`).remove();

                x--;
            });

            $('.queryField').change(function(e) {
                e.preventDefault();

                var number = $(this).data('number');
                var value = $(this).val();

                if (value == 'tanggal_mulai_perusahaan' || value == 'tanggal_selesai_perusahaan' || value == 'tanggal_mulai_efektif' || value == 'tanggal_selesai_efektif') {
                    $(`#queryValueContainer${number}`).html('');
                    $(`#queryValueContainer${number}`).append(`
                        <input type="date" name="queryValue[]" class="form-control" id="queryValue${number}">
                    `);
                } else {
                    $(`#queryValueContainer${number}`).html('');
                    $(`#queryValueContainer${number}`).append(`
                        <input type="text" name="queryValue[]" class="form-control" id="queryValue${number}">
                    `);
                }
            });
        });
        // !SECTION tambah input kueri
        // SECTION kirim option ke yang terpilih
        $('#addtoDisplay').click(function(e) {
            e.preventDefault();
            // $(this).remove();

            var text = $('#availableColumn :selected').text();
            var value = $('#availableColumn :selected').val();
            var foo = $('#availableColumn').val();
            var selectedText = $("#availableColumn :selected").map((_, e) => e.text).get();
            var selectedVal = $("#availableColumn :selected").map((_, e) => e.value).get();

            for (let i = 0; i < selectedVal.length; i++) {
                var filter = $('#displayedColumn').find(`[value=${selectedVal[i]}]`).length;
                if (filter == 0) {
                    $('#displayedColumn').append(`
                      <option value="${selectedVal[i]}">${selectedText[i]}</option>
                   `);
                    $("#availableColumn option[value='" + selectedVal[i] + "']").css('display', 'none');
                }
            }
            var selected = $('#displayedColumn option').toArray().map(item => item.value);

            $('#displayedColumnInput').val(selected);
        });
        // SECTION hapus selected option
        $('#returntoAvailable').click(function(e) {
            e.preventDefault();

            var value = $('#displayedColumn :selected').val();
            // console.log(value);
            // $("#availableColumn option[value='" + value + "']").css('display', 'block');

            var selectedValDis = $("#displayedColumn :selected").map((_, d) => d.value).get();

            for (let k = 0; k < selectedValDis.length; k++) {
                $("#availableColumn option[value='" + selectedValDis[k] + "']").css('display', 'block');

                if ($('#displayedColumn').find('option').length == 0) {
                    $('#displayedColumn').find('option').remove().end();
                    $('#displayedColumnInput').val('');
                } else {
                    $('#displayedColumn').find(`[value='${selectedValDis[k]}']`).remove();
                    isNull = true;
                }
            }
            if (isNull) {
                var selected = $('#displayedColumn option').toArray().map(item => item.value);
                $('#displayedColumnInput').val(selected);
            }
        });

        // !SECTION hapus selected option
        // SECTION keatas
        $('#moveup').click(function(e) {
            e.preventDefault();

            var select = $('#displayedColumn');
            var options = select && $('#displayedColumn option')
            var selected = [];

            for (var i = 0, iLen = options.length; i < iLen; i++) {
                if (options[i].selected) {
                    selected.push(options[i]);
                }
            }

            for (i = 0, iLen = selected.length; i < iLen; i++) {
                var index = selected[i].index;

                if (index == 0) {
                    break;
                }

                var temp = selected[i].text;
                selected[i].text = options[index - 1].text;
                options[index - 1].text = temp;

                temp = selected[i].value;
                selected[i].value = options[index - 1].value;
                options[index - 1].value = temp;

                selected[i].selected = false;
                options[index - 1].selected = true;
            }

            var selected = $('#displayedColumn option').toArray().map(item => item.value);

            $('#displayedColumnInput').val(selected);
        });
        // !SECTION keatas
        // SECTION ke paling atas
        $('#movetoFirst').click(function(e) {
            e.preventDefault();

            var select = $('#displayedColumn');
            var value = $('#displayedColumn :selected').val();

            select.find(`option[value=${value}]`).insertBefore(select.find('option:eq(0)'));

            var selected = $('#displayedColumn option').toArray().map(item => item.value);

            $('#displayedColumnInput').val(selected);
        });
        // !SECTION ke paling atas
        // SECTION kebawah
        $('#movedown').click(function(e) {
            e.preventDefault();

            var select = $('#displayedColumn');
            var options = select && $('#displayedColumn option')
            var selected = [];

            for (var i = 0, iLen = options.length; i < iLen; i++) {
                if (options[i].selected) {
                    selected.push(options[i]);
                }
            }

            for (i = selected.length - 1, iLen = 0; i >= iLen; i--) {
                var index = selected[i].index;

                if (index == (options.length - 1)) {
                    break;
                }

                var temp = selected[i].text;
                selected[i].text = options[index + 1].text;
                options[index + 1].text = temp;

                temp = selected[i].value;
                selected[i].value = options[index + 1].value;
                options[index + 1].value = temp;

                selected[i].selected = false;
                options[index + 1].selected = true;

            }

            var selected = $('#displayedColumn option').toArray().map(item => item.value);

            $('#displayedColumnInput').val(selected);
        });
        // !SECTION kebawah
        // SECTION ke paling bawah
        $('#movetoLast').click(function(e) {
            e.preventDefault();

            var select = $('#displayedColumn');
            var value = $('#displayedColumn :selected').val();

            select.find(`option[value=${value}]`).insertAfter(select.find('option:eq(-1)'));

            var selected = $('#displayedColumn option').toArray().map(item => item.value);

            $('#displayedColumnInput').val(selected);
        });
        // !SECTION ke paling bawah
        // !SECTION kirim option ke yang terpilih
    });
</script>