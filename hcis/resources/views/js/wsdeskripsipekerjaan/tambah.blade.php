<script>
    // SECTION tambah input kueri tanggung jawab
    $(function() {
        var wrapper = $('#responsibilityInputContainer');
        var x = 1;

        $('#addResponsibilityInput').click(function(e) {
            e.preventDefault();

            if (x < 1) {
                x++;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">T
                        anggung Jawab 
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="tanggung_jawab[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });
    });
    // !SECTION tambah input kueri tanggung jawab
    // SECTION tambah input kueri wewenang
    $(function() {
        var wrapper = $('#authorityInputContainer');
        var y = 1;

        $('#addAuthorityInput').click(function(e) {
            e.preventDefault();

            if (y < 1) {
                y++;
            }

            y++;

            $(wrapper).append(`
                <div id="authorityClone${y}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="wewenang[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeAuthorityInput" data-number="${y}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeAuthorityInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#authorityClone${number}`).remove();

                y--;
            });
        });
    });
    // !SECTION tambah input kueri wewenang
    // SECTION tambah input kueri kualifikasi
    $(function() {
        var wrapper = $('#qualificationInputContainer');
        var z = 1;

        $('#addQualificationInput').click(function(e) {
            e.preventDefault();

            if (z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="qualificationClone${z}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan </label>
                    <div class="col-sm-3">
                        <input type="text" style="font-size:11px;" name="kualifikasi_jabatan[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi</label>
                    <div class="col-sm-4">
                        <select name="tipe_kualifikasi[]" style="font-size:11px;" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="pendidikan">Pendidikan</option>
                            <option value="pengalaman">Pengalaman</option>
                            <option value="pelatihan/pengetahuan tambahan">Pelatihan/pengetahuan Tambahan</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeQualificationInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#qualificationClone${number}`).remove();

                z--;
            });
        });
    });
    // !SECTION tambah input kueri kualifikasi
    // SECTION tambah input kueri deskripsi
    $(function() {

        var wrapper = $('#descriptionInputContainer');
        var aa = 1;

        $('#addDescriptionInput').click(function(e) {
            e.preventDefault();

            if (aa < 1) {
                aa++;
            }

            aa++;

            $(wrapper).append(`
                <div id="descriptionClone${aa}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan</label>
                    <div class="col-sm-3">
                        <textarea rows="10" cols="60" style="font-size:11px;" name="deskripsi_pekerjaan[]" class="form-control form-control-sm" required></textarea>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                    <div class="col-sm-2">
                        <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                            <option value="plan">Plan</option>
                            <option value="do">Do</option>
                            <option value="check">Check</option>
                            <option value="action">Action</option>
                        </select>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                    <div class="col-sm-2">
                        <select style="font-size:11px;" name="bsc[]" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="Perspektif BSC">Perspektif BSC</option>
                            <option value="C">C</option>
                            <option value="F">F</option>
                            <option value="IP">IP</option>
                            <option value="LG">LG</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button style="font-size:11px;" type="button" class="btn btn-danger btn-sm removeDescriptionInput" data-number="${aa}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                    </div>
                    `);

            $('.removeDescriptionInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#descriptionClone${number}`).remove();

                aa--;
            });
        });

        // yepi

        var looks = <?= json_encode($datapo['looks']) ?>;
        var debug = <?= json_encode($debug) ?>;
        if (debug) {
            setDummy();
        }

        var wrapperDet = $('#detailWrapper');
        var cc = 1;
        var htmlCode = '';
        var htmlNama = '';
        looks.forEach(el => {
            htmlCode += `<option value="${el.kode_posisi}">${el.kode_posisi}</option>`;
            htmlNama += `<option value="${el.nama_posisi}">${el.nama_posisi}</option>`;

        });


        $('#addDescriptionDet').click(function(e) {
            e.preventDefault();

            if (cc < 1) {
                cc++;
            }

            cc++;

            $(wrapperDet).append(`
              <div id="descriptionCloneDet${cc}" class="mt-3 col-sm-11 descriptionCloneDet${cc}">
                
                 
                 <div class="form-group row">
                    <label for="deskripsi_pekerjaan${cc}" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan</label>
                    <div class="col-sm-3">
                        <textarea style="font-size:11px;" rows="5" cols="80"   id="deskripsi_pekerjaan${cc}" name="deskripsi_pekerjaan[]" class="form-control form-control-sm" required></textarea>
                    </div>
                    <label for="pdca${cc}" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                    <div class="col-sm-2">
                        <select name="pdca[]" id="pdca${cc}" style="font-size:11px;" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="plan">Plan</option>
                            <option value="do">Do</option>
                            <option value="check">Check</option>
                            <option value="action">Action</option>
                        </select>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                    <div class="col-sm-2">
                        <select style="font-size:11px;" name="bsc[]" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="Perspektif BSC">Perspektif BSC</option>
                            <option value="C">C</option>
                            <option value="F">F</option>
                            <option value="IP">IP</option>
                            <option value="LG">LG</option>
                        </select>
                    </div>                  
                 </div>
               </div>
                 <div class="col-sm-1 descriptionCloneDet${cc}">
                        <button style="font-size:11px;" type="button" class="btn btn-danger btn-sm removeDescriptionDet" data-number="${cc}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                </div>
            `);
            $('.removeDescriptionDet').click(function(e) {
                e.preventDefault();
                var numbers = $(this).data('number');
                $(`.descriptionCloneDet${numbers}`).remove();
                cc--;
            });
        });
    });
    // !SECTION tambah input kueri deskripsi
    function setDummy() {

        let now = new Date();
        let today = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
        // console.log(today);
        // $("input[type=date]").val(today);

        $("#wewenang").val("Test wewenang");
        $("#fungsi_jabatan").val("Test fungsi jabatan");
        $("#nama_pengawas").val("Test nama pengawas");
        $("#kualifikasi_jabatan").val("Test kualifikasi jabatan");
        $("#tipe_kualifikasi").val("pendidikan");
    }
</script>