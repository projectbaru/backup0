<script>
    // SECTION tambah input kueri tanggung jawab
    $(function() {
        var wrapper = $('#qualificationInputContainer');
        var x = 1;
        $('#addQualificationInput').click(function(e) {
            e.preventDefault();

            if (x < 1) {
                x++;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pewawancara</label>
                    <div class="col-sm-8">
                        <input type="text" name="nama_pewawancara[]" required style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
                    </div>                    
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });
    });
    // !SECTION tambah input kueri tanggung jawab
    // SECTION tambah input kueri wewenang
   
</script>