<script type="text/javascript">
	$(document).ready(function() {
		$("#ptkIfPergantian").hide();
		$('.ptkp').attr('required', false);

		// setDumy();

		$("#isCheckTambhan").change(function() {
			if (this.checked) {
				$("#isCheckPergantian").attr('disabled', true);
				$("#isCheckPergantian").removeAttr('required', true);
				$('.ptkp').attr('required', false);
				$("#ptkIfTambahan").show();
				$("#ptkIfPergantian").hide();
			} else {
				$("#isCheckPergantian").attr('disabled', false);
				$("#isCheckPergantian").attr('required', false);
				$('.ptkp').attr('required', true);
			}
		});

		$("#isCheckPergantian").change(function() {
			if (this.checked) {
				$("#isCheckTambhan").attr('disabled', true);
				$('.ptkt').attr('required', false);

				$("#ptkIfTambahan").hide();
				$("#ptkIfPergantian").show();
			} else {
				$("#isCheckTambhan").attr('disabled', false);
				$('.ptkt').attr('required', true);

				$("#ptkIfTambahan").show();
				$("#ptkIfPergantian").hide();
			}
		});
	});

	function setDumy() {
		$("#nomor_permintaan").val("014fsfs");
		$("#tanggal_mulai_efektif").val("2022-10-24");
		$("#tanggal_selesai_efektif").val("2022-10-25");
		$("#tanggal_permintaan").val("2022-10-22");
		$("#tanggal_efektif_transaksi").val("2022-10-23");
		$("#nama_ptk_tujuan_ptkt").val("test");
		$("#total_ptk").val(10);
		$("#deskripsi").val("test juga");
	};
</script>