<script>
    $(function () {
        // SECTION tanggung jawab
        var wrapper     = $('#responsibilityInputContainer');

        @if(count($analisa_hasil_interview) > 0)
            var x = '{{ count($analisa_hasil_interview) }}';
        @else
            var x = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($analisa_hasil_interview as $row)
            @if($i == 0)
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Karyawan
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="nama_karyawan[]" class="form-control form-control-sm" value="{{ $row->nama_karyawan }}" required>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-primary btn-sm" id="addResponsibilityInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Karyawan
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="nama_karyawan[]" class="form-control form-control-sm" required value="{{ $row->nama_karyawan }}">
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-danger btn-sm removeResponsibilityInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addResponsibilityInput').click(function (e) {
            e.preventDefault();

            if(x < 1) {
                x = 1;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama Karyawan 
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="nama_karyawan[]" value="{{ $row->nama_karyawan }}" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });

        $('.removeResponsibilityInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#responsibilityClone${number}`).remove();

            x--;
        });
        // !SECTION tanggung jawab
        

        
    });
</script>
