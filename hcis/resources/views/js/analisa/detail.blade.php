<script>
    $(function () {
        
        var wrapper     = $('#qualificationInputContainer');

        @if(count($rc_an_det) > 0)
            var x = '{{ count($rc_an_det) }}';
        @else
            var x = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($rc_an_det as $row)
            @if($i == 0)
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pewawancara</label>
                        <div class="col-sm-8">
                            <input type="text" name="" style="font-size:11px;" value="" class="form-control form-control-sm" id="colFormLabelSm" />
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-primary btn-sm" id="addQualificationInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pewawancara</label>
                        <div class="col-sm-8">
                            <input type="text" name="" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" />
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-danger btn-sm removeResponsibilityInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addQualificationInput').click(function (e) {
            e.preventDefault();

            if(x < 1) {
                x = 1;
            }
            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Pewawancara</label>
                    <div class="col-sm-8">
                        <input type="text" name="" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" />
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });

        $('.removeResponsibilityInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#responsibilityClone${number}`).remove();

            x--;
        });
        
        
    });
</script>
