<script>
     $(function() {
    var wrapper = $('#qualificationInputContainer');
    var z = $('.qualificationClone').length + 2;

    $('#addQualificationInput').click(function(e) {
        e.preventDefault();

        var newFormGroup = `
            <div id="qualificationClone${z}" class="qualificationClone mt-3 col-sm-12 row">
                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
                <div class="col-sm-2">
                    <input type="text" style="font-size:11px;" name="nama_step[]" class="form-control form-control-sm" required>
                </div>
                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
                <div class="col-sm-2">
                    <input type="number" style="font-size:11px;" name="no_urut[]" class="form-control form-control-sm" required value="${z}" disabled>
                </div>
                <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
                <div class="col-sm-2">
                    <textarea type="text" style="font-size:11px;" name="keterangan[]" class="form-control form-control-sm" required></textarea>
                </div>
                <div class="col-sm-1">
                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
                        <i class="fa-solid fa-minus"></i>
                    </button>
                </div>
            </div>
        `;

        $(wrapper).append(newFormGroup);

        updateNoUrut();

        z++;
    });

    // Menghapus form yang telah ditambahkan
    $(wrapper).on('click', '.removeQualificationInput', function(e) {
        e.preventDefault();

        var container = $(this).closest('.qualificationClone');
        var number = $(this).data('number');

        container.remove();

        updateNoUrut();
    });

    function updateNoUrut() {
        $('.qualificationClone').each(function(index) {
            var inputNoUrut = $(this).find('input[name="no_urut[]"]');
            inputNoUrut.val(index + 2);
        });
    }

    $('form').submit(function() {
        // Mengaktifkan semua input nomor urutan sebelum form disubmit
        $('.qualificationClone').each(function(index) {
            var inputNoUrut = $(this).find('input[name="no_urut[]"]');
            inputNoUrut.prop('disabled', false);
        });
    });
});
</script>