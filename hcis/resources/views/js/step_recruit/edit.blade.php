<script>


$(function() {
  var wrapper = $('#qualificationInputContainer');
  var z = $('.qualificationClone').length + 1;

  function removeQualificationInput() {
    var number = $(this).data('number');
    var id = $(this).data('id');

    // Hapus data terkait
    $(`#nama_step_${number}`).val('');
    $(`#no_urut_${number}`).val('');
    $(`#keterangan_${number}`).val('');

    $(`#qualificationClone${number}`).remove();

    // Mengupdate nomor urutan setelah penghapusan elemen
    $('.qualificationClone').each(function(index) {
      var inputNoUrut = $(this).find('input[name="no_urut[]"]');
      inputNoUrut.val(index + 1);
    });
  }

  $('#addQualificationInput').click(function(e) {
    e.preventDefault();

    var newNoUrut = $('.qualificationClone').length + 1;

    $(wrapper).append(`
      <div id="qualificationClone${z}" class="qualificationClone mt-3 col-sm-12 row">
        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Nama Step</label>
        <div class="col-sm-2">
          <input type="text" style="font-size:11px;" name="nama_step[]" id="nama_step_${z}" class="form-control form-control-sm" required>
        </div>
        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Urutan</label>
        <div class="col-sm-2">
          <input type="number" style="font-size:11px;" name="no_urut[]" id="no_urut_${z}" class="form-control form-control-sm" required value="${newNoUrut}" disabled>
        </div>
        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">Keterangan</label>
        <div class="col-sm-2">
          <textarea type="text" style="font-size:11px;" name="keterangan[]" id="keterangan_${z}" class="form-control form-control-sm" required></textarea>
        </div>
        <div class="col-sm-1">
          <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
            <i class="fa-solid fa-minus"></i>
          </button>
        </div>
      </div>
    `);

    z++;
  });

  $(wrapper).on('click', '.removeQualificationInput', removeQualificationInput);

  $('form').submit(function() {
    // Mengaktifkan semua input nomor urutan sebelum form disubmit
    $('.qualificationClone').each(function(index) {
      var inputNoUrut = $(this).find('input[name="no_urut[]"]');
      inputNoUrut.prop('disabled', false);
    });
  });
});
   
    // !SECTION tambah input kueri kualifikasi
    // SECTION tambah input kueri deskripsi
 
    // !SECTION tambah input kueri deskripsi
    function setDummy() {

        let now = new Date();
        let today = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
        // console.log(today);
        // $("input[type=date]").val(today);

        $("#wewenang").val("Test wewenang");
        $("#fungsi_jabatan").val("Test fungsi jabatan");
        $("#nama_pengawas").val("Test nama pengawas");
        $("#kualifikasi_jabatan").val("Test kualifikasi jabatan");
        $("#tipe_kualifikasi").val("pendidikan");
    }
</script>