<script>

    // SECTION tambah input kueri kualifikasi

    // !SECTION tambah input kueri kualifikasi
    // SECTION tambah input kueri deskripsi
    $(function() {
        var wrapper = $('#kursus');
        var z = 1;

        $('#addKursusInput').click(function(e) {
            e.preventDefault();

            if (z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="KursusClone${z}" style="margin-top:-15px;" class="col-sm-12 row">
                <table class="table table-bordered">

                    <tbody>															
                        <tr>
                            <td><input type="text" required class="form-control form-control-sm" name="bidang_pelatihan[]"></td>
                            <td><input type="text" required class="form-control form-control-sm" name="penyelenggara[]"></td>
                            <td><input type="text" required class="form-control form-control-sm" name="tempat_kota[]"></td>
                            <td><input type="number" required class="form-control form-control-sm" name="lama_pelatihan[]"></td>
                            <td><input type="number" class="form-control form-control-sm" required name="tahun_pelatihan[]"></td>
                            <td><input type="text" class="form-control form-control-sm" required name="dibiayai_oleh[]"></td>
                            <td>     
                                <div class="col-sm-1">
                                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeKursusInput" data-number="${z}">
                                        <i class="fa-solid fa-minus"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                  
                </div>
            `);

            $('.removeKursusInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#KursusClone${number}`).remove();

                z--;
            });
        });
    });

    // !SECTION tambah input kueri deskripsi
    $(function() {
        var wrapper = $('#kursus');
        var z = 1;

        $('#addKursusInput').click(function(e) {
            e.preventDefault();

            if (z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="KursusClone${z}" style="margin-top:-15px;" class="col-sm-12 row">
                <table class="table table-bordered">

                    <tbody>															
                        <tr>
                            <td><input type="text" required class="form-control form-control-sm" name="bidang_pelatihan[]"></td>
                            <td><input type="text" required class="form-control form-control-sm" name="penyelenggara[]"></td>
                            <td><input type="text" required class="form-control form-control-sm" name="tempat_kota[]"></td>
                            <td><input type="number" required class="form-control form-control-sm" name="lama_pelatihan[]"></td>
                            <td><input type="number" class="form-control form-control-sm" required name="tahun_pelatihan[]"></td>
                            <td><input type="text" class="form-control form-control-sm" required name="dibiayai_oleh[]"></td>
                            <td>     
                                <div class="col-sm-1">
                                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeKursusInput" data-number="${z}">
                                        <i class="fa-solid fa-minus"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                  
                </div>
            `);

            $('.removeKursusInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#KursusClone${number}`).remove();

                z--;
            });
        });
    });


    $(function() {
        var wrapper = $('#bahasa');
        var z = 1;

        $('#addBahasaInput').click(function(e) {
            e.preventDefault();

            if (z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="BahasaClone${z}" style="margin-top:-15px;" class="col-sm-12 row">
                <table class="table table-bordered">

                    <tbody>															
                        <tr>
                            <td scope="row">
                                <input type="text" required class="form-control form-control-sm" name="bahasa[]">
                            </td>
                            <td>
                                <div class="col-sm-10">
                                    <select name="mendengar[]" required id="mendengar" class="form-control form-control-sm">
                                        <option value="kurang"  >Kurang</option>
                                        <option value="baik" data-kode ="" >Baik</option>
                                        <option value="sangat baik" data-kode ="" >Sangat Baik</option>
                                    </select>	
                                </div>
                            </td>
                            <td>
                                <div class="col-sm-10">
                                    <select name="membaca[]" required id="membaca" class="form-control form-control-sm">
                                        <option value="kurang">Kurang</option>
                                        <option value="baik">Baik</option>
                                        <option value="sangat baik">Sangat Baik</option>
                                    </select>	
                                </div>
                            </td>
                            <td>
                                <div class="col-sm-10">
                                    <select name="berbicara[]" required id="berbicara" class="form-control form-control-sm">
                                        <option value="kurang" >Kurang</option>
                                        <option value="baik" >Baik</option>
                                        <option value="sangat baik" >Sangat Baik</option>
                                    </select>	
                                </div>
                            </td>
                            <td>
                                <div class="col-sm-10">
                                    <select name="menulis[]" required id="menulis" class="form-control form-control-sm">
                                        <option value="kurang" >Kurang</option>
                                        <option value="baik"  >Baik</option>
                                        <option value="sangat baik">Sangat Baik</option>
                                    </select>	
                                </div>
                            </td>
                            <td>     
                                <div class="col-sm-1">
                                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeBahasaInput" data-number="${z}">
                                        <i class="fa-solid fa-minus"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                  
                </div>
            `);

            $('.removeBahasaInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#BahasaClone${number}`).remove();

                z--;
            });
        });
    });

    $(function() {
        var wrapper = $('#Sosial');
        var z = 1;

        $('#addSosialInput').click(function(e) {
            e.preventDefault();

            if (z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="SosialClone${z}" style="margin-top:-15px;" class="col-sm-12 row">
                <table class="table table-bordered">

                    <tbody>															
                        <tr>
                            <td><input type="text" class="form-control form-control-sm" name="nama_organisasi[]"></td>
								<td><input type="text" class="form-control form-control-sm" name="jenis_kegiatan[]"></td>
								<td><input type="text" class="form-control form-control-sm" name="jabatan[]"></td>
								<td><input type="number" class="form-control form-control-sm" name="tahun[]"></td>
                            <td>
                                <div class="col-sm-1">
                                    <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeSosialInput" data-number="${z}">
                                        <i class="fa-solid fa-minus"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                  
                </div>
            `);

            $('.removeSosialInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#SosialClone${number}`).remove();

                z--;
            });
        });
    });


    function setDummy() {

        let now = new Date();
        let today = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
        // console.log(today);
        // $("input[type=date]").val(today);

        $("#wewenang").val("Test wewenang");
        $("#fungsi_jabatan").val("Test fungsi jabatan");
        $("#nama_pengawas").val("Test nama pengawas");
        $("#kualifikasi_jabatan").val("Test kualifikasi jabatan");
        $("#tipe_kualifikasi").val("pendidikan");
    }
</script>