<script>
    $(function () {
        
        var wrapper     = $('#add_pkhr');

        @if(count($rc_kpi_hr) > 0)
            var x = '{{ count($rc_kpi_hr) }}';
        @else
            var x = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($pkhr as $row)
            @if($i == 0)
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                <div class="col-sm-9">
                                    <select name="bulan" id="bulan" class="form-control form-control-sm">
                                        @foreach($pkhr as $kpi)
                                            <option value="{{$kpi->bulan}}">{{$kpi->bulan}}</option>
                                        @endforeach
                                    </select>	
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="target_perbulan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="weight" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="act" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="ach" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-primary btn-sm" id="addQualificationInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                    <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                <div class="col-sm-9">
                                    <select name="bulan" id="bulan" class="form-control form-control-sm">
                                        @foreach($pkhr as $kpi)
                                            <option value="{{$kpi->bulan}}">{{$kpi->bulan}}</option>
                                        @endforeach
                                    </select>	
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="target_perbulan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="weight" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="act" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                    <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group row">
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                <div class="col-sm-9">
                                    <input type="text"  required name="ach" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-danger btn-sm removeResponsibilityInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addQualificationInput').click(function (e) {
            e.preventDefault();

            if(x < 1) {
                x = 1;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                            <div class="col-sm-9">
                                <select name="bulan" id="bulan" class="form-control form-control-sm">
                                    @foreach($pkhr as $kpi)
                                        <option value="{{$kpi->bulan}}">{{$kpi->bulan}}</option>
                                    @endforeach
                                </select>	
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                            <div class="col-sm-9">
                                <input type="text"  required name="target_perbulan" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                            <div class="col-sm-9">
                                <input type="text"  required name="weight" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                            <div class="col-sm-9">
                                <input type="text"  required name="act" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                            <div class="col-sm-9">
                                <input type="text"  required name="ach" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            `);
            $('.removeResponsibilityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });

        $('.removeResponsibilityInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#responsibilityClone${number}`).remove();

            x--;
        });
        
        
    });
</script>
