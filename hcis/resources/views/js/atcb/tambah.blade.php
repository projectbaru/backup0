<script>
    // SECTION tambah input kueri tanggung jawab
    $(function() {
        // var x = 1;
        // $('#jmldetail').val(x);
        $(document).on('click','.add_pkhr',function(e) {
            e.preventDefault();

            var val = $('#myText').val()

            let wp = $(this).attr('data-detailnum');
            var x = $(`.container-position${wp}`).length;

            x++;

            $(`#descriptionInputContainer${wp}`).append(`
                <div id="responsibilityClone${wp}${x}" class="row container-fluid container-position${wp}">
                <div class="card">
				<div class="card-body">
				<div class="row">    
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                            <div class="col-sm-8">
                                <select name="nama_posisi[${wp-1}][]" onchange="myFunction(event)" required class="form-control form-control-sm">
                                    <option value="" selected disabled>--Pilih Posisi--</option>
                                    @foreach($po['looks'] as $pos)
                                    <option value="{{$pos->nama_posisi}}">{{$pos->nama_posisi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Total Vacant in AVG/Month </label>
                            <div class="col-sm-4">
                                <input type="number" name="tahun_total_vacant[${wp-1}][]" id="tahun_total_vacant" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="number" name="rata_rata_perbulan[${wp-1}][]" id="rata_rata_perbulan"  class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Avg/ month</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer Candidate</label>
                            <div class="col-sm-4">
                                <input type="number" name="tahun_buffer_candidate[${wp-1}][]" id="tahun_buffer_candidate" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="number" name="target_buffer_candidate[${wp-1}][]" id="target_buffer_candidate" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer/Quarter</label>
                            <div class="col-sm-4">
                                <input type="number" name="tahun_target_buffer_quarter[${wp-1}][]" id="tahun_target_buffer_quarter" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="number" name="target_buffer_quarter[${wp-1}][]" id="target_buffer_quarter" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
					</div>
                    <div class="col-md-4">
                    <div class="mb-2" style="float:right;">
                        <button type="button" style="font-size:10px;" class="btn btn-sm btn-danger removeResponsibilityInputDetail" data-header="${wp}" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th>Target</th>
                                <th>Actual</th>
                                <th>Ach</th>
                            </tr>
                            <tr>
                                <th><input type="text"  readonly name="quarter[${wp-1}][]" class="form-control form-control-sm" value="Q1" required></th>
                                <th><input type="number" name="target_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number"  name="actual_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number" name="achivement_quarter[${wp-1}][]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text"  readonly name="quarter[${wp-1}][]" class="form-control form-control-sm" value="Q2" required></th>
                                <th><input type="number" name="target_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number"  name="actual_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number" name="achivement_quarter[${wp-1}][]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text" readonly name="quarter[${wp-1}][]" class="form-control form-control-sm" value="Q3" required></th>
                                <th><input type="number" name="target_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number"  name="actual_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number" name="achivement_quarter[${wp-1}][]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text" readonly name="quarter[${wp-1}][]" class="form-control form-control-sm" value="Q4" required></th>
                                <th><input type="number" name="target_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number"  name="actual_buffer[${wp-1}][]" class="form-control form-control-sm" required></th>
                                <th><input type="number" name="achivement_quarter[${wp-1}][]" class="form-control form-control-sm" required></th>
                            </tr>
                        </table>
                    </div>
                </div>
                </div>
                </div>
                </div>
            `);

            // wp++;

        });

        $(document).on('click','.removeResponsibilityInputDetail', function(e) {
                e.preventDefault();

                var header = $(this).data('header');
                var number = $(this).data('number');

                $(`#responsibilityClone${header}${number}`).remove();

                // x--;
        });
    });

    $(function() {
        var wrapper = $('#pkhr_tambah');
        // var x = 1;
        $('#add_pkhr_all').click(function(e) {
            e.preventDefault();

            // if (x < 1) {
            //     x++;
            // }
            var x = $(".container-divisi").length;
            x++;

            $('#jmldetail').val(x);

            $(wrapper).append(`

            <br>
            <div class="container-divisi" style="">
            <div class="card">
                <div class="card-header" style="background-color: rgb(42 117 183);color:white;">
                    <div class="row">
                        <div class="col">
			        		<b style="">Data Divisi</b>
			        	</div>
                        <div class="col" style="text-align: right;">
                            <button type="button" style="font-size:11px;" class="btn btn-sm btn-danger removeResponsibilityInput" id="removeResponsibilityInput${x}" data-number="${x}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body" id="responsibilityClone${x}">
                    <div class="row container-fluid">
                        
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row" style="text-align: left;">
                                        <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm" style="font-size:11px;">Nama Divisi</label>
                                        <div class="col-sm-2">
                                            <select name="nama_divisi[]" required id="child_nama_divisi" class="form-control form-control-sm">
                                               <option value="" selected disabled>--Pilih Divisi--</option>
                                                @foreach($to['looks'] as $tog)
                                                    <option value="{{$tog->tingkat_organisasi}}">{{$tog->tingkat_organisasi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row container-fluid" id="descriptionInputContainer${x}">
                            <div id="responsibilityClone${x}1" class="row container-fluid container-position${x}">
                            <div class="card">
							<div class="card-body">
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                        <div class="col-sm-8">
                                            <select name="nama_posisi[${x-1}][]" onchange="myFunction(event)" required class="form-control form-control-sm">
                                                <option value="" selected disabled>--Pilih Posisi--</option>
                                                @foreach($po['looks'] as $pos)
                                                <option value="{{$pos->nama_posisi}}">{{$pos->nama_posisi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Total Vacant</label>
                                        <div class="col-sm-4">
                                            <input type="number" name="tahun_total_vacant[${x-1}][]" class="form-control form-control-sm" id="tahun_total_vacant" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="number" name="rata_rata_perbulan[${x-1}][]" class="form-control form-control-sm" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;" id="rata_rata_perbulan">Avg/ month</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Target Buffer Candidate</label>
                                        <div class="col-sm-4">
                                            <input type="number" name="tahun_buffer_candidate[${x-1}][]" id="tahun_buffer_candidate" class="form-control form-control-sm" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="number" name="target_buffer_candidate[${x-1}][]" id="target_buffer_candidate" class="form-control form-control-sm" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Target Buffer/Quarter</label>
                                        <div class="col-sm-4">
                                            <input type="number" name="tahun_target_buffer_quarter[${x-1}][]" id="tahun_target_buffer_quarter" class="form-control form-control-sm" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;">Tahun</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="number" name="target_buffer_quarter[${x-1}][]" id="target_buffer_quarter" class="form-control form-control-sm" required>
                                            <label for="colFormLabelSm" class="" style="font-size:8px;">Target</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
								</div>
                                <div class="col-md-4">
                                    <div class="mb-2" style="float:right;">
                                        <button type="button" style="font-size:10px;" class="btn btn-sm btn-success add_pkhr" id="add_pkhr" data-detailnum="${x}">
                                            <i class="fa-solid fa-plus"></i>
                                        </button>
                                    </div>
                                    <br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th></th>
                                            <th>Target</th>
                                            <th>Actual</th>
                                            <th>Ach</th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" readonly name="quarter[${x-1}][]"  class="form-control form-control-sm" value="Q1" required></th>
                                            <th><input type="number" name="target_buffer[${x-1}][]" id="target_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number"  name="actual_buffer[${x-1}][]" id="actual_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number" name="achivement_quarter[${x-1}][]" id="achivement_quarter" class="form-control form-control-sm" required></th>

                                        </tr>
                                        <tr>
                                            <th><input type="text" readonly name="quarter[${x-1}][]" class="form-control form-control-sm" value="Q2" required></th>
                                            <th><input type="number" name="target_buffer[${x-1}][]" id="target_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number"  name="actual_buffer[${x-1}][]" id="actual_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number" name="achivement_quarter[${x-1}][]" id="achivement_quarter" class="form-control form-control-sm" required></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" readonly name="quarter[${x-1}][]" class="form-control form-control-sm" value="Q3" required></th>
                                            <th><input type="number" name="target_buffer[${x-1}][]" id="target_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number"  name="actual_buffer[${x-1}][]" id="actual_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number" name="achivement_quarter[${x-1}][]" id="achivement_quarter" class="form-control form-control-sm" required></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" readonly name="quarter[${x-1}][]" class="form-control form-control-sm" value="Q4" required></th>
                                            <th><input type="number" name="target_buffer[${x-1}][]" id="target_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number"  name="actual_buffer[${x-1}][]" id="actual_buffer" class="form-control form-control-sm" required></th>
                                            <th><input type="number" name="achivement_quarter[${x-1}][]" id="achivement_quarter" class="form-control form-control-sm" required></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            `);
        });

        $(document).on('click','.removeResponsibilityInput', function(e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#responsibilityClone${number}`).remove();
            $(`#removeResponsibilityInput${number}`).remove();

            // x--;
        });
    });
    // !SECTION tambah input kueri tanggung jawab
    // SECTION tambah input kueri wewenang

</script>
