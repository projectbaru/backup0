<script>
    // SECTION tambah input kueri tanggung jawab
    $(function() {
        var x = $('#totalChild').val();
        $('#jmldetail').val(x);
        $(document).on('click','.add_pkhr',function(e) {
            e.preventDefault();
            let wp = $(this).attr('data-detailnum');
            $(`#descriptionInputContainer${wp}`).append(`
                <div id="responsibilityClone${x}${wp}" class="mt-3 container col-sm-12 row">    
                    <div class="col">
                        <br>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama_posisi${wp}[]" class="form-control form-control-sm" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Vacant in AVG/Month </label>
                            <div class="col-sm-4">
                                <input type="text" name="tahun_total_vacant${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="avg/month${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:8px;">Avg/ month</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer Candidate</label>
                            <div class="col-sm-4">
                                <input type="text" name="nama_posisi${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="avg/month${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:8px;">Target</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Total Buffer/Quarter</label>
                            <div class="col-sm-4">
                                <input type="text" name="nama_posisi${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:8px;">Tahun</label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="avg/month${wp}[]" class="form-control form-control-sm" required>
                                <label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm" style="font-size:8px;">Target</label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <th rowspan="2">Q1</th>
                                <th>Target</th>
                                <th>Actual</th>
                                <th>Ach</th>
                            </tr>
                            <tr>
                                <th><input type="text" disabled name="target_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="actual_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="achivement_quarter${wp}[]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text" disabled name="quarter${wp}[]" class="form-control form-control-sm" value="Q2" required></th>
                                <th><input type="text" disabled name="target_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="actual_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="achivement_quarter${wp}[]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text" disabled name="quarter${wp}[]" class="form-control form-control-sm" value="Q3" required></th>
                                <th><input type="text" disabled name="target_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="actual_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="achivement_quarter${wp}[]" class="form-control form-control-sm" required></th>
                            </tr>
                            <tr>
                                <th><input type="text" disabled name="quarter${wp}[]" class="form-control form-control-sm" value="Q4" required></th>
                                <th><input type="text" disabled name="target_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="actual_buffer${wp}[]" class="form-control form-control-sm" required></th>
                                <th><input type="text" disabled name="achivement_quarter${wp}[]" class="form-control form-control-sm" required></th>
                            </tr>
                        </table>
                    </div>
                    <div style="">
                        <button type="button" style="font-size:10px;" class="btn btn-sm btn-danger removeResponsibilityInputDetail" data-header="${x}" data-number="${wp}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);
            
            wp++;
            
        });

        $(document).on('click','.removeResponsibilityInputDetail', function(e) {
                e.preventDefault();

                var header = $(this).data('header');
                var number = $(this).data('number');
                $(`#descriptionInputContainer${header}${number}`).remove();
                $(`#responsibilityClone${header}${number}`).remove();
        });
    });

    $(function() {
        var wrapper = $('#pkhr_tambah');
        var x = 1;
        $('#add_pkhr_all').click(function(e) {
            e.preventDefault();

            if (x < 1) {
                x++;
            }

            x++;

            $('#jmldetail').val(x);

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <div class="row container" style="border:1px solid #ced4da;">
                        <div class="col-md-10">
                            <b>KPI</b>
                            <div class="row" >
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">KPI</label>
                                        <div class="col-sm-9">
                                            <select name="nama_kpi${x}" id="nama_kpi" class="form-control form-control-sm">
                                                <option value="A/B Recruitment Cost">A/B Recruitment Cost</option>
                                                <option value="A/B Recruitment Cost">A/T Fullfillment Rate</option>
                                                <option value="% Training Delivery">% Training Delivery</option>
                                                <option value="Training Evaluation">Training Evaluation</option>
                                                <option value="Applicant Target">Applicant Target</option>
                                                <option value="Reporting Management">Reporting Management</option>
                                                <option value="Training Mandays">Training Mandays</option>
                                            </select>	
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Polarization</label>
                                        <div class="col-sm-9">
                                            <select name="polarization${x}" id="polarization" class="form-control form-control-sm">
                                                <option value="Lower Better">Lower Better</option>
                                                <option value="High Better">High Better</option>
                                            </select>	
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">UoM</label>
                                        <div class="col-sm-9">
                                            <select name="uom${x}" id="uom" class="form-control form-control-sm">
                                                <option value="percent">Percent</option>
                                                <option value="point">Point</option>
                                                <option value="hour">Hour</option>
                                            </select>	
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="weight${x}" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Definition 1</label>
                                        <div class="col-sm-7">
                                            <textarea type="text" required name="definition${x}_1" style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm" style="font-size:11px;">Definition 2</label>
                                        <div class="col-sm-7">
                                            <textarea type="text" required name="definition${x}_2" style="font-size:11px;" rows="6" cols="15" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Output</label>
                                        <div class="col-sm-9">
                                            <select name="output${x}" id="output" class="form-control form-control-sm">
                                                <option value="Monthly (Jan – Des)">Monthly (Jan – Des)</option>
                                                <option value="Quarterly (Jan, Apr, Jul, Oct)">Quarterly (Jan, Apr, Jul, Oct)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="target${x}" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="aktif" name="manager${x}" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Mgr
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="aktif" name="pic${x}" id="flexCheckChecked">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    PIC
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="aktif" name="staff${x}" id="flexCheckChecked">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Staff
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="container row" id="descriptionInputContainer${x}">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Bulan</label>
                                        <div class="col-sm-9">
                                            <select name="bulan${x}[]" id="bulan" class="form-control form-control-sm">
                                                    <option value="Januari">Januari</option>
                                                    <option value="Februari">Februari</option>
                                                    <option value="Maret">Maret</option>
                                                    <option value="April">April</option>
                                                    <option value="Mei">Mei</option>
                                                    <option value="Juni">Juni</option>
                                                    <option value="Juli">Juli</option>
                                                    <option value="Agustus">Agustus</option>
                                                    <option value="September">September</option>
                                                    <option value="Oktober">Oktober</option>
                                                    <option value="November">November</option>
                                                    <option value="Desember">Desember</option>
                                            </select>	
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Target</label>
                                        <div class="col-sm-9">
                                            <input type="text" required name="target_perbulan${x}[]" style="font-size:11px;"   class="form-control form-control-sm" placeholder="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Weight</label>
                                        <div class="col-sm-9">
                                            <input type="text"  required name="weightd${x}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                            <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Act</label>
                                        <div class="col-sm-9">
                                            <input type="text"  required name="act${x}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="" >
                                            <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Ach</label>
                                        <div class="col-sm-9">
                                            <input type="text" required name="ach${x}[]" style="font-size:11px;" class="form-control form-control-sm" placeholder="">
                                            <!-- <input type="text" required name="resume_cv" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">  -->
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button type="button" style="font-size:10px;" class="btn btn-sm btn-primary add_pkhr" id="add_pkhr" data-detailnum="${x}">
                                        <i class="fa-solid fa-plus"></i>
                                    </button>
                                </div>
                                
                            </div>
                        </div>    
                    </div>
                    <div class="">
                        <button type="button" style="font-size:10px;" class="btn btn-sm btn-danger removeResponsibilityInput" id="removeResponsibilityInput${x}" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);
        });

        $('.removeResponsibilityInput').click(function(e) {
                e.preventDefault();

                var number = $(this).data('number');
                $(`#responsibilityClone${number}`).remove();
                $(`#removeResponsibilityInput${number}`).remove();

                x--;
        });
    });
    // !SECTION tambah input kueri tanggung jawab
    // SECTION tambah input kueri wewenang
    
</script>
