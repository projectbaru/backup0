<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>HCIS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'> -->

    <!-- <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico') }}"> -->
    @include('layouts.head-css')
    @stack('css')
</head>

<!-- <body class="dark-sidenav" style="background-color:#f4f6f9;"> -->
<body class="dark-sidenav" style="">
    @include('layouts.sidebar')

    <!-- <div class="page-wrapper" style="background-color:#ecf0f5;"> -->
    <div class="page-wrapper">
        @include('layouts.topbar')

        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>

    </div>
    @include('layouts.vendor-scripts')
    @stack('scripts')
    @yield('add-scripts')
</body>
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script> -->


</html>