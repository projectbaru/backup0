@extends('re.lk.side')

@section('content')
<div class="pcoded-wrapper" >
	<div class="pcoded-content" >
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header">
						<div class="page-block">
							<div class="row align-items-center">
								<div class="col-md-12" style="margin-top:10px;">
									<div class="page-header-title" >
										<h5 class="m-b-10">Tambah Lowongan Kerja</h5>
									</div>
									<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#!">Recruitment</a></li>
										<li class="breadcrumb-item"><a href="#!">Lowongan Kerja</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>  
						<form action="{{route('simpan_dcp')}}" method="post" enctype="multipart/form-data">
							<hr>
							{{ csrf_field() }}
							<div class="form-group" style="width:95%;">
								<div class="">
									<!-- <div class="container-fluid" style="color:white;text-align:center;background-color:#0288d1;">
										<p>Tambah Data CV Pelamar</p>
									</div> -->
									<b>Melamar Lowongan</b>
									<div class="row">
										<div class="col" style="font-size: 10px;">
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nomor FPTK</label>
												<!-- <div class="col-sm-9">
												<input type="text" required name="jabatan" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
												</div> -->
												<div class="col-sm-9">
													<input type="text" required name="nomer_dokumen_fptk" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kategori Pekerjaan</label>
												<div class="col-sm-9">
													<!-- {{-- <input type="text" required name="tingkat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> --}} -->
													<select name="kategori_pekerjaan" id="kategori_pekerjaan" class="form-control form-control-sm" style="font-size:11px;">
														<option value="" selected disabled>--Tingkat--</option>
														<option value="staff" >Staff</option>
														<option value="middle" >Middle</option>
														<option value="senior">Senior</option>
													</select>	
												</div>
										
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
												<div class="col-sm-9">
													<input type="text" required name="jabatan_lowongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> 
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi Kerja</label>
												<div class="col-sm-9">
													<select name="lokasi_kerja" id="lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;">
														<option value="staff" >Staff</option>
														<option value="middle" >Middle</option>
														<option value="senior">Senior</option>
													</select>	
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">tingkat</label>
												<div class="col-sm-9">
													<select name="tingkat_kerjaan" id="tingkat_kerjaan" class="form-control form-control-sm" style="font-size:11px;">
														<option value="staff" >Staff</option>
														<option value="middle" >Middle</option>
														<option value="senior">Senior</option>
													</select>	
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
												<div class="col-sm-9">
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja"  value="Berpengalaman" >
														<label class="form-check-label" for="flexRadioDefault1">
															Berpengalaman/Lulusan Baru
														</label>
													</div>&nbsp;&nbsp;&nbsp;
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja" value="Lulusan Baru">
														<label class="form-check-label" for="flexRadioDefault1">
															Berpengalaman
														</label>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kepegawaian</label>
												<div class="col-sm-9">
													<select name="tingkat_kerjaan" id="tingkat_kerjaan" class="form-control form-control-sm" style="font-size:11px;">
														<option value="staff" >Staff</option>
														<option value="middle" >Middle</option>
														<option value="senior">Senior</option>
													</select>	
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Pengalaman Kerja</label>
												<div class="col-sm-9">
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja"  value="SMK/Sederajat" >
														<label class="form-check-label" for="flexRadioDefault1">
															SMK/SMA
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja" value="D3">
														<label class="form-check-label" for="flexRadioDefault1">
															D3
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja" value="S1">
														<label class="form-check-label" for="flexRadioDefault1">
															S1
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja" value="S2">
														<label class="form-check-label" for="flexRadioDefault1">
															S2
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="pengalaman_kerja" value="S3">
														<label class="form-check-label" for="flexRadioDefault1">
															S3
														</label>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Kepegawaian</label>
												<div class="col-sm-9">
													<textarea name="peran_kerja"  id="peran_kerja" cols="30" rows="10" class="form-control form-control-sm" placeholder="Tentang Peran Pekerjaan"></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan</label>
												<div class="col-sm-9">
													<textarea name="deskripsi_pekerjaan" id="deskripsi_pekerjaan" cols="30" rows="10" class="form-control form-control-sm" placeholder="Deskripsi Pekerjaan"></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Pengalaman</label>
												<div class="col-sm-9">
													<textarea name="syarat_pengalaman" id="syarat_pengalaman" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Pengalaman"></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Kemampuan</label>
												<div class="col-sm-9">
													<textarea name="syarat_kemampuan" id="syarat_pengalaman" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Kemampuan"></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Syarat Lainnya</label>
												<div class="col-sm-9">
													<textarea name="syarat_lainnya" id="syarat_pengalaman" cols="30" rows="10" class="form-control form-control-sm" placeholder="Syarat Lainnya"></textarea>
												</div>
											</div>
										</div>
									</div>
										<hr>
										
										<div class="row">
											<div class="col" style="font-size: 10px;">
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
													<div class="col-sm-9">
														<input type="text" name="jumlah_karyawan" style="font-size:11px;" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""/>
													</div>
												</div>
											
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Upload</label>
													<div class="col-sm-9">
													<input type="date" required name="tanggal_upload" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status Lowongan</label>
													<div class="col-sm-9">
														<div class="col-sm-9">
															<div class="form-check">
																<input class="form-check-input" type="radio" name="status_lowongan" value="buka">
																<label class="form-check-label" for="flexRadioDefault1">
																	Buka
																</label>
															</div>
															<div class="form-check">
																<input class="form-check-input" type="radio" name="status_lowongan" value="selesai">
																<label class="form-check-label" for="flexRadioDefault1">
																	Selesai
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div style="width: 100%;">
								<table>
								<tr>
									<!-- <td>
										<input type="text">
									</td> -->
									<td>
										<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
									</td>
									<td>
										<a href="{{ route('list_dcp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
									</td>
								</tr>
								</table>
							</div>
							<br>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>
<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script> 
	$(document).ready(function() {
		$('#nilai_rata').inputmask();
	});
</script>
<script> 
	$(document).ready(function() {
		$('#nilai_skala').inputmask();
	});
</script>
@endsection

