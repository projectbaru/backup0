@extends('layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8"><br>
            Roles dan Perusahaan
            <hr>
            @include('sweetalert::alert')
            <div>
                <form method="POST" action="{{ route('login') }}" style="text-align: left;">
                    @csrf
                    <div class="row mb-3">
                        <!-- <label for="email" class="col-md-4 col-form-label">{{ __('Email Address') }}</label> -->
                        <div class="col-md-6">
                            <select style="font-size:13px;" class="form-select" aria-label="Default select example" name="nama_roles">
                                <!-- <option selected>Administrator</option> -->
                                @foreach ($roles as $data)
                                <option value="{{$data->list}}">{{$data->list}}</option>
                                @endforeach
                            </select><br>
                            <select style="font-size:13px;" class="form-select" aria-label="Default select example" name="nama_perusahaan">
                                @foreach ($np as $npd)
                                <option value="{{$npd->nama_perusahaan}}">{{$npd->nama_perusahaan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row mb-0">
                        <div class="col-md-12 offset-md-4">
                            <a type="submit" class="btn btn-primary" href="{{route('dashboard')}}" style="font-size:13px;">
                                {{ __('Next') }}
                                <i class="fa-solid fa-arrow-right"></i>
                            </a>

                            <!-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
@endsection
