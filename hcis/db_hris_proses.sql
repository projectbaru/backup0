-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Jun 2023 pada 06.16
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hris_proses`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_desc_pekerjaan`
--

CREATE TABLE `detail_desc_pekerjaan` (
  `id` int(11) NOT NULL,
  `desc_id` int(11) DEFAULT NULL,
  `kode_posisi` varchar(50) DEFAULT NULL,
  `nama_posisi` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(250) DEFAULT NULL,
  `pdca` varchar(50) DEFAULT NULL,
  `bsc` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `districts`
--

CREATE TABLE `districts` (
  `dis_id` int(11) NOT NULL,
  `dis_name` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `header`
--

CREATE TABLE `header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) NOT NULL,
  `columns` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `header`
--

INSERT INTO `header` (`id`, `key`, `columns`, `created_at`, `updated_at`) VALUES
(1, 'table_perusahaan', '[\"nama\", \"visi\", \"misi\",\"aksi\"]', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2023_05_30_070713_create_header_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `postalcode`
--

CREATE TABLE `postalcode` (
  `postal_id` int(10) UNSIGNED NOT NULL,
  `subdis_id` int(11) DEFAULT NULL,
  `dis_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE `provinces` (
  `prov_id` int(11) NOT NULL,
  `prov_name` varchar(255) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_analisa_hasil_interview`
--

CREATE TABLE `rc_analisa_hasil_interview` (
  `id_analisa_hasil_interview` int(11) DEFAULT NULL,
  `kode_analisa_hasil_interview` varchar(50) DEFAULT NULL,
  `kode_wawancara_kandidat` varchar(50) DEFAULT NULL,
  `kode_cv_pelamar` varchar(50) DEFAULT NULL,
  `nama_pelamar` varchar(50) DEFAULT NULL,
  `posisi_yang_dilamar` varchar(50) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `tanggal_interview` varchar(50) DEFAULT NULL,
  `status_marketing` varchar(50) DEFAULT NULL,
  `status_form` varchar(50) DEFAULT NULL,
  `waktu_kirim_form` datetime DEFAULT NULL,
  `nama_user_input` varchar(50) DEFAULT NULL,
  `tanggal_input` date DEFAULT NULL,
  `user_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `user_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_applicant_target_candidat_buffer`
--

CREATE TABLE `rc_applicant_target_candidat_buffer` (
  `id_applicant_target` int(11) DEFAULT NULL,
  `kode_cabang_kantor` varchar(100) DEFAULT NULL,
  `nama_cabang_kantor` varchar(100) DEFAULT NULL,
  `tahun_target` int(11) DEFAULT NULL,
  `pengguna_input` varchar(100) DEFAULT NULL,
  `waktu_input` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_bahasa_asing`
--

CREATE TABLE `rc_bahasa_asing` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_bahasa_asing` int(11) DEFAULT NULL,
  `kode_bahasa_asing_pelamar` varchar(50) DEFAULT NULL,
  `bahasa` text DEFAULT NULL,
  `mendengar` varchar(50) DEFAULT NULL,
  `berbicara` varchar(50) DEFAULT NULL,
  `menulis` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_budgeting_recruitment`
--

CREATE TABLE `rc_budgeting_recruitment` (
  `id_budgeting_recruitment` int(11) DEFAULT NULL,
  `kode_budgeting_recruitment` varchar(100) DEFAULT NULL,
  `tahun_budgeting_recruitment` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `user_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `user_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_data_budgeting_recruitment`
--

CREATE TABLE `rc_data_budgeting_recruitment` (
  `id_data_budgeting_recruitment` int(11) DEFAULT NULL,
  `kode_budgeting_recruitment` varchar(100) DEFAULT NULL,
  `cost_center` varchar(100) DEFAULT NULL,
  `semester_budgeting` varchar(100) DEFAULT NULL,
  `bulan_budgeting` varchar(100) DEFAULT NULL,
  `budgeting_year` int(11) DEFAULT NULL,
  `actual` int(11) DEFAULT NULL,
  `actual_persen_month` int(11) DEFAULT NULL,
  `actual_persen_kum` int(11) DEFAULT NULL,
  `quarter` varchar(100) DEFAULT NULL,
  `actual_quarter` int(11) DEFAULT NULL,
  `remaining_budget` int(11) DEFAULT NULL,
  `actual_persen_kum_quarter` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_data_cv_pelamar`
--

CREATE TABLE `rc_data_cv_pelamar` (
  `id_cv_pelamar` int(11) NOT NULL,
  `kode_cv_pelamar` varchar(100) DEFAULT NULL,
  `jabatan_lowongan` varchar(100) DEFAULT NULL,
  `tingkat_pekerjaan` varchar(100) DEFAULT NULL COMMENT '1. Staff 2.Middle 3.Senior',
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `resume_cv` varchar(100) DEFAULT NULL,
  `nama_lengkap` tinytext DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_hp` int(11) DEFAULT NULL,
  `lokasi_pelamar` varchar(100) DEFAULT NULL,
  `pengalaman_kerja` varchar(100) DEFAULT NULL COMMENT '1.berpengalaman 2.lulusan baru',
  `tahun_pengalaman_kerja` int(11) DEFAULT NULL,
  `bulan_pengalaman_kerja` int(11) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(100) DEFAULT NULL COMMENT '1.Laki-laki 2.Perempuan',
  `pendidikan_tertinggi` varchar(100) DEFAULT NULL COMMENT '1. SMK/Sederajat 2.D1 3.D2 4.D3 5.S1 6.S2 7.S3',
  `nama_sekolah` varchar(100) DEFAULT NULL,
  `program_studi_jurusan` varchar(100) DEFAULT NULL,
  `tanggal_wisuda` date DEFAULT NULL,
  `nilai_rata_rata` int(11) DEFAULT NULL,
  `nilai_skala` int(11) DEFAULT NULL,
  `informasi_lowongan` varchar(100) DEFAULT NULL COMMENT '1.Web SRU 2.Job Street 3.Karyawan SRU 4.Lainnya',
  `keterangan_informasi_pilihan` varchar(100) DEFAULT NULL,
  `pesan_pelamar` varchar(100) DEFAULT NULL,
  `tanggal_melamar` date DEFAULT NULL,
  `status_lamaran` varchar(100) DEFAULT NULL COMMENT '1.Belum Diperiksa 2.Sedang Dipertimbangkan 3.Lulus 4.Tidak Lulus',
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rc_data_cv_pelamar`
--

INSERT INTO `rc_data_cv_pelamar` (`id_cv_pelamar`, `kode_cv_pelamar`, `jabatan_lowongan`, `tingkat_pekerjaan`, `lokasi_kerja`, `resume_cv`, `nama_lengkap`, `email`, `no_hp`, `lokasi_pelamar`, `pengalaman_kerja`, `tahun_pengalaman_kerja`, `bulan_pengalaman_kerja`, `tanggal_lahir`, `jenis_kelamin`, `pendidikan_tertinggi`, `nama_sekolah`, `program_studi_jurusan`, `tanggal_wisuda`, `nilai_rata_rata`, `nilai_skala`, `informasi_lowongan`, `keterangan_informasi_pilihan`, `pesan_pelamar`, `tanggal_melamar`, `status_lamaran`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`) VALUES
(1, NULL, 'BE', 'middle', 'abc', 'C:\\xampp\\tmp\\phpE104.tmp', 'abc', 'admin@gmail.com', 897867, 'baru', 'Berpengalaman', 232, 32, '2022-11-23', 'Laki-laki', 'SMK/Sederajat', NULL, NULL, '2022-11-24', 365, 400, NULL, 'baru', 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, 'BE', 'middle', 'abc', 'C:\\xampp\\tmp\\php8DBA.tmp', 'abc', 'admin@gmail.com', 897867, 'baru', 'Berpengalaman', 232, 32, '2022-11-23', 'Laki-laki', 'SMK/Sederajat', 'baru', 'baru', '2022-11-24', 365, 400, NULL, 'baru', 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, 'data1', 'middle', 'abc', NULL, 'dani', 'admin@gmail.com', 8989898, 'dani', 'Berpengalaman', 456, 43, '2022-11-23', 'Laki-laki', 'D3', 'baru', 'baru', '2022-11-23', 365, 34, NULL, NULL, 'ds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, 'data1', 'middle', 'abc', NULL, 'dani', 'admin@gmail.com', 8989898, 'dani', 'Berpengalaman', 456, 43, '2022-11-23', 'Laki-laki', 'D3', 'baru', 'baru', '2022-11-23', 365, 34, NULL, 'lainnya', 'ds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, 'data1', 'middle', 'abc', NULL, 'dani', 'dany@gmail.com', 8989898, 'baru', 'Berpengalaman', 456, 2, '2022-11-30', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-23', 365, 34, NULL, 'fd', 'sf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, 'data1', 'middle', 'abc', NULL, 'dani', 'dany@gmail.com', 8989898, 'baru', 'Berpengalaman', 456, 2, '2022-11-30', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-23', 365, 34, NULL, 'akulaky', 'sf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, 'C:\\xampp\\tmp\\phpD371.tmp', 'dani', 'admin@gmail.com', 434, 'baru', 'Berpengalaman', 43, 43, '2022-11-24', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-22', 43, 4, NULL, 'ds', 'ds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, 'C:\\xampp\\tmp\\php2DD8.tmp', 'dani', 'admin@gmail.com', 434, 'jakarta', 'Lulusan Baru', 12, 12, '2022-11-22', 'Laki-laki', 'D1', 'baru', 'baru', '2022-12-02', 4, 3, 'Lainnya', '45', 'apaan tuh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, '1668926710.', 'da', 'dany@gmail.com', 43434, 'baru', 'Berpengalaman', 45, 45, '2022-11-22', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-23', 45, 4, 'Karyawan SRU', NULL, 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, '1668926887.', 'dani', 'dani@gmal.com', 343, 'jakarta', 'Berpengalaman', 45, 12, '2022-11-23', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-23', 45, 45, 'Karyawan SRU', NULL, 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, '1668927103.WhatsApp Image 2022-11-16 at 22.09.07.jpeg', 'dani putra', 'admin@gmail.com', 43434, 'jakarta 2', 'Lulusan Baru', 32, 23, '2022-11-30', 'Laki-laki', 'D2', 'baru', 'baru', '2022-11-23', 45, 2, 'Job Street', NULL, 'dimana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, 'BE', 'staff', 'null', '1670251402.', 'dani', 'admin@gmail.com', 548989, 'baru', 'Berpengalaman', 456, 54, '2022-11-23', 'Laki-laki', 'D1', 'baru', 'baru', '2022-11-24', 45, 45, NULL, NULL, 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, 'data1', 'senior', '[\"surabaya\",\"jakarta\"]', '1670247513.1668930145.WhatsApp Image 2022-11-16 at 22.09.07.jpeg', 'dani', 'admin@gmail.com', 8989, 'jakarta', 'Berpengalaman', 456, 45, '2022-12-08', 'Laki-laki', 'D3', 'baru', 'baru', '2022-12-22', 43, 43, 'Lainnya', '43', '34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, 'jabatan baru saja', 'senior', '[\"surabaya\",\"jakarta\"]', '1670827540.WhatsApp Image 2022-11-20 at 17.22.44.jpeg', 'nama lengkap', 'admin@gmail.com', 898931231, 'jakarta', 'Berpengalaman', 26, 1, '2022-12-13', 'Laki-laki', 'SMK/Sederajat', 'univ', 'da', '2022-12-29', 65, 2, 'Lainnya', 're', 'gd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, 'null', '1671023342.WhatsApp Image 2022-11-20 at 17.22.44.jpeg', 'da', 'danyputrahermawan95@gmail.com', 42343, 'daf', 'Berpengalaman', 10, 12, '2022-12-15', 'Laki-laki', 'D2', 'nama sekolah', 'program studi', '2022-12-23', 43, 87, 'Job Street', NULL, 'da', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_data_jadwal_wawancara_kandidat`
--

CREATE TABLE `rc_data_jadwal_wawancara_kandidat` (
  `id_data_wawancara_kandidat` int(11) DEFAULT NULL,
  `id_data_jadwal_wawancara_kandidat` int(11) DEFAULT NULL,
  `urutan_interview` int(11) DEFAULT NULL,
  `tanggal_interview` date DEFAULT NULL,
  `nama_pewawancara` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_data_lowongan_kerja`
--

CREATE TABLE `rc_data_lowongan_kerja` (
  `id_lowongan_kerja` int(11) NOT NULL,
  `kode_kategori_lowongan_kerja` varchar(50) DEFAULT NULL,
  `nomer_dokumen_fptk` varchar(50) DEFAULT NULL,
  `kategori_pekerjaan` varchar(50) DEFAULT NULL,
  `jabatan_lowongan` varchar(50) DEFAULT NULL,
  `lokasi_kerja` varchar(50) DEFAULT NULL,
  `tingkat_kerjaan` varchar(50) DEFAULT NULL,
  `pengalaman_kerja` varchar(50) DEFAULT NULL,
  `status_kepegawaian` varchar(50) DEFAULT NULL,
  `minimal_pendidikan` varchar(50) DEFAULT NULL,
  `peran_kerja` longtext DEFAULT NULL,
  `deskripsi_pekerjaan` longtext DEFAULT NULL,
  `syarat_pengalaman` longtext DEFAULT NULL,
  `syarat_kemampuan` longtext DEFAULT NULL,
  `syarat_lainnya` longtext DEFAULT NULL,
  `jumlah_karyawan` varchar(50) DEFAULT NULL,
  `tanggal_upload` varchar(50) DEFAULT NULL,
  `status_lowongan` varchar(50) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` datetime DEFAULT NULL,
  `tanggal_selesai_efektif` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` varchar(50) DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_data_wawancara_kandidat`
--

CREATE TABLE `rc_data_wawancara_kandidat` (
  `id_penyiapan_data_wawancara_kandidat` int(11) DEFAULT NULL,
  `id_data_wawancara_kandidat` int(11) DEFAULT NULL,
  `kode_data_wawancara_kandidat` varchar(100) DEFAULT NULL,
  `kode_cv_pelamar` varchar(100) DEFAULT NULL,
  `kode_process_recruitment_kandidat` varchar(100) DEFAULT NULL,
  `nama_kandidat` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `hasil_konfirmasi` varchar(100) DEFAULT NULL,
  `jam_wawancara` time DEFAULT NULL,
  `nama_atasan` varchar(100) DEFAULT NULL,
  `date_on_join` date DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `status_hasil_interview` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `waktu_input` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_detail_fulfillment_rate`
--

CREATE TABLE `rc_detail_fulfillment_rate` (
  `id_detail_fulfillment_rate` int(11) DEFAULT NULL,
  `kode_departemen` varchar(100) DEFAULT NULL,
  `nama_departemen` varchar(100) DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL,
  `cabang_kantor` varchar(100) DEFAULT NULL,
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `level` varchar(100) DEFAULT NULL,
  `mp_awal_disetujui` int(11) DEFAULT NULL,
  `mp_perubahan_tambah` int(11) DEFAULT NULL,
  `mp_perubahan_pengurangan` int(11) DEFAULT NULL,
  `mp_akhir` int(11) DEFAULT NULL,
  `mp_saat_ini` int(11) DEFAULT NULL,
  `vacant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_detail_process_recruitment`
--

CREATE TABLE `rc_detail_process_recruitment` (
  `id_step_process_recruitment` int(11) DEFAULT NULL,
  `id_detail_step_process_recruitment` varchar(50) DEFAULT NULL,
  `nama_step` varchar(50) DEFAULT NULL,
  `no_urut` varchar(50) DEFAULT NULL,
  `target_tanggal` varchar(50) DEFAULT NULL,
  `tanggal_aktual` date DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_detail_step_process_recruitment`
--

CREATE TABLE `rc_detail_step_process_recruitment` (
  `id_detail_step_process_recruitment` bigint(20) NOT NULL,
  `kode_step_process_recruitment` int(11) DEFAULT NULL,
  `nama_step` varchar(100) DEFAULT NULL,
  `no_urut` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rc_detail_step_process_recruitment`
--

INSERT INTO `rc_detail_step_process_recruitment` (`id_detail_step_process_recruitment`, `kode_step_process_recruitment`, `nama_step`, `no_urut`, `keterangan`) VALUES
(25, 24, 'CV Review', 1, 'barr'),
(26, 24, 'HR Review', 2, 'jbhbhjbjhbjhbjhbjhbj'),
(29, 27, 'd', 3, 'da'),
(30, 28, 'fa', 23, 'fsd'),
(31, 29, 'nama', 32, 'fdsaf'),
(32, 29, 'fdsf', 4, 'dsds'),
(33, 30, 'fas', 32, 'gsfdg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_dimensi_urain_interview`
--

CREATE TABLE `rc_dimensi_urain_interview` (
  `id_dimensi_urain_interview` int(11) DEFAULT NULL,
  `dimensi_urain` varchar(50) DEFAULT NULL,
  `jenis_dimensi_urain` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_divisi_applicant_target_candidat_buffer`
--

CREATE TABLE `rc_divisi_applicant_target_candidat_buffer` (
  `id_applicant_target` int(11) DEFAULT NULL,
  `id_divisi_applicant_target` int(11) DEFAULT NULL,
  `nama_divisi` varchar(100) DEFAULT NULL,
  `tahun_total_vacant` int(11) DEFAULT NULL,
  `rata_rata_perbulan` int(11) DEFAULT NULL,
  `tahun_buffer_candidate` int(11) DEFAULT NULL,
  `target_buffer_candidate` int(11) DEFAULT NULL,
  `tahun_target_buffer_quarter` int(11) DEFAULT NULL,
  `tahun_buffer_quarter` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_formulir_permintaan_tenaga_kerja`
--

CREATE TABLE `rc_formulir_permintaan_tenaga_kerja` (
  `id_fptk` int(11) NOT NULL,
  `no_dokumen` varchar(100) DEFAULT NULL,
  `nik_pemohon` varchar(10) DEFAULT NULL,
  `nama_pemohon` varchar(100) DEFAULT NULL,
  `jabatan_pemohon` varchar(100) DEFAULT NULL,
  `departemen_pemohon` varchar(100) DEFAULT NULL,
  `penggantian_tenaga_kerja` varchar(100) DEFAULT NULL,
  `penambahan_tenaga_kerja` varchar(100) DEFAULT NULL,
  `head_hunter` varchar(100) DEFAULT NULL,
  `status_kepegawaian` varchar(100) DEFAULT NULL,
  `kontrak_selama` varchar(100) DEFAULT NULL,
  `magang_selama` varchar(100) DEFAULT NULL,
  `jabatan_kepegawaian` varchar(100) DEFAULT NULL,
  `tingkat_kepegawaian` varchar(100) DEFAULT NULL,
  `tanggal_mulai_bekerja` date DEFAULT NULL,
  `jumlah_kebutuhan` varchar(100) DEFAULT NULL,
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `alasan_penambahan_tenaga_kerja` varchar(100) DEFAULT NULL,
  `gaji` varchar(100) DEFAULT NULL,
  `tunjangan_kesehatan` varchar(100) DEFAULT NULL,
  `tunjangan_transportasi` varchar(100) DEFAULT NULL,
  `tunjangan_komunikasi` varchar(100) DEFAULT NULL,
  `hari_kerja` varchar(100) DEFAULT NULL,
  `jam_kerja` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(100) DEFAULT NULL,
  `usia_minimal` varchar(100) DEFAULT NULL,
  `usia_maksimal` varchar(100) DEFAULT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `pengalaman` varchar(100) DEFAULT NULL,
  `kemampuan_layanan` varchar(100) DEFAULT NULL,
  `lampiran_file` varchar(100) DEFAULT NULL,
  `jabatan_penyetuju` varchar(100) DEFAULT NULL,
  `nama_penyetuju` varchar(100) DEFAULT NULL,
  `ttd_penyetuju` varchar(100) DEFAULT NULL,
  `nama_penganalisa` varchar(100) DEFAULT NULL,
  `hasil_penyetujuan_hr` varchar(100) DEFAULT NULL,
  `ttd_hr` varchar(100) DEFAULT NULL,
  `analisa_hrd` varchar(100) DEFAULT NULL,
  `ttd_pemohon` varchar(100) DEFAULT NULL,
  `nama_atasan_pemohon` varchar(100) DEFAULT NULL,
  `ttd_atasan_pemohon` varchar(100) DEFAULT NULL,
  `jabatan_atasan_pemohon` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `nik_input_user` varchar(100) DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` varchar(100) DEFAULT NULL,
  `penguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` varchar(100) DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` varchar(100) DEFAULT NULL,
  `dokumen_terakhir` varchar(100) DEFAULT NULL,
  `tanggal_upload_dokumen_terakhir` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rc_formulir_permintaan_tenaga_kerja`
--

INSERT INTO `rc_formulir_permintaan_tenaga_kerja` (`id_fptk`, `no_dokumen`, `nik_pemohon`, `nama_pemohon`, `jabatan_pemohon`, `departemen_pemohon`, `penggantian_tenaga_kerja`, `penambahan_tenaga_kerja`, `head_hunter`, `status_kepegawaian`, `kontrak_selama`, `magang_selama`, `jabatan_kepegawaian`, `tingkat_kepegawaian`, `tanggal_mulai_bekerja`, `jumlah_kebutuhan`, `lokasi_kerja`, `alasan_penambahan_tenaga_kerja`, `gaji`, `tunjangan_kesehatan`, `tunjangan_transportasi`, `tunjangan_komunikasi`, `hari_kerja`, `jam_kerja`, `jenis_kelamin`, `usia_minimal`, `usia_maksimal`, `pendidikan`, `pengalaman`, `kemampuan_layanan`, `lampiran_file`, `jabatan_penyetuju`, `nama_penyetuju`, `ttd_penyetuju`, `nama_penganalisa`, `hasil_penyetujuan_hr`, `ttd_hr`, `analisa_hrd`, `ttd_pemohon`, `nama_atasan_pemohon`, `ttd_atasan_pemohon`, `jabatan_atasan_pemohon`, `status_rekaman`, `nik_input_user`, `nama_user_input`, `pengguna_masuk`, `waktu_masuk`, `penguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`, `dokumen_terakhir`, `tanggal_upload_dokumen_terakhir`) VALUES
(2, 'da', NULL, 'da', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_form_data_pelamar`
--

CREATE TABLE `rc_form_data_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `kode_cv_pelamar` int(11) DEFAULT NULL,
  `kode_data_pelamar` varchar(100) DEFAULT NULL,
  `posisi_lamaran` varchar(100) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `nama_panggilan` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama` varchar(100) DEFAULT NULL COMMENT '1. Islam 2. Kristen 3. Buddha 4. Hindu 5. Kong Hu Cu',
  `kebangsaan` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(100) DEFAULT NULL COMMENT '1. Laki-laki 2.Perempuan',
  `berat_badan` int(11) DEFAULT NULL,
  `tinggi_badan` int(11) DEFAULT NULL,
  `no_ktp` int(11) DEFAULT NULL,
  `email_pribadi` varchar(100) DEFAULT NULL,
  `alamat_sosmed` varchar(100) DEFAULT NULL,
  `kategori_sim` varchar(100) DEFAULT NULL COMMENT '0. None, 1. SIM A, 2. SIM B, 3. SIM B1, 4. SIM B2, 5. SIM C, 6. SIM D',
  `no_sim` int(11) DEFAULT NULL,
  `no_hp` int(11) DEFAULT NULL,
  `no_telepon` int(11) DEFAULT NULL,
  `status_kendaraan` varchar(100) DEFAULT NULL COMMENT '1. Milik Sendiri, 2. Orangtua, 3. Kantor, 4.Lainnya',
  `jenis_merk_kendaraan` varchar(100) DEFAULT NULL,
  `tempat_tinggal` varchar(100) DEFAULT NULL COMMENT '1. Milik Sendiri, 2. Ikut Orangtua, 3. Kost/Kontrak',
  `status_perkawinan` varchar(100) DEFAULT NULL COMMENT '1. Menikah, 2. Janda/Duda, 3. Tidak Menikah',
  `alamat_didalam_kota` varchar(100) DEFAULT NULL,
  `kode_pos_1` int(11) DEFAULT NULL,
  `alamat_diluar_kota` varchar(500) DEFAULT NULL,
  `kode_pos_2` int(11) DEFAULT NULL,
  `pas_foto` varchar(1000) DEFAULT NULL,
  `hoby_dan_kegiatan` varchar(1000) DEFAULT NULL,
  `kegiatan_membaca` varchar(100) DEFAULT NULL,
  `surat_kabar` varchar(100) DEFAULT NULL,
  `majalah` varchar(100) DEFAULT NULL,
  `pokok_yang_dibaca` varchar(100) DEFAULT NULL,
  `tugas_jabatan_terakhir` varchar(100) DEFAULT NULL,
  `tanggal_lamar` date DEFAULT NULL,
  `gambar_denah` varchar(100) DEFAULT NULL,
  `tempat_ttd` varchar(100) DEFAULT NULL,
  `tanda_tangan` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_form_latar_belakang_pendidikan`
--

CREATE TABLE `rc_form_latar_belakang_pendidikan` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pendidikan_pelamar` int(11) NOT NULL,
  `kode_pendidikan_pelamar` varchar(50) DEFAULT NULL,
  `tingkat_pendidikan` varchar(50) DEFAULT NULL,
  `nama_sekolah` text DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `tempat_kota` varchar(50) DEFAULT NULL,
  `tahun_masuk` int(11) DEFAULT NULL,
  `tahun_keluar` int(11) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `status_kelulusan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_fulfillment_rate`
--

CREATE TABLE `rc_fulfillment_rate` (
  `id_fulfillment_rate` int(11) DEFAULT NULL,
  `kode_departemen` varchar(100) DEFAULT NULL,
  `nama_departemen` varchar(100) DEFAULT NULL,
  `total_mp_akhir` int(11) DEFAULT NULL,
  `total_vacant` int(11) DEFAULT NULL,
  `fulfillment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_jawaban_esai`
--

CREATE TABLE `rc_jawaban_esai` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pertanyaan_pilihan_esai` int(11) DEFAULT NULL,
  `jawaban` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_jawaban_marston_model`
--

CREATE TABLE `rc_jawaban_marston_model` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pertanyaan_marston_model` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `jawaban` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_jawaban_pilihan`
--

CREATE TABLE `rc_jawaban_pilihan` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pertanyaan_pilihan_esai` int(11) DEFAULT NULL,
  `jawaban` varchar(50) DEFAULT NULL,
  `penjelasan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_keluarga_pelamar`
--

CREATE TABLE `rc_keluarga_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_susunya_keluarga _pelamar` int(11) DEFAULT NULL,
  `kode_susunan_keluarga_pelamar` varchar(50) DEFAULT NULL,
  `hubungan_keluarga` varchar(50) DEFAULT NULL,
  `nama_keluarga` text DEFAULT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `usia_tahun_lahir` varchar(50) DEFAULT NULL,
  `pendidikan_terakhir` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `keterangan_susunan_keluarga` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_kontak_darurat`
--

CREATE TABLE `rc_kontak_darurat` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_kontak_darurat` int(11) DEFAULT NULL,
  `kode_kontak_darurat` varchar(100) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `alamat_telpon` text DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `hubungan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_kontak_refrensi_pelamar`
--

CREATE TABLE `rc_kontak_refrensi_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_kontak_refrensi_pelamar` int(11) DEFAULT NULL,
  `kode_kontak_refrensi_pelamar` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat_telpon` text DEFAULT NULL,
  `pekerjaan` text DEFAULT NULL,
  `hubungan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_kpi_hr_recruitment`
--

CREATE TABLE `rc_kpi_hr_recruitment` (
  `id_kpi_hr_recruitment` int(11) DEFAULT NULL,
  `id_pencapaian_kpi_hr_recruitment` varchar(50) DEFAULT NULL,
  `nama_kpi` varchar(50) DEFAULT NULL,
  `polarization` varchar(50) DEFAULT NULL,
  `oum` varchar(50) DEFAULT NULL,
  `weight` varchar(50) DEFAULT NULL,
  `definition_1` varchar(50) DEFAULT NULL,
  `definition_2` varchar(50) DEFAULT NULL,
  `output` varchar(50) DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL,
  `manager` varchar(50) DEFAULT NULL,
  `pic` varchar(50) DEFAULT NULL,
  `staff` varchar(50) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `nama_user_input` varchar(50) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `ubah_user` varchar(50) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `user_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_kpi_hr_recruitment_pencapaian_bulanan`
--

CREATE TABLE `rc_kpi_hr_recruitment_pencapaian_bulanan` (
  `id_bulan_pencapaian_kpi_hr_recruitment` int(11) DEFAULT NULL,
  `id_kpi_hr_recruitment` int(11) DEFAULT NULL,
  `bulan` varchar(100) DEFAULT NULL,
  `target_perbulan` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `ach` int(11) DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `user_ubah` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `user_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_latar_belakang_pendidikan`
--

CREATE TABLE `rc_latar_belakang_pendidikan` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pendidikan_pelamar` int(11) DEFAULT NULL,
  `kode_pendidikan_pelamar` varchar(50) DEFAULT NULL,
  `tingkat_pendidikan` varchar(50) DEFAULT NULL,
  `nama_sekolah` text DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `tahun_masuk` int(11) DEFAULT NULL,
  `tahun_keluar` int(11) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `status _kelulusan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_lowongan_kerja`
--

CREATE TABLE `rc_lowongan_kerja` (
  `id_lowongan_kerja` int(11) DEFAULT NULL,
  `kode-kategori_lowongan_kerja` varchar(50) DEFAULT NULL,
  `nomer_dokumen_fptk` varchar(50) DEFAULT NULL,
  `kategori_pekerjaan` varchar(50) DEFAULT NULL,
  `jabatan_lowongan` varchar(50) DEFAULT NULL,
  `lokasi_kerja` varchar(50) DEFAULT NULL,
  `tingkat_kerjaan` varchar(50) DEFAULT NULL,
  `pengalaman_kerja` varchar(50) DEFAULT NULL,
  `status_kepegawaian` varchar(50) DEFAULT NULL,
  `minimaal_pendidikan` varchar(50) DEFAULT NULL,
  `peran_kerja` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(50) DEFAULT NULL,
  `syarat_pengalaman` varchar(50) DEFAULT NULL,
  `syarat_kemampuan` varchar(50) DEFAULT NULL,
  `syarat_lainnya` varchar(50) DEFAULT NULL,
  `jumlah_karyawan` varchar(50) DEFAULT NULL,
  `tanggal_upload` varchar(50) DEFAULT NULL,
  `status_lowongan` varchar(50) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` datetime DEFAULT NULL,
  `tanggal_selesai_efektif` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` varchar(50) DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_negatif_positif _pelamar`
--

CREATE TABLE `rc_negatif_positif _pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_positif_negatif_pelamar` int(11) DEFAULT NULL,
  `kategori` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_negatif_positif_pelamar`
--

CREATE TABLE `rc_negatif_positif_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_positif_negatif_pelamar` int(11) DEFAULT NULL,
  `kategori` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_ontime_fulfillment`
--

CREATE TABLE `rc_ontime_fulfillment` (
  `id_ontime_fulfillment` int(11) DEFAULT NULL,
  `nomor_dokumen_fptk` int(11) DEFAULT NULL,
  `nama_pemohon` varchar(100) DEFAULT NULL,
  `jabatan_pemohon` varchar(100) DEFAULT NULL,
  `departemen_pemohon` varchar(100) DEFAULT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `jumlah_permintaan` int(11) DEFAULT NULL,
  `karyawan_masuk` int(11) DEFAULT NULL,
  `jumlah_pemenuhan` int(11) DEFAULT NULL,
  `kekurangan` int(11) DEFAULT NULL,
  `penggantian_tenaga_kerja` varchar(100) DEFAULT NULL,
  `penambahan_tenaga_kerja` varchar(100) DEFAULT NULL,
  `tgl_user_input_fptk` date DEFAULT NULL,
  `tgl_approval_pic_fptk` date DEFAULT NULL,
  `tgl_atasan_pic_fptk` date DEFAULT NULL,
  `tgl_approval_gm_fptk` date DEFAULT NULL,
  `tanggal_masuk_fptk_hrd` date DEFAULT NULL,
  `tanggal_masuk_fptk_pimpinan` date DEFAULT NULL,
  `waktu_pengerjaan` int(11) DEFAULT NULL,
  `lead_time_un` int(11) DEFAULT NULL,
  `tanggal_target_pemenuhan` date DEFAULT NULL,
  `tanggal_masuk_karyawan` date DEFAULT NULL,
  `kode_process_recruitment` varchar(100) DEFAULT NULL,
  `kode_cv_pelamar` varchar(100) DEFAULT NULL,
  `nama_karyawan` varchar(100) DEFAULT NULL,
  `referensi` varchar(100) DEFAULT NULL,
  `no_rq` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `status_ach` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_organisasi_pelamar`
--

CREATE TABLE `rc_organisasi_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_organisasi_pelamar` int(11) DEFAULT NULL,
  `kode_organisasi_pelamar` varchar(50) DEFAULT NULL,
  `nama_organisasi` text DEFAULT NULL,
  `jenis_kegiatan` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `tahun` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_pelatihan_pelamar`
--

CREATE TABLE `rc_pelatihan_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_pelatihan_pelamar` int(11) DEFAULT NULL,
  `kode_pelatihan_pelamar` varchar(50) DEFAULT NULL,
  `bidang_pelatihan` text DEFAULT NULL,
  `penyelenggara` text DEFAULT NULL,
  `tempat_kota` varchar(50) DEFAULT NULL,
  `lama_pelatihan` varchar(50) DEFAULT NULL,
  `tahun_pelatihan` int(11) DEFAULT NULL,
  `dibiayai_oleh` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_pencapaian_kpi_hr_recruitment`
--

CREATE TABLE `rc_pencapaian_kpi_hr_recruitment` (
  `id_pencapaian_kpi_hr_recruitment` int(11) DEFAULT NULL,
  `nama_parameter` varchar(50) DEFAULT NULL,
  `tahun_pencapaian` varchar(50) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `nama_user_input` varchar(50) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `user_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `user_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_penganalisa_hasil_interview`
--

CREATE TABLE `rc_penganalisa_hasil_interview` (
  `id_penganalisa_hasil_interview` int(11) DEFAULT NULL,
  `id_analisa_hasil_interview` varchar(50) DEFAULT NULL,
  `nikk` varchar(50) DEFAULT NULL,
  `nama_karyawan` varchar(50) DEFAULT NULL,
  `keputusan` varchar(50) DEFAULT NULL,
  `komentar` varchar(50) DEFAULT NULL,
  `tanda_tangan` varchar(50) DEFAULT NULL,
  `waktu_tanda_tangan` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_penilaian_hasil_interview`
--

CREATE TABLE `rc_penilaian_hasil_interview` (
  `id_penilaian_hasil_interview` int(11) DEFAULT NULL,
  `id_analisa_hasil_interview` varchar(50) DEFAULT NULL,
  `id_penganalisa_hasil_interview` varchar(50) DEFAULT NULL,
  `id_dimensi_uraian_interview` varchar(50) DEFAULT NULL,
  `jawaban` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_penyiapan_data_wawancara_kandidat`
--

CREATE TABLE `rc_penyiapan_data_wawancara_kandidat` (
  `id_penyiapan_data_wawancara_kandidat` int(11) DEFAULT NULL,
  `disiapkan_oleh` varchar(100) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `diperiksa_oleh` varchar(100) DEFAULT NULL,
  `revisi` int(11) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `waktu_input` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_permintaan_tenaga_kerja`
--

CREATE TABLE `rc_permintaan_tenaga_kerja` (
  `id_fptk` int(11) DEFAULT NULL,
  `no_dokumn` varchar(100) DEFAULT NULL,
  `nik_pemohon` varchar(10) DEFAULT NULL,
  `nama_pemohon` varchar(100) DEFAULT NULL,
  `jabatan_pemohon` varchar(100) DEFAULT NULL,
  `departemen_pemohon` varchar(100) DEFAULT NULL,
  `penggantian_tenaga_kerja` varchar(100) DEFAULT NULL,
  `penambahan_tenaga_kerja` varchar(100) DEFAULT NULL,
  `head_hunter` varchar(100) DEFAULT NULL,
  `status_kepegawaian` varchar(100) DEFAULT NULL,
  `kontrak_selama` varchar(100) DEFAULT NULL,
  `magang_selama` varchar(100) DEFAULT NULL,
  `jabatan_kepegawaian` varchar(100) DEFAULT NULL,
  `tingkat_kepegawaian` varchar(100) DEFAULT NULL,
  `jumlah_kebutuhan` varchar(100) DEFAULT NULL,
  `tanggal_mulai_bekerja` date DEFAULT NULL,
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `alasan_penambahan_tenaga_kerja` varchar(100) DEFAULT NULL,
  `gaji` varchar(100) DEFAULT NULL,
  `tunjangan_kesehatan` varchar(100) DEFAULT NULL,
  `tunjangan_transportasi` varchar(100) DEFAULT NULL,
  `tunjangan_komunikasi` varchar(100) DEFAULT NULL,
  `hari_kerja` varchar(100) DEFAULT NULL,
  `jam_kerja` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(100) DEFAULT NULL,
  `usia_minimal` varchar(100) DEFAULT NULL,
  `usia_maksimal` varchar(100) DEFAULT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `pengalaman` varchar(100) DEFAULT NULL,
  `kemampuan_layanan` varchar(100) DEFAULT NULL,
  `lampiran_file` varchar(100) DEFAULT NULL,
  `jabatan_penyetuju` varchar(100) DEFAULT NULL,
  `nama_penyetuju` varchar(100) DEFAULT NULL,
  `ttd_penyetuju` varchar(100) DEFAULT NULL,
  `nama_penganalisa` varchar(100) DEFAULT NULL,
  `hasil_penyetujuan_hr` varchar(100) DEFAULT NULL,
  `ttd_hr` varchar(100) DEFAULT NULL,
  `analisa_hrd` varchar(100) DEFAULT NULL,
  `ttd_pemohon` varchar(100) DEFAULT NULL,
  `nama_atasan_pemohon` varchar(100) DEFAULT NULL,
  `ttd_atasan_pemohon` varchar(100) DEFAULT NULL,
  `jabatan_atasan_pemohon` varchar(100) DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `nik_input_user` varchar(100) DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` varchar(100) DEFAULT NULL,
  `penguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` varchar(100) DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` varchar(100) DEFAULT NULL,
  `dokumen_terakhir` varchar(100) DEFAULT NULL,
  `tanggal_upload_dokumen_terakhir` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_pertanyaan_marston_model`
--

CREATE TABLE `rc_pertanyaan_marston_model` (
  `id_pertanyaan_marston_model` int(11) DEFAULT NULL,
  `kode_pertanyaan_marston_model` int(11) DEFAULT NULL,
  `rston_model` varchar(100) DEFAULT NULL,
  `pertanyaan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_pertanyaan_pilihan_esai`
--

CREATE TABLE `rc_pertanyaan_pilihan_esai` (
  `id_pertanyaan_pilihan_sesai` int(11) DEFAULT NULL,
  `kode_pertanyaan_pilihan_esai` varchar(50) DEFAULT NULL,
  `pertanyaan` text DEFAULT NULL,
  `kategori_pertanyaan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_position_applicant_target_candidat_buffer`
--

CREATE TABLE `rc_position_applicant_target_candidat_buffer` (
  `id_divisi_applicant_target` int(11) DEFAULT NULL,
  `id_position_applicant_target` int(11) DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_process_recruitment`
--

CREATE TABLE `rc_process_recruitment` (
  `id_process_recruitment` int(11) DEFAULT NULL,
  `kode_process_recruitment` varchar(100) DEFAULT NULL,
  `no_dokumen` int(11) DEFAULT NULL COMMENT 'RCFORMULIRPERMINTAANTENAGAKERJA',
  `kode_cv_pelamar` varchar(100) DEFAULT NULL COMMENT 'RCDATACVPELAMAR',
  `kode_data_pelamar` varchar(100) DEFAULT NULL COMMENT 'RCDATAPERSONALPELAMAR',
  `nama_kandidat` varchar(100) DEFAULT NULL,
  `jabatan_dilamar` varchar(100) DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL COMMENT 'RCSTEPPROCESSRECRUITMENT',
  `id_step_process_recruitment` varchar(100) DEFAULT NULL COMMENT 'RCSTEPPROCESSRECRUITMENT',
  `status` text DEFAULT NULL,
  `nama_user_input` varchar(100) DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_ptk_detail`
--

CREATE TABLE `rc_ptk_detail` (
  `id_ptk` int(11) NOT NULL,
  `kode_ptk` varchar(100) DEFAULT NULL,
  `nama_ptk` varchar(100) DEFAULT NULL,
  `periode_ptk` int(11) DEFAULT NULL,
  `kode_grup_lokasi_kerja` varchar(1000) DEFAULT NULL,
  `nama_grup_lokasi_kerja` text DEFAULT NULL,
  `kode_lokasi_kerja` varchar(1000) DEFAULT NULL,
  `nama_lokasi_kerja` text DEFAULT NULL,
  `kode_jabatan` varchar(30) DEFAULT NULL,
  `tingkat_jabatan` text DEFAULT NULL,
  `kode_posisi` varchar(50) DEFAULT NULL,
  `nama_posisi` text DEFAULT NULL,
  `ptk_awal` int(11) DEFAULT NULL,
  `ptk_yang_digantikan` int(11) DEFAULT NULL,
  `ptk_tambahan` int(11) NOT NULL,
  `ptk_akhir` int(11) DEFAULT NULL,
  `tksi_perusahaan` int(11) DEFAULT NULL,
  `tksi_pengganti` int(11) DEFAULT NULL,
  `tksi_final` int(11) DEFAULT NULL,
  `selisih` int(11) DEFAULT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `tanggal_mulai_efektif` datetime DEFAULT NULL,
  `tanggal_selesai_efektif` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rc_ptk_detail`
--

INSERT INTO `rc_ptk_detail` (`id_ptk`, `kode_ptk`, `nama_ptk`, `periode_ptk`, `kode_grup_lokasi_kerja`, `nama_grup_lokasi_kerja`, `kode_lokasi_kerja`, `nama_lokasi_kerja`, `kode_jabatan`, `tingkat_jabatan`, `kode_posisi`, `nama_posisi`, `ptk_awal`, `ptk_yang_digantikan`, `ptk_tambahan`, `ptk_akhir`, `tksi_perusahaan`, `tksi_pengganti`, `tksi_final`, `selisih`, `tanggal_awal`, `tanggal_akhir`, `keterangan`, `status_rekaman`, `waktu_user_input`, `tanggal_mulai_efektif`, `tanggal_selesai_efektif`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`) VALUES
(1612, 'kode-123', 'abc', 202401, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1613, 'kode-123', 'abc', 202402, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1614, 'kode-123', 'abc', 202403, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1615, 'kode-123', 'abc', 202404, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1616, 'kode-123', 'abc', 202405, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1617, 'kode-123', 'abc', 202406, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1618, 'kode-123', 'abc', 202407, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1619, 'kode-123', 'abc', 202408, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1620, 'kode-123', 'abc', 202409, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1621, 'kode-123', 'abc', 202410, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1622, 'kode-123', 'abc', 202411, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1623, 'kode-123', 'abc', 202412, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1624, 'kode-124', 'abc', 202401, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1625, 'kode-124', 'abc', 202402, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1626, 'kode-124', 'abc', 202403, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1627, 'kode-124', 'abc', 202404, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1628, 'kode-124', 'abc', 202405, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1629, 'kode-124', 'abc', 202406, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1630, 'kode-124', 'abc', 202407, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1631, 'kode-124', 'abc', 202408, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1632, 'kode-124', 'abc', 202409, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1633, 'kode-124', 'abc', 202410, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1634, 'kode-124', 'abc', 202411, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1635, 'kode-124', 'abc', 202412, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1636, 'kode-123', 'abc', 202301, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1637, 'kode-123', 'abc', 202302, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1638, 'kode-123', 'abc', 202303, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1639, 'kode-123', 'abc', 202304, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1640, 'kode-123', 'abc', 202305, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1641, 'kode-123', 'abc', 202306, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1642, 'kode-123', 'abc', 202307, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1643, 'kode-123', 'abc', 202308, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1644, 'kode-123', 'abc', 202309, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1645, 'kode-123', 'abc', 202310, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1646, 'kode-123', 'abc', 202311, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1647, 'kode-123', 'abc', 202312, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1648, 'kode-124', 'abc', 202301, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1649, 'kode-124', 'abc', 202302, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1650, 'kode-124', 'abc', 202303, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1651, 'kode-124', 'abc', 202304, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1652, 'kode-124', 'abc', 202305, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1653, 'kode-124', 'abc', 202306, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1654, 'kode-124', 'abc', 202307, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1655, 'kode-124', 'abc', 202308, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1656, 'kode-124', 'abc', 202309, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1657, 'kode-124', 'abc', 202310, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1658, 'kode-124', 'abc', 202311, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1659, 'kode-124', 'abc', 202312, NULL, 'jakarta', NULL, NULL, NULL, NULL, NULL, 'GA', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_quarter_divisi_applicant_target_candidat_buffer`
--

CREATE TABLE `rc_quarter_divisi_applicant_target_candidat_buffer` (
  `id_position_applicant_target` int(11) DEFAULT NULL,
  `id_quarter_divisi_applicant_target` int(11) DEFAULT NULL,
  `quarter` varchar(100) DEFAULT NULL,
  `target_buffer` int(11) DEFAULT NULL,
  `actual_buffer` int(11) DEFAULT NULL,
  `achievement_quarter` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_riwayat_kerja_pelamar`
--

CREATE TABLE `rc_riwayat_kerja_pelamar` (
  `id_data_pelamar` int(11) DEFAULT NULL,
  `id_riwayat_kerja_pelamar` int(11) DEFAULT NULL,
  `kode_riwayat_kerja_pelamar` varchar(50) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `nama_atasan` varchar(50) DEFAULT NULL,
  `lama_bekerja` varchar(50) DEFAULT NULL,
  `gaji_terakhir` varchar(50) DEFAULT NULL,
  `jenis_usaha` varchar(50) DEFAULT NULL,
  `alasan_berhenti` text DEFAULT NULL,
  `jabatan_awal` varchar(50) DEFAULT NULL,
  `jabatan_akhir` varchar(50) DEFAULT NULL,
  `telp_perusahaan` varchar(50) DEFAULT NULL,
  `nama_direktur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_step_process_recruitment`
--

CREATE TABLE `rc_step_process_recruitment` (
  `id_step_process_recruitment` int(11) NOT NULL,
  `kode_step_process_recruitment` varchar(100) DEFAULT NULL,
  `nama_jabatan` varchar(255) NOT NULL,
  `status_rekaman` varchar(100) DEFAULT NULL COMMENT '0. Hapus, 1. Aktif, 2.Tidak Aktif',
  `nik_input_user` varchar(100) DEFAULT NULL COMMENT 'Lookup Ke Master Karyawan',
  `nama_user_input` varchar(100) DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rc_step_process_recruitment`
--

INSERT INTO `rc_step_process_recruitment` (`id_step_process_recruitment`, `kode_step_process_recruitment`, `nama_jabatan`, `status_rekaman`, `nik_input_user`, `nama_user_input`, `waktu_user_input`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`) VALUES
(24, NULL, 'MMMMM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, NULL, 'baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_susunan_keluarga_pelamar`
--

CREATE TABLE `rc_susunan_keluarga_pelamar` (
  `id_data_pelamar` int(10) NOT NULL,
  `id_susunan_keluarga _pelamar` int(10) NOT NULL,
  `kode_susunan_keluarga_pelamar` varchar(50) DEFAULT NULL,
  `hubungan_keluarga` varchar(50) DEFAULT NULL,
  `nama_keluarga` text DEFAULT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `usia_tahun_lahir` varchar(50) DEFAULT NULL,
  `pendidikan_terakhir` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `keterangan_susunan_keluarga` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rc_transaksi_ptk`
--

CREATE TABLE `rc_transaksi_ptk` (
  `id_transaksi_ptk` int(11) UNSIGNED NOT NULL,
  `nomor_permintaan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tipe_transaksi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nomor_referensi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tanggal_permintaan` date DEFAULT NULL,
  `tanggal_efektif_transaksi` date DEFAULT NULL,
  `periode_tujuan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `periode_asal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_ptk_asal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_ptk_asal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `total_ptk_asal` int(11) DEFAULT NULL,
  `kode_ptk_tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_ptk_tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `total_ptk_tujuan` int(11) DEFAULT NULL,
  `nama_posisi_tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_lokasi_kerja_tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_posisi_asal` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_lokasi_kerja_asal` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status_rekaman` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `waktu_user_input` datetime DEFAULT NULL,
  `pengguna_masuk` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `pengguna_ubah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `waktu_ubah` datetime DEFAULT NULL,
  `pengguna_hapus` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `waktu_hapus` datetime DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `flag` int(2) NOT NULL DEFAULT 0 COMMENT '0=Tambahan,1:Pergantian',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `rc_transaksi_ptk`
--

INSERT INTO `rc_transaksi_ptk` (`id_transaksi_ptk`, `nomor_permintaan`, `tipe_transaksi`, `nomor_referensi`, `tanggal_permintaan`, `tanggal_efektif_transaksi`, `periode_tujuan`, `periode_asal`, `kode_ptk_asal`, `nama_ptk_asal`, `total_ptk_asal`, `kode_ptk_tujuan`, `nama_ptk_tujuan`, `total_ptk_tujuan`, `nama_posisi_tujuan`, `nama_lokasi_kerja_tujuan`, `nama_posisi_asal`, `nama_lokasi_kerja_asal`, `deskripsi`, `keterangan`, `status_rekaman`, `waktu_user_input`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`, `tanggal_mulai_efektif`, `tanggal_selesai_efektif`, `flag`, `created_at`, `updated_at`) VALUES
(1, '08989423', NULL, '53453', '2022-11-24', '2022-11-24', NULL, NULL, NULL, 'da', NULL, NULL, 'ptk b', 43443, NULL, NULL, NULL, NULL, 'fsdf343', 'fsdf4343434', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-11-16', '2022-11-26', 0, '2022-11-13 10:34:53', '2022-11-13 12:05:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_cv`
--

CREATE TABLE `reftable_cv` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filter` longtext NOT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `kode_cv_pelamar` varchar(255) DEFAULT NULL,
  `minimal_pendidikan` varchar(255) DEFAULT NULL,
  `jabatan_lowongan` varchar(255) DEFAULT NULL,
  `lokasi_kerja` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `reftable_cv`
--

INSERT INTO `reftable_cv` (`id`, `user_id`, `filter`, `query_field`, `query_operator`, `query_value`, `active`, `kode_cv_pelamar`, `minimal_pendidikan`, `jabatan_lowongan`, `lokasi_kerja`, `created_at`, `updated_at`) VALUES
(1, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 8, '[\"no._hp\",\"tanggal_wisuda\",\"nilai_rata-rata\",\"skala\",\"informasi_lowongan\",\"resume\\/cv\",\"tanggal_melamar\",\"email\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 9, '[\"lokasi_saat_ini\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 9, '[\"pengalaman_kerja\",\"lokasi_saat_ini\"]', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 9, '[\"pengalaman_kerja\"]', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_dlo`
--

CREATE TABLE `reftable_dlo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kategori_lowongan_kerja` varchar(255) DEFAULT NULL,
  `nomer_dokumen_fptk` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_dpp`
--

CREATE TABLE `reftable_dpp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_fptk`
--

CREATE TABLE `reftable_fptk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `no_dokumen` varchar(255) DEFAULT NULL,
  `nama_pemohon` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_pro`
--

CREATE TABLE `reftable_pro` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_kandidat` varchar(255) DEFAULT NULL,
  `kode_process_recruitment` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_ptk`
--

CREATE TABLE `reftable_ptk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_ptk` varchar(255) DEFAULT NULL,
  `nama_ptk` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `reftable_ptk`
--

INSERT INTO `reftable_ptk` (`id`, `user_id`, `select`, `kode_ptk`, `nama_ptk`, `query_field`, `query_operator`, `query_value`, `active`) VALUES
(92, 9, '[\"kode_ptk\",\"nama_ptk\",\"periode_ptk\",\"tingkat_jabatan\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(93, 9, '[\"kode_ptk\",\"nama_ptk\",\"periode_ptk\",\"tingkat_jabatan\",\"ptk_awal\"]', NULL, NULL, '[null]', '[null]', '[null]', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reftable_tptk`
--

CREATE TABLE `reftable_tptk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nomor_permintaan` varchar(255) DEFAULT NULL,
  `tipe_transaksi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `reftable_tptk`
--

INSERT INTO `reftable_tptk` (`id`, `user_id`, `select`, `nomor_permintaan`, `tipe_transaksi`, `query_field`, `query_operator`, `query_value`, `active`) VALUES
(29, 9, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"kode_ptk_asal\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(30, 9, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"periode_tujuan\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(31, 9, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"tanggal_efektif_transaksi\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(32, 9, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"nama_ptk_asal\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(33, 9, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"kode_ptk_asal\"]', NULL, NULL, '[null]', '[null]', '[null]', 1),
(34, 8, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"tanggal_efektif_transaksi\"]', NULL, NULL, '[null]', '[null]', '[null]', 0),
(35, 8, '[\"nomor_permintaan\",\"nama_ptk_tujuan\",\"tanggal_efektif_transaksi\",\"kode_ptk_tujuan\"]', NULL, NULL, '[null]', '[null]', '[null]', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id_roles` int(11) NOT NULL,
  `list` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subdistricts`
--

CREATE TABLE `subdistricts` (
  `subdis_id` int(11) NOT NULL,
  `subdis_name` varchar(255) DEFAULT NULL,
  `dis_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Admin', 'admin@mannatthemes.com', NULL, '$2y$10$xuV62GbS9m5xgpm06edRBeMvihgVrNgacJnZqXEON80jnvWCk.bP6', NULL, NULL, NULL, NULL),
(8, 'dany', 'admin@gmail.com', NULL, '$2y$10$C0qL9stR4FrPMaD4gowxoe82NmryQZFkm7qS.dqOxallwVocMMUnO', NULL, '2022-11-06 03:46:00', '2022-11-06 03:46:00', NULL),
(9, 'admin@gmail.com', 'admin123@gmail.com', NULL, '$2y$10$69tDf3wXjhAZkyugfVBNvuiTSxRYeL94n7GHiTNsB0B8iMgU01GFi', NULL, '2022-11-12 19:04:10', '2022-11-12 19:04:10', NULL),
(10, 'nobu', 'admin33@gmail.com', NULL, '$2y$10$IcqgrkKWUINvwXvR8gCwPeVyfw1Yq/Oqw0Hnnv/hQiSupKgZcboDm', NULL, '2022-12-05 07:37:43', '2022-12-05 07:37:43', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsalamatperusahaan`
--

CREATE TABLE `wsalamatperusahaan` (
  `id_alamat` int(11) NOT NULL,
  `kode_alamat` varchar(20) DEFAULT NULL,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `status_alamat_untuk_spt` text DEFAULT NULL,
  `jenis_alamat` text DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `kota` tinytext DEFAULT NULL,
  `kode_pos` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `keterangan` longtext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `wsalamatperusahaan`
--

INSERT INTO `wsalamatperusahaan` (`id_alamat`, `kode_alamat`, `kode_perusahaan`, `nama_perusahaan`, `status_alamat_untuk_spt`, `jenis_alamat`, `alamat`, `kota`, `kode_pos`, `telepon`, `keterangan`, `status_rekaman`, `tanggal_mulai_efektif`, `tanggal_selesai_efektif`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`) VALUES
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan`
--

CREATE TABLE `wsdeskripsipekerjaan` (
  `id_deskripsi_pekerjaan` int(11) NOT NULL,
  `kode_deskripsi` text DEFAULT NULL,
  `nama_atasan_langsung` tinytext DEFAULT NULL,
  `nama_posisi` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `deskripsi_pekerjaan` longtext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` date DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `nomor_dokumen` varchar(50) DEFAULT NULL,
  `edisi` text DEFAULT NULL,
  `tanggal_edisi` date DEFAULT NULL,
  `nomor_revisi` text DEFAULT NULL,
  `tanggal_revisi` date DEFAULT NULL,
  `nama_jabatan` text DEFAULT NULL,
  `nama_karyawan` text DEFAULT NULL,
  `divisi` text DEFAULT NULL,
  `lokasi_kerja` text DEFAULT NULL,
  `dibuat_oleh` varchar(50) DEFAULT NULL,
  `diperiksa_oleh` text DEFAULT NULL,
  `disahkan_oleh` text DEFAULT NULL,
  `fungsi_jabatan` text DEFAULT NULL,
  `tanggung_jawab` longtext DEFAULT NULL,
  `departemen` text DEFAULT NULL,
  `kode_posisi` text DEFAULT NULL,
  `nama_lokasi_kerja` text DEFAULT NULL,
  `nama_pengawas` text DEFAULT NULL,
  `lingkup_aktivitas` text DEFAULT NULL,
  `id_logo_perusahaan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_bag`
--

CREATE TABLE `wsdeskripsipekerjaan_bag` (
  `id_deskripsi_pekerjaan_bag` int(11) NOT NULL,
  `kode_posisi` tinytext DEFAULT NULL,
  `nama_posisi` tinytext DEFAULT NULL,
  `deskripsi_pekerjaan` tinytext DEFAULT NULL,
  `pdca` tinytext DEFAULT NULL,
  `bsc` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--

CREATE TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `deskripsi_pekerjaan` text NOT NULL,
  `pdca` varchar(255) NOT NULL,
  `bsc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--

CREATE TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `kualifikasi_jabatan` text NOT NULL,
  `tipe_kualifikasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_tanggung_jawab`
--

CREATE TABLE `wsdeskripsipekerjaan_tanggung_jawab` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `tanggung_jawab` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_wewenang`
--

CREATE TABLE `wsdeskripsipekerjaan_wewenang` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `wewenang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgolongan`
--

CREATE TABLE `wsgolongan` (
  `id_golongan` int(11) NOT NULL,
  `kode_golongan` tinytext DEFAULT NULL,
  `nama_golongan` tinytext DEFAULT NULL,
  `tingkat_golongan` tinytext DEFAULT NULL,
  `urutan_golongan` tinytext DEFAULT NULL,
  `nama_perusahaan` longtext DEFAULT NULL,
  `tingkat_posisi` tinytext DEFAULT NULL,
  `jabatan` tinytext DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` varchar(50) DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgruplokasikerja`
--

CREATE TABLE `wsgruplokasikerja` (
  `id_grup_lokasi_kerja` int(11) NOT NULL,
  `kode_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `tipe_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `lokasi_kerja` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgruplokasikerja_deskripsi`
--

CREATE TABLE `wsgruplokasikerja_deskripsi` (
  `id_grup_lokasi_kerja` int(11) NOT NULL,
  `kode_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `tipe_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `lokasi_kerja` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsjabatan`
--

CREATE TABLE `wsjabatan` (
  `id_jabatan` int(11) NOT NULL,
  `kode_jabatan` tinytext DEFAULT NULL,
  `nama_jabatan` tinytext DEFAULT NULL,
  `tipe_jabatan` tinytext DEFAULT NULL,
  `kode_kelompok_jabatan` varchar(25) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(30) DEFAULT NULL,
  `grup_jabatan` varchar(30) DEFAULT NULL,
  `deskripsi_jabatan` longtext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `magang` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskantorcabang`
--

CREATE TABLE `wskantorcabang` (
  `id_kantor` int(11) NOT NULL,
  `kode_kantor` tinytext DEFAULT NULL,
  `nama_kantor` tinytext DEFAULT NULL,
  `kode_kelas` tinytext DEFAULT NULL,
  `tipe_kantor` tinytext DEFAULT NULL,
  `kode_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_lokasi_kerja` tinytext DEFAULT NULL,
  `nomor_npwp` tinytext DEFAULT NULL,
  `umk_ump` varchar(100) DEFAULT NULL,
  `ttd_spt` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskelascabang`
--

CREATE TABLE `wskelascabang` (
  `id_kelas_cabang` int(11) NOT NULL,
  `kode_kelas_cabang` tinytext DEFAULT NULL,
  `nama_kelas_cabang` tinytext DEFAULT NULL,
  `area_kelas_cabang` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskelompokjabatan`
--

CREATE TABLE `wskelompokjabatan` (
  `id_kelompok_jabatan` int(11) NOT NULL,
  `kode_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_kelompok_jabatan` tinytext DEFAULT NULL,
  `deskripsi_kelompok_jabatan` text DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `grup_jabatan` varchar(50) DEFAULT NULL,
  `tipe_jabatan` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wslokasikerja`
--

CREATE TABLE `wslokasikerja` (
  `id_lokasi_kerja` int(11) NOT NULL,
  `kode_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_grup_lokasi_kerja` varchar(100) DEFAULT NULL,
  `id_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `id_kantor` tinytext DEFAULT NULL,
  `zona_waktu` int(11) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `provinsi` int(11) DEFAULT NULL,
  `kecamatan` int(11) DEFAULT NULL,
  `kelurahan` int(11) DEFAULT NULL,
  `kabupaten_kota` int(11) DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `negara` tinytext DEFAULT NULL,
  `nomor_telepon` varchar(50) DEFAULT NULL,
  `email` tinytext DEFAULT NULL,
  `fax` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `wslokasikerja`
--

INSERT INTO `wslokasikerja` (`id_lokasi_kerja`, `kode_lokasi_kerja`, `nama_lokasi_kerja`, `nama_grup_lokasi_kerja`, `id_grup_lokasi_kerja`, `id_kantor`, `zona_waktu`, `alamat`, `provinsi`, `kecamatan`, `kelurahan`, `kabupaten_kota`, `kode_pos`, `negara`, `nomor_telepon`, `email`, `fax`, `keterangan`, `status_rekaman`, `tanggal_mulai_efektif`, `tanggal_selesai_efektif`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`) VALUES
(1, 'abc', 'surabaya', 'abc', 'abc', 'abc', NULL, 'abc', NULL, NULL, NULL, NULL, NULL, 'abc', NULL, 'abc', 'abc', 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'abc', 'jakarta', 'abc', 'abc', 'abc', NULL, 'abc', NULL, NULL, NULL, NULL, NULL, 'abc', NULL, 'abc', 'abc', 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsorganisasi`
--

CREATE TABLE `wsorganisasi` (
  `id_organisasi` int(11) NOT NULL,
  `nama_perusahaan` tinytext DEFAULT NULL,
  `kode_organisasi` tinytext DEFAULT NULL,
  `nama_organisasi` tinytext DEFAULT NULL,
  `tipe_area` text DEFAULT NULL,
  `grup_organisasi` text DEFAULT NULL,
  `unit_kerja` tinytext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `id_induk_organisasi` longtext DEFAULT NULL,
  `induk_organisasi` longtext DEFAULT NULL,
  `nama_struktur_organisasi` tinytext DEFAULT NULL,
  `versi_struktur_organisasi` tinytext DEFAULT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `kode_pusat_biaya` tinytext DEFAULT NULL,
  `nomor_indeks_organisasi` int(11) DEFAULT NULL,
  `tingkat_organisasi` int(11) DEFAULT NULL,
  `urutan_organisasi` int(11) DEFAULT NULL,
  `keterangan_organisasi` longtext DEFAULT NULL,
  `leher_struktur` text DEFAULT NULL,
  `aktif` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsperusahaan`
--

CREATE TABLE `wsperusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `perusahaan_logo` text DEFAULT NULL,
  `nama_perusahaan` text DEFAULT NULL,
  `kode_perusahaan` text DEFAULT NULL,
  `singkatan` text DEFAULT NULL,
  `visi_perusahaan` text DEFAULT NULL,
  `misi_perusahaan` longtext DEFAULT NULL,
  `nilai_perusahaan` longtext DEFAULT NULL,
  `keterangan_perusahaan` longtext DEFAULT NULL,
  `tanggal_mulai_perusahaan` date DEFAULT NULL,
  `tanggal_selesai_perusahaan` date DEFAULT NULL,
  `jenis_perusahaan` text DEFAULT NULL,
  `jenis_bisnis_perusahaan` text DEFAULT NULL,
  `jumlah_perusahaan` text DEFAULT NULL,
  `nomor_npwp_perusahaan` text DEFAULT NULL,
  `lokasi_pajak` text DEFAULT NULL,
  `npp` text DEFAULT NULL,
  `npkp` text DEFAULT NULL,
  `id_logo_perusahaan` text DEFAULT NULL,
  `keterangan` longtext DEFAULT NULL,
  `status_rekaman` int(11) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `jumlah_karyawan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsposisi`
--

CREATE TABLE `wsposisi` (
  `id_posisi` int(11) NOT NULL,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_grup_lokasi_kerja` varchar(50) DEFAULT NULL,
  `lokasi_kerja` varchar(50) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `kode_posisi` tinytext DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL,
  `kode_mpp` varchar(15) DEFAULT NULL,
  `tingkat_posisi` text DEFAULT NULL,
  `detail_tingkat_posisi` varchar(25) DEFAULT NULL,
  `kode_jabatan` varchar(25) DEFAULT NULL,
  `nama_jabatan` tinytext DEFAULT NULL,
  `nama_organisasi` tinytext DEFAULT NULL,
  `tingkat_organisasi` tinytext DEFAULT NULL,
  `tipe_posisi` tinytext DEFAULT NULL,
  `deskripsi_pekerjaan_posisi` varchar(1000) DEFAULT NULL,
  `kode_posisi_atasan` varchar(25) DEFAULT NULL,
  `nama_posisi_atasan` varchar(25) DEFAULT NULL,
  `tipe_area` tinytext DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `posisi_aktif` varchar(50) DEFAULT NULL,
  `komisi` varchar(50) DEFAULT NULL,
  `kepala_fungsional` varchar(50) DEFAULT NULL,
  `flag_operasional` varchar(50) DEFAULT NULL,
  `status_posisi` varchar(50) DEFAULT NULL,
  `nomor_surat` varchar(100) DEFAULT NULL,
  `jumlah_karyawan_dengan_posisi_ini` int(11) DEFAULT NULL,
  `keterangan` longtext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `wsposisi`
--

INSERT INTO `wsposisi` (`id_posisi`, `kode_perusahaan`, `nama_grup_lokasi_kerja`, `lokasi_kerja`, `nama_perusahaan`, `kode_posisi`, `nama_posisi`, `kode_mpp`, `tingkat_posisi`, `detail_tingkat_posisi`, `kode_jabatan`, `nama_jabatan`, `nama_organisasi`, `tingkat_organisasi`, `tipe_posisi`, `deskripsi_pekerjaan_posisi`, `kode_posisi_atasan`, `nama_posisi_atasan`, `tipe_area`, `dari_golongan`, `sampai_golongan`, `posisi_aktif`, `komisi`, `kepala_fungsional`, `flag_operasional`, `status_posisi`, `nomor_surat`, `jumlah_karyawan_dengan_posisi_ini`, `keterangan`, `status_rekaman`, `tanggal_mulai_efektif`, `tanggal_selesai_efektif`, `pengguna_masuk`, `waktu_masuk`, `pengguna_ubah`, `waktu_ubah`, `pengguna_hapus`, `waktu_hapus`) VALUES
(3, '453', 'jakarta', 'jakarta 12', NULL, 'kode-123', 'GA', NULL, 'staff', 'abc', NULL, 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '453', 'jakarta', 'jakarta 12', NULL, 'kode-124', 'GA', NULL, 'manager', 'abc', NULL, 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '453', 'bandung', 'bandung 12', NULL, 'kode-123', 'GA', NULL, 'staff', 'abc', NULL, 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '453', 'bandung', 'bandung 12', NULL, 'kode-124', 'GA', NULL, 'manager', 'abc', NULL, 'abc', 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsstandarupahminimum`
--

CREATE TABLE `wsstandarupahminimum` (
  `id_standar_upah_minimum` int(11) NOT NULL,
  `kode_posisi` varchar(20) DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL,
  `tingkat_pendidikan` varchar(20) DEFAULT NULL,
  `kode_lokasi` varchar(100) DEFAULT NULL,
  `nama_lokasi` varchar(50) DEFAULT NULL,
  `upah_minimum` int(8) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser`
--

CREATE TABLE `wstampilantabledashboarduser` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_address_dua`
--

CREATE TABLE `wstampilantabledashboarduser_address_dua` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_alamat`
--

CREATE TABLE `wstampilantabledashboarduser_alamat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_dp`
--

CREATE TABLE `wstampilantabledashboarduser_dp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_glk`
--

CREATE TABLE `wstampilantabledashboarduser_glk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_golongan`
--

CREATE TABLE `wstampilantabledashboarduser_golongan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_golongan` varchar(255) DEFAULT NULL,
  `nama_golongan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_hdp`
--

CREATE TABLE `wstampilantabledashboarduser_hdp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_jabatan`
--

CREATE TABLE `wstampilantabledashboarduser_jabatan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_jabatan` varchar(255) DEFAULT NULL,
  `nama_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kac`
--

CREATE TABLE `wstampilantabledashboarduser_kac` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kantor` varchar(255) DEFAULT NULL,
  `nama_kantor` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kc`
--

CREATE TABLE `wstampilantabledashboarduser_kc` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelas_cabang` varchar(255) DEFAULT NULL,
  `nama_kelas_cabang` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kj`
--

CREATE TABLE `wstampilantabledashboarduser_kj` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_lk`
--

CREATE TABLE `wstampilantabledashboarduser_lk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_organisasi`
--

CREATE TABLE `wstampilantabledashboarduser_organisasi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_organisasi` varchar(255) DEFAULT NULL,
  `kode_organisasi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_po`
--

CREATE TABLE `wstampilantabledashboarduser_po` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_posisi`
--

CREATE TABLE `wstampilantabledashboarduser_posisi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tg`
--

CREATE TABLE `wstampilantabledashboarduser_tg` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_tingkat_golongan` tinytext DEFAULT NULL,
  `kode_tingkat_golongan` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tingkatorganisasi`
--

CREATE TABLE `wstampilantabledashboarduser_tingkatorganisasi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `nama_tingkat_organisasi` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tkj`
--

CREATE TABLE `wstampilantabledashboarduser_tkj` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_to`
--

CREATE TABLE `wstampilantabledashboarduser_to` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `nama_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tp`
--

CREATE TABLE `wstampilantabledashboarduser_tp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_posisi` varchar(255) DEFAULT NULL,
  `nama_tingkat_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tptk`
--

CREATE TABLE `wstampilantabledashboarduser_tptk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_ptk_tujuan` varchar(100) DEFAULT NULL,
  `kode_ptk_tujuan` varchar(100) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_usm`
--

CREATE TABLE `wstampilantabledashboarduser_usm` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatgolongan`
--

CREATE TABLE `wstingkatgolongan` (
  `id_tingkat_golongan` int(11) NOT NULL,
  `nama_perusahaan` tinytext DEFAULT NULL,
  `kode_tingkat_golongan` varchar(100) DEFAULT NULL,
  `nama_tingkat_golongan` tinytext DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `kode_golongan` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `items` longtext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatgolongan_item`
--

CREATE TABLE `wstingkatgolongan_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatkelompokjabatan`
--

CREATE TABLE `wstingkatkelompokjabatan` (
  `id_tingkat_kelompok_jabatan` int(11) NOT NULL,
  `kode_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `kode_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_kelompok_jabatan` tinytext DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` date DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatorganisasi`
--

CREATE TABLE `wstingkatorganisasi` (
  `id_tingkat_organisasi` int(11) NOT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `nama_tingkat_organisasi` tinytext DEFAULT NULL,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatposisi`
--

CREATE TABLE `wstingkatposisi` (
  `id_tingkat_posisi` int(11) NOT NULL,
  `kode_tingkat_posisi` text DEFAULT NULL,
  `nama_tingkat_posisi` text DEFAULT NULL,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` text DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` text DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` text DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`) USING BTREE,
  ADD KEY `FK_cities_data_provinsi` (`prov_id`);

--
-- Indeks untuk tabel `detail_desc_pekerjaan`
--
ALTER TABLE `detail_desc_pekerjaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK__wsdeskripsipekerjaan` (`desc_id`);

--
-- Indeks untuk tabel `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`dis_id`) USING BTREE,
  ADD KEY `FK_districts_cities` (`city_id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING BTREE;

--
-- Indeks untuk tabel `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`) USING BTREE,
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`) USING BTREE;

--
-- Indeks untuk tabel `postalcode`
--
ALTER TABLE `postalcode`
  ADD PRIMARY KEY (`postal_id`) USING BTREE;

--
-- Indeks untuk tabel `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`prov_id`) USING BTREE;

--
-- Indeks untuk tabel `rc_data_cv_pelamar`
--
ALTER TABLE `rc_data_cv_pelamar`
  ADD PRIMARY KEY (`id_cv_pelamar`);

--
-- Indeks untuk tabel `rc_data_lowongan_kerja`
--
ALTER TABLE `rc_data_lowongan_kerja`
  ADD PRIMARY KEY (`id_lowongan_kerja`);

--
-- Indeks untuk tabel `rc_detail_step_process_recruitment`
--
ALTER TABLE `rc_detail_step_process_recruitment`
  ADD PRIMARY KEY (`id_detail_step_process_recruitment`);

--
-- Indeks untuk tabel `rc_formulir_permintaan_tenaga_kerja`
--
ALTER TABLE `rc_formulir_permintaan_tenaga_kerja`
  ADD PRIMARY KEY (`id_fptk`);

--
-- Indeks untuk tabel `rc_form_latar_belakang_pendidikan`
--
ALTER TABLE `rc_form_latar_belakang_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan_pelamar`),
  ADD KEY `id_data_pelamar` (`id_data_pelamar`);

--
-- Indeks untuk tabel `rc_ptk_detail`
--
ALTER TABLE `rc_ptk_detail`
  ADD PRIMARY KEY (`id_ptk`);

--
-- Indeks untuk tabel `rc_step_process_recruitment`
--
ALTER TABLE `rc_step_process_recruitment`
  ADD PRIMARY KEY (`id_step_process_recruitment`),
  ADD UNIQUE KEY `id_step_process_recruitment_UNIQUE` (`id_step_process_recruitment`);

--
-- Indeks untuk tabel `rc_susunan_keluarga_pelamar`
--
ALTER TABLE `rc_susunan_keluarga_pelamar`
  ADD PRIMARY KEY (`id_susunan_keluarga _pelamar`),
  ADD KEY `id_data_pelamar` (`id_data_pelamar`);

--
-- Indeks untuk tabel `rc_transaksi_ptk`
--
ALTER TABLE `rc_transaksi_ptk`
  ADD PRIMARY KEY (`id_transaksi_ptk`);

--
-- Indeks untuk tabel `reftable_cv`
--
ALTER TABLE `reftable_cv`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reftable_dlo`
--
ALTER TABLE `reftable_dlo`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `reftable_dpp`
--
ALTER TABLE `reftable_dpp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `reftable_fptk`
--
ALTER TABLE `reftable_fptk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `reftable_pro`
--
ALTER TABLE `reftable_pro`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `reftable_ptk`
--
ALTER TABLE `reftable_ptk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `reftable_tptk`
--
ALTER TABLE `reftable_tptk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_roles`);

--
-- Indeks untuk tabel `subdistricts`
--
ALTER TABLE `subdistricts`
  ADD PRIMARY KEY (`subdis_id`) USING BTREE,
  ADD KEY `FK_subdistricts_districts` (`dis_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indeks untuk tabel `wsalamatperusahaan`
--
ALTER TABLE `wsalamatperusahaan`
  ADD PRIMARY KEY (`id_alamat`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan`
  ADD PRIMARY KEY (`id_deskripsi_pekerjaan`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_bag`
--
ALTER TABLE `wsdeskripsipekerjaan_bag`
  ADD PRIMARY KEY (`id_deskripsi_pekerjaan_bag`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--
ALTER TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_tanggung_jawab`
--
ALTER TABLE `wsdeskripsipekerjaan_tanggung_jawab`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_wewenang`
--
ALTER TABLE `wsdeskripsipekerjaan_wewenang`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsgolongan`
--
ALTER TABLE `wsgolongan`
  ADD PRIMARY KEY (`id_golongan`) USING BTREE;

--
-- Indeks untuk tabel `wsgruplokasikerja`
--
ALTER TABLE `wsgruplokasikerja`
  ADD PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsgruplokasikerja_deskripsi`
--
ALTER TABLE `wsgruplokasikerja_deskripsi`
  ADD PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsjabatan`
--
ALTER TABLE `wsjabatan`
  ADD PRIMARY KEY (`id_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wskantorcabang`
--
ALTER TABLE `wskantorcabang`
  ADD PRIMARY KEY (`id_kantor`) USING BTREE;

--
-- Indeks untuk tabel `wskelascabang`
--
ALTER TABLE `wskelascabang`
  ADD PRIMARY KEY (`id_kelas_cabang`) USING BTREE;

--
-- Indeks untuk tabel `wskelompokjabatan`
--
ALTER TABLE `wskelompokjabatan`
  ADD PRIMARY KEY (`id_kelompok_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wslokasikerja`
--
ALTER TABLE `wslokasikerja`
  ADD PRIMARY KEY (`id_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsorganisasi`
--
ALTER TABLE `wsorganisasi`
  ADD PRIMARY KEY (`id_organisasi`) USING BTREE;

--
-- Indeks untuk tabel `wsperusahaan`
--
ALTER TABLE `wsperusahaan`
  ADD PRIMARY KEY (`id_perusahaan`) USING BTREE;

--
-- Indeks untuk tabel `wsposisi`
--
ALTER TABLE `wsposisi`
  ADD PRIMARY KEY (`id_posisi`) USING BTREE;

--
-- Indeks untuk tabel `wsstandarupahminimum`
--
ALTER TABLE `wsstandarupahminimum`
  ADD PRIMARY KEY (`id_standar_upah_minimum`);

--
-- Indeks untuk tabel `wstampilantabledashboarduser`
--
ALTER TABLE `wstampilantabledashboarduser`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_address_dua`
--
ALTER TABLE `wstampilantabledashboarduser_address_dua`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_alamat`
--
ALTER TABLE `wstampilantabledashboarduser_alamat`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_dp`
--
ALTER TABLE `wstampilantabledashboarduser_dp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_glk`
--
ALTER TABLE `wstampilantabledashboarduser_glk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_golongan`
--
ALTER TABLE `wstampilantabledashboarduser_golongan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_hdp`
--
ALTER TABLE `wstampilantabledashboarduser_hdp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_jabatan`
--
ALTER TABLE `wstampilantabledashboarduser_jabatan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kac`
--
ALTER TABLE `wstampilantabledashboarduser_kac`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kc`
--
ALTER TABLE `wstampilantabledashboarduser_kc`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kj`
--
ALTER TABLE `wstampilantabledashboarduser_kj`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_lk`
--
ALTER TABLE `wstampilantabledashboarduser_lk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_organisasi`
--
ALTER TABLE `wstampilantabledashboarduser_organisasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_po`
--
ALTER TABLE `wstampilantabledashboarduser_po`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_posisi`
--
ALTER TABLE `wstampilantabledashboarduser_posisi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tg`
--
ALTER TABLE `wstampilantabledashboarduser_tg`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tingkatorganisasi`
--
ALTER TABLE `wstampilantabledashboarduser_tingkatorganisasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tkj`
--
ALTER TABLE `wstampilantabledashboarduser_tkj`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_to`
--
ALTER TABLE `wstampilantabledashboarduser_to`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tp`
--
ALTER TABLE `wstampilantabledashboarduser_tp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tptk`
--
ALTER TABLE `wstampilantabledashboarduser_tptk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_usm`
--
ALTER TABLE `wstampilantabledashboarduser_usm`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatgolongan`
--
ALTER TABLE `wstingkatgolongan`
  ADD PRIMARY KEY (`id_tingkat_golongan`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatgolongan_item`
--
ALTER TABLE `wstingkatgolongan_item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatkelompokjabatan`
--
ALTER TABLE `wstingkatkelompokjabatan`
  ADD PRIMARY KEY (`id_tingkat_kelompok_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatorganisasi`
--
ALTER TABLE `wstingkatorganisasi`
  ADD PRIMARY KEY (`id_tingkat_organisasi`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatposisi`
--
ALTER TABLE `wstingkatposisi`
  ADD PRIMARY KEY (`id_tingkat_posisi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=476;

--
-- AUTO_INCREMENT untuk tabel `detail_desc_pekerjaan`
--
ALTER TABLE `detail_desc_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `districts`
--
ALTER TABLE `districts`
  MODIFY `dis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6995;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `header`
--
ALTER TABLE `header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `postalcode`
--
ALTER TABLE `postalcode`
  MODIFY `postal_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81249;

--
-- AUTO_INCREMENT untuk tabel `provinces`
--
ALTER TABLE `provinces`
  MODIFY `prov_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `rc_data_cv_pelamar`
--
ALTER TABLE `rc_data_cv_pelamar`
  MODIFY `id_cv_pelamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `rc_data_lowongan_kerja`
--
ALTER TABLE `rc_data_lowongan_kerja`
  MODIFY `id_lowongan_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rc_detail_step_process_recruitment`
--
ALTER TABLE `rc_detail_step_process_recruitment`
  MODIFY `id_detail_step_process_recruitment` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `rc_formulir_permintaan_tenaga_kerja`
--
ALTER TABLE `rc_formulir_permintaan_tenaga_kerja`
  MODIFY `id_fptk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rc_form_latar_belakang_pendidikan`
--
ALTER TABLE `rc_form_latar_belakang_pendidikan`
  MODIFY `id_pendidikan_pelamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rc_ptk_detail`
--
ALTER TABLE `rc_ptk_detail`
  MODIFY `id_ptk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1660;

--
-- AUTO_INCREMENT untuk tabel `rc_step_process_recruitment`
--
ALTER TABLE `rc_step_process_recruitment`
  MODIFY `id_step_process_recruitment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `rc_susunan_keluarga_pelamar`
--
ALTER TABLE `rc_susunan_keluarga_pelamar`
  MODIFY `id_susunan_keluarga _pelamar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rc_transaksi_ptk`
--
ALTER TABLE `rc_transaksi_ptk`
  MODIFY `id_transaksi_ptk` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `reftable_cv`
--
ALTER TABLE `reftable_cv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `reftable_dlo`
--
ALTER TABLE `reftable_dlo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reftable_dpp`
--
ALTER TABLE `reftable_dpp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reftable_fptk`
--
ALTER TABLE `reftable_fptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reftable_pro`
--
ALTER TABLE `reftable_pro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reftable_ptk`
--
ALTER TABLE `reftable_ptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT untuk tabel `reftable_tptk`
--
ALTER TABLE `reftable_tptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id_roles` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `subdistricts`
--
ALTER TABLE `subdistricts`
  MODIFY `subdis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81226;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `wsalamatperusahaan`
--
ALTER TABLE `wsalamatperusahaan`
  MODIFY `id_alamat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan`
  MODIFY `id_deskripsi_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_bag`
--
ALTER TABLE `wsdeskripsipekerjaan_bag`
  MODIFY `id_deskripsi_pekerjaan_bag` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--
ALTER TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_tanggung_jawab`
--
ALTER TABLE `wsdeskripsipekerjaan_tanggung_jawab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_wewenang`
--
ALTER TABLE `wsdeskripsipekerjaan_wewenang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `wsgolongan`
--
ALTER TABLE `wsgolongan`
  MODIFY `id_golongan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `wsgruplokasikerja`
--
ALTER TABLE `wsgruplokasikerja`
  MODIFY `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wsgruplokasikerja_deskripsi`
--
ALTER TABLE `wsgruplokasikerja_deskripsi`
  MODIFY `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsjabatan`
--
ALTER TABLE `wsjabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT untuk tabel `wskantorcabang`
--
ALTER TABLE `wskantorcabang`
  MODIFY `id_kantor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `wskelascabang`
--
ALTER TABLE `wskelascabang`
  MODIFY `id_kelas_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `wskelompokjabatan`
--
ALTER TABLE `wskelompokjabatan`
  MODIFY `id_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `wslokasikerja`
--
ALTER TABLE `wslokasikerja`
  MODIFY `id_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `wsorganisasi`
--
ALTER TABLE `wsorganisasi`
  MODIFY `id_organisasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT untuk tabel `wsperusahaan`
--
ALTER TABLE `wsperusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT untuk tabel `wsposisi`
--
ALTER TABLE `wsposisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `wsstandarupahminimum`
--
ALTER TABLE `wsstandarupahminimum`
  MODIFY `id_standar_upah_minimum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser`
--
ALTER TABLE `wstampilantabledashboarduser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_address_dua`
--
ALTER TABLE `wstampilantabledashboarduser_address_dua`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_alamat`
--
ALTER TABLE `wstampilantabledashboarduser_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_dp`
--
ALTER TABLE `wstampilantabledashboarduser_dp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_glk`
--
ALTER TABLE `wstampilantabledashboarduser_glk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_golongan`
--
ALTER TABLE `wstampilantabledashboarduser_golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_hdp`
--
ALTER TABLE `wstampilantabledashboarduser_hdp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_jabatan`
--
ALTER TABLE `wstampilantabledashboarduser_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kac`
--
ALTER TABLE `wstampilantabledashboarduser_kac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kc`
--
ALTER TABLE `wstampilantabledashboarduser_kc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kj`
--
ALTER TABLE `wstampilantabledashboarduser_kj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_lk`
--
ALTER TABLE `wstampilantabledashboarduser_lk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_organisasi`
--
ALTER TABLE `wstampilantabledashboarduser_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_po`
--
ALTER TABLE `wstampilantabledashboarduser_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_posisi`
--
ALTER TABLE `wstampilantabledashboarduser_posisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tg`
--
ALTER TABLE `wstampilantabledashboarduser_tg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tingkatorganisasi`
--
ALTER TABLE `wstampilantabledashboarduser_tingkatorganisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tkj`
--
ALTER TABLE `wstampilantabledashboarduser_tkj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_to`
--
ALTER TABLE `wstampilantabledashboarduser_to`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tp`
--
ALTER TABLE `wstampilantabledashboarduser_tp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tptk`
--
ALTER TABLE `wstampilantabledashboarduser_tptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_usm`
--
ALTER TABLE `wstampilantabledashboarduser_usm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `wstingkatgolongan`
--
ALTER TABLE `wstingkatgolongan`
  MODIFY `id_tingkat_golongan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `wstingkatgolongan_item`
--
ALTER TABLE `wstingkatgolongan_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatkelompokjabatan`
--
ALTER TABLE `wstingkatkelompokjabatan`
  MODIFY `id_tingkat_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `wstingkatorganisasi`
--
ALTER TABLE `wstingkatorganisasi`
  MODIFY `id_tingkat_organisasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `wstingkatposisi`
--
ALTER TABLE `wstingkatposisi`
  MODIFY `id_tingkat_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_desc_pekerjaan`
--
ALTER TABLE `detail_desc_pekerjaan`
  ADD CONSTRAINT `FK__wsdeskripsipekerjaan` FOREIGN KEY (`desc_id`) REFERENCES `wsdeskripsipekerjaan` (`id_deskripsi_pekerjaan`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
