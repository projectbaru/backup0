<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use API\RcFormDataPelamarController;
use API\RcSusunanKeluargaPelamarController;
use API\RcFormLatarBelakangPendidikanController;
use API\RcPelatihanPelamarController;
use API\RcBahasaAsingController;
use API\RcOrganisasiPelamarController;
use API\RcRiwayatKerjaPelamarController;
use API\RcKontakReferensiPelamarController;
use API\RcKontakDaruratController;
use API\RcNegatifPositifPelamarController;
use API\RcJawabanPilihanController;
use API\RcJawabanEsaiController;
use API\RcPertanyaanMarstonModelController;
use API\RcJawabanMarstonModelController;
use API\DPPController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('v1/rc_form_data_pelamar',RcFormDataPelamarController::class);
Route::apiResource('v1/rc_susunan_keluarga_pelamar',RcSusunanKeluargaPelamarController::class);
// Route::apiResource('v1/rc_form_latar_belakang_pendidikan',RcFormLatarBelakangPendidikanController::class);
Route::apiResource('v1/rc_pelatihan_pelamar',RcPelatihanPelamarController::class);
Route::apiResource('v1/rc_bahasa_asing',RcBahasaAsingController::class);
Route::apiResource('v1/rc_organisasi_pelamar',RcOrganisasiPelamarController::class);
Route::apiResource('v1/rc_riwayat_kerja_pelamar',RcRiwayatKerjaPelamarController::class);
Route::apiResource('v1/rc_kontak_referensi_pelamar',RcKontakReferensiPelamarController::class);
Route::apiResource('v1/rc_kontak_darurat',RcKontakDaruratController::class);
Route::apiResource('v1/rc_negatif_positif_pelamar',RcNegatifPositifPelamarController::class);
Route::apiResource('v1/rc_jawaban_pilihan',RcJawabanPilihanController::class);
Route::apiResource('v1/rc_jawaban_esai',RcJawabanEsaiController::class);
Route::apiResource('v1/rc_pertanyaan_marston_model',RcPertanyaanMarstonModelController::class);
Route::apiResource('v1/rc_jawaban_marston_model',RcJawabanMarstonModelController::class);
Route::apiResource('v1/dpp',DPPController::class);

