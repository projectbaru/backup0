<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcCvPelamarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_cv_pelamar', function (Blueprint $table) {
            $table->id();
            $table->string('resume_cv', 100)->nullable();
            $table->tinyText('nama_lengkap')->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('no_hp')->nullable();
            $table->string('lokasi_pelamar', 100)->nullable();
            $table->string('pengalaman_kerja', 100)->nullable()->comment('1.berpengalaman 2.lulusan baru');
            $table->integer('tahun_pengalaman_kerja')->nullable();
            $table->integer('bulan_pengalaman_kerja')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin', 100)->nullable()->comment('1.Laki-laki 2.Perempuan');
            $table->string('pendidikan_tertinggi', 100)->nullable()->comment('1. SMK/Sederajat 2.D1 3.D2 4.D3 5.S1 6.S2 7.S3');
            $table->string('nama_sekolah', 100)->nullable();
            $table->string('program_studi_jurusan', 100)->nullable();
            $table->date('tanggal_wisuda')->nullable();
            $table->string('nilai_rata_rata')->nullable();
            $table->string('nilai_skala')->nullable();
            $table->string('informasi_lowongan', 100)->nullable()->comment('1.Web SRU 2.Job Street 3.Karyawan SRU 4.Lainnya');
            $table->string('keterangan_informasi_pilihan', 100)->nullable();
            $table->string('pesan_pelamar', 100)->nullable();
            $table->date('tanggal_melamar')->nullable();
            $table->string('status_lamaran', 100)->nullable()->comment('1.Belum Diperiksa 2.Sedang Dipertimbangkan 3.Lulus 4.Tidak Lulus');
            $table->string('pengguna_masuk', 100)->nullable();
            $table->dateTime('waktu_masuk')->nullable();
            $table->string('pengguna_ubah', 100)->nullable();
            $table->dateTime('waktu_ubah')->nullable();
            $table->string('pengguna_hapus', 100)->nullable();
            $table->dateTime('waktu_hapus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_cv_pelamar');
    }
}
