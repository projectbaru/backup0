-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_hris_proses
DROP DATABASE IF EXISTS `db_hris_proses`;
CREATE DATABASE IF NOT EXISTS `db_hris_proses` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_hris_proses`;

-- Dumping structure for table db_hris_proses.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`city_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=476 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.data_kabupaten
DROP TABLE IF EXISTS `data_kabupaten`;
CREATE TABLE IF NOT EXISTS `data_kabupaten` (
  `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kabupaten` longtext,
  `id_provinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kabupaten`),
  KEY `id_provinsi` (`id_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.data_kecamatan
DROP TABLE IF EXISTS `data_kecamatan`;
CREATE TABLE IF NOT EXISTS `data_kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` longtext,
  `id_kabupaten` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `id_kabupaten` (`id_kabupaten`)
) ENGINE=InnoDB AUTO_INCREMENT=82290 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.data_kelurahan
DROP TABLE IF EXISTS `data_kelurahan`;
CREATE TABLE IF NOT EXISTS `data_kelurahan` (
  `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelurahan` longtext,
  `id_kecamatan` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_kelurahan`),
  KEY `id_kecamatan` (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=81267 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.data_kode_pos
DROP TABLE IF EXISTS `data_kode_pos`;
CREATE TABLE IF NOT EXISTS `data_kode_pos` (
  `id_kode_pos` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pos` longtext,
  `id_kecamatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kode_pos`),
  KEY `id_kecamatan` (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=81267 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.data_provinsi
DROP TABLE IF EXISTS `data_provinsi`;
CREATE TABLE IF NOT EXISTS `data_provinsi` (
  `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` longtext,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.districts
DROP TABLE IF EXISTS `districts`;
CREATE TABLE IF NOT EXISTS `districts` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_name` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dis_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6995 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.personal_access_tokens
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`) USING BTREE,
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.postalcode
DROP TABLE IF EXISTS `postalcode`;
CREATE TABLE IF NOT EXISTS `postalcode` (
  `postal_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subdis_id` int(11) DEFAULT NULL,
  `dis_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `prov_id` int(11) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`postal_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=81249 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.provinces
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE IF NOT EXISTS `provinces` (
  `prov_id` int(11) NOT NULL AUTO_INCREMENT,
  `prov_name` varchar(255) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`prov_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id_roles` int(11) NOT NULL AUTO_INCREMENT,
  `list` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id_roles`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.subdistricts
DROP TABLE IF EXISTS `subdistricts`;
CREATE TABLE IF NOT EXISTS `subdistricts` (
  `subdis_id` int(11) NOT NULL AUTO_INCREMENT,
  `subdis_name` varchar(255) DEFAULT NULL,
  `dis_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`subdis_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=81226 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsalamatperusahaan
DROP TABLE IF EXISTS `wsalamatperusahaan`;
CREATE TABLE IF NOT EXISTS `wsalamatperusahaan` (
  `id_alamat` int(11) NOT NULL AUTO_INCREMENT,
  `kode_alamat` varchar(20) DEFAULT NULL,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `status_alamat_untuk_spt` text,
  `jenis_alamat` text,
  `alamat` longtext,
  `kota` tinytext,
  `kode_pos` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `keterangan` longtext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_alamat`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan
DROP TABLE IF EXISTS `wsdeskripsipekerjaan`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan` (
  `id_deskripsi_pekerjaan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_deskripsi` text,
  `nama_atasan_langsung` tinytext,
  `nama_posisi` text,
  `keterangan` text,
  `deskripsi_pekerjaan` longtext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` date DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `nomor_dokumen` varchar(50) DEFAULT NULL,
  `edisi` text,
  `tanggal_edisi` date DEFAULT NULL,
  `nomor_revisi` text,
  `tanggal_revisi` date DEFAULT NULL,
  `nama_jabatan` text,
  `nama_karyawan` text,
  `divisi` text,
  `lokasi_kerja` text,
  `dibuat_oleh` date DEFAULT NULL,
  `diperiksa_oleh` text,
  `disahkan_oleh` text,
  `fungsi_jabatan` text,
  `tanggung_jawab` longtext,
  `departemen` text,
  `kode_posisi` text,
  `nama_lokasi_kerja` text,
  `nama_pengawas` text,
  `lingkup_aktivitas` text,
  `id_logo_perusahaan` text,
  PRIMARY KEY (`id_deskripsi_pekerjaan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan_bag
DROP TABLE IF EXISTS `wsdeskripsipekerjaan_bag`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan_bag` (
  `id_deskripsi_pekerjaan_bag` int(11) NOT NULL AUTO_INCREMENT,
  `kode_posisi` tinytext,
  `nama_posisi` tinytext,
  `deskripsi_pekerjaan` tinytext,
  `pdca` tinytext,
  `bsc` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_deskripsi_pekerjaan_bag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan_deskripsi_pekerjaan
DROP TABLE IF EXISTS `wsdeskripsipekerjaan_deskripsi_pekerjaan`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan_deskripsi_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `deskripsi_pekerjaan` text NOT NULL,
  `pdca` varchar(255) NOT NULL,
  `bsc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan_kualifikasi_jabatan
DROP TABLE IF EXISTS `wsdeskripsipekerjaan_kualifikasi_jabatan`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan_kualifikasi_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `kualifikasi_jabatan` text NOT NULL,
  `tipe_kualifikasi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan_tanggung_jawab
DROP TABLE IF EXISTS `wsdeskripsipekerjaan_tanggung_jawab`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan_tanggung_jawab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `tanggung_jawab` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan_wewenang
DROP TABLE IF EXISTS `wsdeskripsipekerjaan_wewenang`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan_wewenang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `wewenang` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsgolongan
DROP TABLE IF EXISTS `wsgolongan`;
CREATE TABLE IF NOT EXISTS `wsgolongan` (
  `id_golongan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_golongan` tinytext,
  `nama_golongan` tinytext,
  `tingkat_golongan` tinytext,
  `urutan_golongan` tinytext,
  `nama_perusahaan` longtext,
  `tingkat_posisi` tinytext,
  `jabatan` tinytext,
  `deskripsi` text,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` varchar(50) DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_golongan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsgruplokasikerja
DROP TABLE IF EXISTS `wsgruplokasikerja`;
CREATE TABLE IF NOT EXISTS `wsgruplokasikerja` (
  `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT,
  `kode_grup_lokasi_kerja` tinytext,
  `nama_grup_lokasi_kerja` tinytext,
  `tipe_grup_lokasi_kerja` tinytext,
  `lokasi_kerja` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsgruplokasikerja_deskripsi
DROP TABLE IF EXISTS `wsgruplokasikerja_deskripsi`;
CREATE TABLE IF NOT EXISTS `wsgruplokasikerja_deskripsi` (
  `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT,
  `kode_grup_lokasi_kerja` tinytext,
  `nama_grup_lokasi_kerja` tinytext,
  `tipe_grup_lokasi_kerja` tinytext,
  `lokasi_kerja` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsjabatan
DROP TABLE IF EXISTS `wsjabatan`;
CREATE TABLE IF NOT EXISTS `wsjabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_jabatan` tinytext,
  `nama_jabatan` tinytext,
  `tipe_jabatan` tinytext,
  `kode_kelompok_jabatan` varchar(25) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(30) DEFAULT NULL,
  `grup_jabatan` varchar(30) DEFAULT NULL,
  `deskripsi_jabatan` longtext,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `dari_golongan` tinytext,
  `sampai_golongan` tinytext,
  `magang` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `status_rekaman` text,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wskantorcabang
DROP TABLE IF EXISTS `wskantorcabang`;
CREATE TABLE IF NOT EXISTS `wskantorcabang` (
  `id_kantor` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kantor` tinytext,
  `nama_kantor` tinytext,
  `kode_kelas` tinytext,
  `tipe_kantor` tinytext,
  `kode_lokasi_kerja` tinytext,
  `nama_lokasi_kerja` tinytext,
  `nomor_npwp` tinytext,
  `umk_ump` int(11) DEFAULT NULL,
  `ttd_spt` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  PRIMARY KEY (`id_kantor`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wskelascabang
DROP TABLE IF EXISTS `wskelascabang`;
CREATE TABLE IF NOT EXISTS `wskelascabang` (
  `id_kelas_cabang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kelas_cabang` tinytext,
  `nama_kelas_cabang` tinytext,
  `area_kelas_cabang` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_kelas_cabang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wskelompokjabatan
DROP TABLE IF EXISTS `wskelompokjabatan`;
CREATE TABLE IF NOT EXISTS `wskelompokjabatan` (
  `id_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kelompok_jabatan` tinytext,
  `nama_kelompok_jabatan` tinytext,
  `deskripsi_kelompok_jabatan` text,
  `dari_golongan` tinytext,
  `sampai_golongan` tinytext,
  `grup_jabatan` varchar(50) DEFAULT NULL,
  `tipe_jabatan` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `status_rekaman` text,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_jabatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wslokasikerja
DROP TABLE IF EXISTS `wslokasikerja`;
CREATE TABLE IF NOT EXISTS `wslokasikerja` (
  `id_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT,
  `kode_lokasi_kerja` tinytext,
  `nama_lokasi_kerja` tinytext,
  `id_grup_lokasi_kerja` tinytext,
  `id_kantor` tinytext,
  `zona_waktu` int(11) DEFAULT NULL,
  `alamat` text,
  `provinsi` tinytext,
  `kecamatan` tinytext,
  `kelurahan` tinytext,
  `kabupaten_kota` text,
  `kode_pos` int(11) DEFAULT NULL,
  `negara` tinytext,
  `nomor_telepon` varchar(50) DEFAULT NULL,
  `email` tinytext,
  `fax` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_lokasi_kerja`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsorganisasi
DROP TABLE IF EXISTS `wsorganisasi`;
CREATE TABLE IF NOT EXISTS `wsorganisasi` (
  `id_organisasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` tinytext,
  `kode_organisasi` tinytext,
  `nama_organisasi` tinytext,
  `tipe_area` text,
  `grup_organisasi` text,
  `unit_kerja` tinytext,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `id_induk_organisasi` longtext,
  `induk_organisasi` longtext,
  `nama_struktur_organisasi` tinytext,
  `versi_struktur_organisasi` tinytext,
  `kode_tingkat_organisasi` tinytext,
  `kode_pusat_biaya` tinytext,
  `nomor_indeks_organisasi` int(11) DEFAULT NULL,
  `tingkat_organisasi` int(11) DEFAULT NULL,
  `urutan_organisasi` int(11) DEFAULT NULL,
  `keterangan_organisasi` longtext,
  `leher_struktur` text,
  `aktif` text,
  `keterangan` text,
  `status_rekaman` text,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_organisasi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsperusahaan
DROP TABLE IF EXISTS `wsperusahaan`;
CREATE TABLE IF NOT EXISTS `wsperusahaan` (
  `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_logo` text,
  `nama_perusahaan` text,
  `kode_perusahaan` text,
  `singkatan` text,
  `visi_perusahaan` text,
  `misi_perusahaan` longtext,
  `nilai_perusahaan` longtext,
  `keterangan_perusahaan` longtext,
  `tanggal_mulai_perusahaan` date DEFAULT NULL,
  `tanggal_selesai_perusahaan` date DEFAULT NULL,
  `jenis_perusahaan` text,
  `jenis_bisnis_perusahaan` text,
  `jumlah_perusahaan` text,
  `nomor_npwp_perusahaan` text,
  `lokasi_pajak` text,
  `npp` text,
  `npkp` text,
  `id_logo_perusahaan` text,
  `keterangan` longtext,
  `status_rekaman` int(11) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_perusahaan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsposisi
DROP TABLE IF EXISTS `wsposisi`;
CREATE TABLE IF NOT EXISTS `wsposisi` (
  `id_posisi` int(11) NOT NULL AUTO_INCREMENT,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `kode_posisi` tinytext,
  `nama_posisi` varchar(100) DEFAULT NULL,
  `kode_mpp` varchar(15) DEFAULT NULL,
  `tingkat_posisi` text,
  `detail_tingkat_posisi` varchar(25) DEFAULT NULL,
  `kode_jabatan` varchar(25) DEFAULT NULL,
  `nama_jabatan` tinytext,
  `nama_organisasi` tinytext,
  `tingkat_organisasi` tinytext,
  `tipe_posisi` tinytext,
  `deskripsi_pekerjaan_posisi` varchar(1000) DEFAULT NULL,
  `kode_posisi_atasan` varchar(25) DEFAULT NULL,
  `nama_posisi_atasan` varchar(25) DEFAULT NULL,
  `tipe_area` tinytext,
  `dari_golongan` tinytext,
  `sampai_golongan` tinytext,
  `posisi_aktif` varchar(50) DEFAULT NULL,
  `komisi` varchar(50) DEFAULT NULL,
  `kepala_fungsional` varchar(50) DEFAULT NULL,
  `flag_operasional` varchar(50) DEFAULT NULL,
  `status_posisi` varchar(50) DEFAULT NULL,
  `nomor_surat` tinytext,
  `jumlah_karyawan_dengan_posisi_ini` int(11) DEFAULT NULL,
  `keterangan` longtext,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_posisi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wsstandarupahminimum
DROP TABLE IF EXISTS `wsstandarupahminimum`;
CREATE TABLE IF NOT EXISTS `wsstandarupahminimum` (
  `id_standar_upah_minimum` int(11) NOT NULL AUTO_INCREMENT,
  `kode_posisi` varchar(20) DEFAULT NULL,
  `nama_posisi` varchar(100) DEFAULT NULL,
  `tingkat_pendidikan` varchar(20) DEFAULT NULL,
  `kode_lokasi` varchar(100) DEFAULT NULL,
  `nama_lokasi` varchar(50) DEFAULT NULL,
  `upah_minimum` int(8) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_standar_upah_minimum`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser
DROP TABLE IF EXISTS `wstampilantabledashboarduser`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_address_dua
DROP TABLE IF EXISTS `wstampilantabledashboarduser_address_dua`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_address_dua` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_alamat
DROP TABLE IF EXISTS `wstampilantabledashboarduser_alamat`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_alamat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_dp
DROP TABLE IF EXISTS `wstampilantabledashboarduser_dp`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_dp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_glk
DROP TABLE IF EXISTS `wstampilantabledashboarduser_glk`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_glk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_golongan
DROP TABLE IF EXISTS `wstampilantabledashboarduser_golongan`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_golongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_golongan` varchar(255) DEFAULT NULL,
  `nama_golongan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_hdp
DROP TABLE IF EXISTS `wstampilantabledashboarduser_hdp`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_hdp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_jabatan
DROP TABLE IF EXISTS `wstampilantabledashboarduser_jabatan`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_jabatan` varchar(255) DEFAULT NULL,
  `nama_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_kac
DROP TABLE IF EXISTS `wstampilantabledashboarduser_kac`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_kac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kantor` varchar(255) DEFAULT NULL,
  `nama_kantor` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_kc
DROP TABLE IF EXISTS `wstampilantabledashboarduser_kc`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_kc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelas_cabang` varchar(255) DEFAULT NULL,
  `nama_kelas_cabang` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_kj
DROP TABLE IF EXISTS `wstampilantabledashboarduser_kj`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_kj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_lk
DROP TABLE IF EXISTS `wstampilantabledashboarduser_lk`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_lk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_organisasi
DROP TABLE IF EXISTS `wstampilantabledashboarduser_organisasi`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_organisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_organisasi` varchar(255) DEFAULT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_po
DROP TABLE IF EXISTS `wstampilantabledashboarduser_po`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_po` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_posisi
DROP TABLE IF EXISTS `wstampilantabledashboarduser_posisi`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_posisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_tg
DROP TABLE IF EXISTS `wstampilantabledashboarduser_tg`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_tg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_tingkat_golongan` tinytext,
  `kode_tingkat_golongan` tinytext,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_tingkatorganisasi
DROP TABLE IF EXISTS `wstampilantabledashboarduser_tingkatorganisasi`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_tingkatorganisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` tinytext,
  `nama_tingkat_organisasi` tinytext,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_tkj
DROP TABLE IF EXISTS `wstampilantabledashboarduser_tkj`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_tkj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_kelompok_jabatan` tinytext,
  `nama_tingkat_kelompok_jabatan` tinytext,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_to
DROP TABLE IF EXISTS `wstampilantabledashboarduser_to`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_to` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `nama_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_tp
DROP TABLE IF EXISTS `wstampilantabledashboarduser_tp`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_tp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_posisi` varchar(255) DEFAULT NULL,
  `nama_tingkat_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstampilantabledashboarduser_usm
DROP TABLE IF EXISTS `wstampilantabledashboarduser_usm`;
CREATE TABLE IF NOT EXISTS `wstampilantabledashboarduser_usm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext,
  `query_operator` longtext,
  `query_value` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstingkatgolongan
DROP TABLE IF EXISTS `wstingkatgolongan`;
CREATE TABLE IF NOT EXISTS `wstingkatgolongan` (
  `id_tingkat_golongan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` tinytext,
  `kode_tingkat_golongan` varchar(25) DEFAULT NULL,
  `nama_tingkat_golongan` tinytext,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `kode_golongan` tinytext,
  `keterangan` tinytext,
  `status_rekaman` tinytext,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `items` longtext,
  `tanggal_mulai` date DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_golongan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstingkatgolongan_item
DROP TABLE IF EXISTS `wstingkatgolongan_item`;
CREATE TABLE IF NOT EXISTS `wstingkatgolongan_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstingkatkelompokjabatan
DROP TABLE IF EXISTS `wstingkatkelompokjabatan`;
CREATE TABLE IF NOT EXISTS `wstingkatkelompokjabatan` (
  `id_tingkat_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_tingkat_kelompok_jabatan` tinytext,
  `nama_tingkat_kelompok_jabatan` tinytext,
  `kode_kelompok_jabatan` tinytext,
  `nama_kelompok_jabatan` tinytext,
  `dari_golongan` tinytext,
  `sampai_golongan` tinytext,
  `keterangan` tinytext,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` date DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_kelompok_jabatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstingkatorganisasi
DROP TABLE IF EXISTS `wstingkatorganisasi`;
CREATE TABLE IF NOT EXISTS `wstingkatorganisasi` (
  `id_tingkat_organisasi` int(11) NOT NULL AUTO_INCREMENT,
  `kode_tingkat_organisasi` tinytext,
  `nama_tingkat_organisasi` tinytext,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_organisasi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table db_hris_proses.wstingkatposisi
DROP TABLE IF EXISTS `wstingkatposisi`;
CREATE TABLE IF NOT EXISTS `wstingkatposisi` (
  `id_tingkat_posisi` int(11) NOT NULL AUTO_INCREMENT,
  `kode_tingkat_posisi` text,
  `nama_tingkat_posisi` text,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` text,
  `status_rekaman` text,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` text,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` text,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` text,
  `waktu_hapus` date DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_posisi`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
