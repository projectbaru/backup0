<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('header')->insert([
            [
                'id' => 1,
                'key' => 'table_perusahaan',
                'columns' => '["nama", "visi", "misi","aksi"]'
            ],
        ]);
    }
}
