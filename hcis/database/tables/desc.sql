-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table db_hris_proses.detail_desc_pekerjaan
DROP TABLE IF EXISTS `detail_desc_pekerjaan`;
CREATE TABLE IF NOT EXISTS `detail_desc_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc_id` int(11) DEFAULT NULL,
  `kode_posisi` varchar(50) DEFAULT NULL,
  `nama_posisi` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(250) DEFAULT NULL,
  `pdca` varchar(50) DEFAULT NULL,
  `bsc` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK__wsdeskripsipekerjaan` (`desc_id`),
  CONSTRAINT `FK__wsdeskripsipekerjaan` FOREIGN KEY (`desc_id`) REFERENCES `wsdeskripsipekerjaan` (`id_deskripsi_pekerjaan`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_hris_proses.detail_desc_pekerjaan: ~0 rows (approximately)
DELETE FROM `detail_desc_pekerjaan`;
/*!40000 ALTER TABLE `detail_desc_pekerjaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `detail_desc_pekerjaan` ENABLE KEYS */;

-- Dumping structure for table db_hris_proses.wsdeskripsipekerjaan
DROP TABLE IF EXISTS `wsdeskripsipekerjaan`;
CREATE TABLE IF NOT EXISTS `wsdeskripsipekerjaan` (
  `id_deskripsi_pekerjaan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_deskripsi` text DEFAULT NULL,
  `nama_atasan_langsung` tinytext DEFAULT NULL,
  `nama_posisi` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `deskripsi_pekerjaan` longtext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` date DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `nomor_dokumen` varchar(50) DEFAULT NULL,
  `edisi` text DEFAULT NULL,
  `tanggal_edisi` date DEFAULT NULL,
  `nomor_revisi` text DEFAULT NULL,
  `tanggal_revisi` date DEFAULT NULL,
  `nama_jabatan` text DEFAULT NULL,
  `nama_karyawan` text DEFAULT NULL,
  `divisi` text DEFAULT NULL,
  `lokasi_kerja` text DEFAULT NULL,
  `dibuat_oleh` varchar(50) DEFAULT NULL,
  `diperiksa_oleh` text DEFAULT NULL,
  `disahkan_oleh` text DEFAULT NULL,
  `fungsi_jabatan` text DEFAULT NULL,
  `tanggung_jawab` longtext DEFAULT NULL,
  `departemen` text DEFAULT NULL,
  `kode_posisi` text DEFAULT NULL,
  `nama_lokasi_kerja` text DEFAULT NULL,
  `nama_pengawas` text DEFAULT NULL,
  `lingkup_aktivitas` text DEFAULT NULL,
  `id_logo_perusahaan` text DEFAULT NULL,
  PRIMARY KEY (`id_deskripsi_pekerjaan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Dumping data for table db_hris_proses.wsdeskripsipekerjaan: ~3 rows (approximately)
DELETE FROM `wsdeskripsipekerjaan`;
/*!40000 ALTER TABLE `wsdeskripsipekerjaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `wsdeskripsipekerjaan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
