<?php
// routes/web.php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\DB;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/roles', 'roles_c@index');

// perusahaan
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/list_p', 'p_c@index')->name('list_p');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_reset_p', 'p_c@allData')->name('hapus_filter_p');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_p','p_c@tambah')->name('tambah_p');
Route::post('/simpan_p','p_c@simpan')->name('simpan_p');
Route::post('/simpan_pd','p_c@duplicate')->name('duplicate_pd');

Route::get('list_p/detail/{id_perusahaan}', 'p_c@detail')->name('detail_p');
Route::get('list_p/edit/{id_perusahaan}', 'p_c@edit')->name('edit_p');
Route::post('list_p/update', 'p_c@update')->name('update_p');
Route::get('list_p/hapus/{id_perusahaan}', 'p_c@hapus')->name('hapus_p');
Route::get('/list_p/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_p/hapus_banyak', 'p_c@multiDelete');
Route::get('/filter_p','p_c@filter')->name('ubah_tamp');
Route::get('/filter_p/{$id}','p_c@filterRemember')->name('ubah_tamp_filter');
Route::post('/filter_p','p_c@filterSubmit')->name('update_tamp');
Route::delete('deleteAll', 'p_c@deleteAll');

//alamat
Route::get('/list_a', 'a_c@index')->name('list_a');
Route::get('/list_a/cari', 'a_c@cari')->name('cari_a');
Route::get('/tambah_a','a_c@tambah')->name('tambah_a');
Route::get('list_a/edit/{id_alamat}', 'a_c@edit')->name('edit_a');
Route::get('list_a/detail/{id_alamat}', 'a_c@detail')->name('detail_a');
Route::post('list_a/update', 'a_c@update')->name('update_a');
Route::get('/list_reset_a', 'a_c@allData')->name('hapus_filter_a');

Route::post('/simpan_a','a_c@simpan')->name('simpan_a');
Route::post('/duplicate_a','a_c@duplicate')->name('duplicate_a');

Route::get('/filter_a','a_c@filter')->name('ubah_tamalamat');
Route::post('/filter_a','a_c@filterSubmit')->name('update_tamalamat');
Route::delete('deleteAll', 'a_c@deleteAll');
Route::post('/list_a/hapus_banyak', 'a_c@multiDelete');
Route::get('list_a/hapus/{id_alamat}', 'a_c@hapus')->name('hapus_a');

//organisasi
Route::get('/list_o','o_c@index')->name('list_o');
Route::post('/delete_multi', 'o_c@delete_multi')->name('deletemulti_organisasi');
Route::get('/tambah_o','o_c@tambah')->name('tambah_o');
Route::get('list_o/edit/{id_organisasi}', 'o_c@edit')->name('edit_o');
Route::get('list_o/hapus/{id_organisasi}', 'o_c@hapus')->name('hapus_o');
Route::get('list_o/detail/{id_organisasi}', 'o_c@detail')->name('detail_o');
Route::post('/simpan_o', 'o_c@simpan')->name('simpan_o');
Route::get('/cari_o', 'o_c@cari')->name('cari_o');
Route::post('/update_o','o_c@update')->name('update_o');
Route::delete('deleteAll', 'o_c@deleteAll');
Route::get('list_o/detail/{id_organisasi}', 'o_c@detail')->name('detail_o');
Route::get('/filter_o','o_c@filter')->name('ubah_tamo');
Route::post('/filter_o','o_c@filterSubmit')->name('update_tamo');
Route::post('/list_o/hapus_banyak', 'o_c@multiDelete');
Route::get('/set_back_display', 'o_c@reset');
Route::get('/list_reset_o', 'o_c@allData')->name('hapus_filter_o');


Route::get('/list_to','to_c@index')->name('list_to');
Route::post('/delete_multi', 'to_c@delete_multi')->name('deletemulti_torganisasi');
Route::get('/tambah_to','to_c@tambah')->name('tambah_to');
Route::get('list_to/edit/{id_tingkat_organisasi}', 'to_c@edit')->name('edit_to');
Route::get('list_to/hapus/{id_tingkat_organisasi}', 'to_c@hapus')->name('hapus_to');
Route::get('list_to/detail/{id_tingkat_organisasi}', 'to_c@detail')->name('detail_to');
Route::post('/simpan_to', 'to_c@simpan')->name('simpan_to');
Route::get('/list_to/cari', 'to_c@cari')->name('cari_to');
Route::post('update_to','to_c@update')->name('update_to');
Route::delete('deleteAll', 'to_c@deleteAll');
Route::get('list_to/detail/{id_tingkat_organisasi}', 'to_c@detail')->name('detail_wsto');
Route::get('/filter_to','to_c@filter')->name('ubah_tamto');
Route::post('/filter_to','to_c@filterSubmit')->name('update_tamto');
Route::post('/list_to/hapus_banyak', 'to_c@multiDelete');
Route::get('/set_back_display', 'to_c@reset');

Route::get('/list_to', 'to_c@index')->name('list_to');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');
Route::get('/tambah_wsperusahaan','p_c@tambah')->name('tambah_wsperusahaan');
Route::post('/simpan_wsperusahaan','p_c@simpan')->name('simpan_wsperusahaan');
Route::get('list_p/detail/{id_perusahaan}', 'p_c@detail')->name('detail_p');
Route::get('list_p/edit/{id_perusahaan}', 'p_c@edit')->name('edit_p');
Route::post('list_p/update', 'p_c@update')->name('update_p');
Route::get('list_p/hapus/{id_perusahaan}', 'p_c@hapus')->name('hapus_p');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('-', 'p_c@multiDelete');

Route::get('/filter_perusahaan','p_c@filter')->name('ubah_tampilanperusahaan');
Route::post('/filter_perusahaan','p_c@filterSubmit')->name('update_tampilanperusahaan');
Route::delete('deleteAll', 'p_c@deleteAll');

// golongan
Route::get('/list_g', 'g_c@index')->name('list_g');
Route::post('/delete_multi', 'g_c@delete_multi');
Route::get('/list_', 'g_c@allData');
Route::get('/set_back_display', 'g_c@reset');
Route::get('/list_reset_g', 'g_c@allData')->name('hapus_filter_g');

Route::get('/list_g/tambah_g','g_c@tambah')->name('tambah_g');
Route::post('/simpan_g','g_c@simpan')->name('simpan_g');
Route::get('/list_g/detail/{id_golongan}', 'g_c@detail')->name('detail_g');
Route::get('/list_g/edit/{id_golongan}', 'g_c@edit')->name('edit_g');
Route::post('list_g/update', 'g_c@update')->name('update_g');
Route::get('list_g/hapus/{id_golongan}', 'g_c@hapus')->name('hapus_g');
Route::get('/list_g/cari', 'p_c@cari')->name('cari_g');
Route::post('/list_g/hapus_banyak', 'g_c@multiDelete');

Route::get('/filter_g','g_c@filter')->name('ubah_tamg');
Route::post('/filter_g','g_c@filterSubmit')->name('update_tamg');
Route::delete('deleteAll', 'p_c@deleteAll');


// tingkat golongan
Route::get('/list_tg', 'tg_c@index')->name('list_tg');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wstg', 'tg_c@allData')->name('alldata_tg');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/list_tg/tambah_tg','tg_c@tambah')->name('tambah_tg');
Route::post('/list_tg/simpan_tg','tg_c@simpan')->name('simpan_tg');
Route::get('/list_tg/detail/{id_tingkat_golongan}', 'tg_c@detail')->name('detail_tg');
Route::get('/list_tg/edit/{id_tingkat_golongan}', 'tg_c@edit')->name('edit_tg');
Route::post('list_tg/update/', 'tg_c@update')->name('update_tg');
Route::get('list_tg/hapus/{id_tingkat_golongan}', 'tg_c@hapus')->name('hapus_tg');
Route::get('/list_wsperusahaan/cari', 'tg_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tg/hapus_banyak', 'tg_c@multiDelete');
Route::post('/list_tg/duplicate', 'tg_c@duplicate')->name('duplicate_tg');

Route::get('/filter_tg','tg_c@filter')->name('ubah_tamtg');
Route::post('/filter_tg','tg_c@filterSubmit')->name('update_tamtg');
Route::delete('deleteAll', 'p_c@deleteAll');

//tingkatposisi
Route::get('/list_tp', 'tp_c@index')->name('list_tp');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_tp','tp_c@tambah')->name('tambah_tp');
Route::post('/simpan_tp','tp_c@simpan')->name('simpan_tp');
Route::get('/list_tp/detail/{id_tingkat_posisi}', 'tp_c@detail')->name('detail_tp');
Route::get('/list_tp/edit/{id_tingkat_posisi}', 'tp_c@edit')->name('edit_tp');
Route::post('list_tp/update', 'tp_c@update')->name('update_tp');
Route::get('list_tp/hapus/{id_tingkat_posisi}', 'tp_c@hapus')->name('hapus_tp');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tp/hapus_banyak', 'tp_c@multiDelete');

Route::get('/filter_tp','tp_c@filter')->name('ubah_tamtp');
Route::post('/filter_tp','tp_c@filterSubmit')->name('update_tamtp');
Route::delete('deleteAll', 'p_c@deleteAll');

//posisi
Route::get('/list_po', 'po_c@index')->name('list_po');
Route::post('/delete_multi', 'po_c@delete_multi');
Route::get('/list_', 'po_c@allData');
Route::get('/set_back_display', 'po_c@reset');

Route::get('/tambah_po','po_c@tambah')->name('tambah_po');
Route::post('/simpan_po','po_c@simpan')->name('simpan_po');
Route::get('list_po/detail/{id_posisi}', 'po_c@detail')->name('detail_po');
Route::get('list_po/edit/{id_posisi}', 'po_c@edit')->name('edit_po');
Route::post('list_po/update', 'po_c@update')->name('update_po');
Route::get('list_po/hapus/{id_posisi}', 'po_c@hapus')->name('hapus_po');
Route::get('/list_po/cari', 'po_c@cari')->name('cari_wsperusahaan');
Route::post('/list_po/hapus_banyak', 'po_c@multiDelete');
Route::get('/filter_po','po_c@filter')->name('ubah_tampo');
Route::post('/filter_po','po_c@filterSubmit')->name('update_tampo');
Route::delete('deleteAll', 'po_c@deleteAll');

//jabatan
Route::get('/list_j', 'j_c@index')->name('list_j');
Route::post('/delete_multi', 'j_c@delete_multi');
Route::get('/list_', 'j_c@allData');
Route::get('/set_back_display', 'j_c@reset');

Route::get('/tambah_j','j_c@tambah')->name('tambah_j');
Route::post('/simpan_j','j_c@simpan')->name('simpan_j');
// Route::get('/detail','HomeController@detail');
Route::get('list_j/detail/{id_jabatan}', 'j_c@detail')->name('detail_j');
Route::get('list_j/edit/{id_jabatan}', 'j_c@edit')->name('edit_j');
Route::post('list_j/update', 'j_c@update')->name('update_j');
Route::get('list_j/hapus/{id_perusahaan}', 'j_c@hapus')->name('hapus_j');
Route::get('/list_j/cari', 'j_c@cari')->name('cari_j');
Route::post('/list_j/hapus_banyak', 'j_c@multiDelete');

Route::get('/filter_j','j_c@filter')->name('ubah_tamj');
Route::post('/filter_j','j_c@filterSubmit')->name('update_tamj');
Route::delete('deleteAll', 'p_c@deleteAll');

//kelompok jabatan
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kj', 'kj_c@index')->name('list_kj');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_kj','kj_c@tambah')->name('tambah_kj');
Route::post('/simpan_kj','kj_c@simpan')->name('simpan_kj');
// Route::get('/detail','HomeController@detail');
Route::get('list_kj/detail/{id_kelompok_jabatan}', 'kj_c@detail')->name('detail_kj');
Route::get('list_kj/edit/{id_kelompok_jabatan}', 'kj_c@edit')->name('edit_kj');
Route::post('list_kj/update', 'kj_c@update')->name('update_kj');
Route::get('list_kj/hapus/{id_kelompok_jabatan}', 'kj_c@hapus')->name('hapus_kj');
Route::get('/list_wsperusahaan/cari', 'kj_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kj/hapus_banyak', 'kj_c@multiDelete');

Route::get('/filter_kj','kj_c@filter')->name('ubah_tamkj');
Route::post('/filter_kj','kj_c@filterSubmit')->name('update_tamkj');
Route::delete('deleteAll', 'kj_c@deleteAll');

//lokasi kerja
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_lk', 'lk_c@index')->name('list_lk');
Route::post('/delete_multi', 'lk_c@delete_multi');
Route::get('/list_', 'lk_c@allData');
Route::get('/set_back_display', 'lk_c@reset');

Route::get('/tambah_lk','lk_c@tambah')->name('tambah_lk');
Route::post('/simpan_lk','lk_c@simpan')->name('simpan_lk');
Route::post('/simpan_duplicate','lk_c@simpan_duplicate')->name('simpan_duplicate');

// Route::get('/detail','HomeController@detail');
Route::get('list_lk/detail/{id_perusahaan}', 'lk_c@detail')->name('detail_wsperusahaan');
Route::get('list_lk/edit/{id_lokasi_kerja}', 'lk_c@edit')->name('edit_lk');
Route::post('list_lk/update', 'lk_c@update')->name('update_lk');
Route::get('list_lk/hapus/{id_lk}', 'lk_c@hapus')->name('hapus_lk');
Route::get('/list_wsperusahaan/cari', 'lk_c@cari')->name('cari_wsperusahaan');
Route::post('/list_lk/hapus_banyak', 'lk_c@multiDelete');
Route::get('/filter_lk','lk_c@filter')->name('ubah_tamlk');
Route::post('/filter_lk','lk_c@filterSubmit')->name('update_tamlk');
Route::delete('deleteAll', 'p_c@deleteAll');


//tkj
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_tkj', 'tkj_c@index')->name('list_tkj');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_tkj','tkj_c@tambah')->name('tambah_tkj');
Route::post('/simpan_tkj','tkj_c@simpan')->name('simpan_tkj');
// Route::get('/detail','HomeController@detail');
Route::get('list_tkj/detail/{id_tingkat_kelompok_jabatan}', 'tkj_c@detail')->name('detail_tkj');
Route::get('list_tkj/edit/{id_tingkat_kelompok_jabatan}', 'tkj_c@edit')->name('edit_tkj');
Route::post('/list_tkj/update', 'tkj_c@update')->name('update_tkj');
Route::get('list_tkj/hapus/{id_tingkat_kelompok_jabatan}', 'tkj_c@hapus')->name('hapus_tkj');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tkj/hapus_banyak', 'tkj_c@multiDelete');

Route::get('/filter_tkj','tkj_c@filter')->name('ubah_tamtkj');
Route::post('/filter_tkj','tkj_c@filterSubmit')->name('update_tamtkj');
Route::delete('deleteAll', 'p_c@deleteAll');

//upah standar minimum
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_usm', 'usm_c@index')->name('list_usm');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');
Route::get('/tambah_usm','usm_c@tambah')->name('tambah_usm');
Route::post('/simpan_usm','usm_c@simpan')->name('simpan_usm');
// Route::get('/detail','HomeController@detail');
Route::get('/list_usm/detail/{id_standar_upah_minimum}', 'usm_c@detail')->name('detail_usm');
Route::get('/list_usm/edit/{id_standar_upah_minimum}', 'usm_c@edit')->name('edit_usm');
Route::post('list_usm/update', 'usm_c@update')->name('update_usm');
Route::get('list_usm/hapus/{id_standar_upah_minimum}', 'usm_c@hapus')->name('hapus_usm');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_usm/hapus_banyak', 'usm_c@multiDelete');

Route::get('/list_usm/filter_sum','usm_c@filter')->name('ubah_tamsum');
Route::post('/list_usm/filter_sum','usm_c@filterSubmit')->name('update_tamsum');
Route::delete('deleteAll', 'p_c@deleteAll');

//kantor cabang
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kc', 'kc_c@index')->name('list_kc');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_kc','kc_c@tambah')->name('tambah_kc');
Route::post('/simpan_kc','kc_c@simpan')->name('simpan_kc');
// Route::get('/detail','HomeController@detail');
Route::get('list_kc/detail/{id_kantor_cabang}', 'kc_c@detail')->name('detail_kc');
Route::get('list_kc/edit/{id_kelas_cabang}', 'kc_c@edit')->name('edit_kc');
Route::post('list_kc/update', 'kc_c@update')->name('update_kc');
Route::get('list_kc/hapus/{id_kelas_cabang}', 'kc_c@hapus')->name('hapus_kc');
Route::get('/list_wsperusahaan/cari', 'kc_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kc/hapus_banyak', 'kc_c@multiDelete');

Route::get('/filter_kc','kc_c@filter')->name('ubah_tamkc');
Route::post('/filter_kc','kc_c@filterSubmit')->name('update_tamkc');
Route::delete('deleteAll', 'p_c@deleteAll');


//kantor cabang
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kac', 'kac_c@index')->name('list_kac');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_kac','kac_c@tambah')->name('tambah_kac');
Route::post('/simpan_kac','kac_c@simpan')->name('simpan_kac');
// Route::get('/detail','HomeController@detail');
Route::get('list_kac/detail/{id_kantor}', 'kac_c@detail')->name('detail_kac');
Route::get('list_kac/edit/{id_kantor}', 'kac_c@edit')->name('edit_kac');
Route::post('list_kac/update', 'kac_c@update')->name('update_kac');
Route::get('list_kac/hapus/{id_kantor}', 'kac_c@hapus')->name('hapus_kac');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kac/hapus_banyak', 'kac_c@multiDelete');

Route::get('/filter_kac','kac_c@filter')->name('ubah_tamkac');
Route::post('/filter_kac','kac_c@filterSubmit')->name('update_tampkac');
Route::delete('deleteAll', 'kac_c@deleteAll');

//deskripsi pekerjaan
// Route::get('/', 'HomeController@index', 'index');
// Route::get('/list_dp', 'dp_c@index')->name('list_dp');
Route::get('/list_dp', 'dp_c@index')->name('list_dp');
Route::post('/delete_multi', 'dp_c@delete_multi');
Route::get('/list_', 'dp_c@allData');
Route::get('/set_back_display', 'dp_c@reset');
// Route::get('list_hdp/tambah','dp_c@tambah_hdp')->name('tambah_hdp');
Route::get('list_dp/tambah','dp_c@tambah')->name('tambah_dp');
// Route::post('/simpan_dp','dp_c@simpan')->name('simpan_dp');
Route::post('/simpan_dp','dp_c@simpan_hdp')->name('simpan_dp');
Route::get('list_dp/detail/{id_deskripsi_pekerjaan}', 'dp_c@detail')->name('detail_dp');
// Route::get('list_hdp/detail/{id_deskripsi_pekerjaan}', 'dp_c@detail')->name('detail_hdp');
Route::get('list_hdp/edit/{id_deskripsi_pekerjaan}', 'dp_c@edit_hdp')->name('edit_hdp');
Route::get('list_dp/edit/{id_deskripsi_pekerjaan}', 'dp_c@edit')->name('edit_dp');

Route::post('list_dp/update', 'dp_c@update')->name('update_dp');
Route::get('list_dp/hapus/{id_deskripsi_pekerjaan}', 'dp_c@hapus')->name('hapus_dp');
// Route::get('list_dp/hapus_des/{id_deskripsi_pekerjaan}', 'dp_c@hapus')->name('hapus_des');

Route::get('/list_wsperusahaan/cari', 'dp_c@cari')->name('cari_wsperusahaan');
// Route::post('/list_dp/hapus_banyak', 'dp_c@multiDelete');

// Route::get('/filter_dp','dp_c@filter')->name('ubah_tamdp');
// Route::post('/filter_dp','dp_c@filterSubmit')->name('update_tamdp');
Route::get('/filter_dp','dp_c@filter')->name('ubah_tamdp');
Route::post('/filter_dp','dp_c@filterSubmit')->name('update_tamdp');

Route::delete('deleteAll', 'dp_c@deleteAll');

//grup lokasi kerja
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_glk', 'glk_c@index')->name('list_glk');
Route::post('/delete_multi', 'glk_c@delete_multi');
Route::get('/list_', 'glk_c@allData');
Route::get('/set_back_display', 'glk_c@reset');

Route::get('/tambah_glk','glk_c@tambah')->name('tambah_glk');
Route::post('/simpan_glk','glk_c@simpan')->name('simpan_glk');
// Route::get('/detail','HomeController@detail');
Route::get('list_glk/detail/{id_grup_lokasi_kerja}', 'glk_c@detail')->name('detail_glk');
Route::get('list_glk/edit/{id_grup_lokasi_kerja}', 'glk_c@edit')->name('edit_glk');
Route::post('list_glk/update', 'glk_c@update')->name('update_glk');
Route::get('list_glk/hapus/{id_grup_lokasi_kerja}', 'glk_c@hapus')->name('hapus_glk');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_glk/hapus_banyak', 'glk_c@multiDelete');
Route::get('/filter_glk','glk_c@filter')->name('ubah_tamglk');
Route::post('/filter_glk','glk_c@filterSubmit')->name('update_tamglk');
Route::delete('deleteAll', 'p_c@deleteAll');

Route::post('dropDownCity', 'DropdownController@dropDownCity');
Route::post('dropDownDistrict', 'DropdownController@dropDownDistrict');
Route::post('dropDownSubDistrict', 'DropdownController@dropDownSubDistrict');

//ptk
Route::get('/list_ptk', 'tpk_c@index')->name('list_ptk');
Route::get('/list_table', 'tpk_c@table_view')->name('view_ptk');
Route::post('/filter','tpk_c@filterSubmit')->name('update_tamptk');
Route::get('/filter','tpk_c@filter')->name('ubah_tamptk');
Route::get('/filter_ptk','tpk_c@filter')->name('filter_ptk');
Route::post('/list_table/hapus_banyak_table', 'tpk_c@multiDelete')->name('hapus_banyak_table');
Route::post('/list_data/hapus_banyak_data', 'tpk_c@multiDelete')->name('hapus_banyak_table');
Route::get('/list_ptk/edit/{id_ptk}', 'tpk_c@edit')->name('edit_ptk');
Route::post('/list_ptk/update', 'tpk_c@update')->name('update_ptk');
Route::get('/list_ptk/detail/{id_ptk}', 'tpk_c@detail')->name('detail_ptk');
Route::get('/list_ptk/ubah_ptk','tpk_c@tambah')->name('tambah_ptk');

Route::post('/list_ptk/detail/{id_ptk}', 'tpk_c@hapus')->name('hapus_ptk');

// Print Preview PTK
Route::get('/list_ptk/preview', 'tpk_c@show')->name('preview_ptk');

//
Route::post('/list_data/filter', 'tpk_c@filter_dua')->name('filter_dua_tpk');


// Fix Refresh Auto Insert
Route::get('/list_data', 'tpk_c@list_data_ptk')->name('ptk_submit_view');
Route::post('/list_data', 'tpk_c@submit_ptk')->name('ptk_submit');

// filter
Route::post('/list_data/filter', 'tpk_c@filter_dua')->name('filter_dua_tpk');


// tptk
// Route::get('/list_tptk', 'tptk_c@index')->name('list_tptk');
// Route::post('/simpan_tptk', 'tptk_c@simpan')->name('simpan_tptk');
// Route::get('/list_tptk/detail/{id}', 'tptk_c@show');
// Route::get('/list_tptk/edit/{id}', 'tptk_c@edit');

// Route::post('/list_tptk/update_tambahan', 'tptk_c@update_tambahan')->name('update_tptkt');
// Route::post('/list_tptk/update_penggantian', 'tptk_c@update_penggantian')->name('update_tptkp');

// Route::get('/list_tptk/tambah_tptk_t', 'tptk_c@tambah_tamb')->name('tambah_tptk');
// Route::get('/list_tptk/tambah_tptk_p', 'tptk_c@tambah_peng')->name('tambah_tptk_peng');

// Route::get('list_tptk/detail_t/', 'tptk_c@detail')->name('detail_tptk_t');
// Route::get('list_tptk/detail_p/', 'tptk_c@detail_peng')->name('detail_tptk_p');

// Route::get('/list_tptk/edit_tamb/{id_transaksi_ptk}', 'tptk_c@edit_tambahan')->name('edit_tptk_t');
// Route::get('list_tptk/edit_peng/{id_transaksi_ptk}', 'tptk_c@edit_peng')->name('edit_tptk_p');
// Route::post('/filter_tptk', 'tptk_c@filterSubmit')->name('update_tamtptk');
// Route::get('/filter_tptk', 'tptk_c@filter')->name('ubah_tamtptk');
// Route::post('/list_tptk/hapus_banyak', 'tptk_c@multiDelete');
// Route::get('/set_back_display', 'tptk_c@reset');
// Route::get('list_tptk/hapus/{id_transaksi_ptk}', 'tptk_c@hapus')->name('hapus_tptk');
// Route::post('/delete_multi', 'tptk_c@delete_multi');

Route::get('/list_tptk', 'tptk_c@index')->name('list_tptk');
Route::post('/simpan_tptk','tptk_c@simpan')->name('simpan_tptk');
Route::get('/list_tptk/detail/{id}', 'tptk_c@show');
Route::get('/list_tptk/edit/{id}', 'tptk_c@edit');

Route::post('/list_tptk/update_tambahan', 'tptk_c@update_tambahan')->name('update_tptkt');
Route::post('/list_tptk/update_penggantian', 'tptk_c@update_penggantian')->name('update_tptkp');

Route::get('/list_tptk/tambah_tptk_t','tptk_c@tambah_tamb')->name('tambah_tptk_t');
Route::get('/list_tptk/tambah_tptk_p','tptk_c@tambah_peng')->name('tambah_tptk_peng');

Route::get('list_tptk/detail_t/', 'tptk_c@detail')->name('detail_tptk_t');
Route::get('list_tptk/detail_p/', 'tptk_c@detail_peng')->name('detail_tptk_p');

Route::get('/list_tptk/edit_tamb/{id_transaksi_ptk}', 'tptk_c@edit_tambahan')->name('edit_tptk_t');
Route::get('list_tptk/edit_peng/{id_transaksi_ptk}', 'tptk_c@edit_peng')->name('edit_tptk_p');
Route::post('/filter_tptk','tptk_c@filterSubmit')->name('update_tamtptk');
Route::get('/filter_tptk','tptk_c@filter')->name('ubah_tamtptk');
Route::post('/list_tptk/hapus_banyak', 'tptk_c@multiDelete');
Route::get('/set_back_display', 'tptk_c@reset');
Route::get('/list_tptk/hapus/{id_transaksi_ptk}', 'tptk_c@hapus')->name('hapus_tptk');
Route::post('/delete_multi', 'tptk_c@delete_multi');
//dcp
Route::get('/list_dcp', 'dcp_c@index')->name('list_dcp');
Route::post('/list_dcp/filter', 'dcp_c@filterList')->name('filter_dcp');
Route::get('/list_dcp/tambah', 'dcp_c@tambah')->name('tambah_dcp');
Route::get('/list_dcp/tambah_u', 'dcp_c@tambah_u')->name('tambah_dcp');
Route::post('/list_dcp/simpan', 'dcp_c@simpan')->name('simpan_dcp');
Route::get('/list_dcp/edit/{id_cv_pelamar}', 'dcp_c@edit')->name('edit_dcp');
Route::get('/list_dcp/detail/{id_cv_pelamar}', 'dcp_c@detail')->name('detail_dcp');
Route::post('/list_dcp/hapus_banyak', 'dcp_c@multiDelete');
Route::post('/list_dcp/update', 'dcp_c@update')->name('update_dcp');
Route::post('/list_dcp/update-dcp', 'dcp_c@updateDCP')->name('update-dcp');
Route::get('/filter_dcp', 'dcp_c@filter')->name('ubah_tamdcp');
Route::get('/filter_dcp', 'dcp_c@filterSubmit')->name('update_tamdcp');
Route::get('/list_dcp/hapus/{id_cv_pelamar}', 'dcp_c@hapus')->name('hapus_dcp');
Route::get('/list_dcp/hapus/', 'dcp_c@multiDelete');
Route::get('/list_dcp/edit_data/{id_cv_pelamar}', 'dcp_c@editData')->name('edit_data_dcp');
Route::get('/list_dcp/detail_data/{id_cv_pelamar}', 'dcp_c@detailData')->name('detail_data_dcp');
Route::post('/list_data/filter', 'tpk_c@filter_dua')->name('filter_dua_dcp');
// Route::post('/list_data/filter_dcp', 'tpk_c@filter_dcp')->name('filter_dcp');


//data

Route::get('/tambah_dcp','dcp_c@tambah')->name('tambah_dcp');
Route::get('/list_glk/hapus/{id_grup_lokasi_kerja}', 'glk_c@hapus')->name('hapus_glk');
Route::post('/list_glk/hapus_banyak', 'glk_c@multiDelete');
Route::post('/list_lk/hapus_banyak', 'lk_c@multiDelete');
Route::delete('deleteAll', 'p_c@deleteAll');
Route::get('/filter_pkhr','dcp_c@filter')->name('ubah_tambpkhr');


//DPP
Route::get('/list_dpp', 'dpp_c@index')->name('list_dpp');
Route::post('/list_dpp/simpan', 'dpp_c@simpan_api')->name('simpan_dpp');
Route::get('/list_dpp/tambah_dpp','dpp_c@tambah')->name('tambah_dpp');
Route::get('/list_dpp/tambah_user','dpp_c@tambah_user')->name('tambah_user');
Route::get('/list_dpp/detail/{id_data_pelamar}','dpp_c@detail')->name('detail_dpp');
Route::get('/list_dpp/edit/{id_data_pelamar}','dpp_c@edit')->name('edit_dpp');
Route::post('/list_dpp/update', 'dpp_c@update')->name('update_dpp');
Route::get('/list_dpp/filter_dpp','dpp_c@filter')->name('ubah_tampersonal');
Route::post('/list_dpp/filter_dpp','dpp_c@filterSubmit')->name('update_tampersonal');
Route::get('/list_dpp/hapus/', 'dcp_c@hapus')->name('hapus_dpp');


//step
Route::get('/list_step', 'step_c@index')->name('list_step');
Route::get('/list_step/tambah','step_c@tambah')->name('tambah_step');
Route::post('/list_step/simpan','step_c@simpan')->name('simpan_step');
Route::post('/list_step/update','step_c@update')->name('update_step');
Route::get('/list_step/detail/{id_step_process_recruitment}','step_c@detail')->name('detail_step');
Route::get('/list_step/hapus/{id_step_process_recruitment}', 'step_c@hapus')->name('hapus_step');
Route::get('/list_step/edit/{id_step_process_recruitment}','step_c@edit')->name('ubah_step');
Route::get('/list_step/detail/{id_step_process_recruitment}','step_c@detail')->name('detail_step');

//process
Route::get('/list_process', 'pro_c@index')->name('list_process');
Route::get('/list_process/tambah','pro_c@tambah')->name('tambah_pro');
Route::post('/list_process/update','pro_c@update')->name('update_pro');
Route::post('/list_process/simpan','pro_c@simpan')->name('simpan_pro');
Route::get('/list_process/edit/{id_process_recruitment}','pro_c@edit')->name('edit_pro');
Route::get('/list_process/hapus/{id_process_recruitment}','pro_c@hapus')->name('hapus_pro');
Route::get('/list_process/detail/{id_process_recruitment}','pro_c@detail')->name('detail_pro');
Route::get('/list_process/d_fptk/{no_dokumen}','pro_c@detail_fptk')->name('detail_fptk');
Route::get('/list_process/d_dcp/{kode_cv_pelamar}','pro_c@detail_cv')->name('detail_cv');
Route::get('/list_process/d_dpp/{kode_data_pelamar}','pro_c@detail_dpp')->name('detail_data');
Route::get('/get-steps-by-position', 'pro_c@getStepsByPosition');
Route::get('/list_process/detail_process/{id_process_recruitment}','pro_c@getDetailProcess')->name('list_process.detail_process');

//fptk
Route::get('/list_fptk', 'fptk_c@index')->name('list_fptk');
Route::get('/list_fptk/tambah','fptk_c@tambah')->name('tambah_fptk');
Route::get('/list_fptk/l_approv','fptk_c@list_approval')->name('list_apr_fptk');
Route::get('/list_fptk/s_approv','fptk_c@approval')->name('status_apr_fptk');
Route::get('/list_fptk/send_approv_list','fptk_c@approval')->name('status_apr_fptk');
Route::get('/list_fptk/send_a','fptk_c@index_send_a')->name('status_apr_fptk');
Route::get('/list_fptk/edit/{id_fptk}','fptk_c@edit')->name('edit_fptk');
Route::get('/list_fptk/detail/{id_fptk}','fptk_c@detail')->name('detail_fptk');
Route::get('/list_fptk/detail_ap/{id_fptk}','fptk_c@detail_ap')->name('detail_ap');
Route::get('/list_fptk/preview/{id_fptk}','fptk_c@preview')->name('preview_fptk');
Route::get('/list_fptk/upload_doc/{id_fptk}','fptk_c@upload_doc')->name('upload_doc_fptk');
Route::post('/list_fptk/hapus/{id_fptk}','fptk_c@hapus')->name('hapus_fptk');
Route::post('/list_fptk/update','fptk_c@update')->name('update_fptk');
Route::post('/list_fptk/simpan','fptk_c@simpan')->name('simpan_fptk');
Route::get('/list_fptk/permintaan_a', 'fptk_c@index_a')->name('list_fptk_a');
Route::get('/list_fptk/send_app/{id_fptk}', 'fptk_c@actionSendapproval_2')->name('send_approval');
Route::post('/list_dcp/hapus_banyak', 'dcp_c@multiDelete');
Route::post('/list_fptk/duplicate_fptk','fptk_c@duplicate')->name('duplicate_fptk');

// Lamar Pekerjaan
Route::get('/lamar_pekerjaan', 'list_it@index')->name('list_it');
Route::get('/lamar_pekerjaan/{id_lowongan_kerja}', 'list_it@detail')->name('detail_it');
Route::post('/lamar_pekerjaan/create', 'list_it@simpan')->name('simpan_it');



//fptk report
Route::get('/list_fptk_r', 'fptk_c@fptk_r')->name('list_fptk_r');
Route::get('/list_fptk_r/detail/{id_fptk}', 'fptk_c@detail_fptk_r')->name('detail_fptk_r');

//Lowongan kerja


Route::get('/list_loker', 'lo_c@index')->name('list_loker');
Route::post('/list_loker/filter', 'lo_c@filterList')->name('filter_loker');
Route::post('/list_lo/filter', 'lo_c@filterList')->name('filter_lo');
Route::get('/list_loker/tambah','lo_c@tambah')->name('tambah_lo');
Route::get('/list_loker/hapus/{id_lowongan_kerja}','lo_c@hapus')->name('hapus_lo');
Route::post('/list_loker/update','lo_c@update')->name('update_lo');
Route::get('/list_loker/detail/{id_lowongan_kerja}','lo_c@detail')->name('detail_lo');
Route::post('/list_loker/simpan','lo_c@simpan')->name('simpan_lo');
Route::get('/list_loker/edit/{id_lowongan_kerja}','lo_c@edit')->name('edit_lo');
Route::get('/list_loker/filter_loker','lo_c@filter')->name('ubah_tamplo');
Route::get('/list_loker/filter_loker','lo_c@filterSubmit')->name('update_tamlo');
Route::get('/list_loker/dashboard', 'lo_c@dashboard')->name('dashboard_lo');
Route::get('/list_loker/list_data', 'lo_c@list_data')->name('list_data');
Route::post('/list_loker/hapus_banyak', 'lo_c@multiDelete')->name('list_data');
Route::get('/list_loker/detail_lowongan/{}', 'lo_c@list_detail')->name('list_detail_lowongan');
Route::post('/list_loker/hapus_banyak', 'lo_c@multiDelete')->name('list_data');
Route::get('/list_loker/list_data_lowongan', 'lo_c@list_data_loker')->name('list_data');

Route::get('/list_loker/detail_data/{id_lowongan_kerja}', 'lo_c@DetailData')->name('detail_lowongan');
Route::get('/list_loker/edit_data/{id_lowongan_kerja}', 'lo_c@editData')->name('edit_lowongan');

// analisa
Route::get('/list_an', 'an_c@index')->name('list_an');
Route::get('/list_an/tambah','an_c@tambah')->name('tambah_an');
Route::get('/list_an/edit/{id_analisa_hasil_interview}','an_c@edit')->name('edit_an');
Route::get('/list_an/hapus/{id_analisa_hasil_interview}','an_c@hapus')->name('hapus_an');
Route::get('/list_an/preview/{id_analisa_hasil_interview}','an_c@preview')->name('preview_an');
Route::post('/list_an/simpan','an_c@simpan')->name('simpan_an');
Route::get('/list_an/detail/{id_analisa_hasil_interview}','an_c@detail')->name('detail_an');
Route::post('/list_an/update','an_c@update')->name('update_an');
Route::post('/list_an/hapus_banyak', 'an_c@multiDelete');
Route::get('/list_an/beri_analisa', 'an_c@beri_analisa');
Route::post('/list_an/cari', 'an_c@cari_list')->name('cari_an');

//ontime
Route::get('/filter_of','of_c@filter')->name('view_of');

Route::get('/list_of', 'of_c@index')->name('list_of');
Route::get('/list_of/tambah','of_c@tambah')->name('tambah_of');
Route::get('/list_of/edit/{id_ontime_fulfillment}','of_c@edit')->name('edit_of');
Route::get('/list_of/preview','of_c@preview')->name('preview_of');
Route::post('/list_of/simpan','of_c@simpan')->name('simpan_of');
Route::post('/list_of/update','of_c@update')->name('update_of');
Route::get('/list_of/detail/{id_ontime_fulfillment}','of_c@detail')->name('detail_of');
Route::post('/list_of/hapus_banyak', 'of_c@multiDelete');

//rc_fulfillment_rate
Route::get('/list_fr', 'fr_c@index')->name('list_fr');
Route::get('/list_fr/tambah','fr_c@tambah')->name('tambah_fr');
Route::get('/list_fr/edit/{id_fulfillment_rate}','fr_c@edit')->name('edit_fr');
Route::get('/list_fr/simpan','fr_c@simpan')->name('simpan_fr');
Route::get('/list_fr/detail','fr_c@detail')->name('detail_fr');
Route::post('/list_fr/hapus_banyak', 'fr_c@multiDelete')->name('detail_fr');

//pkhr
Route::get('/list_pen', 'pen_c@index')->name('list_pen');
Route::get('/list_pen/tambah','pen_c@tambah')->name('tambah_fr');
Route::get('/list_pen/edit/{id_fulfillment_rate}','pen_c@edit')->name('edit_fr');
Route::post('/list_pen/simpan','pen_c@simpan')->name('simpan_fr');
Route::get('/list_pen/detail','pen_c@detail')->name('detail_fr');
Route::post('/list_pen/hapus_banyak', 'pen_c@multiDelete')->name('detail_fr');

// budgeting recruitment
// Route::get('/list_br', 'lo_c@index')->name('list_br');
// Route::get('/list_br/tambah','pen_c@tambah')->name('tambah_br');
// Route::get('/list_br/edit/{id_fulfillment_rate}','pen_c@edit')->name('edit_br');
// Route::get('/list_br/simpan','pen_c@simpan')->name('simpan_br');
// Route::get('/list_br/detail','pen_c@detail')->name('detail_br');
// Route::post('/list_br/hapus_banyak', 'pen_c@multiDelete')->name('detail_br');

// rc_applicant_target_candidat_buffer

Route::get('/list_at', 'atcb_c@index')->name('list_at');
Route::get('/list_at/tambah', 'atcb_c@tambah')->name('tambah_at');
Route::get('/list_at/preview', 'atcb_c@preview')->name('preview_at');
Route::post('/list_at/tambah', 'atcb_c@store')->name('store_at');
Route::put('/list_at/update/{id}', 'atcb_c@update')->name('update_atcb');
Route::get('/list_at/edit/{id_applicant}', 'atcb_c@edit')->name('edit_at');
Route::post('/list_at/simpan', 'atcb_c@simpan')->name('simpan_br');
Route::get('/list_at/detail/{id_applicant}', 'atcb_c@detail')->name('detail_at');
Route::get('/list_at/update', 'atcb_c@update')->name('detail_br');
Route::post('/list_at/hapus_banyak', 'atcb_c@multiDelete')->name('detail_br');
Route::get('/list_at/hapus/{id_applicant}','atcb_c@hapus')->name('hapus_at');

//rc_data_wawancara_kandidat
Route::get('/list_dw', 'dw_c@index')->name('list_dw');
Route::get('/list_dw/tambah','dw_c@tambah')->name('tambah_dw');
Route::get('/list_dw/edit/{id_data_wawancara_kandidat}','dw_c@edit')->name('edit_dw');
Route::get('/list_dw/simpan','dw_c@simpan')->name('simpan_dw');
Route::get('/list_dw/detail','dw_c@detail')->name('detail_dw');
Route::post('/list_dw/hapus_banyak', 'dw_c@multiDelete')->name('detail_dw');

//pkhr



Route::get('/list_pkhr', 'pkhr_c@index')->name('list_pkhr');
Route::get('/list_pkhr/tambah', 'pkhr_c@tambah')->name('tambah_pkhr');
Route::post('/list_pkhr/simpan', 'pkhr_c@simpan')->name('simpan_pkhr');
Route::get('/list_pkhr/edit/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@edit')->name('edit_pkhr');
Route::get('/list_pkhr/edit_pkhr/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@edit_pkhr')->name('editpkhr');
Route::get('/list_pkhr/dump_edit/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@dump_edit');
Route::post('/list_pkhr/update', 'pkhr_c@update')->name('update_pkhr');
Route::post('/list_pkhr/update_pkhr', 'pkhr_c@update_pkhr')->name('updatepkhr');
Route::get('/list_pkhr/detail/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@detail')->name('detail_pkhr');
Route::get('/list_pkhr/hapus/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@hapus')->name('hapus_pkhr');
Route::get('/list_pkhr/preview/{id_pencapaian_kpi_hr_recruitment}', 'pkhr_c@preview')->name('preview_pkhr');

//dwk
Route::get('/list_pdwk', 'dwk_c@index_pdwk')->name('list_pdwk');
// Route::get('/list_dwk', 'dwk_c@index_dwk')->name('list_dwk');
Route::post('/list_dwk/simpan_dwk', 'dwk_c@post_tambah_dwk')->name('simpan_dwk');
Route::get('/list_dwk/tambah_dwk', 'dwk_c@tambah_dwk')->name('tambah_dwk');
Route::get('/list_dwk/tambah_pdwk', 'dwk_c@tambah_pdwk')->name('tambah_pdwk');
Route::get('/list_dwk/{id_penyiapan_dwk}', 'dwk_c@tambah_kan')->name('list_dwk');
Route::post('/list_pdwk/simpan_pdwk','dwk_c@simpan_pdwk')->name('simpan_pdwk');
Route::get('/list_pdwk/edit_pdwk/{id_penyiapan_dwk}','dwk_c@edit_pdwk')->name('edit_pdwk');
Route::get('/list_dwk/edit_dwk/{id_data_wawancara_kandidat}','dwk_c@edit_dwk')->name('edit_dwk');
Route::post('/list_pdwk/update_pdwk','dwk_c@update_pdwk')->name('update_pdwk');
Route::get('/list_dwk/detail_dwk/{id_penyiapan_dwk}','dwk_c@detail_dwk')->name('detail_dwk');
Route::get('/list_pdwk/detail_pdwk/{id_penyiapan_dwk}','dwk_c@detail_pdwk')->name('detail_pdwk');
Route::post('/list_dwk/hapus_dwk/{id_data_wawancara_kandidat}','dwk_c@hapus_dwk')->name('hapus_dwk');
Route::get('/list_pdwk/hapus_pdwk/{id_penyiapan_dwk}','dwk_c@hapus_pdwk')->name('hapus_pdwk');
Route::get('/list_dwk/preview_dwk/{id_penyiapan_dwk}', 'dwk_c@preview_dwk')->name('preview_dwk');
Route::post('/list_dwk/update_dwk','dwk_c@update_dwk')->name('update_dwk');
Route::get('/get-dwk-pr', 'pro_c@getDwkPr');

//dwk


//br
Route::get('/list_br', 'br_c@index')->name('list_br');
Route::get('/list_br/tambah', 'br_c@tambah')->name('tambah_br');
Route::get('/list_br/edit/{rc_budgeting_recruitment}','br_c@edit')->name('edit_br');
Route::get('/list_br/detail/{id_budgeting_recruitment}','br_c@detail')->name('detail_br');
Route::post('/list_br/update/{id_budgeting_recruitment}','br_c@update')->name('update_br');
Route::get('/list_br/hapus/{id_budgeting_recruitment}','br_c@hapus')->name('hapus_br');
Route::post('/list_br/simpan', 'br_c@simpan')->name('simpan_br');
Route::get('/list_br/preview/{id_budgeting_recruitment}', 'br_c@preview')->name('preview_br');
Route::post('/list_br/hapus_banyak', 'br_c@multiDelete');
Route::post('/list_br/multiDelete', 'br_c@multiDelete');

//br
Route::get('/list_of/tambah', 'of_c@tambah')->name('tambah_of');

//fr
Route::post('/getKodePerusahaanDanPosisi', 'tpk_c@getKodePerusahaanDanPosisi')->name('getKodePerusahaanDanPosisi');

Route::get('/list_ptk/lokasi-kerja/{nama_group_lokasi_kerja}', 'tpk_c@getLokasiKerjaOptions')->name('list_ptk.lokasi_kerja');
Route::get('/list_ptk/lokasi-kerja-detail/{lokasi_kerja}', 'tpk_c@getLokasiKerjaDetail')->name('list_ptk.lokasi_kerja-detail');

// Lamar Pekerjaan
Route::get('/lamar_pekerjaan', 'list_it@index')->name('list_it');
Route::get('/lamar_pekerjaan/{id_lowongan_kerja}', 'list_it@detail')->name('detail_it');
Route::get('/lamar_pekerjaan/create_html/{id_lowongan_kerja}', 'list_it@createHtml')->name('create_html_it');
Route::post('/lamar_pekerjaan/create', 'list_it@simpan')->name('simpan_it');
Route::get('/lamar_pekerjaan/lamar/{id_lowongan_kerja}', 'list_it@edit')->name('edit_data_it');


// Route::get('/list_br', 'fr_c@index')->name('list_br');
