<!-- views/re/dwk/tambah_dwk.blade.php -->

@extends('layouts.master')
@section('title') Dashboard @endsection

@push('css')
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
@endpush

@section('content')
@component('components.breadcrumb')
@slot('li_1') Recruitment @endslot
@slot('title') Recruitment @endslot
@endcomponent
<div class="card">
    <div class="card-header" style="">
        <b style="font-size: 18px;">Tambah Data Wawancara Kandidat</b>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header" style="background-color: rgb(47 116 181);">
            </div>
            <div class="card-body">
				<form action="{{route('simpan_dwk')}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					
					<input type="hidden" name="temp_id" id="temp_id" />
					<div class="form-group" style="width:95%;" >

						<p>Tambah Data Pribadi</p>
						<hr>
						<div class="row">
							<input type="hidden" required name="pdwk_id" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" value="{{ request()->get('pdwk_id') }}">

							<div class="col" style="font-size: 10px;">
								
								<input type="hidden" required name="kode_data_wawancara_kandidat" readonly style="font-size:11px;" class="form-control form-control-sm" id="kode_data_wawancara_kandidat" value="{{$randomId}}" placeholder="">
							
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode PR Kandidat</label>
									<div class="col-sm-9">
										<select name="kode_process_recruitment_kandidat" id="kode_process_recruitment_kandidat" style="font-size:11px;" class="form-control form-control-sm">
										<option value="" selected disabled>-- Pilih Kode PR Kandidat --</option>	
											@foreach($kprk as $data)
											<option value="{{$data->kode_process_recruitment}}" data-id="{{ $data->id_process_recruitment }}" data-kode_cv_pelamar="{{$data->kode_cv_pelamar}}" data-nama_kandidat="{{$data->nama_kandidat}}">{{$data->kode_process_recruitment}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Kode CV Kandidat</label>
									<div class="col-sm-9">
										<input type="text" required name="kode_cv_pelamar" readonly style="font-size:11px;" class="form-control form-control-sm" id="kode_cv_pelamar" placeholder="">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kandidat</label>
									<div class="col-sm-9">
										<input type="text" required name="nama_kandidat" readonly style="font-size:11px;" class="form-control form-control-sm" id="nama_kandidat" placeholder="">
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Hasil Konfirmasi</label>
									<div class="col-sm-9">
										<div class="form-check">
											<input class="form-check-input" type="radio" name="hasil_konfirmasi" value="hadir via" id="exampleRadios1">
											<label class="form-check-label" for="exampleRadios1">
												<input type="text" disabled name="via" class="form-control form-control-sm" style="font-size:11px;"  id="select_area_kelas_cabang" placeholder="">
											</label>
										</div><br>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="hasil_konfirmasi" value="tidak hadir" id="exampleRadios2">
											<label class="form-check-label" style="font-size:11px;" for="exampleRadios2">
												Tidak Hadir
											</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tempat</label>
									<div class="col-sm-9">
										<input type="text" required name="tempat" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<hr>
						<b>Data Interview</b>
						<br>
						<br>
						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jam</label>
									<div class="col-sm-9">
										<input type="time"  min="00:00" max="24:00" required name="jam_wawancara" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
									<div class="col-sm-9">
										<input type="hidden" readonly required name="interview_1" style="font-size:11px;" value="Interview 1" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
									<div class="col-sm-9">
										<input type="hidden" readonly required name="interview_1" style="font-size:11px;" value="Interview 1" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<div id="interview-list"></div>
					</div>
					<hr>
					<b>Hasil Interview</b>
					<div class="row">
						<div class="col">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Atasan</label>
								<div class="col-sm-9">
									<select name="nama_atasan" id="nama_atasan" style="font-size:11px;" class="form-control form-control-sm">
									<option value="">ab</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">DOJ</label>
								<div class="col-sm-9">
									<input type="date"  name="date_on_join" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Status</label>
								<div class="col-sm-9">
									<select name="status_hasil_interview" id="status_hasil_interview" style="font-size:11px;" class="form-control form-control-sm">
										<option value="On Progress">On Progress</option>
										<option value="Done">Done</option>
										<option value="Cancel">Cancel</option>
										<option value="Disarankan">Disarankan</option>
										<option value="Dicadangkan">Dicadangkan</option>
										<option value="Tidak Disarankan">Tidak Disarankan</option>
										<option value="Magang">Magang</option>
										<option value="Follow Up">Follow Up</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Jabatan</label>
								<div class="col-sm-9">
									<select name="jabatan" id="jabatan" style="font-size:11px;" class="form-control form-control-sm">
										@foreach($jab['looks'] as $data)
										<option value="{{$data->nama_jabatan}}">{{$data->nama_jabatan}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Lokasi</label>
								<div class="col-sm-9">
									<select name="lokasi_kerja" id="lokasi_kerja" style="font-size:11px;" class="form-control form-control-sm">
										@foreach($lk['looks'] as $data)
										<option value="{{$data->nama_lokasi_kerja}}">{{$data->nama_lokasi_kerja}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
								<div class="col-sm-9">
									<textarea type="text" required name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div style="width: 100%;">
						<table>
							<tr>
								<td>
									<button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
								</td>
								<td>
									
									<a href="{{ route('list_pdwk') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
								</td>
							</tr>
						</table>
					</div>
					<br>
				</form>
			</div>
		</div>
	</div>
</div>	
@endsection
@push('scripts')
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/jquery.datatable.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('assets/js/vendor-all.min.js')}}"></script>


<script>
$('#kode_process_recruitment_kandidat').change(function(){
	$('#kode_cv_pelamar').val($('#kode_process_recruitment_kandidat option:selected').data('kode_cv_pelamar'));
	$('#nama_kandidat').val($('#kode_process_recruitment_kandidat option:selected').data('nama_kandidat'));
	const id = $('#kode_process_recruitment_kandidat option:selected').data('id')
	let url = "{{ url('/list_process/detail_process/:id') }}";
	$.ajax({
    url: url.replace(':id', id),
    type: 'GET',
    dataType: 'JSON',
    cache: false,
    success: function(res) {
			console.log(res.data);
			$('#interview-list').html("");
      res.data.map((data, i) => {
				$('#interview-list').append(
					`						<div class="row">
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;"></label>
									<div class="col-sm-9">
										<input type="text" readonly required name="interview_${i+1}" style="font-size:11px;" value="${data.nama_step}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal</label>
									<div class="col-sm-9">
										<input type="date" required name="tanggal_interview_${i+1}" style="font-size:11px;" value="${data.tanggal_aktual}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
									</div>
								</div>
							</div>
							<div class="col" style="font-size: 10px;">
								<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm" style="font-size:11px;">Oleh</label>
									<div class="col-sm-9">
										<select name="nama_pewawancara_${i+1}" required class="form-control form-control-sm" style="font-size:11px;">
											<option value="">a</option>
                                        </select><a style="color:red;">*wajib isi</a>
									</div>
								</div>
							</div>
						</div>`
				)
			})
    },
    error: function(e) {
      console.error(e);
    },
  });
})
</script>

<script>
	$('#exampleRadios1').click(function(){
		$("#select_area_kelas_cabang").attr("disabled", false);
		$("#input_area_kelas_cabang").attr("disabled", true);
		$("#select_area_kelas_cabang").attr("required", true);
		$("#input_area_kelas_cabang").attr("required", false);
		$("#input_area_kelas_cabang").val("");
	})
	$('#exampleRadios2').click(function(){
		$("#select_area_kelas_cabang").attr("disabled", true);
		$("#input_area_kelas_cabang").attr("disabled", false);
		$("#select_area_kelas_cabang").attr("required", false);
		$("#input_area_kelas_cabang").attr("required", true);
		$("#select_area_kelas_cabang").val("");
	})
</script>

@endpush
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_delete").click(function(e) {
            e.preventDefault();
            var link = $(this);
            var linkHref = link.attr('href');
            console.log(linkHref);

            Swal.fire({
                title: '<h4>Anda yakin ingin menghapus?</h4>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",

                confirmButtonText: "<i>Ya, hapus!</i>",
                // buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: linkHref,
                        type: 'GET',
                        dataType: 'JSON',
                        cache: false,
                        success: function(res) {
                            if (res.status) {
                                Swal.fire({
                                    icon: "success",
                                    title: "Berhasil!",
                                    text: "Data telah terhapus.",
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "{{ URL::to('list_dpp') }}";
                                    }
                                });
                            }
                        },
                        error: function(e) {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text: "Data tidak terhapus.",
                            });
                        },
                    });
                }
            })
        });
    });
</script>



@endsection

